/* ********************************************************************************************************* *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
Io.Oidis.Builder.DAO.Resources.Data({
    $interface: "IProjectSolution",
    extendsConfig: "dependencies/io-oidis-connector/bin/project/configs/licensesRegister.jsonp",
    licensesRegister: {
        "io-oidis-builder": {
            format: "executable binary and embedded resources",
            location: [
                "cmd",
                "[application-name][.exe]",
                "[application-name][.exe]/package.json",
                "[application-name][.exe]/resource/configs",
                "[application-name][.exe]/resource/data/Io/Oidis/Builder",
                "[application-name][.exe]/resource/graphics/icon.ico",
                "[application-name][.exe]/resource/esmodules-[build-time]/Io/Oidis/Builder",
                "[application-name][.exe]/resource/javascript/loader.min.js",
                "[application-name][.exe]/resource/javascript/[package-name]-[version].d.ts",
                "[application-name][.exe]/resource/javascript/[package-name]-[version].min.js",
                "[application-name][.exe]/resource/javascript/[package-name]-[version].min.js.map",
                "[application-name][.exe]/resource/scripts"
            ]
        },
        "concat-with-sourcemaps": {
            version: ">= 1.1.0",
            description: "NPM module for concatenating files and generating source maps",
            author: "Florian Reiterer",
            license: "ISC. See LICENSE.md",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/concat-with-sourcemaps"]
        },
        "elastic-apm-node": {
            version: ">= 2.0.6",
            description: "The official Elastic APM agent for Node.js",
            author: "Thomas Watson Steen",
            license: "BSD-2-Clause. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/elastic-apm-node"]
        },
        "dependency-tree": {
            version: ">= 6.2.1",
            description: "Get the dependency tree of a module",
            author: "Dependents",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/dependency-tree"]
        },
        encoding: {
            version: ">= 0.1.12",
            description: "Simple wrapper around node-iconv and iconv-lite to convert strings from one encoding to another",
            author: "Andris Reinman",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/encoding"]
        },
        "file-api": {
            version: ">= 0.10.4",
            description: "HTML5 FileAPI implemented in Node.js",
            author: "AJ ONeal",
            license: "Apache-2.0. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/file-api"]
        },
        "clean-css": {
            version: ">= 4.2.1",
            description: "A well-tested CSS minifier",
            author: "Jakub Pawlowicz",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/clean-css"]
        },
        chai: {
            version: ">= 4.2.0",
            description: "BDD/TDD assertion library for node.js and the browser. Test framework agnostic",
            author: "Jake Luer",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/chai"]
        },
        "istanbul-lib-instrument": {
            version: ">= 3.0.0",
            description: "Core istanbul API for JS code coverage",
            author: "Krishnan Anantheswaran",
            license: "BSD-3-Clause. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/istanbul-lib-instrument"]
        },
        "istanbul-lib-report": {
            version: ">= 3.0.1",
            description: "Base reporting library for istanbul",
            author: "Krishnan Anantheswaran",
            license: "BSD-3-Clause. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/istanbul-lib-report"]
        },
        "istanbul-reports": {
            version: ">= 3.1.6",
            description: "istanbul reports",
            author: "Krishnan Anantheswaran",
            license: "BSD-3-Clause. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/istanbul-reports"]
        },
        "istanbul-lib-coverage": {
            version: ">= 3.2.1",
            description: "Data library for istanbul coverage objects",
            author: "Krishnan Anantheswaran",
            license: "BSD-3-Clause. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/istanbul-lib-coverage"]
        },
        "istanbul-lib-source-maps": {
            version: ">= 4.0.1",
            description: "Source maps support for istanbul",
            author: "Krishnan Anantheswaran",
            license: "BSD-3-Clause. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/istanbul-lib-source-maps"]
        },
        istextorbinary: {
            version: ">= 2.2.1",
            description: "Determines if a buffer is comprised of text or binary",
            author: "2012+ Bevry Pty Ltd",
            license: "MIT. See LICENSE.md",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/istextorbinary"]
        },
        "jira-client": {
            version: ">= 2.2.1",
            description: "Wrapper for the JIRA API",
            author: "Steven Surowiec",
            license: "MIT. See LICENSE.md",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/jira-client"]
        },
        keytar: {
            version: ">= 4.3.0",
            description: "Bindings to native Mac/Linux/Windows password APIs",
            author: "GitHub Inc.",
            license: "MIT. See LICENSE.md",
            format: "source code",
            location: [
                "[application-name][.exe]/node_modules/keytar",
                "resource/libs/keytar"
            ]
        },
        "node-notifier": {
            version: ">= 5.2.1",
            description: "A Node.js module for sending notifications",
            author: "Mikael Brevik",
            license: "MIT. See LICENSE",
            format: "source code",
            location: [
                "[application-name][.exe]/node_modules/node-notifier",
                "resource/libs/node-notifier"
            ]
        },
        "sass": {
            version: ">= 1.83.4",
            description: "A pure JavaScript implementation of Sass.",
            author: "Google Inc.",
            license: "MIT. See LICENSE",
            format: "source code",
            location: [
                "[application-name][.exe]/node_modules/sass"
            ]
        },
        "phonegap-build-api": {
            version: ">= 1.0.0",
            description: "REST Client for the PhoneGap Build API",
            author: "Adobe Systems",
            license: "Apache-2.0. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/phonegap-build-api"]
        },
        stylelint: {
            version: ">= 16.14.1",
            description: "A mighty CSS linter that helps you avoid errors and enforce conventions.",
            author: "stylelint",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/stylelint"]
        },
        "stylelint-checkstyle-formatter": {
            version: ">= 0.1.2",
            description: "Output Checkstyle XML reports of stylelint results",
            author: "David Clark",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/stylelint-checkstyle-formatter"]
        },
        "stylelint-config-standard-scss": {
            version: ">= 14.0.0",
            description: "The standard shareable SCSS config for Stylelint",
            author: "Stylelint SCSS",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/stylelint-config-standard-scss"]
        },
        "stylelint-config-sass-guidelines": {
            version: ">= 12.1.0",
            description: "Sharable stylelint config based on https://sass-guidelin.es/",
            author: "Brett Jankord",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/stylelint-config-sass-guidelines"]
        },
        "stylelint-config-clean-order": {
            version: ">= 7.0.0",
            description: "Order your styles with stylelint-order",
            author: "Kutsan Kaplan",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/stylelint-config-clean-order"]
        },
        "selenium-standalone": {
            version: ">= 6.15.3",
            description: "installs a `selenium-standalone` command line to install and start a standalone selenium server",
            author: "Vincent Voyer",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/selenium-standalone"]
        },
        "selenium-webdriver": {
            version: ">= 4.0.0-alpha.1",
            description: "The official WebDriver JavaScript bindings from the Selenium project",
            author: "Software Freedom Conservancy",
            license: "Apache-2.0. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/selenium-webdriver"]
        },
        eslint: {
            version: ">= 9.18.0",
            description: "An AST-based pattern checker for JavaScript.",
            author: "Nicholas C. Zakas <nicholas+npm@nczconsulting.com>",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/eslint"]
        },
        "@eslint/js": {
            version: ">= 9.18.0",
            description: "ESLint JavaScript language implementation",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/@eslint/js"]
        },
        "typescript-eslint": {
            version: ">= 8.21.0",
            description: "Tooling which enables you to use TypeScript with ESLint",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/typescript-eslint"]
        },
        "eslint-plugin-jsdoc": {
            version: ">= 50.6.3",
            description: "JSDoc linting rules for ESLint.",
            author: "Gajus Kuizinas",
            license: "BSD-3-Clause. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/eslint-plugin-jsdoc"]
        },
        "@stylistic/eslint-plugin": {
            version: ">= 3.0.1",
            description: "Stylistic rules for ESLint, works for both JavaScript and TypeScript.",
            author: "Anthony Fu <anthonyfu117@hotmail.com>",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/@stylistic/eslint-plugin"]
        },
        "eslint-formatter-checkstyle": {
            version: ">= 8.40.0",
            description: "ESLint’s official `checkstyle` formatter, unofficially published as a standalone module",
            author: "Nicholas C. Zakas <nicholas+npm@nczconsulting.com>",
            license: "MIT. See license",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/eslint-formatter-checkstyle"]
        },
        typedoc: {
            version: ">= 0.24.7",
            description: "Create api documentations for typescript projects",
            author: "Sebastian Lenz",
            license: "Apache-2.0. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/typedoc"]
        },
        typescript: {
            version: ">= 3.1.2",
            description: "TypeScript is a language for application scale JavaScript development",
            author: "Microsoft Corp.",
            license: "Apache-2.0. See LICENSE.txt",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/typescript"]
        },
        "virtualbox-soap": {
            version: ">= 1.0.0",
            description: "A wrapper for the SOAP API of Virtual Box",
            author: "ariatemplates",
            license: "Apache-2.0. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/virtualbox-soap"]
        },
        xml2js: {
            version: ">= 0.4.19",
            description: "Simple XML to JavaScript object converter",
            author: "Marek Kubica",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/xml2js"]
        },
        "uglify-js": {
            version: ">= 3.4.9",
            description: "JavaScript parser, mangler/compressor and beautifier toolkit",
            author: "Mihai Bazon",
            license: "BSD-2-Clause. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/uglify-js"]
        },
        dockerode: {
            version: ">= 2.5.7",
            description: "Docker Remote API module",
            author: "Pedro Dias",
            license: "Apache-2.0. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/dockerode"]
        },
        "json-schema-ref-parser": {
            version: ">= 6.1.0",
            description: "Parse, Resolve, and Dereference JSON Schema $ref pointers",
            author: "James Messinger",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/json-schema-ref-parser"]
        },
        showdown: {
            version: ">= 1.9.0",
            description: "A Markdown to HTML converter written in Javascript",
            author: "Estevão Santos",
            license: "BSD-3-Clause. See license.txt",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/showdown"]
        },
        pretty: {
            version: ">= 2.0.0",
            description: "Some tweaks for beautifying HTML with js-beautify according to my preferences",
            author: "Jon Schlinkert",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/pretty"]
        },
        "python-bridge": {
            version: ">= 1.1.0",
            description: "Node.js to Python bridge",
            author: "Ryan Munro",
            license: "MIT",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/python-bridge"]
        },
        "is-elevated": {
            version: ">= 3.0.0",
            description: "Check if the process is running with elevated privileges",
            author: "Sindre Sorhus",
            license: "MIT. See license",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/is-elevated"]
        },
        JSONStream: {
            version: ">= 1.3.5",
            description: "rawStream.pipe(JSONStream.parse()).pipe(streamOfObjects)",
            author: "Dominic Tarr",
            license: "MIT OR Apache-2.0. See LICENSE.MIT or LICENSE.APACHE2",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/JSONStream"]
        },
        yaml: {
            version: ">= 1.10.2",
            description: "JavaScript parser and stringifier for YAML",
            author: "Eemeli Aro",
            license: "ISC. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/yaml"]
        },
        rollup: {
            version: ">= 3.22.0",
            description: "Next-generation ES module bundler",
            author: "Rich Harris",
            license: "MIT. See LICENSE.md",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/rollup"]
        },
        "rollup-plugin-sourcemaps": {
            version: ">= 0.6.3",
            description: "Rollup plugin for grabbing source maps from sourceMappingURLs",
            author: "Max Davidson",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/rollup-plugin-sourcemaps"]
        },
        mime: {
            version: ">=2.6.0",
            description: "A comprehensive library for mime-type mapping",
            author: "Benjamin Thomas, Robert Kieffer",
            license: "MIT. See LICENSE",
            format: "source code",
            location: ["[application-name][.exe]/node_modules/mime"]
        },
        "com-wui-framework-selfextractor": {
            enabled: true,
            version: "2019.1.0",
            description: "SelfExtractor for WUI Framework's applications",
            author: "Oidis",
            license: "BSD-3-Clause. See LICENSE.txt",
            format: "executable binary",
            location: ["wui_modules"]
        },
        "com-wui-framework-launcher": {
            enabled: true,
            version: "2018.0.0",
            description: "Launcher for WUI Framework's applications",
            author: "Oidis",
            license: "BSD-3-Clause. See LICENSE.txt",
            format: "executable binary",
            location: ["wui_modules"]
        },
        "com-wui-framework-connector": {
            enabled: true,
            version: "2019.1.0",
            description: "Stand alone HTTP server for WUI Framework's applications with WebSockets support and built-in web host",
            author: "Oidis",
            license: "BSD-3-Clause. See LICENSE.txt",
            format: "executable binary and embedded resources",
            location: ["wui_modules"]
        },
        "com-wui-framework-chromiumre": {
            enabled: true,
            version: "2019.0.0",
            description: "Stand alone runtime environment for WUI Framework's applications",
            author: "Oidis",
            license: "BSD-3-Clause. See LICENSE.txt",
            format: "application",
            location: ["wui_modules"]
        },
        "com-wui-framework-idejre": {
            enabled: false
        },
        "com-wui-framework-idejre-eclipse": {
            version: "1.2.0",
            description: "Runtime environment for Eclipse IDE",
            author: "Oidis",
            license: "BSD-3-Clause. See LICENSE.txt",
            format: "jar file",
            location: ["wui_modules"]
        },
        "com-wui-framework-idejre-idea": {
            version: "1.2.0",
            description: "Runtime environment for JetBrains IDE",
            author: "Oidis",
            license: "BSD-3-Clause. See LICENSE.txt",
            format: "jar file",
            location: ["wui_modules"]
        },
        Zlib: {
            version: "1.2.8",
            description: "General purpose data compression library",
            author: "Jean-loup Gailly and Mark Adler",
            license: "BSD-3-Clause. See README",
            format: "source code",
            location: ["external_modules/zlib"]
        },
        "Resource Hacker": {
            enabled: false,
            version: "4.3.20",
            description: "Resource Hacker has been designed to be the complete resource editing tool",
            author: "Angus Johnson",
            license: "Freeware - no nags, no ads and fully functional",
            format: "application",
            location: ["external_modules/resourcehacker"]
        },
        CMake: {
            version: "3.14.5",
            description: "Cross Platform Makefile Generator",
            author: "Kitware",
            license: "BSD-3-Clause. See doc/cmake/Copyright.txt",
            format: "tool",
            location: ["external_modules/cmake"]
        },
        Cppcheck: {
            version: "1.89",
            description: "A static analysis tool for C/C++ code",
            author: "Cppcheck authors",
            license: "GNU GPLv3. See COPYING",
            format: "tool",
            location: ["external_modules/cppcheck"]
        },
        cpplint: {
            description: "C++ linting tool",
            author: "Google Inc.",
            license: "BSD-3-Clause",
            format: "source code",
            location: ["external_modules/cpplint"]
        },
        Doxygen: {
            version: "1.8.16",
            description: "Standard tool for generating documentation from annotated C++ sources, " +
                "but it also supports other popular programming languages",
            author: "Dimitri van Heesch",
            license: "GNU GPL",
            format: "tool",
            location: ["external_modules/doxygen"]
        },
        Git: {
            version: ">= 2.15.0",
            description: "Free and open source distributed version control system",
            author: "Linus Torvalds",
            license: "GNU GPLv2. See LICENSE.txt",
            format: "tool",
            location: ["external_modules/git"]
        },
        MSYS2: {
            enabled: false,
            description: "A software distro and building platform for Windows",
            author: "Alexey Pavlov",
            license: "BSD-3-Clause",
            format: "tool",
            location: ["external_modules/msys2"]
        },
        Python2: {
            version: "2.7.17",
            description: "Python is a programming language that lets you work quickly and integrate systems more effectively",
            author: "Python Software Foundation",
            license: "PSF License. See LICENSE.txt",
            format: "tool",
            location: ["external_modules/python"]
        },
        Python3: {
            version: "3.6.4",
            description: "Python is a programming language that lets you work quickly and integrate systems more effectively",
            author: "Python Software Foundation",
            license: "PSFv2 License. See LICENSE.txt",
            format: "tool",
            location: ["external_modules/python36"]
        },
        signtool: {
            enabled: false,
            description: "Command-line tool that digitally signs files, verifies signatures in files, or time stamps files",
            author: "Microsoft",
            license: "Microsoft EULA",
            format: "tool",
            location: ["external_modules/signtool"]
        },
        "Google Test": {
            description: "Google's C++ test framework",
            author: "Google Inc.",
            license: "BSD-3-Clause. See googletest/LICENSE",
            format: "source code",
            location: ["external_modules/gtest"]
        },
        UPX: {
            version: "3.94",
            description: "The Ultimate Packer for eXecutables",
            author: "Markus Oberhumer, Laszlo Molnar & John Reiser",
            license: "GNU GPL. See LICENSE",
            format: "tool",
            location: ["external_modules/upx"]
        },
        Maven: {
            version: "3.5.4",
            description: "Software project management and comprehension tool",
            author: "Maven authors",
            license: "Apache License, Version 2.0. See LICENSE",
            format: "tool",
            location: ["external_modules/maven"]
        },
        JDK: {
            version: "1.8.0_131",
            description: "Java Development Kit - tools for developing, debugging, and monitoring Java applications",
            author: "Oracle Corporation",
            license: "GNU GPLv2. See LICENSE",
            format: "tool",
            location: ["external_modules/jdk"]
        },
        "Eclipse Oxygen 1a": {
            version: "4.7.3",
            description: "An integrated development environment (IDE) used in computer programming",
            author: "Eclipse Foundation",
            license: "EPL",
            format: "IDE",
            location: ["external_modules/eclipse"]
        },
        "IntelliJ IDEA Community Edition": {
            version: "2019.1.3",
            description: "Java integrated development environment (IDE) for developing computer software",
            author: "Copyright (C) JetBrains s.r.o.",
            license: "Apache License, Version 2.0. See LICENSE.txt",
            format: "IDE",
            location: ["external_modules/idea"]
        },
        "Openssl": {
            version: "1.1.0f",
            description: "Open Source toolkit implementing the Transport Layer Security (TLS) protocols (including SSLv3) as well as a" +
                " full-strength general purpose cryptographic library",
            author: "The OpenSSL Project",
            license: "OpenSSL License. See LICENSE",
            format: "source code",
            location: ["external_modules/openssl"]
        },
        Autoconf: {
            version: "2.69",
            description: "GNU Autoconf is a tool for producing configure scripts",
            author: "David Mackenzie",
            license: "GNU GPL",
            format: "tool",
            location: ["external_modules/autoconf"]
        },
        GCC: {
            version: "7.4.0",
            description: "Compiler system produced by the GNU Project supporting various programming languages",
            author: "GNU Project",
            license: "GPLv3+",
            format: "tool",
            location: ["external_modules/gcc"]
        },
        ARM: {
            enabled: false,
            version: "7.4.0",
            description: "Cross-compiler system produced by the GNU Project supporting various programming languages",
            author: "Free Software Foundation, Inc.",
            license: "GPLv3+",
            format: "tool",
            location: ["external_modules/arm"]
        },
        Clang: {
            enabled: false,
            version: "4.2.1",
            description: "Clang is a compiler front end for the C, C++, Objective-C and Objective-C++ programming languages",
            author: "Chris Lattner",
            license: "UIUC Apache License 2.0 ",
            format: "tool",
            location: ["external_modules/clang"]
        },
        NASM: {
            enabled: false,
            version: "2.13.03",
            description: "The Netwide Assembler (NASM) is an assembler and disassembler for the Intel x86 architecture",
            author: "H. Peter Anvin, et al.",
            license: "BSD 2-clause",
            format: "tool",
            location: ["external_modules/nasm"]
        }
    }
});
