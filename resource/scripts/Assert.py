# /* ********************************************************************************************************* *
# *
# * Copyright 2019-2020 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* */
"""
Assert class provides useful assertion methods suitable for unit testing
"""
import traceback
from typing import Any, Callable
from unittest import TestCase

from Com.NXP.eIQ.Pycon.Utils.JSON import JSON


class Assert:
    __case = TestCase()

    @staticmethod
    def equal(actual: Any, expected: Any, message: str = None) -> None:
        message = "" if message is None else message + ": "
        error = None
        try:
            Assert.__case.assertEqual(actual, expected, message)
        # avoid unnecessary trace
        except Exception as e:
            error = e
        if error is not None:
            raise Exception("AssertionError: " + message + "\n" + JSON.Stringify(actual) + "\nexpected to be\n" +
                            JSON.Stringify(expected) + "\n")

    @staticmethod
    def notEqual(actual: Any, expected: Any, message: str = None) -> None:
        message = "" if message is None else message + ": "
        error = None
        try:
            Assert.__case.assertNotEqual(actual, expected, message)
        # avoid unnecessary trace
        except Exception as e:
            error = e
        if error is not None:
            raise Exception("AssertionError: " + message + "\n" + JSON.Stringify(actual) + "\nexpected NOT to be\n" +
                            JSON.Stringify(expected) + "\n")

    @staticmethod
    def throws(block: Callable, message: str = None) -> None:
        message = "" if message is None else message + ": "
        error = None
        try:
            Assert.__case.assertRaises(Exception, block, message)
        # avoid unnecessary trace
        except Exception as e:
            error = e
        if error is not None:
            raise Exception("AssertionError: Assertion expected to throw an error. " + message)

    @staticmethod
    def doesNotThrow(block: Callable, message: str = None) -> None:
        message = "" if message is None else message + ": "
        error = None
        try:
            block()
        # avoid unnecessary trace
        except:
            error = traceback.format_exc()
        if error is not None:
            raise Exception(error + "\nAssertionError: Assertion expected NOT to throw an error. " + message)
