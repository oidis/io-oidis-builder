# * ********************************************************************************************************* *
# *
# * Copyright 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(RESOLVE_SYMBOL symbol resolved)
    set(${resolved} ${symbol})
    STRING(REGEX REPLACE "^\\$" "" symbol_env ${symbol})
    if(DEFINED ENV{${symbol_env}})
        set(${resolved} "$ENV{${symbol_env}}")
    endif()
endmacro()

# set "-DCMAKE_VS_PLATFORM_TOOLSET=v120" or "=v140" in toolchainSettings.cmakeOptions[]
