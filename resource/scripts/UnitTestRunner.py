# /* ********************************************************************************************************* *
# *
# * Copyright 2019-2020 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* */
"""
UnitTestRunner class is designed as helper class for run and execution of Python unit tests
"""
import traceback
import time

from Com.NXP.eIQ.Pycon.Utils.ExecUtils import ExecUtils
from Com.NXP.eIQ.Pycon.Utils.LogIt import LogIt
from Com.NXP.eIQ.Pycon.Utils.StringUtils import StringUtils


class TestStatus:
    def __init__(self):
        self.errors: list = []
        self.skipped: int = 0
        self.testNum: int = 0


class UnitTestRunner:

    def __init__(self):
        self.__methodFilter: list = []

    def before(self) -> None:
        pass

    def after(self) -> None:
        pass

    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    def setMethodFilter(self, *methods: str):
        self.__methodFilter = []
        for method in methods:
            self.__methodFilter.append(StringUtils.ToLowerCase(method))

    def _resolve(self) -> TestStatus:
        ignoredPrefix: str = "_" + self.__class__.__name__ + "__Ignore"
        methods = [method for method in self.__dir__() if
                   hasattr(self.__class__, method) and callable(getattr(self.__class__, method))
                   and (StringUtils.StartsWith(method, ignoredPrefix) or StringUtils.StartsWith(method, "test"))]

        ExecUtils.Run(self.setUp)
        status: TestStatus = TestStatus()
        for method in methods:
            testName: str = StringUtils.Remove(method, ignoredPrefix, "test")
            testMessage: str = str(self.__class__.__module__) + " - " + testName

            if StringUtils.StartsWith(method, ignoredPrefix) \
                    or len(self.__methodFilter) != 0 \
                    and self.__methodFilter.count(StringUtils.ToLowerCase(testName)) == 0 \
                    and self.__methodFilter.count("test" + StringUtils.ToLowerCase(method)) == 0 \
                    and self.__methodFilter.count(StringUtils.ToLowerCase(method)) == 0:
                LogIt.Print("~ " + testMessage)
                status.skipped += 1
            else:
                isSuccess: bool
                before: float = time.time()
                ExecUtils.Run(self.before)
                try:
                    method = getattr(self, method)
                    ExecUtils.Run(method)
                    isSuccess = True
                except:
                    status.errors.append(traceback.format_exc())
                    isSuccess = False
                ExecUtils.Run(self.after)
                LogIt.Print(("+ " if isSuccess else "- ") + testMessage + " (" +
                            str(round((time.time() - before) * 1000)) + "ms)")
            status.testNum += 1
        ExecUtils.Run(self.tearDown)
        return status
