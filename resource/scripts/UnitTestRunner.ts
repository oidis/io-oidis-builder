/* ********************************************************************************************************* *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../test/unit/typescript/reference.d.ts" />
namespace Io.Oidis {
    "use strict";
    import BaseObject = Io.Oidis.Commons.Primitives.BaseObject;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import BaseHttpResolver = Io.Oidis.Commons.HttpProcessor.Resolvers.BaseHttpResolver;
    import ThreadPool = Io.Oidis.Commons.Events.ThreadPool;
    import GeneralEventOwner = Io.Oidis.Commons.Enums.Events.GeneralEventOwner;
    import EventType = Io.Oidis.Commons.Enums.Events.EventType;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import HttpResolver = Io.Oidis.Commons.HttpProcessor.HttpResolver;
    import EnvironmentArgs = Io.Oidis.Commons.EnvironmentArgs;
    import BrowserType = Io.Oidis.Commons.Enums.BrowserType;
    import ExceptionsManager = Io.Oidis.Commons.Exceptions.ExceptionsManager;
    import AsyncRequestEventArgs = Io.Oidis.Commons.Events.Args.AsyncRequestEventArgs;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import HttpRequestEventArgs = Io.Oidis.Commons.Events.Args.HttpRequestEventArgs;
    import BaseOutputHandler = Io.Oidis.Commons.IOApi.Handlers.BaseOutputHandler;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import LogLevel = Io.Oidis.Commons.Enums.LogLevel;
    import EventsManager = Io.Oidis.Commons.Events.EventsManager;
    import Counters = Io.Oidis.Commons.Utils.Counters;
    import HttpRequestParser = Io.Oidis.Commons.HttpProcessor.HttpRequestParser;
    import HttpManager = Io.Oidis.Commons.HttpProcessor.HttpManager;
    import Property = Io.Oidis.Commons.Utils.Property;
    import LogSeverity = Io.Oidis.Commons.Enums.LogSeverity;
    import Logger = Io.Oidis.Commons.LogProcessor.Logger;
    import ConsoleAdapter = Io.Oidis.Commons.LogProcessor.Adapters.ConsoleAdapter;
    import JsonUtils = Io.Oidis.Commons.Utils.JsonUtils;

    export abstract class UnitTestRunner extends BaseObject {
        private static windowCache : Window;
        private static loader : any;
        protected driver : any;
        protected by : any;
        protected vbox : IVBoxManager;
        private readonly unitRootPath : string;
        private readonly mockServerLocation : string;
        private methodFilter : string[];
        private readonly environment : any;
        private defaultTimeout : number = 10000; // ms
        private currentTimeout : number;
        private isFailing : boolean;
        private testName : string;
        private passingCount : number;
        private failingCount : number;
        private assertErrors : Error[];
        private withSolutionConfig : boolean;

        constructor() {
            super();

            if (isBrowserRun) {
                this.driver = chromeBuilder.build();
                this.driver.manage().window().maximize();
                this.by = selenium.By;
            }
            this.vbox = vbox;

            this.prepareAssert();

            this.methodFilter = [];
            this.withSolutionConfig = true;

            if (!ObjectValidator.IsSet((<any>EnvironmentArgs).prototype.Load)) {
                this.environment = new (<any>EnvironmentArgs)(
                    "io-oidis-builder", "2023.2.1", new Date().toTimeString(), false,
                    "Io.Oidis", true);
                if (ObjectValidator.IsSet((<any>Io.Oidis).Localhost)) {
                    this.environment.appName = "UnitTest";
                }
            } else {
                this.environment = {
                    name      : "io-oidis-builder",
                    version   : "2023.2.1",
                    build     : {
                        time     : new Date().toTimeString(),
                        timestamp: new Date().getTime(),
                        type     : "dev"
                    },
                    namespaces: "<? @var project.namespaces ?>".split(","),
                    target    : {
                        name   : "UnitTest",
                        metrics: {
                            enabled : false,
                            location: ""
                        }
                    },
                    domain    : {
                        location: "localhost"
                    }
                };
            }

            this.initLoader();
            this.currentTimeout = this.defaultTimeout;
            this.isFailing = false;
            this.passingCount = 0;
            this.failingCount = 0;
            this.assertErrors = [];
        }

        public Process($done : () => void) : void {
            this.resolver($done);
        }

        protected timeoutLimit($value? : number) : number {
            return this.currentTimeout = Property.Integer(this.currentTimeout, $value);
        }

        protected before() : void | IUnitTestRunnerPromise | Promise<void> {
            // override this method for ability to execute code BEFORE run of ALL tests in current UnitTest
            return;
        }

        protected setUp() : void | IUnitTestRunnerPromise | Promise<void> {
            // override this method for ability to execute code BEFORE EACH test in current UnitTest
            return;
        }

        protected tearDown() : void | IUnitTestRunnerPromise | Promise<void> {
            // override this method for ability to execute code AFTER EACH test in current UnitTest
            return;
        }

        protected after() : void | IUnitTestRunnerPromise | Promise<void> {
            // override this method for ability to execute code AFTER run of ALL tests in current UnitTest
            return;
        }

        protected setMethodFilter(...$names : string[]) : void {
            $names.forEach(($name : string) : void => {
                this.methodFilter.push(StringUtils.ToLowerCase($name));
            });
        }

        protected stripInstrumentation($input : string) : string {
            return ($input + "")
                .replace(/(cov_.*\+\+;)/gm, "")
                .replace(/\(cov_.*?\+\+, (.*?)(\))?\)/g, ($match : string, $value : string, $postfix : string) : string => {
                    return $value + (!ObjectValidator.IsEmptyOrNull($postfix) ? $postfix : "");
                });
        }

        protected resetCounters() : void {
            Counters.Clear();
            (<any>BaseOutputHandler).handlerIterator = 0;
        }

        protected registerElement($id : string, $tagName? : string) : void {
            if (!ObjectValidator.IsSet($tagName)) {
                $tagName = "div";
            }
            let element = document.createElement($tagName);
            element.id = $id;
            document.body.appendChild(element);
        }

        protected reload() : void {
            if (!ObjectValidator.IsEmptyOrNull(UnitTestRunner.loader.getInstance())) {
                this.getHttpManager().RefreshWithoutReload();
            }
        }

        protected setUrl($value : string) : void {
            window.location.href = $value;
            jsdom.reconfigure({url: $value});
            this.reload();
        }

        protected setUserAgent($browserTypeOrValue : BrowserType | string, $modern : boolean = true) : void {
            let value : string = UnitTestRunner.windowCache.navigator.userAgent;
            switch ($browserTypeOrValue) {
            case BrowserType.INTERNET_EXPLORER:
                if ($modern) {
                    value =
                        "Mozilla/4.0 (.NET CLR 3.5.30729; .NET CLR 3.0.30729; " +
                        "Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E; rv:11.0)";
                } else {
                    value =
                        "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; " +
                        "Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; " +
                        "Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E)";
                }
                break;
            case BrowserType.FIREFOX:
                value = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0";
                break;
            case BrowserType.GOOGLE_CHROME:
                value =
                    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                    "Chrome/52.0.2743.116 Safari/537.36";
                break;
            case BrowserType.OPERA:
                value =
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                    "Chrome/52.0.2743.116 Safari/537.36 OPR/39.0.2256.71";
                break;
            case BrowserType.SAFARI:
                value = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2";
                break;
            default:
                if (!ObjectValidator.IsEmptyOrNull($browserTypeOrValue)) {
                    value = <string>$browserTypeOrValue;
                }
                break;
            }
            (<any>window.navigator).__defineGetter__("userAgent", function () {
                return value;
            });
            this.reload();
        }

        protected setCookieItem($key : string, $value : string) : void {
            document.cookie = $key + "=" + $value + ";";
            this.reload();
        }

        protected setStorageItem($key : string, $value : string) : void {
            localStorage.setItem($key, $value);
            this.reload();
        }

        protected getAbsoluteRoot() : string {
            return this.unitRootPath;
        }

        protected getMockServerLocation() : string {
            return this.mockServerLocation;
        }

        protected enableSolutionConfig($value : boolean = true) : void {
            this.withSolutionConfig = $value;
        }

        protected initLoader($className? : any) : void {
            if (ObjectValidator.IsEmptyOrNull($className)) {
                if (ObjectValidator.IsEmptyOrNull(UnitTestRunner.loader)) {
                    let loaderClassName = "<? @var project.loaderClass ?>";
                    if (ObjectValidator.IsEmptyOrNull(loaderClassName)) {
                        loaderClassName = "Io.Oidis.Commons.Loader";
                    }
                    let classObject = window;
                    loaderClassName.split(".").forEach(($part : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull(classObject[$part])) {
                            classObject = classObject[$part];
                        }
                    });
                    UnitTestRunner.loader = classObject;
                }
            } else {
                UnitTestRunner.loader = $className;
            }
        }

        protected initSendBox() : void {
            this.setUrl(UnitTestRunner.windowCache.location.href);
            window.location.headers = UnitTestRunner.windowCache.location.headers;
            this.setUserAgent(BrowserType.FIREFOX);

            this.registerElement("Content");
            let env : any = JsonUtils.Clone(this.environment);
            if (this.withSolutionConfig) {
                env = JsonUtils.Extend(builder.getProjectConfig(), env);
            }
            UnitTestRunner.loader.Load(env);
            ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST);
            if ((<any>Io.Oidis).Gui) {
                (<any>Io.Oidis).Gui.Utils.StaticPageContentManager.Clear(true);
            }
            document.cookie = "";
            localStorage.clear();
            document.documentElement.innerHTML = "";
            this.registerElement("Content");
            Echo.Init("Content", true);

            (<any>ThreadPool).events = new EventsManager();

            const logger : Logger = new Logger();
            logger.setLevels(LogLevel.ALL, LogSeverity.LOW);
            logger.setModes(true, false);
            const adapter : ConsoleAdapter = new ConsoleAdapter(true);
            adapter.ColorsEnabled(false);
            logger.setAdapters([adapter]);
            LogIt.Init(logger);

            console.log = ($message : string) : void => {
                if (!ObjectValidator.IsEmptyOrNull($message)) {
                    console.info($message);
                }
                if (StringUtils.StartsWith($message, "ERROR:")) {
                    throw new Error(this.testName + "\n" + $message);
                }
            };
            if ((<any>Io.Oidis).Localhost) {
                process.env.NODE_PATH = process.cwd() + "/dependencies/nodejs/build/node_modules";
            }
        }

        protected integrityCheck() : string {
            let guiObjectManagerCRC : string = "";
            let bodyContentCRC : string = "";
            if (ObjectValidator.IsSet((<any>this.getHttpResolver()).getGuiRegister)) {
                guiObjectManagerCRC =
                    "    GuiObjectManager CRC: " + StringUtils.getCrc((<any>this.getHttpResolver()).getGuiRegister().ToString("", false)) + "\n";
            }
            if (!ObjectValidator.IsSet((<any>Io.Oidis).Localhost)) {
                bodyContentCRC =
                    "    Body content CRC: " + StringUtils.getCrc(document.documentElement.innerHTML) + "\n";
            }

            return "\n" +
                "    EventsManager CRC: " + StringUtils.getCrc(this.getEventsManager().ToString("", false)) + "\n" +
                "    ExceptionsManager CRC: " + StringUtils.getCrc(ExceptionsManager.getAll().ToString("", false)) + "\n" +
                "    HttpManager CRC: " + StringUtils.getCrc(this.getHttpManager().getResolversCollection().ToString("", false)) + "\n" +
                "    Environment CRC: " + StringUtils.getCrc(this.getEnvironmentArgs().ToString("", false)) + "\n" +
                bodyContentCRC +
                guiObjectManagerCRC;
        }

        protected resolver($done : () => void) : void {
            if (!ObjectValidator.IsSet(UnitTestRunner.windowCache)) {
                UnitTestRunner.windowCache = <Window>{
                    location : {
                        hash    : window.location.hash,
                        headers : window.location.headers,
                        host    : window.location.host,
                        hostname: window.location.hostname,
                        href    : window.location.href,
                        origin  : window.location.origin,
                        pathname: window.location.pathname,
                        port    : window.location.port,
                        protocol: window.location.protocol,
                        search  : window.location.search
                    },
                    navigator: {
                        cookieEnabled: window.navigator.cookieEnabled,
                        language     : window.navigator.language,
                        onLine       : window.navigator.onLine,
                        platform     : window.navigator.platform,
                        userAgent    : window.navigator.userAgent
                    }
                };
                this.initSendBox();
            }

            const tests : string[] = [];
            let startTime : number;
            let beforeTest : string;
            let integrityTestReport : string = "";

            const handlePromiseCall : ($promise : any) => Promise<void> = ($promise : any) : Promise<void> => {
                return new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) => {
                    try {
                        const promiseInterface : any = $promise.apply(this, []);
                        if (!ObjectValidator.IsEmptyOrNull(promiseInterface)) {
                            if (promiseInterface instanceof Promise || promiseInterface.constructor.name === "Promise") {
                                promiseInterface.then($resolve).catch($reject);
                            } else if (ObjectValidator.IsFunction(promiseInterface)) {
                                promiseInterface.apply(this, [$resolve]);
                            } else {
                                $reject(new Error("IUnitTestRunnerPromise or standard Promise interface is required " +
                                    "for asynchronous tests."));
                            }
                        } else {
                            $resolve();
                        }
                    } catch (ex) {
                        $reject(ex);
                    }
                });
            };

            const reportError : any = () : void => {
                this.assertErrors.forEach(($ex : Error) : void => {
                    __unitTestRunner.report.assert(this.testName, $ex);
                });
                this.assertErrors = [];
                __unitTestRunner.report.fail(this.testName, new Date().getTime() - startTime);
            };
            const getNextTest : any = async ($index : number = 0) : Promise<void> => {
                this.isFailing = false;
                this.assertErrors = [];
                if ($index < tests.length) {
                    try {
                        const method : string = tests[$index];
                        const testName : string = StringUtils.Remove(method, "test", "__Ignore");
                        startTime = new Date().getTime();
                        this.testName = this.getClassName() + " - " + testName;

                        if (this.methodFilter.length === 0 && StringUtils.StartsWith(method, "__Ignore")
                            || this.methodFilter.length !== 0 && (
                                this.methodFilter.indexOf(StringUtils.ToLowerCase(testName)) === -1 &&
                                this.methodFilter.indexOf(StringUtils.ToLowerCase("test" + testName)) === -1 &&
                                this.methodFilter.indexOf(StringUtils.ToLowerCase(method)) === -1)) {
                            __unitTestRunner.report.skip(this.testName);
                            await getNextTest($index + 1);
                        } else {
                            beforeTest = this.integrityCheck();
                            this.initSendBox();
                            await handlePromiseCall(this.setUp);
                            __unitTestRunner.events.onTestCaseStart(this.testName);

                            try {
                                let timeoutId : number = null;
                                await Promise.race([
                                    handlePromiseCall(this[method]),
                                    new Promise<void>(($resolve : () => void) : void => {
                                        __unitTestRunner.processExist = $resolve;
                                    }),
                                    new Promise<void>(($resolve : () => void) : void => {
                                        if (process.env.hasOwnProperty("OIDIS_NODEJS_DEBUGGER_ATTACHED")) {
                                            __unitTestRunner.console.log(ColorType.setColor(">>", ColorType.YELLOW) + " WARNING: " +
                                                "executing test with attached debugger sets timeout limit to -1 " +
                                                "- this can lead to test execution block");
                                        } else if (this.currentTimeout > -1) {
                                            timeoutId = <any>setTimeout(() : void => {
                                                this.isFailing = true;
                                                __unitTestRunner.report.assert(this.testName, new Error("Test case/suite has " +
                                                    "reached timeout (" + this.currentTimeout + " ms)"));
                                                $resolve();
                                            }, this.currentTimeout);
                                        } else {
                                            __unitTestRunner.console.log(ColorType.setColor(">>", ColorType.YELLOW) + " WARNING: " +
                                                "executing test with infinite timeout [see this.timeoutLimit(-1)] " +
                                                "- it can lead to test execution block");
                                        }
                                    })
                                ]);
                                if (!ObjectValidator.IsEmptyOrNull(timeoutId)) {
                                    clearTimeout(timeoutId);
                                }
                            } catch (ex) {
                                if (!this.isFailing) {
                                    this.isFailing = true;
                                    __unitTestRunner.report.error(this.testName, ex);
                                }
                            }

                            this.currentTimeout = this.defaultTimeout;
                            await handlePromiseCall(this.tearDown);
                            if (!this.isFailing) {
                                this.passingCount++;
                                __unitTestRunner.report.pass(this.testName, new Date().getTime() - startTime);
                            } else {
                                this.failingCount++;
                                reportError();
                            }

                            __unitTestRunner.events.onTestCaseEnd(this.testName);
                            const afterTest : string = this.integrityCheck();
                            if (beforeTest !== afterTest) {
                                integrityTestReport +=
                                    "\x1b[31m" +
                                    "WARNING: Integrity test failure at: " + this.testName + "\n" +
                                    "Expected: " + beforeTest + "\n" +
                                    "Actual: " + afterTest +
                                    "\x1b[0m";
                            }
                            await getNextTest($index + 1);
                        }
                    } catch (ex) {
                        if (!this.isFailing) {
                            this.isFailing = true;
                            __unitTestRunner.report.error(this.testName, ex);
                        }
                        await getNextTest($index + 1);
                    }
                } else if (this.assertErrors.length > 0) {
                    reportError();
                }
            };

            (async () : Promise<void> => {
                this.testName = "Suite " + this.getClassName();
                await handlePromiseCall(this.before);

                let index : number;
                const methods : string[] = this.getMethods();
                for (index = 0; index < methods.length; index++) {
                    if (StringUtils.Contains(StringUtils.ToLowerCase(methods[index]), "test")) {
                        tests.push(methods[index]);
                    }
                }
                __unitTestRunner.events.onSuiteStart();

                if (tests.length > 0) {
                    if (this.assertErrors.length > 0) {
                        reportError();
                    }
                    await getNextTest();
                    await handlePromiseCall(this.after);
                    if (!ObjectValidator.IsEmptyOrNull(integrityTestReport)) {
                        console.error(integrityTestReport);
                    }
                    __unitTestRunner.events.onSuiteEnd(this.passingCount, this.failingCount);
                } else {
                    console.log("Suite \"" + this.getClassName() + "\" skipped: no test methods found");
                }
            })().then(() : void => {
                $done();
            });
        }

        protected getEnvironmentArgs() : EnvironmentArgs {
            return UnitTestRunner.loader.getInstance().getEnvironmentArgs();
        }

        protected getHttpResolver() : HttpResolver {
            return UnitTestRunner.loader.getInstance().getHttpResolver();
        }

        protected getHttpManager() : HttpManager {
            return UnitTestRunner.loader.getInstance().getHttpManager();
        }

        protected getRequest() : HttpRequestParser {
            return UnitTestRunner.loader.getInstance().getHttpManager().getRequest();
        }

        protected getEventsManager() : EventsManager {
            return UnitTestRunner.loader.getInstance().getHttpResolver().getEvents();
        }

        private prepareAssert() : void {
            const resolveAssert : any = ($validator : () => void) : void => {
                try {
                    $validator();
                    this.isFailing = false;
                    this.assertErrors = [];
                    __unitTestRunner.events.onAssert(true);
                } catch (ex) {
                    this.isFailing = true;
                    this.assertErrors.push(ex);
                    __unitTestRunner.events.onAssert(false);
                    throw ex;
                }
            };

            assert.ok = ($value : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    __unitTestRunner.assert.ok($value, $message);
                });
            };

            assert.equal = ($actual : any, $expected : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull($message)) {
                        $message += ": ";
                    } else {
                        $message = "";
                    }
                    try {
                        __unitTestRunner.assert.equal($actual, $expected, $message);
                    } catch (ex) {
                        throw new Error(
                            "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be\n" +
                            JSON.stringify($expected) + "\n"
                        );
                    }
                });
            };

            assert.notEqual = ($actual : any, $expected : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull($message)) {
                        $message += ": ";
                    } else {
                        $message = "";
                    }
                    try {
                        __unitTestRunner.assert.notEqual($actual, $expected, $message);
                    } catch (ex) {
                        throw new Error(
                            "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be NOT\n" +
                            JSON.stringify($expected) + "\n"
                        );
                    }
                });
            };

            assert.deepEqual = ($actual : any, $expected : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull($message)) {
                        $message += ": ";
                    } else {
                        $message = "";
                    }
                    try {
                        __unitTestRunner.assert.deepEqual($actual, $expected, $message);
                    } catch (ex) {
                        throw new Error(
                            "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be\n" +
                            JSON.stringify($expected) + "\n"
                        );
                    }
                });
            };

            assert.notDeepEqual = ($actual : any, $expected : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull($message)) {
                        $message += ": ";
                    } else {
                        $message = "";
                    }
                    try {
                        __unitTestRunner.assert.notDeepEqual($actual, $expected, $message);
                    } catch (ex) {
                        throw new Error(
                            "AssertionError: " + $message + "\n" + JSON.stringify($actual) + "\nexpected to be NOT\n" +
                            JSON.stringify($expected) + "\n"
                        );
                    }
                });
            };

            assert.throws = ($block : any, $error? : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    let passed : boolean = true;
                    ExceptionsManager.Clear();
                    try {
                        $block();
                    } catch (ex) {
                        passed = false;
                        if (!ExceptionsManager.IsNativeException(ex) && StringUtils.Contains(ex.message, ".ExRethrow")) {
                            ex.message = ExceptionsManager.getLast().Message();
                            ExceptionsManager.Clear();
                        }
                        if (ObjectValidator.IsString($error)) {
                            $error = new RegExp($error);
                        }
                        if (ObjectValidator.IsSet($error) && $error.exec(ex.message) === null) {
                            if (!ObjectValidator.IsEmptyOrNull($message)) {
                                $message += ": ";
                            } else {
                                $message = "";
                            }
                            throw Error("AssertionError: " + $message + "\"" + ex.message + "\" " +
                                "expected to be in match with regEx: \"" + $error.toString() + "\"");
                        }
                    }
                    if (passed) {
                        throw Error("AssertFailed: Assertion expected throwing an error.");
                    }
                });
            };

            assert.doesHandleException = ($block : any, $error? : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    let passed : boolean = true;
                    let error : string = "";
                    const consoleError : any = console.error;
                    console.error = ($data : string) : void => {
                        if (StringUtils.Contains($data, "][ERROR] ")) {
                            error = ExceptionsManager.getLast().Message();
                            ExceptionsManager.Clear();
                        }
                    };
                    ExceptionsManager.Clear();
                    try {
                        $block();
                    } catch (ex) {
                        passed = false;
                    }
                    console.error = consoleError;
                    if (passed) {
                        if (ObjectValidator.IsString($error)) {
                            $error = new RegExp($error);
                        }
                        if (ObjectValidator.IsSet($error) && $error.exec(error) === null) {
                            if (!ObjectValidator.IsEmptyOrNull($message)) {
                                $message += ": ";
                            } else {
                                $message = "";
                            }
                            throw Error("AssertionError: " + $message + "\"" + error + "\" " +
                                "expected to be in match with regEx: \"" + $error.toString() + "\"");
                        }
                    } else {
                        throw Error("AssertionError: Assertion expected handling an error.");
                    }
                });
            };

            assert.onRedirect = ($setUp : () => void, $resolve : ($eventArgs : AsyncRequestEventArgs) => void, $done : () => void,
                                 $urlBase? : string) : void => {
                resolveAssert(() : void => {
                    console.log = ($message : string) : void => {
                        console.info($message);
                    };
                    this.getHttpResolver().CreateRequest($urlBase);
                    this.reload();
                    this.getEventsManager().Clear(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST);
                    this.getEventsManager().setEvent(GeneralEventOwner.WINDOW, EventType.ON_ASYNC_REQUEST,
                        ($eventArgs : AsyncRequestEventArgs) : void => {
                            try {
                                $resolve($eventArgs);
                            } catch (ex) {
                                throw ex;
                            }
                            $done();
                        });
                    $setUp();
                });
            };

            assert.doesNotThrow = ($block : any, $error? : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    ExceptionsManager.Clear();
                    try {
                        $block();
                    } catch (ex) {
                        if (!ExceptionsManager.IsNativeException(ex) && StringUtils.Contains(ex.message, ".ExRethrow")) {
                            ex.message = ExceptionsManager.getLast().Message();
                            ExceptionsManager.Clear();
                        }
                        if (ObjectValidator.IsString($error)) {
                            $error = new RegExp($error);
                        }
                        if (!ObjectValidator.IsSet($error) || ObjectValidator.IsSet($error) && $error.exec(ex.message) !== null) {
                            if (!ObjectValidator.IsEmptyOrNull($message)) {
                                $message += ": ";
                            } else {
                                $message = "";
                            }
                            $message = this.testName + "\n" +
                                "AssertionError: Assertion expected not throwing an error. " + $message + ex.message;
                            throw Error($message);
                        }
                    }
                });
            };

            __unitTestRunner.assert.patternEqual = ($actual : any, $expected : any, $message? : string) : void => {
                if (!ObjectValidator.IsEmptyOrNull($message)) {
                    $message += ": ";
                } else {
                    $message = "";
                }
                try {
                    __unitTestRunner.assert.ok(StringUtils.PatternMatched($expected, $actual));
                } catch (ex) {
                    throw new Error(
                        "AssertionError: " + $message + "\n" + JSON.stringify($actual, null, 2) + "\nexpected to be\n" +
                        JSON.stringify($expected, null, 2) + "\n"
                    );
                }
            };

            assert.patternEqual = ($actual : any, $expected : any, $message? : string) : void => {
                resolveAssert(() : void => {
                    __unitTestRunner.assert.patternEqual($actual, $expected, $message);
                });
            };

            assert.resolveEqual = ($className : any, $expected : string, $eventArgs? : HttpRequestEventArgs | AsyncRequestEventArgs,
                                   $message? : string) : BaseHttpResolver => {
                if (ObjectValidator.IsEmptyOrNull($eventArgs)) {
                    $eventArgs = new HttpRequestEventArgs(this.getRequest().getScriptPath());
                }
                let resolver : BaseHttpResolver = new $className();
                resolver.RequestArgs($eventArgs);
                resolveAssert(() : void => {
                    this.setUserAgent(BrowserType.FIREFOX);
                    Echo.ClearAll();
                    resolver.Process();
                    let actual : string = document.documentElement.innerHTML;
                    if (ObjectValidator.IsEmptyOrNull(actual) ||
                        actual === "<head></head><body></body>" ||
                        actual === "<head></head><body><div id=\"Content\"></div></body>") {
                        actual = Echo.getStream();
                    }

                    this.initSendBox();
                    if (StringUtils.Contains($expected, "*")) {
                        __unitTestRunner.assert.patternEqual(actual, $expected, $message);
                    } else {
                        __unitTestRunner.assert.equal(actual, $expected, $message);
                    }
                });
                return resolver;
            };

            assert.onGuiComplete = ($instance : any, $resolve : () => void, $done : () => void, $owner? : any) : void => {
                resolveAssert(() : void => {
                    if (ObjectValidator.IsSet($owner)) {
                        $instance.InstanceOwner($owner);
                    } else {
                        $instance.InstanceOwner({});
                    }
                    (<any>Io.Oidis).Gui.Utils.StaticPageContentManager.BodyAppend($instance.Draw());
                    (<any>Io.Oidis).Gui.Utils.StaticPageContentManager.Draw();

                    $instance.getEvents().setOnComplete(() : void => {
                        $resolve();
                        this.initSendBox();
                        $done();
                    });
                    $instance.Visible(true);
                });
            };

            let validatorName : string;
            for (validatorName in __unitTestRunner.assert) {
                if (__unitTestRunner.assert.hasOwnProperty(validatorName) && !assert.hasOwnProperty(validatorName)) {
                    assert[validatorName] = (...$args : any[]) : void => {
                        resolveAssert(() : void => {
                            __unitTestRunner.assert.apply(this, $args);
                        });
                    };
                }
            }
        }
    }

    export type IUnitTestRunnerPromise = ($done : () => void) => void;
}
