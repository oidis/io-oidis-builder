/* ********************************************************************************************************* *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <iostream>
#include <string>
#include <algorithm>
#include <future>

#ifdef WIN_PLATFORM
#include <io.h>
#include <windows.h>
#else
#include <dlfcn.h>
#include <unistd.h>
#include <regex>
#endif

#include "UnitTestRunner.hpp"

using std::string;

int main(int $argc, char **$argv) {
    string cwd = "<? @var testConfig.environment.cwd ?>";  // NOLINT(readability-redundant-string-init)
    string path = "<? @var testConfig.environment.path ?>";  // NOLINT(readability-redundant-string-init)

    string libraryPath = "<? @var properties.projectBase ?>/build/compiled/bin";
#ifdef WIN_PLATFORM
    libraryPath += "\\internal-unit-test-runner.dll";
#elif LINUX_PLATFORM
    libraryPath += "/libinternal-unit-test-runner.so";
#elif MAC_PLATFORM
    libraryPath += "/libinternal-unit-test-runner.dylib";
#else
#error Platform not implemented
#endif

    auto future = std::async(std::launch::async, [&]() -> int {
        // dummy async call here to pre-init task/async ecosystem,
        // possible SIGSEGV from std::async (launch::async only) in future usage without this on UNIX
        // issue is connected with shared library loaded from main context
        return 0;
    });
    (void)future.get();

    if (!cwd.empty()) {
#ifdef WIN_PLATFORM
        std::replace(cwd.begin(), cwd.end(), '/', '\\');
        SetCurrentDirectory(cwd.c_str());
#else
        chdir(cwd.c_str());
#endif
    }

    if (!path.empty()) {
#ifdef WIN_PLATFORM
        std::replace(path.begin(), path.end(), '/', '\\');

        auto *buff = new char[8192];
        DWORD size = ExpandEnvironmentStringsA(path.c_str(), buff, 8192);
        if (size > 8192) {
            delete[] buff;
            buff = new char[size];
            size = ExpandEnvironmentStringsA(path.c_str(), buff, size);
        }
        if (size != 0) {
            path = string(buff, size);
        }

        SetEnvironmentVariable("PATH", path.c_str());
#elif MAC_PLATFORM
        std::replace(path.begin(), path.end(), '\\', '/');
        std::replace(path.begin(), path.end(), ';', ':');

        auto resolve = [](const string &$key) -> string {
            string retVal = $key;
            char *pVar = getenv($key.substr(1).c_str());
            if (pVar != nullptr) {
                retVal = string(pVar);
            }

            return retVal;
        };

        std::smatch m{};
        string::const_iterator it = path.cbegin(), end = path.cend();
        while (std::regex_search(it, end, m, std::regex("\\$[A-Za-z0-9_]+"))) {
            auto tmp = resolve(m.str());
            if (!tmp.empty()) {
                size_t start = path.find(m.str());
                path = path.substr(0, start) + tmp + path.substr(start + m.str().length());
                it = path.cbegin();
                end = path.cend();
            }
            std::advance(it, m.position() + (tmp.empty() ? m.length() : tmp.length()));
        }

        setenv("DYLD_FALLBACK_LIBRARY_PATH", path.c_str(), 1);
        system(("install_name_tool -add_rpath \"" + path + "\" " + libraryPath + " 2> /dev/null").c_str());
#else
        std::replace(path.begin(), path.end(), '\\', '/');
        std::replace(path.begin(), path.end(), ';', ':');

        auto resolve = [](const string &$key) -> string {
            string retVal = $key;
            char *pVar = getenv($key.substr(1).c_str());
            if (pVar != nullptr) {
                retVal = string(pVar);
            }

            return retVal;
        };

        std::smatch m{};
        string::const_iterator it = path.cbegin(), end = path.cend();
        while (std::regex_search(it, end, m, std::regex("\\$[A-Za-z0-9_]+"))) {
            auto tmp = resolve(m.str());
            if (!tmp.empty()) {
                size_t start = path.find(m.str());
                path = path.substr(0, start) + tmp + path.substr(start + m.str().length());
                it = path.cbegin();
                end = path.cend();
            }
            std::advance(it, m.position() + (tmp.empty() ? m.length() : tmp.length()));
        }

        setenv("PATH", path.c_str(), 1);
        system(("patchelf --set-rpath '" + path + "' " + libraryPath).c_str());
        std::cout << "Patch applied on " << libraryPath << std::endl;
#endif
    }

    int retVal;
    typedef void *(*CreateInstance)();
    CreateInstance createInstance;
    Internal::IUnitTestRunner *runner = nullptr;

#ifdef WIN_PLATFORM
    std::replace(libraryPath.begin(), libraryPath.end(), '/', '\\');
    HMODULE libraryHandle = LoadLibrary(libraryPath.c_str());
    if (libraryHandle != nullptr) {
        createInstance = (CreateInstance)GetProcAddress(libraryHandle, "CreateInstance");
        if (createInstance != nullptr) {
            runner = static_cast<Internal::IUnitTestRunner *>(createInstance());
            retVal = runner->RunUnitTests($argc, $argv);
        } else {
            std::cerr << "A \"CreateInstance\" function can not be located in loaded library." << std::endl;
            retVal = static_cast<int>(GetLastError());
        }
        FreeLibrary(libraryHandle);
    } else {
        std::cerr << "Can not load library from path: " + libraryPath + " or one of its dependencies." << std::endl;
        retVal = ERROR_DLL_NOT_FOUND;
    }
#else
    void *libraryHandle = dlopen(libraryPath.c_str(), RTLD_LAZY | RTLD_LOCAL);
    if (libraryHandle != nullptr) {
        createInstance = (CreateInstance)dlsym(libraryHandle, "CreateInstance");
        if (createInstance != nullptr) {
            runner = static_cast<Internal::IUnitTestRunner *>(createInstance());
            retVal = runner->RunUnitTests($argc, $argv);
        } else {
            std::cerr << "A \"CreateInstance\" function can not be located in loaded library." << std::endl;
            std::cerr << dlerror() << std::endl;
            retVal = 1;
        }
        dlclose(libraryHandle);
    } else {
        std::cerr << "Can not load library from path: " + libraryPath + " or one of its dependencies." << std::endl;
        std::cerr << dlerror() << std::endl;
        retVal = 1;
    }
#endif

    return retVal;
}
