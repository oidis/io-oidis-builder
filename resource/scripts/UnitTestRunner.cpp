/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "UnitTestRunner.hpp"
#include "../../../test/unit/cpp/reference.hpp"

namespace Internal {
    class UnitTestRunner : public IUnitTestRunner {
     public:
        int RunUnitTests(int argc, char **argv) override {
    #ifdef IO_OIDIS_XCPPCOMMONS_SOURCEFILESMAP_HPP_
            Io::Oidis::XCppCommons::EnvironmentArgs::getInstance().Load();
            Io::Oidis::XCppCommons::Utils::LogIt::Init(Io::Oidis::XCppCommons::Enums::LogLevel::ERROR,
                                                             Io::Oidis::XCppCommons::IOApi::IOHandlerFactory::getHandler(
                                                                     Io::Oidis::XCppCommons::Enums::IOHandlerType::OUTPUT_FILE));
    #endif

            testing::InitGoogleTest(&argc, argv);
            return RUN_ALL_TESTS();
        }
    };
}

// CreateInstance should be published as extern "C" function to produce undecorated entry
extern "C" {
API_EXPORT Internal::IUnitTestRunner *API_CALL CreateInstance() {
    return new Internal::UnitTestRunner();
}
}
