# /* ********************************************************************************************************* *
# *
# * Copyright 2019-2020 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* */
"""
UnitTestLoader class is designed for load and report of Python unit tests
"""
import argparse
import glob
import os
import sys
import importlib.util



class UnitTestLoader:
    @staticmethod
    def resolve():
        cwd = os.getcwd()
        sourceFolders = [os.path.normpath(cwd + "/test/unit/python/"), os.path.normpath(cwd + "/source/python/")]
        sourceFolders.extend(glob.glob(cwd + "/dependencies/**/source/python/", recursive=True))
        sys.path.extend(sourceFolders)

        from UnitTestRunner import TestStatus
        from Com.NXP.eIQ.Pycon.Utils.LogIt import LogIt
        from Com.NXP.eIQ.Pycon.Utils.ObjectValidator import ObjectValidator
        from Com.NXP.eIQ.Pycon.Utils.StringUtils import StringUtils
        from Com.NXP.eIQ.Pycon.Enums.LoggerLevel import LoggerLevel
        from Com.NXP.eIQ.Pycon.Connectors.FileSystemHandler import FileSystemHandler
        from Com.NXP.eIQ.Pycon.EnvironmentArgs import EnvironmentArgs

        LogIt.setLevel(LoggerLevel.WARNING)

        parser = argparse.ArgumentParser()
        parser.add_argument("--filter", dest="filter", type=str or None, required=False)
        args = parser.parse_args()

        fileSystem = FileSystemHandler(EnvironmentArgs.getProject())

        if ObjectValidator.IsEmptyOrNull(args.filter) or args.filter == "*":
            files = [path for path in glob.glob(cwd + "/test/unit/python/**/*.py", recursive=True)
                     if StringUtils.EndsWith(os.path.basename(path), "Test.py")]
        else:
            files = [path for path in glob.glob(args.filter, recursive=True)
                     if StringUtils.EndsWith(os.path.basename(path), "Test.py")]

        suiteStatus: TestStatus = TestStatus()
        for file in files:
            normalizedFile = fileSystem.NormalizePath(file)
            className = StringUtils.Replace(os.path.basename(normalizedFile), ".py", "")
            spec = importlib.util.spec_from_file_location(className, normalizedFile)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            cls = getattr(module, className)
            instance = cls()
            absPath = StringUtils.Replace(normalizedFile, fileSystem.NormalizePath(cwd + "/test/unit/python/"),
                                          "")[1:-3]
            instance.__class__.__module__ = StringUtils.Replace(absPath, os.path.sep, ".")
            status: TestStatus = instance._resolve()
            suiteStatus.errors.extend(status.errors)
            suiteStatus.skipped += status.skipped
            suiteStatus.testNum += status.testNum

        LogIt.Print("\n")
        if ObjectValidator.IsEmptyOrNull(suiteStatus.errors):
            LogIt.Print("SUCCESS")
        else:
            for error in suiteStatus.errors:
                LogIt.Error(error)
            LogIt.Print("FAILURES!")

        LogIt.Print("Skipped tests: " + str(suiteStatus.skipped))
        LogIt.Print("Tests: " + str(suiteStatus.testNum) + ", Failures: " + str(len(suiteStatus.errors)))


if __name__ == '__main__':
    UnitTestLoader.resolve()
