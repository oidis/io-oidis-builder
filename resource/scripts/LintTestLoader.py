# /* ********************************************************************************************************* *
# *
# * Copyright 2019-2020 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* */
"""
LintTestLoader file is designed for load of Python lint test
"""

if __name__ == '__main__':
    import argparse
    import glob
    import os
    import sys
    from pylint import lint

    __cwd = os.getcwd()
    __dependencies = __cwd + "/dependencies/**/source/python/"
    __sourceFolders = [os.path.normpath(__cwd + "/test/unit/python/"), os.path.normpath(__cwd + "/source/python/")]
    __lintFolders = __sourceFolders.copy()
    __sourceFolders.extend(glob.glob(__dependencies, recursive=True))
    sys.path.extend(__sourceFolders)

    if os.environ["WITH_DEPENDENCIES"] == "1":
        __lintFolders.extend(glob.glob(__dependencies, recursive=True))

    parser = argparse.ArgumentParser()
    parser.add_argument("--rcFile", dest="rcFile", type=str or None, required=False)
    args = parser.parse_args()
    lintArgs: list = ["--rcfile=" + os.path.normpath(args.rcFile)]
    for sourceFolder in __lintFolders:
        lintArgs.extend(glob.glob(sourceFolder + "/**/*.py", recursive=True))
    lint.Run(lintArgs)
