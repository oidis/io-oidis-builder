/* ********************************************************************************************************* *
 *
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Io.Oidis;

import junit.framework.AssertionFailedError;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class Assert {
    public static void equal(Object actual, Object expected) {
        assertEquals(expected, actual);
    }

    public static void equal(Object actual, Object expected, String message) {
        assertEquals(message, expected, actual);
    }

    public static void notEqual(Object actual, Object expected) {
        assertNotEquals(expected, actual);
    }

    public static void notEqual(Object actual, Object expected, String message) {
        assertNotEquals(message, expected, actual);
    }

    public static void throwsException(Runnable block) {
        throwsException(block, null);
    }

    public static void throwsException(Runnable block, String message) {
        try {
            block.run();
            String msg = message == null ? "Exception must be thrown." : message;
            throw new AssertionFailedError(msg);
        } catch (Exception e) {

        }
    }

    public static void doesNotThrowException(Runnable block) {
        doesNotThrowException(block, null);
    }

    public static void doesNotThrowException(Runnable block, String message) {
        try {
            block.run();
        } catch (Exception e) {
            String msg = message == null ? "Exception must not be thrown." : message;
            throw new AssertionFailedError(msg);
        }
    }
}
