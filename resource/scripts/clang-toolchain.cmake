# * ********************************************************************************************************* *
# *
# * Copyright 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(RESOLVE_SYMBOL symbol resolved)
    set(${resolved} ${symbol})
    STRING(REGEX REPLACE "^\\$" "" symbol_env ${symbol})
    if(DEFINED ENV{${symbol_env}})
        set(${resolved} "$ENV{${symbol_env}}")
    endif()
endmacro()

RESOLVE_SYMBOL("<? @var project.target.toolchainSettings.CC ?>" CMAKE_C_COMPILER)
RESOLVE_SYMBOL("<? @var project.target.toolchainSettings.CXX ?>" CMAKE_CXX_COMPILER)
