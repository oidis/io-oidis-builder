import setuptools

## with open("README.md", "r") as fh:
##    long_description = fh.read()


setuptools.setup(
    name="<? @var project.name ?>",
    version="<? @var project.version ?>",
    author="<? @var project.author.name ?>",
    author_email="<? @var project.author.email ?>",
    description="<? @var project.description ?>",
    ## long_description=long_description,
    ## long_description_content_type="text/markdown",
    url="<? @var project.homepage ?>",
    packages=setuptools.find_packages(),
    ## todo: parse config
    license="BSD-3-Clause",
    install_requires=["<? @var properties.pythonDependencies ?>"],
    package_data={"": ["<? @var project.name ?>_data/**"]},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent"
    ],
    python_requires=">=3.6"
)
