/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { BaseAdapter } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Adapters/BaseAdapter.js";
import { ILoggerTrace } from "@io-oidis-commons/Io/Oidis/Commons/LogProcessor/Logger.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { StdAdapter } from "@io-oidis-localhost/Io/Oidis/Localhost/LogProcessor/Adapters/StdAdapter.js";

export class ProcessAdapter extends BaseAdapter {
    private persistence : IPersistenceHandler;
    private reportExcludeList : string[];
    private std : StdAdapter;

    constructor() {
        super();
        this.persistence = PersistenceFactory.getPersistence(this.getClassName());
        this.reportExcludeList = [
            "run-unit-test",
            "run-coverage-test",
            "test:xcpplint",
            "project-cleanup",
            "test:lint",
            "test-lint-stagged-only",
            "Cannot start Mock server",
            "Connection has been lost",
            "Uncommitted changes has been found",
            "are lint free"
        ];
        this.std = new StdAdapter();
    }

    protected formatter($trace : ILoggerTrace) : ILoggerTrace {
        return $trace;
    }

    protected getOwner() : any {
        return super.getOwner();
    }

    protected print($trace : ILoggerTrace) : void {
        if ($trace.level === LogLevel.ERROR) {
            const loader : any = this.getOwner();
            loader.errorDetected = true;
            if (this.persistence.Variable("lastError") !== $trace.message) {
                let reportEnabled : boolean = true;
                this.reportExcludeList.forEach(($exclude : string) : void => {
                    if (loader.getProgramArgs().getTasks().indexOf($exclude) !== -1 ||
                        StringUtils.ContainsIgnoreCase($trace.message, $exclude)) {
                        reportEnabled = false;
                    }
                });
                if (reportEnabled) {
                    if (!loader.getAppConfiguration().noReport) {
                        this.persistence.Variable("lastError", $trace.message);
                        loader.reportMessage = $trace.message;
                    } else {
                        this.stderrWrite("Crash report skipped: reports are disabled by noReport option");
                    }
                } else {
                    this.stderrWrite("Crash report skipped: error has been found at exclude list");
                }
            } else {
                this.stderrWrite("Crash report skipped: report has been already send");
            }
            loader.Exit(-1);
        }
    }

    private stderrWrite($message : string) : void {
        this.std.Process({
            entryPoint: null,
            level     : LogLevel.ERROR,
            message   : $message,
            severity  : LogSeverity.HIGH,
            time      : new Date().getTime(),
            trace     : null
        });
    }
}
