/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileSystemHandler } from "./Connectors/FileSystemHandler.js";
import { IProject, IProjectModule } from "./Interfaces/IProject.js";
import { IProperties } from "./Interfaces/IProperties.js";
import { ScriptHandler } from "./IOApi/Hadlers/ScriptHandler.js";
import { Loader } from "./Loader.js";

export class ModulesResolver extends BaseObject {
    private readonly modules : ArrayList<IProjectModule>;
    private readonly events : any;
    private owner : string;
    private project : IProject;
    private properties : IProperties;
    private fileSystem : FileSystemHandler;

    public static getInstance() : ModulesResolver {
        const singleton : ModulesResolver = new (<any>this)();
        this.getInstance = () : ModulesResolver => {
            return singleton;
        };
        return singleton;
    }

    constructor() {
        super();
        this.modules = new ArrayList<IProjectModule>();
        this.events = {};
        this.owner = this.getClassName();
        const instance : Loader = Loader.getInstance();
        this.fileSystem = instance.getFileSystemHandler();
        this.project = instance.getProjectConfig();
        this.properties = instance.getAppProperties();

        const normalizeModules : any = ($register : any, $cwd : string) : void => {
            let moduleName : string;
            for (moduleName in $register) {
                if ($register.hasOwnProperty(moduleName)) {
                    const module : IProjectModule = JsonUtils.Extend({
                        args        : [],
                        events      : {
                            after : [],
                            before: []
                        },
                        name        : moduleName,
                        overrideTask: false
                    }, $register[moduleName]);
                    if (!StringUtils.StartsWith(module.script, "/") &&
                        !StringUtils.StartsWith(module.script, "~/") &&
                        !StringUtils.Contains(module.script, ":/")) {
                        module.script = this.fileSystem.NormalizePath($cwd + "/" + module.script);
                    }
                    if (ObjectValidator.IsString(module.events.before)) {
                        module.events.before = [<string>module.events.before];
                    }
                    if (ObjectValidator.IsString(module.events.after)) {
                        module.events.after = [<string>module.events.after];
                    }
                    $register[moduleName] = module;
                }
            }
            return $register;
        };
        const loadModules : any = ($cwd : string, $path : string) : void => {
            $path = $cwd + "/" + $path;
            if (this.fileSystem.Exists($path)) {
                try {
                    JsonUtils.Extend(this.project.modules, normalizeModules(JSON.parse(this.fileSystem.Read($path).toString()), $cwd));
                } catch (ex) {
                    LogIt.Warning(ex.stack);
                }
            }
        };
        const registerEvents : any = ($events : string[], $type : string, $module : string) : void => {
            $events.forEach(($owner : string) : void => {
                if (ObjectValidator.IsEmptyOrNull(this.events[$owner])) {
                    this.events[$owner] = {};
                }
                if (ObjectValidator.IsEmptyOrNull(this.events[$owner].before)) {
                    this.events[$owner].before = [];
                }
                if (ObjectValidator.IsEmptyOrNull(this.events[$owner].after)) {
                    this.events[$owner].after = [];
                }
                this.events[$owner][$type].push($module);
            });
        };

        normalizeModules(this.project.modules, this.properties.projectBase);
        JsonUtils.Extend(this.project.modules, normalizeModules(instance.getAppConfiguration().modules, this.properties.binBase));
        loadModules(this.properties.binBase, "resource/configs/modules.register.json");
        loadModules(this.properties.projectBase, "resource/configs/modules.register.json");

        let moduleName : string;
        for (moduleName in this.project.modules) {
            if (this.project.modules.hasOwnProperty(moduleName)) {
                const module : IProjectModule = this.project.modules[moduleName];
                this.modules.Add(module, moduleName);
                registerEvents(<string[]>module.events.before, "before", moduleName);
                registerEvents(<string[]>module.events.after, "after", moduleName);
            }
        }
    }

    public setOwner($value : string) : void {
        this.owner = Property.String(this.owner, $value);
    }

    public Resolve($module : string, $args : any = null, $callback : ($status : boolean, $returnValue? : any) => void) : void {
        const handler : ScriptHandler = new ScriptHandler();
        const module : IProjectModule = this.modules.getItem($module);
        handler.Path(module.script);
        handler.Environment({
            __owner: this.owner
        });
        handler.Interpreter(module.interpreter);
        handler.SuccessHandler((...$args : any[]) : void => {
            this.owner = this.getClassName();
            const returnVal : any = !ObjectValidator.IsEmptyOrNull($args) && $args.length === 1 ? $args[0] : $args;
            if (!ObjectValidator.IsEmptyOrNull(returnVal)) {
                LogIt.Debug("Return value: " + JSON.stringify($args));
            }
            $callback(true, returnVal);
        });
        handler.ErrorHandler(($error : Error) : void => {
            // TODO(mkelnar) temporary issue bypass until [ATT-332] will be fixed
            if (!($error instanceof Error)) {
                const error : Error = new Error((<Error>$error).message);
                error.stack = (<Error>$error).stack;
                $error = error;
            }
            LogIt.Error("Module execution has failed.", $error);
            $callback(false, "Module execution has failed. " + $error.message);
        });
        handler.Process(this.properties.projectBase, ObjectValidator.IsEmptyOrNull($args) ? this.modules.getItem($module).args : $args);
    }

    public async ResolveAsync($module : string, $args : any = null) : Promise<IModuleResolveResult> {
        return new Promise<IModuleResolveResult>(($resolve) => {
            this.Resolve($module, $args, ($status : boolean, $returnValue? : any) : void => {
                $resolve({status: $status, returnValue: $returnValue});
            });
        });
    }

    public getAll() : string[] {
        const modules : string[] = [];
        this.modules.foreach(($task : IProjectModule, $name : string) : void => {
            modules.push($name);
        });
        return modules;
    }

    public getTaskEvents($task : string) : any {
        let before : string[] = [];
        let after : string[] = [];
        if (this.events.hasOwnProperty($task)) {
            before = before.concat(this.events[$task].before);
            after = after.concat(this.events[$task].after);
        }
        if (StringUtils.Contains($task, ":")) {
            const task : string = StringUtils.Substring($task, 0, StringUtils.IndexOf($task, ":"));
            if (this.events.hasOwnProperty(task)) {
                before = before.concat(this.events[task].before);
                after = after.concat(this.events[task].after);
            }
        }
        return {
            after,
            before,
            owner: $task
        };
    }
}

export interface IModuleResolveResult {
    status : boolean;
    returnValue? : any;
}

// generated-code-start
export const IModuleResolveResult = globalThis.RegisterInterface(["status", "returnValue"]);
// generated-code-end
