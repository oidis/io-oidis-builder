/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { StaticPageContentManager } from "@io-oidis-gui/Io/Oidis/Gui/Utils/StaticPageContentManager.js";
import { BaseHttpResolver } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/Resolvers/BaseHttpResolver.js";
import { Snippets } from "./RuntimeTests/Snippets.js";

export class Index extends BaseHttpResolver {
    protected resolver() : void {
        const EOL : string = StringUtils.NewLine(false);
        const wrapper : any = ($content : string) : string => {
            return "" +
                "<div class=\"GuiInterface\">" + EOL +
                "   <h1>Oidis Builder " + this.getEnvironmentArgs().getProjectVersion() + "</h1>" + EOL +
                "   <h3>Build Tool for Oidis Framework.</h3>" + EOL +
                "   <div class=\"Index\">" + EOL +
                $content + EOL +
                "   </div>" + EOL +
                "</div>" + EOL +
                "<div class=\"Note\">" + EOL +
                "version: " + this.getEnvironmentArgs().getProjectVersion() +
                ", build: " + this.getEnvironmentArgs().getBuildTime() +
                "</div>" + EOL +
                "<div class=\"Logo\">" + EOL +
                "   <div class=\"WUI\"></div>" + EOL +
                "</div>";
        };

        StaticPageContentManager.Clear();
        StaticPageContentManager.Title("Oidis Framework Builder");
        if (!this.getEnvironmentArgs().HtmlOutputAllowed()) {
            StaticPageContentManager.License(
                "<!--" + EOL +
                EOL +
                "Copyright 2014-2016 Freescale Semiconductor, Inc." + EOL +
                "Copyright 2017-2019 NXP" + EOL +
                "Copyright " + StringUtils.YearFrom(2019) + " Oidis" + EOL +
                EOL +
                "SPDX-License-Identifier: BSD-3-Clause" + EOL +
                "The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution" + EOL +
                "or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText" + EOL +
                EOL +
                "-->"
            );
            let packageName : string = this.getEnvironmentArgs().getProjectName() + "-" + this.getEnvironmentArgs().getProjectVersion();
            packageName = StringUtils.Replace(packageName, ".", "-");
            StaticPageContentManager.HeadScriptAppend("resource/javascript/" + packageName + ".min.js");
            StaticPageContentManager.HeadScriptAppend("resource/javascript/loader.min.js");

            StaticPageContentManager.BodyAppend(
                "<noscript>" + EOL +
                wrapper(
                    "This library requires enabled JavaScript in the browser. See link below for more information:" +
                    StringUtils.NewLine() +
                    "<a href=\"http://www.enable-javascript.com/\" target=\"_blank\">How to enable JavaScript?</a>"
                ) + EOL +
                "</noscript>");
        } else {
            let content : string = "";
            /* dev:start */
            content +=
                "<H3>Pages</H3>" +

                "<H3>Runtime tests</H3>" +
                "<a href=\"" + Snippets.CallbackLink() + "\">Snippets</a>" + StringUtils.NewLine() +
                StringUtils.NewLine();
            /* dev:end */
            StaticPageContentManager.BodyAppend(wrapper(content));
        }
        StaticPageContentManager.Draw();
    }
}
