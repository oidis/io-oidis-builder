/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { VMHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/IOApi/Handlers/VMHandler.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { FileSystemHandler } from "../../Connectors/FileSystemHandler.js";
import { BaseDAO } from "../../DAO/BaseDAO.js";
import { IProperties } from "../../Interfaces/IProperties.js";
import { Loader } from "../../Loader.js";

export class ScriptHandler extends BaseObject {
    private path : string;
    private env : any;
    private properties : IProperties;
    private fileSystem : FileSystemHandler;
    private mainMethod : string;
    private processHandler : any;
    private successHandler : any;
    private errorHandler : any;
    private initialized : boolean;
    private loaded : boolean;
    private interpreter : string;
    private loaderInstance : Loader;

    constructor() {
        super();
        this.path = "";
        this.env = {};
        this.interpreter = null;
        this.loaderInstance = Loader.getInstance();
        this.properties = this.loaderInstance.getAppProperties();
        this.fileSystem = this.loaderInstance.getFileSystemHandler();
        this.mainMethod = "Process";
        this.processHandler = (...$args : any[]) : void => {
            this.successHandler();
        };
        this.successHandler = () : void => {
            // default success handler
        };
        this.errorHandler = ($error : Error) : void => {
            LogIt.Error($error.message, $error);
        };
        this.initialized = false;
        this.loaded = false;
    }

    public Path($value? : string) : string {
        return this.path = Property.String(this.path, $value);
    }

    public Environment($value? : any) : any {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.env = $value;
        }
        return this.env;
    }

    public Interpreter($value? : any) : any {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.interpreter = $value;
        }
        return this.interpreter;
    }

    public MainMethodName($value? : string) : string {
        return this.mainMethod = Property.String(this.mainMethod, $value);
    }

    public SuccessHandler($handler : ($returnValue? : any) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull($handler)) {
            this.successHandler = (...$returnVal : any[]) : void => {
                $handler(...$returnVal);
            };
        }
    }

    public ErrorHandler($handler : ($error : Error) => void) : void {
        if (!ObjectValidator.IsEmptyOrNull($handler)) {
            this.errorHandler = $handler;
        }
    }

    public Process(...$args : any[]) : void {
        const processTick : any = () : void => {
            if (this.loaded) {
                this.processHandler.apply(this, $args.concat(this.successHandler)); // eslint-disable-line prefer-spread
            } else {
                setTimeout(processTick, 100); // eslint-disable-line @typescript-eslint/no-implied-eval
            }
        };
        if (!this.initialized) {
            this.Load();
            processTick();
        } else {
            processTick();
        }
    }

    public Load() : void {
        this.initialized = true;
        this.loaded = false;
        const script : string = this.fileSystem.NormalizePath(this.path);
        const extension : string = require("path").extname(script);

        if (extension !== ".py" && !ObjectValidator.IsEmptyOrNull(this.Interpreter())) {
            LogIt.Warning("Setting interpreter isn't supported for " + extension + " files.");
        }

        switch (extension) {
        case ".py":
            // eslint-disable-next-line no-case-declarations
            const isVenv : boolean = this.loaderInstance.getEnvironmentArgs().getAppProperties().projectBase !==
                this.loaderInstance.getEnvironmentArgs().getAppProperties().binBase;
            // eslint-disable-next-line no-case-declarations
            let pythonPath = this.properties.externalModules + (this.Interpreter() === "python3" ?
                "/python36" : "/python");
            if (isVenv) {
                pythonPath += "/venv/" + this.loaderInstance.getProjectConfig().name + "/python";
            } else {
                pythonPath += this.Interpreter() === "python3" ? "/python3" : "/python";
            }
            if (EnvironmentHelper.IsWindows()) {
                pythonPath += ".exe";
            }
            const python : any = require("python-bridge")({ // eslint-disable-line no-case-declarations
                cwd  : process.cwd(),
                env  : {
                    ...this.env,
                    PYTHONPATH: pythonPath
                },
                stdio: ["pipe", "pipe", "pipe"]
            });
            const Writable : any = require("stream").Writable; // eslint-disable-line no-case-declarations
            const stream : any = new Writable({ // eslint-disable-line no-case-declarations
                write($chunk : Buffer, $encoding : string, $callback : () => void) {
                    LogIt.Debug($chunk.toString());
                    $callback();
                }
            });
            python.stdout.pipe(stream);
            python.stderr.pipe(stream);

            python
                .ex([this.fileSystem.Read(script).toString()])
                .then(() : void => {
                    this.loaded = true;
                    this.processHandler = (...$args : any[]) : void => {
                        python`globals()[${this.mainMethod}](*list(${$args.slice(0, -1).reverse()}.values()))`
                            .then(($returnValue : string) : void => {
                                python.end();
                                setTimeout(() : void => {
                                    $args[$args.length - 1]($returnValue);
                                }, 100);
                            })
                            .catch(($ex : Error) : void => {
                                if (StringUtils.Contains($ex.message, "KeyError: '" + this.mainMethod + "'")) {
                                    this.errorHandler(new Error("Script did not specify " + this.mainMethod + " method."));
                                } else {
                                    this.errorHandler(new Error("Script processing has failed.\n" + $ex.message));
                                }
                            });
                    };
                })
                .catch(($ex : Error) : void => {
                    this.errorHandler(new Error("Script load has failed.\n" + $ex.message));
                });
            break;

        case ".js":
            const parent : any = <any>this; // eslint-disable-line no-case-declarations
            const vm : VMHandler = new VMHandler(); // eslint-disable-line no-case-declarations
            vm.Path(script);
            vm.Sandbox({
                async ProcessAsync($cwd : string, $args : string[]) : Promise<void> {
                    LogIt.Warning("Script did not specify Process method.");
                },
                Process($cwd : string, $args : string[], $done : () => void) : void {
                    this.ProcessAsync($cwd, $args)
                        .then($done)
                        .catch(parent.errorHandler);
                }
            });
            vm.Sandbox(this.env);
            vm.ErrorHandler(($error : Error) : void => {
                this.errorHandler(new Error("Script execution has failed. " + $error.message));
            });
            vm.SuccessHandler(() : void => {
                this.loaded = true;
                this.env = vm.Sandbox();
                this.processHandler = (...$args : any[]) : void => {
                    try {
                        const instance : any = vm.Sandbox();
                        if (!ObjectValidator.IsEmptyOrNull(instance[this.mainMethod])) {
                            instance[this.mainMethod].apply(instance, $args); // eslint-disable-line prefer-spread
                        } else {
                            LogIt.Warning("Script did not specify " + this.mainMethod + " method.");
                            $args[$args.length - 1]();
                        }
                    } catch (ex) {
                        this.errorHandler(new Error("Script has failed.\n" + ex.stack));
                    }
                };
            });
            vm.Load();
            break;

        case ".jsonp":
            const dao : BaseDAO = new BaseDAO(); // eslint-disable-line no-case-declarations
            dao.setConfigurationPath(script);
            dao.ConfigurationLibrary(this.env);
            dao.Load(LanguageType.EN, () : void => {
                this.loaded = true;
                this.processHandler = (...$args : any[]) : void => {
                    const instance : any = dao.getConfigurationInstance();
                    if (!ObjectValidator.IsEmptyOrNull(instance[this.mainMethod])) {
                        instance[this.mainMethod].apply(instance, $args); // eslint-disable-line prefer-spread
                    } else {
                        LogIt.Warning("Script did not specify " + this.mainMethod + " method.");
                        $args[$args.length - 1]();
                    }
                };
            }, true);
            break;

        default:
            this.errorHandler(new Error("Unsupported script format \"*" + extension + "\". " +
                "Supported are only scripts with extension .py, .js or .jsonp"));
            break;
        }
    }
}
