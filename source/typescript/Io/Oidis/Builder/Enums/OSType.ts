/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export class OSType extends BaseEnum {
    public static readonly WIN : string = "win";
    public static readonly LINUX : string = "linux";
    public static readonly MAC : string = "mac";
    public static readonly ANDROID : string = "android";
    public static readonly WINPHONE : string = "winphone";
    public static readonly IOS : string = "ios";
    /**
     * @deprecated The IMX enum is deprecated and will be removed soon. Replace the OSType definition by ARM.
     */
    public static readonly IMX : string = "imx";
    public static readonly ARM : string = "arm";
    public static readonly OTHER : string = "other";
}
