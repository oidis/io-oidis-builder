/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export class BuildTaskType extends BaseEnum {
    public static readonly DEFAULT : string = "build";
    public static readonly BUILD_SETTINGS : string = "build-settings";
    public static readonly COPY_TO_TARGET : string = "copy-local-scripts:target";
}
