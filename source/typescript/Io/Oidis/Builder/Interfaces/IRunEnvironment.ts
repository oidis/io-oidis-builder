/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IRunPlatform } from "./IBuildPlatform.js";

export interface IRunEnvironment {
    executablePath : string;
    executableName : string;
    cmd : string;
    cwd : string;
    args : string[];
}

export interface IRunConfig {
    release? : string;
    platform : IRunPlatform;
    port? : number;
    args : string[];
}

// generated-code-start
export const IRunEnvironment = globalThis.RegisterInterface(["executablePath", "executableName", "cmd", "cwd", "args"]);
export const IRunConfig = globalThis.RegisterInterface(["release", "platform", "port", "args"]);
// generated-code-end
