/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IProject as Parent } from "@io-oidis-connector/Io/Oidis/Connector/Interfaces/IProject.js";
import { IProjectTarget as ILocalhostProjectTarget } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IProject.js";
import { DeployServerType } from "../Enums/DeployServerType.js";
import { ResourceType } from "../Enums/ResourceType.js";
import { IDeploySecureServers, IDeployServers } from "./IDeployServers.js";

export interface IProject extends Parent {
    schema : string;
    builder : IProjectBuilder;
    repository : IProjectRepository;
    target : IProjectTarget;
    deploy : IProjectDeploy;
    releases : IProjectTarget[];
    docs : IProjectDocs;
    dependencies : IProjectDependency[];
    solutionsLocation : string;
    solutions : string[];
    runConfigs : IProjectRunConfig[];
    modules : IProjectModule[];
    licensesRegister : ILicenseRecord[];
    noServer : boolean;
    nexusConfig : INexusConfig;
    noCompress : boolean;
    lintErrorsAsWarnings : boolean;
}

export interface IProjectBuilder {
    location : string;
    version : string;
    port : number;
}

export interface IProjectAuthor {
    name : string;
    organization : string;
    email : string;
}

export interface IProjectRepository {
    type : string;
    url : string;
}

export interface IProjectDependencyScriptConfig {
    "name" : string;
    "path" : string;
    "ignore-parent-attribute" : boolean;
    "attributes" : string[] | string;
}

export interface IProjectDependency {
    version : string;
    name : string;
    groupId : string;
    location : string | IProjectDependencyLocation;
    configureScript : IProjectDependencyScriptConfig;
    installScript : IProjectDependencyScriptConfig;
    platforms? : string | string[];
}

export interface IProjectDependencyLocation {
    type : string;
    path : string | IProjectDependencyLocationPath;
    branch : string;
    sha? : string;
    releaseName? : string;
    platform? : string;
    arch? : string;
    headers? : any;
    files? : IProjectDependencyFiles[];
}

export interface IProjectDependencyLocationPath {
    win : string | IProjectDependencyLocationArch;
    linux : string | IProjectDependencyLocationArch;
    /**
     * @deprecated The "imx" location is deprecated and will be removed soon, use arm/arm32 instead
     */
    imx : string | IProjectDependencyLocationArch;
    arm : string | IProjectDependencyLocationArch;
    arm32 : string | IProjectDependencyLocationArch;
    mac : string | IProjectDependencyLocationArch;
}

export interface IProjectDependencyLocationArch {
    x32 : string;
    x64 : string;
}

export interface IProjectDependencyFiles {
    src : string;
    dest : string;
}

export interface IProjectSplashScreenFont {
    name : string;
    size : number;
    bold : boolean;
    color : string;
}

export interface IProjectSplashScreenLabel {
    text : string;
    x : number;
    y : number;
    width : number;
    height : number;
    justify : string;
    visible : boolean;
    font : IProjectSplashScreenFont;
}

export interface IProjectSplashScreenProgressBar {
    x : number;
    y : number;
    width : number;
    height : number;
    marquee : boolean;
    visible : boolean;
    trayProgress : boolean;
    background : string;
    foreground : string;
}

export interface IProjectSplashScreenControls {
    label : IProjectSplashScreenLabel;
    labelStatic : IProjectSplashScreenLabel;
    progress : IProjectSplashScreenProgressBar;
}

export interface IProjectSplashScreenResource {
    location : string;
    copyOnly : boolean;
    type : ResourceType;
    name? : string;
}

export interface IProjectSplashScreen {
    url : string;
    width : number;
    height : number;
    background : string;
    installPath : string;
    executable : string;
    executableArgs? : string[];
    localization? : string;
    userControls : IProjectSplashScreenControls;
    resources : IProjectSplashScreenResource[];
}

export interface IProjectTarget extends ILocalhostProjectTarget {
    icon : string;
    remotePort : number;
    width : number;
    height : number;
    maximized : boolean;
    resources : IProjectTargetResources[];
    platforms : string | string[];
    elevate : boolean;
    singletonApp : boolean;
    startPage : string;
    codeSigning : ICodeSigning;
    upx : IUpx;
    splashScreen : IProjectSplashScreen | string;
    toolchain : string;
    toolchainSettings : IToolchainSettings;
    skipped : boolean;
    configs : string[];
    wuiModulesType : DeployServerType;
    hubLocation : string;
    appDataPath : string;
    selfinstall : IProjectTargetSelfinstall;
    mockServer : IProjectTargetMockServer;
}

export interface IProjectTargetResourceFiles {
    src : string | string[];
    dest : string;
    cwd? : string;
}

export interface IProjectTargetResources {
    name : string;
    input : string;
    files : IProjectTargetResourceFiles[];
    embed : boolean;
    output : string;
    copy : boolean;
    skipReplace : boolean;
    resx : boolean;
    keepLinks : boolean;
    platforms : string | string[];
    protect : string | boolean;
    postBuildCopy : boolean;
    toolchain : string | string[];
}

export interface IProjectTargetSelfinstall {
    script : string;
    chain : string[];
}

export interface IProjectDeploy {
    server : IDeploySecureServers;
    agentsHub : string;
    updates : IProjectDeployUpdate[];
    chunkSize : number;
    user : string;
    pass : string;
}

export interface IProjectDocs {
    systemName : string;
    copyright : string;
    htmlOutput : string;
}

export interface ICodeSigning {
    enabled : boolean;
    certPath : string;
    timestampServer : string;
    serverConfig : string;
    files : string[];
    extensions : string[];
    notarize : boolean;
    prefix : string;
    bundleId : string;
    username : string;
    password : string;
    noWait : boolean;
}

export interface IUpx {
    enabled : boolean;
    options : string[];
    files : string[];
}

export interface IToolchainSettings {
    cwd : string;
    version : string;
    cmakeToolchain : string;
    cmakeOptions : string[];
    cmakeGenerator : string;
    initScript : string;
    CC : string;
    CXX : string;
    LD : string;
    AR : string;
    RANLIB : string;
    STRIP : string;
    OBJCOPY : string;
    OBJDUMP : string;
    NM : string;
    CFLAGS : string;
    CXXFLAGS : string;
    LDFLAGS : string;
    SYSROOT : string;
    types : string[];
    serverless : boolean;
    gypVcVer : string;
    withDependenciesScan : boolean;
}

export interface IProjectDeployUpdate {
    name? : string;
    skipped? : boolean;
    register? : string;
    location : IProjectDeployUpdateLocation;
    agents : IProjectDeployUpdateAgent[];
    app : IProjectDeployUpdateApp;
    postDeployScript? : string;
}

export interface IProjectDeployUpdateLocation {
    path? : IDeploySecureServers;
    projectName? : string;
    releaseName? : string;
    platform? : string;
    version? : string;
}

export interface IProjectDeployUpdateAgent {
    skipped? : boolean;
    hub : string;
    name : string;
    profile? : string;
}

export interface IProjectDeployUpdateApp {
    name? : string;
    path? : string;
    config? : string;
    args? : string[];
    cwd? : string;
}

export interface IProjectRunConfig {
    release? : string | IReleasePlatform;
    platform? : string;
    port? : number;
    args? : string[];
}

export interface IReleasePlatform {
    name? : string;
    platform? : string;
}

export interface IProjectModule {
    name : string;
    script : string;
    args? : string[];
    interpreter? : string;
    overrideTask? : boolean;
    events? : IProjectModuleEvents;
}

export interface IProjectModuleEvents {
    before? : string | string[];
    after? : string | string[];
}

export interface ILicenseRecord {
    releaseName : string;
    version : string;
    description : string;
    author : string;
    license : string;
    format : string;
    location : string[];
    optional : boolean;
    enabled : boolean;
}

export interface IProjectTargetMockServer {
    port : number;
    config : string | string[] | any;
}

export interface INexusConfig {
    user : string;
    pass : string;
    filter : string;
    location : IDeployServers;
}

// generated-code-start
/* eslint-disable */
export const IProject = globalThis.RegisterInterface(["schema", "builder", "repository", "target", "deploy", "releases", "docs", "dependencies", "solutionsLocation", "solutions", "runConfigs", "modules", "licensesRegister", "noServer", "nexusConfig", "noCompress", "lintErrorsAsWarnings"], <any>Parent);
export const IProjectBuilder = globalThis.RegisterInterface(["location", "version", "port"]);
export const IProjectAuthor = globalThis.RegisterInterface(["name", "organization", "email"]);
export const IProjectRepository = globalThis.RegisterInterface(["type", "url"]);
export const IProjectDependencyScriptConfig = globalThis.RegisterInterface(["\"name\"", "\"path\"", "\"ignore-parent-attribute\"", "\"attributes\""]);
export const IProjectDependency = globalThis.RegisterInterface(["version", "name", "groupId", "location", "configureScript", "installScript", "platforms"]);
export const IProjectDependencyLocation = globalThis.RegisterInterface(["type", "path", "branch", "sha", "releaseName", "platform", "arch", "headers", "files"]);
export const IProjectDependencyLocationPath = globalThis.RegisterInterface(["win", "linux", "imx", "arm", "arm32", "mac"]);
export const IProjectDependencyLocationArch = globalThis.RegisterInterface(["x32", "x64"]);
export const IProjectDependencyFiles = globalThis.RegisterInterface(["src", "dest"]);
export const IProjectSplashScreenFont = globalThis.RegisterInterface(["name", "size", "bold", "color"]);
export const IProjectSplashScreenLabel = globalThis.RegisterInterface(["text", "x", "y", "width", "height", "justify", "visible", "font"]);
export const IProjectSplashScreenProgressBar = globalThis.RegisterInterface(["x", "y", "width", "height", "marquee", "visible", "trayProgress", "background", "foreground"]);
export const IProjectSplashScreenControls = globalThis.RegisterInterface(["label", "labelStatic", "progress"]);
export const IProjectSplashScreenResource = globalThis.RegisterInterface(["location", "copyOnly", "type", "name"]);
export const IProjectSplashScreen = globalThis.RegisterInterface(["url", "width", "height", "background", "installPath", "executable", "executableArgs", "localization", "userControls", "resources"]);
export const IProjectTarget = globalThis.RegisterInterface(["icon", "remotePort", "width", "height", "maximized", "resources", "platforms", "elevate", "singletonApp", "startPage", "codeSigning", "upx", "splashScreen", "toolchain", "toolchainSettings", "skipped", "configs", "wuiModulesType", "hubLocation", "appDataPath", "selfinstall", "mockServer"], <any>ILocalhostProjectTarget);
export const IProjectTargetResourceFiles = globalThis.RegisterInterface(["src", "dest", "cwd"]);
export const IProjectTargetResources = globalThis.RegisterInterface(["name", "input", "files", "embed", "output", "copy", "skipReplace", "resx", "keepLinks", "platforms", "protect", "postBuildCopy", "toolchain"]);
export const IProjectTargetSelfinstall = globalThis.RegisterInterface(["script", "chain"]);
export const IProjectDeploy = globalThis.RegisterInterface(["server", "agentsHub", "updates", "chunkSize", "user", "pass"]);
export const IProjectDocs = globalThis.RegisterInterface(["systemName", "copyright", "htmlOutput"]);
export const ICodeSigning = globalThis.RegisterInterface(["enabled", "certPath", "timestampServer", "serverConfig", "files", "extensions", "notarize", "prefix", "bundleId", "username", "password", "noWait"]);
export const IUpx = globalThis.RegisterInterface(["enabled", "options", "files"]);
export const IToolchainSettings = globalThis.RegisterInterface(["cwd", "version", "cmakeToolchain", "cmakeOptions", "cmakeGenerator", "initScript", "CC", "CXX", "LD", "AR", "RANLIB", "STRIP", "OBJCOPY", "OBJDUMP", "NM", "CFLAGS", "CXXFLAGS", "LDFLAGS", "SYSROOT", "types", "serverless", "gypVcVer", "withDependenciesScan"]);
export const IProjectDeployUpdate = globalThis.RegisterInterface(["name", "skipped", "register", "location", "agents", "app", "postDeployScript"]);
export const IProjectDeployUpdateLocation = globalThis.RegisterInterface(["path", "projectName", "releaseName", "platform", "version"]);
export const IProjectDeployUpdateAgent = globalThis.RegisterInterface(["skipped", "hub", "name", "profile"]);
export const IProjectDeployUpdateApp = globalThis.RegisterInterface(["name", "path", "config", "args", "cwd"]);
export const IProjectRunConfig = globalThis.RegisterInterface(["release", "platform", "port", "args"]);
export const IReleasePlatform = globalThis.RegisterInterface(["name", "platform"]);
export const IProjectModule = globalThis.RegisterInterface(["name", "script", "args", "interpreter", "overrideTask", "events"]);
export const IProjectModuleEvents = globalThis.RegisterInterface(["before", "after"]);
export const ILicenseRecord = globalThis.RegisterInterface(["releaseName", "version", "description", "author", "license", "format", "location", "optional", "enabled"]);
export const IProjectTargetMockServer = globalThis.RegisterInterface(["port", "config"]);
export const INexusConfig = globalThis.RegisterInterface(["user", "pass", "filter", "location"]);
/* eslint-enable */
// generated-code-end
