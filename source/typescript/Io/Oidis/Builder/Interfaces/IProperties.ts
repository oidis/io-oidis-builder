/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IProject, IProjectDependency, IToolchainSettings } from "./IProject.js";

export interface IProperties {
    binBase : string;
    externalModules : string;
    projectBase : string;
    sources : string;
    resources : string;
    dependencies : string;
    dependenciesConfig : IProject[];
    cppDependencies : IProjectDependency[];
    cppIncludes : string;
    cppLibraries : string;
    oidisDependencies : IProjectDependency[];
    esModuleDependencies : IProjectDependency[];
    mobilePlatforms : string[];
    mobileExtensions : any[];
    dependenciesResources : string;
    tmp : string;
    dest : string;
    compiled : string;
    packageName : string;
    packageVersion : string;
    loaderPath : string;
    nodejsPort : number;
    licenseText : string;
    projectHas : IProjectHas;
    javaProjectVersion : string;
    javaDependencies : IProjectDependency[];
    mavenResourcePaths : string[];
    mavenArtifacts : IMavenArtifact[];
    mavenRepositories : IMavenRepository[];
    stats : IPropertiesStats;
    buildCheckRegister : Array<($option? : string) => Promise<void>>;
    clientConfig : any;
    serverConfig : any;
    toolchains : IToolchains;
    pythonDependencies : string;
    builderPlatform : string;
    sourceNamespacePath : string;
}

export interface IMavenArtifact {
    groupId : string;
    artifactId : string;
    version : string;
    location : string;
}

export interface IMavenRepository {
    id : string;
    url : string;
}

export interface IProjectHasCodeType {
    Source : () => boolean;
    Tests : () => boolean;
}

export interface IProjectHas {
    TypeScript : IProjectHasCodeType;
    Java : IProjectHasCodeType;
    Cpp : IProjectHasCodeType;
    Python : IProjectHasCodeType;
    SASS : () => boolean;
    Resources : () => boolean;
    SeleniumTests : () => boolean;
    Markdown : () => boolean;
    ContentFor : ($pattern : string) => boolean;
    ESModules : () => boolean;
}

export interface IPropertiesStats {
    buildMinutes : number;
}

export interface IToolchains {
    gcc : IToolchainSettings;
    arm : IToolchainSettings;
    aarch32 : IToolchainSettings;
    aarch64 : IToolchainSettings;
    clang : IToolchainSettings;
    msvc : IToolchainSettings;
}

// generated-code-start
/* eslint-disable */
export const IProperties = globalThis.RegisterInterface(["binBase", "externalModules", "projectBase", "sources", "resources", "dependencies", "dependenciesConfig", "cppDependencies", "cppIncludes", "cppLibraries", "oidisDependencies", "esModuleDependencies", "mobilePlatforms", "mobileExtensions", "dependenciesResources", "tmp", "dest", "compiled", "packageName", "packageVersion", "loaderPath", "nodejsPort", "licenseText", "projectHas", "javaProjectVersion", "javaDependencies", "mavenResourcePaths", "mavenArtifacts", "mavenRepositories", "stats", "buildCheckRegisterArray", "clientConfig", "serverConfig", "toolchains", "pythonDependencies", "builderPlatform", "sourceNamespacePath"]);
export const IMavenArtifact = globalThis.RegisterInterface(["groupId", "artifactId", "version", "location"]);
export const IMavenRepository = globalThis.RegisterInterface(["id", "url"]);
export const IProjectHasCodeType = globalThis.RegisterInterface(["Source", "Tests"]);
export const IProjectHas = globalThis.RegisterInterface(["TypeScript", "Java", "Cpp", "Python", "SASS", "Resources", "SeleniumTests", "Markdown", "ContentFor", "ESModules"]);
export const IPropertiesStats = globalThis.RegisterInterface(["buildMinutes"]);
export const IToolchains = globalThis.RegisterInterface(["gcc", "arm", "aarch32", "aarch64", "clang", "msvc"]);
/* eslint-enable */
// generated-code-end
