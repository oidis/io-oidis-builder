/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 NXP
 * Copyright 2023-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";

export class ICloudFilterType extends BaseEnum {
    public static readonly PUSH : string = "push";
    public static readonly PULL : string = "pull";
    public static readonly PROJECT : string = "project";
}

export interface ICloudSyncArgs {
    syncFolder : string;
    includeLocalMetadata? : boolean;
    filters : Array<ICloudFilter | ICloudFilterType>;
}

export interface ICloudSyncFileMetadata {
    syncFolder : string;
    localPath : string;
    uploadTimestamp : number;
}

export interface ICloudFilter {
    excludes? : string[];
}

export interface ICloudSyncMap {
    items : ICloudItemStats[];
}

export interface ICloudChangeArgs {
    path? : string;
    changed? : number;
}

export interface ICloudItemStats extends ICloudChangeArgs {
    hash? : string;
    indexed? : number;
    size? : number;
    changedLocal? : number;
}

export interface ICloudChanges {
    pushFiles : string[];
    pushFolders : string[];
    deleteItems : string[];
}

// generated-code-start
export const ICloudSyncArgs = globalThis.RegisterInterface(["syncFolder", "includeLocalMetadata", "filters"]);
export const ICloudSyncFileMetadata = globalThis.RegisterInterface(["syncFolder", "localPath", "uploadTimestamp"]);
export const ICloudFilter = globalThis.RegisterInterface(["excludes"]);
export const ICloudSyncMap = globalThis.RegisterInterface(["items"]);
export const ICloudChangeArgs = globalThis.RegisterInterface(["path", "changed"]);
export const ICloudItemStats = globalThis.RegisterInterface(["hash", "indexed", "size", "changedLocal"], <any>ICloudChangeArgs);
export const ICloudChanges = globalThis.RegisterInterface(["pushFiles", "pushFolders", "deleteItems"]);
// generated-code-end
