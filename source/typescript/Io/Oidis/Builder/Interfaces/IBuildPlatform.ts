/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IBuildPlatform {
    os? : string;
    type? : string;
    arch? : string;
    toolchain? : string;
}

export interface IRunPlatform extends IBuildPlatform {
    value? : string;
}

// generated-code-start
export const IBuildPlatform = globalThis.RegisterInterface(["os", "type", "arch", "toolchain"]);
export const IRunPlatform = globalThis.RegisterInterface(["value"], <any>IBuildPlatform);
// generated-code-end
