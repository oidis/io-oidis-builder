/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IDeployCredentials {
    user : string;
    pass : string;
}

export interface IDeploySecureServer extends IDeployCredentials {
    location : string;
}

export interface IDeploySecureServers {
    prod : IDeploySecureServer;
    dev : IDeploySecureServer;
    eap : IDeploySecureServer;
}

export interface IDeployServers {
    prod : string;
    dev : string;
    eap : string;
}

export interface IDeployServersCredentials {
    prod : IDeployCredentials;
    dev : IDeployCredentials;
    eap : IDeployCredentials;
}

// generated-code-start
export const IDeployCredentials = globalThis.RegisterInterface(["user", "pass"]);
export const IDeploySecureServer = globalThis.RegisterInterface(["location"], <any>IDeployCredentials);
export const IDeploySecureServers = globalThis.RegisterInterface(["prod", "dev", "eap"]);
export const IDeployServers = globalThis.RegisterInterface(["prod", "dev", "eap"]);
export const IDeployServersCredentials = globalThis.RegisterInterface(["prod", "dev", "eap"]);
// generated-code-end
