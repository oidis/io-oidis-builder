/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IProject, IProjectTarget } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IProject.js";
import { IBuilderAgent } from "../Connectors/BuilderHubConnector.js";
import { CliTaskType } from "../Enums/CliTaskType.js";
import { BuildProductArgs } from "../Structures/BuildProductArgs.js";
import { IDeployServers, IDeployServersCredentials } from "./IDeployServers.js";
import { INexusConfig, IProjectModule } from "./IProject.js";

export interface IAppConfiguration extends IProject {
    schema : string;
    deployServer : IDeployServers;
    deployCredentials : IDeployServersCredentials;
    deployAgentsHub : string;
    repository : any;
    devDependencies : any;
    scripts : any;
    engines : any;
    wuiModules : IBuilderWuiModules;
    tokens : IBuilderTokens;
    servicePort : number;
    workspaces : string[];
    eclipseBase : string;
    ideaBase : string;
    symbols : any;
    vcvarsPath : string;
    cloud : ICloudConfig;
    dockerRegistry : IDockerRegistryConfig;
    jira : IServerConfigurationJira;
    hub : IBuilderServerCredentials;
    agent : IBuilderAgent;
    service : IServiceConfig;
    noSudo : boolean;
    target : IAppTargetConfiguration;
    noReport : boolean;
    noExternalTypes : boolean;
    noSelfupdate : boolean;
    noAutoinstall : boolean;
    reportHub : string;
    modules : IProjectModule[];
    nexusConfig : INexusConfig;
    dependenciesDownloadConfig : IDependenciesDownloadConfig;
}

export interface IAppTargetConfiguration extends IProjectTarget {
    updater : string;
}

export interface IBuilderWuiModules {
    "com-wui-framework-chromiumre" : IBuilderModuleConfig[];
    "com-wui-framework-connector" : IBuilderModuleConfig[];
    "com-wui-framework-launcher" : IBuilderModuleConfig[];
    "com-wui-framework-selfextractor" : IBuilderModuleConfig[];
    "com-wui-framework-idejre-eclipse" : IBuilderModuleConfig[];
    "com-wui-framework-idejre-idea" : IBuilderModuleConfig[];
}

export interface IBuilderModuleConfig {
    name : string;
    location : string | IBuilderModuleLocation;
    os : string;
    arch : string;
    version? : string;
    sourcePath : string;
    fullPath : string;
}

export interface IBuilderModuleLocation {
    path? : string;
    projectName : string;
    releaseName? : string;
    platform? : string;
    version? : string;
}

export interface IBuilderTokens {
    phonegap : string;
}

export interface IBuilderServerCredentials {
    url : string;
    user : string;
    pass : string;
}

export interface ICloudConfig {
    url : string;
    syncBase : string;
}

export interface IDockerRegistryConfig extends IBuilderServerCredentials {
    email : string;
}

export interface IServerConfigurationJira {
    cachePath : string;
    projects : string[];
    source : IBuilderServerCredentials;
    target : IBuilderServerCredentials;
    ignoreKeys : string[];
}

export interface IServiceConfig {
    address : string;
    port : number;
}

export interface IBuildArgs {
    isRebuild : boolean;
    isTest : boolean;
    platform : string;
    product : BuildProductArgs;
    releaseName : string;
    time : Date;
    timestamp : number;
    type : CliTaskType;
    year : number;
}

export interface ITestEnvironment {
    path : string;
    cwd : string;
}

export interface ITestConfig {
    environment : ITestEnvironment;
}

export interface IDependenciesDownloadConfig {
    maxRetry : number;
    timeout : number;
}

// generated-code-start
/* eslint-disable */
export const IAppConfiguration = globalThis.RegisterInterface(["schema", "deployServer", "deployCredentials", "deployAgentsHub", "repository", "devDependencies", "scripts", "engines", "wuiModules", "tokens", "servicePort", "workspaces", "eclipseBase", "ideaBase", "symbols", "vcvarsPath", "cloud", "dockerRegistry", "jira", "hub", "agent", "service", "noSudo", "target", "noReport", "noExternalTypes", "noSelfupdate", "noAutoinstall", "reportHub", "modules", "nexusConfig", "dependenciesDownloadConfig"], <any>IProject);
export const IAppTargetConfiguration = globalThis.RegisterInterface(["updater"], <any>IProjectTarget);
export const IBuilderWuiModules = globalThis.RegisterInterface(["\"com-wui-framework-chromiumre\"", "\"com-wui-framework-connector\"", "\"com-wui-framework-launcher\"", "\"com-wui-framework-selfextractor\"", "\"com-wui-framework-idejre-eclipse\"", "\"com-wui-framework-idejre-idea\""]);
export const IBuilderModuleConfig = globalThis.RegisterInterface(["name", "location", "os", "arch", "version", "sourcePath", "fullPath"]);
export const IBuilderModuleLocation = globalThis.RegisterInterface(["path", "projectName", "releaseName", "platform", "version"]);
export const IBuilderTokens = globalThis.RegisterInterface(["phonegap"]);
export const IBuilderServerCredentials = globalThis.RegisterInterface(["url", "user", "pass"]);
export const ICloudConfig = globalThis.RegisterInterface(["url", "syncBase"]);
export const IDockerRegistryConfig = globalThis.RegisterInterface(["email"], <any>IBuilderServerCredentials);
export const IServerConfigurationJira = globalThis.RegisterInterface(["cachePath", "projects", "source", "target", "ignoreKeys"]);
export const IServiceConfig = globalThis.RegisterInterface(["address", "port"]);
export const IBuildArgs = globalThis.RegisterInterface(["isRebuild", "isTest", "platform", "product", "releaseName", "time", "timestamp", "type", "year"]);
export const ITestEnvironment = globalThis.RegisterInterface(["path", "cwd"]);
export const ITestConfig = globalThis.RegisterInterface(["environment"]);
export const IDependenciesDownloadConfig = globalThis.RegisterInterface(["maxRetry", "timeout"]);
/* eslint-enable */
// generated-code-end
