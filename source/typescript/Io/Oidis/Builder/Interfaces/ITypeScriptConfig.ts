/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface ITypeScriptConfigOptions {
    target : string;
    module : string;
    removeComments : boolean;
    sourceMap : boolean;
    declaration : boolean;
    references : string;
}

export interface ITypeScriptConfig {
    src : string[];
    dest : string;
    reference : string;
    "interface" : string; // eslint-disable-line @stylistic/quote-props
    options : ITypeScriptConfigOptions;
}

export interface ITypeScriptConfigs {
    source : ITypeScriptConfig;
    unit : ITypeScriptConfig;
    selenium : ITypeScriptConfig;
    coverage : ITypeScriptConfig;
    docs : ITypeScriptConfig;
    cache : ITypeScriptConfig;
    selfbuild : ITypeScriptConfig;
}

// generated-code-start
/* eslint-disable */
export const ITypeScriptConfigOptions = globalThis.RegisterInterface(["target", "module", "removeComments", "sourceMap", "declaration", "references"]);
export const ITypeScriptConfig = globalThis.RegisterInterface(["src", "dest", "reference", "\"interface\"", "options"]);
export const ITypeScriptConfigs = globalThis.RegisterInterface(["source", "unit", "selenium", "coverage", "docs", "cache", "selfbuild"]);
/* eslint-enable */
// generated-code-end
