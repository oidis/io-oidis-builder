/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IPythonPackage {
    installed : boolean;
    location? : string;
    name : string;
    version : string;
}

// generated-code-start
export const IPythonPackage = globalThis.RegisterInterface(["installed", "location", "name", "version"]);
// generated-code-end
