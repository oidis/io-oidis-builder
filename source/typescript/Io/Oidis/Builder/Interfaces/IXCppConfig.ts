/*! ******************************************************************************************************** *
 *
 * Copyright 2017 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

export interface IXCppConfigOptions {
    targetName : string;
    cmakeDest : string;
}

export interface IXCppConfig {
    src : string[];
    srcBase : string[];
    dest : string;
    reference : string;
    "interface" : string; // eslint-disable-line @stylistic/quote-props
    reflection : string;
    options : IXCppConfigOptions;
    report : string;
}

export interface IXCppConfigs {
    source : IXCppConfig;
    unit : IXCppConfig;
    coverage : IXCppConfig;
}

// generated-code-start
/* eslint-disable */
export const IXCppConfigOptions = globalThis.RegisterInterface(["targetName", "cmakeDest"]);
export const IXCppConfig = globalThis.RegisterInterface(["src", "srcBase", "dest", "reference", "\"interface\"", "reflection", "options", "report"]);
export const IXCppConfigs = globalThis.RegisterInterface(["source", "unit", "coverage"]);
/* eslint-enable */
// generated-code-end
