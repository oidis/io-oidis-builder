/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IBaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IBaseObject.js";
import { TaskEnvironment } from "../Structures/TaskEnvironment.js";
import { TasksResolver } from "../TasksResolver.js";

export interface IBaseTask extends IBaseObject {
    Name() : string;

    Process($option? : string) : Promise<void>;

    getDependenciesTree($option? : string) : string[];

    setEnvironment($value : TaskEnvironment) : void;

    setOwner($owner : TasksResolver) : void;
}

// generated-code-start
/* eslint-disable */
export const IBaseTask = globalThis.RegisterInterface(["Name", "Process", "getDependenciesTree", "setEnvironment", "setOwner"], <any>IBaseObject);
/* eslint-enable */
// generated-code-end
