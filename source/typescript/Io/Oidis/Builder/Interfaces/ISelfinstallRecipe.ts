/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { IInstallationEnvironment } from "@io-oidis-services/Io/Oidis/Services/Interfaces/DAO/IBaseInstallationRecipe.js";
import { ProgramArgs } from "../Structures/ProgramArgs.js";

export interface ISelfinstallRecipe extends IInstallationEnvironment {
    externalModules : string;
    programArgs : ProgramArgs;
    setColor : ($input : string, $type : ColorType) => string;
}

// generated-code-start
/* eslint-disable */
export const ISelfinstallRecipe = globalThis.RegisterInterface(["externalModules", "programArgs", "setColor"], <any>IInstallationEnvironment);
/* eslint-enable */
// generated-code-end
