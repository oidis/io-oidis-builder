/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { IProject } from "../Interfaces/IProject.js";
import { IProperties } from "../Interfaces/IProperties.js";
import { Loader } from "../Loader.js";

export class ResourceHandler extends BaseObject {

    public static GenerateVersionRc($executablePath : string, $targetName : string) : string {
        const EOL : string = require("os").EOL;
        const project : IProject = Loader.getInstance().getProjectConfig();

        if (EnvironmentHelper.IsWindows()) {
            const versions : string[] = project.version.split("-")[0].split(".");

            let rcFileTemplate : string = "" +
                "1 VERSIONINFO" + EOL +
                "FILEVERSION     " + versions[0] + "," + versions[1] + "," + versions[2] + ",0" + EOL +
                "PRODUCTVERSION  " + versions[0] + "," + versions[1] + "," + versions[2] + ",0" + EOL +
                "BEGIN" + EOL +
                "   BLOCK \"StringFileInfo\"" + EOL +
                "   BEGIN" + EOL +
                "       BLOCK \"040904E4\"" + EOL +
                "       BEGIN" + EOL +
                "           VALUE \"ProductName\", \"" + $targetName + "\"" + EOL +
                "           VALUE \"ProductVersion\", \"" + project.version + "\"" + EOL +
                "           VALUE \"CompanyName\", \"" + project.target.companyName + "\"" + EOL +
                "           VALUE \"FileDescription\", \"" + project.target.description + "\"" + EOL +
                "           VALUE \"LegalCopyright\", \"" + project.target.copyright + "\"" + EOL +
                "           VALUE \"InternalName\", \"" + project.name + "\"" + EOL +
                "           VALUE \"FileVersion\", \"" + project.version + "\"" + EOL +
                "           VALUE \"OriginalFilename\", \"" + project.name + ".exe\"" + EOL;

            if (!ObjectValidator.IsEmptyOrNull($executablePath)) {
                $executablePath = Loader.getInstance().getFileSystemHandler().NormalizePath($executablePath);
                const oldVersionInfo : any = ResourceHandler.getVersionInfo($executablePath);
                if (oldVersionInfo !== null && oldVersionInfo.hasOwnProperty("BaseProjectName")) {
                    rcFileTemplate += "" +
                        "           VALUE \"BaseProjectName\", \"" + oldVersionInfo.BaseProjectName + "\"" + EOL;
                }
            }

            rcFileTemplate += "" +
                "       END" + EOL +
                "   END" + EOL + EOL +
                "   BLOCK \"VarFileInfo\"" + EOL +
                "   BEGIN" + EOL +
                "       VALUE \"Translation\", 0x409, 1252" + EOL +
                "   END" + EOL +
                "END";

            return rcFileTemplate;
        }
        return "";
    }

    public static async ModifyVersionInfo($filePath : string, $targetName : string) : Promise<void> {
        $filePath = Loader.getInstance().getFileSystemHandler().NormalizePath($filePath);
        const properties : IProperties = Loader.getInstance().getAppProperties();

        if (EnvironmentHelper.IsWindows()) {
            const rcFilePath : string = Loader.getInstance().getFileSystemHandler().NormalizePath(
                properties.projectBase + "/" + properties.compiled + "/version.rc");

            if (Loader.getInstance().getFileSystemHandler()
                .Write(rcFilePath, ResourceHandler.GenerateVersionRc($filePath, $targetName))) {
                await ResourceHandler.Compile(rcFilePath);
                await ResourceHandler.EmbedResource($filePath, rcFilePath + ".res");
                LogIt.Info("Resources has been updated " + $filePath);
            } else {
                LogIt.Error("Failed to create version.rc file.");
            }
        } else {
            LogIt.Info("Version info update has been skipped: supported only on Windows platform");
        }
    }

    public static async ModifyIcon($filePath : string, $iconPath : string, $resIndex : number = 0) : Promise<void> {
        if (EnvironmentHelper.IsWindows()) {
            await ResourceHandler.EmbedFile($filePath, "ICONGROUP", $resIndex.toString(), $iconPath);
            LogIt.Info("Icon has been updated " + $filePath);
        } else {
            LogIt.Info("Icon update has been skipped: supported only on Windows platform");
        }
    }

    public static async ModifyManifest($filePath : string) : Promise<void> {
        const EOL : string = require("os").EOL;
        const project : IProject = Loader.getInstance().getProjectConfig();
        const properties : IProperties = Loader.getInstance().getAppProperties();

        if (EnvironmentHelper.IsWindows()) {
            const rcFilePath : string = Loader.getInstance().getFileSystemHandler().NormalizePath(
                properties.projectBase + "/" + properties.compiled + "/manifest.rc");
            const level : string = project.target.elevate ? "requireAdministrator" : "asInvoker";

            const rcFileTemplate : string =
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + EOL +
                "<assembly xmlns=\"urn:schemas-microsoft-com:asm.v1\" manifestVersion=\"1.0\">" + EOL +
                "  <trustInfo xmlns=\"urn:schemas-microsoft-com:asm.v3\">" + EOL +
                "    <security>" + EOL +
                "      <requestedPrivileges>" + EOL +
                "        <requestedExecutionLevel level=\"" + level + "\" uiAccess=\"false\"/>" + EOL +
                "      </requestedPrivileges>" + EOL +
                "    </security>" + EOL +
                "  </trustInfo>" + EOL +
                "  <compatibility xmlns=\"urn:schemas-microsoft-com:compatibility.v1\">" + EOL +
                "    <application>" + EOL +
                "      <!--The ID below indicates application support for Windows Vista -->" + EOL +
                "      <supportedOS Id=\"{e2011457-1546-43c5-a5fe-008deee3d3f0}\"/>" + EOL +
                "      <!--The ID below indicates application support for Windows 7 -->" + EOL +
                "      <supportedOS Id=\"{35138b9a-5d96-4fbd-8e2d-a2440225f93a}\"/>" + EOL +
                "      <!--The ID below indicates application support for Windows 8 -->" + EOL +
                "      <supportedOS Id=\"{4a2f28e3-53b9-4441-ba9c-d69d4a4a6e38}\"/>" + EOL +
                "      <!--The ID below indicates application support for Windows 8.1 -->" + EOL +
                "      <supportedOS Id=\"{1f676c76-80e1-4239-95bb-83d0f6d0da78}\"/>" + EOL +
                "      <!--The ID below indicates application support for Windows 10 -->" + EOL +
                "      <supportedOS Id=\"{8e0f7a12-bfb3-4fe8-b9a5-48fd50a15a9a}\"/>" + EOL +
                "    </application>" + EOL +
                "  </compatibility>" + EOL +
                "</assembly>" + EOL;
            Loader.getInstance().getFileSystemHandler().Write(rcFilePath, rcFileTemplate);

            await ResourceHandler.Compile(rcFilePath);
            await ResourceHandler.EmbedFile($filePath, "MANIFEST", "1", rcFilePath + ".res");
            LogIt.Info("Manifest has been updated " + $filePath);
        } else {
            LogIt.Info("Manifest update has been skipped: supported only on Windows platform");
        }
    }

    public static CreateDesktopEntryTemplate($targetName : string, $iconPath : string) : void {
        const EOL : string = require("os").EOL;
        const project : IProject = Loader.getInstance().getProjectConfig();
        const properties : IProperties = Loader.getInstance().getAppProperties();

        const template : string = "" +
            "[Desktop Entry]" + EOL +
            "Type=Application" + EOL +
            "Name=" + $targetName + EOL +
            "Icon={appRoot}/" + $iconPath + EOL +
            "Exec={appRoot}/" + $targetName + EOL +
            "Comment=" + project.target.description + EOL +
            "";
        Loader.getInstance().getFileSystemHandler()
            .Write(properties.projectBase + "/build/target/" + project.target.name + ".desktop", template);
    }

    public static async EmbedFile($filePath : string, $type : string, $name : string, $dataFile : string) : Promise<void> {
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        $filePath = fileSystem.NormalizePath($filePath);
        const path : any = require("path");
        const properties : IProperties = Loader.getInstance().getAppProperties();

        if (fileSystem.Exists($filePath) && fileSystem.Exists($dataFile)) {
            if (fileSystem.IsFile($filePath) && fileSystem.IsFile($dataFile)) {
                if (!ObjectValidator.IsEmptyOrNull($type) && !ObjectValidator.IsEmptyOrNull($name)) {
                    if (EnvironmentHelper.IsWindows()) {
                        const run : any = async ($dataTmpPath : string) : Promise<void> => {
                            const command : string[] = [
                                "-open", "\"" + $filePath + "\"",
                                "-save", "\"" + $filePath + "\"",
                                "-action", "addoverwrite",
                                "-res", "\"" + fileSystem.NormalizePath($dataTmpPath) + "\"",
                                "-mask", $type + "," + $name + ","
                            ];
                            await this.executeResourceHacker(command);
                        };
                        if (path.extname($dataFile).toLocaleLowerCase() === ".exe") {
                            let tmpDataFile : string = (properties.projectBase + "/" + properties.compiled + "/" +
                                path.basename($dataFile) + "_" + Math.random().toString());
                            tmpDataFile = tmpDataFile.replace(/\./g, "_") + ".dump";
                            await fileSystem.CopyAsync($dataFile, tmpDataFile);
                            await run(tmpDataFile);
                            fileSystem.Delete(tmpDataFile);
                        } else {
                            await run($dataFile);
                        }
                    } else {
                        const fullSectionName : string = "." + $type + "/" + $name;
                        const res : IExecuteResult = await Loader.getInstance().getTerminal().ExecuteAsync("objcopy", [
                            "--add-section " + fullSectionName + "=" + StringUtils.Replace($dataFile, "$", "\\$"),
                            "--set-section-flags " + fullSectionName + "=noload,readonly",
                            $filePath
                        ], null);
                        if (res.exitCode !== 0) {
                            LogIt.Error("Unable to update resource.\n" + res.std[0] + "\n" + res.std[1]);
                        }
                    }
                } else {
                    LogIt.Warning("Resource type (=" + $type + ") and name(=" + $name + ") must be specified.");
                }
            } else {
                LogIt.Error("EmbedFile method do not support directories embedding.");
            }
        } else {
            LogIt.Error("Requested file to embed (" + $filePath + ") or " +
                "file to be embedded (" + $dataFile + ") does not exists.");
        }
    }

    public static async EmbedResource($filePath : string, $resourceFile : string) : Promise<void> {
        if (EnvironmentHelper.IsWindows()) {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (fileSystem.Exists($filePath) && fileSystem.Exists($resourceFile)) {
                if (fileSystem.IsFile($filePath) && fileSystem.IsFile($resourceFile)) {
                    const command : string[] = [
                        "-open", "\"" + $filePath + "\"",
                        "-save", "\"" + $filePath + "\"",
                        "-action", "addoverwrite",
                        "-res", "\"" + Loader.getInstance().getFileSystemHandler().NormalizePath($resourceFile) + "\""
                    ];
                    await this.executeResourceHacker(command);
                } else {
                    LogIt.Error("EmbedResource method do not support directories embedding.");
                }
            } else {
                LogIt.Error("Requested file to embed or resource file must exists.");
            }
        } else {
            LogIt.Info("Resource embed has been skipped: supported only on Windows platform");
        }
    }

    public static async Compile($filePath : string) : Promise<void> {
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        if (fileSystem.Exists($filePath)) {
            if (fileSystem.IsFile($filePath)) {
                const command : string[] = [
                    "-open", "\"" + $filePath + "\"",
                    "-save", "\"" + $filePath + ".res\"",
                    "-action", "compile"
                ];
                await this.executeResourceHacker(command);
            } else {
                LogIt.Error("EmbedFile method do not support directories embedding.");
            }
        } else {
            LogIt.Error("Requested file to be compiled not exists.");
        }
    }

    public static getVersionInfo($filePath : string) : any {
        const readVer : any = require("win-version-info");
        const path : any = require("path");
        let verInfo : any = null;
        const filePath : string = path.normalize(StringUtils.Remove($filePath, "\""));

        if (Loader.getInstance().getFileSystemHandler().Exists(filePath)) {
            verInfo = readVer(filePath);
        }
        return verInfo;
    }

    public static async RunScript($script : string) : Promise<void> {
        if (EnvironmentHelper.IsWindows()) {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            const properties : IProperties = Loader.getInstance().getAppProperties();
            const scriptPath : string = properties.projectBase + "/" + properties.compiled + "/" +
                "script_" + ResourceHandler.UID() + ".txt";
            if (fileSystem.Write(scriptPath, $script)) {
                await ResourceHandler.executeResourceHacker(["-script", "\"" + scriptPath + "\""]);
            } else {
                LogIt.Error("Failed to create script file.");
            }
        } else {
            LogIt.Error("Execution of scripts is supported only on Windows platform.");
        }
    }

    private static async executeResourceHacker($commands : string[]) : Promise<void> {
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const cwd : string = fileSystem.NormalizePath(Loader.getInstance().getProgramArgs().AppDataPath() +
            "/external_modules/resourcehacker");
        if (!fileSystem.Exists(cwd + "/ResourceHacker.exe")) {
            LogIt.Error("Instance of ResourceHacker has not been found. " +
                "To fix this issue, please execute 'oidis selfinstall --force' command.");
        } else {
            const res : IExecuteResult = await Loader.getInstance().getTerminal().ExecuteAsync("ResourceHacker.exe", $commands, cwd);
            if (res.exitCode !== 0) {
                LogIt.Error("Unable to update resource.\n" + res.std[0] + "\n" + res.std[1]);
            }
        }
    }
}
