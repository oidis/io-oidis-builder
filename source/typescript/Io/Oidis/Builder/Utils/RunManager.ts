/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { Terminal } from "../Connectors/Terminal.js";
import { ToolchainType } from "../Enums/ToolchainType.js";
import { IAppConfiguration } from "../Interfaces/IAppConfiguration.js";
import { IRunPlatform } from "../Interfaces/IBuildPlatform.js";
import { IRunConfig, IRunEnvironment } from "../Interfaces/IRunEnvironment.js";
import { Loader } from "../Loader.js";

export class RunManager extends BaseObject {

    public static Run($env : IRunEnvironment, $config : IRunConfig, $response : IResponse) : void {
        const env : IRunEnvironment = JsonUtils.Clone($env);
        const toolchain : ToolchainType = (<IRunPlatform>$config.platform).toolchain;
        env.args.push(...$config.args);
        switch (toolchain) {
        case ToolchainType.NODEJS:
            if (!ObjectValidator.IsEmptyOrNull($config.port)) {
                env.args = ["--inspect=" + $config.port].concat(env.args);
            }
            this.runGeneral(env, $response);
            break;
        case ToolchainType.ECLIPSE:
            this.runEclipse(env, $response);
            break;
        case ToolchainType.IDEA:
            this.runIdea(env, $response);
            break;
        default:
            this.runGeneral(env, $response);
            break;
        }
    }

    private static runGeneral($env : IRunEnvironment, $response : IResponse) : void {
        const terminal : Terminal = Loader.getInstance().getTerminal();
        if (StringUtils.StartsWith($env.cmd, "file://")) {
            // todo: replace with browser spawn to enable args passing
            terminal.Open($env.cmd);
        } else {
            terminal.Spawn($env.cmd, $env.args, $env.cwd, $response);
        }
    }

    private static runEclipse($env : IRunEnvironment, $response : IResponse) : void {
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const terminal : Terminal = Loader.getInstance().getTerminal();

        const dropinsPath : string = $env.cwd + "/dropins";
        const targetPath : string = dropinsPath + "/" + $env.executableName;
        fileSystem.Delete(dropinsPath);
        fileSystem.Copy($env.executablePath, targetPath,
            ($status : boolean) : void => {
                if ($status) {
                    terminal.Spawn($env.cmd, $env.args, $env.cwd, $response);
                } else {
                    LogIt.Error("Unable to copy plugin jar from: " + $env.executablePath + " to: " + targetPath);
                }
            });
    }

    private static runIdea($env : IRunEnvironment, $response : IResponse) : void {
        const builderConfig : IAppConfiguration = Loader.getInstance().getAppConfiguration();
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const terminal : Terminal = Loader.getInstance().getTerminal();
        const ideaPath : string = builderConfig.ideaBase + "/bin";

        terminal.Kill(ideaPath + "/idea64.exe", ($status : boolean) : void => {
            if ($status) {
                let pluginPath : string = process.env.home;
                if (ObjectValidator.IsEmptyOrNull(pluginPath)) {
                    pluginPath = process.env.homedrive + process.env.homepath;
                }
                pluginPath += "/.IdeaIC2019.1/config/plugins/" + $env.executableName;
                pluginPath = StringUtils.Replace(pluginPath, "\\", "/");
                fileSystem.Unpack($env.executablePath, <any>{
                    output  : pluginPath,
                    override: true
                }, () : void => {
                    terminal.Spawn($env.cmd, $env.args, $env.cwd, $response);
                });
            } else {
                LogIt.Error("Unable to kill current IDE process");
            }
        });
    }
}
