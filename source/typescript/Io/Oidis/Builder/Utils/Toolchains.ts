/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { Resources } from "../DAO/Resources.js";
import { ToolchainType } from "../Enums/ToolchainType.js";
import { IToolchainSettings } from "../Interfaces/IProject.js";
import { IToolchains } from "../Interfaces/IProperties.js";
import { Loader } from "../Loader.js";
import { BuildProductArgs } from "../Structures/BuildProductArgs.js";
import { BuildExecutor } from "../Tasks/BuildProcessor/BuildExecutor.js";

export class Toolchains extends BaseObject {

    private static singleton : Toolchains;

    public static getInstance() : Toolchains {
        if (ObjectValidator.IsEmptyOrNull(Toolchains.singleton)) {
            Toolchains.singleton = new Toolchains();
        }

        return Toolchains.singleton;
    }

    public static getDefaults() : IToolchains {
        return Toolchains.getInstance().getDefaults();
    }

    public static getToolchain($type? : ToolchainType) : IToolchainSettings {
        return Toolchains.getInstance().getToolchain($type);
    }

    protected getDefaults() : IToolchains {
        return Loader.getInstance().getAppProperties().toolchains;
    }

    protected getToolchain($type? : ToolchainType) : IToolchainSettings {
        const loader : Loader = Loader.getInstance();
        let toolchain : IToolchainSettings;
        if (ObjectValidator.IsEmptyOrNull($type)) {
            if (!ObjectValidator.IsEmptyOrNull(loader.getProjectConfig().build) &&
                !ObjectValidator.IsEmptyOrNull(loader.getProjectConfig().build.product)) {
                $type = loader.getProjectConfig().build.product.toolchain;
            } else {
                const args : BuildProductArgs[] = (new BuildExecutor()).getTargetProducts(loader.getProjectConfig().target);
                if (args.length > 1) {
                    LogIt.Error("Toolchain specification can not be loaded outside release process (like in selfinstall) " +
                        "for multi-platform target configuration. Please check your project configuration and rewrite it to multiple " +
                        "solutions.");
                }
                $type = args[0].Toolchain();
            }
        }

        const defaults : any = this.getDefaults();
        if (defaults.hasOwnProperty($type.toString())) {
            toolchain = this.getDefaults()[$type.toString()];
        }
        if (ObjectValidator.IsEmptyOrNull(toolchain)) {
            // TODO(mkelnar) when target platform is not explicitly defined in project (like using the same configuration
            //  as in localhost with solution imports) then the real platform is not available until dependencies-prefetch and resolve.
            //  Not sure if this needs to be resolved before install finished (currently called by EnvArgs.LoadTargetEnv() twice,
            //  first time before download-preload and second time after).
            LogIt.Warning("Can not find default toolchain settings.");
        }
        toolchain = Resources.Extend(toolchain, loader.getProjectConfig().target.toolchainSettings);

        if (!ObjectValidator.IsEmptyOrNull(toolchain)) {
            toolchain.cmakeToolchain = this.findScript(toolchain.cmakeToolchain);
            toolchain.initScript = this.findScript(toolchain.initScript);
            if (ObjectValidator.IsEmptyOrNull(toolchain.cmakeOptions)) {
                toolchain.cmakeOptions = [];
            }
        }

        if ($type === ToolchainType.PIP) {
            if (ObjectValidator.IsEmptyOrNull(toolchain)) {
                toolchain = loader.getProjectConfig().target.toolchainSettings;
                if (ObjectValidator.IsEmptyOrNull(toolchain)) {
                    toolchain = this.getDefaults()[$type.toString()];
                }
            }
            if (toolchain.version === "python2") {
                toolchain.version = "2";
            }
            if (toolchain.version === "python3") {
                toolchain.version = "3";
            }
            if (toolchain.version !== "3" && toolchain.version !== "2") {
                LogIt.Error("Unsupported Python toolchain version. Supported is only python2 or python3.");
            }
        }
        return toolchain;
    }

    protected findScript($path : string) : string {
        $path = StringUtils.Remove($path, "\"");
        if (!ObjectValidator.IsEmptyOrNull($path)) {
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (!fileSystem.Exists($path)) {
                const scripts : string[] = fileSystem.Expand([
                    Loader.getInstance().getProgramArgs().TargetBase() + "/build/target/**/" + $path,
                    Loader.getInstance().getProgramArgs().TargetBase() + "/build/**/" + $path,
                    Loader.getInstance().getProgramArgs().TargetBase() + "/build_cache/" + $path,
                    Loader.getInstance().getProgramArgs().TargetBase() + "/bin/resource/**/" + $path
                ]);
                if (scripts.length > 0) {
                    $path = scripts[0];
                } else {
                    if (StringUtils.Contains($path, "/vcvars64.bat")) {
                        LogIt.Debug("Ignoring not found script \"" + $path + "\".");
                    } else {
                        LogIt.Warning("Script \"" + $path + "\" has not been found.");
                    }
                }
            }
        } else if ($path !== "") {
            $path = "";
        }
        return $path;
    }
}
