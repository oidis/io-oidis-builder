/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { ToolchainType } from "../Enums/ToolchainType.js";
import { IToolchainSettings } from "../Interfaces/IProject.js";
import { IPythonPackage } from "../Interfaces/IPythonPackage.js";
import { Loader } from "../Loader.js";
import { Toolchains } from "./Toolchains.js";

export class PythonUtils extends BaseObject {

    public static getInterpreterPath() : string {
        const toolchain : IToolchainSettings = Toolchains.getToolchain(ToolchainType.PIP);
        let env : string = "pyenv";
        if (toolchain.version === "3") {
            env += "3";
        }
        if (!EnvironmentHelper.IsWindows()) {
            env += "/bin/python";
        } else {
            env += "/python.exe";
        }
        return Loader.getInstance().getAppProperties().projectBase + "/build_cache/" + env;
    }

    public static PackageToString($pipPackage : IPythonPackage) : string {
        const version : string = StringUtils.Replace($pipPackage.version, "*", "");
        const isVersionRangeVal : boolean = version === "" ||
            StringUtils.StartsWith($pipPackage.version, ">") ||
            StringUtils.StartsWith($pipPackage.version, "<") ||
            StringUtils.StartsWith($pipPackage.version, "=");
        return $pipPackage.name + (isVersionRangeVal ? version : "==" + version);
    }

    public static getRequiresMap($basePath : string, $pythonVersion : string) : any {
        const fileSystem : FileSystemHandler = new FileSystemHandler();
        const required : any = {};
        const configPath : string = $basePath + "/resource/configs/python" + $pythonVersion + ".config.json";
        if (fileSystem.Exists(configPath)) {
            try {
                JsonUtils.Extend(required, JSON.parse(fileSystem.Read(configPath).toString()));
            } catch (ex) {
                LogIt.Warning(ex.stack);
            }
        }
        return required;
    }

    public static getPackages($basePath : string, $requiresMap : any) : IPythonPackage[] {
        const packages : IPythonPackage[] = [];
        let pipPackage : string;
        for (pipPackage in $requiresMap) {
            if ($requiresMap.hasOwnProperty(pipPackage)) {
                if (ObjectValidator.IsObject($requiresMap[pipPackage])) {
                    packages.push({
                        installed: false,
                        location : $basePath + "/" + $requiresMap[pipPackage].location,
                        name     : pipPackage,
                        version  : $requiresMap[pipPackage].version
                    });
                } else {
                    packages.push({
                        installed: false,
                        name     : pipPackage,
                        version  : $requiresMap[pipPackage]
                    });
                }
            }
        }
        return packages;
    }
}
