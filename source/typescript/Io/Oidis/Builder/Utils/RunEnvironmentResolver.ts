/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { ToolchainType } from "../Enums/ToolchainType.js";
import { IAppConfiguration } from "../Interfaces/IAppConfiguration.js";
import { IRunPlatform } from "../Interfaces/IBuildPlatform.js";
import { IProject, IProjectRunConfig, IReleasePlatform } from "../Interfaces/IProject.js";
import { IProperties } from "../Interfaces/IProperties.js";
import { IRunConfig, IRunEnvironment } from "../Interfaces/IRunEnvironment.js";
import { Loader } from "../Loader.js";
import { BuildProductArgs } from "../Structures/BuildProductArgs.js";
import { BuildExecutor, IReleaseProperties } from "../Tasks/BuildProcessor/BuildExecutor.js";

export interface IResolveResult {
    status : boolean;
    message : string;
    value? : any;
}

export interface IResolveConfigResult extends IResolveResult {
    value? : IRunConfig;
}

export interface IResolveEnvResult extends IResolveResult {
    value? : IRunEnvironment;
}

export class RunEnvironmentResolver extends BaseObject {
    public static ResolveConfig($project : IProject, $name : string) : IResolveConfigResult {
        const defaultConf : string = "default";
        if ($name === defaultConf &&
            $project.runConfigs.hasOwnProperty(defaultConf) && ObjectValidator.IsString($project.runConfigs[defaultConf])) {
            $name = $project.runConfigs[defaultConf];
        }
        let projectRunConf : IProjectRunConfig = $project.runConfigs[$name];
        if (ObjectValidator.IsEmptyOrNull(projectRunConf)) {
            projectRunConf = $project.runConfigs[defaultConf];
        }
        const runConf : IRunConfig = JsonUtils.Clone(projectRunConf);
        const executor : BuildExecutor = new BuildExecutor();
        const releases : IReleaseProperties[] = executor.getReleases();
        let isConfMatch : boolean = false;

        const result : IResolveConfigResult = <IResolveConfigResult>{};
        if (<any>runConf.platform === "tunnel" || <any>runConf.platform === "builder") {
            isConfMatch = true;
        } else {
            const targetRelease : IReleasePlatform = <IReleasePlatform>(ObjectValidator.IsString(projectRunConf.release) ?
                {name: projectRunConf.release} : projectRunConf.release);
            const isReleaseSet : boolean = !ObjectValidator.IsEmptyOrNull(targetRelease);
            const targetPlatform : string = isReleaseSet ? targetRelease.platform : projectRunConf.platform;
            const isPlatformSet : boolean = !ObjectValidator.IsEmptyOrNull(targetPlatform);

            releases.forEach(($release : IReleaseProperties) : boolean => {
                if (!isReleaseSet || $release.name === targetRelease.name) {
                    const args : BuildProductArgs[] = executor.getTargetProducts($release.target);
                    args.forEach(($product : BuildProductArgs) : boolean => {
                        if (!isPlatformSet || $product.Value() === targetPlatform) {
                            runConf.platform = $product.getRunPlatform();
                            runConf.release = $release.name;
                            isConfMatch = true;
                        }
                        return !isConfMatch;
                    });
                }
                return !isConfMatch;
            });

            if (!isConfMatch) {
                result.message = "Invalid run configuration detected. No release/target configuration matches " +
                    (isPlatformSet ? "platform \"" + targetPlatform + "\"" : "") +
                    (isReleaseSet ? ((isPlatformSet ? " and " : "") + "release \"" + targetPlatform + "\"") : "") + ".";
            } else if (projectRunConf === $project.runConfigs[defaultConf]) {
                if ($project.runConfigs.length === 1) {
                    result.message = "Property runConfig is missing from package.conf.json or private.conf.json. " +
                        "Using default configuration.";
                } else {
                    result.message = "Task option run-config not set. Using default configuration.";
                }
            } else if (!isPlatformSet && !isReleaseSet) {
                result.message = "No platform or release type has been set inside run configuration.";
            }

            if (isConfMatch) {
                result.message += "\nResolved run configuration with platform \"" +
                    runConf.platform.value + "\"" + (ObjectValidator.IsEmptyOrNull(runConf.release) ?
                        "" : " and release type \"" + runConf.release + "\"") + ".";
            }
        }

        if (isConfMatch) {
            result.status = true;
            result.value = runConf;
        }

        return result;
    }

    public static ResolveEnvironment($platform : IRunPlatform, $release : string) : IResolveEnvResult {
        const properties : IProperties = Loader.getInstance().getAppProperties();
        const builderConfig : IAppConfiguration = Loader.getInstance().getAppConfiguration();
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();

        let pathSearch : string[] = [];
        let execResult : IResolveResult;
        let result : IResolveEnvResult = <IResolveEnvResult>{};
        result.status = true;
        result.value = <IRunEnvironment>{};
        switch ($platform.toolchain) {
        case(ToolchainType.NODEJS):
            pathSearch = fileSystem.Expand(properties.projectBase + "/dependencies/nodejs/+(node.exe|node)");
            if (pathSearch.length === 0) {
                result.message = "Project's Node.js dependencies are missing. Please validate that project " +
                    "was built successfully.";
                result.status = false;
            } else {
                execResult = RunEnvironmentResolver.resolveExecutablePath(properties.projectBase, $platform, $release);
                if (execResult.status) {
                    result.value.executablePath = execResult.value;
                    result.value.cmd = pathSearch[0];
                    result.value.cwd = properties.projectBase;
                    result.value.args = [result.value.executablePath];
                } else {
                    result = execResult;
                }
            }
            break;
        case(ToolchainType.ECLIPSE):
            const eclipsePath : string = builderConfig.eclipseBase; // eslint-disable-line no-case-declarations
            if (!fileSystem.Exists(eclipsePath)) {
                result.message = "Eclipse doesn't exist at \"" + eclipsePath + "\". Please validate that project " +
                    "was built successfully.";
                result.status = false;
            } else {
                execResult = RunEnvironmentResolver.resolveExecutablePath(properties.projectBase, $platform, $release);
                if (execResult.status) {
                    result.value.executablePath = execResult.value;
                    result.value.cmd = "eclipse.exe";
                    result.value.cwd = eclipsePath;
                    result.value.args = ["-clean", "-debug"];
                } else {
                    result = execResult;
                }
            }
            break;
        case(ToolchainType.IDEA):
            const ideaPath : string = builderConfig.ideaBase + "/bin"; // eslint-disable-line no-case-declarations
            if (!fileSystem.Exists(ideaPath)) {
                result.message = "Idea doesn't exist at \"" + ideaPath + "\". Please validate that project " +
                    "was built successfully.";
                result.status = false;
            } else {
                execResult = RunEnvironmentResolver.resolveExecutablePath(properties.projectBase, $platform, $release);
                if (execResult.status) {
                    result.value.executablePath = execResult.value;
                    result.value.cmd = "idea64.exe";
                    result.value.cwd = ideaPath;
                    result.value.args = [];
                } else {
                    result = execResult;
                }
            }
            break;
        default:
            execResult = RunEnvironmentResolver.resolveExecutablePath(properties.projectBase, $platform, $release);
            if (execResult.value) {
                result.value.executablePath = execResult.value;
                result.value.cmd = result.value.executablePath;
                result.value.args = [];
            } else {
                result = execResult;
            }
            break;
        }
        if (result.status) {
            const parts : string[] = result.value.executablePath.split("/");
            result.value.executableName = parts[parts.length - 1];
            result.message = "Environment resolved successfully.";
        }
        return result;
    }

    private static resolveExecutablePath($projectBase : string, $platform : IRunPlatform, $release : string = "") : IResolveResult {
        const properties : IProperties = Loader.getInstance().getAppProperties();
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const executor : BuildExecutor = new BuildExecutor();
        const releases : IReleaseProperties[] = executor.getReleases();

        let path : string = $projectBase + "/build";
        if (releases.length !== 1) {
            path += "/releases";
            if ($release !== "") {
                path += "/" + $release;
            }
            if ($platform.value !== "") {
                path += "/" + $platform.value;
            }
        }
        path += "/target/";

        const result : any = {status: true, message: "", value: null};

        let pathSearch : string[] = [];
        const executablePath : string = null;
        if (fileSystem.Exists(path)) {
            switch ($platform.toolchain) {
            case ToolchainType.NODEJS:
                if (fileSystem.Exists(path + "resource/javascript/loader.min.js")) {
                    result.value = path + "resource/javascript/loader.min.js";
                } else {
                    result.message = "Loader " + executablePath + " doesn't exist. Please validate that project " +
                        "was built successfully.";
                    result.status = false;
                }
                break;
            case ToolchainType.IDEA:
                pathSearch = fileSystem.Expand(path + properties.packageName + ".jar");
                if (pathSearch.length > 0) {
                    result.value = pathSearch[0];
                } else {
                    result.message = "Idea package at path: " + path + "doesn't exist. Please validate that project " +
                        "was built successfully.";
                    result.status = false;
                }
                break;
            case ToolchainType.ECLIPSE:
                pathSearch = fileSystem.Expand(path + properties.packageName + ".zip");
                // pathSearch = fileSystem.Expand(path + "*.zip");
                if (pathSearch.length > 0) {
                    result.value = pathSearch[0];
                } else {
                    result.message = "Eclipse package at path: " + path + "doesn't exist. Please validate that project " +
                        "was built successfully.";
                    result.status = false;
                }
                break;
            default:
                pathSearch = fileSystem.Expand(path + "WuiLauncher.exe");
                if (pathSearch.length === 0) {
                    pathSearch = fileSystem.Expand(path + "*.exe");
                    if (pathSearch.length === 0) {
                        // todo: perhaps we could detect browser, and start remote debug on it if needed -> would allow us to control
                        // stuff like web security disabling, etc., without developer needing to set it up inside IDE, would still need
                        // to attach though
                        const pagePath : string = path + "index.html";
                        if (fileSystem.Exists(pagePath)) {
                            result.value = "file://" + pagePath;
                        } else {
                            result.message = "Neither page or executable at path: " + path + " exists. Please validate that the " +
                                "project was built successfully.";
                            result.status = false;
                        }
                    } else {
                        result.value = pathSearch[0];
                    }
                } else {
                    result.value = pathSearch[0];
                }
            }
        } else {
            result.message = "Project executable doesn't exist.";
            result.status = false;
        }
        return result;
    }
}

// generated-code-start
export const IResolveResult = globalThis.RegisterInterface(["status", "message", "value"]);
export const IResolveConfigResult = globalThis.RegisterInterface(["value"], <any>IResolveResult);
export const IResolveEnvResult = globalThis.RegisterInterface(["value"], <any>IResolveResult);
// generated-code-end
