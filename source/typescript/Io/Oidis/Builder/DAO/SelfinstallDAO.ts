/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import {ColorType} from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { InstallationRecipeDAO } from "@io-oidis-services/Io/Oidis/Services/DAO/InstallationRecipeDAO.js";
import { IInstallationPackage } from "@io-oidis-services/Io/Oidis/Services/Interfaces/DAO/IBaseInstallationRecipe.js";
import { ISelfinstallRecipe } from "../Interfaces/ISelfinstallRecipe.js";
import { Loader } from "../Loader.js";
import { Resources } from "./Resources.js";

export class SelfinstallDAO extends InstallationRecipeDAO {
    public static defaultConfigurationPath : string =
        "resource/data/Io/Oidis/Builder/Configuration/Selfinstall.jsonp";

    protected static getDaoInterfaceClassName($interfaceName : string) : any {
        return SelfinstallDAO;
    }

    public getPackage($name : string) : IInstallationPackage {
        return super.getPackage($name);
    }

    public ConfigurationLibrary($value? : any) : ISelfinstallRecipe {
        const library : ISelfinstallRecipe = <ISelfinstallRecipe>super.ConfigurationLibrary($value);
        library.setColor = ColorType.setColor;
        return library;
    }

    protected getResourcesHandler() : any {
        return Resources;
    }

    protected getFileSystem() : any {
        return Loader.getInstance().getFileSystemHandler();
    }

    protected getTerminal() : any {
        return Loader.getInstance().getTerminal();
    }
}
