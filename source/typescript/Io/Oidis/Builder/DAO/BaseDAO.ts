/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseDAO as Parent } from "@io-oidis-services/Io/Oidis/Services/DAO/BaseDAO.js";
import { IBaseConfiguration } from "@io-oidis-services/Io/Oidis/Services/Interfaces/DAO/IBaseConfiguration.js";
import { Resources } from "./Resources.js";

export class BaseDAO extends Parent {

    protected static getDaoInterfaceClassName($interfaceName : string) : any {
        return BaseDAO;
    }

    public OnErrorHandler($handler : ($error : ErrorEvent, $path? : string) => void) : void {
        this.setErrorHandler($handler);
    }

    public setConfigurationPath($value : string) : void {
        if (StringUtils.Contains($value, "/dependencies/")) {
            Resources.setCWD($value.substring(0, $value.indexOf("/", $value.indexOf("/dependencies/") + 14)));
        }
        super.setConfigurationPath($value);
    }

    public Load($language : LanguageType, $asyncHandler : () => void, $force : boolean = false) : void {
        super.Load($language, () : void => {
            const config : IBaseConfiguration = this.getStaticConfiguration();
            const solution : any = {};
            if (this.getConfigurationInterface() === "IProjectSolution") {
                for (const importName in config.imports) {
                    if (config.imports.hasOwnProperty(importName)) {
                        Resources.Extend(solution, this.getImportedDAO(importName).getStaticConfiguration());
                    }
                }
            }
            Resources.Extend(solution, config);
            (<any>BaseDAO).staticConfiguration.getItem(this.getDaoDataSource()).Add(solution, $language);
            $asyncHandler();
        }, $force);
    }

    public async LoadAsync($options? : IDAOLoadOptions) : Promise<any> {
        $options = JsonUtils.Extend({
            force   : false,
            language: LanguageType.EN,
            path    : null
        }, $options);
        return new Promise<any>(($resolve, $reject) : void => {
            this.OnErrorHandler(($error : ErrorEvent, $path? : string) : void => {
                $reject({error: $error, path: $path});
            });
            if (!ObjectValidator.IsEmptyOrNull($options.path)) {
                this.setConfigurationPath($options.path);
            }
            try {
                this.Load($options.language, () : void => {
                    $resolve(this.getStaticConfiguration());
                }, $options.force);
            } catch (ex) {
                $reject(ex);
            }
        });
    }

    protected getResourcesHandler() : any {
        return Resources;
    }
}

export interface IDAOLoadOptions {
    force? : boolean;
    language? : LanguageType;
    path? : string;
}

// generated-code-start
export const IDAOLoadOptions = globalThis.RegisterInterface(["force", "language", "path"]);
// generated-code-end
