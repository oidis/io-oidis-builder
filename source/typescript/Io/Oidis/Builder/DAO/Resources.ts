/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Resources as Parent } from "@io-oidis-services/Io/Oidis/Services/DAO/Resources.js";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { StringReplaceType } from "../Enums/StringReplaceType.js";
import { Loader } from "../Loader.js";
import { StringReplace } from "../Tasks/Utils/StringReplace.js";

export class Resources extends Parent {
    private static cwd : string;

    public static setCWD($value : string) : void {
        if (ObjectValidator.IsEmptyOrNull($value)) {
            this.cwd = Loader.getInstance().getAppProperties().projectBase;
        } else {
            this.cwd = $value;
        }
    }

    protected static filterPath($value : string) : string {
        $value = StringReplace.Content($value, StringReplaceType.VARIABLES);
        const values : string[] = StringUtils.Split($value, "||");
        const instance : Loader = Loader.getInstance();
        const fileSystem : FileSystemHandler = instance.getFileSystemHandler();

        const nextValue : any = ($index : number = 0) : void => {
            if ($index < values.length) {
                let value : string = values[$index].trim();
                if (!StringUtils.Contains(value, "http://", "https://", "file://", ":/") &&
                    !StringUtils.StartsWith(value, "/")) {
                    if (StringUtils.Contains(value, "?")) {
                        value = StringUtils.Substring(value, 0, StringUtils.IndexOf(value, "?"));
                    }
                    const projectPath : string = this.cwd + "/" + value;
                    if (!fileSystem.Exists(projectPath)) {
                        const projectPath : string = instance.getAppProperties().projectBase + "/" + value;
                        if (!fileSystem.Exists(projectPath)) {
                            $value = instance.getAppProperties().binBase + "/" + value;
                            if (!fileSystem.Exists($value) &&
                                StringUtils.Contains($value, "/dependencies/") &&
                                !fileSystem.Exists(instance.getAppProperties().projectBase + "/dependencies")) {
                                nextValue($index + 1);
                            }
                        } else {
                            $value = projectPath;
                        }
                    } else {
                        $value = projectPath;
                    }
                } else {
                    $value = value;
                }
            }
        };
        nextValue();
        return $value;
    }
}
