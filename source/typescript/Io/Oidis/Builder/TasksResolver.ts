/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IBaseTask } from "./Interfaces/IBaseTask.js";
import { IProjectModule } from "./Interfaces/IProject.js";
import { Loader } from "./Loader.js";
import { ModulesResolver } from "./ModulesResolver.js";
import { BaseTask } from "./Primitives/BaseTask.js";

export class TasksResolver extends BaseObject {
    private readonly tasks : ArrayList<IBaseTask>;
    private readonly aliases : any;
    private executed : string[];
    private currentTaskName : string;

    constructor() {
        super();
        this.tasks = new ArrayList<IBaseTask>();
        this.aliases = {};
        const reflection : Reflection = Reflection.getInstance();
        reflection.getAllClasses().forEach(($className : string) : void => {
            if (reflection.ClassHasInterface($className, IBaseTask) && $className !== BaseTask.ClassName()) {
                const classObject : any = reflection.getClass($className);
                const task : IBaseTask = new classObject();
                task.setOwner(this);
                this.tasks.Add(task, task.Name());
            }
        });
        this.executed = [];
        this.currentTaskName = "Undefined";
        const tasks : string[] = this.getAll();
        ModulesResolver.getInstance().getAll().forEach(($name) : void => {
            if (tasks.indexOf($name) === -1 || Loader.getInstance().getProjectConfig().modules[$name].overrideTask) {
                this.aliases[$name] = "module:" + $name;
            } else {
                LogIt.Warning("Module alias for \"" + $name + "\" can not be created.");
            }
        });
    }

    public async Resolve($tasks : string | string[]) : Promise<void> {
        if (ObjectValidator.IsString($tasks)) {
            $tasks = [<string>$tasks];
        }
        const tasksMetadata : any[] = [];
        const modulesResolver : ModulesResolver = ModulesResolver.getInstance();
        (<string[]>$tasks).forEach(($task : string) : void => {
            const events : any = modulesResolver.getTaskEvents($task);
            events.before.forEach(($event : string) : void => {
                tasksMetadata.push({
                    name : $event,
                    owner: "before:" + events.owner
                });
            });
            tasksMetadata.push({
                name : events.owner,
                owner: events.owner
            });
            events.after.forEach(($event : string) : void => {
                tasksMetadata.push({
                    name : $event,
                    owner: "after:" + events.owner
                });
            });
        });
        const finalSet : any[] = [];
        const modules : IProjectModule[] = Loader.getInstance().getProjectConfig().modules;
        tasksMetadata.forEach(($task : any) : void => {
            let alias : string = $task.name;
            let option : string = "";
            if (StringUtils.Contains($task.name, ":")) {
                option = StringUtils.Substring($task.name, StringUtils.IndexOf($task.name, ":") + 1);
                alias = StringUtils.Substring($task.name, 0, StringUtils.IndexOf($task.name, ":"));
            }
            if (this.aliases.hasOwnProperty(alias)) {
                if (!ObjectValidator.IsEmptyOrNull(option)) {
                    modules[alias].args = [option];
                }
                $task.name = this.aliases[alias];
            }
            finalSet.push($task);
        });
        for await (const taskDescriptor of finalSet) {
            let task : string = taskDescriptor.name;
            let option : string;
            if (StringUtils.Contains(task, ":")) {
                option = StringUtils.Substring(task, StringUtils.IndexOf(task, ":") + 1);
                task = StringUtils.Substring(task, 0, StringUtils.IndexOf(task, ":"));
            }
            if (this.tasks.KeyExists(task)) {
                try {
                    this.currentTaskName = taskDescriptor.name;
                    modulesResolver.setOwner(taskDescriptor.owner);
                    LogIt.Info((<any>("\nRunning \"" + this.currentTaskName + "\" task")).underline);
                    this.executed.push(this.currentTaskName);
                    await  this.tasks.getItem(task).Process(option);
                } catch (ex) {
                    LogIt.Error("Task \"" + task + "\" has failed.", ex);
                }
            } else {
                LogIt.Error("Required task \"" + task + "\" does not exist.");
            }
        }
    }

    public getAll() : string[] {
        return <string[]>this.tasks.getKeys();
    }

    public getReport() : void {
        LogIt.Debug("List of all executed tasks: {0}", this.executed);
        this.executed = [];
    }

    public getCurrentTaskName() : string {
        return this.currentTaskName;
    }
}
