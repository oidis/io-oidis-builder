/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EnvironmentArgs as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/EnvironmentArgs.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { ISchemaError, SchemaValidator } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/SchemaValidator.js";
import { IBaseConfiguration } from "@io-oidis-services/Io/Oidis/Services/Interfaces/DAO/IBaseConfiguration.js";
import stripJsonComments from "@nodejs/strip-json-comments/index.js";
import { FileSystemHandler } from "./Connectors/FileSystemHandler.js";
import { BaseDAO } from "./DAO/BaseDAO.js";
import { Resources } from "./DAO/Resources.js";
import { CliTaskType } from "./Enums/CliTaskType.js";
import { DeployServerType } from "./Enums/DeployServerType.js";
import { OSType } from "./Enums/OSType.js";
import { StringReplaceType } from "./Enums/StringReplaceType.js";
import { ToolchainType } from "./Enums/ToolchainType.js";
import { IAppConfiguration, IBuildArgs, ITestConfig } from "./Interfaces/IAppConfiguration.js";
import { IDeployServers, IDeployServersCredentials } from "./Interfaces/IDeployServers.js";
import {
    IProject, IProjectDependency, IProjectDependencyLocation,
    IProjectDependencyLocationArch,
    IProjectDependencyLocationPath,
    IProjectDependencyScriptConfig, IProjectTargetResourceFiles, IProjectTargetResources, IToolchainSettings
} from "./Interfaces/IProject.js";
import { IProperties } from "./Interfaces/IProperties.js";
import { Loader } from "./Loader.js";
import { ProgramArgs } from "./Structures/ProgramArgs.js";
import { StringReplace } from "./Tasks/Utils/StringReplace.js";
import { Toolchains } from "./Utils/Toolchains.js";

export class EnvironmentArgs extends Parent {
    private project : IProject;
    private properties : IProperties;
    private programArgs : ProgramArgs;
    private buildArgs : IBuildArgs;
    private testConfig : ITestConfig;
    private fileSystem : FileSystemHandler;
    private configChanged : boolean;
    private metadata : any;
    private isInitialized : boolean;
    private readonly buildTime : Date = new Date();

    constructor() {
        super();
        this.isInitialized = false;
        this.buildTime = new Date();
    }

    public getProjectConfig() : IAppConfiguration {
        return <IAppConfiguration>super.getProjectConfig();
    }

    public getTargetConfig() : IProject {
        return this.project;
    }

    public getAppProperties() : IProperties {
        return this.properties;
    }

    public getBuildArgs() : IBuildArgs {
        return this.buildArgs;
    }

    public getTestConfig() : ITestConfig {
        return this.testConfig;
    }

    public getRealProjectPath($path : string) : string {
        if ($path !== "..") {
            $path = StringUtils.Replace($path, "\\", "/");
            const workspace : any = /\[WORKSPACE_(.*?)\]\//gm.exec($path);
            const appConfig : IAppConfiguration = this.getProjectConfig();
            if (workspace !== null && workspace.length === 2 &&
                appConfig.workspaces.hasOwnProperty(workspace[1])) {
                $path = StringUtils.Replace($path,
                    "[WORKSPACE_" + workspace[1] + "]/", appConfig.workspaces[workspace[1]]);
            } else {
                const cloudspace : any = /\[CLOUDBASE_(.*?)\]\//gm.exec($path);
                if (cloudspace !== null && cloudspace.length === 2) {
                    let syncBase : string = appConfig.cloud.syncBase;
                    if (!StringUtils.EndsWith(syncBase, "/")) {
                        syncBase += "/";
                    }
                    syncBase += cloudspace[1] + "/";
                    if (!this.fileSystem.Exists(syncBase)) {
                        this.fileSystem.CreateDirectory(syncBase);
                    }
                    $path = StringUtils.Replace($path, "[CLOUDBASE_" + cloudspace[1] + "]/", syncBase);
                }
            }
        }
        return $path;
    }

    public getConfigChanged() : boolean {
        return this.configChanged;
    }

    public UpdateStats() : void {
        PersistenceFactory.getPersistence(this.getClassName()).Variable("Stats", this.properties.stats);
    }

    public ResolveDependencyPath($path : string | IProjectDependencyLocationPath) : string {
        let path : string;
        if (ObjectValidator.IsString($path)) {
            path = <string>$path;
        } else {
            let pathWithArch : string | IProjectDependencyLocationArch = "";
            if (EnvironmentHelper.IsWindows()) {
                pathWithArch = (<IProjectDependencyLocationPath>$path).win;
            } else if (EnvironmentHelper.IsMac()) {
                pathWithArch = (<IProjectDependencyLocationPath>$path).mac;
            } else {
                pathWithArch = (<IProjectDependencyLocationPath>$path).linux;
            }
            if (!ObjectValidator.IsEmptyOrNull(pathWithArch)) {
                if (ObjectValidator.IsString(pathWithArch)) {
                    path = <string>pathWithArch;
                } else {
                    if (EnvironmentHelper.Is64bit()) {
                        path = (<IProjectDependencyLocationArch>pathWithArch).x64;
                    } else {
                        path = (<IProjectDependencyLocationArch>pathWithArch).x32;
                    }
                }
            }
            if (ObjectValidator.IsEmptyOrNull($path)) {
                LogIt.Error("Unmet dependency path based on builder platform and architecture. " +
                    "Specify platform specific paths for win, linux and mac as location.path attribute. " +
                    "Optionally can be specified x86 or x64 architecture.");
            }
        }
        return path;
    }

    public LoadProjectEnvironment($callback : () => void) : void {
        const loader : Loader = Loader.getInstance();
        this.fileSystem = loader.getFileSystemHandler();
        this.programArgs = loader.getProgramArgs();
        const appConfig : IAppConfiguration = this.getProjectConfig();
        this.createProject();

        if (ObjectValidator.IsEmptyOrNull(appConfig.workspaces)) {
            appConfig.workspaces = <any>{};
        }
        let workspace : string;
        for (workspace in appConfig.workspaces) {
            if (appConfig.workspaces.hasOwnProperty(workspace)) {
                let path : string = appConfig.workspaces[workspace];
                if (!ObjectValidator.IsEmptyOrNull(path)) {
                    path = StringUtils.Replace(path, "\\", "/");
                    if (!StringUtils.EndsWith(path, "/")) {
                        path += "/";
                    }
                } else {
                    path = "";
                }
                appConfig.workspaces[workspace] = path;
            }
        }
        if (ObjectValidator.IsEmptyOrNull(appConfig.cloud)) {
            appConfig.cloud = <any>{};
        }
        if (ObjectValidator.IsEmptyOrNull(appConfig.cloud.syncBase)) {
            appConfig.cloud.syncBase = this.programArgs.AppDataPath() + "/sync";
        }
        if (ObjectValidator.IsEmptyOrNull(appConfig.hub)) {
            appConfig.hub = <any>{};
        }
        if (ObjectValidator.IsEmptyOrNull(appConfig.hub.user)) {
            appConfig.hub.user = "unregistered";
        }

        loader.setAnonymizer({
            targetBase: this.getTargetBase()
        });

        if (this.programArgs.IsForwardedTask()) {
            Echo.Println("Global chain identifier: " + this.programArgs.ChainId());
        }

        Resources.Extend(this.project, appConfig);
        this.createProperties();
        this.processConfig();
        $callback();
    }

    public async LoadTargetEnvironment() : Promise<IEnvironmentLoadResponse> {
        if (!this.programArgs.getOptions().noTarget) {
            Resources.setCWD(null);
            const targetBase : string = this.getTargetBase();
            if (!this.isInitialized) {
                this.isInitialized = true;
                LogIt.Info("Processing Oidis Builder task at: \"" + targetBase + "\"");
            }
            this.createProject();

            const sourceRoot : string = "/source/typescript/";
            if (this.fileSystem.Exists(targetBase + sourceRoot)) {
                const sources : string[] = this.fileSystem.Expand([
                    targetBase + sourceRoot + "*/*", targetBase + "/dependencies/*" + sourceRoot + "*/*"
                ]);
                sources.forEach(($path : string) : void => {
                    const namespace = StringUtils.Replace(
                        StringUtils.Substring($path, StringUtils.IndexOf($path, sourceRoot) + StringUtils.Length(sourceRoot)),
                        "/", ".");
                    if (this.project.namespaces.indexOf(namespace) === -1) {
                        this.project.namespaces.push(namespace);
                    }
                });
            }

            const packageResult : IProjectConfigLoadResponse = await this.LoadPackageConf();
            if (ObjectValidator.IsEmptyOrNull(packageResult.data)) {
                LogIt.Error("Target folder has not been detected as project suitable for Oidis Builder. " +
                    "Validate if package.conf.jsonp is at folder root and that its content is not corrupted.");
            }
            Resources.Extend(this.project, this.checkConfig(this.normalizeConfig(packageResult.data)));

            const privateConf : IProject = await this.loadPrivateConf();
            if (privateConf.hasOwnProperty("solutionsLocation")) {
                this.project.solutionsLocation = privateConf.solutionsLocation;
            }
            if (ObjectValidator.IsEmptyOrNull(this.project.solutionsLocation)) {
                this.project.solutionsLocation = this.project.deploy.server.dev.location;
            }
            if (privateConf.hasOwnProperty("solutions")) {
                Resources.Extend(this.project.solutions, privateConf.solutions);
            }
            const solutionResult : IProjectConfigLoadResponse = await this.loadSolution();
            const validateAndMerge : any = () : void => {
                if (privateConf.hasOwnProperty("dependencies") && !this.project.hasOwnProperty("dependencies")) {
                    LogIt.Error("Dependencies can not be overridden by private.conf.json, " +
                        "because they are not specified by package.conf.json file.");
                }
                this.properties.serverConfig = JsonUtils.Clone(this.project);
                Resources.Extend(this.project, this.normalizeConfig(privateConf));
                this.loadTestConfig();
            };
            if (packageResult.status && solutionResult.status) {
                Resources.Extend(this.project, this.checkConfig(this.normalizeConfig(solutionResult.data)));
                validateAndMerge();
                const schema : any = await this.loadSchema();
                this.checkConfig(this.project, schema);
                this.processConfig(schema);

                const currentConfig : string = StringUtils.getSha1(
                    JsonUtils.ToString(this.project.solutions) +
                    JsonUtils.ToString(this.project.solutionsLocation) +
                    JsonUtils.ToString(this.project.dependencies) +
                    JsonUtils.ToString(this.project.releases) +
                    JsonUtils.ToString(this.project.target.platforms) +
                    JsonUtils.ToString(this.project.target.toolchainSettings)
                );

                this.configChanged = false;
                const metadataPath : string = this.properties.projectBase + "/build_cache/metadata.json";
                this.metadata = {lastProjectConfig: ""};
                if (this.fileSystem.Exists(metadataPath)) {
                    try {
                        const data = this.fileSystem.Read(metadataPath).toString();
                        if (!ObjectValidator.IsEmptyOrNull(data) && (data.length > 1)) {
                            JsonUtils.Extend(this.metadata, JSON.parse(data));
                        }
                    } catch (ex) {
                        LogIt.Warning("Failed to parse build metadata from: " + metadataPath + "\n" + ex.stack);
                    }
                }
                if (!ObjectValidator.IsEmptyOrNull(this.metadata.lastProjectConfig)) {
                    this.configChanged = currentConfig !== this.metadata.lastProjectConfig;
                }
                this.metadata.lastProjectConfig = currentConfig;

                return {status: true};
            } else {
                validateAndMerge();
                this.processConfig();
                if (!packageResult.status) {
                    solutionResult.path = packageResult.path;
                }
                return {status: false, path: StringReplace.Content(solutionResult.path, StringReplaceType.VARIABLES)};
            }
        } else {
            if (!this.isInitialized) {
                this.isInitialized = true;
                LogIt.Info("Processing Oidis Builder task without target.");
            }
            return {status: true};
        }
    }

    public async LoadPackageConf($path? : string) : Promise<IProjectConfigLoadResponse> {
        let projectConfPath : string = this.getTargetBase() + "/package.conf.jsonp";
        if (!ObjectValidator.IsEmptyOrNull($path)) {
            projectConfPath = $path;
        }
        let isJson : boolean = StringUtils.EndsWith(projectConfPath, ".json");
        if (!this.fileSystem.Exists(projectConfPath) && !isJson) {
            projectConfPath = projectConfPath.slice(0, -1);
            isJson = true;
        }

        if (!this.fileSystem.Exists(projectConfPath)) {
            if (!this.programArgs.getOptions().noTarget && ObjectValidator.IsEmptyOrNull($path)) {
                LogIt.Error(this.getUnknownFolderMessage());
            } else {
                return {
                    data  : <any>{},
                    path  : projectConfPath,
                    status: false
                };
            }
        } else {
            if (isJson) {
                try {
                    return {
                        data  : JSON.parse(this.fileSystem.Read(projectConfPath).toString()),
                        path  : projectConfPath,
                        status: true
                    };
                } catch (ex) {
                    LogIt.Warning("Failed to load config from: " + projectConfPath + "\n" + ex.stack);
                    return {
                        data  : <any>{},
                        path  : projectConfPath,
                        status: false
                    };
                }
            } else {
                const loader : BaseDAO = new BaseDAO();
                try {
                    return {
                        data  : await loader.LoadAsync({path: projectConfPath, force: true}),
                        path  : projectConfPath,
                        status: true
                    };
                } catch (error) {
                    LogIt.Warning("Failed to load config from: " + projectConfPath);
                    return {data: <any>loader.getStaticConfiguration(), status: false, path: error.path};
                }
            }
        }
    }

    public UpdateMetadata() : void {
        if (!ObjectValidator.IsEmptyOrNull(this.metadata)) {
            this.fileSystem.Write(this.properties.projectBase + "/build_cache/metadata.json", JSON.stringify(this.metadata));
        }
    }

    public getTargetBase() : string {
        let targetBase : string = this.programArgs.TargetBase(this.fileSystem.NormalizePath(
            this.getRealProjectPath(this.programArgs.TargetBase())));
        if (targetBase === this.programArgs.BinBase()) {
            targetBase = this.programArgs.TargetBase(this.fileSystem.NormalizePath(process.cwd()));
        }
        this.getTargetBase = () : string => {
            return targetBase;
        };
        return targetBase;
    }

    protected getUnknownFolderMessage() : string {
        return "Target folder \"" + this.getTargetBase() + "\" has not been recognized as Oidis Project root";
    }

    protected async loadSolution() : Promise<IProjectConfigLoadResponse> {
        let solution : string = StringUtils.Replace(this.programArgs.getOptions().solution, "\\", "/");
        if (solution === "default") {
            solution = this.project.solutions[solution];
        }
        if (!StringUtils.Contains(solution, "/") && this.project.solutions.hasOwnProperty(solution)) {
            solution = this.project.solutions[solution];
        }
        let output : IProjectConfigLoadResponse = {data: <any>{}, status: true};
        if (!ObjectValidator.IsEmptyOrNull(solution)) {
            solution = StringReplace.Content(solution, StringReplaceType.VARIABLES);
            const targetBase : string = this.getTargetBase();
            const solutions : string[] = StringUtils.Split(solution, "||");
            let index : number = 0;
            for await (let solution of solutions) {
                if (index < solutions.length) {
                    solution = solution.trim();
                    if (!StringUtils.Contains(solution, "http://", "https://", "file://", ":/") &&
                        !StringUtils.StartsWith(solution, "/")) {
                        solution = targetBase + "/" + solution;
                    }
                    if (StringUtils.Contains(solution, "/dependencies/") &&
                        !this.fileSystem.Exists(targetBase + "/dependencies")) {
                        index++;
                    } else {
                        if (StringUtils.Contains(solution, "http://", "https://") || this.fileSystem.Exists(solution)) {
                            LogIt.Debug("> load config from: " + solution);
                            const solutionConf : BaseDAO = new BaseDAO();
                            try {
                                const data : IBaseConfiguration = await solutionConf.LoadAsync({path: solution, force: true});
                                if (!ObjectValidator.IsEmptyOrNull(data.version)) {
                                    delete data.version;
                                }
                                output = {data: <any>data, status: true};
                                break;
                            } catch (error) {
                                if (!ObjectValidator.IsEmptyOrNull(error.path)) {
                                    solution = error.path;
                                }
                                output = {data: <any>{}, status: false, path: solution};
                                break;
                            }
                        } else {
                            output = {data: <any>{}, status: false, path: solution};
                            break;
                        }
                    }
                } else {
                    output = {data: <any>{}, status: false, path: solutions.length > 1 ? JSON.stringify(solutions) : solutions[0]};
                    break;
                }
            }
        }
        return output;
    }

    private async loadPrivateConf() : Promise<IProject> {
        const appConfig : IAppConfiguration = this.getProjectConfig();
        const targetBase : string = this.getTargetBase();

        let privateConfPath : string = targetBase + "/private.conf.jsonp";
        let isJson : boolean = false;
        if (!this.fileSystem.Exists(privateConfPath)) {
            privateConfPath = privateConfPath.slice(0, -1);
            isJson = true;
        }
        if (this.fileSystem.Exists(privateConfPath)) {
            const resolveConfig : any = ($config : any) : any => {
                if (ObjectValidator.IsEmptyOrNull($config)) {
                    return $config;
                }
                let data : string = JSON.stringify($config);
                const symbols : any = appConfig.symbols;
                if (ObjectValidator.IsSet(symbols) && ObjectValidator.IsObject(symbols)) {
                    for (const item in symbols) {
                        if (symbols.hasOwnProperty(item)) {
                            data = StringUtils.Replace(data, "<? " + item + " ?>", symbols[item]);
                        }
                    }
                }
                return JSON.parse(StringReplace.Content(data, StringReplaceType.VARIABLES));
            };
            if (isJson) {
                try {
                    return resolveConfig(JSON.parse(stripJsonComments(this.fileSystem.Read(privateConfPath).toString())));
                } catch (ex) {
                    LogIt.Warning("Failed to load config from: " + privateConfPath + "\n" + ex.stack);
                    return <any>{};
                }
            } else {
                const result : IProjectConfigLoadResponse = await this.LoadPackageConf(privateConfPath);
                if (result.status) {
                    return resolveConfig(result.data);
                } else {
                    LogIt.Warning("Failed to load config from: " + privateConfPath);
                    return <any>{};
                }
            }
        } else {
            return <any>{};
        }
    }

    private loadTestConfig() : void {
        this.testConfig = {
            environment: {
                cwd : "",
                path: ""
            }
        };

        let testConfigSubDir : string = "";
        if (EnvironmentHelper.IsWindows()) {
            testConfigSubDir = "Win";
        } else if (EnvironmentHelper.IsLinux()) {
            testConfigSubDir = "Linux";
        } else if (EnvironmentHelper.IsMac()) {
            testConfigSubDir = "Mac";
        }
        const testConfigs : string[] = this.fileSystem.Expand(
            this.properties.projectBase + "/test/resource/config/**/" + testConfigSubDir + "/test.config.json"
        );
        if (testConfigs.length === 1) {
            Resources.Extend(this.testConfig, JSON.parse(this.fileSystem.Read(testConfigs[0]).toString()));
        }
    }

    private checkConfig($data : IProject, $schema? : any) : IProject {
        if (ObjectValidator.IsEmptyOrNull($data)) {
            return $data;
        }

        if (!ObjectValidator.IsEmptyOrNull($schema)) {
            if (!this.programArgs.getOptions().noTarget) {
                const validator : SchemaValidator = new SchemaValidator();
                validator.Schema($schema, {
                    IProjectDependency: () : boolean => {
                        return true;
                    }
                });
                const configErrors : ISchemaError[] = validator.Validate($data);
                if (!ObjectValidator.IsEmptyOrNull(configErrors)) {
                    LogIt.Warning("Schema error:\n" + validator.ErrorsToString(configErrors));
                    LogIt.Warning("" +
                        "NOTE: schema error may be caused by:\n" +
                        "- incorrectly downloaded or configured project dependencies,\n" +
                        "- manual edit of dependencies schemas,\n" +
                        "- forgotten project configuration in private.conf.json or\n" +
                        "- by outdated builder.\n\n" +
                        "Please check all possible root causes or do the manual delete of dependencies folder " +
                        "and run project install again.\n");

                    LogIt.Error("Invalid project configuration has been detected.");
                } else {
                    LogIt.Info("Configuration has been loaded without issues.");
                }
            }
        } else {
            const path : any = require("path");
            if ($data.hasOwnProperty("dependencies")) {
                const url : any = require("url");

                const fail : any = ($key : string) : void => {
                    LogIt.Error("It is not allowed to specify local path of \"" + $key + "\" in package.conf.json file, " +
                        "but configuration can be overridden in private.conf.json file.");
                };
                const checkScript : any = ($name : string, $source : any, $scriptName : string) : void => {
                    if ($source.hasOwnProperty($scriptName)) {
                        const script : IProjectDependencyScriptConfig = $source[$scriptName];
                        if (script.hasOwnProperty("path")) {
                            const scriptPath : string = script.path;
                            if (path.isAbsolute(scriptPath)) {
                                fail($name + ":" + $scriptName + ":path");
                            }
                        }
                    }
                };
                Object.keys($data.dependencies).forEach(($name : string) : void => {
                    const source : IProjectDependency | string = $data.dependencies[$name];
                    if (ObjectValidator.IsString(source)) {
                        if (StringUtils.StartsWith(<string>source, "!")) {
                            LogIt.Error("It is not allowed to force bypass dependency branches tree check. " +
                                "This feature is allowed only in private.conf.json file.");
                        } else if (StringUtils.StartsWith(<string>source, "dir+")) {
                            LogIt.Error("It is not allowed to specify local repo as dependency by package.conf.json file, " +
                                "but configuration of dependencies with remote repo can be overridden by private.conf.json file.");
                        }
                    } else if (ObjectValidator.IsObject(source)) {
                        if (source.hasOwnProperty("location")) {
                            const location : IProjectDependencyLocation =
                                <IProjectDependencyLocation>(<IProjectDependency>source).location;
                            if (location.hasOwnProperty("type") && location.type === "dir") {
                                fail($name + ":location");
                            }

                            if (location.hasOwnProperty("path")) {
                                const path : string = this.ResolveDependencyPath((<IProjectDependencyLocation>location).path);
                                if (url.parse(path).protocol === "file:") {
                                    fail($name + ":location:path");
                                }
                            }
                        }
                        checkScript($name, source, "configureScript");
                        checkScript($name, source, "installScript");
                    }
                });
            }

            if ($data.hasOwnProperty("target") && $data.target.hasOwnProperty("resources")) {
                let resources : IProjectTargetResources[] = $data.target.resources;
                if (ObjectValidator.IsObject($data.target.resources)) {
                    const resourcesObject : IProjectTargetResources[] = JsonUtils.Clone($data.target.resources);
                    resources = [];
                    let name : string;
                    for (name in resourcesObject) {
                        if (resourcesObject.hasOwnProperty(name)) {
                            resourcesObject[name].name = name;
                            resources.push(resourcesObject[name]);
                        }
                    }
                }

                let resIndex : number = 0;
                resources.forEach(($resource : IProjectTargetResources) : void => {
                    let hasAbsolutePath : boolean = false;
                    if ($resource.hasOwnProperty("input") && path.isAbsolute($resource.input)) {
                        hasAbsolutePath = true;
                    }
                    if (!hasAbsolutePath && $resource.hasOwnProperty("files")) {
                        $resource.files.forEach(($file : IProjectTargetResourceFiles) : void => {
                            if (!hasAbsolutePath) {
                                if ($file.hasOwnProperty("cwd") && path.isAbsolute($file.cwd)) {
                                    hasAbsolutePath = true;
                                } else {
                                    if (ObjectValidator.IsString($file.src)) {
                                        hasAbsolutePath = path.isAbsolute(<string>$file.src);
                                    } else {
                                        (<string[]>$file.src).forEach(($input : string) : void => {
                                            if (!hasAbsolutePath) {
                                                hasAbsolutePath = path.isAbsolute($input);
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                    if (hasAbsolutePath) {
                        const name : string =
                            ObjectValidator.IsEmptyOrNull($resource.name) || StringUtils.StartsWith($resource.name, "$sha_") ?
                                resIndex + "" : $resource.name;
                        LogIt.Error(
                            "Local resource input path for target:resources[" + name + "] " +
                            "can be used only in private.conf.json file.");
                    }
                    resIndex++;
                });
            }
        }

        return $data;
    }

    private normalizeConfig($data : IProject) : IProject {
        if (ObjectValidator.IsEmptyOrNull($data)) {
            return $data;
        }

        if ($data.hasOwnProperty("dependencies")) {
            Object.keys($data.dependencies).forEach(($name : string) : void => {
                const source : IProjectDependency = $data.dependencies[$name];
                if (ObjectValidator.IsObject(source)) {
                    if (source.hasOwnProperty("configure-script")) {
                        LogIt.Warning("Keyword \"configure-script\" has been deprecated and should be replaced by " +
                            "\"configureScript\" at dependency: " + $name);
                        source.configureScript = source["configure-script"];
                    }
                    if (source.hasOwnProperty("install-script")) {
                        LogIt.Warning("Keyword \"install-script\" has been deprecated and should be replaced by " +
                            "\"installScript\" at dependency: " + $name);
                        source.installScript = source["install-script"];
                    }
                }
            });
        }

        if ($data.hasOwnProperty("target")) {
            if ($data.target.hasOwnProperty("resources") && ObjectValidator.IsArray($data.target.resources)) {
                const resources : IProjectTargetResources[] = JsonUtils.Clone($data.target.resources);
                $data.target.resources = <any>{};
                resources.forEach(($resource : IProjectTargetResources) : void => {
                    $data.target.resources["$sha_" + this.getUID()] = $resource;
                });
            }

            if ($data.target.hasOwnProperty("requestPatterns")) {
                if (ObjectValidator.IsString($data.target.requestPatterns)) {
                    $data.target.requestPatterns = [<any>$data.target.requestPatterns];
                }
                if (ObjectValidator.IsArray($data.target.requestPatterns)) {
                    $data.target.requestPatterns = <any>{
                        default: $data.target.requestPatterns
                    };
                }
            }

            if ($data.target.hasOwnProperty("embedFeatures")) {
                if (ObjectValidator.IsArray($data.target.embedFeatures)) {
                    $data.target.embedFeatures = <any>{
                        default: $data.target.embedFeatures
                    };
                }
            }
        }

        if (ObjectValidator.IsString($data.repository)) {
            $data.repository = {
                type: "git",
                url : <any>$data.repository
            };
        }

        if ($data.hasOwnProperty("deploy") && $data.deploy.hasOwnProperty("server")) {
            [DeployServerType.PROD, DeployServerType.EAP, DeployServerType.DEV].forEach(($type : string) : void => {
                if (ObjectValidator.IsString($data.deploy.server[$type])) {
                    $data.deploy.server[$type] = {
                        location: $data.deploy.server[$type]
                    };
                }
            });
        }

        return $data;
    }

    private createProject() : void {
        const deployServer : IDeployServers = this.getProjectConfig().deployServer;
        const deployCredentials : IDeployServersCredentials = this.getProjectConfig().deployCredentials;
        this.project = <any>{
            builder             : {
                location: "host",
                version : "",
                port    : -1
            },
            version             : "",
            namespaces          : ["Com.Wui", "Io.Oidis"],
            namespaceMappings   : [],
            loaderClass         : "",
            target              : {
                name           : "",
                companyName    : "Oidis",
                copyright      : "Copyright " + StringUtils.YearFrom(2019) + " Oidis",
                description    : "",
                icon           : "resource/graphics/icon.ico",
                remotePort     : -1,
                width          : 1036,
                height         : 780,
                maximized      : false,
                resources      : {},
                platforms      : ["web"],
                elevate        : false,
                singletonApp   : false,
                startPage      : "",
                codeSigning    : {
                    enabled        : false,
                    certPath       : "",
                    timestampServer: "http://timestamp.digicert.com",
                    serverConfig   : "",
                    files          : []
                },
                upx            : {
                    enabled: false,
                    options: [
                        "--best"
                    ],
                    files  : []
                },
                splashScreen   : {
                    width         : 900,
                    height        : 600,
                    background    : "#ffffff",
                    installPath   : "",
                    executable    : "",
                    executableArgs: [],
                    userControls  : {
                        label      : {
                            text   : "",
                            x      : 0,
                            y      : 0,
                            width  : 200,
                            height : 35,
                            justify: "left",
                            visible: true,
                            font   : {
                                name : "Courier New",
                                size : 14,
                                bold : false,
                                color: "#ff0000"
                            }
                        },
                        labelStatic: {
                            text   : "",
                            x      : 0,
                            y      : 0,
                            width  : 200,
                            height : 35,
                            justify: "left",
                            visible: true,
                            font   : {
                                name : "Courier New",
                                size : 14,
                                bold : false,
                                color: "#0050ff"
                            }
                        },
                        progress   : {
                            background  : "#ffff00",
                            x           : 0,
                            y           : 50,
                            width       : 400,
                            height      : 20,
                            marquee     : false,
                            visible     : true,
                            trayProgress: false,
                            foreground  : "#ff00ff"
                        }
                    },
                    resources     : []
                },
                toolchain      : "",
                configs        : [],
                wuiModulesType : DeployServerType.PROD,
                requestPatterns: [],
                embedFeatures  : [],
                selfinstall    : {
                    script: "",
                    chain : []
                },
                mockServer     : {
                    port  : 0,
                    config: []
                }
            },
            deploy              : {
                server   : {
                    prod: {
                        location: deployServer.prod,
                        user    : deployCredentials.prod.user,
                        pass    : deployCredentials.prod.pass
                    },
                    eap : {
                        location: deployServer.eap,
                        user    : deployCredentials.eap.user,
                        pass    : deployCredentials.eap.pass
                    },
                    dev : {
                        location: deployServer.dev,
                        user    : deployCredentials.dev.user,
                        pass    : deployCredentials.dev.pass
                    }
                },
                chunkSize: 1024 * 1024 * 1.5, // 1.5 MB
                user     : "",
                pass     : ""
            },
            releases            : {},
            docs                : {
                systemName: "",
                copyright : ""
            },
            solutionsLocation   : "",
            solutions           : {
                default: ""
            },
            runConfigs          : {
                default        : "__defaultConfig",
                __defaultConfig: {
                    port: 2225,
                    args: []
                }
            },
            modules             : {},
            licensesRegister    : {
                filePath: "SW-Content-Register.txt"
            },
            noServer            : false,
            noCompress          : false,
            lintErrorsAsWarnings: false,
            nexusConfig         : {}
        };
        this.project.docs.copyright = this.project.target.copyright;
    }

    private createProperties() : void {
        const path : any = require("path");
        const binBase : string = this.programArgs.BinBase();
        const targetBase : string = this.getTargetBase();
        const noTarget : boolean = this.programArgs.getOptions().noTarget;

        const patternExists : any = ($pattern : string) : boolean => {
            let result : boolean = false;
            if (!noTarget) {
                LogIt.MuteToggle();
                if (this.fileSystem.Expand(targetBase + "/" + $pattern).length === 0) {
                    const filter : string[] = [targetBase + "/dependencies/*/package.conf.json*"];
                    OSType.getProperties().forEach(($value : string) : void => {
                        filter.push(targetBase + "/dependencies/*/" + OSType[$value] + "/package.conf.json*");
                    });
                    const dependencies : string[] = this.fileSystem.Expand(filter);
                    let index : number;
                    for (index = 0; index < dependencies.length; index++) {
                        const dependencyPath : string = path.dirname(dependencies[index]);
                        if (this.fileSystem.Expand(dependencyPath + "/" + $pattern).length > 0) {
                            result = true;
                            break;
                        }
                    }
                } else {
                    result = true;
                }
                LogIt.MuteToggle();
            }
            return result;
        };

        let builderPlatform : OSType = OSType.WIN;
        if (!EnvironmentHelper.IsWindows()) {
            if (EnvironmentHelper.IsLinux()) {
                builderPlatform = OSType.LINUX;
            } else if (EnvironmentHelper.IsMac()) {
                builderPlatform = OSType.MAC;
            } else if (EnvironmentHelper.IsArm()) {
                builderPlatform = OSType.IMX;
            }
        }

        this.properties = <any>{
            binBase,
            binBaseUnix             : StringUtils.Replace(binBase, "\\", "/"),
            projectBase             : targetBase,
            sources                 : "source/**",
            resources               : "resource",
            dependencies            : "dependencies/*/source/**",
            javaDependencies        : [],
            javaProjectVersion      : "",
            mavenResourcePaths      : [],
            mavenArtifacts          : [],
            mavenRepositories       : [],
            wuiMavenModules         : [],
            dependencyMavenArtifacts: [],
            cppDependencies         : [],
            cppIncludes             : "",
            cppLibraries            : "",
            dependenciesConfig      : {},
            oidisDependencies       : [],
            esModuleDependencies    : [],
            mobileExtensions        : {
                android : ".apk",
                ios     : ".ipa",
                winphone: ".appx"
            },
            dependenciesResources   : "dependencies/*/resource",
            tmp                     : "build/compiled/source",
            dest                    : "build/target",
            compiled                : "build/compiled",
            packageName             : "",
            loaderPath              : "resource/javascript/loader.min.js",
            nodejsPort              : 35666,
            licenseText             : "/* Copyright " + StringUtils.YearFrom(2019) + " Oidis. " +
                "The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution " +
                "or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText. */",
            stats                   : {
                buildMinutes: 0
            },
            projectHas              : {
                TypeScript   : {
                    Source: () : boolean => {
                        const result : boolean = patternExists("source/typescript/**/*.{ts,tsx}");
                        this.properties.projectHas.TypeScript.Source = () : boolean => {
                            return result;
                        };
                        return result;
                    },
                    Tests : () : boolean => {
                        const result : boolean = patternExists("test/unit/typescript/**/*.{ts,tsx}");
                        this.properties.projectHas.TypeScript.Tests = () : boolean => {
                            return result;
                        };
                        return result;
                    }
                },
                Cpp          : {
                    Source: () : boolean => {
                        const result : boolean = patternExists("source/cpp/**/*.cpp");
                        this.properties.projectHas.Cpp.Source = () : boolean => {
                            return result;
                        };
                        return result;
                    },
                    Tests : () : boolean => {
                        const result : boolean = patternExists("test/unit/cpp/**/*.cpp");
                        this.properties.projectHas.Cpp.Tests = () : boolean => {
                            return result;
                        };
                        return result;
                    }
                },
                Java         : {
                    Source: () : boolean => {
                        const result : boolean = patternExists("source/java/**/*.java");
                        this.properties.projectHas.Java.Source = () : boolean => {
                            return result;
                        };
                        return result;
                    },
                    Tests : () : boolean => {
                        const result : boolean = patternExists("test/unit/java/**/*.java");
                        this.properties.projectHas.Java.Tests = () : boolean => {
                            return result;
                        };
                        return result;
                    }
                },
                Python       : {
                    Source: () : boolean => {
                        const result : boolean = patternExists("source/python/**/*.py");
                        this.properties.projectHas.Python.Source = () : boolean => {
                            return result;
                        };
                        return result;
                    },
                    Tests : () : boolean => {
                        const result : boolean = patternExists("test/unit/python/**/*.py");
                        this.properties.projectHas.Python.Tests = () : boolean => {
                            return result;
                        };
                        return result;
                    }
                },
                SASS         : () : boolean => {
                    const result : boolean = patternExists("resource/sass/**/*.scss");
                    this.properties.projectHas.SASS = () : boolean => {
                        return result;
                    };
                    return result;
                },
                Resources    : () : boolean => {
                    const result : boolean = patternExists("resource/**/*.*");
                    this.properties.projectHas.Resources = () : boolean => {
                        return result;
                    };
                    return result;
                },
                SeleniumTests: () : boolean => {
                    const result : boolean = patternExists("test/selenium/**/*.{ts,tsx}");
                    this.properties.projectHas.SeleniumTests = () : boolean => {
                        return result;
                    };
                    return result;
                },
                Markdown     : () : boolean => {
                    const result : boolean = patternExists("source/markdown/**/*.md");
                    this.properties.projectHas.Markdown = () : boolean => {
                        return result;
                    };
                    return result;
                },
                ContentFor($pattern : string) : boolean {
                    return patternExists($pattern);
                },
                ESModules: () : boolean => {
                    const toolchain : IToolchainSettings = JsonUtils.Extend(Toolchains.getToolchain(ToolchainType.NODEJS),
                        this.project.target.toolchainSettings);
                    const result : boolean = toolchain.version === "module";
                    this.properties.projectHas.ESModules = () : boolean => {
                        return result;
                    };
                    return result;
                }
            },
            buildCheckRegister      : [],
            serverConfig            : {},
            toolchains              : {
                gcc    : {
                    cmakeToolchain: "gcc-toolchain.cmake",
                    CC            : "<? @var properties.externalModules ?>/gcc/gcc",
                    CXX           : "<? @var properties.externalModules ?>/gcc/g++"
                },
                arm    : {
                    cmakeToolchain: "arm-toolchain.cmake",
                    CC            : "<? @var properties.externalModules ?>/aarch64/aarch64-linux-gnu-gcc",
                    CXX           : "<? @var properties.externalModules ?>/aarch64/aarch64-linux-gnu-g++",
                    AR            : "<? @var properties.externalModules ?>/aarch64/aarch64-linux-gnu-ar",
                    LD            : "<? @var properties.externalModules ?>/aarch64/aarch64-linux-gnu-g++",
                    RANLIB        : "<? @var properties.externalModules ?>/aarch64/aarch64-linux-gnu-ranlib"
                },
                aarch32: {
                    cmakeToolchain: "aarch32-toolchain.cmake",
                    CC            : "<? @var properties.externalModules ?>/aarch32/bin/arm-linux-gnueabihf-gcc",
                    CXX           : "<? @var properties.externalModules ?>/aarch32/bin/arm-linux-gnueabihf-g++",
                    AR            : "<? @var properties.externalModules ?>/aarch32/bin/arm-linux-gnueabihf-ar",
                    LD            : "<? @var properties.externalModules ?>/aarch32/bin/arm-linux-gnueabihf-g++",
                    RANLIB        : "<? @var properties.externalModules ?>/aarch32/bin/arm-linux-gnueabihf-ranlib"
                },
                aarch64: {
                    cmakeToolchain: "aarch64-toolchain.cmake",
                    CC            : "<? @var properties.externalModules ?>/aarch64/bin/aarch64-linux-gnu-gcc",
                    CXX           : "<? @var properties.externalModules ?>/aarch64/bin/aarch64-linux-gnu-g++",
                    AR            : "<? @var properties.externalModules ?>/aarch64/bin/aarch64-linux-gnu-ar",
                    LD            : "<? @var properties.externalModules ?>/aarch64/bin/aarch64-linux-gnu-g++",
                    RANLIB        : "<? @var properties.externalModules ?>/aarch64/bin/aarch64-linux-gnu-ranlib"
                },
                clang  : {
                    cmakeToolchain: "clang-toolchain.cmake",
                    CC            : "<? @var properties.externalModules ?>/clang/cc",
                    CXX           : "<? @var properties.externalModules ?>/clang/c++"
                },
                msvc   : {
                    cmakeToolchain: "msvc-toolchain.cmake",
                    cmakeOptions  : [
                        "-DCMAKE_VS_PLATFORM_TOOLSET=v142"
                    ],
                    initScript    : "vcvars64.bat"
                },
                pip    : {
                    version: "3"
                },
                nodejs : {
                    types               : [
                        "@types/node"
                    ],
                    withDependenciesScan: false
                },
                gyp    : {
                    gypVcVer: "vs2019"
                }
            },
            builderPlatform,
            sourceNamespacePath     : ""
        };
        if (EnvironmentHelper.IsMac()) {
            this.properties.toolchains.aarch32 = <any>{
                AR            : "<? @var properties.externalModules ?>/aarch32/bin/arm-unknown-linux-gnueabihf-ar",
                CC            : "<? @var properties.externalModules ?>/aarch32/bin/arm-unknown-linux-gnueabihf-gcc",
                CXX           : "<? @var properties.externalModules ?>/aarch32/bin/arm-unknown-linux-gnueabihf-g++",
                LD            : "<? @var properties.externalModules ?>/aarch32/bin/arm-unknown-linux-gnueabihf-g++",
                RANLIB        : "<? @var properties.externalModules ?>/aarch32/bin/arm-unknown-linux-gnueabihf-ranlib",
                cmakeToolchain: "aarch32-toolchain.cmake"
            };
            this.properties.toolchains.aarch64 = <any>{
                AR            : "<? @var properties.externalModules ?>/aarch64/bin/aarch64-unknown-linux-gnu-ar",
                CC            : "<? @var properties.externalModules ?>/aarch64/bin/aarch64-unknown-linux-gnu-gcc",
                CXX           : "<? @var properties.externalModules ?>/aarch64/bin/aarch64-unknown-linux-gnu-g++",
                LD            : "<? @var properties.externalModules ?>/aarch64/bin/aarch64-unknown-linux-gnu-g++",
                RANLIB        : "<? @var properties.externalModules ?>/aarch64/bin/aarch64-unknown-linux-gnu-ranlib",
                cmakeToolchain: "aarch64-toolchain.cmake"
            };
        }
    }

    private async loadSchema() : Promise<any> {
        if (!this.programArgs.getOptions().noTarget) {
            const defaultSchema : IProjectConfigLoadResponse =
                await this.LoadPackageConf(this.programArgs.BinBase() + "/resource/configs/default.conf.schema.jsonp");
            delete (<any>defaultSchema.data).$interface;
            delete (<any>defaultSchema.data).extendsConfig;
            delete (<any>defaultSchema.data).imports;
            const targetBase : string = this.getTargetBase();

            let projectSchemaPath : string;
            if (!ObjectValidator.IsEmptyOrNull(this.project.schema)) {
                projectSchemaPath = StringReplace.Content(this.project.schema, StringReplaceType.VARIABLES);
                if (!StringUtils.StartsWith(projectSchemaPath, "/")) {
                    projectSchemaPath = "/" + projectSchemaPath;
                }
                projectSchemaPath = targetBase + projectSchemaPath;
            } else {
                projectSchemaPath = targetBase + "/bin/project/configs/package.conf.schema.jsonp";
                if (!this.fileSystem.Exists(projectSchemaPath)) {
                    projectSchemaPath = targetBase + "/resource/configs/package.conf.schema.jsonp";
                }
            }

            const projectSchema : IProjectConfigLoadResponse = await this.LoadPackageConf(projectSchemaPath);
            delete (<any>projectSchema.data).$interface;
            delete (<any>projectSchema.data).extendsConfig;
            delete (<any>projectSchema.data).imports;
            Resources.Extend(defaultSchema.data, projectSchema.data);
            return defaultSchema.data;
        } else {
            return {};
        }
    }

    private processConfig($schema? : any) : void {
        const appConfig : IAppConfiguration = this.getProjectConfig();

        this.properties.externalModules = this.programArgs.AppDataPath() + "/external_modules";
        this.project = JSON.parse(StringReplace.Content(JSON.stringify(this.project), StringReplaceType.VARIABLES));
        this.properties.toolchains = JSON.parse(StringReplace.Content(
            JSON.stringify(this.properties.toolchains), StringReplaceType.VARIABLES));

        if (ObjectValidator.IsEmptyOrNull(appConfig.eclipseBase)) {
            appConfig.eclipseBase = this.properties.externalModules + "/eclipse";
        }
        if (ObjectValidator.IsEmptyOrNull(appConfig.ideaBase)) {
            appConfig.ideaBase = this.properties.externalModules + "/idea";
        }
        if (ObjectValidator.IsEmptyOrNull(appConfig.vcvarsPath)) {
            appConfig.vcvarsPath = "C:/Program Files (x86)/Microsoft Visual Studio 14.0/VC";
        }
        if (ObjectValidator.IsEmptyOrNull(appConfig.reportHub)) {
            appConfig.reportHub = appConfig.hub.url;
        }

        if (!this.programArgs.getOptions().noTarget && !ObjectValidator.IsEmptyOrNull($schema)) {
            this.project.schema = $schema;
        }

        if (ObjectValidator.IsObject(this.project.target.resources)) {
            const resources : IProjectTargetResources[] = JsonUtils.Clone(this.project.target.resources);
            this.project.target.resources = [];
            let name : string;
            for (name in resources) {
                if (resources.hasOwnProperty(name)) {
                    resources[name].name = name;
                    this.project.target.resources.push(resources[name]);
                }
            }
        }
        if (ObjectValidator.IsObject(this.project.target.requestPatterns)) {
            const patterns : any = JsonUtils.Clone(this.project.target.requestPatterns);
            this.project.target.requestPatterns = [];
            let name : string;
            for (name in patterns) {
                if (patterns.hasOwnProperty(name)) {
                    if (ObjectValidator.IsString(patterns[name])) {
                        patterns[name] = [patterns[name]];
                    }
                    this.project.target.requestPatterns = this.project.target.requestPatterns.concat(patterns[name]);
                }
            }
        }
        if (ObjectValidator.IsObject(this.project.target.embedFeatures)) {
            const patterns : any = JsonUtils.Clone(this.project.target.embedFeatures);
            this.project.target.embedFeatures = [];
            let name : string;
            for (name in patterns) {
                if (patterns.hasOwnProperty(name)) {
                    this.project.target.embedFeatures = this.project.target.embedFeatures.concat(patterns[name]);
                }
            }
        }
        if (ObjectValidator.IsEmptyOrNull(this.project.target.name)) {
            this.project.target.name = this.project.name;
        }
        if (ObjectValidator.IsEmptyOrNull(this.project.target.description)) {
            this.project.target.description = this.project.description;
        }
        if (ObjectValidator.IsEmptyOrNull(this.project.target.codeSigning.certPath)) {
            this.project.target.codeSigning.certPath = this.programArgs.BinBase() + "/resource/data/Io/Oidis/Builder/CodeSign/SPC.pfx";
        }

        if (ObjectValidator.IsEmptyOrNull(this.project.docs.systemName)) {
            this.project.docs.systemName = this.project.target.name;
        }

        [DeployServerType.PROD, DeployServerType.EAP, DeployServerType.DEV].forEach(($type : string) : void => {
            if (ObjectValidator.IsEmptyOrNull(this.project.deploy.server[$type].user) ||
                !ObjectValidator.IsEmptyOrNull(this.project.deploy.user) &&
                this.project.deploy.server[$type].user === appConfig.deployCredentials[$type].user) {
                this.project.deploy.server[$type].user = this.project.deploy.user;
            }
            if (ObjectValidator.IsEmptyOrNull(this.project.deploy.server[$type].pass) ||
                !ObjectValidator.IsEmptyOrNull(this.project.deploy.pass) &&
                this.project.deploy.server[$type].pass === appConfig.deployCredentials[$type].pass) {
                this.project.deploy.server[$type].pass = this.project.deploy.pass;
            }
        });

        this.properties.javaProjectVersion = StringUtils.Replace(this.project.version, "-", ".");
        this.properties.packageVersion = StringUtils.Replace(this.project.version, ".", "-");
        this.properties.packageName = this.project.name + "-" + this.properties.packageVersion;

        if (this.project.hasOwnProperty("license") && this.project.license !== "") {
            this.properties.licenseText = "/* " + this.project.license + " */";
        }
        const stats : any = PersistenceFactory.getPersistence(this.getClassName()).Variable("Stats");
        if (!ObjectValidator.IsEmptyOrNull(stats)) {
            this.properties.stats = stats;
        }
        let rootPath : string = "";
        const sourceBase : string = this.properties.projectBase + "/source/typescript/";
        const loaderName : string = "Loader.ts";
        this.fileSystem.Expand(sourceBase + "**/" + loaderName).forEach(($path : string) : void => {
            if (this.fileSystem.IsFile($path) && ObjectValidator.IsEmptyOrNull(rootPath)) {
                rootPath = StringUtils.Remove($path, sourceBase, "/" + loaderName);
            }
        });
        this.properties.sourceNamespacePath = rootPath;

        const allowedReleases : any = {};
        const allowedReleaseNames : string[] = [];
        const skippedReleases : string[] = [];
        let releaseName : string;
        for (releaseName in this.project.releases) {
            if (this.project.releases.hasOwnProperty(releaseName)) {
                if (this.project.releases[releaseName].hasOwnProperty("skipped")) {
                    if (!this.project.releases[releaseName].skipped) {
                        allowedReleases[releaseName] = this.project.releases[releaseName];
                        allowedReleaseNames.push(releaseName);
                    } else {
                        skippedReleases.push(releaseName);
                    }
                } else {
                    allowedReleases[releaseName] = this.project.releases[releaseName];
                    allowedReleaseNames.push(releaseName);
                }
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(skippedReleases)) {
            LogIt.Warning("Some releases will be skipped: " + skippedReleases.join(", "));
        }

        this.project.releases = allowedReleases;
        this.project.target.toolchainSettings = Toolchains.getToolchain();
        if (allowedReleaseNames.length === 1) {
            Resources.Extend(this.project.target, allowedReleases[allowedReleaseNames[0]]);
        }

        if (EnvironmentHelper.IsWindows()) {
            process.env.PATH = Loader.getInstance().getTerminal().NormalizeEnvironmentPath(appConfig.vcvarsPath + ";");
        }

        if (!ObjectValidator.IsEmptyOrNull(this.project.target.wuiModulesType) &&
            !DeployServerType.Contains(this.project.target.wuiModulesType)) {
            LogIt.Error("Unsupported WUI Modules type \"" + this.project.target.wuiModulesType + "\", " +
                "please use one of: prod, dev or eap");
        }

        this.buildArgs = {
            isRebuild  : false,
            isTest     : false,
            platform   : "web",
            product    : null,
            releaseName: this.project.name,
            time       : this.buildTime,
            timestamp  : this.buildTime.getTime(),
            type       : CliTaskType.PROD,
            year       : this.buildTime.getFullYear()
        };

        const serverConfig : any = this.properties.serverConfig;
        serverConfig.version = this.project.version;
        delete serverConfig.extendsConfig;
        delete serverConfig.imports;
        delete serverConfig.$interface;
        serverConfig.build = this.buildArgs;
        serverConfig.schema = "";
        serverConfig.releases = {};
        serverConfig.dependencies = {};
        serverConfig.repository = {};
        serverConfig.solutions = {};
        serverConfig.deploy = {};
        serverConfig.runConfigs = {};
        serverConfig.licensesRegister = {};
        if (ObjectValidator.IsSet(serverConfig.noReport)) {
            serverConfig.noReport = false;
            serverConfig.eclipseBase = "";
            serverConfig.ideaBase = "";
        }
        serverConfig.target = JsonUtils.Clone(this.project.target);
        serverConfig.target.appDataPath = "";
        serverConfig.target.codeSigning = {};
        serverConfig.target.resources = [];
        serverConfig.target.splashScreen = {};
        serverConfig.loaderClass = this.project.loaderClass;
        serverConfig.packageName = this.properties.packageName;
        serverConfig.logger = this.project.logger;

        this.properties.clientConfig = {
            name             : serverConfig.name,
            version          : serverConfig.version,
            description      : serverConfig.description,
            license          : serverConfig.license,
            homepage         : serverConfig.homepage,
            author           : serverConfig.author,
            contributors     : serverConfig.contributors,
            loaderClass      : serverConfig.loaderClass,
            target           : {
                name       : serverConfig.target.name,
                companyName: serverConfig.target.companyName,
                copyright  : serverConfig.target.copyright,
                description: serverConfig.target.description,
                appDataPath: ""
            },
            namespaces       : serverConfig.namespaces,
            namespaceMappings: serverConfig.namespaceMappings,
            build            : serverConfig.build,
            packageName      : serverConfig.packageName,
            logger           : serverConfig.logger,
            nexusConfig      : serverConfig.nexusConfig
        };
        this.project.target.embedFeatures.forEach(($feature : string) : void => {
            let source : any = {
                project: JsonUtils.Clone(this.project)
            };
            const namespaces : string[] = StringUtils.Split($feature, ".");
            let found : boolean = false;
            for (const item of namespaces) {
                if (source.hasOwnProperty(item)) {
                    source = source[item];
                    found = true;
                } else {
                    found = false;
                }
            }
            if (found) {
                const target : any = {project: {}};
                let index : number;
                let item : any = target;
                for (index = 0; index < namespaces.length; index++) {
                    if (index < namespaces.length - 1) {
                        item = item[namespaces[index]] = {};
                    } else {
                        item[namespaces[index]] = source;
                    }
                }
                JsonUtils.Extend(this.properties.clientConfig, target.project);
            }
        });
        const patterns : string[] = [
            "*pass*",
            "*token*"
        ];
        JSON.stringify(this.properties.clientConfig, ($key : string, $value : any) : any => {
            let passed : boolean = true;
            patterns.forEach(($pattern : string) : void => {
                if (passed && StringUtils.PatternMatched($pattern, StringUtils.ToLowerCase($key))) {
                    passed = false;
                }
            });
            if (!passed && !ObjectValidator.IsEmptyOrNull($value)) {
                LogIt.Warning("Client config contains sensitive information. " +
                    "Please validate project configuration and settings of embedFeatures. Value removed for key: " + $key);
                return undefined;
            }
            return $value;
        });
    }
}

export interface IEnvironmentLoadResponse {
    status : boolean;
    path? : string;
}

export interface IProjectConfigLoadResponse extends IEnvironmentLoadResponse {
    data : IProject;
}

// generated-code-start
export const IEnvironmentLoadResponse = globalThis.RegisterInterface(["status", "path"]);
export const IProjectConfigLoadResponse = globalThis.RegisterInterface(["data"], <any>IEnvironmentLoadResponse);
// generated-code-end
