/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { FileSystemHandler } from "../Connectors/FileSystemHandler.js";
import { Terminal } from "../Connectors/Terminal.js";
import { Resources } from "../DAO/Resources.js";
import { IProjectConfigLoadResponse } from "../EnvironmentArgs.js";
import { IAppConfiguration, IBuildArgs } from "../Interfaces/IAppConfiguration.js";
import { IBaseTask } from "../Interfaces/IBaseTask.js";
import { IProject } from "../Interfaces/IProject.js";
import { IProperties } from "../Interfaces/IProperties.js";
import { Loader } from "../Loader.js";
import { ProgramArgs } from "../Structures/ProgramArgs.js";
import { TaskEnvironment } from "../Structures/TaskEnvironment.js";
import { TasksResolver } from "../TasksResolver.js";

export abstract class BaseTask extends BaseObject implements IBaseTask {
    protected programArgs : ProgramArgs;
    protected project : IProject;
    protected properties : IProperties;
    protected build : IBuildArgs;
    protected builderConfig : IAppConfiguration;
    protected externalModules : string;
    protected wuiModules : string;
    protected fileSystem : FileSystemHandler;
    protected terminal : Terminal;
    private environment : TaskEnvironment;
    private readonly name : string;
    private owner : TasksResolver;

    constructor() {
        super();
        this.name = this.getClassNameWithoutNamespace();
        this.initEnvironment();
    }

    public setEnvironment($value : TaskEnvironment) : void {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.environment = $value;
            this.project = $value.Project();
            this.properties = $value.Properties();
            this.programArgs = $value.ProgramArgs();
            this.build = $value.BuildArgs();
            this.externalModules = this.programArgs.AppDataPath() + "/external_modules";
            this.wuiModules = this.programArgs.AppDataPath() + "/wui_modules/" + this.project.target.wuiModulesType;
        }
    }

    public setOwner($owner : TasksResolver) : void {
        this.owner = $owner;
    }

    public Name() : string {
        return this.getName();
    }

    public async Process($option? : string) : Promise<void> {
        return new Promise<void>(($resolve, $reject) => {
            try {
                this.process($resolve, $option);
            } catch (ex) {
                $reject(ex);
            }
        });
    }

    public getDependenciesTree($option? : string) : string[] {
        return [];
    }

    protected initEnvironment() : void {
        const instance : Loader = Loader.getInstance();
        this.fileSystem = instance.getFileSystemHandler();
        this.terminal = instance.getTerminal();
        this.builderConfig = instance.getAppConfiguration();

        const environment : TaskEnvironment = new TaskEnvironment();
        environment.Project(instance.getProjectConfig());
        environment.Properties(instance.getAppProperties());
        environment.ProgramArgs(instance.getProgramArgs());
        environment.BuildArgs(instance.getEnvironmentArgs().getBuildArgs());
        this.setEnvironment(environment);
    }

    protected process($done : () => void, $option? : string) : void {
        this.processAsync($option)
            .then(() : void => {
                $done();
            })
            .catch(($error : Error) : void => {
                LogIt.Error($error);
            });
    }

    protected async processAsync($option? : string) : Promise<void> {
        LogIt.Info("running task: " + this.getName());
    }

    protected getName() : string {
        return this.name;
    }

    protected runTask($done : () => void, $tasks : string | string[]) : void;
    protected runTask($done : () => void, ...$tasks : string[]) : void;
    protected runTask($done : () => void, ...$tasks : any[]) : void {
        if (ObjectValidator.IsString($tasks)) {
            $tasks = [<any>$tasks];
        } else if ($tasks.length === 1 && ObjectValidator.IsArray($tasks[0])) {
            $tasks = <string[]>$tasks[0];
        }
        this.runTaskAsync($tasks).then($done);
    }

    protected async runTaskAsync($tasks : string | string[]) : Promise<void>;
    protected async runTaskAsync(...$tasks : string[]) : Promise<void>;
    protected async runTaskAsync(...$tasks : any[]) : Promise<void> {
        if (ObjectValidator.IsString($tasks)) {
            $tasks = [<any>$tasks];
        } else if ($tasks.length === 1 && ObjectValidator.IsArray($tasks[0])) {
            $tasks = <string[]>$tasks[0];
        }
        await this.owner.Resolve($tasks);
    }

    protected async resolveExternConfig($name : string) : Promise<any> {
        const config : any = {};
        let defaultConfig : string = this.properties.binBase + "/resource/configs/" + $name + ".conf.json";
        if (this.fileSystem.Exists(defaultConfig)) {
            JsonUtils.Extend(config, JSON.parse(this.fileSystem.Read(defaultConfig).toString()));
        } else {
            defaultConfig += "p";
            if (this.fileSystem.Exists(defaultConfig)) {
                const baseConfig : IProjectConfigLoadResponse = await Loader.getInstance().getEnvironmentArgs()
                    .LoadPackageConf(defaultConfig);
                delete (<any>baseConfig.data).$interface;
                delete (<any>baseConfig.data).extendsConfig;
                delete (<any>baseConfig.data).imports;
                Resources.Extend(config, baseConfig.data);
            }
        }
        let overridePath : string = this.properties.projectBase + "/bin/project/configs/" + $name + ".conf.jsonp";
        if (!this.fileSystem.Exists(overridePath)) {
            overridePath = overridePath.slice(0, -1);
        }
        if (!this.fileSystem.Exists(overridePath)) {
            overridePath = this.properties.projectBase + "/resource/configs/" + $name + ".conf.json";
        }
        const res : IProjectConfigLoadResponse = await Loader.getInstance().getEnvironmentArgs().LoadPackageConf(overridePath);
        if (res.status) {
            delete (<any>res.data).$interface;
            delete (<any>res.data).extendsConfig;
            delete (<any>res.data).imports;
            Resources.Extend(config, res.data);
        }
        return config;
    }
}
