/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ArchType } from "../Enums/ArchType.js";
import { OSType } from "../Enums/OSType.js";
import { ProductType } from "../Enums/ProductType.js";
import { ToolchainType } from "../Enums/ToolchainType.js";
import { IBuildPlatform, IRunPlatform } from "../Interfaces/IBuildPlatform.js";

export class BuildProductArgs extends BaseObject {
    private type : ProductType;
    private os : OSType;
    private arch : ArchType;
    private toolchain : ToolchainType;
    private valueString : string;

    constructor() {
        super();
        this.setDefaults();
    }

    public Parse($value : string | IBuildPlatform, $toolchain? : string) : void {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.setDefaults();
            if (!ObjectValidator.IsEmptyOrNull($toolchain)) {
                LogIt.Warning("Toolchain should be specified as part of platform definition object.");
            }
            if (ObjectValidator.IsString($value)) {
                $value = StringUtils.ToLowerCase(<string>$value);
                this.valueString = $value;
                if (!ObjectValidator.IsEmptyOrNull($toolchain)) {
                    $toolchain = StringUtils.ToLowerCase($toolchain);
                    this.Toolchain($toolchain);
                }
                if (StringUtils.StartsWith(<string>$value, "lib")) {
                    $value = StringUtils.Remove(<string>$value, "-" + ProductType.STATIC);
                    if (StringUtils.Contains(<string>$value, "-" + ProductType.SHARED)) {
                        $value = StringUtils.Remove(<string>$value, "-" + ProductType.SHARED);
                        this.Type(ProductType.SHARED);
                    }
                }

                const splitters : number = StringUtils.OccurrenceCount(<string>$value, "-");
                if (splitters === 0) {
                    if (StringUtils.Contains(<string>$value, "32")) {
                        LogIt.Warning(" Conversion to 64-bit architecture has been done, " +
                            "because specification of 32-bit architecture has been deprecated for inline platform values. " +
                            "Use platform object instead if 32-bit architecture is required.");
                        $value = StringUtils.Remove(<string>$value, "32");
                    }
                    $value = StringUtils.Remove(<string>$value, "64");

                    switch ($value) {
                    case "web":
                        if (this.Toolchain() === ToolchainType.JDK) {
                            this.Type(ProductType.SHARED);
                        }
                        break;

                    case OSType.WIN:
                        this.Type(ProductType.APP);
                        if (ObjectValidator.IsEmptyOrNull($toolchain)) {
                            this.Toolchain(ToolchainType.CHROMIUM_RE);
                        }
                        break;
                    case OSType.LINUX:
                        this.Type(ProductType.APP);
                        this.OS(OSType.LINUX);
                        if (ObjectValidator.IsEmptyOrNull($toolchain)) {
                            this.Toolchain(ToolchainType.GCC);
                        }
                        break;
                    case OSType.MAC:
                        this.Type(ProductType.APP);
                        this.OS(OSType.MAC);
                        break;
                    case ToolchainType.ARM:
                        if (ObjectValidator.IsEmptyOrNull($toolchain) || $toolchain !== ToolchainType.NODEJS) {
                            this.OS(OSType.LINUX);
                            this.Toolchain(ToolchainType.ARM);
                        } else {
                            this.Type(ProductType.APP);
                            this.OS(OSType.ARM);
                            this.Toolchain(ToolchainType.NODEJS);
                        }
                        break;
                    case ToolchainType.AARCH64:
                        if (ObjectValidator.IsEmptyOrNull($toolchain) || $toolchain !== ToolchainType.NODEJS) {
                            this.OS(OSType.LINUX);
                            this.Toolchain(ToolchainType.AARCH64);
                        } else {
                            this.Type(ProductType.APP);
                            this.OS(OSType.ARM);
                            this.Toolchain(ToolchainType.NODEJS);
                        }
                        break;
                    case ToolchainType.AARCH32:
                        this.Arch(ArchType.X32);
                        if (ObjectValidator.IsEmptyOrNull($toolchain) || $toolchain !== ToolchainType.NODEJS) {
                            this.OS(OSType.LINUX);
                            this.Toolchain(ToolchainType.AARCH32);
                        } else {
                            this.Type(ProductType.APP);
                            this.OS(OSType.ARM);
                            this.Toolchain(ToolchainType.NODEJS);
                        }
                        break;

                    case "lib":
                    case "libwin":
                        if (ObjectValidator.IsEmptyOrNull($toolchain)) {
                            this.Toolchain(ToolchainType.GCC);
                        }
                        break;
                    case "liblinux":
                        this.OS(OSType.LINUX);
                        if (this.Toolchain() !== ToolchainType.ARM && this.Toolchain() !==
                            ToolchainType.AARCH32 && this.Toolchain() !== ToolchainType.AARCH64) {
                            this.Toolchain(ToolchainType.GCC);
                        }
                        break;
                    case "libmac":
                        this.OS(OSType.MAC);
                        break;

                    case ToolchainType.ECLIPSE:
                        if (ObjectValidator.IsEmptyOrNull($toolchain)) {
                            this.Type(ProductType.PLUGIN);
                            this.Toolchain(ToolchainType.ECLIPSE);
                        } else if ($toolchain === ToolchainType.JDK) {
                            this.Type(ProductType.SHARED);
                            this.Toolchain(ToolchainType.ECLIPSE);
                        }
                        break;
                    case ToolchainType.IDEA:
                        if (ObjectValidator.IsEmptyOrNull($toolchain)) {
                            this.Type(ProductType.PLUGIN);
                            this.Toolchain(ToolchainType.IDEA);
                        } else if ($toolchain === ToolchainType.JDK) {
                            this.Type(ProductType.SHARED);
                            this.Toolchain(ToolchainType.IDEA);
                        }
                        break;

                    case OSType.ANDROID:
                        this.Type(ProductType.APP);
                        this.OS(OSType.ANDROID);
                        this.Toolchain(ToolchainType.PHONEGAP);
                        break;
                    case OSType.WINPHONE:
                        this.Type(ProductType.APP);
                        this.OS(OSType.WINPHONE);
                        this.Toolchain(ToolchainType.PHONEGAP);
                        break;

                    case ToolchainType.NODEJS:
                        this.Type(ProductType.SHARED);
                        this.Toolchain(ToolchainType.NODEJS);
                        break;
                    case ProductType.INSTALLER:
                        this.Type(ProductType.INSTALLER);
                        break;

                    default:
                        LogIt.Debug("Unrecognized build argument option: {0}", $value);
                        break;
                    }
                } else if (splitters === 2) {
                    const parts : string[] = StringUtils.Split(<string>$value, "-");
                    this.Type(parts[0]);
                    this.OS(StringUtils.Remove(parts[1], "32", "64"));
                    this.Arch(StringUtils.Contains(parts[1], "32") ? "32" : "64");
                    this.Toolchain(parts[2]);
                } else {
                    LogIt.Error("Parsing of \"" + $value + "\" has failed, " +
                        "because build platform must have 0 or 2 \"-\" delimiters.");
                }
            } else {
                const platformObject : IBuildPlatform = <IBuildPlatform>$value;
                this.Type(platformObject.type);
                this.Arch(platformObject.arch);
                this.Toolchain(platformObject.toolchain);
                if (this.Toolchain() === ToolchainType.CHROMIUM_RE) {
                    this.Type(ProductType.APP);
                } else if (this.Toolchain() === ToolchainType.ARM || this.Toolchain() === ToolchainType.AARCH32 ||
                    this.Toolchain() === ToolchainType.AARCH64) {
                    this.OS(OSType.LINUX);
                } else if (this.Toolchain() === ToolchainType.APT_GET) {
                    this.Type(ProductType.PACKAGE);
                    this.OS(OSType.LINUX);
                } else {
                    this.OS((<IBuildPlatform>$value).os);
                }
            }
        }
    }

    public getBuildPlatform() : IBuildPlatform {
        return {
            arch     : this.arch.toString(),
            os       : this.os.toString(),
            toolchain: this.toolchain.toString(),
            type     : this.type.toString()
        };
    }

    public getRunPlatform() : IBuildPlatform {
        const runPlatform : IRunPlatform = this.getBuildPlatform();
        runPlatform.value = this.ValueString();
        return runPlatform;
    }

    public Type($value? : ProductType) : ProductType {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            $value = StringUtils.ToLowerCase($value.toString());
        }
        return this.type = Property.EnumType(this.type, $value, ProductType, ProductType.STATIC);
    }

    public OS($value? : OSType) : OSType {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            $value = StringUtils.ToLowerCase($value.toString());
        }
        return this.os = Property.EnumType(this.os, $value, OSType, OSType.WIN);
    }

    public Arch($value? : ArchType) : ArchType {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            $value = StringUtils.ToLowerCase($value.toString());
            if ($value === "32-bit" || $value === "32bit" || $value === "x32") {
                $value = ArchType.X32;
            } else if ($value === "64-bit" || $value === "64bit" || $value === "x64") {
                $value = ArchType.X64;
            }
        }
        return this.arch = Property.EnumType(this.arch, $value, ArchType, ArchType.X64);
    }

    public Toolchain($value? : ToolchainType) : ToolchainType {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            $value = StringUtils.ToLowerCase($value.toString());
            $value = StringUtils.Remove($value.toString(), "-");
        }
        return this.toolchain = Property.EnumType(this.toolchain, $value, ToolchainType, ToolchainType.NONE);
    }

    public Value() : string {
        const arch : string = this.arch === ArchType.X32 ? "32" : "";
        return this.type + "-" + this.os + arch + "-" + this.toolchain;
    }

    public ValueString() : string {
        return this.valueString;
    }

    public toString() : string {
        return this.Value();
    }

    private setDefaults() : void {
        this.type = ProductType.STATIC;
        this.os = OSType.WIN;
        this.arch = ArchType.X64;
        this.toolchain = ToolchainType.NONE;
    }
}
