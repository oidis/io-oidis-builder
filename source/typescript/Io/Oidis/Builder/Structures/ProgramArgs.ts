/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ProgramArgs as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/Structures/ProgramArgs.js";
import { CliTaskType } from "../Enums/CliTaskType.js";
import { Loader } from "../Loader.js";

export class ProgramArgs extends Parent {
    protected readonly internalOptions : IProgramArgsOptions;
    private tasks : string[];
    private isHubClient : boolean;
    private isServiceClient : boolean;
    private isBuild : boolean;
    private isTest : boolean;
    private isDeploy : boolean;
    private readonly options : string[];
    private port : number;
    private isForce : boolean;
    private startAgent : boolean;
    private agentName : string;
    private isExternalModuleTask : boolean;
    private isRun : boolean;
    private chainId : string;
    private runConfig : string;
    private isTargetInfo : boolean;

    constructor() {
        super();
        this.tasks = [];
        this.options = [];
        this.isHubClient = false;
        this.isServiceClient = false;
        this.isBuild = false;
        this.isTest = false;
        this.isDeploy = false;
        this.port = -1;
        this.isForce = false;
        this.isTargetInfo = false;
        this.internalOptions = {
            noServer          : false,
            file              : "",
            to                : "",
            newVersion        : "",
            description       : "",
            forwardArgs       : [],
            noSudo            : false,
            noTarget          : false,
            selfextract       : false,
            selfupdate        : false,
            project           : "",
            isServiceTask     : false,
            isAgentTask       : false,
            isForwardTask     : false,
            withDependencies  : false,
            withTest          : false,
            solution          : "default",
            withProductsSync  : false,
            deployOptions     : {},
            targetInfoProperty: "version",
            testIgnore        : null
        };
        this.startAgent = false;
        this.agentName = "";
        this.isExternalModuleTask = false;
        this.chainId = "";
        this.runConfig = "default";
    }

    public getTasks() : string[] {
        return this.tasks;
    }

    public IsServiceClient($value? : boolean) : boolean {
        return this.isServiceClient = Property.Boolean(this.isServiceClient, $value);
    }

    public IsHubClient($value? : boolean) : boolean {
        return this.isHubClient = Property.Boolean(this.isHubClient, $value);
    }

    public Port($value? : number) : number {
        return this.port = Property.PositiveInteger(this.port, $value, 1);
    }

    public IsForce($value? : boolean) : boolean {
        return this.isForce = Property.Boolean(this.isForce, $value);
    }

    public getOptions() : IProgramArgsOptions {
        return this.internalOptions;
    }

    public StartAgent($value? : boolean) : boolean {
        return this.startAgent = Property.Boolean(this.startAgent, $value);
    }

    public AgentName($value? : string) : string {
        return this.agentName = Property.String(this.agentName, $value);
    }

    public ChainId($value? : string) : string {
        return this.chainId = Property.String(this.chainId, $value);
    }

    public IsForwardedTask() : boolean {
        return !ObjectValidator.IsEmptyOrNull(process.env.IS_OIDIS_PROXY_TASK) && <any>process.env.IS_OIDIS_PROXY_TASK === 1 ||
            this.internalOptions.isServiceTask || this.internalOptions.isAgentTask || this.internalOptions.isForwardTask;
    }

    public IsServiceTask() : boolean {
        return this.internalOptions.isServiceTask;
    }

    public IsAgentTask() : boolean {
        return this.internalOptions.isAgentTask;
    }

    public IsExternalModuleTask() : boolean {
        return this.isExternalModuleTask;
    }

    public IsTargetInfo($value? : boolean) : boolean {
        return this.isTargetInfo = Property.Boolean(this.isTargetInfo, $value);
    }

    public PrintHelp() : void {
        let body : string = "";
        let question : string = "";
        if (this.isBuild) {
            body =
                "Usage: build <type> [--with-test]                                            \n" +
                "                                                                             \n" +
                "where <type> is one of:                                                      \n" +
                "   prod, eap, dev, help                                                      \n" +
                "                                                                             \n" +
                "Tasks description:                                                           \n" +
                "   help                Involved overview                                     \n" +
                "   dev                 Build development type of release                     \n" +
                "   dev --with-test     Build development type of release with testing        \n" +
                "   eap                 Build EAP type of release                             \n" +
                "                         all tests are skipped                               \n" +
                "                         all development code is removed                     \n" +
                "                         code optimize is turned on                          \n" +
                "   prod                Build production type of release                      \n" +
                "                         all tests are performed                             \n";
            question = "Hit Enter for exit or specify build type and args";
        } else if (this.isTest) {
            body =
                "Usage: test <type>                                                           \n" +
                "                                                                             \n" +
                "where <type> is one of:                                                      \n" +
                "   all, unit, coverage, selenium, lint                                       \n" +
                "                                                                             \n" +
                "Tasks description:                                                           \n" +
                "   help                  Involved overview                                   \n" +
                "   all                   Run all implemented tests                           \n" +
                "   unit                  Run only unit tests                                 \n" +
                "   unit <filePath>       Run only single unit test with desired file path    \n" +
                "                           from the root test folder                         \n" +
                "   coverage              Run only code coverage test                         \n" +
                "   selenium              Run only integral testing provided by selenium      \n" +
                "   lint                  Run only lint tests                                 \n" +
                "   --ignore-tests=<list> Run unit tests with specified ignor list            \n";
            question = "Hit Enter for exit or specify test type";
        } else if (this.isDeploy) {
            body =
                "Usage: deploy:<type>                                                         \n" +
                "where <type> is one of:                                                      \n" +
                "    dev, eap, prod     Select one of deploy.server profiles                  \n" +
                "Tasks description:                                                           \n" +
                "   --action=<value>    list|geturl|redeploy|getmethods                       \n" +
                "   --id=<value>        Model ID                                              \n" +
                "   --upload-to=<value> dev|eap|prod deploy.server configuration or url       \n" +
                "   --filter=<value>    Coma separated list of model properties               \n" +
                "                       [name,version,platform,id,release,buildtime]          \n" +
                "                       i.e.: 'name=builder,version=2023.1.1'                 \n";
            question = "Hit Enter for exit or specify test type";
        } else {
            body =
                "Basic options:                                                               \n" +
                "   -h [ --help ]      Prints application help description.                   \n" +
                "   -v [ --version ]   Prints application version message.                    \n" +
                "   -p [--path]        Print current Oidis Builder location.                  \n" +
                "   --base=<path>      Specify project base path.                             \n" +
                "   --info=<property>  Prints target metadata.                                \n" +
                "                                                                             \n" +
                "Mode options:                                                                \n" +
                "   service [stop]     Start or stop Oidis Builder service                    \n" +
                "   agent              Connect Oidis Builder service to Oidis Hub             \n" +
                "   --agent-name       Specify name for agent instance                        \n" +
                "                                                                             \n" +
                "Tasks options:                                                               \n" +
                "   update             Update current Oidis Builder instance                  \n" +
                "   install            Prepare project dependencies and settings              \n" +
                "   build              Build project with desired type and runtime env.       \n" +
                "   test               Run test of source code or linting                     \n" +
                "   run                Build and run development release as plugin            \n" +
                "   docs               Provide automatically generated documentation          \n" +
                "   clean              Clean up all files generated by builder tasks          \n" +
                "   deploy             Deploy package                                         \n" +
                "   <task_name>        Execute specific builder task.                         \n" +
                "                                                                             \n" +
                "Other options:                                                               \n" +
                "   --force            Force execute task.                                    \n" +
                "   --service          Specify if task should be forwarded to service.        \n" +
                "   --port=<value>     Specify port for service mode.                         \n" +
                "   --hub              Specify if task should be forwarded to Oidis Hub.      \n" +
                "   --solution         Specify name of solution for project configuration.    \n" +
                "   -- <cmd> <args>    Forward args directly to Oidis Builder toolchain.      \n";
        }
        this.getHelp("" +
            "Oidis Framework Builder                                                      \n" +
            "   - build tool based on Node.js                                             \n" +
            "                                                                             \n" +
            "   copyright:         Copyright 2014-2016 Freescale Semiconductor, Inc.      \n" +
            "                      Copyright 2017-2019 NXP                                \n" +
            "                      Copyright " + StringUtils.YearFrom(2019) + " Oidis                              \n" +
            "   author:            Oidis z.s., info@oidis.io                              \n" +
            "   license:           BSD-3-Clause License distributed with this material    \n",
            body, "",
            question
        );
    }

    public IsBaseTask() : boolean {
        return this.IsVersion() || this.IsPath() || this.IsHelp();
    }

    public IsRun() : boolean {
        return this.isRun;
    }

    public RunConfig() : string {
        return this.runConfig;
    }

    public Normalize($args : string[]) : string[] {
        return $args;
    }

    protected processArgs($args : ArrayList<string>) : void {
        $args.foreach(($value : string, $key : string) : void => {
            if (!this.isExternalModuleTask) {
                if (!this.processArg($key, $value)) {
                    Echo.Printf("Unrecognized program option: {0}", $key);
                }
            } else {
                this.options.push($key);
            }
        });
        if (this.isExternalModuleTask) {
            this.internalOptions.forwardArgs = [].concat(this.options);
        } else if (this.isServiceClient || this.isHubClient) {
            this.internalOptions.forwardArgs = [].concat(this.tasks.concat(this.options));
        } else {
            if (this.isBuild || this.isTest || this.isDeploy) {
                if (!this.isDeploy) {
                    this.IsHelp($args.Length() === 1 && this.options.length === 0);
                }
                let taskOption : string = "";
                let isUnit : boolean = false;
                this.options.forEach(($option : string) : void => {
                    if (this.isBuild) {
                        switch ($option) {
                        case "prod":
                        case "eap":
                        case "beta":
                        case "alpha":
                        case "dev":
                            taskOption = $option;
                            break;
                        case "--with-test":
                            this.internalOptions.withTest = true;
                            break;
                        case "--skip-test":
                            this.internalOptions.withTest = false;
                            break;
                        default:
                            process.stderr.write("" +
                                "ERROR:\n" +
                                "   \"" + $option + "\" is not any of supported build type.");
                            setTimeout(() : void => {
                                Loader.getInstance().Exit(-1);
                            }, 3000);
                            break;
                        }
                    } else if (this.isDeploy) {
                        switch ($option) {
                        case "dev":
                        case "eap":
                        case "prod":
                            taskOption = $option;
                            break;
                        default:
                            process.stderr.write("" +
                                "ERROR:\n" +
                                "   \"" + $option + "\" is not any of supported deploy type.");
                            Loader.getInstance().Exit(-1);
                            break;
                        }
                    } else {
                        switch ($option) {
                        case "all":
                        case "selenium":
                        case "lint":
                            this.tasks.push("test:" + $option);
                            break;
                        case "unit":
                            this.tasks.push("run-unit-test");
                            isUnit = true;
                            break;
                        case "coverage":
                            this.tasks.push("run-coverage-test");
                            isUnit = true;
                            break;
                        default:
                            if (isUnit && ObjectValidator.IsEmptyOrNull(this.internalOptions.file)) {
                                this.internalOptions.file = $option;
                            } else {
                                process.stderr.write("" +
                                    "ERROR:\n" +
                                    "   \"" + $option + "\" is not any of supported test type.");
                                setTimeout(() : void => {
                                    Loader.getInstance().Exit(-1);
                                }, 3000);
                            }
                            break;
                        }
                    }
                });
                if (!ObjectValidator.IsEmptyOrNull(taskOption)) {
                    this.tasks.push((this.isDeploy ? "deploy:" : "build:") + taskOption);
                } else if (this.isDeploy) {
                    this.tasks.push("deploy");
                }
            } else if (this.options.length !== 0) {
                this.tasks = this.tasks.concat(this.options);
                if (this.IsServiceClient() || this.IsHubClient()) {
                    this.internalOptions.forwardArgs = [].concat(this.tasks);
                    this.internalOptions.forwardArgs.shift();
                    this.tasks = [this.tasks[0]];
                }
            }
        }
    }

    protected processArg($key : string, $value : string) : boolean {
        switch ($key) {
        case "service":
            this.StartServer(true);
            this.internalOptions.noTarget = true;
            break;
        case "--port":
            this.Port(StringUtils.ToInteger($value));
            break;
        case "stop":
            this.StartServer(false);
            this.StopServer(true);
            this.internalOptions.noTarget = true;
            break;
        case "agent":
            this.StartAgent(true);
            this.internalOptions.noTarget = true;
            break;
        case "--agent-name":
            this.AgentName($value);
            break;
        case "--service":
            this.IsServiceClient(true);
            break;
        case "--hub":
            this.IsHubClient(true);
            break;
        case "--base":
            this.TargetBase($value);
            break;
        case "--solution":
        case "-s":
            this.internalOptions.solution = $value;
            break;
        case "--chain-id":
            this.ChainId($value);
            break;
        case "--run-config":
            this.runConfig = $value;
            break;
        case "install":
        case "docs":
            this.tasks.push($key);
            break;
        case "clean":
            this.tasks.push("project-cleanup:all");
            break;
        case "build":
            this.isBuild = true;
            if (this.IsServiceClient() || this.IsHubClient()) {
                this.tasks.push($key);
            }
            break;
        case "test":
            this.isTest = true;
            if (this.IsServiceClient() || this.IsHubClient()) {
                this.tasks.push($key);
            }
            break;
        case "deploy":
            this.isDeploy = true;
            break;
        case "task":
            this.tasks.push($value);
            break;
        case "grunt":
            if (this.IsServiceClient() || this.IsHubClient()) {
                this.tasks.push($key);
            }
            break;
        case "run":
            this.tasks.push($key);
            this.isRun = true;
            break;
        case CliTaskType.ALPHA:
        case CliTaskType.BETA:
        case CliTaskType.EAP:
        case CliTaskType.PROD:
        case CliTaskType.DEV:
        case CliTaskType.DEV_SKIP_TEST:
            if ($key === CliTaskType.ALPHA || $key === CliTaskType.BETA) {
                $key = CliTaskType.EAP;
            }
            if (!this.isBuild) {
                this.tasks.push("build:" + $key);
            } else {
                this.options.push($key);
            }
            break;
        case CliTaskType.REBUILD:
        case CliTaskType.REBUILD_DEV:
        case CliTaskType.REBUILD_DEV_SKIP_TEST:
        case CliTaskType.REBUILD_EAP:
        case CliTaskType.REBUILD_EAP_SKIP_TEST:
            this.tasks.push("build-machine:" + $key);
            break;
        case "--with-test":
            this.internalOptions.withTest = true;
            break;
        case "--force":
            this.IsForce(true);
            break;
        case "update":
            this.internalOptions.selfupdate = true;
            this.internalOptions.noTarget = true;
            break;
        case "--info":
            this.IsTargetInfo(true);
            this.internalOptions.targetInfoProperty = $value;
            break;
        case "--":
            this.options.splice(0, this.options.length);
            this.isExternalModuleTask = true;
            this.internalOptions.noTarget = true;
            break;
        default:
            if (!this.processOtherOptions($key, $value) && !super.processArg($key, $value)) {
                if (StringUtils.StartsWith($key, "run:")) {
                    this.tasks.push($key);
                    this.isRun = true;
                } else if (StringUtils.StartsWith($key, "deploy:")) {
                    this.isDeploy = true;
                    this.options.push(StringUtils.Remove($key, "deploy:"));
                } else {
                    this.options.push($key);
                }
            }
            break;
        }
        return true;
    }

    protected processOtherOptions($key : string, $value : string) : boolean {
        switch ($key) {
        case "--no-server":
            this.internalOptions.noServer = true;
            break;
        case "--file":
            this.internalOptions.file = $value;
            break;
        case "--to":
            this.internalOptions.to = $value;
            break;
        case "--new-version":
            this.internalOptions.newVersion = $value;
            break;
        case "--description":
            this.internalOptions.description = $value;
            break;
        case "--no-sudo":
            this.internalOptions.noSudo = true;
            break;
        case "--no-target":
            this.internalOptions.noTarget = true;
            break;
        case "--selfextract":
            this.internalOptions.noTarget = true;
            this.internalOptions.selfextract = true;
            break;
        case "--project":
            this.internalOptions.project = $value;
            break;
        case "--service-task":
            this.internalOptions.isServiceTask = true;
            this.internalOptions.noServer = true;
            break;
        case "--agent-task":
            this.internalOptions.isAgentTask = true;
            this.internalOptions.noServer = true;
            break;
        case "--forward-task":
            this.internalOptions.isForwardTask = true;
            break;
        case "--with-dependencies":
            this.internalOptions.withDependencies = true;
            break;
        case "--with-products-sync":
            this.internalOptions.withProductsSync = true;
            break;
        case "--action":
            this.internalOptions.deployOptions.action = $value;
            break;
        case "--filter":
            this.internalOptions.deployOptions.filter = $value;
            break;
        case "--upload-to":
            this.internalOptions.deployOptions.uploadTo = $value;
            break;
        case "--id":
            this.internalOptions.deployOptions.id = $value;
            break;
        case "--ignore-tests":
            this.internalOptions.testIgnore = StringUtils.Split($value, ",");
            break;
        default:
            return false;
        }
        return true;
    }
}

export interface IProgramArgsOptions {
    noServer : boolean;
    file : string;
    to : string;
    newVersion : string;
    description : string;
    forwardArgs : string[];
    noSudo : boolean;
    noTarget : boolean;
    selfextract : boolean;
    selfupdate : boolean;
    project : string;
    isServiceTask : boolean;
    isAgentTask : boolean;
    isForwardTask : boolean;
    withDependencies : boolean;
    withTest : boolean;
    solution : string;
    withProductsSync : boolean;
    deployOptions : IDeployOptions;
    targetInfoProperty : string;
    testIgnore : string[];
}

export interface IDeployOptions {
    action? : string;
    filter? : string;
    uploadTo? : string;
    id? : string;
}

// generated-code-start
/* eslint-disable */
export const IProgramArgsOptions = globalThis.RegisterInterface(["noServer", "file", "to", "newVersion", "description", "forwardArgs", "noSudo", "noTarget", "selfextract", "selfupdate", "project", "isServiceTask", "isAgentTask", "isForwardTask", "withDependencies", "withTest", "solution", "withProductsSync", "deployOptions", "targetInfoProperty", "testIgnore"]);
export const IDeployOptions = globalThis.RegisterInterface(["action", "filter", "uploadTo", "id"]);
/* eslint-enable */
// generated-code-end
