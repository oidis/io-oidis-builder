/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { ProgramArgs as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/Structures/ProgramArgs.js";
import { IBuildArgs } from "../Interfaces/IAppConfiguration.js";
import { IProject } from "../Interfaces/IProject.js";
import { IProperties } from "../Interfaces/IProperties.js";
import { ProgramArgs } from "./ProgramArgs.js";

export class TaskEnvironment extends Parent {
    private project : IProject;
    private properties : IProperties;
    private programArgs : ProgramArgs;
    private buildArgs : IBuildArgs;

    public Project($value? : IProject) : IProject {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.project = $value;
        }
        return this.project;
    }

    public Properties($value? : IProperties) : IProperties {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.properties = $value;
        }
        return this.properties;
    }

    public ProgramArgs($value? : ProgramArgs) : ProgramArgs {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.programArgs = $value;
        }
        return this.programArgs;
    }

    public BuildArgs($value? : IBuildArgs) : IBuildArgs {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            this.buildArgs = $value;
        }
        return this.buildArgs;
    }
}
