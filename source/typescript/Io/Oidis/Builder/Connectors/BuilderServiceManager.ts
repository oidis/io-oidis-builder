/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { CallbackResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/CallbackResponse.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { Loader } from "../Loader.js";
import { FileSystemHandler } from "./FileSystemHandler.js";

export class BuilderServiceManager extends BaseConnector {

    @Extern()
    public static StartTask($projectPath : string, $task : string,
                            $callback? : (($status : boolean, $message : string) => void) | IResponse,
                            $syncMessage? : string) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();

        if (Loader.getInstance().getProgramArgs().getOptions().noTarget) {
            $projectPath = Loader.getInstance().getProgramArgs().ProjectBase();
        }

        if (fileSystem.Exists($projectPath) || Loader.getInstance().getProgramArgs().getOptions().noTarget) {
            let configPath : string = $projectPath + "/package.conf.jsonp";
            if (!fileSystem.Exists(configPath)) {
                configPath = configPath.slice(0, -1);
            }
            if (fileSystem.Exists(configPath) ||
                Loader.getInstance().getProgramArgs().getOptions().noTarget) {
                const serviceName : string = BuilderServiceManager.getServiceName($projectPath, $task);
                const taskResponse : IResponse = new CallbackResponse(($exitCode : number, $std : string[]) : void => {
                    if ($exitCode !== 0) {
                        response.Send(false, $std[0] + $std[1]);
                    }
                });
                if (ObjectValidator.IsEmptyOrNull($syncMessage)) {
                    $syncMessage = "Register service PID:";
                }
                taskResponse.OnChange = ($message : string) : any => {
                    if (StringUtils.ContainsIgnoreCase($message, $syncMessage)) {
                        BuilderServiceManager.IsTaskOnline($projectPath, $task, true, ($status : boolean) : void => {
                            if (!$status) {
                                response.Send(false, "Oidis Builder service did not start correctly.");
                            } else {
                                const servicePid : number = BuilderServiceManager.getRegister().Variable(serviceName);
                                response.Send(true, "Oidis Builder service [" + servicePid + "] started successfully.");
                            }
                        });
                    }
                };
                const start : any = () : void => {
                    LogIt.Info("Starting Oidis Builder service " + serviceName + " ...");
                    let cmd : string;
                    const args : string[] = [];
                    let path : string = "";
                    if (!EnvironmentHelper.IsEmbedded()) {
                        cmd = "node";
                        args.push(Loader.getInstance().getProgramArgs().BinBase() + "/resource/javascript/loader.min.js");
                        path = Loader.getInstance().getProgramArgs().BinBase() + "/../../dependencies/nodejs";
                    } else {
                        cmd = Loader.getInstance().getEnvironmentArgs().getAppName();
                        if (!EnvironmentHelper.IsWindows()) {
                            cmd = "./" + cmd;
                        } else {
                            cmd += ".exe";
                        }
                    }
                    args.push("task=" + $task);
                    if (Loader.getInstance().getProgramArgs().getOptions().noTarget) {
                        args.push("--no-target");
                    } else {
                        args.push("--base=\"" + $projectPath + "\"");
                    }
                    if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getProgramArgs().AgentName())) {
                        args.push("--agent-name=\"" + Loader.getInstance().getProgramArgs().AgentName() + "\"");
                    }
                    if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getProgramArgs().RunConfig())) {
                        args.push("--run-config=\"" + Loader.getInstance().getProgramArgs().RunConfig() + "\"");
                    }
                    Loader.getInstance().getTerminal().Spawn(cmd, args, {
                        cwd     : Loader.getInstance().getProgramArgs().ProjectBase(),
                        detached: true,
                        env     : {
                            NODE_PATH: process.env.NODE_PATH,
                            PATH     : Loader.getInstance().getTerminal().NormalizeEnvironmentPath(path)
                        },
                        shell   : false,
                        verbose : false
                    }, taskResponse);
                };

                BuilderServiceManager.IsTaskOnline($projectPath, $task, true, ($status : boolean) : void => {
                    if (!$status) {
                        start();
                    } else {
                        BuilderServiceManager.StopTask($projectPath, $task, ($status : boolean) => {
                            if ($status) {
                                start();
                            } else {
                                response.Send(false, "Unable to start Oidis Builder service.");
                            }
                        });
                    }
                });
            } else {
                response.Send(false, "Oidis Project has not been detected at: " + $projectPath);
            }
        } else {
            response.Send(false, "Project does not exist at: " + $projectPath);
        }
    }

    @Extern()
    public static StopTask($projectPath : string, $task : string,
                           $callback? : (($status : boolean, $message : string) => void) | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();

        if (Loader.getInstance().getProgramArgs().getOptions().noTarget) {
            $projectPath = Loader.getInstance().getProgramArgs().ProjectBase();
        }

        if (fileSystem.Exists($projectPath)) {
            let configPath : string = $projectPath + "/package.conf.jsonp";
            if (!fileSystem.Exists(configPath)) {
                configPath = configPath.slice(0, -1);
            }
            if (fileSystem.Exists(configPath) || Loader.getInstance().getProgramArgs().getOptions().noTarget) {
                const serviceName : string = BuilderServiceManager.getServiceName($projectPath, $task);
                const configPath : string = "/resource/data/Io/Oidis/Builder";
                const configs : string[] = [
                    $projectPath + configPath + "/connector.config.jsonp",
                    $projectPath + configPath + "/connector.config.js",
                    $projectPath + "/build/target" + configPath + "/connector.config.jsonp",
                    $projectPath + "/build/target" + configPath + "/connector.config.js"
                ];
                for (const config of configs) {
                    if (fileSystem.Exists(config)) {
                        fileSystem.Delete(config);
                    }
                }
                LogIt.Info("Trying to stop Oidis Builder service " + serviceName + " ...");
                BuilderServiceManager.IsTaskOnline($projectPath, $task, true, ($status : boolean) : void => {
                    if (!$status) {
                        response.Send(true, "Oidis Builder service stop skipped: service is not running.");
                    } else {
                        const register : IPersistenceHandler = BuilderServiceManager.getRegister();
                        if (register.Exists(serviceName)) {
                            try {
                                process.kill(register.Variable(serviceName));
                                response.Send(true, "Oidis Builder service stopped successfully.");
                            } catch (ex) {
                                LogIt.Warning(ex.stack);
                                response.Send(false, "Oidis Builder service did not stop correctly.");
                            }
                        } else {
                            response.Send(true, "Oidis Builder service stopped successfully.");
                        }
                    }
                });
            } else {
                response.Send(false, "Oidis Project has not been detected at: " + $projectPath);
            }
        } else {
            response.Send(false, "Project does not exist at: " + $projectPath);
        }
    }

    @Extern()
    public static IsTaskOnline($projectPath : string, $task : string, $verbose : boolean = true,
                               $callback? : (($status : boolean) => void) | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        const serviceName : string = BuilderServiceManager.getServiceName($projectPath, $task);
        if ($verbose) {
            LogIt.Info("Checking Oidis Builder service " + serviceName + " status ...");
        }
        const register : IPersistenceHandler = BuilderServiceManager.getRegister();
        if (register.Exists(serviceName)) {
            let exits : boolean = true;
            try {
                process.kill(register.Variable(serviceName), 0);
            } catch (e) {
                exits = false;
            }
            if (exits) {
                response.Send(true);
            } else {
                register.Destroy(serviceName);
                response.Send(false);
            }
        } else {
            response.Send(false);
        }
    }

    @Extern()
    public static Start($projectPath : string, $callback? : (($status : boolean, $message : string) => void) | IResponse) : void {
        const taskOption : string = $projectPath !== Loader.getInstance().getProgramArgs().ProjectBase() ? "restart" : "service";
        BuilderServiceManager.StartTask($projectPath, "builder-server:" + taskOption, $callback,
            "has been started at:");
    }

    @Extern()
    public static Stop($projectPath : string,
                       $callback? : (($status : boolean, $message : string) => void) | IResponse) : void {
        const taskOption : string = $projectPath !== Loader.getInstance().getProgramArgs().ProjectBase() ? "restart" : "service";
        BuilderServiceManager.StopTask($projectPath, "builder-server:" + taskOption, $callback);
    }

    @Extern()
    public static Clear($callback? : (($status : boolean, $message : string) => void) | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        const register : IPersistenceHandler = BuilderServiceManager.getRegister();
        const services : ArrayList<number> = (<any>register).variables;
        if (!services.IsEmpty()) {
            let passed : boolean = true;
            (<any>register).variables.foreach(($pid : number) : void => {
                try {
                    process.kill($pid);
                } catch (ex) {
                    LogIt.Warning(ex.stack);
                    passed = false;
                }
            });
            register.Clear();
            if (passed) {
                response.Send(true, "All Oidis Builder service instances have been stopped successfully.");
            } else {
                response.Send(false, "Failed to stop some of Oidis Builder services.");
            }
        } else {
            response.Send(true, "Skipped: none of Oidis Builder service is running.");
        }
    }

    @Extern()
    public static IsOnline($projectPath : string, $verbose : boolean = true,
                           $callback? : (($status : boolean) => void) | IResponse) : void {
        const taskOption : string = $projectPath !== Loader.getInstance().getProgramArgs().ProjectBase() ? "restart" : "service";
        BuilderServiceManager.IsTaskOnline($projectPath, "builder-server:" + taskOption, $verbose, $callback);
    }

    public static RemovePIDRecord() : void {
        const serviceName : string = this.getCurrentServiceName();
        const register : IPersistenceHandler = this.getRegister();
        if (register.Exists(serviceName)) {
            register.Destroy(serviceName);
        }
    }

    public static CreatePIDRecord() : void {
        const serviceName : string = this.getCurrentServiceName();
        this.getRegister().Variable(serviceName, process.pid);
        if (Loader.getInstance().getProgramArgs().IsForwardedTask()) {
            Echo.Println("Register service PID: " + process.pid);
        }
    }

    private static getCurrentServiceName() : string {
        return this.getServiceName(Loader.getInstance().getProgramArgs().getOptions().noTarget ?
                Loader.getInstance().getProgramArgs().ProjectBase() : Loader.getInstance().getProgramArgs().TargetBase(),
            Loader.getInstance().getProgramArgs().getTasks().join(","));
    }

    private static getServiceName($projectPath : string, $task : string) : string {
        return StringUtils.getSha1(StringUtils.Replace($projectPath, "\\", "/") + $task);
    }

    private static getRegister() : IPersistenceHandler {
        const register : any = PersistenceFactory.getPersistence(BuilderServiceManager.ClassName());
        register.session = null;
        register.variables = null;
        return register;
    }
}
