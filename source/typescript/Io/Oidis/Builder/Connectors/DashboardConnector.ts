/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IProject } from "../Interfaces/IProject.js";
import { Loader } from "../Loader.js";

export class DashboardConnector extends BaseConnector {

    @Extern()
    public static getSchema($callback : IResponse) : void {
        this.getInstance<DashboardConnector>().getSchema($callback);
    }

    @Extern()
    public static getProjectConfig($callback : IResponse) : void {
        const config : IProject = JsonUtils.DeepClone(this.getInstance<DashboardConnector>().getProjectConfig(), false);
        config.schema = "";
        ResponseFactory.getResponse($callback).Send(config);
    }

    @Extern()
    public static SaveProjectConfig($config : IProject, $callback : IResponse) : void {
        this.pipe<DashboardConnector>($callback).SaveProjectConfig($config);
    }

    public getSchema($callback : (($schema : any) => void) | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        const parser : any = require("json-schema-ref-parser");
        try {
            parser
                .dereference(JsonUtils.DeepClone(this.getProjectConfig().schema))
                .then(($schema : any) : void => {
                    response.Send(JsonUtils.DeepClone($schema, false));
                })
                .catch(($err : Error) : void => {
                    response.OnError($err);
                });
        } catch (ex) {
            response.OnError(ex);
        }
    }

    public getProjectConfig() : IProject {
        return Loader.getInstance().getProjectConfig();
    }

    public SaveProjectConfig($config : IProject) : boolean {
        return Loader.getInstance().getFileSystemHandler().Write(
            Loader.getInstance().getAppProperties().projectBase + "/.oidis/Project.json",
            JSON.stringify($config, null, 2));
    }
}
