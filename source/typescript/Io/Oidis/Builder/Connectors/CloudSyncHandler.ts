/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IFileTransferProtocol, IRemoteFileTransferProtocol } from "@io-oidis-gui/Io/Oidis/Gui/Interfaces/Components/IFileUpload.js";
import { FileTransferHandler } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileTransferHandler.js";
import { EventsManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Events/EventsManager.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { IFileInfo, IRemoteFileInfo } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IFileInfo.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { IAppConfiguration } from "../Interfaces/IAppConfiguration.js";
import {
    ICloudChangeArgs, ICloudChanges,
    ICloudFilter,
    ICloudFilterType,
    ICloudItemStats,
    ICloudSyncArgs,
    ICloudSyncFileMetadata,
    ICloudSyncMap
} from "../Interfaces/ICloudItem.js";
import { Loader } from "../Loader.js";
import { FileSystemHandler } from "./FileSystemHandler.js";

export class CloudSyncHandler extends FileTransferHandler {
    private readonly baseSyncPath : string;
    private fileSystem : FileSystemHandler;
    private syncStampPath : string;
    private fs : any;
    private fileUploadMetadata : ArrayList<ICloudSyncFileMetadata>;

    constructor($syncFolder? : string) {
        super();
        const appConfig : IAppConfiguration = Loader.getInstance().getAppConfiguration();
        if (ObjectValidator.IsEmptyOrNull($syncFolder)) {
            this.baseSyncPath = appConfig.cloud.syncBase + "/" + appConfig.hub.user + "/{0}/";
        } else {
            this.baseSyncPath = $syncFolder + "/";
        }
        this.fileSystem = new FileSystemHandler();
        this.fs = require("fs");
        this.syncStampPath = this.fileSystem.getTempPath() + "/sync-timer.json";
        this.fileUploadMetadata = new ArrayList<any>();
    }

    public CreateFilter($syncFolder : string, $filters : Array<ICloudFilterType | ICloudFilter>) : ICloudFilter {
        const syncPath : string = this.getSyncPath($syncFolder);
        const metadata : string[] = [
            "/.idea/",
            "/.git/",
            "/__cleanup/",
            "/log/",
            "/bin/resource/",
            "/resource/data/Com/Wui/Framework/Builder/",
            "/resource/data/Io/Oidis/Builder/",
            "/package-map.json"
        ];
        const buildData : string[] = [
            "/build/"
        ];
        const buildCacheData : string[] = [
            "/build_cache/",
            "/cmake-build/"
        ];
        const generalDependencies : string[] = [];
        this.fileSystem.Expand(syncPath + "dependencies/*").forEach(($path : string) : void => {
            let configPath : string = $path + "/package.conf.jsonp";
            if (!this.fileSystem.Exists(configPath)) {
                configPath = configPath.slice(0, -1);
            }
            if (!this.fileSystem.Exists(configPath)) {
                generalDependencies.push("/" + StringUtils.Remove($path, syncPath) + "/");
            }
        });

        const excludes : string[] = [];
        $filters.forEach(($filter : ICloudFilterType | ICloudFilter) : void => {
            if (ICloudFilterType.Contains($filter)) {
                switch ($filter) {
                case ICloudFilterType.PUSH:
                    excludes.push(...buildData);
                    excludes.push(...buildCacheData);
                    excludes.push(...metadata);
                    excludes.push(...generalDependencies);
                    break;
                case ICloudFilterType.PULL:
                    if (!Loader.getInstance().getProgramArgs().getOptions().withProductsSync) {
                        excludes.push(...buildData);
                    }
                    excludes.push(...buildCacheData);
                    excludes.push(...metadata);
                    excludes.push(...generalDependencies);
                    break;
                default:
                    excludes.push(...metadata);
                    break;
                }
            } else {
                excludes.push(...((<ICloudFilter>$filter).excludes || []));
            }
        });
        return {
            excludes
        };
    }

    public ToGlobFilter($syncPath : string, $filter : ICloudFilter) : ICloudFilter {
        const syncPath : string = $syncPath.substr(0, $syncPath.length - 1);
        const excludes : string[] = [];
        $filter.excludes.forEach(($exclude : string) : void => {
            let path : string = "";
            if (StringUtils.StartsWith($exclude, "/")) {
                path += syncPath;
            }
            path += $exclude;
            if (StringUtils.EndsWith($exclude, "/")) {
                path += "**";
            }
            excludes.push("!" + path);
        });
        return {
            excludes
        };
    }

    @Extern()
    public getSyncMap($args : ICloudSyncArgs, $callback : (($map : ICloudSyncMap) => void) | IResponse) : void {
        const syncPath : string = this.getSyncPath($args.syncFolder);
        const filter : ICloudFilter = this.CreateFilter($args.syncFolder, $args.filters);
        const globFilter : ICloudFilter = this.ToGlobFilter(syncPath, filter);
        const files : string[] = this.getFiles(syncPath, globFilter);
        const statsMap : ICloudItemStats[] = this.parseItemStatsMap($args.syncFolder, files);
        const prevSyncMap : ICloudSyncMap = this.loadSyncMap(syncPath);
        const syncTime : number = this.getSyncTime();
        const syncMap : ICloudSyncMap = {items: <any>{}};

        Object.keys(prevSyncMap.items).forEach(($path : string) : void => {
            if (this.getContainedExcludes($path, filter).length === 0 && !ObjectValidator.IsEmptyOrNull(statsMap[$path])) {
                syncMap.items[$path] = prevSyncMap.items[$path];
            }
        });

        let saveMap : boolean = false;
        const continueParse : any = ($index : number) : void => {
            if ($index % 1000 === 0) {
                EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                    parse($index);
                }, false);
            } else {
                parse($index);
            }
        };

        const parse : any = ($index : number) : void => {
            if ($index < files.length) {
                const file : string = files[$index];
                let prevItem : ICloudItemStats = prevSyncMap.items[file];
                if (ObjectValidator.IsSet(statsMap[file])) {
                    const item : ICloudItemStats = statsMap[file];
                    if (!ObjectValidator.IsSet(prevItem)) {
                        prevItem = {changedLocal: -1, size: -1, changed: -1};
                    }
                    if (ObjectValidator.IsEmptyOrNull(prevItem.hash) ||
                        item.changedLocal >= prevItem.indexed ||
                        prevItem.changedLocal !== item.changedLocal ||
                        prevItem.size !== item.size) {
                        saveMap = true;
                        this.getFileHash(syncPath + file, ($value : string) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($value)) {
                                item.hash = $value;
                            } else {
                                item.size = undefined;
                            }
                            item.indexed = syncTime;
                            item.changed = item.changedLocal;
                            syncMap.items[file] = item;
                            continueParse($index + 1);
                        });
                    } else {
                        item.hash = prevItem.hash;
                        item.changed = prevItem.changed;
                        item.indexed = syncTime;
                        syncMap.items[file] = item;
                        continueParse($index + 1);
                    }
                } else {
                    continueParse($index + 1);
                }
            } else {
                let isSuccess : boolean = true;
                if (saveMap) {
                    isSuccess = this.saveSyncMap(syncPath, syncMap);
                }
                if (isSuccess) {
                    ResponseFactory.getResponse($callback)
                        .Send(this.createTransferMap(syncMap, statsMap, $args.includeLocalMetadata));
                } else {
                    ResponseFactory.getResponse($callback).Send(false);
                }
            }
        };
        parse(0);
    }

    @Extern()
    public UploadFile($data : IFileTransferProtocol, $response : ($status : boolean) => void) : void {
        const response : IResponse = ResponseFactory.getResponse($response);
        const metadata : any = JSON.parse($data.metadata);
        if (!ObjectValidator.IsEmptyOrNull(metadata)) {
            const syncPath : string = this.getSyncPath(metadata.syncFolder);
            const data : IRemoteFileTransferProtocol = <IRemoteFileTransferProtocol>$data;
            data.cwd = syncPath;
            super.UploadFile(data, ($status : boolean) : void => {
                if ($status) {
                    if ($data.end >= $data.size) {
                        this.UpdateFileSyncMetadata(metadata.syncFolder, $data.path, metadata.isCreate, metadata.changed,
                            ($success : boolean) => {
                                response.Send($success);
                            });
                    } else {
                        response.Send(true);
                    }
                } else {
                    response.Send(false);
                }
            });
        } else {
            response.Send(false);
        }
    }

    @Extern()
    public DownloadFile($fileInfo : IFileInfo, $response : IResponse) : void {
        const metadata : any = $fileInfo.metadata;
        if (!ObjectValidator.IsEmptyOrNull(metadata)) {
            const info : IRemoteFileInfo = <IRemoteFileInfo>$fileInfo;
            info.cwd = this.getSyncPath(metadata.syncFolder);
            super.DownloadFile(info, $response);
        } else {
            ResponseFactory.getResponse($response).OnError("File descriptor is missing metadata.");
        }
    }

    @Extern()
    public AcknowledgeChunk($chunkId : string) : void {
        super.AcknowledgeChunk($chunkId);
    }

    @Extern()
    public CreateFolders($syncFolder : string, $args : ICloudChangeArgs[]) : boolean {
        const syncPath : string = this.getSyncPath($syncFolder);
        const syncMap : ICloudSyncMap = this.loadSyncMap(syncPath);

        let status : boolean = true;
        let saveMap : boolean = false;
        ArrayList.ToArrayList($args).foreach(($arg : ICloudChangeArgs) : boolean => {
            status = this.fileSystem.CreateDirectory(syncPath + $arg.path);
            if (status) {
                const syncTime : number = this.getSyncTime() + .1;
                this.updateParentSyncMetadata(syncPath, syncMap, $arg.path, $arg.changed, syncTime);
                const item : ICloudItemStats = this.parseItemStats(syncPath, $arg.path);
                item.changed = $arg.changed;
                item.indexed = syncTime;
                item.size = undefined;
                syncMap.items[$arg.path] = item;
                saveMap = true;
            }
            return status;
        });
        if (saveMap) {
            return this.saveSyncMap(syncPath, syncMap);
        }
        return status;
    }

    @Extern()
    public DeleteItems($syncFolder : string, $args : ICloudChangeArgs[]) : boolean {
        const syncPath : string = this.getSyncPath($syncFolder);
        const syncMap : ICloudSyncMap = this.loadSyncMap(syncPath);

        let status : boolean = true;
        let saveMap : boolean = false;
        ArrayList.ToArrayList($args).foreach(($arg : ICloudChangeArgs) : boolean => {
            const itemPath : string = syncPath + $arg.path;
            status = this.fileSystem.Delete(itemPath);
            if (status) {
                const syncTime : number = this.getSyncTime() + .1;
                this.updateParentSyncMetadata(syncPath, syncMap, $arg.path, $arg.changed, syncTime);
                delete syncMap.items[$arg.path]; // eslint-disable-line @typescript-eslint/no-array-delete
                saveMap = true;
            }
            return status;
        });
        if (saveMap) {
            return this.saveSyncMap(syncPath, syncMap);
        }
        return status;
    }

    @Extern()
    public getSyncPath($syncFolder : string) : string {
        return StringUtils.Format(this.baseSyncPath, $syncFolder);
    }

    public UpdateFileSyncMetadata($syncFolder : string, $path : string, $isCreate : boolean, $changed : number,
                                  $callback : ($success : boolean) => void) : void {
        const syncPath : string = this.getSyncPath($syncFolder);
        this.getFileHash(syncPath + $path, ($hash : string) : void => {
            const syncMap : ICloudSyncMap = this.loadSyncMap(syncPath);
            const item : ICloudItemStats = this.parseItemStats(syncPath, $path);
            const syncTime : number = this.getSyncTime() + .1;
            if ($isCreate) {
                this.updateParentSyncMetadata(syncPath, syncMap, $path, $changed, syncTime);
            }
            item.indexed = syncTime;
            item.changed = $changed;
            item.hash = $hash;
            syncMap.items[$path] = item;
            $callback(this.saveSyncMap(syncPath, syncMap));
        });
    }

    public getChanges($from : ICloudSyncMap, $to : ICloudSyncMap) : ICloudChanges {
        const putArr : string[] = [];
        const removeMap : ICloudSyncMap = JsonUtils.Clone($to);
        Object.keys($from.items).forEach(($key : string) : void => {
            const fromItem : ICloudItemStats = $from.items[$key];
            const toItem : ICloudItemStats = removeMap.items[$key];
            if (!ObjectValidator.IsSet(toItem) || toItem.hash !== fromItem.hash || toItem.size !== fromItem.size) {
                putArr.push($key);
            }
            delete removeMap.items[$key]; // eslint-disable-line @typescript-eslint/no-array-delete
        });
        let index : number = 0;
        const size : number = putArr.length;
        const pushFiles : string[] = [];
        const pushFolders : string[] = [];
        for (index; index < size; index++) {
            const curItem : string = putArr[index];
            const item : ICloudItemStats = $from.items[curItem];
            const isFolder : boolean = !(ObjectValidator.IsSet(item.hash) || ObjectValidator.IsSet(item.size));
            if (isFolder) {
                pushFolders.push(curItem);
            } else {
                pushFiles.push(curItem);
            }
        }
        return {pushFiles, pushFolders, deleteItems: Object.keys(removeMap.items)};
    }

    public getGlobalChangeTimeMap($syncMap : ICloudSyncMap, $paths : string[]) : number[] {
        const changeTimeMap : number[] = <any>{};
        const mapPaths : string[] = Object.keys($syncMap.items);
        $paths.forEach(($path : string) : void => {
            let targetPath : string = $path;
            const sourceItem : ICloudItemStats = $syncMap.items[$path];
            let item : ICloudItemStats = $syncMap.items[$path];
            let changeTime : number = (item || {})?.changed || -1;

            if (ObjectValidator.IsEmptyOrNull(sourceItem)) {
                while (!ObjectValidator.IsSet(item) && !ObjectValidator.IsEmptyOrNull(targetPath)) {
                    targetPath = this.getParentPath(targetPath);
                    item = $syncMap.items[targetPath];
                }
                changeTime = (item || {})?.changed || -1;
            } else {
                let index : number = mapPaths.indexOf($path) + 1;
                for (index; index < mapPaths.length; index++) {
                    if (StringUtils.Contains(mapPaths[index], $path)) {
                        const targetItem : ICloudItemStats = $syncMap.items[mapPaths[index]];
                        if (targetItem.changed > changeTime) {
                            changeTime = targetItem.changed;
                        }
                    } else {
                        break;
                    }
                }
            }
            changeTimeMap[$path] = changeTime;
        });
        return changeTimeMap;
    }

    public ReduceChanges($changes : ICloudChanges) : ICloudChanges {
        const pushFoldersMap : any[] = <any>{};
        const pushFilesMap : any[] = <any>{};
        $changes.pushFolders.forEach(($path : string) : void => {
            pushFoldersMap[$path] = null;
        });
        $changes.pushFiles.forEach(($path : string) : void => {
            pushFilesMap[$path] = null;
        });

        $changes.pushFiles.forEach(($path : string) : void => {
            let path : string = this.getParentPath($path);
            while (!ObjectValidator.IsEmptyOrNull(path)) {
                delete pushFoldersMap[path]; // eslint-disable-line @typescript-eslint/no-array-delete
                path = this.getParentPath(path);
            }
            delete pushFoldersMap[path]; // eslint-disable-line @typescript-eslint/no-array-delete
        });
        $changes.pushFolders.forEach(($path : string) : void => {
            let path : string = this.getParentPath($path);
            while (!ObjectValidator.IsEmptyOrNull(path)) {
                delete pushFoldersMap[path]; // eslint-disable-line @typescript-eslint/no-array-delete
                path = this.getParentPath(path);
            }
            delete pushFoldersMap[path]; // eslint-disable-line @typescript-eslint/no-array-delete
        });

        const deleteItems : string[] = [];
        const deleteList : ArrayList<string> = new ArrayList<string>();
        $changes.deleteItems.forEach(($item : string) : void => {
            deleteList.Add($item, $item);
        });
        deleteList.SortByKeyUp();
        const deleteArr : Array<string | number> = deleteList.getKeys();
        let curFolder : string = null;
        deleteArr.forEach(($value : string, $index : number) : void => {
            const curItem : string = $value;
            const prevItem : string | number = deleteArr[$index - 1];
            if (ObjectValidator.IsEmptyOrNull(prevItem) || !StringUtils.Contains(curItem, curFolder)) {
                deleteItems.push(curItem);
                curFolder = curItem;
            }
        });
        return {pushFiles: Object.keys(pushFilesMap), pushFolders: Object.keys(pushFoldersMap), deleteItems};
    }

    public getChangeArgs($targetItems : string[], $globalChangeTimeMap : number[]) : ICloudChangeArgs[] {
        const args : ICloudChangeArgs[] = [];
        $targetItems.forEach(($item : string) : void => {
            args.push({
                changed: $globalChangeTimeMap[$item],
                path   : $item
            });
        });
        return args;
    }

    public parseItemStatsMap($syncFolder : string, $items : string[]) : ICloudItemStats[] {
        const syncPath : string = this.getSyncPath($syncFolder);
        const statsMap : ICloudItemStats[] = <any>{};
        $items.forEach(($file : string) : void => {
            statsMap[$file] = this.parseItemStats(syncPath, $file);
        });
        return statsMap;
    }

    public parseItemStats($syncPath : string, $file : string) : ICloudItemStats {
        const path : string = $syncPath + $file;
        if (this.fs.existsSync(path)) {
            const stats : any = this.fs.lstatSync(path);
            return {
                changedLocal: Math.max(stats.mtimeMs, stats.ctimeMs),
                size        : stats.size
            };
        }
        return {};
    }

    private updateParentSyncMetadata($syncPath : string, $syncMap : ICloudSyncMap, $path : string, $changedGlobal : number,
                                     $syncTime : number) : void {
        const parentPath : string = this.getParentPath($path);
        const prevParent : ICloudItemStats = $syncMap.items[parentPath];
        const parentItem : ICloudItemStats = this.parseItemStats($syncPath, parentPath);
        if (!ObjectValidator.IsEmptyOrNull(prevParent) && prevParent.changed > $changedGlobal) {
            parentItem.changed = prevParent.changed;
        } else {
            parentItem.changed = $changedGlobal;
        }
        parentItem.indexed = $syncTime;
        parentItem.size = undefined;
        $syncMap.items[parentPath] = parentItem;
    }

    private getSyncTime() : number {
        if (this.fileSystem.Write(this.syncStampPath, "")) {
            return this.fs.lstatSync(this.syncStampPath).mtimeMs;
        }
        return -1;
    }

    private getFiles($syncPath : string, $filter : ICloudFilter) : string[] {
        const fileMap : string[] = [];
        [""].concat(this.fileSystem.Expand([$syncPath + "**/*"].concat($filter.excludes)))
            .forEach(($file : string, $index : number) : void => {
                fileMap[$index] = $file.replace($syncPath, "");
            });
        return fileMap;
    }

    private createTransferMap($itemMap : ICloudSyncMap, $fileMap : any[], $includeLocalMetadata : boolean = false) : ICloudSyncMap {
        const filtered : ICloudSyncMap = {items: <any>{}};
        const createTransferItem : any = ($from : ICloudItemStats) : ICloudItemStats => {
            const item : ICloudItemStats = {};
            if (ObjectValidator.IsSet($from.changed)) {
                item.changed = $from.changed;
            }
            if (ObjectValidator.IsSet($from.hash)) {
                item.hash = $from.hash;
            }
            if (ObjectValidator.IsSet($from.size)) {
                item.size = $from.size;
            }
            if ($includeLocalMetadata && ObjectValidator.IsSet($from.changedLocal)) {
                item.changedLocal = $from.changedLocal;
            }
            if ($includeLocalMetadata && ObjectValidator.IsSet($from.indexed)) {
                item.indexed = $from.indexed;
            }
            return item;
        };
        Object.keys($itemMap.items).forEach(($file : string) : void => {
            if (ObjectValidator.IsSet($fileMap[$file])) {
                filtered.items[$file] = createTransferItem($itemMap.items[$file]);
            }
        });
        return filtered;
    }

    private getFileHash($path : string, $callback : ($value : string) => void) : void {
        if (Loader.getInstance().getFileSystemHandler().IsFile($path)) {
            const crypto : any = require("crypto");

            const getfileSha1 : any = ($path : string) : Promise<string> => {
                return new Promise(($resolve, $reject) : void => {
                    const hash : any = crypto.createHash("sha1");
                    const stream : any = this.fs.createReadStream($path);
                    stream.on("error", $reject);
                    stream.on("data", ($chunk : string) : void => {
                        hash.update($chunk);
                    });
                    stream.on("end", () : void => {
                        $resolve(hash.digest("hex"));
                    });
                });
            };
            getfileSha1($path).then(($hash : string) : void => {
                $callback($hash);
            }).catch(($ex : Error) : void => {
                LogIt.Error("Unable to calculate hash for: " + $path, $ex);
                $callback(null);
            });
        } else {
            $callback(null);
        }
    }

    private loadSyncMap($syncPath : string) : ICloudSyncMap {
        const syncMapPath : string = $syncPath + "package-map.json";
        let map : ICloudSyncMap = {items: <any>{}};
        if (this.fileSystem.Exists(syncMapPath)) {
            try {
                const loadedMap : ICloudSyncMap = JSON.parse(this.fileSystem.Read(syncMapPath).toString());
                if (ObjectValidator.IsSet(loadedMap.items)) {
                    map = loadedMap;
                }
            } catch ($error) {
                // empty catch
            }
        }
        return map;
    }

    private saveSyncMap($syncPath : string, $syncMap : ICloudSyncMap) : boolean {
        const syncMapPath : string = $syncPath + "package-map.json";
        return this.fileSystem.Write(syncMapPath, JSON.stringify($syncMap,
            ($key : string, $value : any) : void => {
                if ($value !== null) {
                    return $value;
                }
            }));
    }

    private getParentPath($file : string) : string {
        const split : string[] = $file.split("/");
        split.pop();
        return split.join("/");
    }

    private getContainedExcludes($path : string, $filter : ICloudFilter) : string[] {
        const containedFilters : string[] = [];
        const path : string = "/" + $path;
        $filter.excludes.forEach(($pattern : string) : void => {
            const isRootRelative : boolean = StringUtils.StartsWith($pattern, "/");
            if (isRootRelative ? StringUtils.StartsWith(path, $pattern) : StringUtils.Contains(path, $pattern)) {
                containedFilters.push($pattern);
            }
        });
        return containedFilters;
    }
}
