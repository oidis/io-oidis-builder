/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { AgentsRegisterConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/AgentsRegisterConnector.js";
import { IAcknowledgePromise } from "@io-oidis-services/Io/Oidis/Services/Connectors/ReportServiceConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { IWebServiceProtocol } from "@io-oidis-services/Io/Oidis/Services/Interfaces/IWebServiceProtocol.js";
import { IConnectorOptions } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";
import { Loader } from "../Loader.js";

export class BuilderHubConnector extends AgentsRegisterConnector {

    public RegisterAgent($capabilities : IBuilderAgent) : IBuilderHubRegisterPromise {
        const callbacks : any = {
            onComplete($success : boolean) : any {
                // declare default callback
            },
            onError: null,
            onInvokeMethod($data : IWebServiceProtocol, $taskId? : string) : any {
                // declare default callback
            },
            onMessage($data : any, $taskId? : string) : any {
                // declare default callback
            },
            onTaskRequest($args : string[], $taskId? : string) : any {
                // declare default callback
            }
        };
        LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(),
            "RegisterAgent", $capabilities)
            .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                this.errorHandler(callbacks.onError, $error, $args);
            })
            .Then(($result : any) : void => {
                if (ObjectValidator.IsSet($result.type)) {
                    if ($result.type === EventType.ON_CHANGE) {
                        if ($result.data.type === "taskRequest") {
                            callbacks.onTaskRequest($result.data.data, $result.data.taskId);
                        } else if (StringUtils.Contains($result.data.protocol.type, "LiveContentWrapper")) {
                            callbacks.onInvokeMethod($result.data.protocol, $result.data.taskId);
                        } else {
                            callbacks.onMessage($result.data.protocol.data, $result.data.taskId);
                        }
                    }
                } else if (ObjectValidator.IsBoolean($result)) {
                    callbacks.onComplete($result);
                }
            });
        const promise : IBuilderHubRegisterPromise = {
            OnError       : ($callback : ($error : ErrorEventArgs) => void) : IAcknowledgePromise => {
                callbacks.onError = $callback;
                return promise;
            },
            OnInvokeMethod: ($callback : ($data : IWebServiceProtocol, $taskId? : string) => void) : IBuilderHubRegisterPromise => {
                callbacks.onInvokeMethod = $callback;
                return promise;
            },
            OnMessage     : ($callback : ($data : any, $taskId? : string) => void) : IBuilderHubRegisterPromise => {
                callbacks.onMessage = $callback;
                return promise;
            },
            OnTaskRequest : ($callback : ($args : string[], $taskId? : string) => void) : IBuilderHubRegisterPromise => {
                callbacks.onTaskRequest = $callback;
                return promise;
            },
            Then          : ($callback : ($success : boolean) => void) : void => {
                callbacks.onComplete = $callback;
            }
        };
        return promise;
    }

    public RunTask($task : IBuilderTask) : IBuilderHubTaskPromise {
        const callbacks : any = {
            onError($message : string) : any {
                // declare default callback
            },
            onComplete($success : boolean, $builderId? : string, $taskId? : string) : any {
                // declare default callback
            },
            onMessage($data : any, $taskId? : string) : any {
                // declare default callback
            }
        };
        LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(),
            "RunTask", $task)
            .Then(($result : any) : void => {
                if (ObjectValidator.IsSet($result.type)) {
                    if ($result.type === EventType.ON_CHANGE) {
                        callbacks.onMessage($result.data, $result.data.taskId);
                    } else if ($result.type === EventType.ON_COMPLETE && !$result.data.status) {
                        callbacks.onError($result.data.message);
                    }
                } else if (ObjectValidator.IsObject($result) && ObjectValidator.IsSet($result.builderId)) {
                    callbacks.onComplete(true, $result.builderId, $result.taskId);
                }
            });
        return {
            OnError: ($callback : ($message : string) => void) : IBuilderHubTaskBasePromise => {
                callbacks.onError = $callback;
                const subPromise : IBuilderHubTaskBasePromise = {
                    OnMessage: ($callback : ($data : any, $taskId? : string) => void) : IBuilderHubTaskBasePromise => {
                        callbacks.onMessage = $callback;
                        return subPromise;
                    },
                    Then     : ($callback : ($success : boolean, $builderId? : string, $taskId? : string) => void) : void => {
                        callbacks.onComplete = $callback;
                    }
                };
                return subPromise;
            }
        };
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.HUB_CONNECTOR] = "Io.Oidis.Hub.Utils.BuilderHub";
        return namespaces;
    }

    protected resolveDefaultOptions($options : IConnectorOptions) : IConnectorOptions {
        if (ObjectValidator.IsEmptyOrNull($options.serverConfigurationSource)) {
            $options.serverConfigurationSource = Loader.getInstance().getAppConfiguration().hub.url + "/connector.config.jsonp";
        }
        return super.resolveDefaultOptions($options);
    }
}

export interface IBuilderAgent {
    name : string;
    platform : string;
    domain : string;
    version : string;
}

export interface IBuilderTask {
    args : string[];
    platforms : string[];
    agent : IBuilderAgent;
}

export interface IBuilderHubRegisterPromise extends IAcknowledgePromise {
    OnTaskRequest($callback : ($args : string[], $taskId? : string) => void) : IBuilderHubRegisterPromise;

    OnMessage($callback : ($data : any, $taskId? : string) => void) : IBuilderHubRegisterPromise;

    OnInvokeMethod($callback : ($data : IWebServiceProtocol, $taskId? : string) => void) : IBuilderHubRegisterPromise;
}

export interface IBuilderHubTaskBasePromise {
    OnMessage($callback : ($data : any, $taskId? : string) => void) : IBuilderHubTaskBasePromise;

    Then($callback : ($success : boolean, $builderId? : string, $taskId? : string) => void) : void;
}

export interface IBuilderHubTaskPromise {
    OnError($callback : ($message : string) => void) : IBuilderHubTaskBasePromise;
}

// generated-code-start
/* eslint-disable */
export const IBuilderAgent = globalThis.RegisterInterface(["name", "platform", "domain", "version"]);
export const IBuilderTask = globalThis.RegisterInterface(["args", "platforms", "agent"]);
export const IBuilderHubRegisterPromise = globalThis.RegisterInterface(["OnTaskRequest", "OnMessage", "OnInvokeMethod"], <any>IAcknowledgePromise);
export const IBuilderHubTaskBasePromise = globalThis.RegisterInterface(["OnMessage", "Then"]);
export const IBuilderHubTaskPromise = globalThis.RegisterInterface(["OnError"]);
/* eslint-enable */
// generated-code-end
