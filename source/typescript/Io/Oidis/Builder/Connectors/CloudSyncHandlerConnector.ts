/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { FileTransferConnector } from "@io-oidis-connector/Io/Oidis/Connector/Connectors/FileTransferConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { ICloudChangeArgs, ICloudSyncArgs, ICloudSyncMap } from "../Interfaces/ICloudItem.js";

export class CloudSyncHandlerConnector extends FileTransferConnector {

    public getSyncMap($args : ICloudSyncArgs) : ICloudResponsePromise {
        return this.invoke("getSyncMap", $args);
    }

    public CreateFolders($syncFolder : string, $args : ICloudChangeArgs[]) : ICloudResponseStatusPromise {
        return this.invoke("CreateFolders", $syncFolder, $args);
    }

    public DeleteItems($syncFolder : string, $args : ICloudChangeArgs[]) : ICloudResponseStatusPromise {
        return this.invoke("DeleteItems", $syncFolder, $args);
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.BUILDER_CONNECTOR] = "Io.Oidis.Builder.Connectors.CloudSyncHandler";
        namespaces[WebServiceClientType.DESKTOP_CONNECTOR] = "Io.Oidis.Builder.Connectors.CloudSyncHandler";
        namespaces[WebServiceClientType.FORWARD_CLIENT] = "Io.Oidis.Builder.Connectors.CloudSyncHandler";
        return namespaces;
    }
}

export interface ICloudResponsePromise {
    Then($callback : ($data : ICloudSyncMap) => void) : void;
}

export interface ICloudResponseStatusPromise {
    Then($callback : ($status : boolean) => void) : void;
}

// generated-code-start
export const ICloudResponsePromise = globalThis.RegisterInterface(["Then"]);
export const ICloudResponseStatusPromise = globalThis.RegisterInterface(["Then"]);
// generated-code-end
