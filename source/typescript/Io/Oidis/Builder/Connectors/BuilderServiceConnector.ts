/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { WebServiceClientType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/WebServiceClientType.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { WebServiceConfiguration } from "@io-oidis-commons/Io/Oidis/Commons/WebServiceApi/WebServiceConfiguration.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { IServiceConfig } from "../Interfaces/IAppConfiguration.js";
import { Loader } from "../Loader.js";
import { BuilderServer } from "../Tasks/Servers/BuilderServer.js";

export class BuilderServiceConnector extends BaseConnector {

    constructor($port? : number, $address? : string) {
        super({
            reconnect   : true,
            suppressInit: true
        });
        let config : IServiceConfig = Loader.getInstance().getAppConfiguration().service;
        if (ObjectValidator.IsEmptyOrNull(config)) {
            config = <any>{};
        }
        if (!ObjectValidator.IsEmptyOrNull($port) && $port > 0) {
            config.port = $port;
        }
        if (!ObjectValidator.IsEmptyOrNull($address) && $address !== "host" && $address !== "service") {
            config.address = $address;
        }
        if (ObjectValidator.IsEmptyOrNull(config.port)) {
            config.port = 3666;
        }
        if (ObjectValidator.IsEmptyOrNull(config.address)) {
            config.address = Loader.getInstance().getHttpManager().getRequest().getServerIP();
        }
        const serverConfigurationSource : WebServiceConfiguration = new WebServiceConfiguration();
        serverConfigurationSource.ServerAddress(config.address);
        serverConfigurationSource.ServerPort(config.port);
        serverConfigurationSource.ServerBase("/wuihost");
        this.resolveDefaultOptions({
            clientType: WebServiceClientType.WEB_SOCKETS,
            serverConfigurationSource
        });
        this.init();
    }

    public RunTask($args : string[]) : IBuilderServerPromise {
        return this.invoke.apply(this, ["TaskProxy"].concat($args)); // eslint-disable-line prefer-spread
    }

    protected getClientType() : WebServiceClientType {
        return WebServiceClientType.WEB_SOCKETS;
    }

    protected getServerNamespaces() : any {
        const namespaces : any = {};
        namespaces[WebServiceClientType.WEB_SOCKETS] = BuilderServer.ClassName();
        return namespaces;
    }
}

export interface IBuilderServerPromise {
    OnMessage($callback : ($data : any, $taskId? : string) => void) : IBuilderServerPromise;

    Then($callback : ($success : boolean, $builderId? : string, $taskId? : string) => void) : void;
}

// generated-code-start
export const IBuilderServerPromise = globalThis.RegisterInterface(["OnMessage", "Then"]);
// generated-code-end
