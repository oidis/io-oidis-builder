/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IVBoxSession } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { VirtualBoxManager, VirtualBoxSessionHandler } from "../Utils/VirtualBoxManager.js";

export class VirtualBoxHandler extends BaseObject {
    private static sessions : ArrayList<IVBoxSession>;

    public static Start($machine : string, $user : string, $pass : string,
                        $callback : (($status : boolean, $sessionId : string) => void) | IResponse) : void {
        if (!ObjectValidator.IsSet(VirtualBoxHandler.sessions)) {
            VirtualBoxHandler.sessions = new ArrayList<IVBoxSession>();
        }
        VirtualBoxManager.getInstanceSingleton()
            .Start($machine, $user, $pass)
            .Then(($status : boolean, $session : VirtualBoxSessionHandler) : void => {
                const sessionId : string = $session.getId();
                VirtualBoxHandler.sessions.Add($session, sessionId);
                ResponseFactory.getResponse($callback).Send($status, sessionId);
            });
    }

    public static Copy($sessionId : string, $sourcePath : string, $destinationPath : string,
                       $callback : (($status : boolean) => void) | IResponse) : void {
        VirtualBoxHandler.getSession($sessionId, ($session : IVBoxSession) : void => {
            $session.Copy($sourcePath, $destinationPath).Then(($status : boolean) : void => {
                ResponseFactory.getResponse($callback).Send($status);
            });
        }, $callback);
    }

    public static Execute($sessionId : string, $filePath : string, $callback : (($status : boolean) => void) | IResponse) : void {
        VirtualBoxHandler.getSession($sessionId, ($session : IVBoxSession) : void => {
            $session.Execute($filePath).Then(($status : boolean) : void => {
                ResponseFactory.getResponse($callback).Send($status);
            });
        }, $callback);
    }

    private static getSession($sessionId : string, $onFound : ($session : IVBoxSession) => void, $errorResponse : any) : void {
        if (VirtualBoxHandler.sessions.KeyExists($sessionId)) {
            $onFound(VirtualBoxHandler.sessions.getItem($sessionId));
        } else {
            ResponseFactory.getResponse($errorResponse).Send(false);
        }
    }
}
