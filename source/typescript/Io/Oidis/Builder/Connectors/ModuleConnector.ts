/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ResponseFactory } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/ResponseFactory.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { ModulesResolver } from "../ModulesResolver.js";

export class ModuleConnector extends BaseConnector {

    @Extern()
    public static Execute($name : string, $args : any, $callback : (($returnValue : any) => void) | IResponse) : void {
        const response : IResponse = ResponseFactory.getResponse($callback);
        const resolver : ModulesResolver = new ModulesResolver();
        resolver.Resolve($name, $args, ($status : boolean, $returnValue? : any) : void => {
            if ($status) {
                response.Send($returnValue);
            } else {
                response.OnError($returnValue);
            }
        });
    }
}
