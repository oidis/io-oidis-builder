/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { CredentialType } from "../Enums/CredentialType.js";

export class CredentialsManager extends BaseObject {
    private static serviceName : string = "OidisBuilder";

    public static async setLinuxRoot($password : string) : Promise<boolean> {
        return CredentialsManager.setPassword(CredentialType.LINUX, $password);
    }

    public static async getLinuxRoot() : Promise<string> {
        return CredentialsManager.getPassword(CredentialType.LINUX);
    }

    public static async setPhonegapToken($password : string) : Promise<boolean> {
        return CredentialsManager.setPassword(CredentialType.PHONEGAP, $password);
    }

    public static async getPhonegapToken() : Promise<string> {
        return CredentialsManager.getPassword(CredentialType.PHONEGAP);
    }

    public static async setJIRAPass($userName : string, $password : string) : Promise<boolean> {
        return CredentialsManager.setPassword($userName, $password);
    }

    public static async getJIRAPass($userName : string) : Promise<string> {
        return CredentialsManager.getPassword($userName);
    }

    private static async setPassword($type : CredentialType, $value : string) : Promise<boolean> {
        const keytar : any = require("keytar");
        if (ObjectValidator.IsString($value)) {
            try {
                await keytar.setPassword(CredentialsManager.serviceName, $type, $value);
                return true;
            } catch (ex) {
                LogIt.Warning(ex.stack);
            }
        }
        return false;
    }

    private static async getPassword($type : CredentialType) : Promise<string> {
        const keytar : any = require("keytar");
        try {
            let value : string = await keytar.getPassword(CredentialsManager.serviceName, $type);
            if (value === null) {
                value = "";
            }
            return value;
        } catch (ex) {
            LogIt.Warning(ex.stack);
        }
        return "";
    }
}
