/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IFileSystemItemProtocol } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IFileSystemItemProtocol.js";
import { FileSystemHandler as Parent } from "@io-oidis-connector/Io/Oidis/Connector/Connectors/FileSystemHandler.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import {
    IArchiveOptions,
    IFileSystemDownloadOptions,
    IShortcutOptions
} from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";

export class FileSystemHandler extends Parent {

    @Extern()
    public Exists($path : string) : boolean {
        return super.Exists($path);
    }

    @Extern()
    public IsEmpty($path : string) : boolean {
        return super.IsEmpty($path);
    }

    @Extern()
    public IsFile($path : string) : boolean {
        return super.IsFile($path);
    }

    @Extern()
    public IsDirectory($path : string) : boolean {
        return super.IsDirectory($path);
    }

    @Extern()
    public IsSymbolicLink($path : string) : boolean {
        return super.IsSymbolicLink($path);
    }

    @Extern()
    public getTempPath() : string {
        return super.getTempPath();
    }

    @Extern()
    public Expand($pattern : string | string[]) : string[] {
        return super.Expand($pattern).map(($path : string) : string => {
            return this.NormalizePath($path);
        });
    }

    @Extern()
    public CreateDirectory($path : string) : boolean {
        return super.CreateDirectory($path);
    }

    @Extern()
    public Rename($oldPath : string, $newPath : string) : boolean {
        return super.Rename($oldPath, $newPath);
    }

    @Extern()
    public Read($path : string) : string | Buffer {
        return super.Read($path);
    }

    @Extern()
    public Write($path : string, $data : any, $append : boolean = false) : boolean {
        return super.Write($path, $data, $append);
    }

    @Extern()
    public Copy($sourcePath : string, $destinationPath : string, $callback? : (($status : boolean) => void) | IResponse) : void {
        super.Copy($sourcePath, $destinationPath, $callback);
    }

    @Extern()
    public Download($urlOrOptions : string | IFileSystemDownloadOptions,
                    $callback? : (($headers : string, $bodyOrPath : string) => void) | IResponse) : void {
        super.Download($urlOrOptions, $callback);
    }

    @Extern()
    public AbortDownload($id : number) : boolean {
        return super.AbortDownload($id);
    }

    @Extern()
    public Delete($path : string,
                  $callback? : (($status : boolean) => void) | IResponse) : boolean {
        return super.Delete($path, $callback);
    }

    @Extern()
    public Pack($path : string | string[], $options? : IArchiveOptions,
                $callback? : (($tmpPath : string) => void) | IResponse) : void {
        super.Pack($path, $options, $callback);
    }

    @Extern()
    public Unpack($path : string, $options? : IArchiveOptions,
                  $callback? : (($tmpPath : string) => void) | IResponse) : void {
        super.Unpack($path, $options, $callback);
    }

    @Extern()
    public NormalizePath($source : string, $osSeparator : boolean = false) : string {
        return super.NormalizePath($source, $osSeparator);
    }

    @Extern()
    public getPathMap($path : string,
                      $callback : (($map : IFileSystemItemProtocol[]) => void) | IResponse) : void {
        super.getPathMap($path, $callback);
    }

    @Extern()
    public getDirectoryContent($path : string) : IFileSystemItemProtocol[] {
        return super.getDirectoryContent($path);
    }

    @Extern()
    public CreateShortcut($source : string, $destination : string, $options : IShortcutOptions,
                          $callback : (($status : boolean) => void) | IResponse) : void {
        super.CreateShortcut($source, $destination, $options, $callback);
    }

    @Extern()
    public ReadStream($path : string) : string {
        return super.ReadStream($path);
    }

    @Extern()
    public WriteStream($path : string, $data : string, $append : boolean = false) : boolean {
        return super.WriteStream($path, $data, $append);
    }
}
