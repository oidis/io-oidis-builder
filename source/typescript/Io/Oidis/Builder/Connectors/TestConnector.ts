/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseConnector } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/BaseConnector.js";
import { Extern } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";

export class TestConnector extends BaseConnector {

    @Extern()
    public static TestArgs($part1 : string, $part2 : string, $callback : IResponse) : void {
        $callback.Send($part1 + $part2);
        setTimeout(() : void => {
            $callback.OnChange("TESTING CHANGE");
        }, 5000);
    }

    @Extern()
    public static TestCallbacks($callback : IResponse) : void {
        $callback.Send("TESTING VALUE");
        setTimeout(() : void => {
            $callback.OnStart("TESTING START");
            setTimeout(() : void => {
                $callback.OnChange("TESTING CHANGE");
                setTimeout(() : void => {
                    $callback.OnError("TESTING ERROR");
                }, 1000);
            }, 1000);
        }, 1000);
    }
}
