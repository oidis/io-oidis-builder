/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { FileTransferConnector, IFileTransferPromise } from "@io-oidis-connector/Io/Oidis/Connector/Connectors/FileTransferConnector.js";
import { IFileInfo } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IFileInfo.js";
import { IAcknowledgePromise } from "@io-oidis-services/Io/Oidis/Services/Connectors/ReportServiceConnector.js";
import { UpdatesManagerConnector as Parent } from "@io-oidis-services/Io/Oidis/Services/Connectors/UpdatesManagerConnector.js";
import { LiveContentWrapper } from "@io-oidis-services/Io/Oidis/Services/WebServiceApi/LiveContentWrapper.js";

export class UpdatesManagerConnector extends Parent {

    public async UploadPackage($fileInfo : IFileInfo, $sourcePath : string, $onMessage? : ($data : string) => void) : Promise<void> {
        return new Promise<void>(($resolve, $reject) : void => {
            const response : IFileTransferPromise = FileTransferConnector.getUploadProtocol(this, "Upload", $fileInfo, $sourcePath);
            response.OnMessage($onMessage);
            response.OnError(($data : string) : void => {
                $reject(new Error($data));
            });
            response.Then($resolve);
        });
    }

    public async UploadConfiguration($appName : string, $configName : string, $version : string, $data : string) : Promise<boolean> {
        return new Promise<boolean>(($resolve, $reject) : void => {
            const response : IAcknowledgePromise = LiveContentWrapper.InvokeMethod(this.getClient(),
                "Io.Oidis.Hub.Utils.ConfigurationsManager", "Upload", $appName, $configName, $version, $data);
            response.OnError(($error : ErrorEventArgs) : void => {
                const error : Error = new Error($error.Message());
                error.stack = $error.Exception().Stack();
                $reject(error);
            });
            response.Then($resolve);
        });
    }

    public async RegisterPackage($appName : string, $releaseName : string, $platform : string, $version : string, $buildTime : string,
                                 $fileName : string, $type? : string) : Promise<boolean> {
        return this.asyncInvoke("Register", $appName, $releaseName, $platform, $version, $buildTime, $fileName, $type);
    }

    public async FindPackages($options? : IPackageFindOptions) : Promise<any> {
        return this.asyncInvoke("FindPackages", $options);
    }

    public async getDownloadLink($options? : IPackageFindOptions, $relative? : boolean) : Promise<string> {
        return this.asyncInvoke("getDownloadLink", $options, $relative);
    }
}

export interface IPackage {
    Name : string;
    Release : string;
    Platform : string;
    Version : string;
    BuildTime : number;
    FileName : string;
    Type : string;
}

export interface IPackageFindOptions {
    id? : string;
    name? : string;
    release? : string;
    platform? : string;
    version? : string;
    buildTime? : Date;
}

// generated-code-start
export const IPackage = globalThis.RegisterInterface(["Name", "Release", "Platform", "Version", "BuildTime", "FileName", "Type"]);
export const IPackageFindOptions = globalThis.RegisterInterface(["id", "name", "release", "platform", "version", "buildTime"]);
// generated-code-end
