/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { HubConnector, IConnectorPipeProtocol } from "@io-oidis-connector/Io/Oidis/Connector/Connectors/HubConnector.js";
import { EventsManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Events/EventsManager.js";
import { BaseResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/BaseResponse.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { Network } from "../../Connectors/Network.js";
import { IRunConfig, IRunEnvironment } from "../../Interfaces/IRunEnvironment.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { IResolveConfigResult, IResolveEnvResult, RunEnvironmentResolver } from "../../Utils/RunEnvironmentResolver.js";
import { RunManager } from "../../Utils/RunManager.js";

export class RunRemote extends BaseTask {

    protected getName() : string {
        return "run-remote";
    }

    protected process($done : any, $option : string) : void {
        const runType : string = !ObjectValidator.IsEmptyOrNull($option) ? $option : this.programArgs.RunConfig();
        let initializedPort : number;
        let debugSessionId : string;
        let clientId : string;
        const configResult : IResolveConfigResult = RunEnvironmentResolver.ResolveConfig(this.project, runType);
        if (configResult.status) {
            const conf : IRunConfig = configResult.value;
            if (!ObjectValidator.IsEmptyOrNull(configResult.message)) {
                LogIt.Debug(configResult.message);
            }
            const envResult : IResolveEnvResult = <any>conf.platform !== "tunnel" ? RunEnvironmentResolver
                .ResolveEnvironment(configResult.value.platform, configResult.value.release) : <any>{
                status: true
            };
            if (envResult.status) {
                const env : IRunEnvironment = envResult.value;
                const pipeId : string = Loader.getInstance().getProgramArgs().ChainId() + ":debug";
                const network : Network = new Network();
                const connector : HubConnector = new HubConnector(true, this.builderConfig.hub.url + "/connector.config.jsonp");
                const queuedMessages : string[] = [];
                if (!ObjectValidator.IsEmptyOrNull(envResult.message)) {
                    LogIt.Debug(envResult.message);
                }

                const response : IResponse = new BaseResponse();
                response.OnComplete = () : any => {
                    $done();
                };
                response.OnError = ($data : string) : void => {
                    LogIt.Error($data);
                };
                if (!ObjectValidator.IsEmptyOrNull(conf.port)) {
                    connector.getEvents().OnStart(() : void => {
                        connector
                            .InitPipe(pipeId)
                            .OnConnect(($clientId : string) : void => {
                                clientId = $clientId;
                                LogIt.Debug("Connection successful.");
                                if (ObjectValidator.IsString(conf.port)) {
                                    const localPort : string = StringUtils.Split(<any>conf.port, ":")[1];
                                    if (localPort === "*" || localPort === "-1") {
                                        conf.port = -1;
                                    } else {
                                        conf.port = StringUtils.ToInteger(localPort);
                                    }
                                } else {
                                    conf.port = -1;
                                }
                                if (<any>conf.platform !== "tunnel") {
                                    const run : any = () : void => {
                                        RunManager.Run(env, conf, response);
                                    };
                                    if (conf.port === -1) {
                                        network.getFreePort(($port : number) : void => {
                                            conf.port = $port;
                                            run();
                                        });
                                    } else {
                                        network.IsPortFree(conf.port, ($status : boolean) : void => {
                                            if ($status) {
                                                run();
                                            } else {
                                                LogIt.Error("Required port " + conf.port + " is already in use.");
                                            }
                                        });
                                    }
                                }
                            })
                            .OnMessage(($data : IConnectorPipeProtocol) : void => {
                                if (!ObjectValidator.IsEmptyOrNull(conf.port)) {
                                    if (ObjectValidator.IsEmptyOrNull(initializedPort)) {
                                        queuedMessages.push($data.data);
                                        if (ObjectValidator.IsEmptyOrNull(debugSessionId)) {
                                            const listen : any = () : any => {
                                                const listenerResponse : IResponse = new BaseResponse();
                                                listenerResponse.OnStart = ($sessionId : string) : any => {
                                                    debugSessionId = $sessionId;
                                                    initializedPort = conf.port;
                                                    queuedMessages.forEach(($message : string) : void => {
                                                        network.WriteToPort($message, initializedPort);
                                                    });
                                                };
                                                listenerResponse.OnChange = ($data : any) : any => {
                                                    connector.WriteToPipe(clientId, pipeId, {type: "debug", data: $data});
                                                };
                                                listenerResponse.OnError = ($data : string) : void => {
                                                    LogIt.Error($data);
                                                };
                                                LogIt.Debug("Started listening on port: {0}", conf.port);
                                                network.AddPortListener(conf.port, listenerResponse);
                                            };
                                            if (<any>conf.platform !== "tunnel") {
                                                response.OnStart = listen;
                                            } else {
                                                listen();
                                            }
                                        }
                                    } else {
                                        network.WriteToPort($data.data, initializedPort);
                                    }
                                }
                            })
                            .OnDisconnect(() : void => {
                                LogIt.Debug("Pipe disconnected. Ending run.");
                                network.RemovePortListener(debugSessionId, initializedPort);
                                $done();
                            })
                            .Then(($status : boolean, $message? : string) : void => {
                                if ($status) {
                                    LogIt.Debug("Pipe initialized. Awaiting connection.");
                                } else {
                                    LogIt.Error($message);
                                }
                            });
                    });
                    connector.getEvents().OnClose(() : void => {
                        LogIt.Error("Connection has been closed.");
                    });
                    connector.getEvents().OnTimeout(() : void => {
                        LogIt.Error("Connection timeout has been reached.");
                    });
                    connector.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                        LogIt.Error($eventArgs.Exception().ToString());
                    });
                    connector.StartCommunication();
                    EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                        if (ObjectValidator.IsEmptyOrNull(clientId)) {
                            LogIt.Error("Pipe connection timed out.");
                        }
                    }, 60 * 1000);
                } else {
                    RunManager.Run(env, conf, response);
                }
            } else {
                LogIt.Error(envResult.message);
            }
        } else {
            LogIt.Error(configResult.message);
        }
    }
}
