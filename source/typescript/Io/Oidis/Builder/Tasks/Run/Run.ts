/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/ResponseApi/Handlers/BaseResponse.js";
import { IResponse } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IResponse.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { IResolveConfigResult, IResolveEnvResult, RunEnvironmentResolver } from "../../Utils/RunEnvironmentResolver.js";
import { RunManager } from "../../Utils/RunManager.js";

export class Run extends BaseTask {

    protected getName() : string {
        return "run";
    }

    protected async processAsync($option : string) : Promise<void> {
        const runType : string = !ObjectValidator.IsEmptyOrNull($option) ? $option : this.programArgs.RunConfig();
        const configResult : IResolveConfigResult = RunEnvironmentResolver.ResolveConfig(this.project, runType);
        if (!configResult.status) {
            LogIt.Error(configResult.message);
        } else if (!ObjectValidator.IsEmptyOrNull(configResult.message)) {
            LogIt.Debug(configResult.message);
        }
        if (<any>configResult.value.platform === "builder") {
            await this.runTaskAsync(configResult.value.args);
        } else if (this.programArgs.IsForwardedTask()) {
            await this.runTaskAsync("run-remote:" + runType);
        } else {
            if (this.programArgs.IsHubClient() || this.programArgs.IsServiceClient()) {
                if (!ObjectValidator.IsEmptyOrNull(configResult.value.port)) {
                    await this.runTaskAsync("cloud-sync-manager:push", "remote-attach:" + runType);
                } else {
                    await this.runTaskAsync("cloud-sync-manager:push");
                }
            } else {
                const envResult : IResolveEnvResult = RunEnvironmentResolver
                    .ResolveEnvironment(configResult.value.platform, configResult.value.release);
                if (!envResult.status) {
                    LogIt.Error(envResult.message);
                } else if (!ObjectValidator.IsEmptyOrNull(envResult.message)) {
                    LogIt.Debug(envResult.message);
                }
                const response : IResponse = new BaseResponse();
                response.OnChange(($data : string) : void => {
                    Echo.Print($data);
                });
                RunManager.Run(envResult.value, configResult.value, response);
            }
        }
    }
}
