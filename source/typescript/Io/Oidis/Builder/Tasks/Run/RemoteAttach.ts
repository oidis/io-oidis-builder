/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { HubConnector, IConnectorPipeProtocol } from "@io-oidis-connector/Io/Oidis/Connector/Connectors/HubConnector.js";
import { EventsManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Events/EventsManager.js";
import { Network } from "../../Connectors/Network.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { IRunConfig } from "../../Interfaces/IRunEnvironment.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { IResolveConfigResult, RunEnvironmentResolver } from "../../Utils/RunEnvironmentResolver.js";

export class RemoteAttach extends BaseTask {

    protected getName() : string {
        return "remote-attach";
    }

    protected process($done : any, $option : string) : void {
        const projectBase : string = Loader.getInstance().getAppProperties().projectBase;
        let projectName : string = require("path").basename(projectBase);
        if (!ObjectValidator.IsEmptyOrNull(this.programArgs.getOptions().project)) {
            projectName = this.programArgs.getOptions().project;
        }
        const runType : string = !ObjectValidator.IsEmptyOrNull($option) ? $option : this.programArgs.RunConfig();
        const confResult : IResolveConfigResult = RunEnvironmentResolver.ResolveConfig(this.project, runType);
        if (!confResult.status) {
            LogIt.Error(confResult.message);
        } else {
            const runConfig : IRunConfig = confResult.value;
            if (<any>runConfig.platform !== "tunnel" && runConfig.platform.toolchain !== ToolchainType.NODEJS) {
                LogIt.Error("Toolchain " + runConfig.platform.toolchain + " isn't supported for remote attach.");
            }
            if (!ObjectValidator.IsEmptyOrNull(confResult.message)) {
                LogIt.Debug(confResult.message);
            }
            const localNetwork : Network = new Network();
            const defaultConf : string = "default";
            let clientPort : number = this.project.runConfigs[defaultConf].port;
            if (!ObjectValidator.IsEmptyOrNull(runConfig.port)) {
                clientPort = runConfig.port;
            }
            if (ObjectValidator.IsString(clientPort)) {
                const localPort : string = StringUtils.Split(<any>clientPort, ":")[0];
                if (localPort === "*" || localPort === "-1") {
                    LogIt.Error("Client port must be specified. Values \"*\" and \"-1\" are allowed only on remote side.");
                } else {
                    clientPort = StringUtils.ToInteger(localPort);
                }
            }
            const pipeId : string = Loader.getInstance().getProgramArgs().ChainId() + ":debug";
            localNetwork.IsPortFree(clientPort, ($status : boolean) : void => {
                if ($status) {
                    const net = require("net");
                    let activeSocket : any = null;
                    let isConnected : boolean = false;
                    const connector : HubConnector = new HubConnector(true,
                        this.builderConfig.hub.url + "/connector.config.jsonp");
                    connector.getEvents().OnStart(() : void => {
                        connector
                            .InitPipe(pipeId)
                            .OnConnect(($clientId : string) : void => {
                                LogIt.Debug("[local] Connection successful.");
                                const localServer = net.createServer({pauseOnConnect: true}, ($socket : any) : void => {
                                    isConnected = true;
                                    localServer.getConnections(($error : Error, $connectionNum : number) : void => {
                                        if (!ObjectValidator.IsEmptyOrNull($error)) {
                                            LogIt.Error("[local] Failed to parse debug server info.");
                                        } else if ($connectionNum > 1) {
                                            LogIt.Warning("[local] Debugger is already attached.");
                                        } else {
                                            activeSocket = $socket;
                                            $socket.on("data", ($data : any) => {
                                                $socket.pause();
                                                connector.WriteToPipe($clientId, pipeId, {
                                                    data: $data.toString("base64"),
                                                    type: "debug"
                                                })
                                                    .Then(($status : boolean, $message? : string) : void => {
                                                        if ($status) {
                                                            $socket.resume();
                                                        } else {
                                                            LogIt.Error($message);
                                                        }
                                                    });
                                            });
                                            $socket.on("error", ($data : any) : void => {
                                                LogIt.Error("[local] Socket error: " + $data.message);
                                            });
                                            $socket.on("end", () : void => {
                                                isConnected = false;
                                                // mustn't end on protocol swap, but must protect against weird debugger behaviour - eg.
                                                // restarting on attach disconnect, which happens even during direct testing through cmd
                                                EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                                                    if (!isConnected) {
                                                        LogIt.Error("[local] Debugger disconnected. Ending remote attach.");
                                                    }
                                                }, 300);
                                            });
                                            $socket.resume();
                                        }
                                    });
                                });
                                localServer.listen(clientPort, () : void => {
                                    LogIt.Debug("[local] Debugger for project \"" +
                                        projectName + "\" is listening on port " + clientPort + ".");
                                });
                            })
                            .OnMessage(($data : IConnectorPipeProtocol) : void => {
                                switch ($data.type) {
                                case "debug":
                                    if (!ObjectValidator.IsEmptyOrNull(activeSocket) && activeSocket.writable) {
                                        // LogIt.Debug(Buffer.from($data, "base64").toString());
                                        activeSocket.write(Buffer.from($data.data, "base64"));
                                    }
                                    break;
                                default:
                                    LogIt.Error("[local] Invalid message type received from pipe: \"" + $data.type + "\"");
                                }
                            })
                            .OnDisconnect(() : void => {
                                LogIt.Error("[local] Pipe disconnected. Ending remote attach.");
                            })
                            .Then(($status : boolean, $message? : string) : void => {
                                if ($status) {
                                    LogIt.Debug("Pipe initialized. Awaiting debugger attach.");
                                    $done();
                                } else {
                                    LogIt.Error($message);
                                }
                            });
                    });
                    connector.getEvents().OnClose(() : void => {
                        LogIt.Error("[local] Connection has been closed.");
                    });
                    connector.getEvents().OnTimeout(() : void => {
                        LogIt.Error("[local] Connection timeout has been reached.");
                    });
                    connector.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                        LogIt.Error("[local] " + $eventArgs.Exception().ToString());
                    });
                    connector.StartCommunication();
                } else {
                    LogIt.Error("Port " + clientPort + " is not free. Attach initialization failed.");
                }
            });
        }
    }
}
