/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class JSCompress extends BaseTask {

    protected getName() : string {
        return "uglify";
    }

    protected async processAsync($option : string) : Promise<void> {
        const uglify : any = require("uglify-js");
        const os : any = require("os");

        const configs : any = {
            prod  : {
                options: {
                    compress: {
                        booleans     : true,
                        collapse_vars: true,
                        comparisons  : true,
                        conditionals : true,
                        dead_code    : true,
                        drop_console : false,
                        drop_debugger: true,
                        evaluate     : true,
                        expression   : true,
                        hoist_funs   : true,
                        hoist_vars   : false,
                        if_return    : true,
                        inline       : true,
                        join_vars    : true,
                        keep_fargs   : false,
                        keep_fnames  : false,
                        keep_infinity: false,
                        loops        : true,
                        negate_iife  : true,
                        properties   : true,
                        pure_getters : true,
                        reduce_vars  : true,
                        sequences    : true,
                        side_effects : true,
                        switches     : true,
                        unsafe       : false,
                        unsafe_comps : false,
                        unsafe_math  : false,
                        unsafe_proto : false,
                        unsafe_regexp: false,
                        unused       : false
                    },
                    ie8     : true
                },
                source : [this.properties.tmp + "/typescript/*.js"],
                dest   : this.properties.dest + "/resource/javascript/" + this.properties.packageName + ".min.js"
            },
            loader: {
                options: {
                    compress: {
                        hoist_vars: true
                    },
                    ie8     : true
                },
                source : ["build/compiled/resource/javascript/Loader.js"],
                dest   : this.properties.dest + "/" + this.properties.loaderPath
            },
            es    : {
                options: {
                    compress: {
                        booleans     : true,
                        collapse_vars: true,
                        comparisons  : true,
                        conditionals : true,
                        dead_code    : true,
                        drop_console : false,
                        drop_debugger: true,
                        evaluate     : true,
                        expression   : true,
                        hoist_funs   : true,
                        hoist_vars   : false,
                        if_return    : true,
                        inline       : true,
                        join_vars    : true,
                        keep_fargs   : false,
                        keep_fnames  : false,
                        keep_infinity: false,
                        loops        : true,
                        negate_iife  : true,
                        properties   : true,
                        pure_getters : true,
                        reduce_vars  : true,
                        sequences    : true,
                        side_effects : true,
                        switches     : true,
                        unsafe       : false,
                        unsafe_comps : false,
                        unsafe_math  : false,
                        unsafe_proto : false,
                        unsafe_regexp: false,
                        unused       : false,
                        module       : true,
                        webkit       : true
                    },
                    ie8     : true
                }
            }
        };
        if (ObjectValidator.IsEmptyOrNull($option)) {
            LogIt.Error("uglify task requires option specification");
        }
        if (!configs.hasOwnProperty($option)) {
            LogIt.Error("uglify config \"" + $option + "\" has not been found");
        }
        const optionConfig : any = configs[$option];
        if ($option === "loader" &&
            !this.fileSystem.Exists(this.properties.projectBase + "/build/compiled/resource/javascript/Loader.js")) {
            optionConfig.source = ["build/compiled/dependencies/**/resource/javascript/Loader.js"];
        }
        let banner : string = this.properties.licenseText;
        if (!ObjectValidator.IsEmptyOrNull(banner)) {
            banner += os.EOL;
        }
        if (this.properties.projectHas.ESModules()) {
            this.fileSystem.Expand([
                this.properties.dest + "/resource/javascript/**/*.js",
                this.properties.dest + "/resource/esmodules-*/**/*.js"
            ]).forEach(($file : string) : void => {
                if (!StringUtils.EndsWith($file, ".min.js") || StringUtils.EndsWith($file, "loader.min.js")) {
                    try {
                        const code : any = {};
                        code[$file] = this.fileSystem.Read($file).toString();
                        const result : any = uglify.minify(code, optionConfig.options);
                        if (!ObjectValidator.IsEmptyOrNull(result.error)) {
                            LogIt.Warning("Uglify task has failed for: {0}. {1}", $file, result.error.message);
                        } else {
                            this.fileSystem.Write($file, banner + result.code);
                        }
                    } catch (ex) {
                        LogIt.Warning("Failed to compress file: {0}. {1}", $file, ex.message);
                    }
                }
            });
        } else {
            optionConfig.source.forEach(($source : string) : void => {
                try {
                    const code : any = {};
                    this.fileSystem.Expand($source).forEach(($file : string) : void => {
                        code[$file] = this.fileSystem.Read($file).toString();
                    });
                    const result : any = uglify.minify(code, optionConfig.options);
                    if (!ObjectValidator.IsEmptyOrNull(result.error)) {
                        LogIt.Error("Uglify task has failed.", result.error);
                    }
                    this.fileSystem.Write(this.properties.projectBase + "/" + optionConfig.dest, banner + result.code);
                } catch (ex) {
                    LogIt.Error("Uglify task has failed.", ex);
                }
            });
        }
    }
}
