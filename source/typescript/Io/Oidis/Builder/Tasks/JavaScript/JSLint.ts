/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class JSLint extends BaseTask {

    protected getName() : string {
        return "jslint";
    }

    protected async processAsync($option : string) : Promise<void> {
        const config : any = {
            bin    : [
                this.properties.projectBase + "/bin/resource/scripts/**/*.js",
                this.properties.projectBase + "/bin/project/**/*.jsonp"
            ],
            configs: [
                this.properties.projectBase + "/" + this.properties.resources + "/configs/**/*.jsonp",
                this.properties.projectBase + "/" + this.properties.resources + "/data/**/*.jsonp"
            ],
            source : [
                this.properties.projectBase + "/" + this.properties.resources + "/javascript/**/*.js",
                this.properties.projectBase + "/xcpp_scripts/**/*.js"
            ]
        };
        if (this.programArgs.getOptions().withDependencies) {
            config.configs.push(this.properties.projectBase + "/" + this.properties.dependenciesResources + "/data/**/*.jsonp");
            config.source.push(this.properties.projectBase + "/" + this.properties.dependenciesResources + "/javascript/**/*.js");
        }
        if (config.hasOwnProperty($option)) {
            let options : any = await this.resolveExternConfig("jslint");
            let oldDetected : boolean = options.hasOwnProperty("esversion");
            if (oldDetected) {
                options = {};
            }
            const {ESLint} = require("eslint");
            const eslint = require("@eslint/js");
            const tseslint = require("typescript-eslint");
            const files : string[] = this.fileSystem.Expand(config[$option]);
            if (files.length === 0) {
                LogIt.Info(">>"[ColorType.RED] + " 0 files linted");
                return;
            }
            const configs : any = [
                eslint.configs.recommended,
                options
            ];
            if (ObjectValidator.IsSet(options.overrides)) {
                const overrides : any[] = JsonUtils.Clone(options.overrides);
                delete options.overrides;
                if (!ObjectValidator.IsEmptyOrNull(overrides)) {
                    configs.push(...overrides);
                }
            }
            configs.push({
                files
            });
            const linter : any = new ESLint({
                fix               : false,
                overrideConfig    : tseslint.config(...configs),
                overrideConfigFile: true
            });
            const result = await linter.lintFiles(files);
            let errorCount = 0;
            result.forEach(($report : any) : void => {
                errorCount += $report.errorCount;
                if (StringUtils.Contains($report.source, "/* jshint ignore")) {
                    oldDetected = true;
                }
            });
            if (oldDetected) {
                LogIt.Warning("Detected old JS Hint definition incompatible with current linting.\n" +
                    "Linting errors will be ignored and automatic cleanup disabled.\n" +
                    "For ability to enable linting migrate to ES Lint definition or downgrade to Oidis Builder 2024.3.2.");
            }
            if (errorCount > 0) {
                const formatter = await linter.loadFormatter("stylish");
                LogIt.Info(formatter.format(result));
                const message = "Not all of JavaScript files are lint free.";
                if (!this.project.lintErrorsAsWarnings && !oldDetected) {
                    LogIt.Error(message);
                } else {
                    LogIt.Warning("Suppressed lint error: " + message);
                    return;
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " " + files.length + " files lint free.");
            }
        } else {
            LogIt.Error("Unsupported jslint option \"" + $option + "\"");
        }
    }
}
