/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { ResourceType } from "../../Enums/ResourceType.js";
import { ScriptHandler } from "../../IOApi/Hadlers/ScriptHandler.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { ResourceHandler } from "../../Utils/ResourceHandler.js";

export class NodejsPackaging extends BaseTask {
    private isShared : boolean;

    protected getName() : string {
        return "nodejs-packaging";
    }

    protected async processAsync($option : string) : Promise<void> {
        this.isShared = $option === "shared";
        await this.prepareBasePackage();
        await this.embedResources();
        if (!this.isShared) {
            await this.createRelease();
        }
    }

    private resolveNodeLibName() : string {
        let nodeLibName : string;
        if (EnvironmentHelper.IsWindows()) {
            nodeLibName = "libnode.dll";
            if (!this.fileSystem.Exists(this.getNodePath() + "/" + nodeLibName)) {
                nodeLibName = "node.dll";
            }
        } else if (EnvironmentHelper.IsMac()) {
            nodeLibName = "libnode.dylib";
        } else {
            nodeLibName = "libnode.so";
        }
        return nodeLibName;
    }

    private async prepareBasePackage() : Promise<void> {
        const binExtension : string = EnvironmentHelper.IsWindows() ? ".exe" : "";
        const nodeLibName : string = this.resolveNodeLibName();
        const nodePath : string = this.getNodePath() + "/node" + binExtension;
        const libPath : string = this.getNodePath() + "/" + nodeLibName;
        if (!this.fileSystem.Exists(nodePath)) {
            LogIt.Error("Node.js instance has not been found at: " + nodePath);
        }
        if (this.isShared) {
            if (this.fileSystem.Exists(libPath)) {
                await this.fileSystem.CopyAsync(libPath, this.properties.projectBase + "/build/target/" + nodeLibName);
            }
            await this.fileSystem.CopyAsync(nodePath, this.properties.projectBase + "/build/target/node" + binExtension);
        } else {
            const pkgPath : string = this.properties.projectBase + "/build_cache/NodejsRE/" + this.build.product.OS() +
                "/" + this.project.target.name + binExtension;
            if (!this.fileSystem.Exists(pkgPath)) {
                await this.fileSystem.CopyAsync(nodePath, pkgPath);
                if (this.fileSystem.Exists(libPath)) {
                    await this.fileSystem.CopyAsync(libPath, this.properties.projectBase + "/build_cache/NodejsRE/" +
                        this.build.product.OS() + "/" + nodeLibName);
                    await ResourceHandler.ModifyIcon(pkgPath, this.properties.projectBase + "/" + this.project.target.icon);
                    await ResourceHandler.ModifyVersionInfo(pkgPath, this.project.target.name);
                } else {
                    LogIt.Warning("Using static build of NodejsRE is deprecated.");
                    await ResourceHandler.ModifyIcon(pkgPath, this.properties.projectBase + "/" + this.project.target.icon);
                    await ResourceHandler.ModifyVersionInfo(pkgPath, this.project.target.name);
                }
            }
        }
    }

    private async embedResources() : Promise<void> {
        const withDependencyScan : boolean = this.project.target.toolchainSettings.withDependenciesScan;
        const dependencyTree : any = require("dependency-tree");
        const fs : any = require("fs");
        const PATH : any = require("path");

        const extension : string = EnvironmentHelper.IsWindows() ? ".exe" : "";
        const pkgPath : string = this.properties.projectBase + "/build_cache/NodejsRE/" + this.build.product.OS() +
            "/" + this.project.target.name + extension;
        const pkgConfigPath : string = this.getNodeBuildPath() + "/package.json";
        const pkgConfig : any = JSON.parse(this.fileSystem.Read(pkgConfigPath).toString()).pkg;
        const nodeModules : string = this.fileSystem.NormalizePath(this.getNodeBuildPath() + "/node_modules");
        const jsFilesPath : string = this.properties.projectBase + "/build/target/resource/javascript";
        const mjsFilesPath : string = this.properties.projectBase + "/build/target/resource/esmodules-" + this.build.timestamp;
        const mainFile : string = jsFilesPath + "/" + this.properties.packageName + ".min.js";

        if (ObjectValidator.IsEmptyOrNull(pkgConfig.scripts)) {
            pkgConfig.scripts = [];
        }
        if (ObjectValidator.IsEmptyOrNull(pkgConfig.assets)) {
            pkgConfig.assets = [];
        }
        const patchesScripts : any[] = [];
        if (!ObjectValidator.IsEmptyOrNull(pkgConfig.patches)) {
            const patches = {};
            pkgConfig.patches.forEach(($patch : any) : void => {
                let patchRoot : string = nodeModules;
                if (!StringUtils.IsEmpty($patch.cwd)) {
                    patchRoot += "/" + $patch.cwd;
                }
                if (!StringUtils.IsEmpty($patch.src) && !StringUtils.IsEmpty($patch.dest)) {
                    patches[patchRoot + "/" + $patch.dest] = $patch;
                }
                if (!StringUtils.IsEmpty($patch.script)) {
                    if (StringUtils.IsEmpty($patch.cwd)) {
                        $patch.cwd = nodeModules;
                    }
                    if (ObjectValidator.IsEmptyOrNull($patch.attributes)) {
                        $patch.attributes = [];
                    }
                    patchesScripts.push($patch);
                }
            });
            pkgConfig.patches = patches;
        } else {
            pkgConfig.patches = {};
        }
        const scripts : string[] = [mainFile];
        const assets : string[] = this.fileSystem.Expand(nodeModules + "/**/package.json");
        assets.push(pkgConfigPath);
        if (this.fileSystem.Exists(mainFile + ".map")) {
            scripts.push(nodeModules + "/source-map-support/source-map-support.js");
            assets.push(mainFile + ".map");
        }

        if (this.properties.projectHas.ESModules()) {
            const modules : string[] = this.fileSystem.Expand([
                jsFilesPath + "/loader.min.js",
                mjsFilesPath + "/**/*.js"
            ]);
            for (const module of modules) {
                let content : string = this.fileSystem.Read(module).toString();
                let moduleDestination : string = module;
                if (!this.isShared) {
                    content = content.replace(/^(import .* from ")(.*).js(";)$/gmi, "$1$2.mjs$3");
                    content = content.replace(/(await import\(.*).js("\))/gmi, "$1.mjs$2");
                    content = content.replace(/^(import ".*).js(";)$/gmi, "$1.mjs$2");
                    this.fileSystem.Write(module, content);
                    moduleDestination = StringUtils.Replace(module, ".js", ".mjs");
                    this.fileSystem.Rename(module, moduleDestination);
                }
                if (withDependencyScan &&
                    !scripts.includes(moduleDestination) && StringUtils.Contains(content, "require(")) {
                    scripts.push(moduleDestination);
                    const requireParser : RegExp = /require\("(?<id>.*)"\)/gm;
                    let requireData : any = requireParser.exec(content);
                    while (!ObjectValidator.IsEmptyOrNull(requireData)) {
                        const requireModule : string = nodeModules + "/" + requireData.groups.id;
                        if (this.fileSystem.Exists(requireModule) && !scripts.includes(requireModule)) {
                            LogIt.Debug("> required " + requireModule);
                            this.fileSystem.Expand(requireModule + "/index.js").forEach(($asset : string) : void => {
                                if (assets.indexOf($asset) === -1) {
                                    assets.push($asset);
                                    if (!scripts.includes($asset)) {
                                        scripts.push($asset);
                                    }
                                }
                            });
                            this.fileSystem.Expand(requireModule + "/package.json").forEach(($asset : string) : void => {
                                try {
                                    const packageContent : any = JSON.parse(this.fileSystem.Read($asset).toString());
                                    if (packageContent.hasOwnProperty("main")) {
                                        let packageMain : string =
                                            this.fileSystem.NormalizePath(requireModule + "/" + packageContent.main);
                                        if (!this.fileSystem.IsFile(packageMain)) {
                                            packageMain += ".js";
                                        }
                                        if (this.fileSystem.IsFile(packageMain) && assets.indexOf(packageMain) === -1) {
                                            assets.push(packageMain);
                                            if (!scripts.includes(packageMain)) {
                                                scripts.push(packageMain);
                                            }
                                        }
                                    }
                                } catch (ex) {
                                    LogIt.Warning(ex.stack);
                                }
                            });
                        }
                        requireData = requireParser.exec(content);
                    }
                }
            }
        }

        pkgConfig.scripts.forEach(($scriptPattern : string) : void => {
            this.fileSystem.Expand(nodeModules + "/" + $scriptPattern).forEach(($script : string) : void => {
                if (scripts.indexOf($script) === -1) {
                    scripts.push($script);
                }
            });
        });
        const aliases : any[] = [];
        pkgConfig.assets.forEach(($assetPattern : string) : void => {
            this.fileSystem.Expand([
                this.properties.projectBase + "/build/target/" + $assetPattern,
                nodeModules + "/" + $assetPattern
            ]).forEach(($asset : string) : void => {
                if (this.fileSystem.IsFile($asset)) {
                    if (!this.isShared) {
                        if (StringUtils.EndsWith($asset, ".ico")) {
                            aliases.push({
                                file: $asset,
                                res : $asset + "_res"
                            });
                            $asset += "_res";
                        } else if (!StringUtils.Contains($asset, ".")) {
                            aliases.push({
                                file: $asset,
                                res : $asset + ".bin_res"
                            });
                            $asset += ".bin_res";
                        }
                    }
                    if (assets.indexOf($asset) === -1) {
                        assets.push($asset);
                    }
                }
            });
        });
        const rollbackAliasName : any = ($path : string) : string => {
            if (StringUtils.EndsWith($path, ".ico_res")) {
                $path = StringUtils.Replace($path, ".ico_res", ".ico");
            } else if (StringUtils.EndsWith($path, ".bin_res")) {
                $path = StringUtils.Replace($path, ".bin_res", "");
            }
            return $path;
        };
        const processAliases : any = ($revert : boolean = false) : void => {
            if (!this.isShared) {
                aliases.forEach(($alias : any) : void => {
                    if (!$revert) {
                        this.fileSystem.Rename($alias.file, $alias.res);
                    } else {
                        this.fileSystem.Rename($alias.res, $alias.file);
                    }
                });
            }
        };
        const packagedMapPath : string = this.properties.projectBase + "/build_cache/NodejsRE/" +
            this.build.product.OS() + "/packedMap.json";
        let packageMap : any = {};
        let isUpdate : boolean = false;
        if (!this.isShared && this.fileSystem.Exists(packagedMapPath)) {
            isUpdate = true;
            packageMap = JSON.parse(this.fileSystem.Read(packagedMapPath).toString());
        }

        const resources : string[] = [];
        if (!isUpdate) {
            if (withDependencyScan) {
                LogIt.Info("> scanning dependencies ...");
                const nonExistent : string[] = [];
                const cache : any = {};
                scripts.forEach(($file : string) : void => {
                    LogIt.Debug("> based on script: {0}", $file);
                    const scanOptions : any = {
                        detective: {
                            es6: {
                                mixedImports: true
                            }
                        },
                        directory: nodeModules,
                        filename : $file,
                        nonExistent,
                        visited  : cache
                    };
                    dependencyTree.toList(scanOptions).forEach(($resource : string) : void => {
                        if (resources.indexOf($resource) === -1) {
                            resources.push($resource);
                        }
                    });
                });
                if (!ObjectValidator.IsEmptyOrNull(nonExistent)) {
                    LogIt.Debug("WARNING: Can't resolve dependencies: {0}", nonExistent);
                }
            } else {
                this.fileSystem.Expand(nodeModules + "/**/*.*js*").forEach(($resource : string) : void => {
                    if (this.fileSystem.IsFile($resource) && resources.indexOf($resource) === -1) {
                        resources.push($resource);
                    }
                });
            }
        } else if (this.isShared) {
            const snapshotPath : string = "/snapshot";
            let packageItem : string;
            for (packageItem in packageMap) {
                if (packageMap.hasOwnProperty(packageItem)) {
                    let localPath : string = packageItem;
                    if (StringUtils.Contains(localPath, "/node_modules/")) {
                        localPath = StringUtils.Replace(localPath, snapshotPath + "/node_modules/", nodeModules + "/");
                        if (resources.indexOf(localPath) === -1) {
                            resources.push(localPath);
                        }
                    }
                }
            }
        }
        assets.forEach(($asset : string) : void => {
            if (resources.indexOf($asset) === -1) {
                resources.push($asset);
            }
        });
        const embedMap : any[] = [];
        const registerEmbedRecord : any = ($type : ResourceType, $name : string, $source : string, $module : string,
                                           $isUpdate : boolean = false) : void => {
            $source = this.fileSystem.NormalizePath($source);
            embedMap.push({type: $type, name: $name, source: $source, module: $module, isUpdate: $isUpdate});
        };
        const finishEmbedding : any = () : void => {
            processAliases(true);
            LogIt.Info(">>"[ColorType.YELLOW] + " " + embedMap.length + " files have been " +
                (this.isShared ? "copied" : "embedded") + ".");
        };

        const processPatch : any = ($patch : any) : Promise<void> => {
            return new Promise<void>(($resolve) => {
                const handler : ScriptHandler = new ScriptHandler();
                handler.Path(this.findPatch($patch.script));
                handler.Environment({
                    Process($cwd : string, $args : string[], $done : () => void) : void {
                        LogIt.Warning("Patch skipped: script did not override global Process method");
                        $done();
                    }
                });
                handler.SuccessHandler(() : void => {
                    $resolve();
                });
                handler.ErrorHandler(($error : Error) : void => {
                    LogIt.Error("Patch script has failed.", $error);
                });
                handler.Process($patch.cwd, $patch.attributes);
            });
        };

        const snapshotPath : string = "/snapshot";
        const resourcesMap : string[] = [
            snapshotPath + "/",
            snapshotPath + "/node_modules/",
            snapshotPath + "/resource/"
        ];
        packageMap[snapshotPath] = {
            name : null,
            stats: null
        };
        packageMap[snapshotPath + "/node_modules"] = {
            name : null,
            stats: null
        };
        packageMap[snapshotPath + "/resource"] = {
            name : null,
            stats: this.serializeStats(fs.lstatSync(this.properties.projectBase + "/build/target/resource"))
        };
        const processResource : any = ($file : string) : void => {
            $file = this.fileSystem.NormalizePath($file);
            let module : string;
            if ($file === pkgConfigPath) {
                module = snapshotPath + "/package.json";
            } else if (pkgConfig.patches.hasOwnProperty($file)) {
                const patch : any = pkgConfig.patches[$file];
                LogIt.Debug("Patch {0} by {1}", $file, patch.src);
                $file = this.properties.projectBase + "/" + patch.src;
                module = snapshotPath + "/node_modules/" + patch.dest;
            } else {
                module = StringUtils.Replace($file, nodeModules + "/", snapshotPath + "/node_modules/");
                module = StringUtils.Replace(module, this.properties.projectBase + "/build/target/", snapshotPath + "/");
                module = rollbackAliasName(module);
            }
            const record : any = {
                name : "RES_" + StringUtils.ToUpperCase(StringUtils.getSha1($file)),
                stats: null
            };
            const exists : boolean = packageMap.hasOwnProperty(module);
            const isModule : boolean = StringUtils.Contains(module, "node_modules");
            if (!exists || !isModule || this.isShared && isModule) {
                registerEmbedRecord(ResourceType.RESOURCE, record.name, $file, module, exists && isUpdate);
            }
            let stats : any = fs.lstatSync(rollbackAliasName($file));
            if ((stats.size <= 0) && (PATH.extname($file) === ".js")) {
                LogIt.Warning("Empty resource to embed has been found: " + $file);
                this.fileSystem.Write($file, "// this is dummy file to bypass resource manager empty check because some module " +
                    "requires this file while it is empty :-)");
                stats = fs.lstatSync(rollbackAliasName($file));
            }
            record.stats = this.serializeStats(stats);
            if (StringUtils.StartsWith(module, snapshotPath)) {
                let modulePath : string = "/";
                StringUtils.Split(StringUtils.Substring(module, 1, StringUtils.IndexOf(module, "/", false)), "/")
                    .forEach(($path : string) : void => {
                        modulePath += $path;
                        if (resourcesMap.indexOf(modulePath) === -1) {
                            let localPath : string = modulePath + "/";
                            if (StringUtils.Contains(localPath, "/node_modules/")) {
                                localPath = StringUtils.Replace(localPath, snapshotPath + "/node_modules/", nodeModules + "/");
                            } else {
                                localPath = StringUtils.Replace(localPath,
                                    snapshotPath + "/", this.properties.projectBase + "/build/target/");
                            }
                            packageMap[modulePath] = {
                                name : null,
                                stats: this.serializeStats(fs.lstatSync(localPath))
                            };
                        }
                        modulePath += "/";
                    });
            }
            packageMap[module] = record;
        };
        resources.forEach(processResource);

        processAliases();
        const metadataFile : string = this.properties.projectBase + "/build/target/resource/metadata.json";
        const metadata : any = {
            modules: JSON.parse(this.fileSystem.Read(this.getNodeBuildPath() + "/package.json").toString()).devDependencies,
            native : this.fileSystem.Expand(this.getNodeBuildPath() + "/**/*.node")
                .map(($file) => $file.slice($file.indexOf("node_modules")).replace("node_modules/", ""))
        };
        if (this.fileSystem.Write(metadataFile, JSON.stringify(metadata, null, 2))) {
            processResource(metadataFile);
        } else {
            LogIt.Error("Failed to save metadata resource file.");
        }

        if (this.fileSystem.Write(packagedMapPath, JSON.stringify(packageMap, null, 2))) {
            registerEmbedRecord(ResourceType.CONFIG, "PACKAGE_MAP", packagedMapPath, snapshotPath + "/packedMap.json", isUpdate);
        } else {
            LogIt.Error("Failed to create map of embedded resources.");
        }
        if (!isUpdate) {
            for await (const script of patchesScripts) {
                await processPatch(script);
            }
        }
        await this.embed(pkgPath, embedMap);
        finishEmbedding();
    }

    private findPatch($scriptName : string) : string {
        const getPath : any = ($pattern : string) : string => {
            $pattern = this.fileSystem.NormalizePath($pattern);
            const fileList : string[] = this.fileSystem.Expand([$pattern]);
            if (fileList.length > 0) {
                return fileList[0];
            } else {
                return "";
            }
        };
        // todo use DependenciesInstall find script method instead
        const availablePaths : string[] = [
            this.properties.projectBase + "/" + $scriptName,
            this.properties.projectBase + "/bin/resource/scripts/" + $scriptName,
            this.properties.projectBase + "/resource/scripts/" + $scriptName,
            this.properties.projectBase + "/dependencies/*/resource/scripts/" + $scriptName
        ];
        let path : string = "";
        let index : number;
        for (index = 0; index < availablePaths.length; index++) {
            if (ObjectValidator.IsEmptyOrNull(path)) {
                path = getPath(availablePaths[index]);
            } else {
                break;
            }
        }
        return path;
    }

    private serializeStats($stats : any) : string {
        const statsRecord : any = {};
        if (!$stats.isFile()) {
            statsRecord.isFile = false;
        }
        if ($stats.isDirectory()) {
            statsRecord.isDirectory = true;
        }
        if ($stats.isBlockDevice()) {
            statsRecord.isBlockDevice = true;
        }
        if ($stats.isCharacterDevice()) {
            statsRecord.isCharacterDevice = true;
        }
        if ($stats.isSymbolicLink()) {
            statsRecord.isSymbolicLink = true;
        }
        if ($stats.isFIFO()) {
            statsRecord.isFIFO = true;
        }
        if ($stats.isSocket()) {
            statsRecord.isSocket = true;
        }
        statsRecord.dev = $stats.dev;
        statsRecord.mode = $stats.mode;
        if ($stats.nlink !== 1) {
            statsRecord.nlink = $stats.nlink;
        }
        if ($stats.uid !== 0) {
            statsRecord.uid = $stats.uid;
        }
        if ($stats.gid !== 0) {
            statsRecord.gid = $stats.gid;
        }
        if ($stats.rdev !== 0) {
            statsRecord.rdev = $stats.rdev;
        }
        statsRecord.ino = $stats.ino;
        statsRecord.size = $stats.size;
        statsRecord.atimeMs = $stats.atimeMs;
        statsRecord.mtimeMs = $stats.mtimeMs;
        statsRecord.ctimeMs = $stats.ctimeMs;
        statsRecord.atime = $stats.atime;
        statsRecord.mtime = $stats.mtime;
        statsRecord.ctime = $stats.ctime;
        statsRecord.birthtime = $stats.birthtime;
        return JSON.stringify(statsRecord);
    }

    private async createRelease() : Promise<void> {
        const targetPath : string = this.properties.projectBase + "/build/target";
        const cleanTarget : string = this.properties.projectBase + "/build/cleantarget";

        let targetName : string = this.project.target.name;
        if (EnvironmentHelper.IsWindows()) {
            targetName += ".exe";
        }
        const absolutePath : string = targetPath + "/" + targetName;
        let status : boolean = true;
        try {
            await this.terminal.KillAsync(absolutePath);
        } catch (ex) {
            status = false;
        }
        if (status || EnvironmentHelper.IsLinux() || !this.fileSystem.Exists(absolutePath)) {
            const pkgConfig : any = JSON.parse(this.fileSystem.Read(this.getNodeBuildPath() + "/package.json").toString()).pkg;
            let assets : string[] = [
                targetPath + "/localStorage",
                targetPath + "/log",
                targetPath + "/**/connector.config.jsonp",
                targetPath + "/resource/javascript/" + this.properties.packageName + ".min.js",
                targetPath + "/resource/metadata.json"
            ];
            if (!ObjectValidator.IsEmptyOrNull(pkgConfig.assets)) {
                pkgConfig.assets.forEach(($asset : string) : void => {
                    $asset = targetPath + "/" + $asset;
                    if (assets.indexOf($asset) === -1) {
                        assets.push($asset);
                    }
                });
            }
            assets = this.fileSystem.Expand(assets);

            const targetFiles : string[] = this.fileSystem.Expand(targetPath + "/**/*");
            for await (const file of targetFiles) {
                if (assets.indexOf(file) === -1 && this.fileSystem.IsFile(file)) {
                    await this.fileSystem.CopyAsync(file,
                        cleanTarget + "/" + StringUtils.Remove(file, targetPath + "/"));
                }
            }
            this.fileSystem.Delete(targetPath);
            await this.fileSystem.CopyAsync(cleanTarget, targetPath);
            this.fileSystem.Delete(cleanTarget);
            await this.fileSystem.CopyAsync(
                this.properties.projectBase + "/build_cache/NodejsRE/" + this.build.product.OS() + "/" + targetName,
                targetPath + "/" + targetName);
            const libName : string = this.resolveNodeLibName();
            await this.fileSystem.CopyAsync(
                this.properties.projectBase + "/build_cache/NodejsRE/" + this.build.product.OS() +
                "/" + libName,
                targetPath + "/" + libName);
        } else {
            LogIt.Error("Unable to clean up target folder. " +
                "Validated that \"" + targetPath + "\" is not hold by another process.");
        }
    }

    private getNodePath() : string {
        let nodePath : string = this.properties.projectBase + "/dependencies/nodejs";
        if (this.fileSystem.Exists(nodePath + "/" + this.build.product.OS().toString())) {
            nodePath = nodePath + "/" + this.build.product.OS().toString();
        } else if (this.fileSystem.Exists(nodePath)) {
            LogIt.Info(">>"[ColorType.YELLOW] + " Node.js selected for default (single) prepared platform.");
        } else {
            LogIt.Error("Node.js instance has not been found at: " + nodePath);
        }
        return nodePath;
    }

    private getNodeBuildPath() : string {
        let node : string = this.getNodePath();
        if (this.fileSystem.Exists(node + "/build_" + this.build.product.OS().toString())) {
            node = node + "/build_" + this.build.product.OS().toString();
        } else {
            // fallback to old nodejs package structure
            node = node + "/build";
        }
        return node;
    }

    private async embed($pkgPath : string, $resourcesMap : any[]) : Promise<void> {
        if (this.isShared) {
            const targetPath : string = this.properties.projectBase + "/build/target/resource/nodejs/";
            for (const resource of $resourcesMap) {
                if (this.fileSystem.IsFile(resource.source)) {
                    if (StringUtils.Contains(resource.module, "/node_modules/")) {
                        LogIt.Debug("Copy from {0} to {1}", resource.source,
                            targetPath + StringUtils.Remove(resource.module, "/snapshot/"));
                        this.fileSystem.WriteStream(targetPath + StringUtils.Remove(resource.module, "/snapshot/"),
                            this.fileSystem.ReadStream(resource.source));
                    }
                } else if (!this.fileSystem.Exists(resource.source)) {
                    LogIt.Warning("Resource not found: " + resource.source);
                }
            }
        } else if (EnvironmentHelper.IsWindows()) {
            let embeddingScript : string =
                "[FILENAMES]\n" +
                "Exe=   \"" + $pkgPath + "\"\n" +
                "SaveAs=\"" + $pkgPath + "\"\n" +
                "[COMMANDS]\n";
            $resourcesMap.forEach(($resource : any) : void => {
                const source = this.fileSystem.NormalizePath($resource.source);
                embeddingScript += "-addoverwrite \"" + source + "\", " + $resource.type + "," + $resource.name + ",\n";
            });
            await ResourceHandler.RunScript(embeddingScript);
        } else {
            const resources : any = require("resource_manager");
            resources.initialize($pkgPath);
            const logCallback : any = ($message : string, $verbosity : string) : void => {
                if ($verbosity === "error") {
                    LogIt.Error($message);
                } else if (this.programArgs.IsDebug()) {
                    LogIt.Debug($message);
                }
            };

            for (const resource of $resourcesMap) {
                if (this.fileSystem.Exists(resource.source)) {
                    resources.write(resource.type, resource.name, resource.source, logCallback);
                } else {
                    LogIt.Warning("Resource not found: " + resource.source);
                }
            }
        }
    }
}
