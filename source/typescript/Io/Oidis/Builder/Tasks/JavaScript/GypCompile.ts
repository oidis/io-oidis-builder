/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { ITerminalOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/TerminalConnector.js";
import { ArchType } from "../../Enums/ArchType.js";
import { OSType } from "../../Enums/OSType.js";
import { ProductType } from "../../Enums/ProductType.js";
import { IToolchainSettings } from "../../Interfaces/IProject.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { ResourceHandler } from "../../Utils/ResourceHandler.js";
import { Toolchains } from "../../Utils/Toolchains.js";

export class GypCompile extends BaseTask {
    private toolchain : IToolchainSettings;
    private cwd : string;
    private isStatic : boolean;
    private is64bit : boolean;
    private productExits : boolean;

    protected getName() : string {
        return "gyp-compile";
    }

    protected async processAsync($option : string) : Promise<void> {
        this.toolchain = Toolchains.getToolchain();
        this.cwd = this.toolchain.cwd;
        if (ObjectValidator.IsEmptyOrNull(this.cwd)) {
            this.cwd = this.properties.projectBase;
        } else if (!StringUtils.StartsWith(this.cwd, this.properties.projectBase)) {
            this.cwd = this.properties.projectBase + "/" + this.cwd;
        }
        if (this.fileSystem.Exists(this.cwd + "/node.gyp")) {
            this.isStatic = this.build.product.Type() === ProductType.STATIC;
            this.is64bit = this.build.product.Arch() === ArchType.X64;
            let buildPath : string = "Release";
            if (!EnvironmentHelper.IsWindows()) {
                buildPath = "out/" + buildPath;
            }
            this.productExits = !this.fileSystem.IsEmpty(this.cwd + "/" + buildPath);

            if (this.build.isRebuild || !this.productExits) {
                if (EnvironmentHelper.IsWindows()) {
                    const versionRc : string =
                        "0 ICON \"src/res/icon.ico\"\r\n" +
                        "\r\n" +
                        ResourceHandler.GenerateVersionRc("", this.project.target.name);
                    if (this.fileSystem.Write(this.cwd + "/src/res/node.rc", versionRc)) {
                        await this.fileSystem.CopyAsync(this.properties.projectBase + "/" + this.project.target.icon,
                            this.cwd + "/src/res/icon.ico");
                    } else {
                        LogIt.Error("Failed to create version.rc file");
                    }
                }
                await this.configure();
            } else {
                if (!EnvironmentHelper.IsWindows()) {
                    await this.configure();
                }
            }

            LogIt.Info("Gyp compile started");
            let cmd : string;
            const args : string[] = [];
            if (EnvironmentHelper.IsWindows()) {
                cmd = "vcbuild.bat";
                args.push("release",
                    "noprojgen",
                    "no-cctest",
                    this.is64bit ? "x64" : "x86",
                    this.isStatic ? "static" : "dll");
            } else {
                cmd = "make -j" + EnvironmentHelper.getCores();

                // On some configurations, even the configure with specified CC and CXX is not enough, and we have to force it here
                if (this.build.product.OS() === OSType.MAC) {
                    cmd = "CC=clang CXX=clang++ " + cmd;
                }
            }
            const res : IExecuteResult = await this.terminal.SpawnAsync(cmd, args, this.cwd);
            if (!(res.exitCode === 0 && !StringUtils.ContainsIgnoreCase(res.std[0] + res.std[1],
                "Building Node with reused solution failed."))) {
                LogIt.Error("Failed to compile GYP project. " + res.std[1]);
            }
        } else {
            LogIt.Error("GYP project has not been detected at: " + this.cwd);
        }
    }

    private async configure() : Promise<void> {
        LogIt.Info("Gyp configure started");
        let cmd : string;
        const args : string[] = [];
        const options : ITerminalOptions = <ITerminalOptions>{cwd: this.cwd};
        if (EnvironmentHelper.IsWindows()) {
            cmd = "vcbuild.bat";
            args.push(
                "release",
                "projgen",
                "clean",
                "nobuild",
                "no-cctest",
                this.isStatic ? "static" : "dll",
                this.is64bit ? "x64" : "x86");
            if (!ObjectValidator.IsEmptyOrNull(this.toolchain.gypVcVer)) {
                args.push(this.toolchain.gypVcVer);
            }
        } else {
            cmd = "./configure";
            args.push(
                "--without-npm",
                this.isStatic ? "--enable-static" : "--shared"
            );
            if (this.build.product.OS() === OSType.MAC) {
                cmd = "CC=clang CXX=clang++ " + cmd;
                args.push("--openssl-no-asm");
            }
            if (this.build.product.OS() === OSType.IMX || this.build.product.OS() === OSType.ARM) {
                let toolchain : IToolchainSettings = this.properties.toolchains.aarch64;
                if (this.build.product.Arch() === ArchType.X32) {
                    toolchain = this.properties.toolchains.aarch32;
                    args.push("--dest-cpu=arm");
                } else {
                    args.push("--dest-cpu=arm64");
                }

                args.push(
                    "--dest-os=linux",
                    "--cross-compiling",
                    "--with-arm-float-abi=hard",
                    "--openssl-no-asm"  // build with asm causes errors with -fPIC somewhere in openssl
                );
                options.env = process.env;
                options.env.CC = toolchain.CC;
                options.env.CXX = toolchain.CXX;
                options.env.LINK = toolchain.LD;
                options.env.RANLIB = toolchain.RANLIB;
                options.env.AR = toolchain.AR;

                options.env.CC_host = "gcc";
                options.env.CXX_host = "g++";
                options.env.LINK_host = "g++";
                options.env.RANLIB_host = "ranlib";
                options.env.AR_host = "ar";
            }
            if (this.build.isRebuild && this.productExits) {
                cmd = "make clean && " + cmd;
            }
        }

        const res : IExecuteResult = await this.terminal.SpawnAsync(cmd, args, options);
        if (res.exitCode !== 0) {
            LogIt.Error("Failed to configure GYP project. " + res.std[1]);
        }
    }
}
