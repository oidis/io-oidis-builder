/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BaseTask } from "../Primitives/BaseTask.js";

export class Snippet extends BaseTask {

    protected getName() : string {
        return "snippet";
    }

    protected async processAsync($option : string) : Promise<void> {
        /* THIS TASK IS ONLY FOR DEVELOPMENT, DO NOT COMMIT CHANGES HERE */
        // cli usage: oidis snippet [--no-target]

        LogIt.Info("Hello from Oidis Builder snippet task");
    }
}
