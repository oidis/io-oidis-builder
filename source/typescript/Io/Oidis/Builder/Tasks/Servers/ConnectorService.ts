/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BuilderServiceManager } from "../../Connectors/BuilderServiceManager.js";
import { HttpServer } from "../../HttpProcessor/HttpServer.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class ConnectorService extends BaseTask {

    protected getName() : string {
        return "connector-service";
    }

    protected process($done : any, $option : string) : void {
        const asyncHandler : any = ($status : boolean, $message : string) : void => {
            if ($status) {
                LogIt.Info($message);
                $done();
            } else {
                LogIt.Error($message);
            }
        };
        if ($option === "stop") {
            BuilderServiceManager.StopTask(this.properties.projectBase, "connector-service:start", asyncHandler);
        } else if ($option === "start") {
            const server : HttpServer = new HttpServer();
            server.setOnCloseHandler(() : void => {
                Loader.getInstance().Exit();
            });
            server.getEvents().OnStart(() : void => {
                if ($option !== "start") {
                    $done();
                } else {
                    LogIt.Info("Connector service has been started");
                }
            });
            server.StartConnector();
        } else {
            BuilderServiceManager.StartTask(this.properties.projectBase, "connector-service:start", asyncHandler,
                "Connector service has been started");
        }
    }
}
