/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { HttpMethodType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpMethodType.js";
import { HttpStatusType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/HttpStatusType.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BuilderServiceManager } from "../../Connectors/BuilderServiceManager.js";
import { Network } from "../../Connectors/Network.js";
import { Resources } from "../../DAO/Resources.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class MockServer extends BaseTask {
    private static singleton : MockServer;
    private port : number;
    private verbose : boolean;
    private logs : string[];

    public static getInstance() : MockServer {
        if (ObjectValidator.IsEmptyOrNull(MockServer.singleton)) {
            MockServer.singleton = new MockServer();
        }
        MockServer.getInstance = () : MockServer => {
            return MockServer.singleton;
        };
        return MockServer.singleton;
    }

    constructor() {
        super();
        this.port = -1;
        this.verbose = true;
        this.logs = [];
        MockServer.singleton = this;
    }

    public getLocation() : string {
        if (this.port !== -1) {
            return "http://localhost:" + this.port;
        }
        return null;
    }

    public Verbose($value? : boolean) : boolean {
        return this.verbose = Property.Boolean(this.verbose, $value);
    }

    public getLogs() : string[] {
        return this.logs;
    }

    protected getName() : string {
        return "mock-server";
    }

    protected process($done : any, $option : string) : void {
        const asyncHandler : any = ($status : boolean, $message : string) : void => {
            if ($status) {
                LogIt.Info($message);
                $done();
            } else {
                LogIt.Error($message);
            }
        };

        if ($option === "start" || $option === "restart") {
            BuilderServiceManager.StartTask(this.properties.projectBase, "mock-server:service", asyncHandler,
                "Mock server has been started");
        } else if ($option === "stop") {
            BuilderServiceManager.StopTask(this.properties.projectBase, "mock-server:service", asyncHandler);
        } else if (!ObjectValidator.IsEmptyOrNull(this.getLocation())) {
            LogIt.Info("> skipped: mock server is already running");
            $done();
        } else {
            if ($option === "service") {
                Loader.getInstance().setLogPath(this.properties.projectBase + "/log/mockserver.log");
            }
            const http : any = require("http");
            const fs : any = require("fs");
            const server : any = http.createServer();
            let configFiles : string[] = [];
            const configuration : any = {};
            if (!ObjectValidator.IsEmptyOrNull(this.project.target.mockServer.config)) {
                if (ObjectValidator.IsObject(this.project.target.mockServer.config)) {
                    Resources.Extend(configuration, this.project.target.mockServer.config);
                } else {
                    configFiles = this.fileSystem.Expand(this.project.target.mockServer.config);
                }
            } else {
                const configName : string = "MockServer.config.json";
                configFiles = this.fileSystem.Expand([
                    this.properties.projectBase + "/dependencies/*/test/resource/config/**/" + configName,
                    this.properties.projectBase + "/test/resource/**/" + configName
                ]);
            }
            if (configFiles.length === 0 && configuration.length === 0) {
                LogIt.Warning("Mock server has been skipped: Unable to find configuration file.");
                $done();
            } else {
                configFiles.forEach(($file : string) => {
                    Resources.Extend(configuration, JSON.parse(this.fileSystem.Read($file).toString()));
                });
                const logResponse : any = ($status : HttpStatusType, $headers : any, $body : string, $chunked : boolean) : void => {
                    if (ObjectValidator.IsObject($body)) {
                        $body = JSON.stringify($body, null, 2);
                    }
                    const message : string = StringUtils.Format("mock response status: {0}, headers: {1}, body: \"{2}\", chunked: {3}",
                        $status + "", JSON.stringify($headers, null, 2), $body, $chunked + "");
                    this.logs.push(message);
                    if (this.verbose) {
                        LogIt.Debug(message);
                    }
                };
                const send : any = (data : any, $request : any, $response : any) : void => {
                    let status : number = 200;
                    if (data.hasOwnProperty("status")) {
                        status = data.status;
                    }
                    let headers : any = {};
                    if (data.hasOwnProperty("headers")) {
                        headers = data.headers;
                    }
                    $response.writeHead(status, headers);
                    let body : string = "";
                    let stream : any;
                    const chunked : boolean = $request.headers.hasOwnProperty("transfer-encoding") &&
                        StringUtils.ContainsIgnoreCase($request.headers["transfer-encoding"], "chunked");
                    if (data.hasOwnProperty("body")) {
                        if (ObjectValidator.IsDigit(data.body)) {
                            logResponse(status, headers, "random with length " + Convert.IntegerToSize(data.body), chunked);
                            const crypto : any = require("crypto");
                            body = crypto.randomBytes(data.body).toString("hex");
                            if (chunked) {
                                const Readable : any = require("stream").Readable;
                                stream = new Readable();
                                stream.push(body);
                                stream.push(null);
                            }
                        } else {
                            body = data.body;
                            if (StringUtils.Contains(body, "\\", "/", ":")) {
                                let path : string = body;
                                if (!this.fileSystem.Exists(path)) {
                                    if (!StringUtils.StartsWith(path, "/")) {
                                        path = "/" + path;
                                    }
                                    path = this.properties.projectBase + path;
                                }
                                if (this.fileSystem.Exists(path) && this.fileSystem.IsFile(path)) {
                                    logResponse(status, headers, "from file " + body, chunked);
                                    if (chunked) {
                                        stream = fs.createReadStream(path);
                                    } else {
                                        body = this.fileSystem.Read(path).toString();
                                    }
                                } else {
                                    LogIt.Warning("Configured body file has not been found at: " + path);
                                    logResponse(status, headers, "", chunked);
                                }
                            } else {
                                logResponse(status, headers, body, chunked);
                            }
                        }
                    } else {
                        logResponse(status, headers, body, chunked);
                    }

                    if (!ObjectValidator.IsEmptyOrNull(stream)) {
                        stream.pipe($response);
                    } else {
                        if (ObjectValidator.IsObject(body) || ObjectValidator.IsNativeArray(body)) {
                            body = JSON.stringify(body);
                        }
                        $response.write(body);
                        $response.end();
                    }
                };
                const notFound : any = ($response : any) : void => {
                    logResponse(HttpStatusType.NOT_FOUND, {}, "", false);
                    $response.writeHead(HttpStatusType.NOT_FOUND);
                    $response.end();
                };
                server.on("error", ($error : any) : void => {
                    LogIt.Warning($error);
                });

                server.on("request", ($request : any, $response : any) : void => {
                    try {
                        const message : string = StringUtils.Format("mock request {0}, headers: {1}",
                            JSON.stringify($request.url, null, 2),
                            JSON.stringify($request.headers, null, 2));
                        this.logs.push(message);
                        if (this.verbose) {
                            LogIt.Debug(message);
                        }
                        if (configuration.hasOwnProperty($request.url)) {
                            let mockResponses : any[] = configuration[$request.url];
                            if (!ObjectValidator.IsArray(mockResponses)) {
                                mockResponses = [mockResponses];
                            }
                            let data : any = null;
                            mockResponses.forEach(($data) : void => {
                                if (ObjectValidator.IsEmptyOrNull($data.method)) {
                                    $data.method = HttpMethodType.GET;
                                } else {
                                    $data.method = StringUtils.ToUpperCase($data.method);
                                }
                                if ($request.method === $data.method) {
                                    data = $data;
                                }
                            });
                            if (!ObjectValidator.IsEmptyOrNull(data)) {
                                send(data, $request, $response);
                            } else {
                                notFound($response);
                            }
                        } else {
                            notFound($response);
                        }
                    } catch (e) {
                        LogIt.Warning(e.stack);
                    }
                });

                let port : number = this.project.target.mockServer.port;
                const optionPort : number = StringUtils.ToInteger($option);
                if (ObjectValidator.IsInteger(optionPort)) {
                    port = optionPort;
                    $option = "service";
                }
                if (port > 0) {
                    LogIt.Debug("> using specific port: " + port);
                } else if (port !== 0) {
                    LogIt.Error("Unable to start instance of Mock server: invalid port " + port);
                }
                const checkPort : any = ($callback : ($status : boolean) => void) : void => {
                    if (port === 0) {
                        $callback(true);
                    } else {
                        new Network().IsPortFree(port, $callback);
                    }
                };
                checkPort(($status : boolean) : void => {
                    if ($status) {
                        server.listen(port, () : void => {
                            try {
                                this.port = server.address().port;
                                this.logs = [];
                                LogIt.Info("Mock server has been started at: " + this.getLocation());
                                if ($option !== "service") {
                                    $done();
                                }
                            } catch (ex) {
                                LogIt.Error("Unable to register instance of Mock server.", ex);
                            }
                        });
                    } else {
                        LogIt.Error("Unable to start instance of Mock server: required port " + port + " is already in use");
                    }
                });
            }
        }
    }
}
