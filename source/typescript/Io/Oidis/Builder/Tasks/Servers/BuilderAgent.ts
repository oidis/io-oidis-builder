/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BuilderHubConnector, IBuilderAgent } from "../../Connectors/BuilderHubConnector.js";
import { BuilderServiceManager } from "../../Connectors/BuilderServiceManager.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class BuilderAgent extends BaseTask {

    protected getName() : string {
        return "builder-agent";
    }

    protected process($done : any, $option : string) : void {
        if ($option === "start") {
            BuilderServiceManager.StartTask(this.properties.projectBase, "builder-server:agent",
                ($status : boolean, $message : string) : void => {
                    if ($status) {
                        LogIt.Info($message);
                        $done();
                    } else {
                        LogIt.Error($message);
                    }
                }, "Agent registered");
        } else if ($option === "list") {
            const connector : BuilderHubConnector = new BuilderHubConnector();
            connector.getAgentsList().Then(($list : IBuilderAgent[]) : void => {
                Echo.Printf($list);
                $done();
            });
        } else {
            LogIt.Error("Unsupported builder-agent option: " + $option);
        }
    }
}
