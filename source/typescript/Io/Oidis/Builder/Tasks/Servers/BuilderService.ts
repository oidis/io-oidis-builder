/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BuilderServiceManager } from "../../Connectors/BuilderServiceManager.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class BuilderService extends BaseTask {

    protected getName() : string {
        return "builder-service";
    }

    protected process($done : any, $option : string) : void {
        const asyncHandler : any = ($status : boolean, $message : string) : void => {
            if ($status) {
                LogIt.Info($message);
                $done();
            } else {
                LogIt.Error($message);
            }
        };
        if ($option === "start") {
            BuilderServiceManager.Start(this.properties.projectBase, asyncHandler);
        } else if ($option === "stop") {
            BuilderServiceManager.Stop(this.properties.projectBase, asyncHandler);
        } else if ($option === "stop-all") {
            BuilderServiceManager.Clear(asyncHandler);
        } else {
            asyncHandler(false, "Unsupported service option: " + $option);
        }
    }
}
