/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class SeleniumServerStop extends BaseTask {

    protected getName() : string {
        return "selenium-server-stop";
    }

    protected async processAsync($option : string) : Promise<void> {
        const port : number = this.properties.nodejsPort;
        const description : string = "Selenium server on port " + port;

        let cmd : string = "";
        if (EnvironmentHelper.IsWindows()) {
            cmd = "netstat -a -o -n | find \"0.0.0.0:" + port + "\"";
        } else {
            cmd = "netstat -anp 2> /dev/null | grep 0.0.0.0:" + port + " | egrep -o \"[0-9]+/node\" | cut -d\"/\" -f1";
        }
        const res : IExecuteResult = await this.terminal.ExecuteAsync(cmd, [], null);
        if (res.exitCode === 0) {
            let pid : number = null;
            const stdout : string = res.std[0];
            if (EnvironmentHelper.IsWindows()) {
                pid = parseInt(stdout.substring(stdout.length - 10), 10);
                cmd = "taskkill /F /PID " + pid;
            } else {
                pid = parseInt(stdout, 10);
                cmd = "kill -9 " + pid;
            }
            if (!isNaN(pid)) {
                if ((await this.terminal.ExecuteAsync(cmd, [], null)).exitCode === 0) {
                    LogIt.Info(description + " has been stopped.");
                } else {
                    LogIt.Error("Unable to stop " + description + ".");
                }
            } else {
                LogIt.Info(description + " has not been found.");
            }
        } else {
            if (res.std[0] + res.std[1] !== "") {
                LogIt.Error("Unable to stop " + description + ".");
            } else {
                LogIt.Info(description + " has not been found.");
            }
        }
    }
}
