/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BuilderServiceManager } from "../../Connectors/BuilderServiceManager.js";
import { HttpServer } from "../../HttpProcessor/HttpServer.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class Dashboard extends BaseTask {

    protected getName() : string {
        return "dashboard";
    }

    protected process($done : any, $option : string) : void {
        const asyncHandler : any = ($status : boolean, $message : string) : void => {
            if ($status) {
                LogIt.Info($message);
                $done();
            } else {
                LogIt.Error($message);
            }
        };
        if ($option === "stop") {
            BuilderServiceManager.StopTask(this.properties.projectBase, "dashboard:start", asyncHandler);
        } else if ($option === "start") {
            const server : HttpServer = new HttpServer();
            server.setOnCloseHandler(() : void => {
                Loader.getInstance().Exit();
            });
            server.getEvents().OnStart(() : void => {
                if ($option !== "start") {
                    $done();
                } else {
                    LogIt.Info("Dashboard has been started");
                }
            });
            this.programArgs.AppDataPath(this.properties.binBase);
            server.Start();
        } else if ($option === "open") {
            this.terminal.Open("https://board.localhost.oidis.io/#/Settings/" + this.builderConfig.servicePort, $done);
        } else {
            BuilderServiceManager.StartTask(this.properties.projectBase, "dashboard:start", asyncHandler,
                "Dashboard has been started");
        }
    }
}
