/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { OSType } from "../../Enums/OSType.js";
import { ProductType } from "../../Enums/ProductType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildProductArgs } from "../../Structures/BuildProductArgs.js";
import { BuildCheck } from "../Testing/BuildCheck.js";

export class ReleasePlatform extends BaseTask {

    protected getName() : string {
        return "release-platform";
    }

    protected async processAsync($option : string) : Promise<void> {
        const args : BuildProductArgs = new BuildProductArgs();
        args.Parse($option);
        const buildPath : string = this.properties.projectBase + "/build";
        const targetPath : string = buildPath + "/target";
        const archiveName : string = this.properties.packageName + (
            (args.OS() === OSType.LINUX || args.OS() === OSType.IMX || args.OS() === OSType.ARM ||
                args.OS() === OSType.MAC) ? ".tar.gz" : ".zip");
        const archivePath : string = buildPath + "/" + archiveName;

        let destination : string = buildPath + "/releases";
        if (!ObjectValidator.IsEmptyOrNull(this.build.releaseName)) {
            destination += "/" + this.build.releaseName;
        }
        destination += "/" + $option;

        if (args.Type() === ProductType.INSTALLER) {
            let appName : string = this.project.target.name;
            if (this.project.target.name === this.project.name) {
                appName = this.properties.packageName;
            }
            if (args.OS() === OSType.WIN) {
                appName += ".exe";
            }
            await this.fileSystem.CopyAsync(buildPath + "/" + appName, destination + "/" + appName);
            await this.fileSystem.DeleteAsync(buildPath + "/" + appName);
            await this.fileSystem.DeleteAsync(archivePath);
        } else {
            await this.fileSystem.DeleteAsync(destination + "/target");
            await this.fileSystem.CopyAsync(targetPath, destination + "/target");
            await this.fileSystem.CopyAsync(archivePath, destination + "/" + archiveName);
            if (args.Toolchain() === ToolchainType.PHONEGAP) {
                const appName : string = this.properties.packageName + this.properties.mobileExtensions[<any>args.OS()];
                await this.fileSystem.CopyAsync(buildPath + "/" + appName, destination + "/" + appName);
                await this.fileSystem.DeleteAsync(buildPath + "/" + appName);
                await this.fileSystem.DeleteAsync(archivePath);
            } else if (args.Toolchain() === ToolchainType.NODEJS) {
                let binaryPath : string = buildPath + "/" + this.project.target.name;
                if (args.OS() === OSType.WIN) {
                    binaryPath += ".exe";
                }
                await this.fileSystem.DeleteAsync(binaryPath);
            } else if (args.Type() !== ProductType.PLUGIN &&
                (args.Toolchain() === ToolchainType.ECLIPSE || args.Toolchain() === ToolchainType.IDEA)) {
                await this.fileSystem.DeleteAsync(archivePath);
                await this.fileSystem.DeleteAsync(targetPath);
            } else {
                await this.fileSystem.DeleteAsync(archivePath);
                if (args.Toolchain() !== ToolchainType.NONE) {
                    const backupPath : string = buildPath + "/webtarget";
                    let appTarget : string = targetPath;
                    let webTarget : string = targetPath + "/target";
                    if (args.Toolchain() === ToolchainType.ECLIPSE ||
                        args.Toolchain() === ToolchainType.IDEA) {
                        appTarget += "/target";
                        webTarget += "/target";
                    }
                    await this.fileSystem.CopyAsync(webTarget, backupPath);
                    if (args.Toolchain() !== ToolchainType.ECLIPSE &&
                        args.Toolchain() !== ToolchainType.IDEA) {
                        this.fileSystem.Write(backupPath + "/LICENSE.txt",
                            this.fileSystem.Read(appTarget + "/LICENSE.txt"));
                        this.fileSystem.Write(backupPath + "/SW-Content-Register.txt",
                            this.fileSystem.Read(appTarget + "/SW-Content-Register.txt"));
                    }
                    await this.fileSystem.DeleteAsync(backupPath + "/resource/libs/OidisConnector");
                    await this.fileSystem.DeleteAsync(backupPath + "/wuirunner.config.jsonp");
                    await this.fileSystem.DeleteAsync(targetPath);
                    await this.fileSystem.CopyAsync(backupPath, targetPath);
                    await this.fileSystem.DeleteAsync(backupPath);
                    if (args.Toolchain() === ToolchainType.ECLIPSE ||
                        args.Toolchain() === ToolchainType.IDEA) {
                        const packagePath = destination + "/" + this.properties.packageName;
                        const finalPackageName = this.project.name + "-" + this.project.version.replace(/\./g, "-");
                        const jarPath = destination + "/" + finalPackageName + ".jar";

                        this.fileSystem.Rename(packagePath + ".zip", jarPath);
                        await this.fileSystem.DeleteAsync(destination + "/target");
                        BuildCheck.RegisterCheck(async () : Promise<void> => {
                            if (!this.fileSystem.Exists(destination + "/" + finalPackageName + ".jar") &&
                                !this.fileSystem.Exists(destination + "/" + finalPackageName + ".zip")) {
                                LogIt.Error("Release packaged not found.");
                            }
                        });
                        if (args.Toolchain() === ToolchainType.IDEA) {
                            await this.fileSystem.CopyAsync(
                                this.properties.projectBase + "/build_cache/WuiIdeaRE/lib",
                                destination + "/" + finalPackageName + "/lib");
                            await this.fileSystem.CopyAsync(jarPath,
                                destination + "/" + finalPackageName + "/lib/" + finalPackageName + ".jar");
                            await this.fileSystem.DeleteAsync(jarPath);
                            await this.fileSystem.PackAsync(destination + "/" + finalPackageName,
                                <any>{output: destination + "/" + finalPackageName + ".zip"});
                            await this.fileSystem.DeleteAsync(destination + "/" + finalPackageName);
                        }
                    }
                }
            }
        }
    }
}
