/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { AuthManagerConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { OSType } from "../../Enums/OSType.js";
import { ProductType } from "../../Enums/ProductType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { BuildProductArgs } from "../../Structures/BuildProductArgs.js";
import { BuildExecutor, IReleaseProperties } from "../BuildProcessor/BuildExecutor.js";
import { Deploy } from "./Deploy.js";

export class TargetDeploy extends Deploy {
    private buildTime : string;

    protected getName() : string {
        return "target-deploy";
    }

    protected async processAsync($option : string) : Promise<void> {
        await Deploy.SingInTo(this.server);

        const logPath : string = this.properties.projectBase + "/log/build.log";
        if (this.fileSystem.Exists(logPath)) {
            const regexp : RegExp = />> Build type: (.*?), Build time: (\w+\s\w+\s\d+\s\d{4}\s\d+:\d+:\d+\s\w+\+\d+)/igm;
            // match build time at the beginning of the build.log file
            const match : RegExpExecArray = regexp.exec(this.fileSystem.Read(logPath).toString("utf8", 0, 2048));
            if (match.length === 3) {
                this.buildTime = new Date(match[2]).getTime() + "";

                const executor : BuildExecutor = new BuildExecutor();
                const releases : IReleaseProperties[] = executor.getReleases();
                let platforms : BuildProductArgs[] = [];

                for await (const release of releases) {
                    platforms = executor.getTargetProducts(release.target);
                    for await (const platform of platforms) {
                        if (platform.Type() !== ProductType.INSTALLER) {
                            if (platforms.length > 1) {
                                await this.uploadPackage(release.name, platform, false);
                            } else if (platforms.length === 1) {
                                await this.uploadPackage(
                                    release.name,
                                    platform,
                                    this.fileSystem.Expand(this.properties.projectBase + "/build/*.+(zip|tar.gz)").length === 1);
                            }
                        }
                    }
                }
            } else {
                LogIt.Error("Unable to parse build time from \"" + logPath + "\".");
            }
        } else {
            LogIt.Error("Unable to locate \"" + logPath + "\" required for deploy of the package." + (
                this.project.noCompress ? " Package is missing probably due to noCompress option enabled on target project." : ""
            ));
        }
    }

    private async uploadPackage($releaseName : string, $platform : BuildProductArgs, $isSinglePlatform : boolean) : Promise<void> {
        const extension : string = $platform.OS() === OSType.WIN ? "zip" : "tar.gz";
        const platformString = $platform.Value();

        let filePath : string = this.properties.projectBase + "/build";
        if (!$isSinglePlatform && ($releaseName !== "" || platformString !== "")) {
            filePath += "/releases";
            if ($releaseName !== "") {
                filePath += "/" + $releaseName;
            }
            if (platformString !== "") {
                filePath += "/" + platformString;
            }
        }
        filePath += "/" + this.properties.packageName + "." + extension;
        if (this.fileSystem.Exists(filePath)) {
            try {
                LogIt.Info("Uploading file \"" + filePath + "\" to " + this.server.location + " as " + JSON.stringify({
                    name       : this.project.name,
                    releaseName: $releaseName,
                    platform   : $platform,
                    version    : this.project.version,
                    buildTime  : this.buildTime
                }));

                const fileName : string = (new Date()).getTime() + "";
                await this.client.UploadPackage({path: fileName, maxChunkSizeKb: this.project.deploy.chunkSize / 1024}, filePath,
                    ($data : string) : void => {
                        LogIt.Info($data);
                    });
                LogIt.Info("File has been uploaded successfully.");

                if (await this.client.RegisterPackage(
                    this.project.name,
                    $releaseName,
                    platformString,
                    this.project.version,
                    this.buildTime,
                    fileName,
                    extension
                )) {
                    if ($platform.Toolchain() === ToolchainType.NODEJS) {
                        LogIt.Info("Uploading extern methods configuration.");
                        const client : AuthManagerConnector = new AuthManagerConnector(
                            false,
                            this.server.location + "/connector.config.jsonp");
                        client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                            LogIt.Error($eventArgs.Exception().ToString());
                        });
                        try {
                            if (await client.RegisterApp({
                                appName    : this.project.name,
                                releaseName: $releaseName,
                                platform   : platformString,
                                version    : this.project.version,
                                buildTime  : this.buildTime,
                                authMethods: JSON.parse(this.fileSystem.Read(
                                    this.properties.projectBase + "/build/compiled/externApi.json").toString())
                            })) {
                                LogIt.Info("Extern methods configuration uploaded.");
                            } else {
                                LogIt.Error("Registration of extern methods has failed.");
                            }
                        } catch (ex) {
                            LogIt.Error("Extern methods registration failed.", ex);
                        }
                    }
                } else {
                    LogIt.Error("Registration of the package has failed.");
                }
            } catch (ex) {
                LogIt.Error(ex.message);
            }
        } else {
            LogIt.Error("File for upload does not exists: " + filePath);
        }
    }
}
