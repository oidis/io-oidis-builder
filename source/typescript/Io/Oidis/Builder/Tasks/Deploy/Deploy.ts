/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IDownloadResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { AuthManagerConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/AuthManagerConnector.js";
import { IAuthorizedMethods } from "@io-oidis-services/Io/Oidis/Services/Structures/IAuthorizedMethods.js";
import { FileSystemHandler } from "../../Connectors/FileSystemHandler.js";
import { IPackage, IPackageFindOptions, UpdatesManagerConnector } from "../../Connectors/UpdatesManagerConnector.js";
import { DeployServerType } from "../../Enums/DeployServerType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { IDeploySecureServer } from "../../Interfaces/IDeployServers.js";
import { IProjectDeploy } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { IDeployOptions } from "../../Structures/ProgramArgs.js";

export class Deploy extends BaseTask {
    protected server : IDeploySecureServer;
    protected reDeployServer : IDeploySecureServer;
    protected client : UpdatesManagerConnector;

    // oidis deploy:eap|dev|prod
    // - deploy pkg to selected hub as now

    // :eap|dev|prod (default eap) in means destination
    // oidis deploy:eap|dev|prod --action=list --filter="application=,version=,platform="
    // oidis deploy:eap|dev|prod --action=geturl --filter=
    // oidis deploy:eap|dev|prod --action=geturl --id=*
    // oidis deploy:eap|dev|prod --action=getmethods --filter=

    // only if single item will be found, ask for specification otherwise
    // oidis deploy:eap|dev|prod --action=reupload [filter] --upload-to=eap|dev|prod|<url>?

    public static async SingInTo($server : IDeploySecureServer) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull($server.user) && !ObjectValidator.IsEmptyOrNull($server.pass)) {
            const auth : AuthManagerConnector =
                new AuthManagerConnector(false, $server.location + "/connector.config.jsonp");
            if (!await auth.IsAuthenticated()) {
                LogIt.Info("> sing in to deploy server: " + $server.location + " as " + $server.user);
                const token : string = await auth.LogIn($server.user, $server.pass);
                if (ObjectValidator.IsEmptyOrNull(token)) {
                    LogIt.Error("Failed to sign in");
                }
                auth.CurrentToken(token);
            }
        }
    }

    public async Process($option? : string) : Promise<void> {
        this.server = this.resolveDeployServer($option);

        this.client = new UpdatesManagerConnector(false, this.server.location + "/connector.config.jsonp");
        this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            LogIt.Error($eventArgs.Exception().ToString());
        });
        return super.Process($option);
    }

    protected getName() : string {
        return "deploy";
    }

    protected async processAsync($option : string) : Promise<void> {
        if (ObjectValidator.IsEmptyOrNull($option)) {
            $option = DeployServerType.EAP;
        }
        const deployOptions : IDeployOptions = this.programArgs.getOptions().deployOptions;

        if (ObjectValidator.IsEmptyOrNull(deployOptions.action) || deployOptions.action === "deploy") {
            if ($option === DeployServerType.DEV || $option === DeployServerType.EAP || $option === DeployServerType.PROD) {
                await this.runTaskAsync("config-deploy:" + $option, "target-deploy:" + $option);
            }
        } else {
            switch (deployOptions.action) {
            case "list":
                await this.fetchPackageList();
                break;
            case "geturl":
                LogIt.Info("Package download url: " + await this.getUrl());
                break;
            case "redeploy":
                this.reDeployServer = this.resolveReDeployServer(deployOptions.uploadTo);
                await this.redeploy();
                break;
            case "getmethods":
                LogIt.Info("AppMethods: " + JSON.stringify(await this.fetchAuthMethods()));
                break;
            default:
                LogIt.Error("Unsupported deploy action: " + deployOptions.action);
            }
        }
    }

    protected async fetchPackageList() : Promise<IPackage[]> {
        const data = await this.client.FindPackages(this.parseFilter());
        if (ObjectValidator.IsEmptyOrNull(data) || data.size === 0) {
            LogIt.Warning("No packages found");
            return [];
        }

        const pkgs : any[] = data.data.sort(($a, $b) => $b.BuildTime - $a.BuildTime);
        pkgs.forEach(($item) => {
            if (ObjectValidator.IsEmptyOrNull($item.Release)) {
                $item.Release = "";
            }
        });

        LogIt.Info("Found " + data.size + " packages");

        const printPkgs : any[] = JsonUtils.Clone(pkgs);
        for (const item of printPkgs) {
            item.BuildTime = new Date(item.BuildTime);
        }
        // eslint-disable-next-line no-console
        console.table(printPkgs, ["Id", "Name", "Platform", "Version", "BuildTime", "Release"]);
        return pkgs;
    }

    protected async getUrl() : Promise<string> {
        let linkPath : string = await this.client.getDownloadLink(this.parseFilter(), true);
        linkPath = StringUtils.Remove((<any>this.client).options.serverConfigurationSource, "/connector.config.jsonp") + linkPath;
        return linkPath;
    }

    protected async fetchAuthMethods() : Promise<IAuthorizedMethods[]> {
        await Deploy.SingInTo(this.server);
        const packages : IPackage[] = await this.fetchPackageList();
        if (packages.length !== 1) {
            LogIt.Error("None or multiple packages found, application method can not be gathered.");
        }
        const authClient : AuthManagerConnector = new AuthManagerConnector(false, this.server.location + "/connector.config.jsonp");
        return authClient.getAppMethods(<any>{
            appName: packages[0].Name,
            buildTime: packages[0].BuildTime,
            platform: packages[0].Platform,
            version: packages[0].Version
        });
    }

    protected async redeploy() : Promise<void> {
        const packages : IPackage[] = await this.fetchPackageList();
        const link : string = await this.getUrl();
        const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
        const response : IDownloadResult = await fileSystem.DownloadAsync(link);
        if (!fileSystem.Exists(response.bodyOrPath)) {
            LogIt.Error("Package download failed from: " + link);
        }
        if (packages.length !== 1) {
            LogIt.Error("None or multiple packages detected, redeploy failed.");
        }
        // TODO(nxa33894) fetch and download auth methods
        await Deploy.SingInTo(this.server);
        const authClient : AuthManagerConnector = new AuthManagerConnector(false, this.server.location + "/connector.config.jsonp");
        const authMethods : IAuthorizedMethods[] = await authClient.getAppMethods(<any>{
            appName  : packages[0].Name,
            buildTime: packages[0].BuildTime,
            platform : packages[0].Platform,
            version  : packages[0].Version
        });

        await Deploy.SingInTo(this.reDeployServer);
        const reDeployClient : UpdatesManagerConnector = new UpdatesManagerConnector(false,
            this.reDeployServer.location + "/connector.config.jsonp");
        LogIt.Info("Uploading file downloaded file to " + this.reDeployServer.location + " as " + JSON.stringify({
            name       : packages[0].Name,
            releaseName: packages[0].Release,
            platform   : packages[0].Platform,
            version    : packages[0].Version,
            buildTime  : packages[0].BuildTime
        }));
        const fileName : string = (new Date()).getTime() + "";
        await reDeployClient.UploadPackage({path: fileName, maxChunkSizeKb: this.project.deploy.chunkSize / 1024},
            response.bodyOrPath,
            ($data : string) : void => {
                LogIt.Info($data);
            });

        LogIt.Info("File has been uploaded successfully.");
        if (await reDeployClient.RegisterPackage(
            packages[0].Name,
            packages[0].Release,
            packages[0].Platform,
            packages[0].Version,
            packages[0].BuildTime + "",
            packages[0].FileName,
            packages[0].Type
        )) {
            if (StringUtils.Contains(packages[0].Platform, ToolchainType.NODEJS) && !ObjectValidator.IsEmptyOrNull(authMethods)) {
                LogIt.Info("Uploading extern methods configuration.");
                const client : AuthManagerConnector = new AuthManagerConnector(false,
                    this.reDeployServer.location + "/connector.config.jsonp");
                client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                    LogIt.Error($eventArgs.Exception().ToString());
                });
                try {
                    if (await client.RegisterApp({
                        appName    : packages[0].Name,
                        releaseName: packages[0].Release,
                        platform   : packages[0].Platform,
                        version    : packages[0].Version,
                        buildTime  : packages[0].BuildTime,
                        authMethods
                    })) {
                        LogIt.Info("Extern methods configuration uploaded.");
                    } else {
                        LogIt.Error("Registration of extern methods has failed.");
                    }
                } catch (ex) {
                    LogIt.Error("Extern methods registration failed.", ex);
                }
            }
        } else {
            LogIt.Error("Registration of the package has failed.");
        }
    }

    protected resolveDeployServer($option : string) : IDeploySecureServer {
        const deploy : IProjectDeploy = this.project.deploy;
        let retVal : IDeploySecureServer = deploy.server.eap;
        if (!ObjectValidator.IsEmptyOrNull($option)) {
            if (deploy.server.hasOwnProperty($option)) {
                retVal = deploy.server[$option];
            } else {
                LogIt.Error("Unsupported target deploy option \"" + $option + "\".");
            }
        }
        if (ObjectValidator.IsEmptyOrNull(retVal.location)) {
            LogIt.Error("Target deploy failed: unable to find deploy server configuration");
        }
        return retVal;
    }

    private parseFilter($filter? : string) : IPackageFindOptions {
        if (ObjectValidator.IsEmptyOrNull($filter)) {
            $filter = this.programArgs.getOptions().deployOptions.filter;
        }
        const options : IPackageFindOptions = {};
        if (!ObjectValidator.IsEmptyOrNull(this.programArgs.getOptions().deployOptions.id)) {
            options.id = this.programArgs.getOptions().deployOptions.id;
        } else if (!ObjectValidator.IsEmptyOrNull($filter)) {
            for (const item of $filter.split(/[;,]/)) {
                const entries : string[] = item.split("=");
                if (entries.length === 2) {
                    switch (entries[0].toLowerCase()) {
                    case "name":
                    case "application":
                        options.name = entries[1];
                        break;
                    case "release":
                        options.release = entries[1];
                        break;
                    case "version":
                        options.version = entries[1];
                        break;
                    case "platform":
                        options.platform = entries[1];
                        break;
                    case "buildtime":
                        options.buildTime = new Date(entries[1]);
                        break;
                    case "id":
                        options.id = entries[1];
                        break;
                    default:
                        LogIt.Error("Unknown filter option: " + entries[0]);
                    }
                }
            }
        }
        return options;
    }

    private resolveReDeployServer($option : string) : IDeploySecureServer {
        if (ObjectValidator.IsEmptyOrNull($option)) {
            LogIt.Error("ReDeploy server needs to be specified. Use --upload-to=dev|eap|prod|<url> argument.");
        }
        const normOption : string = $option.toLowerCase();
        let retVal : IDeploySecureServer;
        if (normOption === "dev" || normOption === "eap" || normOption === "prod") {
            retVal = this.resolveDeployServer(normOption);
        } else {
            $option = StringUtils.Remove($option, "/connector.config.jsonp");
            retVal = <IDeploySecureServer>{location: $option};
        }
        return retVal;
    }
}
