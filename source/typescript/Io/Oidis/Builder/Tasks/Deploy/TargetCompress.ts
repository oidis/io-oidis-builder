/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { OSType } from "../../Enums/OSType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class TargetCompress extends BaseTask {

    protected getName() : string {
        return "compress";
    }

    protected async processAsync($option : string) : Promise<void> {
        const configs : ICompressConfigs[] = <any>{
            deploy      : {
                type: !ObjectValidator.IsEmptyOrNull(this.build.product) && this.build.product.OS() === OSType.WIN ? "zip" : "tar.gz"
            },
            coverage    : {
                type : "zip",
                files: "build/reports/coverage/typescript/**/*",
                dest : "build/reports/coverage/coverage.zip"
            },
            deployJava  : {
                ignore: "build/target/target/**/*"
            },
            pluginTarget: {
                dest : "build/target/target",
                files: "build/target/target/**/*"
            },
            docs        : {
                dest: "build/" + this.properties.packageName + "[docs]"
            },
            phonegap    : {}
        };
        if ($option === "deploy" && this.project.noCompress) {
            LogIt.Warning("Target compress skipped due to project noCompress config");
        } else {
            if (configs.hasOwnProperty($option)) {
                const config : ICompressConfigs = configs[$option];
                if (ObjectValidator.IsEmptyOrNull(config.dest)) {
                    config.dest = "build/" + this.properties.packageName;
                }
                if (ObjectValidator.IsEmptyOrNull(config.files)) {
                    config.files = "build/target/**/*";
                }
                if (ObjectValidator.IsEmptyOrNull(config.type)) {
                    config.type = "zip";
                }
                if (!ObjectValidator.IsEmptyOrNull(config.ignore)) {
                    await this.fileSystem.PackAsync([
                        this.properties.projectBase + "/" + config.files,
                        "!" + this.properties.projectBase + "/" + config.ignore
                    ], {
                        cwd   : this.properties.projectBase + "/" + StringUtils.Substring(config.files, 0,
                            StringUtils.IndexOf(config.files, "/*", false)),
                        output: this.properties.projectBase + "/" + config.dest + "." + config.type,
                        type  : config.type
                    });
                } else {
                    await this.fileSystem.PackAsync(this.properties.projectBase + "/" + config.files, {
                        output: this.properties.projectBase + "/" + config.dest + "." + config.type,
                        type  : config.type
                    });
                }
            } else {
                LogIt.Error("Target compress failed: unable to find \"" + $option + "\" configuration");
            }
        }
    }
}

export interface ICompressConfigs {
    files : string;
    dest : string;
    type : string;
    ignore : string;
}

// generated-code-start
export const ICompressConfigs = globalThis.RegisterInterface(["files", "dest", "type", "ignore"]);
// generated-code-end
