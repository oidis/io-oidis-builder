/*! ******************************************************************************************************** *
 *
 * Copyright 2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ProgressEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ProgressEventArgs.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileSystemHandlerConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";
import { TerminalConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/TerminalConnector.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { BaseConnector } from "@io-oidis-services/Io/Oidis/Services/Primitives/BaseConnector.js";
import { UpdatesManagerConnector } from "../../Connectors/UpdatesManagerConnector.js";
import { OSType } from "../../Enums/OSType.js";
import { ProductType } from "../../Enums/ProductType.js";
import {
    IProjectDeployUpdate,
    IProjectDeployUpdateAgent,
    IProjectDeployUpdateApp,
    IProjectDeployUpdateLocation
} from "../../Interfaces/IProject.js";
import { ScriptHandler } from "../../IOApi/Hadlers/ScriptHandler.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildProductArgs } from "../../Structures/BuildProductArgs.js";

export class UpdateDeploy extends BaseTask {
    private remoteFileSystem : FileSystemHandlerConnector;
    private remoteTerminal : TerminalConnector;
    private remoteRegister : any;
    private update : IProjectDeployUpdate;
    private agent : IProjectDeployUpdateAgent;

    protected getName() : string {
        return "update-deploy";
    }

    protected process($done : any, $option : string) : void {
        if (ObjectValidator.IsEmptyOrNull($option)) {
            $option = "dev";
        }

        const updates : IProjectDeployUpdate[] = [];
        const skippedUpdates : string[] = [];
        let name : string;
        for (name in this.project.deploy.updates) {
            if (this.project.deploy.updates.hasOwnProperty(name)) {
                const update : IProjectDeployUpdate = this.project.deploy.updates[name];
                if (update.skipped) {
                    skippedUpdates.push(name);
                } else {
                    update.name = name;
                    updates.push(update);
                }
            }
        }
        if (!ObjectValidator.IsEmptyOrNull(skippedUpdates)) {
            LogIt.Warning("Some updates will be skipped: " + skippedUpdates.join(", "));
        }

        const getNextUpdate : any = ($updateIndex : number) : void => {
            if ($updateIndex < updates.length) {
                this.update = updates[$updateIndex];
                LogIt.Info("Started update for: " + this.update.name);

                const product : BuildProductArgs = new BuildProductArgs();
                product.Parse(this.update.location.platform);
                if (ObjectValidator.IsEmptyOrNull(this.update.app)) {
                    this.update.app = {
                        name: this.project.name
                    };
                }
                if (ObjectValidator.IsEmptyOrNull(this.update.app.name)) {
                    this.update.app.name = this.project.target.name;
                }
                if (product.Type() === ProductType.APP &&
                    product.OS() === OSType.WIN && !StringUtils.EndsWith(this.update.app.name, ".exe")) {
                    this.update.app.name += ".exe";
                }
                if (ObjectValidator.IsEmptyOrNull(this.update.app.path)) {
                    if (product.OS() === OSType.WIN) {
                        this.update.app.path = "C:/oidis/apps/" + this.project.name;
                    } else {
                        this.update.app.path = "/var/oidis/apps/" + this.project.name;
                    }
                }
                if (ObjectValidator.IsEmptyOrNull(this.update.register)) {
                    if (product.OS() === OSType.WIN) {
                        this.update.register = "C:/oidis/appdata/io-oidis-updater/Register.json";
                    } else {
                        this.update.register = "/var/lib/io-oidis-updater/Register.json";
                    }
                }
                if (!ObjectValidator.IsEmptyOrNull(this.update.postDeployScript)) {
                    if (!StringUtils.Contains(this.update.postDeployScript, ":/") &&
                        !StringUtils.StartsWith(this.update.postDeployScript, "/")) {
                        this.update.postDeployScript = this.properties.projectBase + "/" + this.update.postDeployScript;
                    }
                }
                const getNextAgent : any = ($agentIndex : number) : void => {
                    if ($agentIndex < this.update.agents.length) {
                        this.agent = this.update.agents[$agentIndex];
                        if (ObjectValidator.IsEmptyOrNull(this.agent.hub)) {
                            this.agent.hub = this.project.deploy.agentsHub;
                            if (ObjectValidator.IsEmptyOrNull(this.agent.hub)) {
                                this.agent.hub = this.builderConfig.deployAgentsHub;
                            }
                        }
                        if (this.agent.skipped ||
                            !ObjectValidator.IsEmptyOrNull(this.agent.profile) && this.agent.profile !== $option) {
                            LogIt.Warning("Update push skipped for: " + this.agent.name + " [" + this.agent.hub + "]");
                            getNextAgent($agentIndex + 1);
                        } else {
                            LogIt.Info("Push update to: " + this.agent.name + " [" + this.agent.hub + "]");
                            this.remoteFileSystem = new FileSystemHandlerConnector(3, this.agent.hub,
                                WebServiceClientType.FORWARD_CLIENT);
                            this.remoteTerminal = new TerminalConnector(3, this.agent.hub,
                                WebServiceClientType.FORWARD_CLIENT);
                            [this.remoteFileSystem, this.remoteTerminal].forEach(($connector : BaseConnector) : void => {
                                $connector.setAgent(this.agent.name, this.agent.hub);
                                $connector.ErrorPropagation(false);
                                $connector.getEvents().OnError(() : void => {
                                    getNextAgent($agentIndex + 1);
                                });
                            });
                            this.remoteRegister = {};
                            this.remoteFileSystem.Read(this.update.register).Then(($data : any) : void => {
                                if (!ObjectValidator.IsEmptyOrNull($data)) {
                                    this.remoteRegister = $data;
                                }
                                const onUpdateComplete : any = () : void => {
                                    this.remoteFileSystem
                                        .Write(this.update.register, JSON.stringify(this.remoteRegister))
                                        .Then(($success : boolean) : void => {
                                            if (!$success) {
                                                LogIt.Warning("> failed to save updater register at: " + this.update.register);
                                            }
                                            getNextAgent($agentIndex + 1);
                                        });
                                };
                                const updateLocation : IProjectDeployUpdateLocation = this.update.location;
                                if (ObjectValidator.IsEmptyOrNull(updateLocation.projectName)) {
                                    updateLocation.projectName = this.project.name;
                                }
                                if (ObjectValidator.IsEmptyOrNull(updateLocation.path)) {
                                    updateLocation.path = this.project.deploy.server;
                                }
                                if (ObjectValidator.IsEmptyOrNull(updateLocation.releaseName)) {
                                    updateLocation.releaseName = "null";
                                }
                                if (ObjectValidator.IsEmptyOrNull(updateLocation.platform)) {
                                    updateLocation.platform = "null";
                                }
                                if (ObjectValidator.IsEmptyOrNull(updateLocation.version) || updateLocation.version === "latest") {
                                    updateLocation.version = "";
                                }
                                if (ObjectValidator.IsEmptyOrNull(this.remoteRegister[this.agent.name]) ||
                                    this.programArgs.IsForce()) {
                                    this.remoteRegister[this.agent.name] = {
                                        platform   : updateLocation.platform,
                                        projectName: updateLocation.projectName,
                                        releaseName: updateLocation.releaseName
                                    };
                                    this.downloadPackage($option, onUpdateComplete);
                                } else {
                                    const updaterRegister : any = this.remoteRegister[this.agent.name];
                                    const updateManager : UpdatesManagerConnector = new UpdatesManagerConnector(3,
                                        updateLocation.path[$option].location + "/connector.config.jsonp");
                                    updateManager.ErrorPropagation(false);
                                    updateManager.getEvents().OnError(() : void => {
                                        getNextAgent($agentIndex + 1);
                                    });
                                    updateManager
                                        .UpdateExists(
                                            updateLocation.projectName,
                                            updateLocation.releaseName,
                                            updateLocation.platform,
                                            updaterRegister.version,
                                            new Date(updaterRegister.buildTime).getTime(),
                                            !ObjectValidator.IsEmptyOrNull(updaterRegister.version)
                                        )
                                        .Then(($success : boolean) : void => {
                                            if (!$success) {
                                                const done : any = ($status : boolean) : void => {
                                                    if ($status) {
                                                        LogIt.Info("> application has been restarted");
                                                    } else {
                                                        LogIt.Warning("> failed to restart application");
                                                    }
                                                    getNextAgent($agentIndex + 1);
                                                };
                                                if (product.Type() === ProductType.APP) {
                                                    this.restartInstance(false, done);
                                                } else if (!ObjectValidator.IsEmptyOrNull(this.update.postDeployScript)) {
                                                    this.executeDeployScript(done);
                                                } else {
                                                    LogIt.Info("> application restart has been skipped for platform: " +
                                                        this.update.location.platform);
                                                    getNextAgent($agentIndex + 1);
                                                }
                                            } else {
                                                this.downloadPackage($option, onUpdateComplete);
                                            }
                                        });
                                }
                            });
                        }
                    } else {
                        getNextUpdate($updateIndex + 1);
                    }
                };
                getNextAgent(0);
            } else {
                $done();
            }
        };
        getNextUpdate(0);
    }

    private downloadPackage($profile : string, $done : () => void) : void {
        let downloadSource : string = this.update.location.path[$profile].location + "/Update/" +
            this.update.location.projectName + "/" + this.update.location.releaseName + "/" + this.update.location.platform;
        if (!ObjectValidator.IsEmptyOrNull(this.update.location.version)) {
            downloadSource += "/" + this.update.location.version;
        }
        let progressCounter : number = 0;
        this.remoteFileSystem
            .Download(downloadSource)
            .OnStart(() : void => {
                LogIt.Info("> started package download from: " + downloadSource);
            })
            .OnChange(($args : ProgressEventArgs) : void => {
                if (progressCounter < 100) {
                    Echo.Print(".");
                } else {
                    Echo.Println(" (" + Math.floor(100 / $args.RangeEnd() * $args.CurrentValue()) + "%)");
                    progressCounter = 0;
                }
                progressCounter++;
            })
            .OnComplete(($tmpPath : string, $headers : ArrayList<string>) : void => {
                LogIt.Info("\n> started unpacking");
                this.remoteRegister[this.agent.name].buildTime = $headers.getItem("last-modified");
                this.remoteRegister[this.agent.name].version = $headers.getItem("content-version");
                if (ObjectValidator.IsEmptyOrNull(this.remoteRegister[this.agent.name].version)) {
                    this.remoteRegister[this.agent.name].version = $headers.getItem("x-content-version");
                }
                const app : IProjectDeployUpdateApp = this.update.app;
                this.remoteFileSystem.Unpack($tmpPath, {
                    output  : app.path + ".new",
                    override: true
                }).Then(() : void => {
                    const finishUpdate : any = () : void => {
                        this.remoteFileSystem.Delete($tmpPath).Then(() : void => {
                            this.remoteFileSystem.Delete(app.path + ".new").Then(() : void => {
                                this.remoteFileSystem.Delete(app.path + ".back").Then(() : void => {
                                    LogIt.Info("> update has been finished");
                                    $done();
                                });
                            });
                        });
                    };
                    const product : BuildProductArgs = new BuildProductArgs();
                    product.Parse(this.update.location.platform);
                    if (product.Type() === ProductType.APP) {
                        this.remoteTerminal.Kill(app.path + "/" + app.name).Then(($success : boolean) : void => {
                            if ($success) {
                                LogIt.Info("> application has been killed");
                            }
                            this.remoteFileSystem.Rename(app.path, app.path + ".back").Then(() : void => {
                                this.remoteFileSystem.Rename(app.path + ".new", app.path).Then(() : void => {
                                    this.restartInstance(true, ($status : boolean) : void => {
                                        if (!$status) {
                                            this.remoteFileSystem.Rename(app.path + ".back", app.path).Then(() : void => {
                                                $done();
                                            });
                                        } else {
                                            finishUpdate();
                                        }
                                    });
                                });
                            });
                        });
                    } else {
                        if (ObjectValidator.IsEmptyOrNull(this.update.postDeployScript)) {
                            LogIt.Info("> application start has been skipped for platform: " + this.update.location.platform);
                        }
                        this.remoteFileSystem.Rename(app.path, app.path + ".back").Then(() : void => {
                            this.remoteFileSystem.Rename(app.path + ".new", app.path).Then(() : void => {
                                if (!ObjectValidator.IsEmptyOrNull(this.update.postDeployScript)) {
                                    this.executeDeployScript(finishUpdate);
                                } else {
                                    finishUpdate();
                                }
                            });
                        });
                    }
                });
            });
    }

    private restartInstance($skipKill : boolean, $done : ($status : boolean) => void) : void {
        const detachDetect : any = setTimeout(() : void => {
            if (!this.programArgs.IsDebug()) {
                $done(true);
            }
        }, 5000);
        const app : IProjectDeployUpdateApp = this.update.app;
        if (ObjectValidator.IsEmptyOrNull(app.args)) {
            app.args = [];
        }
        if (ObjectValidator.IsEmptyOrNull(app.cwd)) {
            app.cwd = app.path;
        }
        const start : any = () : void => {
            if (!ObjectValidator.IsEmptyOrNull(this.update.postDeployScript)) {
                this.executeDeployScript($done);
            } else {
                let cmd : string = app.name;
                if (app.cwd !== app.path) {
                    cmd = app.path + "/" + cmd;
                } else if (!StringUtils.EndsWith(cmd, ".exe")) {
                    cmd = "./" + cmd;
                }
                LogIt.Info(app.cwd + " > " + cmd + " " + app.args.join(" "));
                this.remoteTerminal
                    .Spawn(cmd, app.args, app.cwd)
                    .OnMessage(($data : string) : void => {
                        Echo.Printf($data);
                    })
                    .Then(($exitCode : number) : void => {
                        clearTimeout(detachDetect);
                        $done($exitCode === 0);
                    });
            }
        };
        const restart : any = () : void => {
            if ($skipKill) {
                start();
            } else {
                this.remoteTerminal.Kill(app.path + "/" + app.name).Then(($success : boolean) : void => {
                    if ($success) {
                        LogIt.Info("> application has been killed");
                    }
                    start();
                });
            }
        };

        if (!ObjectValidator.IsEmptyOrNull(app.config)) {
            const config : string = this.fileSystem.Read(this.properties.projectBase + "/" + app.config).toString();
            if (ObjectValidator.IsEmptyOrNull(config)) {
                LogIt.Warning("App config upload skipped: config file has not been found or is empty");
                restart();
            } else {
                this.remoteFileSystem
                    .Write(app.path + "/" + StringUtils.Remove(app.name, ".exe") + ".config.jsonp", config)
                    .Then(() : void => {
                        restart();
                    });
            }
        } else {
            restart();
        }
    }

    private executeDeployScript($done : ($status : boolean) => void) : void {
        if (this.fileSystem.Exists(this.update.postDeployScript)) {
            const handler : ScriptHandler = new ScriptHandler();
            handler.Path(this.update.postDeployScript);
            handler.Environment({
                Process($config : IProjectDeployUpdate, $target : IPostDeployProcessTarget,
                        $done : ($status : boolean) => void) : void {
                    LogIt.Warning("Post-deploy script skipped: override for global Process method has not been found");
                    $done(false);
                }
            });
            handler.SuccessHandler(($status? : boolean) : void => {
                $done(ObjectValidator.IsEmptyOrNull($status) ? true : $status);
            });
            handler.ErrorHandler(($error : Error) : void => {
                LogIt.Error("Post-deploy script has failed.", $error);
            });
            handler.Process(this.update, {
                agent     : this.agent,
                fileSystem: this.remoteFileSystem,
                terminal  : this.remoteTerminal
            });
        } else {
            LogIt.Error("Post-deploy script has not been found at: " + this.update.postDeployScript);
        }
    }
}

export interface IPostDeployProcessTarget {
    agent : IProjectDeployUpdateAgent;
    terminal : TerminalConnector;
    fileSystem : FileSystemHandlerConnector;
}

// generated-code-start
export const IPostDeployProcessTarget = globalThis.RegisterInterface(["agent", "terminal", "fileSystem"]);
// generated-code-end
