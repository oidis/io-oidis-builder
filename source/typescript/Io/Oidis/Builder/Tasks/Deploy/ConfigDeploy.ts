/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileTransferConnector } from "@io-oidis-connector/Io/Oidis/Connector/Connectors/FileTransferConnector.js";
import { IRemoteFileInfo } from "@io-oidis-localhost/Io/Oidis/Localhost/Interfaces/IFileInfo.js";
import stripJsonComments from "@nodejs/strip-json-comments/index.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { StringReplace } from "../Utils/StringReplace.js";
import { Deploy } from "./Deploy.js";

export class ConfigDeploy extends Deploy {
    private transfer : FileTransferConnector;

    protected getName() : string {
        return "config-deploy";
    }

    protected async processAsync($option : string) : Promise<void> {
        await Deploy.SingInTo(this.server);

        if (ObjectValidator.IsEmptyOrNull(this.project.target.hubLocation)) {
            this.project.target.hubLocation = this.server.location;

        }
        this.transfer = new FileTransferConnector(false, this.server.location + "/connector.config.jsonp");
        this.transfer.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
            LogIt.Error($eventArgs.Exception().ToString());
        });

        if (!ObjectValidator.IsEmptyOrNull(this.programArgs.getOptions().file)) {
            await this.processConfig(this.properties.projectBase + "/" + this.programArgs.getOptions().file);
        } else {
            const configs : string[] = this.fileSystem.Expand(this.project.target.configs);
            for await (const config of configs) {
                await this.processConfig(this.properties.projectBase + "/" + config);
            }
        }
    }

    private async processConfig($path : string) : Promise<void> {
        $path = this.fileSystem.NormalizePath($path);
        const info : IConfigFileInfo = this.getConfigInfo($path);
        if (!ObjectValidator.IsEmptyOrNull(info)) {
            try {
                if (info.files.length > 0) {
                    for await (const file of info.files) {
                        LogIt.Info("Uploading resource: " + file.path);
                        await new Promise<void>(($resolve) => {
                            this.transfer.UploadFile({path: (<any>file).name}, file.path)
                                .OnMessage(($data : string) : void => {
                                    LogIt.Debug($data);
                                })
                                .OnError(($data : string) : void => {
                                    LogIt.Error($data);
                                })
                                .Then($resolve);
                        });
                    }
                }
                await this.uploadConfig(this.project.name, $path, info.version, info.data);
            } catch (ex) {
                LogIt.Error(ex.message);
            }
        } else {
            LogIt.Error("Failed to process config data.");
        }
    }

    private getConfigInfo($path : string) : IConfigFileInfo {
        if (!this.fileSystem.Exists($path)) {
            LogIt.Warning("File for upload does not exists: " + $path);
            return null;
        }
        if (StringUtils.EndsWith($path, ".jsonp")) {
            const path : any = require("path");

            let version : string = "1.0.0";
            const files : any[] = [];
            let data : string = this.fileSystem.Read($path).toString();
            if (data.indexOf("({") !== -1 && data.lastIndexOf("});") !== -1) {
                const interfaces : any = /^\s*(\$interface:|"\$interface":)\s*"(.*)"/gm.exec(data);
                let confInterface : string = "";
                if (interfaces !== null && interfaces.length >= 2) {
                    confInterface = interfaces[2];
                }
                if (confInterface !== "IProjectSolution") {
                    data = StringReplace.Content(data, StringReplaceType.VARIABLES);
                }
                const versions : any = /^\s*(version:|"version":)\s*"(\d+\.\d+\.\d+(?:[-.][a-zA-Z]+)*)"/gm.exec(data);
                if (versions !== null && versions.length >= 2) {
                    version = versions[2];
                }
                if (confInterface === "ISelfExtractorConfiguration") {
                    const jsonConfigData : string = data.substring(data.indexOf("({") + 1,
                        data.lastIndexOf("});") + 1);
                    const updatedConfigData : string = jsonConfigData.replace(
                        /(\.\/)?((test\/)?resource\/(([a-zA-Z0-9\-_\/\\\s.]+)+))/g, // eslint-disable-line no-useless-escape
                        ($substring : string) : string => {
                            let filePath = $substring;
                            if (this.fileSystem.Exists(filePath)) {
                                const symbol : string = this.project.name + "_" +
                                    this.project.version.replace(/\./g, "-") + "_" + path.basename(filePath);
                                files.push({
                                    name: symbol,
                                    path: filePath
                                });
                                filePath = this.server.location + "/Download/" + symbol;
                            }
                            return filePath;
                        });

                    data = data.replace(jsonConfigData, stripJsonComments(updatedConfigData));
                }
            }
            return {
                data,
                files,
                version
            };
        } else {
            LogIt.Warning("Only files with valid JSONP format are supported");
            return null;
        }
    }

    private async uploadConfig($name : string, $path : string, $version : string, $data : string) : Promise<void> {
        const path : any = require("path");
        if (await this.client.UploadConfiguration($name, path.basename($path), $version, $data)) {
            LogIt.Info("Uploaded file \"" + $path + "\" to " + this.server.location);
        } else {
            LogIt.Error("Failed to upload \"" + $path + "\" to " + this.server.location);
        }
    }
}

export interface IConfigFileInfo {
    version : string;
    files : IRemoteFileInfo[];
    data : any;
}

// generated-code-start
export const IConfigFileInfo = globalThis.RegisterInterface(["version", "files", "data"]);
// generated-code-end
