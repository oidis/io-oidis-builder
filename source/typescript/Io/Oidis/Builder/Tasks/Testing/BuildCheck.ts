/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class BuildCheck extends BaseTask {

    public static RegisterCheck($rule : ($option? : string) => Promise<void>) : void {
        Loader.getInstance().getAppProperties().buildCheckRegister.push($rule);
    }

    protected getName() : string {
        return "build-check";
    }

    protected async processAsync($option : string) : Promise<void> {
        for await (const check of this.properties.buildCheckRegister) {
            await check($option);
        }
        LogIt.Info(">>"[ColorType.YELLOW] + " " + this.properties.buildCheckRegister.length + " rules have passed.");
    }
}
