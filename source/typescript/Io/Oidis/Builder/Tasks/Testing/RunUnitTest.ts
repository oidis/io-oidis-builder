/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { GeneralTaskType } from "../../Enums/GeneralTaskType.js";
import { ProductType } from "../../Enums/ProductType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildProductArgs } from "../../Structures/BuildProductArgs.js";
import { BuildExecutor } from "../BuildProcessor/BuildExecutor.js";
import { MavenEnv } from "../Java/MavenEnv.js";
import { PythonTest } from "../Python/PythonTest.js";
import { TypeScriptCompile } from "../TypeScript/TypeScriptCompile.js";
import { XCppTest } from "../XCpp/XCppTest.js";

export class RunUnitTest extends BaseTask {
    protected taskType : string;

    constructor() {
        super();
        this.taskType = "unit";
    }

    public getDependenciesTree($option? : string) : string[] {
        const skipBuild : boolean = $option === "skipbuild";
        let chain : string[] = [];
        if (!skipBuild) {
            chain.push("product-settings:0");
        }
        chain = chain.concat(this.getUnitTestChain(skipBuild));
        if (this.programArgs.IsAgentTask()) {
            chain.push("cloud-manager:push");
        }
        return chain;
    }

    protected getName() : string {
        return "run-unit-test";
    }

    protected getInitCompileTasks($isCpp : boolean) : string[] {
        const tasks : string[] = [
            "install:dev",
            "mock-server",
            "builder-server:start",
            GeneralTaskType.VAR_REPLACE
        ];
        if ($isCpp) {
            tasks.push("string-replace:xcpp");
        }
        tasks.push(
            "copy-staticfiles:targetresource",
            "copy-staticfiles:resource",
            "copy-local-scripts:schema");
        return tasks;
    }

    protected getTypeScriptCompileTasks() : string[] {
        const tasks : string[] = [];
        if (this.properties.projectHas.ESModules()) {
            TypeScriptCompile.getConfig().cache.options.removeComments = false;
            tasks.push(
                "cache-manager:generate-prod-ts",
                "cache-manager:prepare-ts"
            );
        }
        tasks.push(
            "typescript-interfaces:test",
            "typescript-mappings",
            "typescript-reference:test",
            "typescript:unit"
        );
        if (this.properties.projectHas.ESModules()) {
            tasks.push(
                "cache-manager:finalize-ts-nobundle",
                "copy-local-scripts:declaration"
            );
        }
        if (this.build.product.Type() === ProductType.SHARED) {
            tasks.push("nodejs-packaging:shared");
        }
        return tasks;
    }

    protected getCppCompileTasks() : string[] {
        return [
            "xcpp-interfaces:source",
            "xcpp-reference:source",
            "xcpp-compile:" + this.taskType,
            "copy-staticfiles:targetresource-postbuild"
        ];
    }

    protected getSingleTestFilter($fileName : string) : void {
        const path : any = require("path");
        $fileName = this.fileSystem.NormalizePath($fileName);
        if (StringUtils.StartsWith($fileName, "/")) {
            $fileName = StringUtils.Substring($fileName, 1);
        }
        const ext : string = path.extname($fileName);

        switch (ext) {
        case ".ts":
        case ".tsx":
            const config : any = TypeScriptCompile.getConfig(); // eslint-disable-line no-case-declarations
            if (this.properties.projectHas.ESModules()) {
                config.unitModule.src = [
                    "test/" + $fileName,
                    "!**/*.d.ts"
                ];
                LogIt.Info("Run typescript " + this.taskType + " tests from source: " + config.unitModule.src);
            } else {
                config.unit.src = [
                    this.properties.sources + "/*.{ts,tsx}", this.properties.dependencies + "/*.{ts,tsx}",
                    "test/" + $fileName,
                    "bin/resource/scripts/UnitTestRunner.ts",
                    "!**/*.d.ts"
                ];
                LogIt.Info("Run typescript " + this.taskType + " tests from source: " + config.unit.src);
            }
            break;

        case ".cpp":
            XCppTest.getConfig().filter = $fileName.replace(".cpp", ".*");
            LogIt.Info("Run C++ " + this.taskType + " tests from source: " + $fileName);
            break;

        case ".java":
            MavenEnv.getConfig().unit.src = [
                $fileName.replace(".java", "").replace(/\//g, ".")
            ];
            LogIt.Info("Run Java " + this.taskType + " tests from source: " + $fileName);
            break;

        case ".py":
            PythonTest.getConfig().unit.filter = $fileName;
            LogIt.Info("Run Python " + this.taskType + " tests from source: " + $fileName);
            break;

        default:
            LogIt.Error("Unsupported single " + this.taskType + " test file \"" + ext + "\" extension. " +
                "Supported only \"*.ts\", \"*.cpp\" or \"*.java\"");
            break;
        }
    }

    protected getMultiTestFiler() : void {
        LogIt.Info("Run all " + this.taskType + " tests from source: test/**/*");

        if (this.properties.projectHas.TypeScript.Tests()) {
            if (!this.properties.projectHas.ESModules() && this.programArgs.getOptions().withDependencies) {
                const config : any = TypeScriptCompile.getConfig().unit;
                config.src = [
                    this.properties.sources + "/*.{ts,tsx}", this.properties.dependencies + "/*.{ts,tsx}",
                    "test/unit/**/*.{ts,tsx}", "dependencies/*/test/unit/**/*.{ts,tsx}",
                    "bin/resource/scripts/UnitTestRunner.ts",
                    "!**/*.d.ts"
                ];
                LogIt.Info("Typescript " + this.taskType + " tests will include tests from dependencies.");
            }
        }
        if (this.properties.projectHas.Cpp.Tests()) {
            XCppTest.getConfig().filter = "*";
        }
    }

    protected getUnitTestChain($skipBuild : boolean) : string[] {
        let compileTasks : string[] = this.getInitCompileTasks(this.properties.projectHas.Cpp.Tests());
        const testTasks : string[] = [];
        if (this.properties.projectHas.TypeScript.Tests()) {
            compileTasks = compileTasks.concat(this.getTypeScriptCompileTasks());
            testTasks.push("typescript-test:" + this.taskType);
        }
        if (this.properties.projectHas.Cpp.Tests()) {
            compileTasks = compileTasks.concat(this.getCppCompileTasks());
            testTasks.push("test:xcpp" + this.taskType);
        }
        if (this.properties.projectHas.Java.Tests()) {
            testTasks.push(
                "maven-env:init",
                "maven-build:" + this.taskType,
                "maven-env:clean");
        }
        if (this.properties.projectHas.Python.Tests()) {
            testTasks.push("python-test:" + this.taskType);
        }

        if (!$skipBuild) {
            return compileTasks.concat(testTasks);
        } else {
            return [
                "install:dev",
                "mock-server",
                "builder-server:start"
            ].concat(testTasks);
        }
    }

    protected async processAsync($option : string) : Promise<void> {
        this.build.isTest = true;
        const executor : BuildExecutor = new BuildExecutor();
        const products : BuildProductArgs[] = executor.getTargetProducts(this.project.target);
        if (products.length > 1) {
            LogIt.Error("Project contains multiple platforms: " + JSON.stringify(products));
        }
        this.build.product = products[0];

        const testFile : string = this.programArgs.getOptions().file;
        if (!ObjectValidator.IsEmptyOrNull(testFile)) {
            this.getSingleTestFilter(testFile);
        } else {
            this.getMultiTestFiler();
        }
        await this.runTaskAsync(this.getDependenciesTree($option));
    }
}
