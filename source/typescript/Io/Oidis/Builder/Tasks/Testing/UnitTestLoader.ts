/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { EnvironmentHelper, nodejsRoot } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import * as chai from "@nodejs/chai/chai.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { VirtualBoxManager } from "../../Utils/VirtualBoxManager.js";
import { MockServer } from "../Servers/MockServer.js";

class ActiveXObject {
    constructor($name : string) {
        // default constructor without implementation
    }
}

declare const __unitTestRunner : any;

export class UnitTestLoader extends BaseTask {
    private suites : any[];

    protected getName() : string {
        return "unit-test-loader";
    }

    protected process($done : any, $option : string) : void {
        const os : any = require("os");
        const vm : any = require("vm");
        const path : any = require("path");
        const instrument : any = require("istanbul-lib-instrument");
        const libCoverage : any = require("istanbul-lib-coverage");
        const sourceMapSupport : any = require("source-map-support");
        const isCIRun : boolean = !!process.env.OIDIS_CI_RUN;
        const noTestFail : boolean = !!process.env.OIDIS_NO_TEST_FAIL;

        let sourceFile : string = this.properties.projectBase + "/build/compiled/test";
        if ($option === "selenium") {
            sourceFile += "/selenium";
        } else {
            sourceFile += "/unit";
        }
        sourceFile += "/typescript/source.js";

        this.suites = [];
        const stdOut : string[] = [];
        const reportNums : any = {
            failing: 0,
            passing: 0,
            skipped: 0,
            total  : 0
        };
        const errors : string[] = [];
        const coverageData : any[] = [];
        let coverageMap : any = null;
        const escapeXmlData : any = ($value : string) : string => {
            $value = StringUtils.Replace($value, this.properties.projectBase + "/", "");
            $value = StringUtils.Replace($value, "&", "&amp;");
            $value = StringUtils.Replace($value, "'", "&apos;");
            $value = StringUtils.Replace($value, "<", "&lt;");
            $value = StringUtils.Replace($value, ">", "&gt;");
            $value = StringUtils.Replace($value, "\"", "&quot;");
            return $value;
        };
        const generateReport : any = ($callback : () => void) : void => {
            let junitContent : string = "";
            let totalTime : number = 0;
            for (const suite in this.suites) {
                if (this.suites.hasOwnProperty(suite)) {
                    const suiteData : any = this.suites[suite];
                    let suiteContent : string = "";
                    let time : number = 0;
                    suiteData.failing.forEach(($testCase : any) : void => {
                        const parts : string[] = $testCase.name.split(" - ");
                        time += $testCase.time;
                        suiteContent += `
        <testcase classname="${parts[0]}" name="${parts[1]}" time="${$testCase.time / 1000}">`;
                        suiteData.errors.forEach(($error : any) : void => {
                            if ($error.name === $testCase.name) {
                                suiteContent += `
            <failure message="${escapeXmlData(StringUtils.Remove($error.error.message, "\n", "\r"))}" type="failure">
                ${escapeXmlData($error.error.stack)}
            </failure>`;
                            }
                        });
                        suiteContent += `
        </testcase>`;
                    });
                    suiteData.passing.forEach(($testCase : any) : void => {
                        const parts : string[] = $testCase.name.split(" - ");
                        time += $testCase.time;
                        suiteContent += `
        <testcase classname="${parts[0]}" name="${parts[1]}" time="${$testCase.time / 1000}"/>`;
                    });
                    suiteData.skipped.forEach(($testCase : any) : void => {
                        const parts : string[] = $testCase.name.split(" - ");
                        suiteContent += `
        <testcase classname="${parts[0]}" name="${parts[1]}" time="0">
            <skipped />
        </testcase>`;
                    });
                    totalTime += time;
                    const tests : number = suiteData.failing.length + suiteData.passing.length + suiteData.skipped.length;
                    const errorsCount : number = suiteData.errors.length < suiteData.failing.length ?
                        suiteData.failing.length : suiteData.errors.length;
                    junitContent += `
    <testsuite name="${StringUtils.Replace(suite, ".js", ".ts")}" errors="${errorsCount}" failures="${suiteData.failing.length}" skipped="${suiteData.skipped.length}" timestamp="${suiteData.timestamp}" time="${time / 1000}" tests="${tests}">${suiteContent}
    </testsuite>`;
                }
            }
            junitContent =
                `<?xml version="1.0" encoding="UTF-8"?>
<testsuites name="jest tests" tests="${reportNums.total}" failures="${reportNums.failing}" errors="${errors.length}" time="${totalTime / 1000}">${junitContent}
</testsuites>`;
            this.fileSystem.Write(this.properties.projectBase + "/build/reports/unit/typescript/junit.xml", junitContent);
            stdOut.push(MockServer.getInstance().getLogs().join(os.EOL));
            if (!ObjectValidator.IsEmptyOrNull(stdOut) && !isCIRun) {
                LogIt.Info(os.EOL +
                    "=====================================================" + os.EOL +
                    "              SANDBOX console printout               "[ColorType.YELLOW] + os.EOL +
                    "=====================================================");
                LogIt.Info(stdOut.join("") + os.EOL);
                LogIt.Info("" +
                    "=====================================================" + os.EOL +
                    "            SANDBOX console printout end             "[ColorType.YELLOW] + os.EOL +
                    "=====================================================");
            } else {
                LogIt.Info(os.EOL +
                    "SANDBOX console printout skipped due to detected OIDIS_CI_RUN process environment variable."[ColorType.YELLOW]);
            }
            errors.map(($entry) => StringUtils.Replace($entry, this.properties.projectBase + "/", "")).forEach(($message) : void => {
                LogIt.Info($message[ColorType.RED]);
            });
            let report : string = reportNums.failing === 0 ? "SUCCESS" : "FAILURES!";
            report += os.EOL;
            if (reportNums.skipped > 0) {
                report += "Skipped tests: " + reportNums.skipped + os.EOL;
            }
            report += "Tests: " + reportNums.total + ", Failures: " + reportNums.failing;
            report = os.EOL + report;
            if (reportNums.failing > 0) {
                LogIt.Info(report[ColorType.RED]);
            } else if (reportNums.skipped > 0) {
                LogIt.Info(report[ColorType.YELLOW]);
            } else {
                LogIt.Info(report[ColorType.GREEN]);
            }

            if ($option === "coverage") {
                const buildPath : string = this.properties.projectBase + "/build/reports/coverage/typescript";
                this.fileSystem.Delete(buildPath);
                this.fileSystem.CreateDirectory(buildPath);
                if (!this.properties.projectHas.ESModules()) {
                    const dataPath : string = StringUtils.Replace(sourceFile, "/", path.sep);
                    const data : any = {
                        _coverageSchema: "",
                        b              : {},
                        branchMap      : {},
                        f              : {},
                        fnMap          : {},
                        hash           : "",
                        path           : dataPath,
                        s              : {},
                        statementMap   : {}
                    };
                    let statementMapIndex : number = 0;
                    let branchMapIndex : number = 0;
                    let fnMapIndex : number = 0;
                    coverageData.forEach(($coverageData : any) : void => {
                        data._coverageSchema = $coverageData._coverageSchema;
                        data.hash = $coverageData.hash;
                        let item : string;
                        for (item in $coverageData.statementMap) {
                            if ($coverageData.statementMap.hasOwnProperty(item)) {
                                data.statementMap[statementMapIndex + ""] = $coverageData.statementMap[item];
                                data.s[statementMapIndex + ""] = $coverageData.s[item];
                                statementMapIndex++;
                            }
                        }
                        for (item in $coverageData.fnMap) {
                            if ($coverageData.fnMap.hasOwnProperty(item)) {
                                data.fnMap[fnMapIndex + ""] = $coverageData.fnMap[item];
                                data.f[fnMapIndex + ""] = $coverageData.f[item];
                                fnMapIndex++;
                            }
                        }
                        for (item in $coverageData.branchMap) {
                            if ($coverageData.branchMap.hasOwnProperty(item)) {
                                data.branchMap[branchMapIndex + ""] = $coverageData.branchMap[item];
                                data.b[branchMapIndex + ""] = $coverageData.b[item];
                                branchMapIndex++;
                            }
                        }
                    });
                    coverageMap = {};
                    coverageMap[dataPath] = data;
                }

                LogIt.Info("> Generating coverage report ...");
                const createReport : any = async () : Promise<void> => {
                    const iLibSourceMaps : any = require("istanbul-lib-source-maps");
                    const libReport : any = require("istanbul-lib-report");
                    const reports : any = require("istanbul-reports");

                    const mapStore : any = iLibSourceMaps.createSourceMapStore();
                    const transformed : any = await mapStore.transformCoverage(coverageMap);

                    const context : any = libReport.createContext({
                        coverageMap      : transformed,
                        defaultSummarizer: "nested",
                        dir              : this.properties.projectBase + "/build/reports/coverage/typescript"
                    });

                    reports.create("html").execute(context);
                    reports.create("clover").execute(context);
                    const cloverPath : string = this.properties.projectBase + "/build/reports/coverage/typescript/clover.xml";
                    const clover : string = this.fileSystem.Read(cloverPath).toString();
                    this.fileSystem.Write(cloverPath, StringUtils.Remove(clover, this.properties.projectBase + "/"));
                    reports.create("teamcity", {file: "../teamcity.echo"}).execute(context);
                };
                createReport()
                    .then(() : void => {
                        $callback();
                    })
                    .catch(($error : any) : void => {
                        LogIt.Error("Coverage report error.", $error);
                    });
            } else {
                $callback();
            }
        };

        if (this.properties.projectHas.ESModules()) {
            try {
                const mockServer : MockServer = MockServer.getInstance();
                mockServer.Verbose(false);
                const mockServerLocation : string = mockServer.getLocation();
                if (ObjectValidator.IsEmptyOrNull(mockServerLocation)) {
                    LogIt.Error("Unable to find instance of Mock server.");
                }
                /// TODO: enable external config or include it somehow into tsconfig
                const syntheticPatterns : string[] = ["node:*", "crypto"];

                const moduleMap : any = new Map();
                const linker : any = async ($specifier : string, $parent : any) : Promise<any> => {
                    let absolutePath : string;
                    let module : any;
                    if (StringUtils.PatternMatched(syntheticPatterns, $specifier)) {
                        absolutePath = $specifier;
                        if (moduleMap.has(absolutePath)) {
                            return moduleMap.get(absolutePath);
                        }
                        const imported : any = await import($specifier);
                        const exportNames : string[] = Object.keys(imported);
                        module = new vm.SyntheticModule(
                            exportNames,
                            () : void => {
                                exportNames.forEach(($key : string) => module.setExport($key, imported[$key]));
                            },
                            {identifier: $specifier, context: $parent.context}
                        );
                    } else {
                        absolutePath = path.normalize(path.join(path.dirname($parent.identifier), $specifier));
                        // LogIt.Info("link: " + modPath);
                        if (moduleMap.has(absolutePath)) {
                            return moduleMap.get(absolutePath);
                        }
                        if (!this.fileSystem.Exists(absolutePath)) {
                            throw new Error("> failed to load module from: " + absolutePath);
                        }
                        module = new vm.SourceTextModule(this.fileSystem.Read(absolutePath).toString(), {
                            context                : $parent.context,
                            identifier             : absolutePath,
                            importModuleDynamically: ($specifier : string, $module : any) => {
                                return $module;
                            }
                        });
                    }

                    moduleMap.set(absolutePath, module);
                    /// TODO: is createCachedData somehow relevant?
                    // const cachedData = module.createCachedData();
                    return module;
                };
                const sourceRoot : string = this.properties.projectBase + "/build/target/resource/esmodules-" + this.build.timestamp;
                const dependencies : string[] = [
                    "Io/Oidis/Commons/Utils/ReflectionEmitter.js",
                    "Io/Oidis/Commons/Interfaces/Interface.js"
                ];
                const loadTests : any = async ($path : string) : Promise<void> => {
                    const sandbox : any = this.createSandbox({
                        generateReport,
                        done: $done,
                        reportNums,
                        errors,
                        type: $option,
                        stdOut,
                        coverageData
                    }, StringUtils.Substring($path, StringUtils.IndexOf($path, "/test/unit/", false) + 11));
                    sandbox.__unitTestRunner.mockServerLocation = mockServerLocation;
                    const context : any = vm.createContext(sandbox);

                    moduleMap.clear();

                    for await (const dependency of dependencies) {
                        const files : string[] = this.fileSystem.Expand(sourceRoot + "/" + dependency);
                        if (files.length === 0) {
                            LogIt.Error("Dependency not found for: " + dependency);
                        } else if (files.length > 1) {
                            LogIt.Error("Found more than one dependency: " + dependency);
                        }
                        try {
                            const depModule : any = await linker(path.basename(files[0]), {identifier: files[0], context});
                            await depModule.link(linker);
                            await depModule.evaluate();
                        } catch (ex) {
                            LogIt.Error("Unit sandbox dependencies injection error.", ex);
                        }
                    }
                    const module : any = await linker(path.basename($path), {identifier: $path, context});
                    try {
                        LogIt.Debug("> linking modules ... ");
                        await module.link(linker);
                    } catch (ex) {
                        LogIt.Error("Unit sandbox link error.", ex);
                    }
                    try {
                        LogIt.Debug("> evaluating tests ... ");
                        await module.evaluate();
                        if (ObjectValidator.IsEmptyOrNull(sandbox.__unitTestRunner.process)) {
                            LogIt.Error("Missing __unitTestRunner.process() implementation");
                        } else {
                            try {
                                await sandbox.__unitTestRunner.process();
                            } catch (ex) {
                                LogIt.Error("__unitTestRunner.process execution error.", ex);
                            }
                            if (ObjectValidator.IsEmptyOrNull(coverageMap)) {
                                coverageMap = libCoverage.createCoverageMap(sandbox.__coverage__);
                            } else {
                                coverageMap.merge(sandbox.__coverage__);
                            }
                        }
                    } catch (ex) {
                        /// TODO: maybe import of UnitTestRunner can be detected directly by parsing of test file
                        //        if this catch is not accurate enough
                        LogIt.Warning("> WARNING: unit sandbox evaluate error can be caused by incorrect imports order!\n" +
                            "Please validate that UnitTestRunner or its child is imported as first in failing Test module (file).\n" +
                            "TIP: Automatic cleanup of imports order can be overcome by consecutive blank lines between imports.\n");
                        LogIt.Error("Unit sandbox evaluate error.", ex);
                    }
                };
                const instrumentFile : any = ($inst : any, $path : string) : Promise<void> => {
                    return new Promise(($resolve : any, $reject : any) : void => {
                        $inst.instrument(this.fileSystem.Read($path).toString("utf8"), $path,
                            async ($error : Error, $code : string) : Promise<void> => {
                                if ($error === null) {
                                    $code += `
//# sourceMappingURL=${path.basename($path)}.map`;
                                    $code = StringUtils.Replace($code,
                                        `var global = new Function("return this")();`, `var global = globalThis;`);
                                    this.fileSystem.Write($path, $code);
                                    $resolve();
                                } else {
                                    $reject($error);
                                }
                            }, JSON.parse(this.fileSystem.Read($path + ".map").toString()));
                    });
                };

                const dependenciesMap : string[] = [];
                this.fileSystem.Expand(this.properties.projectBase + "/build/compiled/dependencies/**/source/typescript/**/*.js")
                    .forEach(($file : string) : void => {
                        dependenciesMap.push(StringUtils.Split($file, "/source/typescript/")[1]);
                    });
                const filterDependencies : boolean = !this.programArgs.getOptions().withDependencies;
                let testName : string = this.programArgs.getOptions().file;
                let allowFilter : boolean = false;
                if (!ObjectValidator.IsEmptyOrNull(testName)) {
                    testName = StringUtils.Replace(testName, "\\", "/");
                    testName = StringUtils.Substring(testName, StringUtils.IndexOf(testName, "/unit/") + 6);
                    testName = StringUtils.Remove(testName, "Test.tsx");
                    testName = StringUtils.Remove(testName, "Test.ts");
                    allowFilter = this.fileSystem.Exists(this.properties.projectBase + "/source/" + testName + ".ts");
                    if (!allowFilter) {
                        allowFilter = this.fileSystem.Exists(this.properties.projectBase + "/source/" + testName + ".tsx");
                    }
                    if (allowFilter) {
                        testName = StringUtils.Remove(testName, "typescript/");
                        LogIt.Debug("Filtering of coverage report for: {0}", testName);
                    } else {
                        LogIt.Warning("Unable to filter coverage report for selected file. Processing all reports instead ...");
                    }
                }
                const findTests : any = async () : Promise<void> => {
                    if ($option === "coverage") {
                        LogIt.Info("> Instrumenting code");
                        const inst : any = instrument.createInstrumenter({
                            compact         : false,
                            esModules       : true,
                            produceSourceMap: true
                        });
                        const sourceFiles : string[] = this.fileSystem.Expand(sourceRoot + "/**/*.js");
                        try {
                            for await (const path of sourceFiles) {
                                if (this.fileSystem.IsFile(path) && !StringUtils.Contains(path, "/test/unit/")) {
                                    let instrument : boolean = true;
                                    if (StringUtils.Contains(path, "/RuntimeTests/", "ForceCompile.js")) {
                                        instrument = false;
                                    } else if (dependenciesMap.includes(StringUtils.Remove(path, sourceRoot + "/"))) {
                                        instrument = !filterDependencies;
                                    } else if (allowFilter) {
                                        instrument = StringUtils.Contains(path, testName + ".js");
                                    }
                                    if (instrument) {
                                        await instrumentFile(inst, path);
                                    }
                                }
                            }
                        } catch (ex) {
                            LogIt.Error("Code instrumentation error.", ex);
                        }
                    }
                    const testFiles : string[] = this.fileSystem.Expand(sourceRoot + "/test/unit/**/*.js");
                    for await (const path of testFiles) {
                        if (this.fileSystem.IsFile(path) && !StringUtils.Contains(path,
                            "/BaseUnitTestRunner.js", "/UnitTestRunner.js", "/UnitTestEnvironment.js")) {
                            if (!isCIRun) {
                                LogIt.Info("> processing path: " + path);
                            }
                            try {
                                await loadTests(path);
                            } catch (ex) {
                                LogIt.Error("Unit sandbox load error.", ex);
                            }
                        }
                    }
                };
                findTests()
                    .then(() : void => {
                        generateReport(() : void => {
                            process.cwd = () : string => {
                                return this.properties.projectBase;
                            };
                            if (reportNums.failing > 0 && !noTestFail) {
                                Loader.getInstance().Exit(-1);
                            } else {
                                if (reportNums.failing > 0 && noTestFail) {
                                    LogIt.Info(os.EOL +
                                        "FAILURES detected! "[ColorType.RED] +
                                        "Continuing due to OIDIS_NO_TEST_FAIL process environment variable..."[ColorType.RED]);
                                }
                                $done();
                            }
                        });
                    })
                    .catch(($ex) : void => {
                        LogIt.Error("Unit sandbox init error.", $ex);
                    });
            } catch (ex) {
                LogIt.Error("Unit sandbox error.", ex);
            }
        } else {
            try {
                const sandbox : any = this.createSandbox({
                    generateReport,
                    done: $done,
                    reportNums,
                    errors,
                    type: $option,
                    stdOut,
                    coverageData
                }, "All");
                vm.createContext(sandbox);
                let content : string = this.fileSystem.Read(sourceFile).toString();
                const contentMap : string = JSON.parse(this.fileSystem.Read(sourceFile + ".map").toString());
                const coverageIds : string[] = [];
                const loadTests : any = () : void => {
                    const unitRootPath : string = this.properties.projectBase + "/build/target";
                    const mockServer : MockServer = MockServer.getInstance();
                    mockServer.Verbose(false);
                    const mockServerLocation : string = mockServer.getLocation();
                    if (ObjectValidator.IsEmptyOrNull(mockServerLocation)) {
                        LogIt.Error("Unable to find instance of Mock server.");
                    }

                    const loader : any = ($imports : any, $namespaces : string[], $unitRootPath : string,
                                          $mockServerLocation : string) : void => {
                        // eslint-disable-next-line prefer-spread
                        $imports.Reflection.setInstanceNamespaces.apply($imports.Reflection, $namespaces);
                        const reflection : Reflection = $imports.Reflection.getInstance();
                        const instances : any[] = [];
                        const nextTest : any = ($index : number) : void => {
                            if ($index < instances.length) {
                                instances[$index].Process(() : void => {
                                    nextTest($index + 1);
                                });
                            } else {
                                __unitTestRunner.done();
                            }
                        };
                        reflection.getAllClasses().forEach(($class : string) : void => {
                            if ($imports.StringUtils.EndsWith($class, "Test")) {
                                const classObject : any = reflection.getClass($class);
                                if (classObject.prototype.prepareAssert !== undefined) {
                                    const instance : any = new classObject();
                                    if (reflection.IsMemberOf(instance, $imports.UnitTestRunner)) {
                                        instance.unitRootPath = $unitRootPath;
                                        instance.mockServerLocation = $mockServerLocation;
                                        instances.push(instance);
                                    }
                                }
                            }
                        });
                        nextTest(0);
                    };
                    content += os.EOL + "(" + Convert.FunctionToString(loader) + ")" +
                        "({" +
                        "Reflection: Io.Oidis.Commons.Utils.Reflection, " +
                        "UnitTestRunner: Io.Oidis.UnitTestRunner, " +
                        "StringUtils: Io.Oidis.Commons.Utils.StringUtils" +
                        "}, " + JSON.stringify(this.project.namespaces) + ", \"" + unitRootPath + "\", " +
                        "\"" + mockServerLocation + "\");";
                    if ($option === "coverage") {
                        content += os.EOL +
                            "getCoverageData = function(){return [" + coverageIds.join(",") + "];}";
                    }

                    const sandboxUrl : string = "file:///" + unitRootPath + "/index.html#UnitTestLoader";
                    sandbox.window.location.href = sandboxUrl;
                    sandbox.jsdom.reconfigure({url: sandboxUrl});

                    sourceMapSupport.install({
                        environment             : "node",
                        handleUncaughtExceptions: false,
                        retrieveSourceMap($source : string) : any {
                            if ($source === sourceFile || StringUtils.Contains($source, "evalmachine")) {
                                return {
                                    map: contentMap,
                                    url: $source + ".map"
                                };
                            }
                            return null;
                        }
                    });

                    LogIt.Debug("> Loading tests ... ");
                    vm.runInContext(content, sandbox, sourceFile);
                };
                if ($option === "coverage") {
                    LogIt.Debug("> Prepare for instrumentation");
                    content = StringUtils.Replace(content, ") || this;", ") || /* istanbul ignore next */ this;");
                    let counter : number = 0;
                    const excludedBranch : string[] = [];
                    const files : string[] = this.fileSystem.Expand([
                        this.properties.projectBase + "/source/typescript/**/*.{ts,tsx}",
                        this.properties.projectBase + "/test/typescript/**/*.{ts,tsx}",
                        this.properties.projectBase + "/dependencies/**/source/typescript/**/*.{ts,tsx}",
                        this.properties.projectBase + "/dependencies/**/test/typescript/**/*.{ts,tsx}"
                    ]);
                    let fileIndex : number = 0;
                    files.forEach(($file : string) : void => {
                        if (counter < 100) {
                            Echo.Print(".");
                        } else {
                            Echo.Println(" (" + Math.floor(100 / files.length * fileIndex) + "%)");
                            counter = 0;
                        }
                        $file = StringUtils.Replace($file, "\\", "/");
                        $file = StringUtils.Replace($file, "//", "/");
                        $file = StringUtils.Substring($file, StringUtils.IndexOf($file, "/typescript/") + 12);
                        const namespaces : string[] = StringUtils.Split($file, "/");
                        let index : number;
                        for (index = 0; index < namespaces.length; index++) {
                            if (excludedBranch.indexOf(namespaces[index]) === -1) {
                                excludedBranch.push(namespaces[index]);
                                if (index === 0) {
                                    content = StringUtils.Replace(content,
                                        "})(" + namespaces[index] + " || (" + namespaces[index] + " = {}));",
                                        "})(" + namespaces[index] +
                                        " || /* istanbul ignore next */ (" + namespaces[index] + " = {}));");
                                } else {
                                    content = StringUtils.Replace(content,
                                        "})(" + namespaces[index] + " = " + namespaces[index - 1] + "." + namespaces[index] +
                                        " || (" + namespaces[index - 1] + "." + namespaces[index] + " = {}));",
                                        "})(" + namespaces[index] + " = " + namespaces[index - 1] + "." + namespaces[index] +
                                        " || /* istanbul ignore next */ (" +
                                        namespaces[index - 1] + "." + namespaces[index] + " = {}));");
                                }
                            }
                        }
                        fileIndex++;
                        counter++;
                    });
                    Echo.Println(" (100%)");

                    LogIt.Debug("> Instrumenting code");
                    const splitContent : string[] = StringUtils.Split(content, os.EOL + "var ");
                    const inst : any = instrument.createInstrumenter({compact: false, esModules: false, produceSourceMap: true});
                    content = "";
                    counter = 0;
                    let linesCount : number = 0;
                    const instrumentNextContent : any = ($index : number) : void => {
                        if ($index < splitContent.length) {
                            if (counter < 100) {
                                Echo.Print(".");
                            } else {
                                Echo.Println(" (" + Math.floor(100 / splitContent.length * $index) + "%)");
                                counter = 0;
                            }
                            let instrumentCode : string = splitContent[$index];
                            if ($index > 0) {
                                instrumentCode = "var " + instrumentCode;
                            }
                            if ($index < splitContent.length - 1) {
                                instrumentCode += os.EOL;
                            }
                            instrumentCode = Array(linesCount + 1).join(os.EOL) + instrumentCode;
                            linesCount += StringUtils.OccurrenceCount(splitContent[$index], os.EOL) + 1;
                            inst.instrument(instrumentCode, "source_" + $index + ".js", ($error : Error, $code : string) : void => {
                                if ($error === null) {
                                    coverageIds.push($code.substr(0, $code.indexOf("\n"))
                                        .replace("var ", "")
                                        .replace("function ", "")
                                        .replace("() {", "")
                                        .trim() + "()");
                                    content += $code + os.EOL;
                                    counter++;
                                    instrumentNextContent($index + 1);
                                } else {
                                    LogIt.Error("Code instrumentation error.", $error);
                                }
                            });
                        } else {
                            Echo.Println(" (100%)");
                            loadTests();
                        }
                    };
                    instrumentNextContent(0);
                } else {
                    loadTests();
                }
            } catch (ex) {
                LogIt.Error("Unit sandbox error.", ex);
            }
        }
    }

    private createSandbox($options : any, $name : string) : any {
        this.suites[$name] = {
            timestamp: new Date().toISOString(),
            passing  : [],
            skipped  : [],
            failing  : [],
            errors   : []
        };
        const fs : any = require("fs");
        const url : any = require("url");
        const mime : any = require("mime");
        const LocalStorage : any = require("node-localstorage").LocalStorage;
        const FileAPI : any = require("file-api");
        const {JSDOM, ResourceLoader, VirtualConsole} = require("jsdom");
        const path : any = require("path");
        const os : any = require("os");
        const vm : any = require("vm");

        let projectCwd : string = this.properties.projectBase + "/build/target";
        process.cwd = () : string => {
            return projectCwd;
        };
        process.chdir(projectCwd);
        const localStorage : any = new LocalStorage(this.properties.projectBase + "/build/compiled/localStorageTemp");

        FileAPI.File = function File($input : string) : void {
            this.path = $input;
            this.name = this.name || path.basename(this.path || "");
            if (!this.name) {
                throw new Error("No name");
            }
            this.type = this.type || mime.getType(this.name);
        };

        let sandbox : any = {};

        const origResourceFetch : any = ResourceLoader.prototype._fetch;
        ResourceLoader.prototype.fetch = function ($url : string, $fetchOptions : any) : any {
            try {
                if (!StringUtils.Contains($url, "http://", "https://")) {
                    if (EnvironmentHelper.IsWindows()) {
                        $url = StringUtils.Remove($url, "file:///");
                    }
                    $url = StringUtils.Remove($url, "file://");
                    if (StringUtils.Contains($url, "?")) {
                        $url = StringUtils.Substring($url, 0, StringUtils.IndexOf($url, "?"));
                    }
                    $url = path.resolve($url);
                    $url = StringUtils.Replace($url, "\\", "/");
                    if (!fs.existsSync($url)) {
                        $options.stdOut.push(os.EOL + ">>"[ColorType.RED] + " Required resource has not been found at: " + $url);
                        return null;
                    }
                    if (!StringUtils.StartsWith($url, "file://")) {
                        $url = "file:///" + $url;
                    }
                }
                return new Promise(($resolve : ($data : any) => void, $reject : ($ex : Error) => void) : void => {
                    origResourceFetch
                        .apply(this, [$url, $fetchOptions])
                        .then(($data : string) : void => {
                            try {
                                vm.runInContext($data, sandbox, $url);
                                $resolve(Buffer.from(""));
                            } catch (ex) {
                                $options.stdOut.push(os.EOL + ">>"[ColorType.RED] + " Failed to execute resource from: " + $url +
                                    os.EOL + ex.stack);
                                $reject(ex);
                            }
                        })
                        .catch(() : void => {
                            $options.stdOut.push(os.EOL + ">>"[ColorType.RED] + " Failed to load resource from: " + $url);
                            $reject(new Error("Failed to load resource from: " + $url));
                        });
                });
            } catch (ex) {
                $options.stdOut.push(os.EOL + ">>"[ColorType.RED] + " Failed to load resource from: " + $url + os.EOL +
                    ex.message);
                return new Promise(($resolve : ($data : any) => void, $reject : ($ex : Error) => void) : void => {
                    $reject(ex);
                });
            }
        };
        const virtualConsole = new VirtualConsole();
        virtualConsole.on("jsdomError", ($error : Error) : void => {
            $options.stdOut.push(os.EOL + ">>"[ColorType.RED] + " " + $error.stack);
        });
        const jsdom : any = new JSDOM("", {
            url                 : "http://builder.oidis.io/#UnitTestLoader",
            contentType         : "text/html",
            userAgent           : "Node.js/" + process.version + " (" + os.platform() + "; rv: " + process.version + ")",
            includeNodeLocations: true,
            resources           : new ResourceLoader({
                strictSSL: false
            }),
            runScripts          : "dangerously",
            virtualConsole
        });
        const sandboxWindow : any = jsdom.window;
        sandboxWindow.scrollTo = () : void => {
            // default handler
        };
        sandboxWindow.WebSocket = WebSocket;
        sandboxWindow.ActiveXObject = ActiveXObject;
        sandboxWindow.localStorage = localStorage;
        sandboxWindow.File = FileAPI.File;
        sandboxWindow.FileList = FileAPI.FileList;
        sandboxWindow.FileReader = FileAPI.FileReader;
        sandboxWindow.Blob = {};
        const createElement : any = sandboxWindow.document.createElement;

        const event = undefined;
        const projectBase : string = this.properties.projectBase;
        let projectNodejsRoot : string = projectBase + "/dependencies/nodejs";
        if (!this.fileSystem.Exists(projectNodejsRoot)) {
            projectNodejsRoot = nodejsRoot;
        }
        const unitRootPath : string = this.properties.projectBase + "/build/target";
        const timeoutIDs : any[] = [];
        const setTimeoutOverride : any = ($handler : any, $timeout? : number, ...$arguments : any[]) : any => {
            const id : any = setTimeout.apply(this, [
                (...$args : any[]) : void => {
                    try {
                        $handler.apply(this, $args);
                    } catch (ex) {
                        if (!StringUtils.Contains(ex.stack, "AssertionError: ", "AssertFailed: ")) {
                            sandbox.console.log(">>"[ColorType.RED] + " Uncatched timeout error detected: " + ex.stack + os.EOL);
                        } else if (!ObjectValidator.IsEmptyOrNull(sandbox.__unitTestRunner.processExist)) {
                            sandbox.__unitTestRunner.processExist();
                            sandbox.__unitTestRunner.processExist = null;
                        }
                    }
                }, $timeout
            ].concat($arguments));
            timeoutIDs.push(id);
            return id;
        };
        sandbox = {
            __unitTestRunner: {
                done() : void {
                    $options.coverageData = $options.coverageData.concat(sandbox.getCoverageData());
                    $options.generateReport(() : void => {
                        process.cwd = () : string => {
                            return projectBase;
                        };
                        $options.done();
                    });
                },
                loaderClass  : this.project.loaderClass,
                namespaces   : this.project.namespaces,
                process      : null,
                processExist : null,
                console,
                setTimeout   : setTimeoutOverride,
                clearTimeout,
                clearTimeouts: () : void => {
                    timeoutIDs.forEach(($id : any) : void => {
                        clearTimeout($id);
                    });
                },
                assert       : chai.assert,
                report       : {
                    pass  : ($testName : string, $time : number) : void => {
                        this.suites[$name].passing.push({name: $testName, time: $time});
                        $options.reportNums.total++;
                        console.log(("+ " + $testName + " (" + $time + "ms)")[ColorType.GREEN]); // eslint-disable-line no-console
                    },
                    fail  : ($testName : string, $time : number, $error : Error) : void => {
                        this.suites[$name].failing.push({name: $testName, time: $time});
                        $options.reportNums.total++;
                        $options.reportNums.failing++;
                        if (ObjectValidator.IsEmptyOrNull($error)) {
                            console.log(("- " + $testName + " (" + $time + "ms)")[ColorType.RED]); // eslint-disable-line no-console
                        } else {
                            console.log(("- " + $testName + " (" + $error + ")")[ColorType.RED]); // eslint-disable-line no-console
                        }
                    },
                    skip  : ($testName : string) : void => {
                        this.suites[$name].skipped.push({name: $testName});
                        $options.reportNums.total++;
                        $options.reportNums.skipped++;
                        console.log(("~ " + $testName)[ColorType.YELLOW]); // eslint-disable-line no-console
                    },
                    assert: ($testName : string, $ex : Error) : void => {
                        let message : string = $ex.stack;
                        if (StringUtils.Contains(message, "AssertionError: ", "AssertFailed: ") &&
                            StringUtils.StartsWith(message, "Error: ")) {
                            message = StringUtils.Substring(message, 7);
                        }
                        this.suites[$name].errors.push({name: $testName, error: $ex});
                        $options.errors.push(
                            os.EOL +
                            $testName + " (ASSERT ERROR DETAILS):" + os.EOL +
                            message + os.EOL);
                    },
                    error : ($ownerName : string, $ex : Error) : void => {
                        this.suites[$name].errors.push({name: $ownerName, error: $ex});
                        $options.errors.push(
                            os.EOL +
                            $ownerName + " (ERROR DETAILS):" + os.EOL +
                            $ex.stack + os.EOL);
                    }
                },
                events       : {
                    onSuiteStart   : () : void => {
                        // default handler
                    },
                    onSuiteEnd     : () : void => {
                        // default handler
                    },
                    onTestCaseStart: ($testName : string) : void => {
                        // default handler
                    },
                    onTestCaseEnd  : ($testName : string) : void => {
                        // default handler
                    },
                    onAssert       : ($status : boolean) : void => {
                        // default handler
                    }
                },
                unitRootPath,
                coverageIds  : [],
                ignoreList   : this.programArgs.getOptions().testIgnore
            },
            jsdom,
            assert          : {},
            isBrowserRun    : false,
            File            : FileAPI.File,
            FileList        : FileAPI.FileList,
            FileReader      : FileAPI.FileReader,
            NodeFile        : FileAPI.File,
            nodeVM          : vm,
            Blob            : sandboxWindow.Blob,
            postMessage     : sandboxWindow.postMessage,
            HTMLElement     : sandboxWindow.HTMLElement,
            HTMLInputElement: sandboxWindow.HTMLInputElement,
            WebSocket,
            ActiveXObject,
            XMLHttpRequest  : sandboxWindow.XMLHttpRequest,
            ColorType,
            TextEncoder,
            TextDecoder,
            event,
            localStorage,
            setTimeout      : setTimeoutOverride,
            setInterval,
            clearTimeout,
            clearInterval,
            console,
            window          : {
                location: {
                    __href: sandboxWindow.location.href
                }
            },
            document        : sandboxWindow.document,
            getCoverageData() : any[] {
                return [];
            },
            require($id) : any {
                try {
                    return require(projectNodejsRoot + "/build/node_modules/" + $id);
                } catch (e) {
                    return require($id);
                }
            },
            module             : {
                exports: {}
            },
            process            : {
                argv: ["--test-run"],
                cwd() : string {
                    return projectCwd;
                },
                exit($code : number) : void {
                    sandbox.console.log(">>"[ColorType.RED] + " ATTEMPTING TO CLOSE Application with exit code: " + $code + os.EOL);
                },
                on($eventName : string, $handler : () => any) : void {
                    sandbox.console.log(">>"[ColorType.YELLOW] + " ATTEMPTING TO Align process event: " + $eventName + os.EOL);
                },
                stdout: {
                    write($message : string) : void {
                        console.log($message); // eslint-disable-line no-console
                    }
                },
                stderr: {
                    write($message : string) : void {
                        console.log($message); // eslint-disable-line no-console
                    }
                },
                env   : process.env,
                chdir($dir : string) : void {
                    sandbox.console.log(">>"[ColorType.RED] + " Changing CWD to \"" + $dir + "\"");
                    projectCwd = $dir;
                    process.chdir($dir);
                },
                kill($pid : number) : void {
                    sandbox.console.log(">>"[ColorType.RED] + " Going to kill process with PID: " + $pid);
                    process.kill($pid);
                }
            },
            global             : {},
            Buffer,
            Error              : sandboxWindow.Error,
            ErrorEvent         : sandboxWindow.ErrorEvent,
            EventTarget        : sandboxWindow.EventTarget,
            __dirname          : projectCwd,
            __filename,
            nodejsRoot         : projectNodejsRoot,
            builder            : Loader.getInstance(),
            JsonpData          : () : void => {
                // default handler
            },
            appExceptionHandler: ($message : string, $stack : Error) : void => {
                LogIt.Error("Unit sandbox error: \n" + (ObjectValidator.IsEmptyOrNull($stack) ? $message : $stack));
            }
        };

        if (!Loader.getInstance().getProgramArgs().IsDebug()) {
            sandbox.console = {
                log($message : string) : void {
                    $options.stdOut.push(os.EOL + $message);
                },
                info($message : string) : void {
                    $options.stdOut.push(os.EOL + $message);
                },
                debug($message : string) : void {
                    $options.stdOut.push(os.EOL + $message);
                },
                warn($message : string) : void {
                    $options.stdOut.push(os.EOL + $message);
                },
                error($message : string) : void {
                    $options.stdOut.push(os.EOL + $message);
                },
                clear() : void {
                    // dummy
                }
            };

            sandbox.process.exit = ($code : number) : void => {
                // skip process exit
            };
            sandbox.process.on = ($eventName : string, $handler : () => any) : void => {
                // skip process event listeners
            };
            sandbox.process.stdout = {
                write($message : string) : void {
                    $options.stdOut.push($message);
                }
            };
            sandbox.process.stderr = {
                write($message : string) : void {
                    $options.stdOut.push($message);
                }
            };
        } else {
            sandbox.process.exit = ($code : number) : void => {
                $options.stdOut.push(
                    os.EOL + ">>"[ColorType.RED] + " ATTEMPTING TO CLOSE Application with exit code: " + $code + os.EOL);
            };
            sandbox.process.on = ($eventName : string, $handler : () => any) : void => {
                $options.stdOut.push(os.EOL + ">>"[ColorType.YELLOW] + " ATTEMPTING TO Align process event: " + $eventName + os.EOL);
            };
        }
        sandbox.process.env.NODE_PATH = projectNodejsRoot + "/build/node_modules";

        this.project.namespaces.forEach(($item : string) : void => {
            const rootPart = $item.split(".")[0];
            if (!sandbox.hasOwnProperty(rootPart)) {
                sandbox[rootPart] = {};
                sandbox.global[rootPart] = sandbox[rootPart];
                sandbox.window[rootPart] = sandbox[rootPart];
            }
        });

        Object.defineProperty(sandbox.window.location, "href", {
            get() : string {
                return this.__href;
            },
            set($value : string) : void {
                this.__href = $value;
                const parsedUrl : any = url.parse($value);
                let property : string;
                for (property in parsedUrl) {
                    if (parsedUrl.hasOwnProperty(property) && property !== "href") {
                        this[property] = parsedUrl[property];
                    }
                }
            }
        });
        let domProperty : string;
        for (domProperty in sandboxWindow) {
            if (sandboxWindow.hasOwnProperty(domProperty) && domProperty !== "location") {
                sandbox.window[domProperty] = sandboxWindow[domProperty];
            }
        }
        for (domProperty in sandboxWindow.location) {
            if (sandboxWindow.location.hasOwnProperty(domProperty) && domProperty !== "href") {
                sandbox.window.location[domProperty] = sandboxWindow.location[domProperty];
            }
        }

        const origSend : any = sandbox.XMLHttpRequest.prototype.send;
        const origGetAllResponseHeaders : any = sandbox.XMLHttpRequest.prototype.getAllResponseHeaders;
        sandbox.XMLHttpRequest.prototype.send = function ($data : string) : void {
            if ($data !== null) {
                origSend.apply(this, [$data]);
            }
        };
        sandbox.XMLHttpRequest.prototype.getAllResponseHeaders = function () : string {
            if (!ObjectValidator.IsEmptyOrNull(sandbox.window.location.headers)) {
                return sandbox.window.location.headers;
            }
            return origGetAllResponseHeaders.apply(this, []);
        };

        sandbox.window.document.createElement = ($name : string) : HTMLElement => {
            let element : any = null;
            try {
                element = createElement.apply(sandbox.window.document, [$name]);
            } catch (ex) {
                LogIt.Debug(ex.stack);
            }

            return element;
        };
        const sandboxUrl : string = "file:///" + unitRootPath + "/index.html#UnitTestLoader";
        sandbox.window.location.href = sandboxUrl;
        sandbox.jsdom.reconfigure({url: sandboxUrl});

        if ($options.type === "selenium") {
            sandbox.isBrowserRun = true;
            let port = "";
            if (this.properties.nodejsPort !== 80) {
                port = ":" + this.properties.nodejsPort;
            }
            sandbox.selenium = require("selenium-webdriver");
            sandbox.chromeBuilder = new sandbox.selenium.Builder()
                .forBrowser("chrome")
                .usingServer("http://127.0.0.1" + port + "/wd/hub");
        }
        sandbox.vbox = VirtualBoxManager.getInstanceSingleton();

        return sandbox;
    }
}
