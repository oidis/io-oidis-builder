/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { RunUnitTest } from "./RunUnitTest.js";

export class RunCoverageTest extends RunUnitTest {

    constructor() {
        super();
        this.taskType = "coverage";
    }

    protected getName() : string {
        return "run-coverage-test";
    }

    protected getSingleTestFilter($fileName : string) : void {
        if (StringUtils.EndsWith($fileName, ".ts") || StringUtils.EndsWith($fileName, ".tsx")) {
            super.getSingleTestFilter($fileName);
        } else {
            LogIt.Error("Single coverage tests are supported only for TypeScript files with \"*.ts\" extension.");
        }
    }
}
