/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseTask } from "../../Primitives/BaseTask.js";

export class TestLintStaggedOnly extends BaseTask {

    protected getName() : string {
        return "test-lint-stagged-only";
    }

    protected async processAsync($option : string) : Promise<void> {
        await this.runTaskAsync("git-manager:backup", "test:tslint", "test:sasslint", "test:jslint", "test:xcpplint",
            "test:javalint", "git-manager:restore");
    }
}
