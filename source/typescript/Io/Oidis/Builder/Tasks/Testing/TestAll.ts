/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import {ColorType} from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { TypeScriptCompile } from "../TypeScript/TypeScriptCompile.js";

export class TestAll extends BaseTask {

    protected getName() : string {
        return "test";
    }

    protected async processAsync($option : string) : Promise<void> {
        let chain : string[] = [];
        switch ($option) {
        case "all":
            chain = ["test:lint", "test:code"];
            break;
        case "lint":
            chain = ["test:tslint", "test:sasslint", "test:jslint", "test:xcpplint", "test:javalint", "test:pythonlint"];
            break;
        case "code":
            chain = ["run-coverage-test", "test:selenium"];
            break;

        case "tslint":
            chain = ["tslint:project"];
            break;
        case "sasslint":
            chain = ["sass-lint"];
            break;
        case "jslint":
            chain = ["jslint:bin", "jslint:source", "jslint:configs"];
            break;
        case "xcpplint":
            chain = ["xcpp-lint:source"];
            if (this.programArgs.getOptions().withDependencies) {
                chain.push("xcpp-lint:dependencies");
            }
            chain.push("xcpp-lint:unit");
            break;
        case "javalint":
            chain = ["java-lint"];
            break;
        case "pythonlint":
            chain = ["python-lint"];
            break;
        case "typescriptunit":
            chain = ["typescript-test:unit"];
            break;
        case "xcppunit":
            chain = ["xcpp-test:unit"];
            break;
        case "pythonunit":
            chain = ["python-test:unit"];
            break;
        case "xcppcoverage":
            chain = ["xcpp-test:coverage"];
            break;
        case "coverage":
            chain = ["typescript:unit", "typescript-test:coverage"];
            break;
        case "selenium":
            if (this.properties.projectHas.SeleniumTests()) {
                if (this.properties.projectHas.TypeScript.Tests()) {
                    if (!this.properties.projectHas.ESModules() && this.programArgs.getOptions().withDependencies) {
                        const config : any = TypeScriptCompile.getConfig().selenium;
                        config.src = [
                            this.properties.sources + "/*.{ts,tsx}", this.properties.dependencies + "/*.{ts,tsx}",
                            "test/selenium/**/*.{ts,tsx}", "dependencies/*/test/selenium/**/*.{ts,tsx}",
                            "bin/resource/scripts/UnitTestRunner.ts",
                            "!**/*.d.ts"
                        ];
                        LogIt.Info("Selenium tests will include tests from dependencies.");
                    }
                }
                chain = [
                    "typescript:selenium", "selenium-server-stop", "selenium-server-start", "mock-server", "typescript-test:selenium",
                    "selenium-server-stop"
                ];
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Selenium test task skipped: test files not found");
            }
            break;

        default:
            LogIt.Error("Unsupported test option: " + $option);
            break;
        }
        await this.runTaskAsync(chain);
    }
}
