/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class VirtualBoxServer extends BaseTask {

    protected getName() : string {
        return "vbox-server-start";
    }

    protected async processAsync($option : string) : Promise<void> {
        const res : IExecuteResult = await this.terminal.ExecuteAsync("tasklist", [], null);
        if (res.exitCode === 0) {
            if (!StringUtils.ContainsIgnoreCase(res.std[0], "VBoxWebSrv.exe")) {
                const res : IExecuteResult = await this.terminal.SpawnAsync("VBoxWebSrv.exe",
                    ["-A", "null"], "C:/Program Files/Oracle/VirtualBox");
                if (res.exitCode !== 0) {
                    LogIt.Error(res.std[0] + res.std[1]);
                }
            }
        } else {
            LogIt.Error(res.std[0] + res.std[1]);
        }
    }
}
