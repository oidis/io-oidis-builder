/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BuildTaskType } from "../../Enums/BuildTaskType.js";
import { CliTaskType } from "../../Enums/CliTaskType.js";
import { GeneralTaskType } from "../../Enums/GeneralTaskType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class BuildStateMachine extends BaseTask {

    protected getName() : string {
        return "build-machine";
    }

    protected async processAsync($option : string) : Promise<void> {
        let chain : string[] = [];
        switch ($option) {
        case CliTaskType.EAP:
            chain = [GeneralTaskType.GET_SYSTEM_INFO, "install:eap", "build-machine:product-eap"];
            if (ObjectValidator.IsEmptyOrNull(this.project.target.hubLocation)) {
                this.project.target.hubLocation = this.project.deploy.server.eap.location;
            }
            break;
        case CliTaskType.PROD:
            chain = [GeneralTaskType.GET_SYSTEM_INFO, "install:prod", "git-manager:porcelaincheck", "build-machine:product-prod"];
            if (ObjectValidator.IsEmptyOrNull(this.project.target.hubLocation)) {
                this.project.target.hubLocation = this.project.deploy.server.prod.location;
            }
            break;
        case CliTaskType.DEV:
        case CliTaskType.DEV_SKIP_TEST:
            if (this.programArgs.getOptions().withTest) {
                chain = ["install:dev", "test:lint", "build-machine:product-dev"];
            } else {
                chain = [GeneralTaskType.GET_SYSTEM_INFO, "install:dev", "build-machine:product-dev-skip-test"];
            }
            break;
        case CliTaskType.REBUILD:
        case CliTaskType.REBUILD_DEV:
        case CliTaskType.REBUILD_DEV_SKIP_TEST:
            this.build.isRebuild = true;
            chain = ["project-cleanup:cache", "build:dev-skip-test"];
            break;
        case CliTaskType.REBUILD_EAP:
        case CliTaskType.REBUILD_EAP_SKIP_TEST:
            this.build.isRebuild = true;
            chain = ["project-cleanup:cache", "build:eap"];
            break;

        case "product-dev-skip-test":
            chain = ["compile:dev", BuildTaskType.COPY_TO_TARGET];
            break;
        case "product-dev":
            chain = ["test:code", "compile:dev", BuildTaskType.COPY_TO_TARGET];
            break;
        case "product-eap":
            chain = ["compile:eap", BuildTaskType.COPY_TO_TARGET, "project-cleanup:prod"];
            break;
        case "product-prod":
            chain = ["test:all", "compile:prod", BuildTaskType.COPY_TO_TARGET, "project-cleanup:prod"];
            break;

        default:
            LogIt.Error("Unsupported build state option: " + $option);
            break;
        }
        await this.runTaskAsync(chain);
    }
}
