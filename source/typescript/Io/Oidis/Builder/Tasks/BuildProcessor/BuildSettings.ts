/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import {ColorType} from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { BuildTaskType } from "../../Enums/BuildTaskType.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class BuildSettings extends BaseTask {

    protected getName() : string {
        return BuildTaskType.BUILD_SETTINGS;
    }

    protected async processAsync($option : string) : Promise<void> {
        const buildLog : string = this.properties.projectBase + "/log/build.log";
        this.fileSystem.Delete(buildLog);
        Loader.getInstance().setLogPath(buildLog);
        this.build.type = $option;
        if (ObjectValidator.IsEmptyOrNull(this.project.target.hubLocation)) {
            this.project.target.hubLocation = this.project.deploy.server.dev.location;
        }
        LogIt.Info(">>"[ColorType.YELLOW] + " Build type: " + this.build.type + ", Build time: " + this.build.time +
            ", Platforms: " + this.project.target.platforms);
    }
}
