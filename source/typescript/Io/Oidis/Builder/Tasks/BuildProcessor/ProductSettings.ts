/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildProductArgs } from "../../Structures/BuildProductArgs.js";
import { BuildExecutor, IReleaseProperties } from "./BuildExecutor.js";

export class ProductSettings extends BaseTask {

    protected getName() : string {
        return "product-settings";
    }

    protected async processAsync($option : string) : Promise<void> {
        const executor : BuildExecutor = new BuildExecutor();
        const products : any = [];
        this.properties.mobilePlatforms = [];
        executor.getReleases().forEach(($release : IReleaseProperties) : void => {
            executor.getTargetProducts($release.target).forEach(($product : BuildProductArgs) : void => {
                products.push({
                    args       : $product,
                    releaseName: $release.name,
                    target     : $release.target
                });
                if ($product.Toolchain() === ToolchainType.PHONEGAP) {
                    this.properties.mobilePlatforms.push($product.OS().toString());
                }
            });
        });
        const index : number = StringUtils.ToInteger($option);
        const product : any = products[index];
        this.build.product = product.args;
        if (this.properties.projectHas.Cpp.Source()) {
            this.build.platform = product.args.Type();
        } else {
            this.build.platform = product.args.Value();
        }
        this.build.releaseName = product.releaseName;
        this.project.target = product.target;
        if (product.args.Toolchain() !== ToolchainType.MSVC ||
            product.args.Toolchain() === ToolchainType.MSVC && ObjectValidator.IsEmptyOrNull(this.project.target.toolchain)) {
            this.project.target.toolchain = product.args.Toolchain();
        }
    }
}
