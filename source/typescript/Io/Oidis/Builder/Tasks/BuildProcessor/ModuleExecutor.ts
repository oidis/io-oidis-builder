/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { ModulesResolver } from "../../ModulesResolver.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class ModuleExecutor extends BaseTask {

    protected getName() : string {
        return "module";
    }

    protected async processAsync($option : string) : Promise<void> {
        const resolver : ModulesResolver = ModulesResolver.getInstance();
        if (resolver.getAll().indexOf($option) !== -1) {
            if (!ObjectValidator.IsEmptyOrNull(this.programArgs.getOptions().forwardArgs)) {
                this.project.modules[$option].args = this.programArgs.getOptions().forwardArgs;
            }
            await resolver.ResolveAsync($option, null);
        } else {
            LogIt.Error("Unsupported module: " + $option);
        }
    }
}
