/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { Resources } from "../../DAO/Resources.js";
import { ArchType } from "../../Enums/ArchType.js";
import { BuildTaskType } from "../../Enums/BuildTaskType.js";
import { CliTaskType } from "../../Enums/CliTaskType.js";
import { OSType } from "../../Enums/OSType.js";
import { ProductType } from "../../Enums/ProductType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { IProjectTarget } from "../../Interfaces/IProject.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildProductArgs } from "../../Structures/BuildProductArgs.js";
import { BuildCheck } from "../Testing/BuildCheck.js";

export class BuildExecutor extends BaseTask {
    private productIndex : number;
    private releases : IReleaseProperties[];

    public getReleases() : IReleaseProperties[] {
        const releases : IReleaseProperties[] = [];
        if (!ObjectValidator.IsEmptyOrNull(this.project.releases)) {
            let name : string;
            for (name in this.project.releases) {
                if (this.project.releases.hasOwnProperty(name)) {
                    const target : any = {};
                    Resources.Extend(target, this.project.target);
                    Resources.Extend(target, this.project.releases[name]);
                    this.project.releases[name] = target;
                    if (!target.hasOwnProperty("skipped") || !target.skipped) {
                        releases.push({
                            name,
                            target
                        });
                    }
                }
            }
        }
        if (releases.length === 0) {
            releases.push({
                name  : "",
                target: this.project.target
            });
        }
        return releases;
    }

    public getTargetProducts($target? : IProjectTarget) : BuildProductArgs[] {
        let products : BuildProductArgs[] = [];
        if (ObjectValidator.IsEmptyOrNull($target)) {
            for (const item of this.getReleases()) {
                products = products.concat(this.getTargetProducts(item.target));
            }
        } else {
            if (ObjectValidator.IsString($target.platforms)) {
                $target.platforms = [<string>$target.platforms];
            }
            (<string[]>$target.platforms).forEach(($platform : string) : void => {
                const args : BuildProductArgs = new BuildProductArgs();
                args.Parse($platform, $target.toolchain);
                if (this.properties.projectHas.Cpp.Source()) {
                    if (ObjectValidator.IsEmptyOrNull(args.Toolchain()) && (args.Type() === ProductType.APP)) {
                        if (args.OS() === OSType.MAC) {
                            args.Toolchain(ToolchainType.CLANG);
                        } else if (args.OS() === OSType.IMX) {
                            args.Toolchain(ToolchainType.ARM);
                            args.OS(OSType.ARM);  // replace IMX by ARM
                            args.Arch(ArchType.X64);
                        } else if (args.OS() === OSType.ARM && args.Arch() === ArchType.X32) {
                            args.Toolchain(ToolchainType.AARCH32);
                        } else if (args.OS() === OSType.ARM) {
                            args.Toolchain(ToolchainType.AARCH64);
                        } else {
                            args.Toolchain(ToolchainType.GCC);
                        }
                    }
                } else if (this.properties.projectHas.Java.Source()) {
                    args.Type(ProductType.SHARED);
                    if (ObjectValidator.IsEmptyOrNull($target.toolchain) &&
                        args.Toolchain() !== ToolchainType.ECLIPSE &&
                        args.Toolchain() !== ToolchainType.IDEA) {
                        args.Toolchain(ToolchainType.JDK);
                    }
                }
                products.push(args);
            });
        }
        return products;
    }

    public getDependenciesTree($option : string) : string[] {
        if ($option === CliTaskType.ALPHA || $option === CliTaskType.BETA) {
            $option = CliTaskType.EAP;
        }

        let tree : string[] = [];

        this.releases = this.getReleases();

        const releasesChain : ITargetChain = {
            chain             : ["project-cleanup:init"],
            requiresServer    : false,
            requiresWuiModules: false
        };
        this.productIndex = 0;
        for (let index : number = 0; index < this.releases.length; index++) {
            const result : ITargetChain = this.getTargetChain(this.releases[index].target, $option, index);
            if (result.requiresWuiModules) {
                releasesChain.requiresWuiModules = true;
            }
            if (result.requiresServer) {
                releasesChain.requiresServer = true;
            }
            releasesChain.chain = releasesChain.chain.concat(result.chain);
        }

        if (releasesChain.requiresWuiModules) {
            if ($option === CliTaskType.PROD) {
                tree.push("wui-modules-download:force");
            } else {
                tree.push("wui-modules-download");
            }
        }
        tree = tree.concat(releasesChain.chain);

        if (tree.length > 0) {
            tree.unshift("builder-service:stop");
            tree.push("build-check:" + $option);
            if (!this.programArgs.getOptions().noServer && !this.project.noServer && releasesChain.requiresServer) {
                tree.push("builder-service:start");
            }
        }

        return tree;
    }

    protected getName() : string {
        return BuildTaskType.DEFAULT;
    }

    protected async processAsync($option : string) : Promise<void> {
        BuildCheck.RegisterCheck(async () : Promise<void> => {
            if (!this.fileSystem.Exists(this.properties.projectBase + "/build/target") &&
                !this.fileSystem.Exists(this.properties.projectBase + "/build/releases")) {
                LogIt.Error("Unable to find target folder");
            }
        });

        await this.runTaskAsync(this.getDependenciesTree($option));
    }

    private getProductChain($args : BuildProductArgs, $buildType : string,
                            $buildProduct : boolean = true, $releaseProduct : boolean = false) : string[] {
        let isProd : boolean = $buildType === CliTaskType.PROD || $buildType === CliTaskType.EAP;
        const chain : string[] = ["product-settings:" + this.productIndex];

        if ($args.Type() !== ProductType.INSTALLER) {
            if ($buildProduct) {
                chain.push("build-machine:" + $buildType);
            } else {
                if ($args.Toolchain() === ToolchainType.ECLIPSE ||
                    $args.Toolchain() === ToolchainType.IDEA) {
                    chain.push("plugin-clean-compile");
                    chain.push("build-machine:product-" + $buildType);
                }
            }
        }

        if ($args.Type() === ProductType.INSTALLER) {
            if (ObjectValidator.IsEmptyOrNull(this.project.target.hubLocation)) {
                switch ($buildType) {
                case CliTaskType.PROD:
                    this.project.target.hubLocation = this.project.deploy.server.prod.location;
                    break;
                case CliTaskType.EAP:
                    this.project.target.hubLocation = this.project.deploy.server.eap.location;
                    break;
                default:
                    this.project.target.hubLocation = this.project.deploy.server.dev.location;
                    break;
                }
            }
            chain.push(
                "copy-local-scripts:target",
                "selfextractor",
                "upx",
                "code-sign");
        } else if ($args.Type() === ProductType.PLUGIN) {
            if (!$releaseProduct) {
                chain.push("compress:deploy");
            }
            chain.push(
                "var-replace",
                "copy-local-scripts:target",
                "cache-manager:finalize-" + $args.Toolchain(),
                "wui-modules-release:" + $args.Toolchain(),
                "string-replace:variables-plugin");
            if (isProd) {
                chain.push("project-cleanup:prod");
            }
            chain.push(
                "compress:pluginTarget",
                "compress:deployJava");
        } else {
            switch ($args.Toolchain()) {
            case ToolchainType.CHROMIUM_RE:
                if (!$releaseProduct) {
                    chain.push("compress:deploy");
                }
                chain.push(
                    "var-replace",
                    "copy-local-scripts:target",
                    "cache-manager:finalize-" + $args.Toolchain(),
                    "wui-modules-release:" + $args.Toolchain(),
                    "upx",
                    "code-sign");
                break;
            case ToolchainType.GCC:
            case ToolchainType.ARM:
            case ToolchainType.AARCH32:
            case ToolchainType.AARCH64:
            case ToolchainType.CLANG:
            case ToolchainType.GYP:
                if ($args.Type() === ProductType.APP) {
                    chain.push("upx", "code-sign");
                } else if ($args.Type() === ProductType.SHARED || $args.Type() === ProductType.STATIC) {
                    chain.push("upx");
                }
                $releaseProduct = true;
                break;
            case ToolchainType.NODEJS:
                chain.push("copy-local-scripts:schema");
                if ($args.Type() === ProductType.APP) {
                    chain.push(
                        "nodejs-packaging",
                        "upx",
                        "code-sign"
                    );
                } else if ($args.Type() === ProductType.SHARED) {
                    chain.push(
                        "nodejs-packaging:shared",
                        "code-sign"
                    );
                }
                break;
            case ToolchainType.JDK:
                chain.push("java-packaging");
                break;
            case ToolchainType.ECLIPSE:
            case ToolchainType.IDEA:
                chain.push("java-packaging:" + $args.Toolchain());
                isProd = false;
                break;
            case ToolchainType.PHONEGAP:
                if ($buildProduct) {
                    chain.push(
                        "compress:deploy",
                        "var-replace",
                        "copy-local-scripts:target");
                    if (isProd) {
                        chain.push("project-cleanup:prod");
                    }
                    chain.push(
                        "copy-scripts:phonegap",
                        "copy-local-scripts:phonegap",
                        "compress:phonegap",
                        "phonegap-build");
                }
                isProd = false;
                $releaseProduct = true;
                break;
            case ToolchainType.PIP:
                chain.push(
                    "copy-local-scripts:pip",
                    "python-packaging"
                );
                break;
            default:
                // ignore none toolchain
                break;
            }

            if (isProd) {
                chain.push("project-cleanup:prod");
            }
            chain.push("compress:deploy");
        }
        if ($releaseProduct) {
            chain.push("release-platform:" + $args.Value());
        }
        this.productIndex++;
        return chain;
    }

    private getTargetChain($target : IProjectTarget, $buildType : string, $releaseIndex : number) : ITargetChain {
        const output : ITargetChain = {
            chain             : [],
            requiresServer    : false,
            requiresWuiModules: false
        };

        const products : BuildProductArgs[] = this.getTargetProducts($target);
        let productIndex : number = 0;
        const productsCount : number = products.length;
        products.forEach(($args : BuildProductArgs) : void => {
            if (this.properties.projectHas.Cpp.Source() && (
                ($args.OS() === OSType.WIN && !EnvironmentHelper.IsWindows()) ||
                ($args.OS() === OSType.LINUX && !EnvironmentHelper.IsLinux()) ||
                ($args.OS() === OSType.MAC && !EnvironmentHelper.IsMac()))) {
                let targetName : string = this.releases[$releaseIndex].name;
                if (ObjectValidator.IsEmptyOrNull(targetName)) {
                    targetName = "target";
                }
                LogIt.Warning("Release platform \"" + $args.Value() + "\" " +
                    "skipped for \"" + targetName + "\": " +
                    "out of capabilities on current builder instance.");
            } else {
                if ($args.Type() === ProductType.INSTALLER) {
                    output.requiresWuiModules = true;
                } else if ($args.Toolchain() === ToolchainType.NONE) {
                    output.requiresServer = true;
                } else if ($args.Type() === ProductType.APP || $args.Type() === ProductType.PLUGIN) {
                    if ($args.Toolchain() !== ToolchainType.NODEJS &&
                        $args.Toolchain() !== ToolchainType.GCC &&
                        $args.Toolchain() !== ToolchainType.MSVC &&
                        $args.Toolchain() !== ToolchainType.CLANG &&
                        $args.Toolchain() !== ToolchainType.GYP &&
                        $args.Toolchain() !== ToolchainType.PHONEGAP) {
                        output.requiresWuiModules = true;
                    }
                }
                output.chain = output.chain.concat(this.getProductChain($args, $buildType, productIndex === 0,
                    productsCount > 1 || this.releases.length > 1));
            }
            productIndex++;
        });

        return output;
    }
}

export interface ITargetChain {
    requiresWuiModules : boolean;
    requiresServer : boolean;
    chain : string[];
}

export interface IReleaseProperties {
    name : string;
    target : IProjectTarget;
}

// generated-code-start
export const ITargetChain = globalThis.RegisterInterface(["requiresWuiModules", "requiresServer", "chain"]);
export const IReleaseProperties = globalThis.RegisterInterface(["name", "target"]);
// generated-code-end
