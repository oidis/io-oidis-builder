/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseTask } from "../../Primitives/BaseTask.js";

export class InitTarget extends BaseTask {

    protected getName() : string {
        return "init-target";
    }

    protected async processAsync($option : string) : Promise<void> {
        const baseTarget : string = this.properties.projectBase + "/build/target";
        const tempPackage : string = baseTarget + "/target";

        await this.fileSystem.CopyAsync(this.properties.projectBase + "/build/compiled/java/output", tempPackage);
        if ($option === "idea") {
            await this.fileSystem.CopyAsync(tempPackage + "/lib", baseTarget + "/lib");
            this.fileSystem.Delete(tempPackage + "/lib");
        }
    }
}
