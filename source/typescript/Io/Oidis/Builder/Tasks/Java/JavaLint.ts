/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class JavaLint extends BaseTask {

    protected getName() : string {
        return "java-lint";
    }

    protected async processAsync($option : string) : Promise<void> {
        if (this.properties.projectHas.Java.Source()) {
            await this.runTaskAsync("maven-env:init", "maven-build:lint", "maven-env:clean");
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " Java lint task skipped: 0 source files have been found");
        }
    }
}
