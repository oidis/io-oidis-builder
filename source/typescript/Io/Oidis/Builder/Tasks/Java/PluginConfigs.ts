/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class PluginConfigs extends BaseTask {

    protected getName() : string {
        return "plugin-configs";
    }

    protected async processAsync($option : string) : Promise<void> {
        let config : string = this.fileSystem.Read(
            this.properties.projectBase + "/resource/configs/default.config.jsonp").toString();
        config = config.substring(config.indexOf("({") + 1,
            config.lastIndexOf("});") + 1);
        const parsedConfig : any = eval("(" + config + ")");
        if ($option === "eclipse") {
            const os : any = require("os");
            const templates : any = {
                command: os.EOL +
                    "<elements xsi:type=\"commands:Command\" xmi:id=\"<? @var project.name ?>.Commands.<? @var this.id ?>\" " +
                    "elementId=\"<? @var project.name ?>.Commands.<? @var this.id ?>\" commandName=\"<? @var this.id ?>\"/>",
                handler: os.EOL +
                    "<elements xsi:type=\"commands:Handler\" xmi:id=\"<? @var project.name ?>.Handlers.<? @var this.id ?>\" " +
                    "elementId=\"<? @var project.name ?>.Handlers.<? @var this.id ?>\" " +
                    "contributionURI=\"bundleclass://<? @var project.name ?>/Io.Oidis.IdeJRE.Eclipse.Handlers.Handler\" " +
                    "command=\"<? @var project.name ?>.Commands.<? @var this.id ?>\"/>",
                menu   : os.EOL +
                    "    <elements xsi:type=\"menu:MenuContribution\" " +
                    "xmi:id=\"<? @var project.name ?>.MenuContributions.<? @var this.id ?>\" " +
                    "elementId=\"<? @var project.name ?>.MenuContributions.<? @var this.id ?>\" " +
                    "parentId=\"org.eclipse.ui.main.menu\">" +
                    os.EOL + "      <children xsi:type=\"menu:Menu\" xmi:id=\"<? @var project.name ?>.Menus.<? @var this.id ?>\" " +
                    "elementId=\"<? @var project.name ?>.Menus.<? @var this.id ?>\" label=\"<? @var this.label ?>\" >" + os.EOL +
                    "        <children xsi:type=\"menu:DynamicMenuContribution\" " +
                    "xmi:id=\"<? @var project.name ?>.DynamicMenuContributions.<? @var this.id ?>\" " +
                    "elementId=\"<? @var project.name ?>.DynamicMenuContributions.<? @var this.id ?>\" " +
                    "label=\"<? @var this.label ?>\" contributionURI=" +
                    "\"bundleclass://<? @var project.name ?>/Io.Oidis.IdeJRE.Eclipse.Menus.DynamicMenuContribution\"/>" +
                    os.EOL +
                    "      </children>" + os.EOL +
                    "    </elements>",
                tool   : os.EOL +
                    "        <children xsi:type=\"menu:HandledToolItem\" xmi:id=\"<? @var project.name ?>.Tools.<? @var this.id ?>\" " +
                    "elementId=\"<? @var project.name ?>.Tools.<? @var this.id ?>\" " +
                    "label=\"<? @var this.label ?>\" iconURI=\"platform:/plugin/<? @var project.name ?>/<? @var this.icon ?>\" " +
                    "command=\"<? @var project.name ?>.Commands.<? @var this.id ?>\"/>",
                toolbar: os.EOL +
                    "    <elements xsi:type=\"menu:TrimContribution\" " +
                    "xmi:id=\"<? @var project.name ?>.TrimContributions.<? @var this.id ?>\" " +
                    "elementId=\"<? @var project.name ?>.TrimContributions.<? @var this.id ?>\" " +
                    "parentId=\"org.eclipse.ui.main.toolbar\" positionInParent=\"\">" + os.EOL +
                    "      <children xsi:type=\"menu:ToolBar\" xmi:id=\"<? @var project.name ?>.ToolBars.<? @var this.id ?>\" " +
                    "elementId=\"<? @var project.name ?>.ToolBars.<? @var this.id ?>\"><!-- @tools -->" + os.EOL +
                    "      </children>" + os.EOL +
                    "    </elements>",
                view   : os.EOL +
                    "    <elements xsi:type=\"basic:PartDescriptor\" xmi:id=\"<? @var project.name ?>.Views.<? @var this.id ?>\" " +
                    "elementId=\"<? @var project.name ?>.Views.<? @var this.id ?>\" label=\"<? @var this.label ?>\" " +
                    "iconURI=\"platform:/plugin/<? @var project.name ?>/<? @var this.icon ?>\" " +
                    "category=\"<? @var project.name ?>\" closeable=\"true\" " +
                    "contributionURI=\"bundleclass://<? @var project.name ?>/Io.Oidis.IdeJRE.Eclipse.Views.ViewContribution\">" +
                    os.EOL + "      <tags>View</tags>" + os.EOL +
                    "    </elements>"
            };

            const toolbar : string = this.generateContribution([this.project.name], templates.toolbar)
                .replace("<!-- @tools -->", this.generateContribution(parsedConfig.tools, templates.tool));

            this.fileSystem.Write(this.properties.projectBase + "/build/compiled/java/output/fragment.e4xmi",
                this.fileSystem.Read(this.properties.projectBase + "/bin/resource/configs/fragment.e4xmi").toString()
                    .replace("<!-- @views -->", this.generateContribution(parsedConfig.views, templates.view))
                    .replace("<!-- @menus -->", this.generateContribution(parsedConfig.menus, templates.menu))
                    .replace("<!-- @commands -->", this.generateContribution(parsedConfig.tools, templates.command))
                    .replace("<!-- @handlers -->", this.generateContribution(parsedConfig.tools, templates.handler))
                    .replace("<!-- @toolbars -->", toolbar));
            this.fileSystem.Write(this.properties.projectBase + "/build/compiled/java/output/plugin.xml",
                this.fileSystem.Read(this.properties.projectBase + "/bin/resource/configs/plugin.eclipse.xml"));
        } else if ($option === "idea") {
            this.fileSystem.Write(this.properties.projectBase + "/build/compiled/java/output/META-INF/plugin.xml",
                this.fileSystem.Read(this.properties.projectBase + "/bin/resource/configs/plugin.idea.xml"));
        }
    }

    private generateContribution($values : any, $template : any) : string {
        let contribution : string = "";
        for (const property in $values) {
            if ($values.hasOwnProperty(property)) {
                const label : string = ObjectValidator.IsSet($values[property].label) ? $values[property].label : property;
                const icon : string = ObjectValidator.IsSet($values[property].icon) ? $values[property].icon : "";
                contribution += $template
                    .replace(/<\? @var this\.label \?>/gi, label)
                    .replace(/<\? @var this\.icon \?>/gi, icon)
                    .replace(/<\? @var this\.id \?>/gi, property)
                    .replace(/<\? @var project\.name \?>/gi, this.project.name);
            }
        }
        return contribution;
    }
}
