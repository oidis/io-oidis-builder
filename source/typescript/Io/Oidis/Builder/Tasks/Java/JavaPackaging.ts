/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseTask } from "../../Primitives/BaseTask.js";

export class JavaPackaging extends BaseTask {

    protected getName() : string {
        return "java-packaging";
    }

    protected async processAsync($option : string) : Promise<void> {
        const targetPath : string = this.properties.projectBase + "/build/target";
        const packageName : string = this.project.target.name + "-" + this.project.version.replace(/\./g, "-");
        const tempPackage : string = targetPath + "/target";
        const packagePath : string = targetPath + "/" + packageName;
        await this.fileSystem.PackAsync(tempPackage + "/**/*", {autoStrip: true, output: packagePath, type: "zip"});
        if ($option === "idea") {
            this.fileSystem.Write(targetPath + "/lib/" + this.project.target.name + ".jar",
                this.fileSystem.Read(packagePath + ".zip"));
            this.fileSystem.Delete(packagePath + ".zip");
            await this.fileSystem.CopyAsync(targetPath + "/lib", tempPackage + "/" + packageName + "/lib");
            await this.fileSystem.PackAsync(tempPackage + "/" + packageName, {output: packagePath, type: "zip"});
            this.fileSystem.Delete(targetPath + "/" + packageName);
            this.fileSystem.Delete(targetPath + "/lib");
            this.fileSystem.Delete(tempPackage);
        } else {
            this.fileSystem.Delete(tempPackage);
        }
    }
}
