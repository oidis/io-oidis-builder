/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseTask } from "../../Primitives/BaseTask.js";

export class PluginCleanCompile extends BaseTask {

    protected getName() : string {
        return "plugin-clean-compile";
    }

    protected async processAsync($option : string) : Promise<void> {
        const outputPath : string = this.properties.projectBase + "/build/compiled/java/output";
        this.fileSystem.Delete(outputPath + "/lib");
        this.fileSystem.Delete(outputPath + "/plugin.xml");
        this.fileSystem.Delete(outputPath + "/META-INF/MANIFEST.MF");
        this.fileSystem.Delete(outputPath + "/META-INF/plugin.xml");
    }
}
