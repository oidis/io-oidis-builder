/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { ToolchainType } from "../../Enums/ToolchainType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class MavenBuild extends BaseTask {

    protected getName() : string {
        return "maven-build";
    }

    protected async processAsync($option : string) : Promise<void> {
        const isEclipseBuild : boolean = this.project.target.toolchain === ToolchainType.ECLIPSE;
        const isIdeaBuild : boolean = this.project.target.toolchain === ToolchainType.IDEA;
        const tasks : string[] = [];
        switch ($option) {
        case "unit":
            tasks.push("maven-command-runner:unit");
            break;
        case "coverage":
            tasks.push("maven-command-runner:coverage");
            break;
        case "lint":
            tasks.push("maven-command-runner:lint");
            break;
        default:
            tasks.push("maven-command-runner:compile");
            tasks.push("config-compile");
            if (isEclipseBuild || isIdeaBuild) {
                if (isEclipseBuild) {
                    tasks.push("plugin-configs:eclipse");
                    tasks.push("manifest-classpath:eclipse");
                    tasks.push("init-target:eclipse");
                } else if (isIdeaBuild) {
                    tasks.push("plugin-configs:idea");
                    tasks.push("manifest-classpath:idea");
                    tasks.push("init-target:idea");
                }
            } else {
                tasks.push("manifest-classpath");
                tasks.push("init-target");
            }
            break;
        }
        await this.runTaskAsync(tasks);
    }
}
