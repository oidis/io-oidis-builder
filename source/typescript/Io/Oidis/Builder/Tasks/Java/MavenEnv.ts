/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { IMavenArtifact } from "../../Interfaces/IProperties.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class MavenEnv extends BaseTask {
    private static config : any;

    public static getConfig() : any {
        if (ObjectValidator.IsEmptyOrNull(MavenEnv.config)) {
            MavenEnv.config = {
                unit: {
                    src: []
                }
            };
        }
        return MavenEnv.config;
    }

    protected getName() : string {
        return "maven-env";
    }

    protected async processAsync($option : string) : Promise<void> {
        const os : any = require("os");
        this.fileSystem.Delete(this.properties.projectBase + "/compiled.pom.xml");
        const isEclipseBuild : boolean = this.project.target.toolchain === ToolchainType.ECLIPSE;
        const isIdeaBuild : boolean = this.project.target.toolchain === ToolchainType.IDEA;
        if ($option === "init") {
            let pomFile : string = this.fileSystem.Read(this.properties.projectBase + "/bin/resource/scripts/pom.xml").toString();
            pomFile = StringUtils.Replace(pomFile, "<!-- @parent -->", "javalibrary.config");
            const pomPath = this.properties.projectBase + "/compiled.pom.xml";
            this.fileSystem.Write(pomPath, pomFile);

            let excludedPackages : string = "";
            if (isEclipseBuild) {
                excludedPackages += os.EOL +
                    "                            <exclude>**/Idea/**</exclude>";
            } else if (isIdeaBuild) {
                excludedPackages += os.EOL +
                    "                            <exclude>**/Eclipse/**</exclude>";
            }
            const libPath : string = this.properties.projectBase + "/bin/resource/configs/javalibrary.config.xml";
            this.fileSystem.Write(libPath, this.fileSystem.Read(libPath).toString().replace("<!-- @excludes -->", excludedPackages));

            if (isEclipseBuild && this.fileSystem.IsEmpty(this.properties.projectBase + "/build_cache/runtime_libs_eclipse")) {
                await this.fileSystem.CopyAsync(
                    this.externalModules + "/eclipse/plugins",
                    this.properties.projectBase + "/build_cache/runtime_libs_eclipse");
            }
            if (isIdeaBuild && this.fileSystem.IsEmpty(this.properties.projectBase + "/build_cache/runtime_libs_idea")) {
                await this.fileSystem.CopyAsync(
                    this.externalModules + "/idea/lib",
                    this.properties.projectBase + "/build_cache/runtime_libs_idea");
            }
            if (this.fileSystem.IsEmpty(this.properties.projectBase + "/build_cache/test")) {
                const testRunnerSource : string = this.properties.projectBase + "/bin/resource/scripts/";
                const testRunnerTarget : string = this.properties.projectBase + "/build_cache/test/Io/Oidis/";
                this.fileSystem.Write(testRunnerTarget + "UnitTestRunner.java",
                    this.fileSystem.Read(testRunnerSource + "UnitTestRunner.java"));
                this.fileSystem.Write(testRunnerTarget + "UnitTestListener.java",
                    this.fileSystem.Read(testRunnerSource + "UnitTestListener.java"));
                this.fileSystem.Write(testRunnerTarget + "Assert.java",
                    this.fileSystem.Read(testRunnerSource + "Assert.java"));
                this.fileSystem.Write(testRunnerTarget + "UnitTest.java",
                    this.fileSystem.Read(testRunnerSource + "UnitTest.java"));
            }

            if (this.fileSystem.IsEmpty(this.properties.projectBase + "/build_cache/lib")) {
                for await (const artifact of this.properties.mavenArtifacts) {
                    if (!StringUtils.Contains(artifact.location, "http://", "https://")) {
                        let location : string = artifact.location;
                        if (EnvironmentHelper.IsWindows()) {
                            location = StringUtils.Remove(location, "file:///");
                        }
                        location = StringUtils.Remove(location, "file://");

                        if ((await this.terminal.SpawnAsync(
                            "mvn", [
                                "-f", pomPath,
                                "install:install-file",
                                "-Dfile=" + location,
                                "-DgroupId=" + artifact.groupId,
                                "-DartifactId=" + artifact.artifactId,
                                "-Dversion=" + artifact.version,
                                "-Dpackaging=jar"
                            ], this.properties.projectBase)).exitCode !== 0) {
                            LogIt.Error("Jar install has failed.");
                        }
                    }
                }
            }
        }
    }
}
