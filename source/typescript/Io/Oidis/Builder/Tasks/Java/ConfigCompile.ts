/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class ConfigCompile extends BaseTask {

    protected getName() : string {
        return "config-compile";
    }

    protected async processAsync($option : string) : Promise<void> {
        const configPath : string = this.properties.projectBase + "/resource/configs/default.config.jsonp";
        if (this.fileSystem.Exists(configPath)) {
            let config : string = this.fileSystem.Read(configPath).toString();
            config = config.substring(config.indexOf("({") + 1,
                config.lastIndexOf("});") + 1);
            const parsedConfig : any = eval("(" + config + ")");
            parsedConfig.views = this.compileToArray(parsedConfig.views);
            parsedConfig.menus = this.compileToArray(parsedConfig.menus);
            parsedConfig.tools = this.compileToArray(parsedConfig.tools);
            this.fileSystem.Write(this.properties.projectBase + "/build/compiled/java/output/resource/configs/compiled.config.json",
                JSON.stringify(parsedConfig));
        } else {
            LogIt.Warning("config-compile tak skipped: default config does not exist");
        }
    }

    private compileToArray($object : any) : any[] {
        const array : any[] = [];
        for (const property in $object) {
            if ($object.hasOwnProperty(property)) {
                const object = {
                    icon : $object[property].icon,
                    id   : property,
                    label: $object[property].label
                };
                array.push(object);
            }
        }
        return array;
    }
}
