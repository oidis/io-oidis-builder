/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { MavenEnv } from "./MavenEnv.js";

export class MavenCommandRunner extends BaseTask {

    protected getName() : string {
        return "maven-command-runner";
    }

    protected async processAsync($option : string) : Promise<void> {
        let args : string[] = ["-f", this.properties.projectBase + "/compiled.pom.xml"];
        switch ($option) {
        case "clean":
            args = args.concat(["clean", "-o"]);
            break;
        case "compile":
            args = args.concat(["compile", "-Djacoco.skip=true", "-Dmaven.test.skip=true", "-Dskiptest=true"]);
            break;
        case "unit":
            const testSources : string[] = MavenEnv.getConfig().unit.src; // eslint-disable-line no-case-declarations
            if (!ObjectValidator.IsEmptyOrNull(testSources)) {
                args = args.concat(["-Dtest=" + testSources.join(",")]);
            }
            args = args.concat([
                "test", "-DfailIfNoTests=false", "-Djacoco.skip=true", "-Dcheckstyle.skip=true"
            ]);
            break;
        case "coverage":
            args = args.concat([
                "test", "-DfailIfNoTests=false", "-Dfindbugs.skip=true", "-Dcheckstyle.skip=true"
            ]);
            break;
        case "lint":
            args = args.concat("compile");
            break;
        default:
            LogIt.Error("Unsupported Maven command \"" + $option + "\".");
            break;
        }
        if ((await this.terminal.SpawnAsync("mvn", args, this.properties.projectBase)).exitCode !== 0) {
            LogIt.Error("Maven command \"" + $option + "\" has failed.");
        }
    }
}
