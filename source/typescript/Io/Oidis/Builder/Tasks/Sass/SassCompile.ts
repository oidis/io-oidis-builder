/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { CliTaskType } from "../../Enums/CliTaskType.js";
import { IProjectDependency } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class SassCompile extends BaseTask {
    private concat : any;
    private referenceRegister : string[];
    private sourceImports : any[];
    private dependenciesImports : any[];

    protected getName() : string {
        return "sass-compile";
    }

    protected async processAsync($option : string) : Promise<void> {
        const concatMap : any = require("concat-with-sourcemaps");

        this.referenceRegister = [];
        this.sourceImports = [];
        this.dependenciesImports = [];

        const configs : ISassConfig[] = <any>{
            source      : {
                options: {
                    style    : "expanded",
                    sourcemap: this.build.type === CliTaskType.DEV
                },
                files  : [
                    {
                        cwd : "resource/sass",
                        src : ["**/*.scss", "!**/*.d.scss"],
                        dest: this.properties.tmp + "/sass/Ι_" + this.project.name,
                        ext : ".css"
                    }
                ]
            },
            dependencies: {
                options: {
                    style    : "expanded",
                    sourcemap: false
                },
                files  : [
                    {
                        cwd : "dependencies",
                        src : ["*/resource/sass/**/*.scss", "!**/*.d.scss"],
                        dest: this.properties.tmp + "/sass",
                        ext : ".css"
                    }
                ]
            },
            cache       : {
                options: {
                    style    : "expanded",
                    sourcemap: true
                },
                files  : [
                    {
                        cwd : "dependencies",
                        src : ["*/resource/sass/**/*.scss", "!**/*.d.scss"],
                        dest: "build_cache/sass",
                        ext : ".css"
                    }
                ]
            }
        };
        if (!ObjectValidator.IsEmptyOrNull(configs)) {
            if (configs.hasOwnProperty($option)) {
                const config : ISassConfig = configs[$option];
                const referencePath : string = this.properties.projectBase + "/resource/sass/reference.d.scss";
                const useDefinition : boolean = this.fileSystem.Exists(referencePath);

                let filesGroup : ISassConfigFiles;
                let groupIndex : number;
                for (groupIndex = 0; groupIndex < config.files.length; groupIndex++) {
                    filesGroup = config.files[groupIndex];
                    if (useDefinition) {
                        this.traverseReference(referencePath, $option);
                        if ($option === "source") {
                            filesGroup.src = this.sourceImports;
                        } else {
                            filesGroup.src = this.dependenciesImports;
                        }
                        filesGroup.cwd = "resource/sass";
                    }
                    const cssFile : string = filesGroup.dest + "/" + $option + ".css";
                    this.concat = new concatMap(true, cssFile, "\n");

                    this.compiler(config.options, filesGroup.src);

                    let content : string = this.concat.content.toString();
                    if (config.options.sourcemap) {
                        content = content.replace(/^\/\*# sourceMappingURL=.*\.css\.map \*\/$/gm, "") +
                            "/*# sourceMappingURL=" + this.properties.packageName + ".min.css.map */\n";
                        const contentMap : any = JSON.parse(this.concat.sourceMap);
                        if (!ObjectValidator.IsEmptyOrNull(contentMap.sources)) {
                            let index : number;
                            for (index = 0; index < contentMap.sources.length; index++) {
                                const source : string = contentMap.sources[index];
                                const replaceKey : string =
                                    StringUtils.Contains(source, "dependencies/") ? "dependencies" : "resource/sass";
                                contentMap.sources[index] = "../../../.." +
                                    StringUtils.Substring(source, StringUtils.IndexOf(source, "/" + replaceKey + "/"));
                            }
                        }
                        this.fileSystem.Write(cssFile + ".map", JSON.stringify(contentMap));
                    }
                    this.fileSystem.Write(cssFile, content);
                }
            } else {
                LogIt.Error("Configuration for SASS compile task " + $option + " does not exist.");
            }
        } else {
            LogIt.Error("Configuration for SASS compile task does not exist.");
        }
    }

    private resolveAlias($path : string) : string {
        this.properties.oidisDependencies.forEach(($value : IProjectDependency) : void => {
            $path = StringUtils.Replace($path, "@" + $value.name + "/", "dependencies/" + $value.name + "/resource/sass/");
        });
        return $path;
    }

    private compiler($options : ISassConfigOptions, $source : string[]) : void {
        const sass : any = require("sass");

        let style : string = "none";
        let sourceMap : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($options)) {
            style = $options.style;
            sourceMap = $options.sourcemap;
        }

        for (let file of $source) {
            file = file + ".scss";
            try {
                if (this.fileSystem.IsFile(file)) {
                    LogIt.Info("> " + file);
                    const result : any = sass.compile(file, {
                        importers              : [
                            {
                                findFileUrl: ($path : string) : URL => {
                                    return new URL("file://" + this.properties.projectBase + "/" + this.resolveAlias($path) + ".scss");
                                }
                            }
                        ],
                        loadPaths              : [
                            this.properties.projectBase + "/"
                        ],
                        sourceMap,
                        sourceMapIncludeSources: false,
                        style
                    });
                    let output : string = result.css.toString();
                    output = output.replace(/\((.*?)\)/gm, ($match : string, $value : string) : string => {
                        let escaped : boolean = false;
                        if (StringUtils.Contains($value, "\"")) {
                            $value = StringUtils.Remove($value, "\"");
                            escaped = true;
                        }
                        ["graphics/", "libs/"].forEach(($path : string) : void => {
                            if (StringUtils.Contains($value, $path)) {
                                $value = "../" + StringUtils.Substring($value, StringUtils.IndexOf($value, $path));
                            }
                        });
                        if (escaped) {
                            $value = "\"" + $value + "\"";
                        }
                        return "(" + $value + ")";
                    });

                    if (ObjectValidator.IsEmptyOrNull(result.sourceMap)) {
                        this.concat.add("/" + file, output);
                    } else {
                        this.concat.add("/" + file, output, JSON.stringify(result.sourceMap));
                    }
                } else {
                    LogIt.Warning("> NOT FOUND: " + file);
                }
            } catch (ex) {
                let status : string;
                switch (ex.status) {
                case 1:
                    status = "[E] Syntax Error";
                    break;
                default:
                    status = "[W] Unknown error status " + ex.status;
                }
                LogIt.Error(ex.file + ":" + ex.line + " " + status + ": " + ex.message);
            }
        }
    }

    private traverseReference($file : string, $option : string, $parent? : string) : void {
        const PATH : any = require("path");

        $file = this.resolveAlias($file);
        LogIt.Debug("> processing file: {0}", $file);
        if (!this.referenceRegister.includes($file)) {
            if (!StringUtils.EndsWith($file, ".scss")) {
                $file += ".scss";
            }
            if (this.fileSystem.Exists($file)) {
                let cwd : string = this.properties.projectBase + "/resource/sass/";
                if (!ObjectValidator.IsEmptyOrNull($parent)) {
                    cwd = StringUtils.Replace($file, "/reference.d.scss", "/");
                }
                const content : any = StringUtils.StripComments(this.fileSystem.Read($file).toString());
                // eslint-disable-next-line no-useless-escape
                const regex : RegExp = new RegExp("^@(import|use) \"(?<path>.*(|\.d|\.d\.scss))\";$", "gm");
                let results : any = regex.exec(content);
                const importedReferences : string[] = [];
                while (!ObjectValidator.IsEmptyOrNull(results)) {
                    if (!ObjectValidator.IsEmptyOrNull(results.groups)) {
                        const path : string = this.resolveAlias(results.groups.path);
                        const fullPath : string = StringUtils.StartsWith(path, "dependencies/") ? this.properties.projectBase + "/" + path :
                            StringUtils.Replace(PATH.resolve(cwd + path), "\\", "/");
                        if (!ObjectValidator.IsEmptyOrNull(fullPath) && !this.referenceRegister.includes(fullPath)) {
                            if (StringUtils.Contains(fullPath, "/reference.d")) {
                                if (!importedReferences.includes(fullPath)) {
                                    importedReferences.push(fullPath);
                                }
                            } else if (!ObjectValidator.IsEmptyOrNull($parent)) {
                                this.referenceRegister.push(fullPath);
                                this.dependenciesImports.push(fullPath);
                            } else {
                                this.referenceRegister.push(fullPath);
                                this.sourceImports.push(fullPath);
                            }
                        } else {
                            LogIt.Debug("Path {0} already imported.", path);
                        }
                    }
                    results = regex.exec(content);
                }
                if ($option !== "source") {
                    importedReferences.forEach(($path : string) : void => {
                        this.traverseReference($path, $option, $file);
                        this.referenceRegister.push($path);
                    });
                }
            } else {
                LogIt.Error("SASS compile failed: reference path {0} not found{1}", $file,
                    ObjectValidator.IsEmptyOrNull($parent) ? "." : " imported by " + $parent + ".");
            }
        } else {
            LogIt.Debug("Parsing of reference path {0} skipped by import once mechanism.", $file);
        }
    }
}

export interface ISassConfigOptions {
    style : string;
    sourcemap : boolean;
}

export interface ISassConfigFiles {
    options : ISassConfigFiles;
    cwd : string;
    src : string[];
    dest : string;
    ext : string;
}

export interface ISassConfig {
    options : ISassConfigOptions;
    files : ISassConfigFiles[];
}

// generated-code-start
export const ISassConfigOptions = globalThis.RegisterInterface(["style", "sourcemap"]);
export const ISassConfigFiles = globalThis.RegisterInterface(["options", "cwd", "src", "dest", "ext"]);
export const ISassConfig = globalThis.RegisterInterface(["options", "files"]);
// generated-code-end
