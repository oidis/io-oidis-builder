/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import stylelint from "@nodejs/stylelint/lib/index.mjs";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class SassLint extends BaseTask {

    protected getName() : string {
        return "sass-lint";
    }

    protected async processAsync($option : string) : Promise<void> {
        const reportPath : string = this.properties.projectBase + "/build/reports/lint/sass";

        let oldSassDetected : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull(this.fileSystem.Expand(this.properties.resources + "/sass/**/definitions.d.scss"))) {
            oldSassDetected = true;
        } else {
            const os : any = require("os");
            const referenceFile : string = this.properties.projectBase + "/resource/sass/reference.d.scss";
            if (this.fileSystem.Exists(referenceFile)) {
                if (StringUtils.Contains(this.fileSystem.Read(referenceFile).toString(), os.EOL + "@import \"")) {
                    oldSassDetected = true;
                }
            }
        }
        if (oldSassDetected) {
            LogIt.Warning("Detected old @import SASS definition incompatible with current linting.\n" +
                "Linting errors will be ignored and automatic cleanup disabled.\n" +
                "For ability to enable linting migrate to @use definition or downgrade to Oidis Builder 2024.3.2.");
        }

        const patterns : string[] = [this.properties.resources + "/sass/**/*.scss"];
        if (this.programArgs.getOptions().withDependencies) {
            patterns.push(this.properties.dependenciesResources + "/sass/**/*.scss");
        }

        const files : string[] = this.fileSystem.Expand(patterns);
        if (files.length > 0) {
            if (!this.fileSystem.Exists(this.properties.projectBase + "/package.json")) {
                // This is hotfix for stylelint-scss plugin failing on projects without packege.json
                this.fileSystem.Write(this.properties.projectBase + "/package.json", "{}");
            }

            if (this.fileSystem.Exists(this.properties.projectBase + "/bin/project/configs/sasslint.conf.yml")) {
                LogIt.Warning(
                    "> sasslint.conf.yml was found in this projected, but it is no longer supported by current version of " +
                    "Oidis Builder and rules will be ignored. " +
                    "Rules must be migrated according to Stylelint definitions and stored into sasslint.conf.jsonp file.");
            }
            const config : any = await this.resolveExternConfig("sasslint");

            let passed = true;
            let results : any[] = [];
            const isAutofix : boolean = $option === "autocleanup";
            if (oldSassDetected && isAutofix) {
                return LogIt.Warning("> automatic fix of linting errors skipped.");
            }
            for await (let file of files) {
                file = this.properties.projectBase + "/" + file;
                if (this.fileSystem.IsFile(file)) {
                    try {
                        const source : string = this.fileSystem.Read(file).toString();
                        const output : any = await stylelint.lint({
                            code: source,
                            config,
                            fix : isAutofix ? "strict" : undefined
                        });
                        if (!isAutofix && output.errored) {
                            passed = false;
                        }
                        output.results.forEach(($result : any) : void => {
                            $result.source = file;
                        });
                        results = results.concat(output.results);
                        if (isAutofix && !ObjectValidator.IsEmptyOrNull(output.code)) {
                            if (source !== output.code) {
                                LogIt.Info("> fixed: " + file);
                                this.fileSystem.Write(file, output.code);
                            }
                        }
                    } catch (ex) {
                        LogIt.Error(ex);
                    }
                }
            }
            if (!isAutofix) {
                const textReporter : any = await stylelint.formatters.string;
                LogIt.Info(textReporter(results, {}));

                const checkstyle : any = require("stylelint-checkstyle-formatter");
                const report : string = StringUtils.Remove(checkstyle(results),
                    this.fileSystem.NormalizePath(this.properties.projectBase + "/", true));
                if (!this.fileSystem.Exists(reportPath)) {
                    this.fileSystem.CreateDirectory(reportPath);
                }
                this.fileSystem.Write(reportPath + "/sass.xml", report);

                if (passed) {
                    LogIt.Info(">>"[ColorType.YELLOW] + " " + files.length + " files lint free.");
                } else {
                    const message : string = "SASS lint has failed";
                    if (!this.project.lintErrorsAsWarnings && !oldSassDetected) {
                        LogIt.Error(message);
                    } else {
                        LogIt.Warning("Suppressed lint error: " + message);
                        return;
                    }
                }
            }
        } else {
            LogIt.Info(">>"[ColorType.RED] + " 0 files linted");
        }
    }
}
