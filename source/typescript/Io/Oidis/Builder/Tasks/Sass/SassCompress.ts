/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import {ColorType} from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class SassCompress extends BaseTask {

    protected getName() : string {
        return "cssmin";
    }

    protected async processAsync($option : string) : Promise<void> {
        const CleanCSS : any = require("clean-css");

        const input : any[] = [];
        this.fileSystem.Expand(this.properties.projectBase + "/" + this.properties.tmp + "/sass/**/*.css")
            .forEach(($file : string) : void => {
                input.push(this.fileSystem.Read($file));
            });
        const result : any = new CleanCSS({}).minify(Buffer.concat(input).toString());
        LogIt.Debug(">>"[ColorType.YELLOW] + " compressed from {0} to {1}",
            Convert.IntegerToSize(result.stats.originalSize), Convert.IntegerToSize(result.stats.minifiedSize));
        if (!ObjectValidator.IsEmptyOrNull(result.warnings)) {
            LogIt.Warning("WARNINGS: "[ColorType.YELLOW] + JSON.stringify(result.warnings, null, 2));
        }
        if (!ObjectValidator.IsEmptyOrNull(result.errors)) {
            LogIt.Error("ERRORS: "[ColorType.RED] + JSON.stringify(result.errors, null, 2));
        }
        this.fileSystem.Write(this.properties.projectBase + "/" + this.properties.dest +
            "/resource/css/" + this.properties.packageName + ".min.css", this.properties.licenseText + result.styles);
    }
}
