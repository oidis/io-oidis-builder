/*! ******************************************************************************************************** *
 *
 * Copyright 2021-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class SassReference extends BaseTask {

    protected getName() : string {
        return "sass-reference";
    }

    protected async processAsync($option : string) : Promise<void> {
        const os : any = require("os");
        const cwd : string = this.properties.projectBase + "/resource/sass";
        const referenceFile : string = cwd + "/reference.d.scss";

        const startTag : string = "// generated-code-start";
        const endTag : string = "// generated-code-end";

        let staticDataStart : string = "";
        let staticDataEnd : string = "";
        let data : string;
        let origContent : string = "";

        if (this.fileSystem.Exists(referenceFile)) {
            const files : string[] = this.fileSystem.Expand([cwd + "/**/*.scss", "!" + cwd + "/**/*.d.scss"]);
            data = this.fileSystem.Read(referenceFile).toString();
            origContent = data;

            let useImports : boolean = false;
            if (StringUtils.Contains(origContent, os.EOL + "@import \"")) {
                useImports = true;
                LogIt.Warning("Detected old SASS import keyword, reference file will keep it until manual migration to new @use keyword. " +
                    "This SASS definition will have disabled all linting features! " +
                    "For ability to enable linting migrate to @use definition or downgrade to Builder 2024.3.2.");
            }

            const startIndex : number = data.indexOf(startTag);
            if (startIndex !== -1) {
                staticDataStart = data.substring(0, startIndex);
                const endIndex : number = data.indexOf(endTag);
                if (endIndex !== -1) {
                    staticDataEnd = data.substring(endIndex + endTag.length + os.EOL.length);
                    if (staticDataEnd !== "") {
                        staticDataEnd = staticDataEnd.substring(0, staticDataEnd.lastIndexOf(";") + 2);
                    }
                }
            }

            let dynamicData : string = "";
            const importRegister : string[] = [];
            const useRegister : string[] = [];
            files.forEach(($file : string) : void => {
                $file = StringUtils.Remove($file, cwd + "/", ".scss");

                if (useImports) {
                    const importDefinition : string = "@import \"" + $file + "\";";
                    if (!importRegister.includes(importDefinition)) {
                        if (!StringUtils.Contains(staticDataStart, importDefinition) &&
                            !StringUtils.Contains(staticDataEnd, importDefinition)) {
                            dynamicData += os.EOL + importDefinition;
                        }
                        importRegister.push(importDefinition);
                    }
                } else {
                    const useDefinition : string = "@use \"" + $file + "\"";
                    if (!useRegister.includes(useDefinition)) {
                        if (!StringUtils.Contains(staticDataStart, useDefinition) &&
                            !StringUtils.Contains(staticDataEnd, useDefinition)) {
                            dynamicData += os.EOL + useDefinition + ";";
                        }
                        useRegister.push(useDefinition);
                    }
                }
            });

            let content : string = "";
            if (staticDataStart !== "") {
                content += staticDataStart;
            } else {
                content = origContent + os.EOL;
            }
            content += startTag +
                dynamicData + os.EOL + (!useImports ? os.EOL : "") +
                endTag + os.EOL;
            if (staticDataEnd !== "") {
                content += staticDataEnd + os.EOL;
            }

            if (origContent !== content) {
                if (dynamicData !== "") {
                    LogIt.Info("Automatically added references to file \"" + referenceFile + "\":");
                    LogIt.Info(dynamicData);
                }
                if (staticDataStart !== "" || dynamicData !== "" || staticDataEnd !== "") {
                    this.fileSystem.Write(referenceFile, content);
                }
            } else {
                LogIt.Info("Reference file \"" + referenceFile + "\" is up to date.");
            }
        } else {
            LogIt.Info("Reference file update skipped: file \"" + referenceFile + "\" does not exist");
        }
    }
}
