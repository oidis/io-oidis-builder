/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseTask } from "../../Primitives/BaseTask.js";

export class Docs extends BaseTask {

    protected getName() : string {
        return "docs";
    }

    protected async processAsync($option : string) : Promise<void> {
        const tasks : string[] = [];
        if (this.properties.projectHas.TypeScript.Source()) {
            tasks.push("typedoc-generate");
        }
        if (this.properties.projectHas.Cpp.Source()) {
            tasks.push("doxygen-generate:cpp");
        }
        if (this.properties.projectHas.Java.Source()) {
            tasks.push("doxygen-generate:java");
        }
        if (this.properties.projectHas.Python.Source()) {
            tasks.push("doxygen-generate:python");
        }
        await this.runTaskAsync(tasks.concat(["copy:license", "scr-generate", "compress:docs"]));
    }
}
