/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class TypedocGenerate extends BaseTask {

    protected getName() : string {
        return "typedoc-generate";
    }

    protected async processAsync($option : string) : Promise<void> {
        if (this.properties.projectHas.TypeScript.Source()) {
            const typedoc : any = require("typedoc");
            const app : any = new typedoc.Application();
            app.options.addReader(new typedoc.TSConfigReader());
            app.options.addReader(new typedoc.TypeDocReader());

            const tsConfigPath : string = this.properties.projectBase + "/build/compiled/tsconfig.json";
            const tsConfig : any = {
                compilerOptions: {
                    module: "amd",
                    target: "ES5",
                    lib   : ["es6", "dom", "scripthost"],
                    outDir: this.properties.projectBase + "/build/compiled/docs"
                },
                files          : this.fileSystem.Expand([
                    this.properties.projectBase + "/source/typescript/**/*.{ts,tsx}",
                    this.properties.projectBase + "/dependencies/*/source/typescript/**/*.{ts,tsx}"
                ])
            };
            this.fileSystem.Write(tsConfigPath, JSON.stringify(tsConfig, null, 2));

            app.bootstrap({
                gitRevision  : StringUtils.Remove(this.project.version, "-alpha", "-beta"),
                hideGenerator: true,
                showConfig   : true,
                entryPoints  : [tsConfig.files[1]],
                name         : this.project.docs.systemName,
                readme       : this.properties.projectBase + "/README.md",
                tsconfig     : tsConfigPath,
                logLevel     : "Verbose"
            });
            const project = app.convert();
            if (!project) {
                LogIt.Error("Failed to compile source files.");
            }
            const dest : string = this.properties.projectBase + "/build/target/docs/typescript/";
            this.fileSystem.Delete(dest);
            try {
                await app.generateDocs(project, dest);
            } catch (ex) {
                LogIt.Error(ex.message, ex);
            }
        } else {
            LogIt.Info("Skipping task typedoc-generate: No parsable TypeScript files were found.");
        }
    }
}
