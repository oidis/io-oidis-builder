/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { StringReplace } from "../Utils/StringReplace.js";

export class DoxygenGenerate extends BaseTask {

    protected getName() : string {
        return "doxygen-generate";
    }

    protected async processAsync($option : string) : Promise<void> {
        const isCpp : boolean = this.properties.projectHas.Cpp.Source() && $option === "cpp";
        const isJava : boolean = this.properties.projectHas.Java.Source() && $option === "java";
        const isPython : boolean = this.properties.projectHas.Python.Source() && $option === "python";
        if (isCpp || isJava || isPython) {
            let buildPath = this.properties.projectBase;
            if (isCpp) {
                buildPath += "/build/docs/cpp";
                this.project.docs.htmlOutput = "cpp";
            } else if (isJava) {
                buildPath += "/build/target/docs/java";
                this.project.docs.htmlOutput = "java";
            } else if (isPython) {
                buildPath += "/build/target/docs/python";
                this.project.docs.htmlOutput = "python";
            }

            const confPath : string = this.properties.projectBase + "/build/doxygen.conf";
            this.fileSystem.Delete(buildPath);
            this.fileSystem.CreateDirectory(buildPath);
            StringReplace.File(this.properties.binBase + "/resource/configs/doxygen.conf", StringReplaceType.VARIABLES, confPath);

            if ((await this.terminal.SpawnAsync("doxygen", [confPath], this.properties.projectBase)).exitCode !== 0) {
                LogIt.Error("doxygen-generate task has failed");
            }
        } else {
            LogIt.Info("Skipping task doxygen-generate: No parsable C++ or Java files were found.");
        }
    }
}
