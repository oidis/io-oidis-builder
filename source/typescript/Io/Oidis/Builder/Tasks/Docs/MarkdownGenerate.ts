/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class MarkdownGenerate extends BaseTask {

    protected getName() : string {
        return "markdown-generate";
    }

    protected async processAsync($option : string) : Promise<void> {
        if (this.properties.projectHas.Markdown()) {
            const showdown = require("showdown");
            const converter = new showdown.Converter({
                completeHTMLDocument   : false,
                omitExtraWLInCodeBlocks: true
            });
            const path : any = require("path");

            const config : any = {
                description: this.project.description,
                index      : "Home.html",
                keywords   : "",
                license    : this.project.license,
                pageTree   : {},
                title      : this.project.target.name
            };
            const confPath : string = this.properties.projectBase + "/resource/configs/markdown.conf.json";
            if (this.fileSystem.Exists(confPath)) {
                let configContent : any = this.fileSystem.Read(confPath).toString();
                if (!ObjectValidator.IsEmptyOrNull(configContent)) {
                    try {
                        configContent = JSON.parse(configContent);
                        JsonUtils.Extend(config, configContent);
                    } catch (e) {
                        LogIt.Warning("Failed to parse markdown configuration file");
                    }
                }
            }

            let pageTree : string = "";
            for (const item in config.pageTree) {
                if (config.pageTree.hasOwnProperty(item)) {
                    pageTree += "<li><a href=\"" + config.pageTree[item] + "\">" + item + "</a></li>";
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(pageTree)) {
                pageTree = "<ul>" + pageTree + "</ul>";
            }

            let index : number = 0;
            this.fileSystem.Expand(this.properties.projectBase + "/source/markdown/**/*.md")
                .forEach(($file : string) : void => {
                    if (this.fileSystem.IsFile($file)) {
                        const fileName : string = StringUtils.Remove(path.basename($file), ".md");
                        const text = this.fileSystem.Read($file).toString();
                        const pretty = require("pretty");
                        let output = pretty(
                            "<!DOCTYPE html>" +
                            "<!--\n" +
                            "\n" +
                            config.license +
                            "\n" +
                            "-->" +
                            "<html>" +
                            "<head>" +
                            "<title>" + config.title + " - " + fileName + "</title>" +
                            "\n\n" +
                            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>" +
                            "<meta name=\"description\" content=\"" + config.title + "\"/>" +
                            "<meta name=\"keywords\" content=\"" + config.keywords + "\"/>" +
                            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>" +
                            "\n\n" +
                            "<link rel=\"stylesheet\" href=\"resource/css/" + this.properties.packageName + ".min.css\" " +
                            "type=\"text/css\"/>" +
                            "<link rel=\"icon\" href=\"resource/graphics/icon.ico\"/>" +
                            "</head>" +
                            "<body>" +
                            "<div class=\"PageTree\">" +
                            "<div class=\"Home\">" +
                            "<div class=\"Icon\"></div><a href=\"" + config.index + "\">" + config.title + "</a><br>" +
                            "</div>" +
                            "<h1>PAGE TREE</h1>" +
                            pageTree +
                            "</div>" +
                            "<div class=\"Content\">" +
                            converter.makeHtml(text) +
                            "</div>" +
                            "</body>" +
                            "</html>");
                        output = StringUtils.Replace(output, "../../resource/", "resource/");
                        output = StringUtils.Replace(output, ".md\"", ".html\"");
                        const dest : string = StringUtils.Replace(
                            StringUtils.Remove($file, this.properties.projectBase + "/source/markdown/"), ".md", ".html");
                        this.fileSystem.Write(this.properties.projectBase + "/build/target/" + dest, output);
                        index++;
                    }
                });

            LogIt.Info("> generated " + index + " files");
        } else {
            LogIt.Info("Skipping task markdown-generate: No parsable Markdown files were found.");
        }
    }
}
