/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { OSType } from "../../Enums/OSType.js";
import { IProjectConfigLoadResponse } from "../../EnvironmentArgs.js";
import { ILicenseRecord, IProject } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class SCRGenerate extends BaseTask {
    private dependencies : IProject[];
    private eol : string;
    private content : string;

    protected getName() : string {
        return "scr-generate";
    }

    protected async processAsync($option : string) : Promise<void> {
        if (!this.fileSystem.Exists(this.properties.projectBase + "/SW-Content-Register.txt")) {
            const path : string = (<any>this.project.licensesRegister).filePath;
            if (!ObjectValidator.IsString(path)) {
                LogIt.Error("Detected invalid configuration for licensesRegister: " +
                    "filePath attribute is reserved only for specification of path to license register");
            }
            if (!this.project.licensesRegister.hasOwnProperty(this.project.name)) {
                LogIt.Warning("Failed to create SW-Content-Register: missing main record with name \"" + this.project.name + "\"");
            } else {
                const mainRecord : ILicenseRecord = {
                    author     : "Oidis",
                    description: this.project.description,
                    enabled    : true,
                    format     : "source code",
                    license    : "BSD-3-Clause. See LICENSE.txt",
                    location   : [],
                    optional   : false,
                    releaseName: this.project.name,
                    version    : this.project.version
                };
                const licensesRegister : ILicenseRecord[] = JsonUtils.Clone(this.project.licensesRegister);
                delete (<any>licensesRegister).filePath;
                JsonUtils.Extend(mainRecord, licensesRegister[this.project.name]);

                if (!mainRecord.enabled) {
                    LogIt.Info("Skipping task scr-generate: creation has been disabled by project configuration");
                } else {
                    const targetOs : OSType = !ObjectValidator.IsEmptyOrNull(this.build.product) ? this.build.product.OS() :
                        (EnvironmentHelper.IsWindows() ? OSType.WIN : OSType.LINUX);
                    this.eol = targetOs === OSType.WIN ? "\r\n" : "\n";
                    this.dependencies = <any>{};
                    this.content = "Release Name: " + mainRecord.releaseName + " v" + mainRecord.version + this.eol;

                    this.createRecord(mainRecord, false);
                    delete licensesRegister[this.project.name]; // eslint-disable-line @typescript-eslint/no-array-delete

                    const dependenciesPath : string[] =
                        this.fileSystem.Expand(this.properties.projectBase + "/dependencies/**/package.conf.json*");
                    for await (const path of dependenciesPath) {
                        const config : IProjectConfigLoadResponse = await Loader.getInstance().getEnvironmentArgs().LoadPackageConf(path);
                        this.dependencies[config.data.name] = config.data;
                    }
                    let recordName : string;
                    for (recordName in licensesRegister) {
                        if (licensesRegister.hasOwnProperty(recordName)) {
                            const dependency : ILicenseRecord = licensesRegister[recordName];
                            if (ObjectValidator.IsEmptyOrNull(dependency.releaseName)) {
                                dependency.releaseName = recordName;
                            }
                            this.createRecord(dependency, true);
                        }
                    }
                    this.fileSystem.Write(this.properties.projectBase + "/build/target/" + path, this.content);
                }
            }
        } else {
            LogIt.Info("Skipping task scr-generate: SW-Content-Register.txt file already exist in project root");
        }
    }

    private setDefault($record : ILicenseRecord, $item : string, $value : any) : void {
        if (!$record.hasOwnProperty($item)) {
            (this.dependencies.hasOwnProperty($record.releaseName) ? LogIt.Debug : LogIt.Warning)(
                "> " + $record.releaseName + " is missing \"" + $item + "\" " +
                "so record is using default value: " + $value);
            $record[$item] = $value;
        }
    }

    private createRecord($record : ILicenseRecord, $isDependency : boolean) : void {
        if (ObjectValidator.IsEmptyOrNull($record.enabled) || $record.enabled) {
            let version : string = "";
            if ($isDependency) {
                version = $record.version;
                if (ObjectValidator.IsEmptyOrNull(version) && this.dependencies.hasOwnProperty($record.releaseName)) {
                    version = this.dependencies[$record.releaseName].version;
                }
                if (ObjectValidator.IsEmptyOrNull(version)) {
                    version = "N/A";
                }
                if (!StringUtils.ContainsIgnoreCase(version, "N/A", ">=")) {
                    version = "v" + version;
                }
                version = " " + version;
            }
            if (this.dependencies.hasOwnProperty($record.releaseName)) {
                this.setDefault($record, "description", this.dependencies[$record.releaseName].description);
                this.setDefault($record, "author", "Oidis");
                this.setDefault($record, "license", "BSD-3-Clause. See LICENSE.txt");
                this.setDefault($record, "format", "source code");
            } else {
                this.setDefault($record, "description", "N/A");
                this.setDefault($record, "author", "N/A");
                this.setDefault($record, "license", "N/A");
                this.setDefault($record, "format", "N/A");
            }
            this.setDefault($record, "location", []);

            this.content += this.eol +
                ($record.optional ? "[OPTIONAL]" + this.eol : "") +
                $record.releaseName + version + this.eol +
                "Description: " + $record.description + this.eol +
                "Author: " + $record.author + this.eol +
                "License: " + $record.license + this.eol +
                "Format: " + $record.format + this.eol +
                "Location: ";
            if ($record.location.length === 0) {
                this.content += "N/A" + this.eol;
            } else {
                let firstItem : boolean = true;
                $record.location.forEach(($location : string) : void => {
                    if (!firstItem) {
                        this.content += "," + this.eol + "          ";
                    }
                    this.content += $location;
                    firstItem = false;
                });
                this.content += this.eol;
            }
        }
    }
}
