/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { ResourceType } from "../../Enums/ResourceType.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { IProjectSplashScreen, IProjectSplashScreenResource } from "../../Interfaces/IProject.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { ResourceHandler } from "../../Utils/ResourceHandler.js";
import { StringReplace } from "../Utils/StringReplace.js";

export class Selfextractor extends BaseTask {
    private config : IProjectSplashScreen;
    private isOffline : boolean;
    private packagePath : string;

    protected getName() : string {
        return "selfextractor";
    }

    protected async processAsync($option : string) : Promise<void> {
        const cwd : string = this.properties.projectBase + "/build";
        this.isOffline = false;

        let targetExtension : string = "";
        if (EnvironmentHelper.IsWindows()) {
            targetExtension = ".exe";
        }

        if (ObjectValidator.IsString(this.project.target.splashScreen)) {
            this.project.target.splashScreen = <IProjectSplashScreen>{
                resources: [],
                url      : StringReplace.Content(<string>this.project.target.splashScreen, StringReplaceType.VARIABLES)
            };
        }

        const url : string = (<IProjectSplashScreen>this.project.target.splashScreen).url;
        if (ObjectValidator.IsEmptyOrNull(url) || ObjectValidator.IsEmptyOrNull(require("url").parse(url).protocol)) {
            this.isOffline = true;
            this.createPackageName(cwd, targetExtension);
            let executable : string = "WUILauncher" + targetExtension;
            if (!this.fileSystem.Exists(this.properties.projectBase + "/build/target/" + executable)) {
                executable = this.project.target.name + targetExtension;
            }
            if (!this.fileSystem.Exists(cwd + "/target/" + executable)) {
                executable = this.properties.packageName + "/target/index.html";
            } else {
                executable = this.properties.packageName + "/" + executable;
            }
            if (this.fileSystem.Exists(this.properties.projectBase + "/build/target")) {
                if ((<IProjectSplashScreen>this.project.target.splashScreen).url === "") {
                    (<IProjectSplashScreen>this.project.target.splashScreen).executable = executable;
                }
                await this.create();
            } else {
                LogIt.Error("Target has to be prepared by Build task before run of this task.");
            }
        } else {
            this.createPackageName(cwd, targetExtension);
            await this.create();
        }
    }

    private createPackageName($cwd : string, $targetExtension : string) : void {
        this.packagePath = $cwd + "/";
        if (this.project.target.name === this.project.name) {
            this.packagePath += this.properties.packageName;
        } else {
            this.packagePath += this.project.target.name;
            if (this.isOffline) {
                this.packagePath += "-" + this.properties.packageVersion;
            }
        }
        this.packagePath += $targetExtension;
    }

    private async create() : Promise<void> {
        this.config = <IProjectSplashScreen>JsonUtils.Clone(this.project.target.splashScreen);

        await this.fileSystem.CopyAsync(
            this.wuiModules + "/com-wui-framework-selfextractor/win/32bit/WuiSelfExtractor.exe", this.packagePath);
        if (this.isOffline) {
            if (ObjectValidator.IsEmptyOrNull(this.config.resources)) {
                this.config.resources = [];
            }

            let archivePath : string = this.properties.projectBase + "/build/" + this.properties.packageName + ".zip";
            if (this.fileSystem.Exists(archivePath)) {
                archivePath = StringUtils.Remove(archivePath, this.properties.projectBase + "/build/");
                this.config.resources.push(<IProjectSplashScreenResource>{
                    copyOnly: false,
                    location: archivePath,
                    name    : this.createKey(archivePath),
                    type    : ResourceType.RESOURCE
                });
            } else {
                this.fileSystem.Expand(this.properties.projectBase + "/build/target/**/*")
                    .forEach(($file : string) : void => {
                        if (this.fileSystem.IsFile($file)) {
                            $file = StringUtils.Remove($file, this.properties.projectBase + "/build/target/");
                            this.config.resources.push(<IProjectSplashScreenResource>{
                                copyOnly: true,
                                location: $file,
                                name    : this.createKey($file),
                                type    : ResourceType.RESOURCE
                            });
                        }
                    });
            }

            LogIt.Info("Offline SelfExtractor: processing resources for given configuration splashScreen");
            LogIt.Debug(JSON.stringify(this.config.resources, null, 2));

            if (this.config.resources.length > 0) {
                const url : any = require("url");
                let embeddingScript : string = "";
                for await (const iResource of this.config.resources) {
                    let location : string = iResource.location;
                    if (!ObjectValidator.IsEmptyOrNull(url.parse(location).protocol)) {
                        LogIt.Error("Offline SelfExtractor can not use externally defined resources.");
                    }
                    let basePath : string = this.properties.projectBase + "/build/";
                    if (!StringUtils.Contains(location, this.properties.packageName + ".zip")) {
                        basePath += "target/";
                    }
                    if (!StringUtils.Contains(location, ".") ||
                        StringUtils.Contains(location, " ", ".ico", ".exe", ".dll")) {
                        location = StringUtils.getSha1(location) + ".bin_res";
                        location = this.properties.projectBase + "/build/compiled/bindump/" + location;
                        this.fileSystem.Write(location, this.fileSystem.Read(basePath + iResource.location));
                    } else {
                        location = basePath + location;
                    }

                    embeddingScript +=
                        "-addoverwrite \"" + this.fileSystem.NormalizePath(location) + "\", " +
                        iResource.type + "," + iResource.name + ",\n";
                }
                if (!ObjectValidator.IsEmptyOrNull(embeddingScript)) {
                    await ResourceHandler.RunScript("" +
                        "[FILENAMES]\n" +
                        "Exe=   \"" + this.packagePath + "\"\n" +
                        "SaveAs=\"" + this.packagePath + "\"\n" +
                        "[COMMANDS]\n" +
                        embeddingScript);
                }
            }
            if (ObjectValidator.IsSet(this.config.localization)) {
                await this.embedLocFile(this.config.localization);
            }
            await this.embedGuiFiles();
        } else {
            LogIt.Info("Online SelfExtractor: resources and configuration is defined externally.");
            await this.finalize();
        }
    }

    private async embedFile($resLoc : string) : Promise<void> {
        if (ObjectValidator.IsSet($resLoc) && ObjectValidator.IsEmptyOrNull(require("url").parse($resLoc).protocol) &&
            $resLoc.substr(0, 1) !== "#" && $resLoc.substr(0, 2) !== "0x") {
            const iResource : IProjectSplashScreenResource = <IProjectSplashScreenResource>{
                copyOnly: true,
                location: $resLoc,
                name    : this.createKey($resLoc),
                type    : ResourceType.RESOURCE
            };
            await ResourceHandler.EmbedFile(this.packagePath, iResource.type.toString(), iResource.name,
                this.properties.projectBase + "/" + $resLoc);
            this.config.resources.push(iResource);
        }
    }

    private async embedLocFile($resLoc : string) : Promise<void> {
        if (ObjectValidator.IsSet($resLoc) && ObjectValidator.IsEmptyOrNull(require("url").parse($resLoc).protocol)) {
            await ResourceHandler.EmbedFile(this.packagePath, "CONFIG", "LANG", this.properties.projectBase + "/" + $resLoc);
        }
    }

    private async embedGuiFiles() : Promise<void> {
        if (ObjectValidator.IsSet(this.config.background)) {
            await this.embedFile(this.config.background);
            if (ObjectValidator.IsSet(this.config.userControls) &&
                ObjectValidator.IsSet(this.config.userControls.progress)) {
                await this.embedFile(this.config.userControls.progress.foreground);
                await this.embedFile(this.config.userControls.progress.background);
                await this.finalize();
            } else {
                await this.finalize();
            }
        } else {
            await this.finalize();
        }
    }

    private async finalize() : Promise<void> {
        const configPath = this.properties.projectBase + "/build/compiled/selfextractor.config.json";
        if (!ObjectValidator.IsEmptyOrNull(this.config.installPath)) {
            this.config.installPath = StringUtils.Replace(this.config.installPath, "{", "<? @var ");
            this.config.installPath = StringUtils.Replace(this.config.installPath, "}", " ?>");
            this.config.installPath = StringReplace.Content(this.config.installPath, StringReplaceType.VARIABLES);
            LogIt.Debug("> resolving installPath to: " + this.config.installPath);
        }
        this.fileSystem.Write(configPath, JSON.stringify(this.config, null, 2));

        let iconPath : string = this.properties.projectBase + "/" + this.project.target.icon;
        if (!this.fileSystem.Exists(iconPath)) {
            iconPath = this.properties.projectBase + "/build/target/" + this.project.target.icon;
        }
        if (!this.fileSystem.Exists(iconPath) &&
            this.fileSystem.Exists(this.properties.projectBase + "/build/target/target")) {
            iconPath = this.properties.projectBase + "/build/target/target/" + this.project.target.icon;
        }
        await ResourceHandler.ModifyIcon(this.packagePath, iconPath);
        await ResourceHandler.ModifyVersionInfo(this.packagePath, this.project.target.name);
        await ResourceHandler.ModifyManifest(this.packagePath);
        await ResourceHandler.EmbedFile(this.packagePath, "CONFIG", "SELFEXTRACTOR_CONF_JSON", configPath);
        LogIt.Info("Selfextract package has been created at: " + this.packagePath);
    }

    private createKey($value : string) : string {
        $value = StringUtils.Replace($value, ".", "_");
        $value = StringUtils.Replace($value, "/", "_");
        $value = StringUtils.Replace($value, "\\", "_");
        $value = StringUtils.Replace($value, "-", "_");
        $value = StringUtils.Replace($value, " ", "_");
        return StringUtils.ToUpperCase($value);
    }
}
