/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildProductArgs } from "../../Structures/BuildProductArgs.js";
import { ResourceHandler } from "../../Utils/ResourceHandler.js";
import { BuildExecutor } from "../BuildProcessor/BuildExecutor.js";
import { StringReplace } from "../Utils/StringReplace.js";

export class WuiModulesRelease extends BaseTask {

    protected getName() : string {
        return "wui-modules-release";
    }

    protected async processAsync($option : string) : Promise<void> {
        const os : any = require("os");
        let connectorLocation : string = "resource/libs/OidisConnector";
        let launcherName : string = "WuiLauncher";
        let launcherIcon : string = "";
        let launcherType : string = "Browser";
        const remotePort : number = this.project.target.remotePort;
        const extension : string = EnvironmentHelper.IsWindows() ? ".exe" : "";

        if (this.project.target.name !== "") {
            launcherName = this.project.target.name;
        }

        switch ($option) {
        case ToolchainType.CHROMIUM_RE:
            launcherType = "WuiChromiumRE";
            break;
        case ToolchainType.IDEA:
            launcherType = "WuiIdeaRE";
            break;
        case ToolchainType.ECLIPSE:
            launcherType = "WuiEclipseRE";
            break;
        case ToolchainType.NODEJS:
            launcherType = "WuiNodejsRE";
            break;
        case "Browser":
            connectorLocation = "";
            break;
        default:
            launcherType = $option;
            $option = ToolchainType.NONE;
            break;
        }

        const runtimeTarget : string = ($option === ToolchainType.ECLIPSE || $option === ToolchainType.IDEA) ? "target/" : "";
        launcherIcon = this.fileSystem.NormalizePath(this.properties.projectBase + "/build/target/target/" +
            runtimeTarget + this.project.target.icon);

        let files : string[];
        let copyPath : string;
        let sourcePath : string;
        let connectorPath : string = "";

        if (connectorLocation !== "") {
            sourcePath = this.properties.projectBase + "/build_cache/OidisConnector/";
            const sourcePathLength : number = sourcePath.length;
            for (const file of this.fileSystem.Expand(sourcePath + "**/*")) {
                if (!this.fileSystem.IsDirectory(file) &&
                    !StringUtils.Contains(file, "/log/", ".config.jsonp", "README.md") &&
                    !StringUtils.EndsWith(file, ".config")) {
                    copyPath = this.properties.projectBase + "/build/target/" + connectorLocation + "/" +
                        file.substr(sourcePathLength, file.length);
                    if (StringUtils.Contains(copyPath, connectorLocation + "/OidisConnector")) {
                        copyPath = copyPath.replace(connectorLocation + "/OidisConnector",
                            connectorLocation + "/" + launcherName + " Connector");
                        connectorPath = copyPath;
                    }
                    this.fileSystem.Write(copyPath, this.fileSystem.Read(file));
                }
            }
        }
        if (connectorPath !== "") {
            connectorPath = this.fileSystem.NormalizePath(
                connectorPath.replace(this.properties.projectBase + "/build/target",
                    this.properties.projectBase + "/build/target/" + runtimeTarget + "target"));
        }

        files = this.fileSystem.Expand(this.properties.projectBase + "/build/target/**/*");
        if (files.length > 0) {
            for (const file of files) {
                if (!this.fileSystem.IsDirectory(file)) {
                    this.fileSystem.Write(file.replace("/build/target/", "/build/apptarget/"), this.fileSystem.Read(file));
                }
            }
            this.fileSystem.Delete(this.properties.projectBase + "/build/target/");

            sourcePath = this.properties.projectBase + "/build_cache/WuiLauncher";
            if (!this.fileSystem.Exists(sourcePath)) {
                LogIt.Error("Cannot find WuiLauncher prepared in cache \"" + sourcePath + "\"");
            }

            this.fileSystem.Write(this.properties.projectBase + "/build/target/" + runtimeTarget + launcherName + extension,
                this.fileSystem.Read(sourcePath + "/WuiLauncher" + extension));
            let appConfig : string = this.fileSystem.Read(sourcePath + "/app.config.template.jsonp").toString();
            appConfig = appConfig
                .replace("target: \"<? @var target ?>\"", "target: \"\"")
                .replace("    port: \"<? @var port ?>\"," + os.EOL, "    port: " + remotePort + "," + os.EOL)
                .replace("restart: \"<? @var restart ?>\"", "restart: false")
                .replace("runtimeEnv: \"<? @var runtime-env ?>\"", "runtimeEnv: \"" + launcherType.toLowerCase() + "\"")
                .replace("connectorName: \"<? @var connector-name ?>\"", "connectorName: \"" + launcherName + " Connector\"")
                .replace("elevateConnector: \"<? @var elevate-connector ?>\"",
                    "elevateConnector: " + this.project.target.elevate)
                .replace("singleton: \"<? @var singleton-app ?>\"", "singleton: " + this.project.target.singletonApp);
            this.fileSystem.Write(this.properties.projectBase + "/build/target/" + runtimeTarget + launcherName + ".config.jsonp",
                appConfig);

            files = this.fileSystem.Expand(this.properties.projectBase + "/build/apptarget/**/*");
            if (files.length > 0) {
                for (const file of files) {
                    if (!this.fileSystem.IsDirectory(file) &&
                        !StringUtils.Contains(file, "/log/", "debug.log", "console.log")) {
                        this.fileSystem.Write(file.replace("/build/apptarget/", "/build/target/target/" + runtimeTarget),
                            this.fileSystem.Read(file));
                    }
                }
                this.fileSystem.Delete(this.properties.projectBase + "/build/apptarget/");
            }

            const chromiumREpath : string = this.properties.projectBase + "/build/target/wuichromiumre/" + launcherName + extension;
            if ($option === ToolchainType.CHROMIUM_RE) {
                for (const file of this.fileSystem.Expand(this.properties.projectBase + "/build_cache/WuiChromiumRE/**/*")) {
                    if (!this.fileSystem.IsDirectory(file)) {
                        this.fileSystem.Write(file.replace("/build_cache/WuiChromiumRE/", "/build/target/wuichromiumre/"),
                            this.fileSystem.Read(file));
                    }
                }
                this.fileSystem.Write(chromiumREpath,
                    this.fileSystem.Read(this.properties.projectBase + "/build/target/wuichromiumre/WuiChromiumRE" + extension));
                this.fileSystem.Delete(this.properties.projectBase + "/build/target/wuichromiumre/WuiChromiumRE" + extension);
            }

            if ($option === ToolchainType.ECLIPSE || $option === ToolchainType.IDEA) {
                if ($option === ToolchainType.ECLIPSE) {
                    for (const file of this.fileSystem.Expand(this.properties.projectBase + "/build_cache/WuiEclipseRe/**/*")) {
                        if (!this.fileSystem.IsDirectory(file)) {
                            this.fileSystem.Write(file.replace("/build_cache/WuiEclipseRe/", "/build/target/"),
                                this.fileSystem.Read(file));
                        }
                    }
                    this.fileSystem.Write(this.properties.projectBase + "/build/target/META-INF/MANIFEST.MF",
                        this.fileSystem.Read(this.properties.projectBase + "/build_cache/WuiEclipseRe/META-INF/MANIFEST.MF").toString()
                            .replace(/Bundle-Name.*/g, "Bundle-Name: " + launcherName)
                            .replace(/Bundle-SymbolicName.*/g, "Bundle-SymbolicName: " + launcherName + ";singleton=true")
                            .replace(/Bundle-Version.*/g, "Bundle-Version: " + this.properties.javaProjectVersion)
                            .replace(/Bundle-License.*/g, "Bundle-License: " + this.project.license)
                    );
                    this.fileSystem.Write(this.properties.projectBase + "/build/target/plugin.xml",
                        this.fileSystem.Read(this.properties.binBase + "/resource/configs/plugin.eclipse.xml"));
                } else {
                    for (const file of this.fileSystem.Expand(this.properties.projectBase + "/build_cache/WuiIdeaRe/app/**/*")) {
                        if (!this.fileSystem.IsDirectory(file)) {
                            this.fileSystem.Write(file.replace("/build_cache/WuiIdeaRe/app/", "/build/target/"),
                                this.fileSystem.Read(file));
                        }
                    }

                    this.fileSystem.Write(this.properties.projectBase + "/build/target/META-INF/MANIFEST.MF",
                        this.fileSystem.Read(this.properties.projectBase + "/build/target/META-INF/MANIFEST.MF").toString()
                            .replace(/Specification-Title.*/g, "Specification-Title: " + launcherName)
                            .replace(/Specification-Version.*/g, "Specification-Version: " + this.properties.javaProjectVersion)
                    );
                    this.fileSystem.Write(this.properties.projectBase + "/build/target/META-INF/plugin.xml",
                        this.fileSystem.Read(this.properties.binBase + "/resource/configs/plugin.idea.xml"));
                }
                const pluginConfigPath : string = this.properties.projectBase + "/build/target/resource/configs/default.config.jsonp";
                const executor : BuildExecutor = new BuildExecutor();
                executor.getTargetProducts(this.project.target).forEach(($product : BuildProductArgs) : void => {
                    if (this.fileSystem.Exists(pluginConfigPath) && (
                        $product.Toolchain() === ToolchainType.ECLIPSE || $product.Toolchain() === ToolchainType.IDEA)) {
                        StringReplace.File(pluginConfigPath, StringReplaceType.VARIABLES);
                    }
                });
            }

            if ($option === ToolchainType.NODEJS) {
                for (const file of this.fileSystem.Expand(this.properties.projectBase + "/build_cache/WuiNodejsRE/**/*")) {
                    if (!this.fileSystem.IsDirectory(file) &&
                        !StringUtils.Contains(file, ".config.jsonp", "/resource/graphics/", "/resource/css/")) {
                        this.fileSystem.Write(file.replace("/build_cache/WuiNodejsRE/", "/build/target/wuinodejsre/"),
                            this.fileSystem.Read(file));
                    }
                }
                this.fileSystem.Write(this.properties.projectBase + "/build/target/wuinodejsre/" + launcherName + extension,
                    this.fileSystem.Read(this.properties.projectBase + "/build/target/wuinodejsre/WuiNodejsRE" + extension));
                this.fileSystem.Delete(this.properties.projectBase + "/build/target/wuinodejsre/WuiNodejsRE" + extension);
            }

            this.fileSystem.Write(this.properties.projectBase + "/build/target/" + runtimeTarget + "LICENSE.txt",
                this.fileSystem.Read(this.properties.projectBase + "/build/target/target/" + runtimeTarget + "LICENSE.txt"));
            this.fileSystem.Write(this.properties.projectBase + "/build/target/" + runtimeTarget + "SW-Content-Register.txt",
                this.fileSystem.Read(this.properties.projectBase + "/build/target/target/" +
                    runtimeTarget + "SW-Content-Register.txt"));
            this.fileSystem.Delete(this.properties.projectBase + "/build/target/target/" + runtimeTarget + "LICENSE.txt");
            this.fileSystem.Delete(this.properties.projectBase + "/build/target/target/" + runtimeTarget + "SW-Content-Register.txt");

            const targetBins : string[] = this.fileSystem.Expand(this.properties.projectBase +
                "/build/target/**/" + launcherName + extension);
            for await (const targetBin of targetBins) {
                if (launcherIcon !== "" && EnvironmentHelper.IsWindows()) {
                    await ResourceHandler.ModifyIcon(targetBin, launcherIcon);
                    await ResourceHandler.ModifyVersionInfo(targetBin, launcherName);
                }
                if (this.fileSystem.Exists(targetBin) && !EnvironmentHelper.IsWindows()) {
                    await this.terminal.ExecuteAsync("chmod", ["+x", "\"" + targetBin + "\""], null);
                }
            }
            if (this.fileSystem.Exists(connectorPath) && EnvironmentHelper.IsWindows()) {
                /// TODO: add ability to override Connector description
                await ResourceHandler.EmbedFile(connectorPath, "ICONGROUP", "0", launcherIcon);
            }
            if (this.fileSystem.Exists(chromiumREpath)) {
                let cmd : string = "";
                const args : string[] = [];
                if (EnvironmentHelper.IsLinux()) {
                    cmd = "patchelf";
                    args.push("--set-rpath");
                    args.push("'$ORIGIN'");
                } else if (EnvironmentHelper.IsMac()) {
                    cmd = "install_name_tool";
                    args.push("-add_rpath");
                    args.push("\"@executable_path\"");
                }
                args.push("\"" + chromiumREpath + "\"");
                if (!ObjectValidator.IsEmptyOrNull(cmd)) {
                    await this.terminal.ExecuteAsync(cmd, args, null);
                }
            }
        }
    }
}
