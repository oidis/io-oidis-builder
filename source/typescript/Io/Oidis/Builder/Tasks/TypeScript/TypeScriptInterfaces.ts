/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ITypeScriptConfig } from "../../Interfaces/ITypeScriptConfig.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { TypeScriptCompile } from "./TypeScriptCompile.js";

export class TypeScriptInterfaces extends BaseTask {
    private interfacesNamesList : string[];
    private interfacesMap : any;

    protected getName() : string {
        return "typescript-interfaces";
    }

    protected async processAsync($option : string) : Promise<void> {
        const os : any = require("os");
        let conf : ITypeScriptConfig = TypeScriptCompile.getConfig();

        const generated : string[] = [];
        this.interfacesNamesList = [];
        this.interfacesMap = {};

        if (this.properties.projectHas.ESModules()) {
            if ($option === "source") {
                LogIt.Warning("Force change of \"source\" config to \"module\" based on toolchain settings version");
                $option = "module";
            } else if ($option === "test") {
                LogIt.Warning("Force change of \"test\" config to \"unitModule\" based on toolchain settings version");
                $option = "unitModule";
                /// TODO: force reload back due to single unit test override -> this should be solved better
                conf = JsonUtils.Clone(conf);
                conf[$option].src = [
                    "test/unit/**/*.{ts,tsx}",
                    "!**/*.d.ts"
                ];
            }
        }
        let confBlock : string;
        for (confBlock in conf) {
            if (conf.hasOwnProperty(confBlock) && confBlock === $option) {
                if (conf[confBlock].hasOwnProperty("interface") || $option === "module" || $option === "unitModule") {
                    const interfaceFile : string = this.properties.projectBase + "/" + conf[confBlock].interface;
                    const origContent : string = this.fileSystem.Read(interfaceFile).toString();
                    const src : string[] = conf[confBlock].src.slice(0);
                    src.push("!" + this.properties.dependencies);
                    src.push("!source/typescript/**/TypeScriptInterfaces.ts");
                    const hash : string = (interfaceFile + src.join(""))
                        .replace(/\*/g, "")
                        .replace(/\./g, "")
                        .replace(/\//g, "")
                        .replace(/<%= /g, "")
                        .replace(/ %>/g, "")
                        .replace(/,/g, "");
                    if (generated.indexOf(hash) === -1) {
                        const files : string[] = this.fileSystem.Expand(src);
                        let index : number;
                        const orderedNamesList : string[] = [];
                        try {
                            for (index = 0; index < files.length; index++) {
                                let data : string = this.fileSystem.Read(this.properties.projectBase + "/" + files[index]).toString();
                                if (data.indexOf("export interface") !== -1 || data.indexOf("export type") !== -1) {
                                    data = data.replace(/(?:\/\*(?:[\s\S]*?)\*\/)|(?:\/\/(?:.*)$)/gm, "");

                                    let module : string = "";
                                    if (!this.properties.projectHas.ESModules()) {
                                        let moduleIndex : number = data.indexOf("namespace ");
                                        if (moduleIndex === -1) {
                                            moduleIndex = data.indexOf("module ");
                                            if (moduleIndex !== -1) {
                                                moduleIndex += 7;
                                            }
                                        } else {
                                            moduleIndex += 10;
                                        }
                                        if (moduleIndex !== -1) {
                                            module = data.substring(moduleIndex, data.indexOf("{")).replace(/ /gm, "");
                                        }
                                    }

                                    const interfaceBodies : any[] =
                                        data.substring(data.indexOf("export interface")).split("export interface ");
                                    for (let bodyIndex : number = 1; bodyIndex < interfaceBodies.length; bodyIndex++) {
                                        let interfaceBody : string =
                                            "export interface " +
                                            interfaceBodies[bodyIndex].substring(0, interfaceBodies[bodyIndex].indexOf("}",
                                                interfaceBodies[bodyIndex].indexOf("{") + 1) + 1);
                                        let withoutGeneric : string = "";
                                        let openTag : number = 0;
                                        for (let index : number = 0; index < interfaceBody.length; index++) {
                                            const char : string = interfaceBody[index];
                                            if (char !== "<" && char !== ">" && openTag === 0) {
                                                if (char === "{") {
                                                    withoutGeneric += interfaceBody.substring(index);
                                                    break;
                                                }
                                                withoutGeneric += char;
                                            } else if (char === "<") {
                                                openTag++;
                                            } else if (char === ">") {
                                                openTag--;
                                            }
                                        }
                                        interfaceBody = withoutGeneric;
                                        const extendsIndex : number = interfaceBody.indexOf("extends ");
                                        let interfaceName : string = "";
                                        let extending : string = "";
                                        if (extendsIndex !== -1) {
                                            extending = interfaceBody.substring(extendsIndex + 8, interfaceBody.indexOf("{"));
                                            interfaceName = interfaceBody.substring(16, extendsIndex);
                                            extending = extending.replace(/ /gm, "");
                                        } else {
                                            interfaceName = interfaceBody.substring(16, interfaceBody.indexOf("{"));
                                        }

                                        const genericIndex : number = interfaceName.indexOf("<");
                                        if (genericIndex !== -1) {
                                            interfaceName = interfaceName.substring(0, genericIndex).replace(/ /gm, "");
                                        }

                                        interfaceName = interfaceName.replace(/ /gm, "");

                                        interfaceBody = interfaceBody
                                            .substring(interfaceBody.indexOf("{") + 1, interfaceBody.lastIndexOf("}"))
                                            .replace(/\n\r/gm, "")
                                            .replace(/\r/gm, "")
                                            .replace(/\n/gm, "")
                                            .replace(/readonly /gm, "")
                                            .replace(/ /gm, "");

                                        const declarations : string[] = interfaceBody.split(";");
                                        const required : string[] = [];
                                        let declared : string = "";
                                        const declaredMap : string[] = [];
                                        let declarationIndex : number;
                                        for (declarationIndex = 0; declarationIndex < declarations.length; declarationIndex++) {
                                            let declaration : string = declarations[declarationIndex];
                                            const typeIndex : number = declaration.indexOf("<");
                                            let methodIndex : number = declaration.indexOf("(");
                                            const fieldIndex : number = declaration.indexOf(":");
                                            if (typeIndex !== -1 && typeIndex < methodIndex) {
                                                methodIndex = typeIndex;
                                            } else if (fieldIndex !== -1 && fieldIndex < methodIndex) {
                                                methodIndex = fieldIndex;
                                            }
                                            if (methodIndex !== -1) {
                                                declaration = declaration.substring(0, methodIndex);
                                            } else {
                                                declaration = declaration.substring(0, fieldIndex);
                                            }
                                            declaration = StringUtils.Remove(declaration, "?");
                                            declaration = StringUtils.Remove(declaration, ":");
                                            if (declaration !== "" && required.indexOf(declaration) === -1) {
                                                required.push(declaration);
                                                if (this.properties.projectHas.ESModules()) {
                                                    declaredMap.push(declaration);
                                                } else {
                                                    if (declared !== "") {
                                                        declared += "," + os.EOL;
                                                    }
                                                    declared += "                \"" + declaration + "\"";
                                                }
                                            }
                                        }

                                        let extendingClassName : string = extending;
                                        if (extending !== "") {
                                            if (extendingClassName.indexOf(".") === -1) {
                                                extendingClassName = module + "." + extending;
                                            }
                                            if (!this.properties.projectHas.ESModules()) {
                                                extending = ", " + extending;
                                            }
                                        }
                                        if (declared !== "") {
                                            declared += os.EOL;
                                        }

                                        let interfaceNamespace : string = "";
                                        if (!ObjectValidator.IsEmptyOrNull(module)) {
                                            interfaceNamespace = module + ".";
                                        }
                                        interfaceNamespace += interfaceName;
                                        this.interfacesNamesList.push(interfaceNamespace);
                                        let definition : any;
                                        if (this.properties.projectHas.ESModules()) {
                                            definition = {
                                                declared: JSON.stringify(declaredMap).replace(/,/g, ", "),
                                                extending
                                            };
                                        } else {
                                            definition = "" +
                                                "namespace " + module + " {" + os.EOL +
                                                "    \"use strict\";" + os.EOL +
                                                "    export let " + interfaceName + " : " +
                                                "Io.Oidis.Commons.Interfaces.Interface =" + os.EOL +
                                                "        function () : Io.Oidis.Commons.Interfaces.Interface {" + os.EOL +
                                                "            return Io.Oidis.Commons.Interfaces.Interface.getInstance([" +
                                                os.EOL +
                                                declared +
                                                "            ]" + extending + ");" + os.EOL +
                                                "        }();" + os.EOL +
                                                "}";
                                        }
                                        this.interfacesMap[interfaceNamespace] = {
                                            definition,
                                            extends: extendingClassName,
                                            module,
                                            name   : interfaceNamespace,
                                            path   : files[index]
                                        };
                                        if (extendingClassName === "") {
                                            orderedNamesList.push(interfaceNamespace);
                                        }
                                    }

                                    if (data.indexOf("export type") !== -1 && !this.properties.projectHas.ESModules()) {
                                        const typeBodies : string[] =
                                            data.substring(data.indexOf("export type")).split("export type ");
                                        for (let bodyIndex : number = 1; bodyIndex < typeBodies.length; bodyIndex++) {
                                            const body : string = typeBodies[bodyIndex];
                                            const interfaceName : string = body.substring(0, body.indexOf(" = ")).replace(/ /gm, "");
                                            const interfaceNamespace : string = module + "." + interfaceName;
                                            this.interfacesNamesList.push(interfaceNamespace);
                                            this.interfacesMap[interfaceNamespace] = {
                                                definition: "" +
                                                    "namespace " + module + " {" + os.EOL +
                                                    "    \"use strict\";" + os.EOL +
                                                    "    export let " + interfaceName +
                                                    " : Io.Oidis.Commons.Interfaces.Interface =" + os.EOL +
                                                    "        function () : Io.Oidis.Commons.Interfaces.Interface {" + os.EOL +
                                                    "            return Io.Oidis.Commons.Interfaces.Interface.getInstance([" + os.EOL +
                                                    "            ]);" + os.EOL +
                                                    "        }();" + os.EOL +
                                                    "}",
                                                extends   : "",
                                                name      : interfaceNamespace
                                            };
                                            orderedNamesList.push(interfaceNamespace);
                                        }
                                    }
                                }
                            }
                        } catch (ex) {
                            LogIt.Error(ex);
                        }

                        if (this.properties.projectHas.ESModules()) {
                            const filesContent : any = {};
                            for (const name in this.interfacesMap) {
                                if (this.interfacesMap.hasOwnProperty(name)) {
                                    const record : any = this.interfacesMap[name];
                                    if (!filesContent.hasOwnProperty(record.path)) {
                                        filesContent[record.path] = [];
                                    }
                                    filesContent[record.path].push(record);
                                }
                            }
                            for (const path in filesContent) {
                                if (filesContent.hasOwnProperty(path)) {
                                    let relativePath : string = ""; // eslint-disable-line @typescript-eslint/no-unused-vars
                                    const parts : string[] = StringUtils.Split(StringUtils.Remove(StringUtils.Remove(
                                        path, "source/typescript/"), ".ts", ".tsx"), "/");
                                    parts.forEach(($part : string, $index : number) : void => {
                                        if ($index < parts.length - 1) {
                                            relativePath += "../";
                                        }
                                    });

                                    const sourcePath : string = this.properties.projectBase + "/" + path;
                                    const data : string = this.fileSystem.Read(sourcePath).toString();
                                    const startTag : string = "// generated-code-start";
                                    const endTag : string = "// generated-code-end";

                                    let generated : string = startTag + os.EOL;
                                    let staticDataStart : string = data;
                                    let staticDataEnd : string = "";
                                    const startIndex : number = data.indexOf(startTag);
                                    let currentData : string = "";
                                    if (startIndex !== -1) {
                                        staticDataStart = data.substring(0, startIndex);
                                        const endIndex : number = data.indexOf(endTag);
                                        const endTagLength : number = endTag.length;
                                        if (endIndex !== -1) {
                                            staticDataEnd = data.substring(endIndex + endTagLength + os.EOL.length);
                                            currentData = data.substring(startIndex, endIndex);
                                        }
                                    } else {
                                        generated = os.EOL + generated;
                                    }
                                    let isTooLong : boolean = false;
                                    let interfaces : string = "";
                                    filesContent[path].forEach(($record : any) : void => {
                                        let extending : string = $record.definition.extending;
                                        if (!ObjectValidator.IsEmptyOrNull(extending)) {
                                            extending = ", <any>" + extending + "";
                                        }
                                        const variable : string = `export const ${$record.name} = globalThis.RegisterInterface(${$record.definition.declared}${extending});`;
                                        if (variable.length > 140) {
                                            isTooLong = true;
                                        }
                                        interfaces += variable + os.EOL;
                                    });
                                    if (StringUtils.Contains(currentData, "/* tslint:")) {
                                        generated +=
                                            "/* tslint:disable */" + os.EOL +
                                            interfaces +
                                            "/* tslint:enable */" + os.EOL;
                                    } else if (isTooLong) {
                                        generated +=
                                            "/* eslint-disable */" + os.EOL +
                                            interfaces +
                                            "/* eslint-enable */" + os.EOL;
                                    } else {
                                        generated += interfaces;
                                    }
                                    generated += endTag + os.EOL;
                                    if (data !== staticDataStart + generated + staticDataEnd) {
                                        LogIt.Info("> update interface at: " + sourcePath);
                                        this.fileSystem.Write(sourcePath, staticDataStart + generated + staticDataEnd);
                                    }
                                }
                            }
                        } else {
                            for (index = 0; index < this.interfacesNamesList.length; index++) {
                                const dependencies : string[] = this.getDependenciesMap(this.interfacesNamesList[index]);
                                for (const dependency of dependencies) {
                                    if (orderedNamesList.indexOf(dependency) === -1) {
                                        orderedNamesList.push(dependency);
                                    }
                                }
                            }
                            let fileContent : string = "";
                            for (index = 0; index < orderedNamesList.length; index++) {
                                fileContent += this.interfacesMap[orderedNamesList[index]].definition + os.EOL + os.EOL;
                            }
                            if (fileContent !== "") {
                                fileContent = "/** WARNING: this file has been automatically generated from typescript interfaces, " +
                                    "which exist in this package. */" + os.EOL + os.EOL +
                                    "/// <reference path=\"reference.d.ts\" />" + os.EOL +
                                    "/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */" + os.EOL +
                                    fileContent +
                                    "/* tslint:enable */" + os.EOL;
                                if (fileContent !== origContent) {
                                    LogIt.Info("Automatically added interfacesMap file to: \"" + interfaceFile + "\"");
                                    this.fileSystem.Write(interfaceFile, fileContent);
                                } else {
                                    LogIt.Info("Interfaces map file \"" + interfaceFile + "\" is up to date.");
                                }
                                generated.push(hash);
                            }
                        }
                    }
                }
            }
        }
    }

    private getDependenciesMap($interfaceName : string) : string[] {
        const extensions : string[] = [];
        this.dependenciesLookup(this.interfacesMap[$interfaceName], extensions);
        extensions.reverse();
        extensions.push($interfaceName);
        return extensions;
    }

    private dependenciesLookup($interface : ITypeScriptInterface, $extensions : string[]) : void {
        if ($interface.extends !== "" && this.interfacesNamesList.indexOf($interface.extends) !== -1) {
            $extensions.push($interface.extends);
            this.dependenciesLookup(this.interfacesMap[$interface.extends], $extensions);
        }
    }
}

export interface ITypeScriptInterface {
    extends : string;
}

// generated-code-start
export const ITypeScriptInterface = globalThis.RegisterInterface(["\"extends\""]);
// generated-code-end
