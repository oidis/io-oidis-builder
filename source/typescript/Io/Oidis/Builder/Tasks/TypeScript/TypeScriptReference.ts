/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { ITypeScriptConfig } from "../../Interfaces/ITypeScriptConfig.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { TypeScriptCompile } from "./TypeScriptCompile.js";

export class TypeScriptReference extends BaseTask {
    private namespacesList : string[];
    private namespacesMap : any;
    private orderedNamesList : string[];
    private classesNamesList : string[];
    private classesMap : any;
    private recursiveDependency : string[];
    private orderedNamespacesList : string[];

    protected getName() : string {
        return "typescript-reference";
    }

    protected async processAsync($option : string) : Promise<void> {
        const os : any = require("os");
        const conf : ITypeScriptConfig = TypeScriptCompile.getConfig();
        const startTag : string = "// " + "generated-code-start";
        const endTag : string = "// " + "generated-code-end";
        const generated : string[] = [];
        let referencePath : string;

        this.namespacesList = [];
        this.namespacesMap = {};
        this.orderedNamesList = [];
        this.classesNamesList = [];
        this.classesMap = {};
        this.recursiveDependency = [];

        if (this.properties.projectHas.ESModules()) {
            if ($option === "source" || $option === "test") {
                LogIt.Info("Reference file for source with module type is not generated");
            } else {
                LogIt.Error("Unsupported generation of reference file for \"" + $option + "\"");
            }
        } else {
            let confBlock : string;
            for (confBlock in conf) {
                if (conf.hasOwnProperty(confBlock) && confBlock === $option) {
                    if (conf[confBlock].hasOwnProperty("reference")) {
                        const referenceFile : string = conf[confBlock].reference;
                        let src : string[] = conf[confBlock].src.slice(0);
                        src.push("!" + this.properties.dependencies);
                        const hash : string = (referenceFile + src)
                            .replace(/\*/g, "")
                            .replace(/\./g, "")
                            .replace(/\//g, "")
                            .replace(/<%= /g, "")
                            .replace(/ %>/g, "")
                            .replace(/,/g, "");
                        if (generated.indexOf(hash) === -1) {
                            referencePath = referenceFile.substring(0, referenceFile.lastIndexOf("/") + 1)
                                .replace("./", "");

                            let staticDataStart : string = "";
                            let staticDataEnd : string = "";
                            let data : string;
                            let origContent : string = "";
                            if (this.fileSystem.Exists(this.properties.projectBase + "/" + referenceFile)) {
                                data = this.fileSystem.Read(this.properties.projectBase + "/" + referenceFile).toString();
                                origContent = data;
                                let startIndex : number = data.indexOf(startTag);
                                if (startIndex === -1) {
                                    startIndex = data.indexOf("//grunt-start");
                                }
                                if (startIndex !== -1) {
                                    staticDataStart = data.substring(0, startIndex);
                                    let endIndex : number = data.indexOf(endTag);
                                    let endTagLength : number = endTag.length;
                                    if (endIndex === -1) {
                                        endIndex = data.indexOf("//grunt-end");
                                        endTagLength = 11;
                                    }
                                    if (endIndex !== -1) {
                                        staticDataEnd = data.substring(endIndex + endTagLength + os.EOL.length);
                                        if (staticDataEnd !== "") {
                                            staticDataEnd = staticDataEnd.substring(0, staticDataEnd.lastIndexOf("/>") + 2);
                                        }
                                    }
                                }
                            }

                            if (typeof src === "string") {
                                src = [<any>src];
                            }
                            src.push("!" + referenceFile.replace("./", ""));
                            const files : string[] = this.fileSystem.Expand(src);
                            this.namespacesList = [];
                            this.namespacesMap = {};
                            let index : number;
                            for (index = 0; index < files.length; index++) {
                                const file : string = StringUtils.Remove(files[index], referencePath);
                                if (staticDataStart.indexOf(file) === -1 && staticDataEnd.indexOf(file) === -1) {
                                    data = this.fileSystem.Read(files[index]).toString();
                                    data = data.replace(/(?:\/\*(?:[\s\S]*?)\*\/)|(?:\/\/(?:.*)$)/gm, "");
                                    data = data.replace(/export abstract class /gm, "export class ");

                                    let module : string = "";
                                    let moduleIndex : number = data.indexOf("namespace");
                                    if (moduleIndex === -1) {
                                        moduleIndex = data.indexOf("module");
                                        if (moduleIndex !== -1) {
                                            moduleIndex += 6;
                                        }
                                    } else {
                                        moduleIndex += 9;
                                    }

                                    if (moduleIndex !== -1) {
                                        module = data.substring(moduleIndex, data.indexOf("{")).replace(/ /gm, "");
                                        if (this.namespacesList.indexOf(module) === -1) {
                                            this.namespacesList.push(module);
                                            this.namespacesMap[module] = [];
                                        }
                                        const classIndex : number = data.indexOf("export class ");
                                        if (classIndex !== -1) {
                                            const classBodies : string[] = data.substring(classIndex).split("export class ");
                                            for (let bodyIndex : number = 1; bodyIndex < classBodies.length; bodyIndex++) {
                                                const classBody : string = classBodies[bodyIndex];
                                                const importsBody : string =
                                                    data.substring(moduleIndex, data.indexOf(classBody))
                                                        .replace(/[\n\r]/gm, "");

                                                let importIndex : number = importsBody.indexOf("import ");
                                                const imports : string[] = [];
                                                while (importIndex !== -1) {
                                                    importIndex += 7;
                                                    const importName : string =
                                                        importsBody.substring(importIndex, importsBody.indexOf(";", importIndex));
                                                    imports.push(importName.substring(importName.indexOf(" = ") + 3)
                                                        .replace(/ /gm, ""));
                                                    importIndex = importsBody.indexOf("import ", importIndex);
                                                }

                                                let className : string = classBody.substring(0, classBody.indexOf(" "));
                                                const extendsIndex : number = classBody.indexOf(" extends ");
                                                let extendsName : string = "";
                                                let interfaceName : string = "";
                                                const implementsIndex : number = classBody.indexOf(" implements ", extendsIndex);
                                                if (extendsIndex !== -1) {
                                                    if (implementsIndex !== -1) {
                                                        extendsName = classBody.substring(extendsIndex + 8, implementsIndex);
                                                        interfaceName =
                                                            classBody.substring(implementsIndex + 12,
                                                                classBody.indexOf("{", extendsIndex));
                                                    } else {
                                                        extendsName =
                                                            classBody.substring(extendsIndex + 8, classBody.indexOf("{", extendsIndex));
                                                    }
                                                    extendsName = extendsName.replace(/ /gm, "");
                                                } else if (implementsIndex !== -1) {
                                                    interfaceName =
                                                        classBody.substring(implementsIndex + 12,
                                                            classBody.indexOf("{", implementsIndex));
                                                }

                                                let genericIndex : number = className.indexOf("<");
                                                if (genericIndex !== -1) {
                                                    className = className.substring(0, genericIndex);
                                                }

                                                const classNameWithNamespace : string = (module + "." + className).replace(/ /gm, "");

                                                if (extendsName !== "") {
                                                    if (extendsName.indexOf(".") === -1) {
                                                        extendsName = module + "." + extendsName;
                                                    }
                                                }

                                                if (interfaceName !== "") {
                                                    interfaceName = interfaceName.replace(/ /gm, "");
                                                    genericIndex = interfaceName.indexOf("<");
                                                    if (genericIndex !== -1) {
                                                        interfaceName = interfaceName.substring(0, genericIndex);
                                                    }
                                                    if (interfaceName.indexOf(".") === -1) {
                                                        interfaceName = module + "." + interfaceName;
                                                    }
                                                }

                                                this.namespacesMap[module].push(classNameWithNamespace);
                                                this.classesNamesList.push(classNameWithNamespace);
                                                this.classesMap[classNameWithNamespace] = <ITypeScriptClassDefinition>{
                                                    className,
                                                    data,
                                                    dependencies: [],
                                                    extends     : extendsName,
                                                    file,
                                                    imports,
                                                    interface   : interfaceName,
                                                    name        : classNameWithNamespace,
                                                    namespace   : module,
                                                    requiredBy  : []
                                                };

                                                if (extendsName === "" && imports.length === 0) {
                                                    if (this.orderedNamesList.indexOf(file) === -1) {
                                                        this.orderedNamesList.push(file);
                                                    }
                                                }
                                            }
                                        } else {
                                            let varIndex : number = data.indexOf("export var ");
                                            if (varIndex !== -1) {
                                                while (varIndex !== -1) {
                                                    varIndex += 11;
                                                    const varBody : string = data.substring(varIndex, data.indexOf(" =", varIndex));
                                                    const varName : string = varBody.substring(0, varBody.indexOf(" : "));
                                                    let varNameWithNamespace : string = varName;
                                                    if (varName.indexOf(".") === -1) {
                                                        varNameWithNamespace = module + "." + varName;
                                                    }
                                                    let varInterface : string = varBody.substring(varBody.indexOf(" : ") + 3);
                                                    if (varInterface.indexOf(".") === -1) {
                                                        varInterface = module + "." + varInterface;
                                                    }
                                                    if (this.classesNamesList.indexOf(varNameWithNamespace) === -1) {
                                                        this.namespacesMap[module].push(varNameWithNamespace);
                                                        this.classesNamesList.push(varNameWithNamespace);
                                                        this.classesMap[varNameWithNamespace] = <ITypeScriptClassDefinition>{
                                                            className   : varName,
                                                            data,
                                                            dependencies: [],
                                                            extends     : "",
                                                            file,
                                                            imports     : [],
                                                            interface   : varInterface,
                                                            name        : varNameWithNamespace,
                                                            namespace   : module,
                                                            requiredBy  : []
                                                        };
                                                    }
                                                    varIndex = data.indexOf("export var ", varIndex);
                                                }
                                            } else if (this.orderedNamesList.indexOf(file) === -1) {
                                                this.orderedNamesList.push(file);
                                            }
                                        }
                                    } else if (this.orderedNamesList.indexOf(file) === -1) {
                                        this.orderedNamesList.push(file);
                                    }
                                }
                            }

                            this.namespacesList.sort();
                            this.namespacesList = this.sortNamespace();
                            this.classesNamesList.forEach(($name) => this.collectDependencies($name));
                            this.classesNamesList.forEach(($name) => this.collectImports($name));
                            this.classesNamesList.forEach(($name) => this.mergeDependencies($name));
                            this.classesNamesList.forEach(($name) => this.getRequiredByMap($name));
                            this.namespacesList.forEach(($name) => this.getSubclassDependencies($name));
                            this.classesNamesList = this.sortClasses(this.namespacesList);

                            this.recursiveDependency = [];
                            this.getDependenciesOrder(this.classesNamesList).forEach(($name) => this.getOrderedFilesList($name));
                            if (this.recursiveDependency.length > 0) {
                                let warning : string =
                                    "WARNING: recursive dependencies have been identified. "[ColorType.RED] +
                                    "Manual reference ordering may be needed for:" + os.EOL;
                                this.recursiveDependency.forEach(($name : string) : void => {
                                    warning += "./" + referencePath + "/" + $name + os.EOL;
                                });
                                LogIt.Warning(warning);
                            }

                            const dynamicData : string = this.getDynamicReference();
                            let content : string = "";
                            if (staticDataStart !== "") {
                                content += staticDataStart;
                            }
                            content += startTag +
                                dynamicData + os.EOL +
                                endTag + os.EOL;
                            if (staticDataEnd !== "") {
                                content += staticDataEnd + os.EOL;
                            }
                            if (origContent !== content) {
                                if (dynamicData !== "") {
                                    LogIt.Info("Automatically added references to file \"" + referenceFile + "\":");
                                    LogIt.Info(dynamicData);
                                }
                                if (staticDataStart !== "" || dynamicData !== "" || staticDataEnd !== "") {
                                    this.fileSystem.Write(this.properties.projectBase + "/" + referenceFile, content);
                                }
                            } else {
                                LogIt.Info("Reference file \"" + referenceFile + "\" is up to date.");
                            }

                            generated.push(hash);
                        }
                    }
                }
            }
        }
    }

    private sortNumberArray($a : number, $b : number) : number {
        return $a - $b;
    }

    private namespaceLookup($namespace : string) : void {
        if (this.orderedNamespacesList.indexOf($namespace) === -1) {
            this.namespacesList.forEach(($subNamespace : string) : void => {
                if ($subNamespace !== $namespace && (<any>$subNamespace).startsWith($namespace)) {
                    this.namespaceLookup($subNamespace);
                }
            });
            this.orderedNamespacesList.push($namespace);
        }
    }

    private sortNamespace() : string[] {
        this.orderedNamespacesList = [];
        this.namespacesList.forEach(($namespace : string) : void => {
            this.namespaceLookup($namespace);
        });
        return this.orderedNamespacesList;
    }

    private dependenciesLookup($class : ITypeScriptClassDefinition, $dependencies : string[]) : void {
        if ($class.extends !== "" && this.classesNamesList.indexOf($class.extends) !== -1 &&
            $dependencies.indexOf(this.classesMap[$class.extends].name) === -1) {
            $dependencies.push(this.classesMap[$class.extends].name);
            this.dependenciesLookup(this.classesMap[$class.extends], $dependencies);
        }
    }

    private getDependenciesMap($className : string) : string[] {
        const dependencies : string[] = [];
        if (dependencies.indexOf(this.classesMap[$className].name) === -1) {
            this.dependenciesLookup(this.classesMap[$className], dependencies);
            dependencies.reverse();
        }
        return dependencies;
    }

    private collectDependencies($name : string) : void {
        this.classesMap[$name].dependencies = this.getDependenciesMap($name);
    }

    private collectImports($name : string) : void {
        this.classesMap[$name].imports.forEach(($import : string) : void => {
            if (this.classesNamesList.indexOf($import) === -1) {
                this.classesNamesList.forEach(($className : string) : void => {
                    if ($className !== $name && $className !== $import && $className.indexOf($import) !== -1) {
                        this.classesMap[$name].imports.push($className);
                    }
                });
            }
        });
    }

    private importsLookup($name : string, $dependencies : string[], $className : string) : void {
        if (this.classesNamesList.indexOf($name) !== -1) {
            this.classesMap[$name].imports.forEach(($import : string) : void => {
                if ($name !== $import && $dependencies.indexOf($import) === -1) {
                    if ($import !== $className) {
                        $dependencies.push($import);
                    }
                    if (this.classesNamesList.indexOf($import) !== -1) {
                        this.classesMap[$import].dependencies.forEach(($dependency : string) : void => {
                            if ($import !== $dependency && $dependencies.indexOf($dependency) === -1) {
                                if ($dependency !== $className) {
                                    $dependencies.push($dependency);
                                }
                                this.importsLookup($dependency, $dependencies, $className);
                            }
                        });
                    }
                    this.importsLookup($import, $dependencies, $className);
                }
            });
        }
    }

    private mergeDependencies($className : string) : void {
        const $dependencies : string[] = [];

        this.classesMap[$className].dependencies.forEach(($dependency : string) : void => {
            if ($className !== $dependency && $dependencies.indexOf($dependency) === -1) {
                $dependencies.push($dependency);
                this.importsLookup($dependency, $dependencies, $className);
            }
        });
        this.importsLookup(this.classesMap[$className].name, $dependencies, $className);
        this.classesMap[$className].dependencies = $dependencies;
    }

    private getRequiredByMap($name : string) : void {
        const classObject : ITypeScriptClassDefinition = this.classesMap[$name];
        this.classesNamesList.forEach(($name : string) : void => {
            if (this.classesMap[$name].dependencies.indexOf(classObject.name) !== -1) {
                classObject.requiredBy.push($name);
            }
        });
    }

    private subDependenciesLookup($classes : string[], $className : string, $namespace : string) : void {
        $classes.forEach(($name : string) : void => {
            if ($className !== $name &&
                this.classesMap[$name].requiredBy.indexOf($className) === -1) {
                if ((
                        this.classesMap[$className].data.indexOf($name + "\"") === -1 && (
                            this.classesMap[$className].data.indexOf(" " + $name) !== -1
                        )
                    ) ||
                    (
                        this.classesMap[$className].data.indexOf($namespace + this.classesMap[$name].className + "\"") === -1 && (
                            this.classesMap[$className].data.indexOf(" " + $namespace + this.classesMap[$name].className) !== -1 ||
                            this.classesMap[$className].data.indexOf("." + this.classesMap[$name].className) !== -1
                        )
                    )) {
                    this.classesMap[$name].requiredBy.push($className);
                }
            }
        });
    }

    private getSubclassDependencies($namespace : string) : void {
        const classes : string[] = this.namespacesMap[$namespace];
        if (classes.length > 0) {
            const subNamespace : string[] = [];
            this.namespacesList.forEach(($name : string) : void => {
                if ((<any>$name).startsWith($namespace + ".")) {
                    subNamespace.push($name.replace($namespace + ".", ""));
                }
            });
            classes.forEach(($className : string) : void => {
                this.subDependenciesLookup(classes, $className, "");
                subNamespace.forEach(($name : string) : void => {
                    this.subDependenciesLookup(this.namespacesMap[$namespace + "." + $name], $className, $name + ".");
                });
            });
        }
    }

    private sortClasses($namespacesList : string[]) : string[] {
        const orderedClassesNamesList : string[] = [];
        $namespacesList.forEach(($namespace : string) : void => {
            const classes : string[] = this.namespacesMap[$namespace];
            if (classes.length > 0) {
                if (classes.length === 1) {
                    orderedClassesNamesList.push(classes[0]);
                } else {
                    const levelsMap : number[] = [];
                    classes.forEach(($name : string) : void => {
                        const level : number = parseInt(this.classesMap[$name].requiredBy.length, 10);
                        if (levelsMap.indexOf(level) === -1) {
                            levelsMap.push(level);
                        }
                    });
                    levelsMap.sort(($a : number, $b : number) : number => {
                        return $a - $b;
                    });
                    levelsMap.reverse();
                    levelsMap.forEach(($level : number) : void => {
                        classes.forEach(($name : string) : void => {
                            if (this.classesMap[$name].requiredBy.length === $level) {
                                orderedClassesNamesList.push($name);
                            }
                        });
                    });
                }
            }
        });
        return orderedClassesNamesList;
    }

    private getDependenciesOrder($classesNamesList : string[]) : number[] {
        const levelsMap : number[] = [];
        $classesNamesList.forEach(($name : string) : void => {
            const level : number = this.classesMap[$name].dependencies.length;
            if (levelsMap.indexOf(level) === -1) {
                levelsMap.push(level);
            }
        });
        levelsMap.sort(this.sortNumberArray);
        return levelsMap;
    }

    private findRecursiveDependency($name : string) : void {
        this.classesMap[$name].dependencies.forEach(($dependency : string) : void => {
            if (this.classesMap[$name].requiredBy.indexOf($dependency) !== -1) {
                if (this.recursiveDependency.indexOf(this.classesMap[$name].file) === -1) {
                    this.recursiveDependency.push(this.classesMap[$name].file);
                }
                if (this.classesNamesList.indexOf($dependency) !== -1 &&
                    this.recursiveDependency.indexOf(this.classesMap[$dependency].file) === -1) {
                    this.recursiveDependency.push(this.classesMap[$dependency].file);
                }
            }
        });
    }

    private getPrependMap($className : string) : string[] {
        const prependMap : string[] = [];
        this.classesMap[$className].dependencies.forEach(($name : string) : void => {
            if (this.classesNamesList.indexOf($name) !== -1 &&
                this.orderedNamesList.indexOf(this.classesMap[$name].file) === -1 &&
                prependMap.indexOf($name) === -1) {
                prependMap.push($name);
            }
        });
        return prependMap;
    }

    private getOrderedFilesList($level : number) : void {
        let index : number;
        for (index = 0; index < this.classesNamesList.length; index++) {
            const name : string = this.classesNamesList[index];
            if (this.classesMap[name].dependencies.length === $level) {
                if (this.orderedNamesList.indexOf(this.classesMap[name].file) === -1) {
                    this.getPrependMap(name).forEach(($name : string) : void => {
                        this.findRecursiveDependency($name);
                        if (this.orderedNamesList.indexOf(this.classesMap[$name].file) === -1) {
                            this.orderedNamesList.push(this.classesMap[$name].file);
                        }
                    });
                    if (this.orderedNamesList.indexOf(this.classesMap[name].file) === -1) {
                        this.orderedNamesList.push(this.classesMap[name].file);
                    }
                }
            }
        }
    }

    private getDynamicReference() : string {
        const os : any = require("os");
        let output : string = "";
        this.orderedNamesList.forEach(($file : string) : void => {
            output += os.EOL + "/// <reference path=\"" + $file + "\" />";
        });
        return output;
    }
}

export interface ITypeScriptClassDefinition {
    name : string;
    namespace : string;
    className : string;
    "extends" : string; // eslint-disable-line @stylistic/quote-props
    "interface" : string; // eslint-disable-line @stylistic/quote-props
    imports : string[];
    dependencies : string[];
    requiredBy : string[];
    file : string;
    data : string;
}

// generated-code-start
/* eslint-disable */
export const ITypeScriptClassDefinition = globalThis.RegisterInterface(["name", "namespace", "className", "\"extends\"", "\"interface\"", "imports", "dependencies", "requiredBy", "file", "data"]);
/* eslint-enable */
// generated-code-end
