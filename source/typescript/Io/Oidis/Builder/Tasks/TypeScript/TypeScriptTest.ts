/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class TypeScriptTest extends BaseTask {
    private namespaces : string[];

    protected getName() : string {
        return "typescript-test";
    }

    protected async processAsync($option : string) : Promise<void> {
        switch ($option) {
        case "unit":
        case "selenium":
            await this.runTaskAsync("unit-test-loader:" + $option);
            break;
        case "coverage":
            await this.runTaskAsync(["unit-test-loader:" + $option, "compress:coverage"]);
            break;
        case "instrument":
            // eslint-disable-next-line no-case-declarations
            const instrument : any = require("istanbul-lib-instrument");
            // eslint-disable-next-line no-case-declarations
            const inst : any = instrument.createInstrumenter({compact: false, esModules: false, produceSourceMap: true});
            // eslint-disable-next-line no-case-declarations
            const file : string = this.properties.projectBase + "/build/target/resource/javascript/" +
                this.properties.packageName + ".min.js";
            LogIt.Info("Processing instrumentation for: " + file);

            this.namespaces = [];
            // eslint-disable-next-line no-case-declarations
            let content : string = this.fileSystem.Read(file).toString();
            // eslint-disable-next-line no-case-declarations
            const files : string[] = this.fileSystem.Expand([
                this.properties.projectBase + "/source/typescript/**/*.{ts,tsx}",
                this.properties.projectBase + "/dependencies/*/source/typescript/**/*.{ts,tsx}"
            ]);
            files.forEach(($file : string) : void => {
                const namespaces : string[] = this.getNamespaces($file);
                if (namespaces.length > 0) {
                    content = this.preprocessor(content, <any>namespaces);
                }
            });

            content = inst.instrumentSync(content, file);
            // eslint-disable-next-line no-case-declarations
            const coveradeId : string = content.substr(0, content.indexOf("\n"))
                .replace("var ", "").replace(" = function () {", "").trim();

            this.fileSystem.Write(file, "var getCoverageDataId=function(){return \"" + coveradeId + "\";};" + content);
            break;
        case "remap":
            // eslint-disable-next-line no-case-declarations
            const remapData : string = this.properties.projectBase + "/build/reports/remap-coverage.json";
            if (this.fileSystem.Exists(remapData)) {
                const buildPath : string = this.properties.projectBase + "/build/reports/coverage/typescript";
                this.fileSystem.Delete(buildPath);
                this.fileSystem.CreateDirectory(buildPath);
                if ((await this.terminal.SpawnAsync(
                    "remap-istanbul", [
                        "-i", this.properties.projectBase + "/build/reports/remap-coverage.json",
                        "-t", "html",
                        "-o", this.properties.projectBase + "/build/reports/coverage/typescript",
                        "-e", "RuntimeTests"
                    ], process.env.NODE_PATH + "/.bin")).exitCode !== 0) {
                    LogIt.Warning("typescript-coverage has failed");
                }
            } else {
                LogIt.Info("Skipping task typescript-coverage: Source map has not been found.");
            }
            break;
        default:
            LogIt.Warning("Skipping task typescript-coverage: task \"" + $option + "\" not found.");
            break;
        }
    }

    private getNamespaces($fileName : string) : string[] {
        $fileName = $fileName.substring(0, $fileName.lastIndexOf("/"))
            .replace("./source/typescript", "")
            .replace("./dependencies/", "");
        const index : number = $fileName.indexOf("/typescript");
        if (index !== -1) {
            $fileName = $fileName.substring(index + 12);
        }
        if ((<any>$fileName).startsWith("/")) {
            $fileName = $fileName.substring(1);
        }
        if ($fileName !== "" && this.namespaces.indexOf($fileName) === -1) {
            this.namespaces.push($fileName);
            return $fileName.split("/");
        }
        return [];
    }

    private preprocessor($data : any, $namespaces : string[]) : string {
        $data = $data.replace(") || this;", ") || /* istanbul ignore next */ this;");
        const replaceAll : any = ($data : string, $oldValue : string, $newValue : string) : string => {
            while ($data.indexOf($oldValue) !== -1) {
                $data = $data.replace($oldValue, $newValue);
            }
            return $data;
        };
        let index : number;
        for (index = 0; index < $namespaces.length; index++) {
            if (index === 0) {
                $data = replaceAll($data,
                    "})(" + $namespaces[index] + " || (" + $namespaces[index] + " = {}));",
                    "})(" + $namespaces[index] + " || /* istanbul ignore next */ (" + $namespaces[index] + " = {}));");
            } else {
                $data = replaceAll($data,
                    "})(" + $namespaces[index] + " = " + $namespaces[index - 1] + "." + $namespaces[index] +
                    " || (" + $namespaces[index - 1] + "." + $namespaces[index] + " = {}));",
                    "})(" + $namespaces[index] + " = " + $namespaces[index - 1] + "." + $namespaces[index] +
                    " || /* istanbul ignore next */ (" + $namespaces[index - 1] + "." + $namespaces[index] + " = {}));");
            }
        }
        return $data;
    }
}
