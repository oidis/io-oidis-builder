/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { Resources } from "../../DAO/Resources.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { IProjectDependency, IToolchainSettings } from "../../Interfaces/IProject.js";
import { IProperties } from "../../Interfaces/IProperties.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { Toolchains } from "../../Utils/Toolchains.js";
import { TsEntity } from "./Inspect/TsEntity.js";
import { TsImport } from "./Inspect/TsImport.js";
import { TsMethod, TsModifier } from "./Inspect/TsMethod.js";
import { TsNode, TsNodeType } from "./Inspect/TsNode.js";
import { TsParam } from "./Inspect/TsParam.js";

export class TypeScriptCompile extends BaseTask {
    private static config : any;
    private externApi : any[];
    private ts : any;
    private taskConfig : any;
    private taskOption : string;
    private extern : any[];
    private filterFiles : string[];
    private moduleNamespace : string;
    private modules : string[];
    private resetModules : boolean;
    private classes : any[];
    private context : any;

    public static getConfig() : any {
        if (ObjectValidator.IsEmptyOrNull(TypeScriptCompile.config)) {
            const properties : IProperties = Loader.getInstance().getAppProperties();
            /// TODO: enable all possible based on https://www.typescriptlang.org/tsconfig/#compilerOptions
            const esModulesOptions : any = {
                target                            : "ES2022",
                module                            : "ES2022",
                strict                            : false,
                esModuleInterop                   : true,
                forceConsistentCasingInFileNames  : true,
                moduleResolution                  : "Node",
                allowSyntheticDefaultImports      : true,
                strictPropertyInitialization      : false,
                strictNullChecks                  : false,
                noImplicitAny                     : false,
                noUnusedLocals                    : false,
                noUnusedParameters                : false,
                allowUnreachableCode              : true,
                allowUnusedLabels                 : true,
                alwaysStrict                      : false,
                exactOptionalPropertyTypes        : false,
                noFallthroughCasesInSwitch        : false,
                noImplicitOverride                : false,
                noImplicitReturns                 : false,
                noImplicitThis                    : false,
                noPropertyAccessFromIndexSignature: false,
                noUncheckedIndexedAccess          : false,
                strictBindCallApply               : false,
                strictBuiltinIteratorReturn       : false,
                strictFunctionTypes               : false,
                useUnknownInCatchVariables        : false
            };
            TypeScriptCompile.config = {
                source    : {
                    src         : [
                        properties.sources + "/*.{ts,tsx}", properties.dependencies + "/*.{ts,tsx}",
                        "!**/*.d.ts"
                    ],
                    dest        : properties.tmp + "/typescript/source.js",
                    reference   : "source/typescript/reference.d.ts",
                    inspectCache: "build_cache/sourceExternApi.json",
                    interface   : "source/typescript/interfacesMap.ts",
                    options     : {}
                },
                module    : {
                    src         : [
                        properties.sources + "/*.{ts,tsx}",
                        "!**/*.d.ts"
                    ],
                    dest        : "build/compiled/esmodules",
                    inspectCache: "build_cache/sourceExternApi.json",
                    options     : esModulesOptions
                },
                unitModule: {
                    src    : [
                        "test/unit/**/*.{ts,tsx}",
                        "!**/*.d.ts"
                    ],
                    dest   : "build/compiled/esmodules",
                    options: JsonUtils.Extend({
                        sourceMap : true,
                        mapRoot   : "",
                        sourceRoot: properties.projectBase
                    }, esModulesOptions)
                },
                unit      : {
                    src      : [
                        properties.sources + "/*.{ts,tsx}", properties.dependencies + "/*.{ts,tsx}",
                        "test/unit/**/*.{ts,tsx}", "bin/resource/scripts/UnitTestRunner.ts",
                        "!**/*.d.ts"
                    ],
                    dest     : "build/compiled/test/unit/typescript/source.js",
                    reference: "test/unit/typescript/reference.d.ts",
                    options  : {
                        sourceMap: true
                    }
                },
                selenium  : {
                    src      : [
                        properties.sources + "/*.{ts,tsx}", properties.dependencies + "/*.{ts,tsx}",
                        "test/selenium/**/*.{ts,tsx}", "bin/resource/scripts/UnitTestRunner.ts",
                        "!**/*.d.ts"
                    ],
                    dest     : "build/compiled/test/selenium/typescript/source.js",
                    reference: "test/selenium/reference.d.ts",
                    options  : {
                        sourceMap: true
                    }
                },
                cache     : {
                    src         : [
                        properties.dependencies + "/*.ts",
                        "!**/*.d.ts"
                    ],
                    dest        : "build_cache/dependencies.js",
                    reference   : "dependencies/*/source/typescript/reference.d.ts",
                    inspectCache: "build_cache/dependenciesExternApi.json",
                    options     : {
                        removeComments: true,
                        sourceMap     : true,
                        declaration   : true
                    }
                }
            };
        }
        return TypeScriptCompile.config;
    }

    protected getName() : string {
        return "typescript";
    }

    protected async processAsync($option : string) : Promise<void> {
        this.taskOption = $option;
        this.ts = require("typescript");

        this.taskConfig = TypeScriptCompile.getConfig();
        if (ObjectValidator.IsEmptyOrNull(this.taskConfig)) {
            LogIt.Error("typescript config is missing");
        }
        if (!ObjectValidator.IsEmptyOrNull($option) && !this.taskConfig.hasOwnProperty($option)) {
            LogIt.Error("typescript config \"" + $option + "\" has not been found");
        }

        this.extern = [];
        const tasks : string[] = [];
        let task : string;
        for (task in this.taskConfig) {
            if (this.taskConfig.hasOwnProperty(task) && (ObjectValidator.IsEmptyOrNull($option) || $option === task)) {
                tasks.push(task);
            }
        }
        for await (const taskType of tasks) {
            LogIt.Info(">>"[ColorType.YELLOW] + " Running TypeScript compile for \"" + taskType + "\"");
            let config : any = this.taskConfig[taskType];
            if (this.properties.projectHas.ESModules()) {
                if (taskType === "source") {
                    LogIt.Warning("Force change of \"source\" config to \"module\" based on toolchain settings version");
                    config = this.taskConfig.module;
                } else if (taskType === "cache") {
                    LogIt.Warning("Update of \"cache\" config suitable for toolchain settings version");
                    config.src = [];
                    config.reference = [];
                    this.properties.oidisDependencies.forEach(($value : IProjectDependency) : void => {
                        if (this.properties.esModuleDependencies.indexOf($value) === -1) {
                            config.src.push("dependencies/" + $value.name + "/source/**/*.ts");
                            config.reference.push(this.properties.projectBase + "/dependencies/" + $value.name +
                                "/source/typescript/reference.d.ts");
                        } else {
                            LogIt.Info("> not included " + $value.name);
                        }
                    });
                    config.src.push("!**/*.d.ts");
                } else if (taskType === "unit") {
                    LogIt.Warning("Force change of \"unit\" config to \"unitModule\" based on toolchain settings version");
                    config = this.taskConfig.unitModule;
                }
            }
            await this.runCompileTask(config);
        }
    }

    private async runCompileTask($config : any) : Promise<void> {
        const files : string[] = [];
        if (ObjectValidator.IsEmptyOrNull($config.reference)) {
            this.fileSystem.Expand($config.src).forEach(($file : string) : void => {
                if (StringUtils.StartsWith($file, "./")) {
                    $file = StringUtils.Replace($file, "./", this.properties.projectBase + "/");
                }
                if (!StringUtils.StartsWith($file, this.properties.projectBase)) {
                    $file = this.properties.projectBase + "/" + $file;
                }
                if (files.indexOf($file) === -1) {
                    files.push($file);
                }
            });
        } else {
            let reference : string[] = $config.reference;
            if (ObjectValidator.IsEmptyOrNull(reference)) {
                reference = [];
            } else if (ObjectValidator.IsString(reference)) {
                reference = [this.properties.projectBase + "/" + reference];
            }
            this.filterFiles = [];
            let useFilter : boolean = false;
            if (!this.fileSystem.Exists(this.properties.projectBase + "/build_cache/dependencies.d.ts") && this.taskOption !== "unit") {
                try {
                    this.getFilePaths(reference);
                    if (this.filterFiles.length > 0) {
                        useFilter = true;
                    }
                } catch (ex) {
                    LogIt.Debug(ex.stack);
                }
            }

            this.fileSystem.Expand(reference)
                .concat(this.fileSystem.Expand($config.src)).forEach(($file : string) : void => {
                if (StringUtils.StartsWith($file, "./")) {
                    $file = StringUtils.Replace($file, "./", this.properties.projectBase + "/");
                }
                if (!StringUtils.StartsWith($file, this.properties.projectBase)) {
                    $file = this.properties.projectBase + "/" + $file;
                }
                if (files.indexOf($file) === -1 && (!useFilter || this.filterFiles.indexOf($file) !== -1)) {
                    files.push($file);
                }
            });
        }

        let nodeModules : string = this.properties.projectBase + "/dependencies/nodejs/build/node_modules";
        const typeRoots : string[] = [nodeModules];
        const types : string[] = [];

        if (!this.builderConfig.noExternalTypes) {
            if (!this.fileSystem.Exists(nodeModules + "/@types")) {
                if (EnvironmentHelper.IsEmbedded()) {
                    types.push(this.properties.binBase + "/node_modules/@types/node");
                } else {
                    nodeModules = this.properties.binBase + "/resource/nodejs/node_modules";
                    if (this.fileSystem.Exists(nodeModules + "/@types")) {
                        types.push(nodeModules + "/@types/node");
                    } else {
                        types.push(this.properties.binBase + "/../../dependencies/nodejs/build/node_modules/@types/node");
                    }
                }
            } else {
                const toolchain : IToolchainSettings = JsonUtils.Extend(Toolchains.getToolchain(ToolchainType.NODEJS),
                    this.project.target.toolchainSettings);
                toolchain.types.forEach(($pattern : string) : void => {
                    this.fileSystem.Expand(nodeModules + "/" + $pattern).forEach(($type : string) : void => {
                        const root : string = StringUtils.Substring($type, 0, StringUtils.IndexOf($type, "/", false));
                        const type : string = StringUtils.Substring($type, StringUtils.IndexOf($type, "/", false) + 1);
                        if (!typeRoots.includes(root)) {
                            typeRoots.push(root);
                        }
                        if (!types.includes(type)) {
                            types.push(type);
                        }
                    });
                });
            }
        }
        const paths : any = {};
        this.properties.esModuleDependencies.forEach(($value : IProjectDependency) : void => {
            paths["@" + $value.name + "/*"] = ["dependencies/" + $value.name + "/source/typescript/*"];
            paths["@" + $value.name + "/test/unit/*"] = ["dependencies/" + $value.name + "/test/unit/typescript/*"];
        });
        const nodeAlias : string = "nodejs";
        paths["@" + nodeAlias + "/*"] = ["dependencies/nodejs/build/node_modules/*"];
        const compilerOptions : any = {
            baseUrl               : ".",
            declaration           : false,
            emitDecoratorMetadata : true,
            experimentalDecorators: true,
            jsx                   : 2,
            module                : "AMD",
            noImplicitUseStrict   : true,
            paths,
            pretty                : true,
            removeComments        : false,
            sourceMap             : false,
            target                : "ES5",
            typeRoots,
            types
        };
        Resources.Extend(compilerOptions, $config.options);
        let dest : string = $config.dest;
        if (StringUtils.StartsWith(dest, "./")) {
            dest = StringUtils.Replace(dest, "./", this.properties.projectBase + "/");
        } else {
            dest = this.properties.projectBase + "/" + dest;
        }
        if (StringUtils.EndsWith($config.dest, ".js")) {
            compilerOptions.outFile = dest;
        } else {
            compilerOptions.outDir = dest;
        }
        if (compilerOptions.strict) {
            compilerOptions.noImplicitUseStrict = false;
        }
        try {
            await this.compile(files, compilerOptions);
        } catch (ex) {
            LogIt.Error(ex);
        }
    }

    private async compile($fileNames : string[], $options : any) : Promise<void> {
        JsonUtils.Extend($options, (await this.resolveExternConfig("ts")).compilerOptions);
        this.fileSystem.Write(this.properties.projectBase + "/build/compiled/tsconfig.json", JSON.stringify({
            compilerOptions: $options
        }, null, 2));
        $options.target = this.ts.ScriptTarget[$options.target];
        $options.module = this.ts.ModuleKind[$options.module];
        let isEsmoduleCompile : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($options.moduleResolution)) {
            $options.moduleResolution = this.ts.ModuleResolutionKind[$options.moduleResolution];
            LogIt.Info("> compiling esmodules");
            isEsmoduleCompile = true;
        }
        const program : any = this.ts.createProgram($fileNames, $options);
        let emitResult : any;
        if (this.properties.projectHas.ESModules() && isEsmoduleCompile) {
            emitResult = program.emit(undefined, undefined, undefined, undefined, {
                after            : [],
                afterDeclarations: [],
                before           : [
                    ($context : any) : any => {
                        this.context = $context;
                        return ($rootNode : any) : any => {
                            return this.transformer($rootNode);
                        };
                    }
                ]
            });
        } else {
            emitResult = program.emit();
        }

        const allDiagnostics : any[] = this.ts.getPreEmitDiagnostics(program).concat(emitResult.diagnostics);

        allDiagnostics.forEach(($diagnostic : any) : void => {
            if ($diagnostic.file) {
                const report : any = $diagnostic.file.getLineAndCharacterOfPosition($diagnostic.start!);
                const message : string = this.ts.flattenDiagnosticMessageText($diagnostic.messageText, "");
                LogIt.Warning(StringUtils.Format("{0} ({1},{2}): {3}",
                    $diagnostic.file.fileName, (report.line + 1).toString(), (report.character + 1).toString(), message));
            } else {
                LogIt.Warning(this.ts.flattenDiagnosticMessageText($diagnostic.messageText, ""));
            }
        });

        const exitCode : number = emitResult.emitSkipped || allDiagnostics.length > 0 ? 1 : 0;
        if (exitCode === 0) {
            if (this.taskOption === "cache") {
                this.externApi = [];
                this.inspect(program);
            } else if (this.taskOption === "source" || this.taskOption === "unit") {
                if (this.taskOption === "source") {
                    this.externApi = this.loadExternApi("cache");
                    this.inspect(program);
                    this.fileSystem.Write(
                        this.properties.projectBase + "/build/compiled/externApi.json", JSON.stringify(this.externApi));
                }
                if (this.properties.projectHas.ESModules()) {
                    if (this.fileSystem.Exists(this.properties.projectBase + "/build/compiled/esmodules/source/")) {
                        await this.fileSystem.CopyAsync(this.properties.projectBase + "/build/compiled/esmodules",
                            this.properties.projectBase + "/build/compiled");
                    } else {
                        await this.fileSystem.CopyAsync(this.properties.projectBase + "/build/compiled/esmodules",
                            this.properties.projectBase + "/build/compiled/source/typescript/" +
                            this.properties.sourceNamespacePath);
                    }
                    await this.fileSystem.DeleteAsync(this.properties.projectBase + "/build/compiled/esmodules");
                    const rootFolder : string = this.properties.projectBase + "/build/target/resource/" +
                        "esmodules-" + this.build.timestamp;
                    const compileFolder : string = this.properties.projectBase + "/" + this.properties.compiled;
                    const sources : any[] = [];
                    this.properties.esModuleDependencies.forEach(($value : IProjectDependency) : void => {
                        sources.push({
                            dest: rootFolder,
                            src : compileFolder + "/dependencies/" + $value.name + "/source/typescript"
                        });
                        if (this.taskOption === "unit") {
                            sources.push({
                                dest: rootFolder + "/test/unit",
                                src : compileFolder + "/dependencies/" + $value.name + "/test/unit/typescript"
                            });
                        }
                    });
                    sources.push({
                        dest: rootFolder,
                        src : compileFolder + "/source/typescript"
                    });
                    if (this.taskOption === "unit") {
                        sources.push({
                            dest: rootFolder + "/test/unit",
                            src : compileFolder + "/test/unit/typescript"
                        });
                    }
                    for await (const source of sources) {
                        if (this.fileSystem.Exists(source.src)) {
                            await this.fileSystem.CopyAsync(source.src, source.dest);
                        }
                    }
                }
            }
        } else {
            LogIt.Error("TypeScript compile has failed.");
        }
    }

    private inspect($program : any) : void {
        const sources : any = $program.getSourceFiles();
        let counter = 0;
        for (const source of sources) {
            if (!StringUtils.EndsWith(source.fileName, ".d.ts") && !StringUtils.EndsWith(source.fileName, "interfacesMap.ts")) {
                const rootNode : TsNode = new TsNode("");
                if (this.properties.projectHas.ESModules()) {
                    const sourceCwd : string = "/source/typescript/";
                    const testCwd : string = "/test/unit/typescript/";
                    let namespace : string = source.fileName;
                    namespace = StringUtils.Remove(namespace, ".tsx", ".ts");
                    if (StringUtils.Contains(namespace, sourceCwd)) {
                        namespace = StringUtils.Substring(namespace, StringUtils.IndexOf(namespace, sourceCwd) + sourceCwd.length);
                    }
                    if (StringUtils.Contains(namespace, testCwd)) {
                        namespace = StringUtils.Substring(namespace, StringUtils.IndexOf(namespace, testCwd) + testCwd.length);
                    }
                    namespace = StringUtils.Replace(namespace, "/", ".").split(".").slice(0, -1).join(".");
                    rootNode.setNamespace(namespace);
                }
                this.ts.forEachChild(source, this.visit(rootNode));

                const entities : TsEntity[] = rootNode.getChildrenDeep();
                entities.forEach(($item : TsEntity) : void => {
                    if (Reflection.getInstance().IsInstanceOf($item, TsNode.ClassName())) {
                        const entity : TsNode = <TsNode>$item;
                        if (entity.getType() !== TsNodeType.MODULE) {
                            const item : any = {
                                className    : $item.getName(),
                                methods      : [],
                                namespaceName: $item.getNamespace()
                            };
                            // todo filter also classes which extends BaseConnector
                            entity.getMethodList().forEach(($method : TsMethod) : void => {
                                const decorators : string[] = $method.getDecorators();
                                if (decorators.indexOf("Extern") >= 0) {
                                    item.methods.push($method.getName());
                                }
                            });
                            if (item.methods.length > 0) {
                                this.extern.push(item);
                            }
                        }
                    }
                });
                counter++;
            }
        }

        this.saveExternApi(this.extern);

        this.externApi = this.externApi.concat(this.extern);

        LogIt.Info("TypesScript program inspection walks through " + counter + " files.");
    }

    private visit($parent : any) : ($node : any) => void {
        return ($node : any) : void => {
            switch ($node.kind) {
            case this.ts.SyntaxKind.ModuleDeclaration:
                this.ts.forEachChild($node, this.visit((<TsNode>$parent).AddChildren($node.name.text, TsNodeType.MODULE)));
                break;
            case this.ts.SyntaxKind.ModuleBlock:
                this.ts.forEachChild($node, this.visit($parent));
                break;
            case this.ts.SyntaxKind.InterfaceDeclaration:
                this.ts.forEachChild($node, this.visit((<TsNode>$parent).AddChildren($node.name.text, TsNodeType.INTERFACE)));
                break;
            case this.ts.SyntaxKind.ClassDeclaration:
                this.ts.forEachChild($node, this.visit((<TsNode>$parent).AddChildren($node.name.text, TsNodeType.CLASS)));
                break;
            case this.ts.SyntaxKind.HeritageClause:
                (<TsNode>$parent).BaseClass(StringUtils.Remove($node.getText(), "extends ", "implements "));
                break;
            case this.ts.SyntaxKind.ImportEqualsDeclaration:
                this.ts.forEachChild($node, this.visit((<TsNode>$parent).AddImport($node.name.text)));
                break;
            case this.ts.SyntaxKind.Identifier:
                this.ts.forEachChild($node, this.visit($parent));
                break;
            case this.ts.SyntaxKind.FirstNode:
                if (Reflection.getInstance().IsInstanceOf($parent, TsImport.ClassName())) {
                    (<TsImport>$parent).Target($node.getText());
                }
                break;
            case this.ts.SyntaxKind.MethodDeclaration:
                this.ts.forEachChild($node, this.visit((<TsNode>$parent).AddMethod($node.name.text)));
                break;
            case this.ts.SyntaxKind.ProtectedKeyword:
                if (Reflection.getInstance().IsInstanceOf($parent, TsMethod.ClassName())) {
                    (<TsMethod>$parent).Modifier(TsModifier.PROTECTED);
                }
                break;
            case this.ts.SyntaxKind.PublicKeyword:
                if (Reflection.getInstance().IsInstanceOf($parent, TsMethod.ClassName())) {
                    (<TsMethod>$parent).Modifier(TsModifier.PUBLIC);
                }
                break;
            case this.ts.SyntaxKind.PrivateKeyword:
                if (Reflection.getInstance().IsInstanceOf($parent, TsMethod.ClassName())) {
                    (<TsMethod>$parent).Modifier(TsModifier.PRIVATE);
                }
                break;
            case this.ts.SyntaxKind.Decorator:
                if (Reflection.getInstance().IsInstanceOf($parent, TsMethod.ClassName())) {
                    const match : any = /@(\w+)\(/g.exec($node.getText());
                    if (!ObjectValidator.IsEmptyOrNull(match)) {
                        (<TsMethod>$parent).AddDecorator(match[1]);
                    }
                }
                break;
            case this.ts.SyntaxKind.Parameter:
                if (Reflection.getInstance().IsInstanceOf($parent, TsMethod.ClassName())) {
                    this.ts.forEachChild($node, this.visit((<TsMethod>$parent).AddParam($node.name.text)));
                }
                break;
            case this.ts.SyntaxKind.TypeReference:
                if (Reflection.getInstance().IsInstanceOf($parent, TsParam.ClassName())) {
                    (<TsParam>$parent).Type($node.getText());
                }
                break;
            case this.ts.SyntaxKind.QuestionToken:
                if (Reflection.getInstance().IsInstanceOf($parent, TsParam.ClassName())) {
                    (<TsParam>$parent).Optional(true);
                }
                break;
            default:
                // uncomment for debug purposes
                // console.log(
                //     "skip-kind: " + ts.SyntaxKind[$node.kind] + "; " +
                //     "name: " + (ObjectValidator.IsEmptyOrNull($node.name) ? "---" : $node.name.text) + ";" +
                //     "parent: " + $parent.getName() + "; " +
                //     "text: " + $node.getText());
            }
        };
    }

    private visitor($node : any) : any {
        if ($node.kind === this.ts.SyntaxKind.ModuleDeclaration) {
            if (this.resetModules) {
                this.modules = [];
                this.resetModules = false;
            }
            this.modules.push($node.name.text);
        }
        if ($node.kind === this.ts.SyntaxKind.ClassDeclaration ||
            $node.kind === this.ts.SyntaxKind.InterfaceDeclaration) {
            this.resetModules = true;
            let isExported : boolean = false;
            if (!ObjectValidator.IsEmptyOrNull($node.modifiers)) {
                $node.modifiers.forEach(($modifier : any) : void => {
                    if ($modifier.kind === this.ts.SyntaxKind.ExportKeyword) {
                        isExported = true;
                    }
                });
            }
            if (isExported) {
                let thisNamespace : string = this.moduleNamespace;
                let thisName : string = $node.name.text;
                if (!ObjectValidator.IsEmptyOrNull(this.modules)) {
                    thisNamespace = this.modules.join(".");
                    thisName = thisNamespace + "." + thisName;
                }
                thisNamespace += "." + $node.name.text;
                this.classes.push({namespace: thisNamespace, name: thisName});
            }
        }
        return this.ts.visitEachChild($node, ($node : any) : any => {
            return this.visitor($node);
        }, this.context);
    }

    private transformer($rootNode : any) : any {
        const {factory} = this.context;
        const register : any[] = [];
        this.classes = [];
        const sourceCwd : string = "/source/typescript/";
        const testCwd : string = "/test/unit/typescript/";

        this.moduleNamespace = $rootNode.fileName;
        this.moduleNamespace = StringUtils.Remove(this.moduleNamespace, ".tsx", ".ts");
        if (StringUtils.Contains(this.moduleNamespace, sourceCwd)) {
            this.moduleNamespace = StringUtils.Substring(this.moduleNamespace,
                StringUtils.IndexOf(this.moduleNamespace, sourceCwd) + sourceCwd.length);
        }
        if (StringUtils.Contains(this.moduleNamespace, testCwd)) {
            this.moduleNamespace = StringUtils.Substring(this.moduleNamespace,
                StringUtils.IndexOf(this.moduleNamespace, testCwd) + testCwd.length);
        }
        this.moduleNamespace = StringUtils.Replace(this.moduleNamespace, "/", ".").split(".").slice(0, -1).join(".");

        this.modules = [];
        this.resetModules = false;

        this.ts.visitNode($rootNode, ($node : any) : any => {
            return this.visitor($node);
        });

        let containsLoaderClass : boolean = false;
        this.classes.forEach(($class : any) : void => {
            if ($class.namespace === this.project.loaderClass ||
                $class.namespace === this.project.guiLoaderClass) {
                containsLoaderClass = true;
            }
            register.push(this.createStatement(factory, $class.namespace, $class.name));
        });
        const statemens : any[] = [];
        /// TODO: this should be part of compiler options which should be possible to be defined by target project
        if (this.programArgs.IsDebug()) {
            statemens.push(this.createDebugStatement(factory, "> load: " + $rootNode.fileName));
        }
        if (containsLoaderClass) {
            LogIt.Debug("> injection of reflection");
            if (this.properties.esModuleDependencies.find(($value) => $value.name === "io-oidis-commons")) {
                statemens.push(this.createImportStatement(factory, "@" + "io-oidis-commons/" +
                    "Io/Oidis/Commons/Utils/ReflectionEmitter.js"));
                statemens.push(this.createImportStatement(factory, "@" + "io-oidis-commons/" +
                    "Io/Oidis/Commons/Interfaces/Interface.js"));
            }
        }
        return factory.updateSourceFile($rootNode, statemens.concat($rootNode.statements.concat(register)));
    }

    private loadExternApi($profile : string) : any[] {
        let retVal : any[] = [];
        const data : string = this.fileSystem.Read(
            this.properties.projectBase + "/" + this.taskConfig[$profile].inspectCache).toString();
        if (!ObjectValidator.IsEmptyOrNull(data)) {
            retVal = JSON.parse(data);
        }
        return retVal;
    }

    private saveExternApi($data : any[]) : void {
        this.fileSystem.Write(this.properties.projectBase + "/" + this.taskConfig[this.taskOption].inspectCache, JSON.stringify($data));
    }

    private getFilePaths($reference : string | string[]) : void {
        const path : any = require("path");

        this.fileSystem.Expand($reference).forEach(($file : string) : void => {
            this.filterFiles.push($file);
            const directory : string = path.dirname($file);
            const content : string = this.fileSystem.Read($file).toString();
            // eslint-disable-next-line no-useless-escape
            const expression : RegExp = /\/\/\/ <reference path=\"(.*?)\" \/>/gm;
            let match : RegExpExecArray = expression.exec(content);
            while (!ObjectValidator.IsEmptyOrNull(match)) {
                if (match.length === 2) {
                    const pathRecord : string = directory + "/" + match[1];
                    if (StringUtils.EndsWith(pathRecord, ".d.ts")) {
                        this.getFilePaths(pathRecord);
                    } else if (this.filterFiles.indexOf(pathRecord) === -1) {
                        this.filterFiles.push(pathRecord);
                    }
                }
                match = expression.exec(content);
            }
        });
    }

    private createStatement($factory : any, $namespace : string, $class : string) : any {
        return $factory.createExpressionStatement(
            $factory.createCallExpression($factory.createIdentifier("globalThis.RegisterClass"), undefined, [
                $factory.createStringLiteral($namespace),
                $factory.createIdentifier($class)
            ]));
    }

    private createDebugStatement($factory : any, $message : string) : any {
        return $factory.createExpressionStatement(
            $factory.createCallExpression($factory.createIdentifier("console.debug"), undefined, [
                $factory.createStringLiteral($message)
            ]));
    }

    private createImportStatement($factory : any, $path : string) : any {
        return $factory.createImportDeclaration(undefined, undefined, $factory.createStringLiteral($path));
    }
}
