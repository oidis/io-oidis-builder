/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { TsEntity } from "./TsEntity.js";

export class TsParam extends TsEntity {
    private type : string;
    private optional : boolean;

    public Type($type : string) : string {
        return this.type = Property.String(this.type, $type);
    }

    public Optional($isOptional : boolean) : boolean {
        return this.optional = Property.Boolean(this.optional, $isOptional);
    }
}
