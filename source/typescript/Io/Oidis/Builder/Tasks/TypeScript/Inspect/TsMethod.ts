/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { TsEntity } from "./TsEntity.js";
import { TsParam } from "./TsParam.js";

export class TsMethod extends TsEntity {
    private modifier : TsModifier;
    private readonly parameters : TsParam[];
    private readonly decorators : string[];

    constructor($name : string, $parent : TsEntity) {
        super($name, $parent);
        this.parameters = [];
        this.decorators = [];
        this.modifier = TsModifier.PUBLIC;
    }

    public Modifier($value? : TsModifier) : TsModifier {
        return this.modifier = Property.EnumType(this.modifier, $value, TsModifier, TsModifier.PRIVATE);
    }

    public AddParam($name : string) : TsParam {
        let index : number = -1;
        this.parameters.forEach(($value : TsParam, $index : number) : void => {
            if ($value.getName() === $name) {
                index = $index;
            }
        });
        if (index === -1) {
            const node = new TsParam($name, this);
            this.parameters.push(node);
            return node;
        } else {
            return this.parameters[index];
        }
    }

    public getParams() : TsParam[] {
        return this.parameters;
    }

    public AddDecorator($name : string) : void {
        if (this.decorators.indexOf($name) === -1) {
            this.decorators.push($name);
        }
    }

    public getDecorators() : string[] {
        return this.decorators;
    }
}

export enum TsModifier {
    PUBLIC,
    PROTECTED,
    PRIVATE
}
