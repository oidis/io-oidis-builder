/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseEnum } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseEnum.js";
import { Property } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Property.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { TsEntity } from "./TsEntity.js";
import { TsImport } from "./TsImport.js";
import { TsMethod } from "./TsMethod.js";

export class TsNode extends TsEntity {
    private readonly children : TsEntity[];
    private readonly imports : TsImport[];
    private readonly methods : TsMethod[];
    private readonly type : TsNodeType;
    private baseClass : string;
    private interface : string;
    private esNamespace : string;

    constructor($name : string, $parent? : TsEntity, $type? : TsNodeType) {
        super($name, $parent);
        this.children = [];
        this.imports = [];
        this.methods = [];
        this.type = $type;
        this.esNamespace = "";
    }

    public setNamespace($value : string) : void {
        this.esNamespace = Property.String(this.esNamespace, $value);
    }

    public getESNamespace() : string {
        return this.esNamespace;
    }

    public getType() : TsNodeType {
        return this.type;
    }

    public BaseClass($value? : string) : string {
        return this.baseClass = Property.String(this.baseClass, $value);
    }

    public Interface($value? : string) : string {
        return this.interface = Property.String(this.baseClass, $value);
    }

    public AddChildren($name : string, $type : TsNodeType) : TsEntity {
        let index : number = -1;
        this.children.forEach(($value : TsNode, $index : number) : void => {
            if ($value.getName() === $name) {
                index = $index;
            }
        });
        if (index === -1) {
            const node = new TsNode($name, this, $type);
            this.children.push(node);
            return node;
        } else {
            return this.children[index];
        }
    }

    public getChildren() : TsEntity[] {
        return this.children;
    }

    public getChildrenDeep() : TsEntity[] {
        let allChilds : TsEntity[] = [];
        let bufferChilds : TsEntity[] = [];
        allChilds = allChilds.concat(this.getChildren());
        allChilds.forEach(($item : TsEntity) : void => {
            if (Reflection.getInstance().IsInstanceOf($item, TsNode.ClassName())) {
                const node : TsNode = <TsNode>$item;
                bufferChilds = bufferChilds.concat(node.getChildrenDeep());
            }
        });
        return allChilds.concat(bufferChilds);
    }

    public AddImport($symbol) : TsImport {
        let index : number = -1;
        this.imports.forEach(($value : TsImport, $index : number) : void => {
            if ($value.getName() === $symbol) {
                index = $index;
            }
        });
        if (index === -1) {
            const node = new TsImport($symbol, this);
            this.imports.push(node);
            return node;
        } else {
            return this.imports[index];
        }
    }

    public getImports() : TsImport[] {
        return this.imports;
    }

    public AddMethod($name : string) : TsMethod {
        let index : number = -1;
        this.methods.forEach(($value : TsMethod, $index : number) : void => {
            if ($value.getName() === $name) {
                index = $index;
            }
        });
        if (index === -1) {
            const node = new TsMethod($name, this);
            this.methods.push(node);
            return node;
        } else {
            return this.methods[index];
        }
    }

    public getMethodList() : TsMethod[] {
        return this.methods;
    }
}

export class TsNodeType extends BaseEnum {
    public static readonly CLASS : string = "CLASS";
    public static readonly INTERFACE : string = "INTERFACE";
    public static readonly MODULE : string = "MODULE";
}
