/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseObject } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/BaseObject.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";

export class TsEntity extends BaseObject {
    private readonly name : string;
    private readonly parent : TsEntity;

    constructor($name : string, $parent : TsEntity) {
        super();
        this.name = $name;
        this.parent = $parent;
    }

    public getName() : string {
        return this.name;
    }

    public getParent() : TsEntity {
        return this.parent;
    }

    public getNamespace() : string {
        const parent : any = this.getParent();
        if (ObjectValidator.IsSet(parent.getESNamespace) && !ObjectValidator.IsEmptyOrNull(parent.getESNamespace())) {
            return parent.getESNamespace();
        }
        const nsEntities : string[] = [];
        const walkParent : any = ($parent : TsEntity) : void => {
            if (!ObjectValidator.IsEmptyOrNull($parent) && ($parent.getName() !== "")) {
                nsEntities.unshift($parent.getName());
                walkParent($parent.getParent());
            }
        };
        walkParent(this.getParent());
        return nsEntities.join(".");
    }
}
