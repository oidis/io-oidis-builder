/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { INamespaceMapping } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IProject.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileSystemHandler } from "../../Connectors/FileSystemHandler.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class TypeScriptMappings extends BaseTask {

    protected getName() : string {
        return "typescript-mappings";
    }

    protected async processAsync($option : string) : Promise<void> {
        const fileSystem : FileSystemHandler = new FileSystemHandler();
        const os : any = require("os");
        const fallbacksPath : string = this.properties.projectBase + "/source/typescript/namespaceMap.ts";
        if (!ObjectValidator.IsEmptyOrNull(this.project.namespaceMappings)) {
            const mappings : INamespaceMapping[] = (<any>Reflection).flattenNamespaceMappings(this.project.namespaceMappings);

            let isMappingValid : boolean = true;
            mappings.forEach(($mapping : INamespaceMapping) : boolean => {
                let isParentExist : boolean = false;
                this.project.namespaces.forEach(($namespace : string) : boolean => {
                    if (StringUtils.StartsWith($namespace, $mapping.namespace)) {
                        LogIt.Error("Invalid namespace mapping detected: \"" + $mapping + "\". " +
                            "Mapping's namespace mustn't be parent or equal to any of namespaces set " +
                            "either inside package.conf.json, or private.conf.json configuration.");
                        isMappingValid = false;
                    } else if (StringUtils.StartsWith($mapping.namespace, $namespace)) {
                        isParentExist = true;
                    }
                    return isMappingValid;
                });
                if (!isParentExist) {
                    LogIt.Error("Invalid namespace mapping detected: \"" + $mapping + "\". " +
                        "Mapping's namespace must contain namespace set inside namespace property " +
                        "either inside package.conf.json, or private.conf.json configuration.");
                    isMappingValid = false;
                }
                return isMappingValid;
            });

            if (isMappingValid) {
                let fileContent : string =
                    "/** WARNING: this file has been automatically generated from namespaceMappings property inside " +
                    "package.conf.json, or private.conf.json. */" + os.EOL;

                const mergedMappings : INamespaceMapping[] = <any>{};
                mappings.forEach(($mapping : INamespaceMapping) : void => {
                    const namespaceParts : string[] = $mapping.namespace.split(".");
                    const namespaceVariable : string = namespaceParts.pop();
                    const namespacePath : string = namespaceParts.join(".");
                    if (ObjectValidator.IsEmptyOrNull(mergedMappings[namespacePath])) {
                        mergedMappings[namespacePath] = [];
                    }
                    mergedMappings[namespacePath].push({mappedTo: $mapping.mappedTo, namespace: $mapping.namespace, namespaceVariable});
                });

                Object.keys(mergedMappings).forEach(($namespace : string) : void => {
                    mergedMappings[$namespace].forEach(($mapping : any) : void => {
                        fileContent += os.EOL +
                            "namespace " + $mapping.mappedTo + " {" + os.EOL +
                            "    \"use strict\";" + os.EOL +
                            "    export const namespaceMapping = \"" + $mapping.namespace + "\";" + os.EOL +
                            "}" + os.EOL;
                    });
                });

                Object.keys(mergedMappings).forEach(($namespace : string) : void => {
                    fileContent += os.EOL +
                        "namespace " + $namespace + " {" + os.EOL +
                        "    \"use strict\";" + os.EOL;
                    mergedMappings[$namespace].forEach(($mapping : any) : void => {
                        fileContent += "    export const " + $mapping.namespaceVariable + " = " + $mapping.mappedTo + ";" + os.EOL;
                    });
                    fileContent += "}" + os.EOL;
                });

                this.fileSystem.Write(fallbacksPath, fileContent);
                LogIt.Info("Automatically added namespaceMap file to: \"" + fallbacksPath + "\"");
            } else {
                fileSystem.Delete(fallbacksPath);
            }
        } else {
            if (fileSystem.Exists(fallbacksPath)) {
                LogIt.Info("No namespace mappings detected, deleting namespaceMap file.");
                fileSystem.Delete(fallbacksPath);
            }
        }
    }
}
