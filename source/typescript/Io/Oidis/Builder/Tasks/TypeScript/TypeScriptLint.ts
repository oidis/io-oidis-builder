/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { IProperties } from "../../Interfaces/IProperties.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class TypeScriptLint extends BaseTask {
    private static config : any;

    public static getConfig() : any {
        if (ObjectValidator.IsEmptyOrNull(TypeScriptLint.config)) {
            const properties : IProperties = Loader.getInstance().getAppProperties();
            TypeScriptLint.config = {
                options: {
                    fix: false
                },
                project: [properties.sources + "/*.{ts,tsx}", "test/**/*.{ts,tsx}", "!**/*.d.ts"]
            };
            if (Loader.getInstance().getProgramArgs().getOptions().withDependencies) {
                TypeScriptLint.config.project.push(properties.dependencies + "/*.{ts,tsx}");
            }
        }
        return TypeScriptLint.config;
    }

    protected getName() : string {
        return "tslint";
    }

    protected async processAsync($option : string) : Promise<void> {
        const reportPath : string = this.properties.projectBase + "/build/reports/lint/typescript";
        const {ESLint} = require("eslint");
        const eslint = require("@eslint/js");
        const tseslint = require("typescript-eslint");
        const checkstyle = require("eslint-formatter-checkstyle");
        const jsdoc = require("eslint-plugin-jsdoc");
        const stylistic = require("@stylistic/eslint-plugin");

        const taskConfig : any = TypeScriptLint.getConfig();
        if (ObjectValidator.IsEmptyOrNull(taskConfig)) {
            LogIt.Error("tslint config is missing");
        }
        const lintConfig : any = await this.resolveExternConfig("tslint");
        const lintOptions : any = {
            fix      : false,
            force    : false,
            formatter: "stylish"
        };

        if (taskConfig.hasOwnProperty("options")) {
            if (taskConfig.options.fix) {
                lintOptions.fix = true;
            }
            if (taskConfig.options.force) {
                lintOptions.force = true;
            }
            if (!ObjectValidator.IsEmptyOrNull(taskConfig.options.formatter)) {
                lintOptions.formatter = taskConfig.options.formatter;
            }
        }

        let src : string[] = [];
        if ($option) {
            if (taskConfig.hasOwnProperty($option)) {
                src = src.concat(taskConfig[$option]);
            } else {
                LogIt.Error("tslint config \"" + $option + "\" has not been found");
            }
        } else {
            let type : string;
            for (type in taskConfig) {
                if (taskConfig.hasOwnProperty(type) && type !== "options") {
                    src = src.concat(taskConfig[type]);
                }
            }
        }
        const configs : any = [
            eslint.configs.recommended,
            tseslint.configs.recommendedTypeChecked,
            tseslint.configs.stylisticTypeChecked,
            jsdoc.configs["flat/recommended-typescript"],
            stylistic.configs["recommended-flat"],
            lintConfig,
            {
                languageOptions: {
                    parserOptions: {
                        projectService : true,
                        tsconfigRootDir: this.properties.projectBase
                    }
                }
            }
        ];
        if (ObjectValidator.IsSet(lintConfig.overrides)) {
            const overrides : any[] = JsonUtils.Clone(lintConfig.overrides);
            delete lintConfig.overrides;
            if (!ObjectValidator.IsEmptyOrNull(overrides)) {
                configs.push(...overrides);
            }
        }

        const linter : any = new ESLint({
            fix               : lintOptions.fix,
            overrideConfig    : tseslint.config(...configs),
            overrideConfigFile: true
        });

        const files : string[] = this.fileSystem.Expand(src);
        const result = await linter.lintFiles(files);

        let errorCount = 0;
        let oldDetected : boolean = false;
        result.forEach(($report : any) : void => {
            errorCount += $report.errorCount;
            if (!oldDetected && StringUtils.Contains(this.fileSystem.Read($report.filePath).toString(), "/* tslint:disable")) {
                oldDetected = true;
            }
        });

        if (oldDetected) {
            LogIt.Warning("Detected old TS Lint definition incompatible with current linting.\n" +
                "Linting errors will be ignored and automatic cleanup disabled.\n" +
                "For ability to enable linting migrate to ES Lint definition or downgrade to Oidis Builder 2024.3.2.");
        } else if (lintOptions.fix) {
            await ESLint.outputFixes(result);
        }
        if (!this.fileSystem.Exists(reportPath)) {
            this.fileSystem.CreateDirectory(reportPath);
        }
        this.fileSystem.Write(reportPath + "/tslint.xml", StringUtils.Remove(checkstyle(result),
            this.fileSystem.NormalizePath(this.properties.projectBase + "/", true)));

        const formatter = await linter.loadFormatter(lintOptions.formatter);
        LogIt.Info(formatter.format(result));

        if (errorCount > 0) {
            const message = "Not all of TypeScript files are lint free.";
            if (!this.project.lintErrorsAsWarnings && !oldDetected) {
                LogIt.Error(message);
            } else {
                LogIt.Warning("Suppressed lint error: " + message);
                return;
            }
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " " + files.length + " files lint free.");
        }
    }
}
