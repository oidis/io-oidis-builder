/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { StringReplace } from "../Utils/StringReplace.js";

export class CopyScripts extends BaseTask {
    private static config : any;

    public static getConfig() : any {
        if (ObjectValidator.IsEmptyOrNull(CopyScripts.config)) {
            CopyScripts.config = {
                typescript: [
                    "resource/scripts/UnitTestEnvironment.d.ts",
                    "resource/scripts/UnitTestRunner.ts"
                ],
                java      : [
                    "resource/scripts/pom.xml",
                    "resource/configs/javalibrary.config.xml",
                    "resource/configs/MANIFEST.MF",
                    "resource/configs/checkstyle-rules.xml",
                    "resource/configs/log4j2.xml",
                    "resource/configs/log4j2-test.xml",
                    "resource/scripts/UnitTest.java",
                    "resource/scripts/UnitTestRunner.java",
                    "resource/scripts/UnitTestListener.java",
                    "resource/scripts/Assert.java"
                ],
                eclipse   : [
                    "resource/configs/MANIFEST.BUNDLE.MF",
                    "resource/configs/plugin.eclipse.xml",
                    "resource/configs/fragment.e4xmi"
                ],
                idea      : [
                    "resource/configs/plugin.idea.xml"
                ],
                cpp       : [
                    "resource/scripts/UnitTestLoader.cpp",
                    "resource/scripts/UnitTestRunner.cpp",
                    "resource/scripts/UnitTestRunner.hpp",
                    "resource/scripts/boost_install.js",
                    "xcpp_scripts/**/*"
                ],
                cmake     : [
                    "resource/scripts/CMakeLists.txt",
                    "resource/scripts/boost_configure.cmake",
                    "resource/scripts/*-toolchain.cmake"
                ],
                phonegap  : [
                    "resource/configs/phonegap.config.xml"
                ],
                python    : [
                    "resource/configs/setup.py",
                    "resource/configs/MANIFEST.in",
                    "resource/scripts/Assert.py",
                    "resource/scripts/UnitTestLoader.py",
                    "resource/scripts/UnitTestRunner.py"
                ]
            };
        }
        return CopyScripts.config;
    }

    protected getName() : string {
        return "copy-scripts";
    }

    protected async processAsync($option : string) : Promise<void> {
        if (this.properties.projectBase !== "..") {
            const copyScript : any = CopyScripts.getConfig();

            if (copyScript.hasOwnProperty($option)) {
                let patterns : string[] = [];
                copyScript[$option].forEach(($pattern : string) : void => {
                    patterns.push(this.properties.binBase + "/" + $pattern);
                });
                let files : string[] = this.fileSystem.Expand(patterns);
                const getPlatformFiles : any = ($platform : string) : void => {
                    if (copyScript.hasOwnProperty($platform) && copyScript[$platform].hasOwnProperty($option)) {
                        patterns = [];
                        copyScript[$platform][$option].forEach(($pattern : string) : void => {
                            patterns.push(this.properties.binBase + "/" + $pattern);
                        });
                        files = files.concat(this.fileSystem.Expand(patterns));
                    }
                };
                if (EnvironmentHelper.IsWindows()) {
                    getPlatformFiles("windowsOnly");
                } else if (EnvironmentHelper.IsMac()) {
                    getPlatformFiles("macOnly");
                } else {
                    getPlatformFiles("linuxOnly");
                }

                const binBase : string = this.properties.binBase + "/";
                for (let file of files) {
                    file = StringUtils.Substring(file, StringUtils.IndexOf(file, binBase) + binBase.length);
                    const sourceFile : string = binBase + file;
                    const destFile : string = this.properties.projectBase + "/bin/" + file;
                    StringReplace.File(sourceFile, StringReplaceType.FOR_EACH, destFile);
                    StringReplace.File(destFile, StringReplaceType.VARIABLES);
                }
                for (let file of files) {
                    file = StringUtils.Substring(file, StringUtils.IndexOf(file, binBase) + binBase.length);
                    StringReplace.File(this.properties.projectBase + "/bin/" + file, StringReplaceType.IMPORTS_FINISH);
                }
                LogIt.Info("Scripts have been copied");
            } else {
                LogIt.Info("Copy script task \"" + $option + "\" does not exist");
            }
        } else {
            LogIt.Info("Scripts copy skipped: running from local instance of Oidis Builder");
        }
    }
}
