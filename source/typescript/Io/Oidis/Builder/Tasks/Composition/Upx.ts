/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class Upx extends BaseTask {

    protected getName() : string {
        return "upx";
    }

    protected async processAsync() : Promise<void> {
        if (this.project.target.upx.enabled) {
            let executables : string[] = this.fileSystem.Expand(this.project.target.upx.files);
            if (ObjectValidator.IsEmptyOrNull(executables)) {
                executables = this.fileSystem.Expand(this.properties.projectBase + "/build/*.{exe,dll,node}");
            }
            if (ObjectValidator.IsEmptyOrNull(executables)) {
                executables = this.fileSystem.Expand(this.properties.projectBase + "/build/target/**/*.{exe,dll,node}");
            }

            for await (const executable of executables) {
                await this.pack(executable);
            }
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " Executable pack skipped: target configuration is missing enabled upx");
        }
    }

    private async pack($executablePath : string) : Promise<void> {
        if (this.fileSystem.Exists($executablePath)) {
            if (EnvironmentHelper.IsWindows()) {
                const options : string[] = <string[]>JsonUtils.Clone(this.project.target.upx.options);
                options.push("-o\"" + $executablePath + ".pack" + "\"");
                options.push("\"" + $executablePath + "\"");
                const res : IExecuteResult = await this.terminal.SpawnAsync("upx.exe", options, null);
                if (res.exitCode !== 0) {
                    if (res.exitCode === 2) {
                        LogIt.Warning(">>"[ColorType.YELLOW] + " Executable is already packed.");
                    } else {
                        LogIt.Error("Upx has failed");
                    }
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " Executable has been packed successfully");
                    await this.fileSystem.DeleteAsync($executablePath);
                    this.fileSystem.Rename($executablePath + ".pack", $executablePath);
                }
            } else if (EnvironmentHelper.IsMac()) {
                LogIt.Warning(">>"[ColorType.YELLOW] + " Upx is not currently supported on MAC");
            } else {
                LogIt.Warning(">>"[ColorType.YELLOW] + " Upx is not currently supported on LINUX");
            }
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " Upx skipped: executable for pack does not exit");
        }
    }
}
