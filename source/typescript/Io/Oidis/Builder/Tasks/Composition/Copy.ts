/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class Copy extends BaseTask {

    protected getName() : string {
        return "copy";
    }

    protected async processAsync($option : string) : Promise<void> {
        const path : any = require("path");
        const config : any = {
            configs: [
                "build/compiled/resource/configs/wuirunner.config.jsonp",
                "resource/configs/robots.txt"
            ],
            html   : [
                this.properties.tmp + "/html/*.html",
                "!**/noscript.html"
            ],
            license: [
                "LICENSE.txt",
                "SW-Content-Register.txt"
            ]
        };
        if (config.hasOwnProperty($option)) {
            const pretty : any = require("pretty");
            const read : any = ($file : string) : any => {
                const data : any = this.fileSystem.Read($file);
                if ($option !== "html") {
                    return data;
                }
                return pretty(data.toString());
            };
            this.fileSystem.Expand(config[$option]).forEach(($file : string) : void => {
                this.fileSystem.Write(this.properties.projectBase + "/" + this.properties.dest + "/" + path.basename($file),
                    read($file));
            });
        } else {
            LogIt.Error("Unsupported copy option \"" + $option + "\".");
        }
    }
}
