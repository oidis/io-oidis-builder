/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { TimeoutManager } from "@io-oidis-commons/Io/Oidis/Commons/Events/TimeoutManager.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IDownloadResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/FileSystemHandler.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { IFileSystemDownloadOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";
import { IProjectConfigLoadResponse } from "../../EnvironmentArgs.js";
import { IDependenciesDownloadConfig } from "../../Interfaces/IAppConfiguration.js";
import {
    IProjectDependency,
    IProjectDependencyLocation,
    IProjectDependencyLocationArch,
    IProjectDependencyLocationPath
} from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildExecutor } from "../BuildProcessor/BuildExecutor.js";
import { GitManager, IGitExecuteResult } from "../Utils/GitManager.js";

export class DependenciesDownload extends BaseTask {
    private static isRunning : boolean;
    private processed : any;
    private registered : any;
    private downloaded : number;
    private buildExecutor : BuildExecutor;
    private config : IDependenciesDownloadConfig;
    private readonly packageLock : any;
    private lockFilePath : string;
    private lockDetected : boolean;

    public static getInstance() : DependenciesDownload {
        const instance : DependenciesDownload = new DependenciesDownload();
        DependenciesDownload.getInstance = () : DependenciesDownload => {
            return instance;
        };
        return instance;
    }

    public static Rollback() : void {
        if (DependenciesDownload.isRunning) {
            LogIt.Warning("Dependencies download failure detected. " +
                "Dependencies will be deleted to protect builder execution failures in next run.");
            Loader.getInstance().getFileSystemHandler().Delete(Loader.getInstance().getAppProperties().projectBase + "/dependencies");
        }
    }

    constructor() {
        super();

        this.buildExecutor = new BuildExecutor();
        this.processed = {};
        this.registered = {};
        this.downloaded = 0;
        this.config = Loader.getInstance().getAppConfiguration().dependenciesDownloadConfig;
        this.packageLock = {};
        this.lockFilePath = this.properties.projectBase + "/package.lock.json";
        this.lockDetected = false;
    }

    public ParseLocation($source : IProjectDependencyLocation | string | string[]) : IProjectDependencyLocation[] {
        const path : any = require("path");
        let output : IProjectDependencyLocation[] = [];

        if (ObjectValidator.IsString($source)) {
            let isRemote : boolean = true;
            let source : string = <string>$source;
            output[0] = <IProjectDependencyLocation>{
                arch       : "64",
                branch     : "",
                headers    : {},
                path       : "",
                platform   : "",
                releaseName: "",
                type       : ""
            };
            output[0].branch = "master";
            output[0].type = "git";
            if (StringUtils.StartsWith(source, "$")) {
                const index : string[] = StringUtils.Split(source, "$").filter(($value : string) : boolean => {
                    return !ObjectValidator.IsEmptyOrNull($value);
                });
                output[0].platform = index[0];
                output[0].arch = index[1];
                source = index[2];
            }

            if (StringUtils.StartsWith(source, "dir+")) {
                output[0].type = "dir";
                isRemote = false;
            } else if (StringUtils.StartsWith(source, "mvn+")) {
                output[0].type = "mvn";
            } else if (StringUtils.StartsWith(source, "file://")) {
                output[0].type = "url";
            }
            source = StringUtils.Remove(source, "git+", "dir+", "mvn+");

            const branchTag : number = StringUtils.IndexOf(<string>source, "#");
            if (branchTag !== -1) {
                output[0].branch = StringUtils.Substring(<string>source, branchTag + 1);
                source = StringUtils.Substring(<string>source, 0, branchTag);
            }
            if (!isRemote) {
                source = StringUtils.Replace(<string>source, "\\", "/");
                source = StringUtils.Replace(<string>source, "/", path.sep);
            }
            output[0].path = source;
        } else if (ObjectValidator.IsArray($source) && (<string[]>$source).length > 0 && ObjectValidator.IsString($source[0])) {
            for (const singleSource of <string[]>$source) {
                output = output.concat(this.ParseLocation(singleSource));
            }
        } else if ((<IProjectDependencyLocation>$source).type === "cdi") {
            output.push(<IProjectDependencyLocation>$source);
        } else {
            const multiLoc : IProjectDependencyLocation[] =
                this.ParseLocation(this.ResolveDependencyPath((<IProjectDependencyLocation>$source).path));

            for (const singleLoc of multiLoc) {
                if ($source.hasOwnProperty("type") && (singleLoc.type === "git")) {
                    singleLoc.type = (<IProjectDependencyLocation>$source).type;
                }
                if ($source.hasOwnProperty("branch") && (singleLoc.type === "master")) {
                    singleLoc.branch = (<IProjectDependencyLocation>$source).branch;
                }
                if ($source.hasOwnProperty("releaseName")) {
                    singleLoc.releaseName = (<IProjectDependencyLocation>$source).releaseName;
                }
                if ($source.hasOwnProperty("platform")) {
                    singleLoc.platform = (<IProjectDependencyLocation>$source).platform;
                }
                if ($source.hasOwnProperty("headers")) {
                    singleLoc.headers = (<IProjectDependencyLocation>$source).headers;
                }
            }
            output = output.concat(multiLoc);
        }
        return output;
    }

    public getSource($dependencyName : string, $source : IProjectDependencyLocation | string, $dependency : IProjectDependency,
                     $targetPath : string, $callback : () => void) : void {
        this.getSourceAsync($dependencyName, $source, $dependency, $targetPath).then($callback);
    }

    public ResolveDependencyPath($path : string | IProjectDependencyLocationPath) : string[] {
        let path : string[] = [];
        if (ObjectValidator.IsString($path)) {
            path = [<string>$path];
        } else {
            for (const product of this.buildExecutor.getTargetProducts()) {
                let pathWithArch : string | IProjectDependencyLocationArch = "";

                if ((<IProjectDependencyLocationPath>$path).hasOwnProperty(product.OS().toString())) {
                    pathWithArch = (<IProjectDependencyLocationPath>$path)[product.OS().toString()];

                    if (!ObjectValidator.IsEmptyOrNull(pathWithArch)) {
                        if (ObjectValidator.IsString(pathWithArch)) {
                            pathWithArch = <string>pathWithArch;
                        } else {
                            if ((<IProjectDependencyLocationArch>pathWithArch).hasOwnProperty("x" + product.Arch().toString())) {
                                pathWithArch = (<IProjectDependencyLocationArch>pathWithArch)["x" + product.Arch().toString()];
                            } else {
                                LogIt.Error("Dependency location path contains wrong definition of architecture.");
                            }
                        }
                    }
                    // todo this could be replaced by change interface to IProj...Path[] and proper loop in location parser
                    path.push("$" + product.OS() + "$" + product.Arch() + "$" + <string>pathWithArch);
                }

                if (ObjectValidator.IsEmptyOrNull(pathWithArch)) {
                    LogIt.Error("Unmet dependency path based on builder platform and architecture. " +
                        "Specify platform specific paths for win, linux and mac as location.path attribute. " +
                        "Optionally can be specified x86 or x64 architecture.");
                }
            }
        }
        return path;
    }

    protected async processSingleSource($dependencyName : string, $source : IProjectDependencyLocation, $dependency : IProjectDependency,
                                        $targetPath : string) : Promise<void> {
        if (!ObjectValidator.IsString($dependency)) {
            const depLock : IProjectDependency = JsonUtils.Clone($dependency);
            if (this.lockDetected) {
                JsonUtils.Extend($source, depLock.location);
            } else {
                JsonUtils.Extend(depLock.location, $source);
            }
            this.packageLock[$dependencyName] = depLock;
        } else {
            this.packageLock[$dependencyName] = {location: $source};
        }

        const dependencyKey : string = $dependencyName + ":" + $targetPath;
        if (this.processed.hasOwnProperty(dependencyKey) || $source.type === "mvn") {
            // do nothing
        } else {
            this.fileSystem.Delete($targetPath);
            if (StringUtils.StartsWith(<string>$source.path, "file+")) {
                $source.type = "file";
                $source.path = StringUtils.Remove(<string>$source.path, "file+");
            }
            if (StringUtils.StartsWith(<string>$source.path, "git+")) {
                $source.type = "git";
                $source.path = StringUtils.Remove(<string>$source.path, "git+");
            }
            if (StringUtils.StartsWith(<string>$source.path, "dir+")) {
                $source.type = "dir";
                $source.path = StringUtils.Remove(<string>$source.path, "dir+");
            }
            if ($source.type === "git" || $source.type === "dir" && this.fileSystem.Exists($source.path + "/.git")) {
                const res : IRepoCloneResult = await this.clone(<string>$source.path, $source.branch, $source.sha, $targetPath);
                $source.sha = res.sha;
                this.downloaded++;
                this.processed[dependencyKey] = res.branch;
                if (!this.lockDetected) {
                    const config : IProjectConfigLoadResponse = await Loader.getInstance().getEnvironmentArgs()
                        .LoadPackageConf(res.targetPath + "/package.conf.jsonp");
                    let dependencies : IProjectDependency[] = null;
                    if (config.data.hasOwnProperty("dependencies")) {
                        dependencies = config.data.dependencies;
                    }
                    if (dependencies !== null) {
                        await this.processDependencies(dependencies);
                    }
                }
            } else if ($source.type === "oidiscloud") {
                let projectName : string = (<IProjectDependency>$dependency).name;
                if (ObjectValidator.IsEmptyOrNull(projectName)) {
                    projectName = $dependencyName;
                }
                if (ObjectValidator.IsEmptyOrNull($source.releaseName)) {
                    $source.releaseName = "null";
                }
                if (ObjectValidator.IsEmptyOrNull($source.platform)) {
                    $source.platform = "null";
                }
                let downloadUrl : string = <string>$source.path + "/Update/" +
                    projectName + "/" + $source.releaseName + "/" + $source.platform;
                if (!ObjectValidator.IsEmptyOrNull((<IProjectDependency>$dependency).version) &&
                    (<IProjectDependency>$dependency).version !== "latest") {
                    downloadUrl += "/" + (<IProjectDependency>$dependency).version;
                }
                await this.download(downloadUrl, $targetPath);
                this.downloaded++;
                this.processed[dependencyKey] = (<IProjectDependency>$dependency).location;
            } else if ($source.type === "cdi") {
                for await (const files of $source.files) {
                    await this.download({
                        headers: $source.headers,
                        url    : files.src
                    }, $targetPath + "/" + files.dest, false);
                }
                this.downloaded++;
                this.processed[dependencyKey] = (<IProjectDependency>$dependency).location;
            } else if ($source.type !== "mvn") {
                if (!StringUtils.StartsWith(<any>$source.path, "file://") &&
                    (this.fileSystem.IsDirectory((<any>$source).path) || this.fileSystem.IsFile((<any>$source).path))) {
                    $source.path = "file:///" + $source.path;
                }
                await this.download({
                    headers: $source.headers,
                    url    : $source.path.toString()
                }, $targetPath);
                this.downloaded++;
                this.processed[dependencyKey] = (<IProjectDependency>$dependency).location;
            }
        }
    }

    protected isPlatformMatch($platform : string) : boolean {
        let output : boolean = false;

        for (const product of this.buildExecutor.getTargetProducts()) {
            if (product.OS().toString() === $platform) {
                output = true;
                break;
            }
        }

        return output;
    }

    protected async clone($repoSource : string, $branch : string, $sha : string, $targetPath : string) : Promise<IRepoCloneResult> {
        const isRemote : boolean = StringUtils.Contains($repoSource, "http://", "https://", "ssh://", "git://");

        if (isRemote) {
            if (ObjectValidator.IsEmptyOrNull($sha)) {
                LogIt.Info("Clone of dependency \"" + $repoSource + "\" to \"" + $targetPath + "\".");
            } else {
                LogIt.Info("Clone of dependency \"" + $repoSource + "\" " +
                    "on branch \"" + $branch + "\" (sha \"" + $sha + "\") to \"" + $targetPath + "\".");
            }
        } else {
            LogIt.Info("WARNING:"[ColorType.YELLOW] + " Clone of dependency \"" + $repoSource + "\" " +
                "to \"" + $targetPath + "\" from local repository.");
        }

        let cwd : string = this.fileSystem.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() +
            "/downloads/" + StringUtils.getSha1($targetPath);
        let archiveSource : string = $repoSource;
        if (isRemote) {
            cwd += ".git";
            archiveSource = cwd;
            this.fileSystem.Delete(cwd);
        }

        if (isRemote) {
            try {
                if (ObjectValidator.IsEmptyOrNull($sha)) {
                    await GitManager.ExecuteAsync(["clone", "--depth=1", "--branch", $branch, $repoSource, "\"" + cwd + "\""]);
                } else {
                    await GitManager.ExecuteAsync(["clone", "--depth=1", $repoSource, "\"" + cwd + "\""]);
                    await GitManager.ExecuteAsync(["fetch", "--depth=1", "origin", $sha], {repoPath: cwd});
                    await GitManager.ExecuteAsync(["checkout", $sha], {repoPath: cwd});
                }
                if (!EnvironmentHelper.IsWindows()) {
                    await GitManager.ExecuteAsync(["lfs", "install"], {repoPath: cwd});
                    if (ObjectValidator.IsEmptyOrNull($sha)) {
                        await GitManager.ExecuteAsync(["lfs", "fetch", "origin", $branch], {repoPath: cwd});
                    } else {
                        await GitManager.ExecuteAsync(["lfs", "fetch", "origin", $sha], {repoPath: cwd});
                        await GitManager.ExecuteAsync(["lfs", "checkout", $sha], {repoPath: cwd});
                    }
                }
            } catch (ex) {
                this.fileSystem.Delete(cwd);
                throw new Error("Dependency clone has failed.");
            }
        }
        const path : any = require("path");
        const outputName : string = path.basename($targetPath).replace(/\//gi, "_") + ".zip";
        const outputPath : string = StringUtils.Replace(
            StringUtils.Replace(archiveSource, "\\", "/") + "/" + outputName, "/", path.sep);
        if (!this.fileSystem.Exists(archiveSource)) {
            LogIt.Error("Dependency local path \"" + archiveSource + "\" does not exist.");
        }
        try {
            await GitManager.ExecuteAsync([
                "archive", "--format", "zip", "-0", "--output", "\"" + outputName + "\"",
                !ObjectValidator.IsEmptyOrNull($sha) ? "HEAD" : $branch
            ], {
                repoPath: archiveSource
            });
            const rev : IGitExecuteResult = await GitManager.ExecuteAsync(["rev-parse", "HEAD"], {repoPath: archiveSource, verbose: false});

            await this.fileSystem.UnpackAsync(outputPath, {output: $targetPath, autoStrip: false});
            if (this.fileSystem.Exists($targetPath)) {
                this.fileSystem.Delete(isRemote ? cwd : outputPath);
                return {
                    branch    : $branch,
                    repoSource: $repoSource,
                    sha       : StringUtils.Remove(rev.stdout, "\r\n", "\n", "\r"),
                    targetPath: $targetPath
                };
            } else {
                LogIt.Error("Dependency unpack has failed.");
            }
        } catch (ex) {
            this.fileSystem.Delete(isRemote ? cwd : outputPath);
            throw new Error("Dependency clone has failed.");
        }
    }

    protected async download($location : string | IFileSystemDownloadOptions, $targetPath : string,
                             $unpack : boolean = true) : Promise<void> {
        const source : string = ObjectValidator.IsString($location) ? "\"" + $location + "\"" : "from online source";
        LogIt.Info("Download of dependency " + source + " to \"" + $targetPath + "\".");
        const res : IDownloadResult = await this.fileSystem.DownloadAsync($location);
        if (this.fileSystem.Exists(res.bodyOrPath)) {
            if (this.fileSystem.IsDirectory(res.bodyOrPath)) {
                try {
                    await this.fileSystem.CopyAsync(res.bodyOrPath, $targetPath);
                    this.fileSystem.Delete(res.bodyOrPath);
                } catch (ex) {
                    LogIt.Error("Dependency copy has failed.");
                }
            } else {
                if ($unpack) {
                    await this.fileSystem.UnpackAsync(res.bodyOrPath, {output: $targetPath});
                } else {
                    await this.fileSystem.CopyAsync(res.bodyOrPath, $targetPath);
                }
                if (this.fileSystem.Exists($targetPath)) {
                    this.fileSystem.Delete(res.bodyOrPath);
                } else {
                    LogIt.Error("Dependency unpack has failed.");
                }
            }
        }
    }

    protected async processDependency($dependencyName : string, $dependency : IProjectDependency | string) : Promise<void> {
        const targetPath : string = this.properties.projectBase + "/dependencies/" + $dependencyName;

        if (ObjectValidator.IsString($dependency) && !ObjectValidator.IsEmptyOrNull($dependency)) {
            await this.getSourceAsync($dependencyName, <string>$dependency, <any>$dependency, targetPath);
        } else if ($dependency.hasOwnProperty("groupId")) {
            // do nothing
        } else if (ObjectValidator.IsObject($dependency)) {
            if ($dependency.hasOwnProperty("location")) {
                const projectDependency : IProjectDependency = <IProjectDependency>$dependency;
                if (ObjectValidator.IsString(projectDependency.location) &&
                    !ObjectValidator.IsEmptyOrNull(projectDependency.location)) {
                    await this.getSourceAsync($dependencyName, projectDependency.location, <IProjectDependency>$dependency, targetPath);
                } else if (ObjectValidator.IsObject(projectDependency.location)) {
                    const dependencyLocation : IProjectDependencyLocation =
                        <IProjectDependencyLocation>projectDependency.location;
                    if (projectDependency.location.hasOwnProperty("path")) {
                        const branch : string = projectDependency.location.hasOwnProperty("branch") ?
                            "#" + dependencyLocation.branch : "";
                        switch (dependencyLocation.type) {
                        case "dir":
                            if (this.fileSystem.Exists(<string>dependencyLocation.path)) {
                                if (this.fileSystem.Exists(dependencyLocation.path + "/.git")) {
                                    await this.getSourceAsync(
                                        $dependencyName,
                                        "dir+" + dependencyLocation.path + branch,
                                        <IProjectDependency>$dependency, targetPath);
                                } else {
                                    LogIt.Info("WARNING:"[ColorType.YELLOW] +
                                        " Dependency \"" + $dependencyName + "\" loaded from local path \"" +
                                        dependencyLocation.path + "\".");
                                }
                            } else {
                                LogIt.Error("Dependency \"" + $dependencyName + "\" not exists at local path \"" +
                                    dependencyLocation.path + "\".");
                            }
                            break;
                        case "git":
                            const path : string = dependencyLocation.path + branch; // eslint-disable-line no-case-declarations
                            if (StringUtils.Contains(path, "http://", "https://", "ssh://", "git://")) {
                                await this.getSourceAsync($dependencyName, "git+" + path, <IProjectDependency>$dependency, targetPath);
                            } else {
                                await this.getSourceAsync($dependencyName, "dir+" + path, <IProjectDependency>$dependency, targetPath);
                            }
                            break;
                        case "mvn":
                            // do nothing
                            break;
                        default:
                            await this.getSourceAsync($dependencyName, projectDependency.location, <IProjectDependency>$dependency,
                                targetPath);
                            break;
                        }
                    } else if (dependencyLocation.type === "cdi") {
                        await this.getSourceAsync($dependencyName, projectDependency.location, <IProjectDependency>$dependency,
                            targetPath);
                    } else {
                        LogIt.Error("Location path is not specified for dependency \"" + $dependencyName + "\".");
                    }
                }
            } else {
                LogIt.Warning("Location is not specified for dependency \"" + $dependencyName +
                    "\". Nothing going to be prepared for next install phase.\nContinue in processing of others...");
            }
        } else if (ObjectValidator.IsEmptyOrNull($dependency)) {
            LogIt.Warning("Download of dependency \"" + $dependencyName + "\" skipped: source has not been defined");
        } else {
            LogIt.Error("Unsupported dependency configuration type: supported are only string or JSON");
        }
    }

    protected collectDependencies($dependencies : IProjectDependency[]) : string[] {
        const dependenciesList : string[] = [];
        let dependencyName : string;
        for (dependencyName in $dependencies) {
            if ($dependencies.hasOwnProperty(dependencyName)) {
                let registeredCheckSkip : boolean = false;
                if (ObjectValidator.IsString($dependencies[dependencyName])) {
                    const source : string = "" + $dependencies[dependencyName];
                    registeredCheckSkip = StringUtils.StartsWith(source, "!");
                    if (registeredCheckSkip) {
                        $dependencies[dependencyName] =
                            <any>StringUtils.Substring("" + $dependencies[dependencyName], 1);
                    }
                } else if (ObjectValidator.IsString($dependencies[dependencyName].location)) {
                    registeredCheckSkip = StringUtils.StartsWith(<string>$dependencies[dependencyName].location, "!");
                    if (registeredCheckSkip) {
                        $dependencies[dependencyName].location =
                            StringUtils.Substring(<string>$dependencies[dependencyName].location, 1);
                    }
                }
                if (!this.registered.hasOwnProperty(dependencyName)) {
                    let isRequired : boolean = false;
                    if (!ObjectValidator.IsString($dependencies[dependencyName]) &&
                        $dependencies[dependencyName].hasOwnProperty("platforms")) {
                        if (!ObjectValidator.IsArray($dependencies[dependencyName].platforms)) {
                            $dependencies[dependencyName].platforms = [<string>$dependencies[dependencyName].platforms];
                        }
                        (<string[]>$dependencies[dependencyName].platforms).forEach(($platforms : string) : void => {
                            if (this.isPlatformMatch($platforms)) {
                                isRequired = true;
                            }
                        });
                    } else {
                        isRequired = !(ObjectValidator.IsObject($dependencies[dependencyName]) &&
                            !$dependencies[dependencyName].hasOwnProperty("location"));
                    }
                    if (isRequired) {
                        this.registered[dependencyName] = {
                            dependency: $dependencies[dependencyName],
                            registeredCheckSkip
                        };
                        dependenciesList.push(dependencyName);
                    }
                } else if (typeof $dependencies[dependencyName] === "string" && <any>$dependencies[dependencyName] !== "" &&
                    (!this.registered[dependencyName].hasOwnProperty("registeredCheckSkip") ||
                        !this.registered[dependencyName].registeredCheckSkip)) {
                    const newSource : IProjectDependencyLocation = this.ParseLocation($dependencies[dependencyName].toString())[0];
                    const registeredSource : IProjectDependencyLocation =
                        typeof this.registered[dependencyName].dependency === "string" ?
                            this.ParseLocation(this.registered[dependencyName].dependency)[0] :
                            this.ParseLocation(this.registered[dependencyName].dependency.location)[0];
                    if (registeredSource.branch !== newSource.branch) {
                        LogIt.Error("Dependencies configuration is corrupted: " +
                            "required repository \"" + dependencyName + ":" + newSource.branch + "\" " +
                            "is not in match with previously cloned repository \"" + dependencyName + ":" +
                            registeredSource.branch + "\". Please, fix the required reference.");
                    }
                }
            }
        }
        return dependenciesList;
    }

    protected async processDependencies($dependencies : IProjectDependency[]) : Promise<void> {
        const dependenciesList : string[] = this.collectDependencies($dependencies);

        for await (const dependencyName of dependenciesList) {
            const dependency : IProjectDependency | string = $dependencies[dependencyName];
            await this.processDependency(dependencyName, dependency);
        }
    }

    protected async initDependencies() : Promise<void> {
        LogIt.Info("Removing dependencies directory. This action can spend some time...");

        try {
            await this.fileSystem.DeleteAsync(this.properties.projectBase + "/dependencies");
        } catch (ex) {
            LogIt.Error("Dependencies directory can not be removed. Please check if directory is blocked by any process" +
                "and re-run install task. You can also delete directory manually.");
        }

        this.fileSystem.CreateDirectory(this.properties.projectBase + "/dependencies");
        if (this.fileSystem.Exists(this.lockFilePath)) {
            LogIt.Warning("Package LOCK has been detected. Dependencies configuration will be suppressed by lock config!");
            const lock : any = JSON.parse(this.fileSystem.Read(this.lockFilePath).toString());
            this.project.dependencies = lock.dependencies;
            this.lockDetected = true;

            if (GitManager.IsGitRepo()) {
                const origin : IGitExecuteResult = await GitManager.ExecuteAsync([
                    "config", "--get", "remote.origin.url"
                ], {repoPath: this.properties.projectBase, verbose: false});
                const revBranch : IGitExecuteResult = await GitManager.ExecuteAsync([
                    "branch", "--show-current"
                ], {repoPath: this.properties.projectBase, verbose: false});
                const revSha : IGitExecuteResult = await GitManager.ExecuteAsync([
                    "rev-parse", "HEAD"
                ], {repoPath: this.properties.projectBase, verbose: false});

                if (!(
                    origin.stdout.includes(lock.project.repository.url) &&
                    revBranch.stdout.includes(lock.project.repository.branch) &&
                    revSha.stdout.includes(lock.project.repository.sha)
                )) {
                    LogIt.Warning("Dependencies lock may not be working for current repo state as it is not aligned with expected " +
                        "package.lock config:\n" + JSON.stringify(lock.project, null, 2));
                }
            } else {
                LogIt.Warning(">>"[ColorType.YELLOW] + " project has not been detect as GIT repository, " +
                    "validation of project setup skipped.");
            }
            for await (const dependencyName of Object.keys(this.project.dependencies)) {
                const dependency : IProjectDependency | string = this.project.dependencies[dependencyName];
                await this.processDependency(dependencyName, dependency);
            }
        } else {
            this.lockDetected = false;
            await this.processDependencies(this.project.dependencies);
        }
        if (this.downloaded === 0) {
            LogIt.Warning("Dependencies download skipped: dependencies are not configured");
        }
    }

    protected getName() : string {
        return "dependencies-download";
    }

    protected async processAsync($option : string) : Promise<void> {
        DependenciesDownload.isRunning = true;

        this.processed = {};
        this.registered = {};
        this.downloaded = 0;
        if ($option === "preload") {
            this.initEnvironment();
            // TODO: second download should not be necessary in case of that lock file exists,
            //       but from some reason dependencies are not kept by single run of preload
        }
        if (this.project.hasOwnProperty("dependencies")) {
            if (this.programArgs.IsAgentTask()) {
                LogIt.Warning("Dependencies download in agent mode may require GIT or WAN proxy for proper functionality");
            }
            await this.initDependencies();

            const repository : any = JsonUtils.Clone(this.project.repository);
            if (GitManager.IsGitRepo()) {
                const revBranch : IGitExecuteResult = await GitManager.ExecuteAsync([
                    "branch", "--show-current"
                ], {repoPath: this.properties.projectBase, verbose: false});
                const revSha : IGitExecuteResult = await GitManager.ExecuteAsync([
                    "rev-parse", "HEAD"
                ], {repoPath: this.properties.projectBase, verbose: false});
                JsonUtils.Extend(repository, {
                    branch: StringUtils.Remove(revBranch.stdout, "\r\n", "\n", "\r"),
                    sha   : StringUtils.Remove(revSha.stdout, "\r\n", "\n", "\r")
                });
            }

            this.fileSystem.Write(this.properties.projectBase + "/build_cache/package.lock.json",
                JSON.stringify({
                    project     : {
                        version  : this.project.version,
                        buildTime: "<? @var build.time ?>",
                        repository
                    },
                    dependencies: this.packageLock
                }, null, 2));
        } else {
            LogIt.Warning("Dependencies download skipped: dependencies are not configured");
        }
        DependenciesDownload.isRunning = false;
    }

    private async getSourceAsync($dependencyName : string, $source : IProjectDependencyLocation | string, $dependency : IProjectDependency,
                                 $targetPath : string) : Promise<void> {
        const parsedSource : IProjectDependencyLocation[] = this.ParseLocation($source);
        for await (const source of parsedSource) {
            let targetPath : string = $targetPath;
            if (parsedSource.length > 1) {
                targetPath = $targetPath + "/" + source.platform;
            }
            const retries : number[] = new Array(this.config.maxRetry);
            let processed : boolean = false;
            let error : Error = null;
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            for await (const index of retries) {
                try {
                    await this.processSingleSource($dependencyName, source, $dependency, targetPath);
                    processed = true;
                    break;
                } catch (ex) {
                    error = ex;
                    await TimeoutManager.Sleep(this.config.timeout * 1000);
                }
            }
            if (!processed) {
                LogIt.Error(error);
            }
        }
    }
}

export interface IRepoCloneResult {
    repoSource : string;
    branch : string;
    targetPath : string;
    sha : string;
}

// generated-code-start
export const IRepoCloneResult = globalThis.RegisterInterface(["repoSource", "branch", "targetPath", "sha"]);
// generated-code-end
