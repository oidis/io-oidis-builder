/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { ArchType } from "../../Enums/ArchType.js";
import { OSType } from "../../Enums/OSType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { IProject, IProjectDependency, IProjectDependencyLocation, IProjectDependencyScriptConfig } from "../../Interfaces/IProject.js";
import { ScriptHandler } from "../../IOApi/Hadlers/ScriptHandler.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildProductArgs } from "../../Structures/BuildProductArgs.js";
import { BuildExecutor } from "../BuildProcessor/BuildExecutor.js";
import { TypeScriptCompile } from "../TypeScript/TypeScriptCompile.js";
import { DependenciesDownload } from "./DependenciesDownload.js";

export class DependenciesInstall extends BaseTask {
    private static currentOs : OSType;
    private static currentArch : ArchType;
    private static currentToolchain : ToolchainType;
    private installed : number;
    private configured : number;
    private configs : IProject[] = [];

    public static getCurrentInstallOsType() : OSType {
        return DependenciesInstall.currentOs;
    }

    public static getCurrentArch() : ArchType {
        return DependenciesInstall.currentArch;
    }

    public static getCurrentToolchain() : ToolchainType {
        return DependenciesInstall.currentToolchain;
    }

    protected findScript($basePath : string, $scriptName : string) : string {
        const getPath : any = ($pattern : string) : string => {
            $pattern = $pattern.replace(/\\/gim, "/").replace(/\/\//gim, "/");
            const fileList : string[] = this.fileSystem.Expand($pattern);
            if (fileList.length > 0) {
                return fileList[0];
            } else {
                return "";
            }
        };
        if (ObjectValidator.IsEmptyOrNull($basePath) || !ObjectValidator.IsString($basePath)) {
            $basePath = this.properties.projectBase;
        }
        const availablePaths : string[] = [
            $basePath + "/" + $scriptName,
            this.properties.projectBase + "/bin/resource/scripts/" + $scriptName,
            this.properties.projectBase + "/bin/xcpp_scripts/" + $scriptName,
            this.properties.projectBase + "/resource/scripts/" + $scriptName,
            this.properties.projectBase + "/xcpp_scripts/" + $scriptName,
            this.properties.projectBase + "/dependencies/*/resource/scripts/" + $scriptName,
            this.properties.projectBase + "/dependencies/*/xcpp_scripts/" + $scriptName
        ];
        let path : string = "";
        let index : number;
        for (index = 0; index < availablePaths.length; index++) {
            if (path === "") {
                path = getPath(availablePaths[index]);
            } else {
                break;
            }
        }
        return path;
    }

    protected configDataLookup($dependencyName : string, $scriptType : string,
                               $data : IProjectDependencyScriptConfig) : IProjectDependencyScriptConfig {
        if (!ObjectValidator.IsEmptyOrNull($data)) {
            if (ObjectValidator.IsEmptyOrNull($data.attributes) || !ObjectValidator.IsArray($data.attributes)) {
                $data.attributes = [];
            }
            if (!$data.hasOwnProperty("ignore-parent-attribute")) {
                $data["ignore-parent-attribute"] = false;
            }
            this.configs.forEach(($config : IProject) : void => {
                if ($config.hasOwnProperty("dependencies") &&
                    $config.dependencies.hasOwnProperty($dependencyName)) {
                    if ($config.dependencies[$dependencyName].hasOwnProperty("configure-script")) {
                        $config.dependencies[$dependencyName].configureScript =
                            $config.dependencies[$dependencyName]["configure-script"];
                    }
                    if ($config.dependencies[$dependencyName].hasOwnProperty("install-script")) {
                        $config.dependencies[$dependencyName].installScript =
                            $config.dependencies[$dependencyName]["install-script"];
                    }
                    if ($config.dependencies[$dependencyName].hasOwnProperty($scriptType) &&
                        $config.dependencies[$dependencyName][$scriptType].hasOwnProperty("attributes")) {
                        if (!$data.hasOwnProperty("ignore-parent-attribute") || $data["ignore-parent-attribute"] !== true) {
                            $config.dependencies[$dependencyName][$scriptType].attributes.forEach(($attribute : string) : void => {
                                if ($data.attributes.indexOf($attribute) === -1) {
                                    (<string[]>$data.attributes).push($attribute);
                                }
                            });
                        }
                    }
                }
            });
        }
        return $data;
    }

    protected configure($dependencyName : string, $dependency : IProjectDependency) : boolean {
        let retVal : boolean = false;
        const path : any = require("path");
        const tagName : string = "configureScript";
        if (!ObjectValidator.IsEmptyOrNull($dependency) && $dependency.hasOwnProperty(tagName) &&
            $dependency[tagName].hasOwnProperty("name")) {
            const configScript : IProjectDependencyScriptConfig =
                this.configDataLookup($dependencyName, tagName, $dependency[tagName]);

            LogIt.Info("Looking for config script \"" + configScript.name + "\"");
            const scriptName : string = this.findScript(configScript.path, configScript.name);

            if (scriptName !== "") {
                configScript.path = path.dirname(scriptName);
                let attributesList : string = "";
                (<string[]>configScript.attributes).forEach(($attribute : string) : void => {
                    if (attributesList !== "") {
                        attributesList += ",";
                    }
                    attributesList += $attribute;
                });
                configScript.attributes = attributesList;
                configScript["macro-name"] = (configScript.name.split(".")[0]).toUpperCase();
                this.configured++;
                retVal = true;
            } else {
                LogIt.Info("WARNING: "[ColorType.YELLOW] + "Specified configuration script \"" + configScript.name +
                    "\" not found in search paths. See documentation for more info.");
            }
        }
        return retVal;
    }

    protected async installSingle($script : string, $dependencyName : string, $dependencyPath : string, $osType : OSType,
                                  $osArch : ArchType,
                                  $installOptions : IProjectDependencyScriptConfig) : Promise<void> {
        LogIt.Info(">>"[ColorType.YELLOW] + " Running script " + $script +
            (ObjectValidator.IsEmptyOrNull($osType) ? "" : (" for " + OSType.getKey($osType) + " platform with arch " +
                ArchType.getKey($osArch))));

        if (!this.fileSystem.IsDirectory($dependencyPath)) {
            $dependencyPath = this.fileSystem.NormalizePath(this.properties.projectBase + "/dependencies/" + $dependencyName);
        }
        if (this.fileSystem.Exists($dependencyPath)) {
            const handler : ScriptHandler = new ScriptHandler();
            handler.Path($script);
            handler.Environment({
                Process($cwd : string, $args : string[], $done : () => void) : void {
                    LogIt.Warning("Install skipped: script did not specify module.exports and " +
                        "also did not override global Process method");
                    $done();
                }
            });
            DependenciesInstall.currentOs = $osType;
            DependenciesInstall.currentArch = ObjectValidator.IsEmptyOrNull($osArch) ? ArchType.X64 : $osArch;
            DependenciesInstall.currentToolchain = this.properties.toolchains.gcc;
            if ($osType === OSType.ARM || $osType === OSType.IMX) {
                DependenciesInstall.currentToolchain = this.properties.toolchains.aarch64;
                if (DependenciesInstall.currentArch === ArchType.X32) {
                    DependenciesInstall.currentToolchain = this.properties.toolchains.aarch32;
                }
            } else if ($osType === OSType.MAC) {
                DependenciesInstall.currentToolchain = this.properties.toolchains.clang;
            }
            return new Promise<void>(($resolve) => {
                handler.SuccessHandler(() : void => {
                    if (!ObjectValidator.IsEmptyOrNull(handler.Environment().module) &&
                        !ObjectValidator.IsEmptyOrNull(handler.Environment().module.exports)) {
                        LogIt.Error("Install script has failed: usage of module.exports API is deprecated. " +
                            "Migrate to older version of builder or convert install script.");
                    } else {
                        DependenciesInstall.currentOs = null;
                        this.installed++;
                        $resolve();
                    }
                });
                handler.ErrorHandler(($error : Error) : void => {
                    LogIt.Error("Install script has failed.", $error);
                });
                handler.Process($dependencyPath, <string[]>$installOptions.attributes);
            });
        } else {
            LogIt.Error("Dependency install failed because dependency folder is not prepared properly before running install script." +
                " Generally this can happen only in customized call of dependencies install task without download.");
        }
    }

    protected async install($dependencyName : string, $dependency : IProjectDependency) : Promise<void> {
        if (this.properties.projectHas.ESModules() && !ObjectValidator.IsEmptyOrNull($dependency)) {
            if (this.properties.esModuleDependencies.indexOf($dependency) !== -1) {
                const referencePath : string = this.properties.projectBase + "/dependencies/" + $dependency.name +
                    "/source/typescript/reference.d.ts";
                if (this.fileSystem.Exists(referencePath) && !this.fileSystem.Exists(referencePath) + ".orig") {
                    this.fileSystem.Write(referencePath + ".orig", this.fileSystem.Read(referencePath));
                    this.fileSystem.Write(this.properties.projectBase + "/dependencies/" + $dependency.name +
                        "/source/typescript/reference.d.ts", this.properties.licenseText + `
// WARNING: this is generated file suitable for compilation and do not respect original content

import { Io } from "../../../../source/typescript/reference.js"

export { Io }
`);
                }
            }
        }

        const tagName : string = "installScript";
        if ($dependency.hasOwnProperty(tagName) && $dependency[tagName].hasOwnProperty("name")) {
            const installScriptObject : IProjectDependencyScriptConfig =
                this.configDataLookup($dependencyName, tagName, $dependency[tagName]);

            LogIt.Info("Looking for install script \"" + installScriptObject.name + "\"");
            const scriptPath : string = this.findScript(installScriptObject.path, installScriptObject.name);

            if (typeof $dependency.location === "string") {
                $dependency.location = <any>{path: $dependency.location};
            }

            if (scriptPath !== "") {
                const dependencyPaths : any[] = [];
                if (ObjectValidator.IsSet(<IProjectDependencyLocation>$dependency.location)) {
                    const depResolvedPaths : string[] =
                        (new DependenciesDownload()).ResolveDependencyPath((<IProjectDependencyLocation>$dependency.location).path);
                    depResolvedPaths.forEach(($value : string) : void => {
                        let platform : string = "";
                        let arch : string = "";
                        if (StringUtils.StartsWith($value, "$")) {
                            const items : string[] = StringUtils.Split($value, "$")
                                .filter(($value : string) : boolean => {
                                    return !ObjectValidator.IsEmptyOrNull($value);
                                });
                            platform = items[0];
                            arch = items[1];
                            $value = items[2] + "/" + platform;
                        }
                        if (!StringUtils.StartsWith($value, "mvn+") &&
                            (<IProjectDependencyLocation>$dependency.location).type !== "mvn") {
                            if (!this.fileSystem.Exists($value)) {
                                $value = this.properties.projectBase + "/dependencies/" + $dependencyName;
                                if (!ObjectValidator.IsEmptyOrNull(platform) && depResolvedPaths.length > 1) {
                                    $value += "/" + platform;
                                }
                            }
                            dependencyPaths.push({
                                arch,
                                os  : platform,
                                path: $value
                            });
                        }
                    });
                } else {
                    dependencyPaths.push({
                        arch: null,
                        os  : null,
                        path: this.properties.projectBase + "/dependencies/" + $dependencyName
                    });
                }

                let index : number = 0;
                let loopOver : any[] = dependencyPaths;
                let singlePath : string = "";
                if (loopOver.length === 1) {
                    loopOver = [];
                    const requestedProducts : BuildProductArgs[] = new BuildExecutor().getTargetProducts();
                    requestedProducts.forEach(($product : BuildProductArgs) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($dependency.platforms)) {
                            let depPlatforms : string[] = [];
                            if (ObjectValidator.IsString($dependency.platforms)) {
                                depPlatforms = [<string>$dependency.platforms];
                            } else {
                                depPlatforms = <string[]>$dependency.platforms;
                            }
                            if (depPlatforms.indexOf($product.OS().toString()) !== -1) {
                                loopOver.push($product.OS());
                            }
                        }
                    });
                    singlePath = dependencyPaths[0].path;
                }
                const iterator : any[] = new Array(loopOver.length === 0 ? 1 : loopOver.length);
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                for await (const iteration of iterator) {
                    if (index < loopOver.length ||
                        (loopOver.length === 0 && index === 0 && !ObjectValidator.IsEmptyOrNull(singlePath))) {
                        let item : any;
                        if (loopOver.length > 0) {
                            item = loopOver[index];
                        } else {
                            item = dependencyPaths[0].os;
                            if (!ObjectValidator.IsEmptyOrNull(singlePath)) {
                                item = {os: item, path: singlePath, arch: dependencyPaths[0].arch};
                            }
                        }
                        await this.installSingle(scriptPath, $dependencyName, item.path, item.os, item.arch, installScriptObject);
                        index++;
                    }
                }
            } else {
                LogIt.Info("WARNING: "[ColorType.YELLOW] +
                    "Specified install script \"" + installScriptObject.name + "\" not found  in search paths. " +
                    "See documentation for more info.");
            }
        }
    }

    protected async processDependency($configureOnly : boolean, $dependencyName : string,
                                      $dependency : IProjectDependency | string) : Promise<void> {
        let dependency : IProjectDependency;
        if (typeof $dependency === "string") {
            dependency = <any>{location: $dependency};
        } else {
            dependency = <IProjectDependency>$dependency;
        }

        this.configure($dependencyName, dependency);

        if (!$configureOnly) {
            await this.install($dependencyName, dependency);
        }
    }

    protected async processDependencies($configureOnly : boolean) : Promise<void> {
        if (this.project.hasOwnProperty("dependencies")) {
            const dependenciesList : string[] = [];
            let dependencyName : string;
            for (dependencyName in this.project.dependencies) {
                if (this.project.dependencies.hasOwnProperty(dependencyName)) {
                    dependenciesList.push(dependencyName);
                }
            }
            for await (const dependencyName of dependenciesList) {
                const dependency : IProjectDependency = this.project.dependencies[dependencyName];
                await this.processDependency($configureOnly, dependencyName, dependency);
            }
            if (this.installed === 0) {
                LogIt.Warning("Dependencies install skipped: none of dependency requires installation");
            }
            if (this.configured === 0) {
                LogIt.Warning("Dependencies configure skipped: no configuration has been processed.");
            }
        } else {
            LogIt.Warning("Dependencies install and configure skipped: dependencies are not configured");
        }
    }

    protected async loadConfigs() : Promise<void> {
        this.configs = [];
        const files : string[] = this.fileSystem.Expand(this.properties.projectBase + "/dependencies/*/package.conf.json*");
        for await (const file of files) {
            this.configs.push((await Loader.getInstance().getEnvironmentArgs().LoadPackageConf(file)).data);
        }
    }

    protected afterInstall() : void {
        if (this.properties.projectHas.ESModules()) {
            const paths : any = {};
            this.properties.esModuleDependencies.forEach(($value : IProjectDependency) : void => {
                paths["@" + $value.name + "/*"] = ["dependencies/" + $value.name + "/source/typescript/*"];
                paths["@" + $value.name + "/test/unit/*"] = ["dependencies/" + $value.name + "/test/unit/typescript/*"];
            });
            if (this.fileSystem.Exists(this.properties.projectBase + "/dependencies/nodejs")) {
                const nodeAlias : string = "nodejs";
                paths["@" + nodeAlias + "/*"] = ["dependencies/nodejs/build/node_modules/*"];
            }
            this.fileSystem.Write(this.properties.projectBase + "/tsconfig.json", JSON.stringify({
                compilerOptions: JsonUtils.Extend(JsonUtils.Clone(TypeScriptCompile.getConfig().module.options), {
                    baseUrl               : ".",
                    experimentalDecorators: true,
                    lib                   : [
                        "DOM",
                        "ScriptHost",
                        "WebWorker",
                        "ESNext"
                    ],
                    paths
                })
            }, null, 2));
        }
        if (this.properties.projectHas.SASS()) {
            const alias : string[] = [];
            this.properties.oidisDependencies.forEach(($value : IProjectDependency) : void => {
                alias.push(`"@${$value.name}": path.resolve(__dirname, "dependencies/${$value.name}/resource/sass")`);
            });
            this.fileSystem.Write(this.properties.projectBase + "/webpack.config.js",
                `/* ********************************************************************************************************* *
 *
 * Copyright ${StringUtils.YearFrom(2025)} Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
// WARNING THIS IS GENERATED FILE DO NOT MODIFY!!
const path = require("path");

module.exports = {
  resolve: {
    alias: {
      ${alias.join(",\n      ")}
    }
  }
};
`);
        }
    }

    protected getName() : string {
        return "dependencies-install";
    }

    protected async processAsync($option : string) : Promise<void> {
        this.installed = 0;
        this.configured = 0;
        let configureOnly : boolean = false;
        if ($option === "configure") {
            configureOnly = true;
        }
        await this.loadConfigs();
        await this.processDependencies(configureOnly);
        if (!configureOnly) {
            this.afterInstall();
        }
    }
}
