/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { ProtectionType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ProtectionType.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { ResourceType } from "../../Enums/ResourceType.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import {
    IProjectDependency,
    IProjectDependencyLocation, IProjectSplashScreenResource,
    IProjectTargetResourceFiles,
    IProjectTargetResources
} from "../../Interfaces/IProject.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { ResourceHandler } from "../../Utils/ResourceHandler.js";
import { CryptoEnvironment } from "../Utils/CryptoEnvironment.js";
import { StringReplace } from "../Utils/StringReplace.js";

export class CopyStaticFiles extends BaseTask {
    private warningReported : boolean = false;

    protected getName() : string {
        return "copy-staticfiles";
    }

    protected async processAsync($option : string) : Promise<void> {
        const path : any = require("path");

        if ($option === "resource") {
            if (this.properties.projectHas.Resources()) {
                if (this.build.product.Toolchain() !== ToolchainType.GYP &&
                    this.build.product.Toolchain() !== ToolchainType.PIP) {
                    const src : string[] = [
                        "resource/graphics",
                        "resource/javascript",
                        "resource/libs",
                        "resource/data"
                    ];
                    for await (const currentSrc of src) {
                        await this.copy("dependencies", currentSrc, "", "*/test/**");
                        await this.copy("dependencies", "test/" + currentSrc, "", "");
                        await this.copy("", currentSrc, "", "*/test/**");
                        await this.copy("", "test/" + currentSrc, "", "");
                    }
                } else {
                    LogIt.Warning("Copy static files task option \"" + $option + "\" skipped: " +
                        "resource files should be defined by target configuration for current project.");
                }
            } else {
                LogIt.Warning("Copy static files task option \"" + $option + "\" skipped: resource files has not been found.");
            }
        } else if ($option === "targetresource") {
            await this.copyTargetResources(false);
        } else if ($option === "targetresource-postbuild") {
            await this.copyTargetResources(true);
        } else if ($option === "xcppresx") {
            class ResxInfo {
                public name : string;
                public file : string;
                public object : IProjectSplashScreenResource;

                constructor($name : string, $file : string, $object : IProjectSplashScreenResource) {
                    this.name = $name;
                    this.file = $file;
                    this.object = $object;
                }
            }

            const resources : IProjectTargetResources[] = this.flattenResources();
            const resItems : ResxInfo[] = [];
            let targetExtension : string = "";
            if (EnvironmentHelper.IsWindows()) {
                targetExtension = ".exe";
            }

            resources.forEach(($resObject : IProjectTargetResources) : void => {
                if ($resObject.hasOwnProperty("input") && ($resObject.hasOwnProperty("resx") && $resObject.resx) &&
                    this.isPlatformMatch($resObject) && this.isToolchainMatch($resObject)) {
                    const res : IResolvedCopyPaths = this.resolveResourcePaths($resObject, $resObject.input);
                    for (const key in res.paths) {
                        if (res.paths.hasOwnProperty(key)) {
                            if (this.fileSystem.Exists(res.paths[key])) {
                                const name : string = res.paths[key]
                                    .replace(/[\/\\.\-]/g, "_") // eslint-disable-line no-useless-escape
                                    .toUpperCase();

                                let output : string = res.paths[key];
                                if ($resObject.hasOwnProperty("output") && $resObject.output.length > 0) {
                                    output = $resObject.output;
                                    if (output.indexOf("/**") === -1) {
                                        output = output
                                                .replace("/*", "")
                                                .replace("\\*", "") +
                                            "/" + path.basename(res.paths[key]);
                                    } else {
                                        let sourceRoot : string = res.resPath;
                                        const wildStart : number = sourceRoot.indexOf("/*");
                                        if (wildStart !== -1) {
                                            sourceRoot = sourceRoot.substring(0, wildStart);
                                        }

                                        output = output
                                                .replace("/**", "")
                                                .replace("/*", "")
                                                .replace("\\*", "") +
                                            res.paths[key].substring(res.paths[key].indexOf(sourceRoot) + sourceRoot.length);
                                    }

                                    LogIt.Debug("file: {0}\nout:  {1}", res.paths[key], output);
                                }

                                const tmpResX : IProjectSplashScreenResource = <IProjectSplashScreenResource>{
                                    copyOnly: true,
                                    location: output,
                                    name,
                                    type    : ResourceType.RESOURCE
                                };
                                resItems.push(new ResxInfo(name, res.paths[key], tmpResX));
                            }
                        }
                    }
                }
            });

            for await (const tmp of resItems) {
                await ResourceHandler.EmbedFile(this.properties.projectBase + "/build/target/" +
                    this.project.target.name + targetExtension, tmp.object.type.toString(),
                    tmp.name, this.properties.projectBase + "/" + tmp.file);
            }
            if (resItems.length !== 0) {
                const cfgPath : string = this.properties.projectBase + "/build/compiled/intern.conf.json";
                const infos : IProjectSplashScreenResource[] = [];
                resItems.forEach(($item) => {
                    infos.push($item.object);
                });
                this.fileSystem.Write(cfgPath, JSON.stringify(infos, null, 2));
                const extension : string = EnvironmentHelper.IsWindows() ? ".exe" : "";
                await ResourceHandler.EmbedFile(this.properties.projectBase + "/build/target/" + this.project.target.name + extension,
                    "CONFIG", "INTERN_CONF_JSON", cfgPath);
            }
        } else {
            LogIt.Warning("Copy static files task option \"" + $option + "\" has not been found.");
        }
    }

    private flattenResources() : IProjectTargetResources[] {
        const resources : IProjectTargetResources[] = [];
        this.project.target.resources.forEach(($resource : IProjectTargetResources) : void => {
            if ($resource.hasOwnProperty("skip-replace")) {
                if (this.warningReported) {
                    LogIt.Warning("Keyword \"skip-replace\" has been deprecated and should be replaced by " +
                        "\"skipReplace\" at all target.resources records.");
                    this.warningReported = true;
                }
                $resource.skipReplace = $resource["skip-replace"];
            }
            if ($resource.hasOwnProperty("files")) {
                $resource.files.forEach(($file : IProjectTargetResourceFiles) : void => {
                    const cwd : string = $file.hasOwnProperty("cwd") ? $file.cwd + "/" : "";
                    const addResource : any = ($source : string) : void => {
                        const resource : IProjectTargetResources = <any>{
                            input: cwd + $source
                        };
                        if ($file.hasOwnProperty("dest")) {
                            resource.output = $file.dest;
                        }
                        [
                            "skipReplace",
                            "copy",
                            "embed",
                            "platforms",
                            "protect",
                            "toolchain",
                            "resx",
                            "postBuildCopy",
                            "keepLinks"
                        ].forEach(($parameter : string) : void => {
                            if ($resource.hasOwnProperty($parameter)) {
                                resource[$parameter] = $resource[$parameter];
                            }
                        });
                        resources.push(resource);
                    };
                    if (ObjectValidator.IsString($file.src)) {
                        addResource($file.src);
                    } else {
                        (<string[]>$file.src).forEach(($input : string) : void => {
                            addResource($input);
                        });
                    }
                });
            } else {
                resources.push($resource);
            }
        });
        return resources;
    }

    private resolveResourcePaths($resObject : IProjectTargetResources, $resInput : string) : IResolvedCopyPaths {
        let resInput : string = $resInput;
        let platformScope : boolean = false;
        if (StringUtils.Contains(resInput, "<platform>")) {
            resInput = StringUtils.Replace(resInput, "<platform>", this.build.product.OS().toString());
            platformScope = true;
        }
        let files : string[] = this.fileSystem.Expand([resInput]);
        if (files.length === 0 && platformScope) {
            LogIt.Info("Platform key identified in resource location, but no file was found. Trying without platform...");
            resInput = StringUtils.Replace(StringUtils.Replace($resInput, "<platform>", ""), "//", "/");
            files = this.fileSystem.Expand([resInput]);
        }
        if (files.length === 0 && (StringUtils.StartsWith(resInput, "dependencies/") ||
            StringUtils.StartsWith($resObject.input, "/dependencies/"))) {
            resInput = StringUtils.Replace(StringUtils.Replace(resInput, "/dependencies/", ""),
                "dependencies/", "");
            const resDepName : string = StringUtils.Substring(resInput, 0, StringUtils.IndexOf(resInput, "/", true));

            for (const key in this.project.dependencies) {
                if (this.project.dependencies.hasOwnProperty(key)) {
                    const dependency = this.project.dependencies[key];
                    if ((key === resDepName) ||
                        (StringUtils.Replace(key, "-", "_") === resDepName)) {
                        const tmp : any = dependency.location;
                        let tmpPath : string = "";
                        if (typeof tmp === "string") {
                            tmpPath = tmp;
                        } else {
                            if ((<IProjectDependencyLocation>tmp).type === "dir" ||
                                (<IProjectDependencyLocation>tmp).type === "url") {
                                tmpPath = <string>(<IProjectDependencyLocation>tmp).path;
                            }
                        }
                        if (EnvironmentHelper.IsWindows()) {
                            tmpPath = StringUtils.Remove(tmpPath, "file:///");
                        }
                        tmpPath = StringUtils.Remove(tmpPath, "file://");
                        if (StringUtils.StartsWith(resInput, resDepName)) {
                            resInput = StringUtils.Substring(resInput, resDepName.length);
                        }
                        resInput = tmpPath + resInput;
                        LogIt.Info("Resource found outside project directory: " + resInput);
                    }
                }
            }
            files = this.fileSystem.Expand([resInput]);
        } else if (files.length === 0 && StringUtils.StartsWith(resInput, "resource/")) {
            LogIt.Info("Resource has not been found at project base, trying at dependencies ...");
            const paths : string[] = [];
            this.properties.oidisDependencies.forEach(($value : IProjectDependency) : void => {
                paths.push(this.properties.projectBase + "/dependencies/" + $value.name + "/" + resInput);
            });
            files = this.fileSystem.Expand(paths);
        }

        return {
            paths  : files,
            resPath: resInput
        };
    }

    private isPlatformMatch($resource : IProjectTargetResources) : boolean {
        let retVal : boolean = false;

        const match : any = ($platform : string) : boolean => {
            let retVal : boolean = false;
            if ($platform === "any" || $platform === "all") {
                retVal = true;
            } else {
                retVal = this.build.product.OS().toString() === $platform;
            }
            return retVal;
        };

        if (!ObjectValidator.IsEmptyOrNull($resource.platforms)) {
            if (ObjectValidator.IsString($resource.platforms)) {
                retVal = match(<string>$resource.platforms);
            } else {
                let resItem : string;
                for (resItem in <string[]>$resource.platforms) {
                    if ($resource.platforms.hasOwnProperty(resItem)) {
                        retVal = retVal || match($resource.platforms[resItem]);
                    }
                }
            }
        } else {
            retVal = true;
        }
        return retVal;
    }

    private isToolchainMatch($resource : IProjectTargetResources) : boolean {
        let retVal : boolean = false;

        function check($item : string) : boolean {
            let status = false;

            if ($item === this.project.target.toolchain ||
                (this.project.target.toolchain.startsWith($item) && $item.length <= this.project.target.toolchain.length)) {
                status = true;
            }

            return status;
        }

        if (!ObjectValidator.IsEmptyOrNull($resource.toolchain)) {
            if (typeof $resource.toolchain === "string") {
                retVal = check.apply(this, [$resource.toolchain]);
            } else {
                let resItem : string;
                for (resItem in <string[]>$resource.toolchain) {
                    if ($resource.toolchain.hasOwnProperty(resItem)) {
                        retVal = retVal || check.apply(this, [$resource.toolchain[resItem]]);
                    }
                }
            }
        } else {
            retVal = true;
        }

        return retVal;
    }

    private protectResource($cryptoEnvironment : CryptoEnvironment, $fileName : string, $data : Buffer) : void {
        const crypto : any = require("crypto");
        const method : ProtectionType = $cryptoEnvironment.getProtectionType();

        if (method === ProtectionType.SIGNATURE) {
            const sign = crypto.createSign("sha512");
            sign.write($data);
            sign.end();

            $cryptoEnvironment.getProtectionMap()[$fileName] = sign.sign($cryptoEnvironment.PrivateKeyData(), "hex");
        } else if (method === ProtectionType.ENCRYPTION) {
            const iv : Buffer = crypto.randomBytes(16);
            const key : Buffer = Buffer.alloc(32);
            const cipher = crypto.createCipheriv("aes-256-gcm", key, iv);

            const enc : Buffer = Buffer.concat([cipher.update($data), cipher.final()]);
            const tag : Buffer = cipher.getAuthTag();

            const path : string = this.properties.projectBase + "/build/target/" + $fileName;
            this.fileSystem.Write(path, Buffer.concat([iv, tag, enc]).toString("hex"));
            $cryptoEnvironment.getProtectionMap().push($fileName);
        } else if (method === ProtectionType.HASH) {
            const hashEngine = crypto.createHash("sha512");
            hashEngine.update($data);
            $cryptoEnvironment.getProtectionMap()[$fileName] = hashEngine.digest("hex");
        } else {
            LogIt.Warning("Protection method '" + method + "' is not supported.");
        }
    }

    private async copyTargetResources($postBuild : boolean) : Promise<void> {
        const encoding : any = require("encoding");
        const istextorbinary : any = require("istextorbinary");
        const os : any = require("os");
        const path : any = require("path");
        const fs : any = require("fs");

        let embResourceHeaderTemplate : string = "" +
            "/*" + os.EOL +
            "    WARNING: This file contains data generated from resources." + os.EOL +
            "    Please check if all used resources match with application license" + os.EOL +
            "*/" + os.EOL +
            "#ifndef RESOURCES_DATA_H_" + os.EOL +
            "#define RESOURCES_DATA_H_" + os.EOL +
            os.EOL +
            "#include <stdlib.h>" + os.EOL +
            "#include <string>" + os.EOL +
            os.EOL +
            "#include <boost/unordered_map.hpp>" + os.EOL +
            "#include <boost/assign.hpp>" + os.EOL +
            os.EOL +
            "namespace Resources {" + os.EOL +
            os.EOL +
            "    namespace _private_resource_data {" + os.EOL +
            os.EOL +
            "<? @data ?>" +
            "<? @map ?>" + os.EOL +
            "    };" + os.EOL +
            os.EOL +
            "    class ResourcesData {" + os.EOL +
            "    private:" + os.EOL +
            "        static std::string *getResourceItem(const std::string &key) {" + os.EOL +
            "            boost::unordered_map<std::string, std::string *>::const_iterator item = " + os.EOL +
            "                    _private_resource_data::map.find(key);" + os.EOL +
            "            if (item != _private_resource_data::map.end()) {" + os.EOL +
            "                return item->second;" + os.EOL +
            "            } else {" + os.EOL +
            "                return nullptr;" + os.EOL +
            "            }" + os.EOL +
            "        }" + os.EOL +
            os.EOL +
            "    public:" + os.EOL +
            "        static const std::string getString(const std::string &key) {" + os.EOL +
            "            std::string *str = ResourcesData::getResourceItem(key);" + os.EOL +
            "            if (str != nullptr) {" + os.EOL +
            "                return std::string(*str);" + os.EOL +
            "            } else {" + os.EOL +
            "                return \"\";" + os.EOL +
            "            }" + os.EOL +
            "        };" + os.EOL +
            os.EOL +
            "        static void setString(const std::string &key, const std::string &data, const bool append = false) {" + os.EOL +
            "            std::string *str = ResourcesData::getResourceItem(key);" + os.EOL +
            "            if (str != nullptr) {" + os.EOL +
            "                if (append) {" + os.EOL +
            "                    str->append(data);" + os.EOL +
            "                } else {" + os.EOL +
            "                    str->assign(data);" + os.EOL +
            "                }" + os.EOL +
            "            } else {" + os.EOL +
            "                _private_resource_data::map.emplace(key, new std::string(data));" + os.EOL +
            "            }" + os.EOL +
            "        };" + os.EOL +
            "    };" + os.EOL +
            "};" + os.EOL +
            os.EOL +
            "#endif //RESOURCES_DATA_H_";

        const embResourceHeaderTemplateData : string = "" +
            "        static const uint8_t <? @name ?>Data[] = {" + os.EOL +
            "                <? @embData ?>" +
            "        };" + os.EOL +
            os.EOL +
            "        static std::string <? @name ?>StringUtils((char *) <? @name ?>Data, sizeof(<? @name ?>Data));" + os.EOL +
            os.EOL;

        let embResourceHeaderTemplateMap : string = "" +
            "        // map for all serialized resources" + os.EOL +
            "        static boost::unordered_map<std::string, std::string *> map = boost::assign::map_list_of<? @resMap ?>;";

        let embResourceDataMap : string = "<? @res ?>";

        let anyEmbeddedResource : boolean = false;

        const resources : IProjectTargetResources[] = this.flattenResources();
        let embedStr : string = "";
        let data : any = "";
        const embedData : any = ($element : string, $index : number) : void => {
            embedStr += "0x" + ("0" + (<any>$element).toString(16)).slice(-2);
            if ($index < data.length - 1) {
                embedStr += ",";
            }
            if (($index + 1) % 16 === 0) {
                embedStr += os.EOL + "                ";
            } else {
                embedStr += " ";
            }
        };

        let embeddedCounter : number = 0;
        let copiedCounter : number = 0;

        const cryptoEnvironment : CryptoEnvironment = new CryptoEnvironment();

        for await (const resObject of resources) {
            const postBuild : boolean = resObject.hasOwnProperty("postBuildCopy") && resObject.postBuildCopy;
            if ((!resObject.hasOwnProperty("resx") || !resObject.resx) &&
                this.isPlatformMatch(resObject) && this.isToolchainMatch(resObject) &&
                postBuild === $postBuild) {
                if (resObject.hasOwnProperty("input")) {
                    let resInput : string = resObject.input;
                    let files : string[] = [];

                    const res : IResolvedCopyPaths = this.resolveResourcePaths(resObject, resInput);
                    files = res.paths;
                    resInput = res.resPath;

                    let sourceRoot : string = resInput;
                    const wildStart : number = sourceRoot.indexOf("/*");
                    if (wildStart !== -1) {
                        sourceRoot = sourceRoot.substring(0, wildStart);
                    }

                    for await (const file of files) {
                        if (this.fileSystem.Exists(file) && fs.statSync(file).isFile()) {
                            const buffer : any = fs.readFileSync(file);
                            const isText : boolean = istextorbinary.isText(file, buffer);

                            if (resObject.hasOwnProperty("protect")) {
                                cryptoEnvironment.Method(resObject.protect);
                                this.protectResource(cryptoEnvironment, file, buffer);
                            }

                            if (resObject.hasOwnProperty("embed") && resObject.embed && !$postBuild) {
                                if (!this.properties.projectHas.Cpp.Source()) {
                                    LogIt.Error("Embed resource is allowed only for project with Cpp source.");
                                }
                                const ext : string = path.extname(file);
                                const name : string = path.basename(file)
                                    .replace(ext, ext.charAt(1).toUpperCase() + ext.slice(2))
                                    .replace(/\./g, "_")
                                    .replace(/\s/g, "_");

                                embedStr = "";

                                if (isText) {
                                    data = encoding.convert(StringReplace.Content(StringReplace.Content(
                                            this.fileSystem.Read(file).toString(), StringReplaceType.FOR_EACH),
                                        StringReplaceType.VARIABLES), "UTF-8");
                                } else {
                                    data = fs.readFileSync(file);
                                }

                                embResourceDataMap = embResourceDataMap.replace(/<\? @res \?>/ig,
                                    os.EOL + "                (\"" + file + "\", &" + name + "StringUtils)<? @res ?>");

                                data.forEach(embedData);
                                if (data.length % 16 !== 0) {
                                    embedStr += os.EOL;
                                }
                                embResourceHeaderTemplate = embResourceHeaderTemplate.replace(/<\? @data \?>/ig,
                                    embResourceHeaderTemplateData.replace(/<\? @name \?>/ig, name)
                                        .replace(/<\? @embData \?>/ig, embedStr) + "<? @data ?>");

                                anyEmbeddedResource = true;

                                if (this.programArgs.IsDebug()) {
                                    LogIt.Info("\"" + file + "\" was embedded.");
                                } else {
                                    embeddedCounter++;
                                }
                            } else if (!resObject.hasOwnProperty("copy") || resObject.copy) {
                                let resOutput : string = "resource";
                                if (resObject.hasOwnProperty("output")) {
                                    resOutput = resObject.output;
                                } else {
                                    LogIt.Warning("Default resource output path is used for resource file " +
                                        resObject.input);

                                    if ((<any>file).startsWith("resource/")) {
                                        resOutput = file;
                                    } else {
                                        resOutput += file;
                                    }
                                }

                                let outPath : string = resOutput;
                                if (this.fileSystem.Exists(outPath) && !fs.statSync(outPath).isFile() ||
                                    (<any>outPath).endsWith("/*") ||
                                    (<any>outPath).endsWith("\\*")) {
                                    if (outPath.indexOf("/**") === -1) {
                                        outPath = outPath
                                                .replace("/*", "")
                                                .replace("\\*", "") +
                                            "/" + path.basename(file);
                                    } else {
                                        outPath = outPath
                                                .replace("/**", "")
                                                .replace("/*", "")
                                                .replace("\\*", "") +
                                            file.substring(file.indexOf(sourceRoot) + sourceRoot.length);
                                    }
                                }

                                const destination : string = this.properties.dest + "/" + outPath;
                                if (!isText) {
                                    await this.fileSystem.CopyAsync(file, destination);
                                } else {
                                    const stats : any = fs.lstatSync(file);
                                    if (resObject.hasOwnProperty("keepLinks") && resObject.keepLinks &&
                                        stats.isSymbolicLink()) {
                                        LogIt.Info("> copy link from \"" + file + "\" to \"" + destination + "\"");
                                        this.fileSystem.CreateDirectory(path.dirname(destination));
                                        const link : string = fs.readlinkSync(file);
                                        fs.symlinkSync(link, destination);
                                    } else {
                                        const mode : number = stats.mode & parseInt("777", 8);
                                        let repData : string = this.fileSystem.Read(file).toString();
                                        if (!resObject.hasOwnProperty("skipReplace") ||
                                            resObject.skipReplace === false) {
                                            repData = StringReplace.Content(
                                                StringReplace.Content(repData, StringReplaceType.FOR_EACH),
                                                StringReplaceType.VARIABLES);
                                        }
                                        this.fileSystem.CreateDirectory(path.dirname(destination));
                                        fs.writeFileSync(destination, repData, {mode});
                                    }
                                }
                                if (this.programArgs.IsDebug()) {
                                    LogIt.Info("\"" + file + "\" was copied.");
                                } else {
                                    copiedCounter++;
                                }
                            }
                        }
                    }
                } else {
                    LogIt.Warning("Target resource file path is not specified");
                    LogIt.Info(JSON.stringify(resObject, null, 2));
                }
            }
        }
        if (this.properties.projectHas.Cpp.Source() && !$postBuild) {
            embResourceDataMap = embResourceDataMap.replace(/<\? @res \?>/ig, "");
            embResourceHeaderTemplateMap = embResourceHeaderTemplateMap.replace(/<\? @resMap \?>/ig, embResourceDataMap);
            embResourceHeaderTemplate = embResourceHeaderTemplate.replace(/<\? @data \?>/ig, "").replace(/<\? @map \?>/ig,
                anyEmbeddedResource ? embResourceHeaderTemplateMap :
                    "        // map for all serialized resources" + os.EOL +
                    "        static boost::unordered_map<std::string, std::string *> map;");

            this.fileSystem.Write(this.properties.compiled + "/resourcesData.hpp", embResourceHeaderTemplate);
        }

        if (cryptoEnvironment.getProtectionType() !== ProtectionType.NONE) {
            const path : string = this.properties.projectBase + "/build/target/resource/protect/";
            this.fileSystem.Write(path + "protectionMap.json", JSON.stringify(cryptoEnvironment.getProtectionMap()));

            if (cryptoEnvironment.getProtectionType() === ProtectionType.SIGNATURE) {
                this.fileSystem.Write(path + "builderPublicKey.pem", cryptoEnvironment.PublicKeyData());
            }
        }

        LogIt.Info(">>"[ColorType.RED] + " " + embeddedCounter + " files were embedded " +
            "and " + copiedCounter + " files were copied");
    }

    private async copy($root : string, $cwd : string, $dest : string, $exclude : string) : Promise<void> {
        const fs : any = require("fs");

        let filesRoot : string = "";
        if ($root !== "") {
            $root += "/";
            filesRoot = $root + "*/";
        }
        const expandPaths : string[] = [filesRoot + $cwd + "/**/*"];
        if (StringUtils.Contains($root, "dependencies")) {
            expandPaths.push(filesRoot + this.build.product.OS().toString() + "/" + $cwd + "/**/*");
        }
        if ($exclude && $exclude !== "") {
            expandPaths.push("!" + $exclude);
        }
        for (let index : number = 0; index < expandPaths.length; index++) {
            if (!StringUtils.StartsWith(expandPaths[index], this.properties.projectBase)) {
                expandPaths[index] = this.properties.projectBase + "/" + expandPaths[index];
            }
        }
        const files : string[] = this.fileSystem.Expand(expandPaths);
        if (!$dest || $dest === "") {
            $dest = this.properties.dest + "/" + $cwd;
        } else {
            $dest = this.properties.dest + "/" + $dest;
        }

        if (files.length > 0) {
            LogIt.Info("copy directory: \"" + $root + $cwd + "\" to \"" + $dest + "\", items found count: " + files.length);

            for await (const file of files) {
                if (!StringUtils.EndsWith(file, "/Loader.js")) {
                    let path : string =
                        $dest + file.substr(file.indexOf($cwd) + $cwd.length, file.length);
                    if (fs.statSync(file).isFile()) {
                        if (StringUtils.EndsWith(path, ".jsonp")) {
                            if (this.programArgs.IsDebug() && !(this.build.product.Toolchain() === ToolchainType.NODEJS)) {
                                path = path.replace(".jsonp", ".js");
                            }
                            StringReplace.File(file, StringReplaceType.FOR_EACH, path);
                            StringReplace.File(path, StringReplaceType.VARIABLES);
                        } else {
                            await this.fileSystem.CopyAsync(file, path);
                        }
                    }
                }
            }
        }
    }
}

export interface IResolvedCopyPaths {
    paths : string[];
    resPath? : string;
}

// generated-code-start
export const IResolvedCopyPaths = globalThis.RegisterInterface(["paths", "resPath"]);
// generated-code-end
