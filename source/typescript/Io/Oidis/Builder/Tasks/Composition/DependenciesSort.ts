/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { Resources } from "../../DAO/Resources.js";
import { IProjectConfigLoadResponse } from "../../EnvironmentArgs.js";
import { IProject, IProjectDependency, IProjectDependencyLocation, IProjectTarget } from "../../Interfaces/IProject.js";
import { IMavenArtifact } from "../../Interfaces/IProperties.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildExecutor } from "../BuildProcessor/BuildExecutor.js";
import { DependenciesDownload } from "./DependenciesDownload.js";

export class DependenciesSort extends BaseTask {

    constructor() {
        super();
    }

    protected getName() : string {
        return "dependencies-sort";
    }

    protected isPlatformMatch($platform : string) : boolean {
        let output : boolean = false;

        for (const product of (new BuildExecutor()).getTargetProducts()) {
            if (product.OS().toString() === $platform) {
                output = true;
                break;
            }
        }

        return output;
    }

    protected async processAsync($option : string) : Promise<void> {
        this.properties.cppIncludes = "";
        this.properties.cppLibraries = "";
        let dependencyName : string;
        const dependencies : any = {};

        const dependenciesPath : string[] = this.fileSystem.Expand(this.properties.projectBase + "/dependencies/**/package.conf.json*");
        for await (const dependencyPath of dependenciesPath) {
            Resources.setCWD(null);
            const config : IProjectConfigLoadResponse = await Loader.getInstance().getEnvironmentArgs().LoadPackageConf(dependencyPath);
            let name : string = StringUtils.Remove(config.path, this.properties.projectBase + "/dependencies/");
            name = name.substring(0, name.indexOf("/"));
            if (config.status) {
                this.properties.dependenciesConfig[name] = config.data;
            }
            if (config.data.hasOwnProperty("dependencies")) {
                for (dependencyName in config.data.dependencies) {
                    if (config.data.dependencies.hasOwnProperty(dependencyName)) {
                        if (!dependencies.hasOwnProperty(dependencyName)) {
                            dependencies[dependencyName] = config.data.dependencies[dependencyName];
                        } else if (ObjectValidator.IsObject(dependencies[dependencyName]) &&
                            ObjectValidator.IsObject(config.data.dependencies[dependencyName])) {
                            // todo this inheritance will work properly only in single chain, to achieve proper extension
                            //  a full dependency chain has to be resolved first!
                            dependencies[dependencyName] = Resources.Extend(config.data.dependencies[dependencyName],
                                dependencies[dependencyName]);
                        }
                    }
                }
            }
        }
        const depCache = JsonUtils.Clone(this.project.dependencies);
        Resources.Extend(dependencies, depCache);

        this.project.dependencies = <any>{};
        for (dependencyName in dependencies) {
            if (dependencies.hasOwnProperty(dependencyName)) {
                if (!this.project.dependencies.hasOwnProperty(dependencyName)) {
                    let isRequired : boolean = false;
                    if (!ObjectValidator.IsString(dependencies[dependencyName]) &&
                        dependencies[dependencyName].hasOwnProperty("platforms")) {
                        if (!ObjectValidator.IsArray(dependencies[dependencyName].platforms)) {
                            dependencies[dependencyName].platforms = [<string>dependencies[dependencyName].platforms];
                        }
                        (<string[]>dependencies[dependencyName].platforms).forEach(($platforms : string) : void => {
                            if (this.isPlatformMatch($platforms)) {
                                isRequired = true;
                            }
                        });
                    } else {
                        isRequired = true;
                    }
                    if (isRequired) {
                        this.project.dependencies[dependencyName] = dependencies[dependencyName];
                    }
                }
            }
        }
        for (dependencyName in this.project.dependencies) {
            if (this.project.dependencies.hasOwnProperty(dependencyName)) {
                if (ObjectValidator.IsObject(this.project.dependencies[dependencyName])) {
                    this.project.dependencies[dependencyName].name = dependencyName;
                } else {
                    this.project.dependencies[dependencyName] = <any>{
                        location: this.project.dependencies[dependencyName],
                        name    : dependencyName
                    };
                }
                const dependency : IProjectDependency = this.project.dependencies[dependencyName];
                if (dependency.hasOwnProperty("configure-script")) {
                    dependency.configureScript = dependency["configure-script"];
                }
                if (dependency.hasOwnProperty("install-script")) {
                    dependency.installScript = dependency["install-script"];
                }
                let dependencyConf : string = this.properties.projectBase + "/dependencies/" + dependencyName + "/package.conf.jsonp";
                if (!this.fileSystem.Exists(dependencyConf)) {
                    dependencyConf = dependencyConf.slice(0, -1);
                }
                if (!this.fileSystem.Exists(dependencyConf)) {
                    if (ObjectValidator.IsObject(dependency) && (
                        ObjectValidator.IsString(dependency.location) &&
                        StringUtils.StartsWith(<string>dependency.location, "mvn+") ||
                        ObjectValidator.IsObject(dependency.location) &&
                        (<IProjectDependencyLocation>dependency.location).type === "mvn")) {
                        this.properties.javaDependencies.push(dependency);
                        let location : string = "";
                        if (ObjectValidator.IsString(dependency.location)) {
                            location = (<string>dependency.location);
                        } else {
                            location = <string>(<IProjectDependencyLocation>dependency.location).path;
                        }
                        location = location.replace("mvn+", "");

                        this.properties.mavenArtifacts.push(<IMavenArtifact>{
                            artifactId: dependency.name,
                            groupId   : dependency.groupId,
                            location,
                            owner     : this.project.name,
                            version   : dependency.version
                        });
                        this.properties.mavenRepositories.push({
                            id : dependency.groupId + "-" + dependency.name + "-" + dependency.version,
                            url: location
                        });
                    } else if (dependency.hasOwnProperty("configureScript")) {
                        if (!ObjectValidator.IsEmptyOrNull(dependency.location)) {
                            const location : IProjectDependencyLocation[] =
                                DependenciesDownload.getInstance().ParseLocation(dependency.location);
                            if (location.length === 1 && !this.fileSystem.Exists(<string>(location[0].path))) {
                                dependency.location = location[0];
                                dependency.location.path = this.properties.projectBase + "/dependencies/" + dependencyName;
                            }
                        }
                        this.properties.cppDependencies.push(dependency);
                        if (this.properties.cppIncludes !== "") {
                            this.properties.cppIncludes += " ";
                        }
                        if (this.properties.cppLibraries !== "") {
                            this.properties.cppLibraries += " ";
                        }
                        this.properties.cppIncludes += "${" + dependencyName.toUpperCase().replace(/-/, "_") + "_INCLUDE_DIRS}";
                        this.properties.cppLibraries += "${" + dependencyName.toUpperCase().replace(/-/, "_") + "_LIBRARIES}";
                    }
                } else {
                    this.properties.oidisDependencies.push(dependency);
                    this.properties.mavenResourcePaths.push(
                        "${project.basedir}/dependencies/" + dependency.name + "/resource/");
                    if (this.properties.dependenciesConfig.hasOwnProperty(dependency.name)) {
                        const target : IProjectTarget = this.properties.dependenciesConfig[dependency.name].target;
                        if (!ObjectValidator.IsEmptyOrNull(target) &&
                            !ObjectValidator.IsEmptyOrNull(target.toolchainSettings) &&
                            !ObjectValidator.IsEmptyOrNull(target.toolchainSettings.version)) {
                            if (target.toolchainSettings.version === "module") {
                                this.properties.esModuleDependencies.push(dependency);
                            }
                        }
                    }
                }
            }
        }
    }
}
