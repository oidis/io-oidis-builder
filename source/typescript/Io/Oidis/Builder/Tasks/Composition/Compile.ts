/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { CliTaskType } from "../../Enums/CliTaskType.js";
import { GeneralTaskType } from "../../Enums/GeneralTaskType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { TypeScriptCompile } from "../TypeScript/TypeScriptCompile.js";

export class Compile extends BaseTask {

    protected getName() : string {
        return "compile";
    }

    protected async processAsync($option : string) : Promise<void> {
        const taskList : string[] = [GeneralTaskType.VAR_REPLACE];
        if (this.properties.projectHas.TypeScript.Source()) {
            if ($option === CliTaskType.PROD || $option === CliTaskType.EAP) {
                if (this.properties.projectHas.ESModules()) {
                    TypeScriptCompile.getConfig().cache.options.removeComments = false;
                    taskList.push("cache-manager:generate-prod-ts", "cache-manager:prepare-ts");
                }
                taskList.push("typescript-interfaces:source", "typescript-mappings", "typescript-reference:source",
                    "typescript:source");
                if (!this.properties.projectHas.ESModules()) {
                    taskList.push("dev-blocks-cleanup", "uglify:prod", "uglify:loader");
                } else {
                    taskList.push("copy-local-scripts:module-loader", "cache-manager:finalize-prod-ts",
                        "dev-blocks-cleanup");
                }
            } else if ($option === CliTaskType.DEV) {
                const sourceConfig : any = TypeScriptCompile.getConfig().source.options;
                sourceConfig.removeComments = true;
                sourceConfig.sourceMap = true;
                sourceConfig.declaration = true;
                taskList.push("cache-manager:generate-ts",
                    "typescript-interfaces:source", "typescript-mappings", "typescript-reference:source",
                    "cache-manager:prepare-ts", "typescript:source");
                if (!this.properties.projectHas.ESModules()) {
                    taskList.push("uglify:loader");
                } else {
                    const moduleConfig : any = TypeScriptCompile.getConfig().module.options;
                    moduleConfig.removeComments = true;
                    moduleConfig.sourceMap = true;
                    taskList.push("copy-local-scripts:module-loader");
                }
                taskList.push("cache-manager:finalize-ts", "copy-local-scripts:declaration");
            }
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " TypeScript compile skipped: no *.ts files have been found");
        }
        if (this.properties.projectHas.SASS()) {
            taskList.push("sass-reference");
            if ($option === CliTaskType.DEV) {
                taskList.push("cache-manager:generate-sass", "sass-compile:source", "cache-manager:finalize-sass");
            } else {
                taskList.push("sass-compile:dependencies", "sass-compile:source", "cssmin");
            }
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " SASS compile skipped: no *.scss files have been found");
        }
        if (this.project.target.toolchain === ToolchainType.GYP) {
            taskList.push("copy-staticfiles:targetresource", "gyp-compile", "copy-staticfiles:targetresource-postbuild");
        } else {
            if (this.properties.projectHas.Cpp.Source()) {
                taskList.push(
                    "xcpp-interfaces:source", "xcpp-reference:source", "string-replace:xcpp", "copy-staticfiles:targetresource",
                    "xcpp-compile:source", "copy-staticfiles:targetresource-postbuild", "copy-staticfiles:xcppresx"
                );
                if ($option === CliTaskType.PROD) {
                    taskList.push("xcpp-compile:unit");
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " C++ compile skipped: no *.cpp files have been found");
                taskList.push("copy-staticfiles:targetresource");
            }
        }
        if (this.properties.projectHas.Java.Source()) {
            taskList.push("maven-env:init", "maven-build", "maven-env:clean");
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " JAVA compile skipped: no *.java files have been found");
        }
        if (this.properties.projectHas.Python.Source()) {
            taskList.push("python-compile");
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " Python compile skipped: no *.py files have been found");
        }
        if (this.properties.projectHas.Markdown()) {
            taskList.push("markdown-generate");
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " Markdown compile skipped: no *.md files have been found");
        }

        await this.runTaskAsync(taskList);
    }
}
