/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { CliTaskType } from "../../Enums/CliTaskType.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { IPythonPackage } from "../../Interfaces/IPythonPackage.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildProductArgs } from "../../Structures/BuildProductArgs.js";
import { PythonUtils } from "../../Utils/PythonUtils.js";
import { BuildExecutor } from "../BuildProcessor/BuildExecutor.js";
import { StringReplace } from "../Utils/StringReplace.js";

export class CopyLocalScript extends BaseTask {

    protected getName() : string {
        return "copy-local-scripts";
    }

    protected async processAsync($option : string) : Promise<void> {
        if ($option === "phonegap") {
            await this.fileSystem.CopyAsync(
                this.properties.projectBase + "/bin/resource/configs/phonegap.config.xml",
                this.properties.projectBase + "/build/target/config.xml");
        } else if ($option === "pip") {
            const requiresByProject : any = {};
            requiresByProject[this.properties.projectBase] = PythonUtils.getRequiresMap(this.properties.projectBase, "3");
            this.fileSystem.Expand(this.properties.projectBase + "/dependencies/*").forEach(($dependency : string) : void => {
                requiresByProject[$dependency] = PythonUtils.getRequiresMap($dependency, "3");
            });
            let packages : IPythonPackage[] = [];
            Object.keys(requiresByProject).forEach(($path : string) : void => {
                packages = packages.concat(PythonUtils.getPackages($path, requiresByProject[$path]));
            });
            const packageStrings : string[] = packages.map(($pipPackage : IPythonPackage) : string => {
                return PythonUtils.PackageToString($pipPackage);
            });
            this.properties.pythonDependencies = StringUtils.Replace(packageStrings.toString(), ",", "\",\"");
            StringReplace.File(this.properties.projectBase + "/bin/resource/configs/setup.py", StringReplaceType.VARIABLES,
                this.properties.projectBase + "/build/target/setup.py");
            await this.fileSystem.CopyAsync(
                this.properties.projectBase + "/bin/resource/configs/MANIFEST.in",
                this.properties.projectBase + "/build/target/MANIFEST.in");
        } else if ($option === "declarations") {
            if (this.fileSystem.Exists(this.properties.projectBase + "/build/compiled/source/typescript/source.d.ts")) {
                this.fileSystem.Write(this.properties.projectBase + "/build/target/" +
                    "resource/javascript/" + this.properties.packageName + ".d.ts",
                    this.fileSystem.Read(this.properties.projectBase + "/build/compiled/source/typescript/source.d.ts"));
            }
        } else if ($option === "module-loader") {
            let configPath : string = this.properties.projectBase + "/resource/configs/loader.package.json";
            if (!this.fileSystem.Exists(configPath)) {
                const locations : string[] = this.fileSystem.Expand(this.properties.projectBase +
                    "/dependencies/*/resource/configs/loader.package.json");
                if (!ObjectValidator.IsEmptyOrNull(locations)) {
                    configPath = locations[0];
                    if (locations.length > 1) {
                        LogIt.Warning("Multiple loader.package.json files found in dependencies. Using first one from: {0}",
                            configPath);
                    }
                }
            }
            StringReplace.File(configPath, StringReplaceType.VARIABLES, this.properties.projectBase + "/build/target/package.json");
            this.fileSystem.Write(this.properties.dest + "/" + this.properties.loaderPath, this.fileSystem
                .Read(this.properties.projectBase + "/build/compiled/resource/javascript/Loader.js"));
        } else if ($option === "target") {
            const tasks : string[] = ["copy-staticfiles:resource"];
            if (this.properties.projectHas.ESModules() && this.build.type === CliTaskType.PROD) {
                tasks.push("uglify:es");
            }
            tasks.push("copy:html", "copy:license", "scr-generate", "copy:configs");
            await this.runTaskAsync(tasks);
        } else if ($option === "schema") {
            this.fileSystem.Write(this.properties.projectBase + "/build/target/" +
                "resource/configs/package.conf.schema.json", JSON.stringify(this.project.schema, null, 2));
        } else {
            const tasks : string[] = [];
            if (!this.properties.projectHas.ESModules() && this.properties.projectHas.TypeScript.Source()) {
                tasks.push("copy-scripts:typescript");
            }
            if (this.properties.projectHas.Cpp.Source()) {
                tasks.push("copy-scripts:cpp");
            }
            if (this.properties.projectHas.Java.Source()) {
                tasks.push("copy-scripts:java");
                const executor : BuildExecutor = new BuildExecutor();
                executor.getTargetProducts(this.project.target).forEach(($product : BuildProductArgs) : void => {
                    if ($product.Toolchain() === ToolchainType.ECLIPSE) {
                        tasks.push("copy-scripts:eclipse");
                    }
                    if ($product.Toolchain() === ToolchainType.IDEA) {
                        tasks.push("copy-scripts:idea");
                    }
                });
            }
            if (this.properties.projectHas.Python.Source()) {
                tasks.push("copy-scripts:python");
            }
            await this.runTaskAsync(tasks);
        }
    }
}
