/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { IProjectDependency, IProjectDependencyLocation } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { XCppCacheRegister } from "../XCpp/XCppCacheRegister.js";
import { DependenciesDownload } from "./DependenciesDownload.js";

export class InstallLocalScript extends BaseTask {

    private static cacheRegister : any;

    public static SaveRegister() : void {
        if (!ObjectValidator.IsEmptyOrNull(InstallLocalScript.cacheRegister)) {
            Loader.getInstance().getFileSystemHandler().Write(
                Loader.getInstance().getProgramArgs().ProjectBase() + "/build_cache/CacheRegister.json",
                JSON.stringify(InstallLocalScript.cacheRegister));
        }
    }

    protected getName() : string {
        return "install-local-script";
    }

    protected async processAsync($option : string) : Promise<void> {
        const tasks : string[] = [];
        let hasAnyDependency : boolean = false;
        let dependencyName : string;

        if (this.project.hasOwnProperty("dependencies")) {
            if (this.project.dependencies instanceof Array && this.project.dependencies.length > 0) {
                hasAnyDependency = true;
            }
            for (dependencyName in this.project.dependencies) {
                if (this.project.dependencies.hasOwnProperty(dependencyName)) {
                    hasAnyDependency = true;
                    break;
                }
            }
        }

        const dependencyExists : boolean = this.fileSystem.Expand(this.properties.projectBase + "/dependencies/*").length > 0;
        let initProject : boolean = $option === "force" || hasAnyDependency && !dependencyExists;
        if (initProject && hasAnyDependency && !dependencyExists) {
            initProject = false;
            const resolvePath : any = ($value : any) : string => {
                if ((<any>$value).startsWith("dir+")) {
                    $value = StringUtils.Remove($value, "dir+");
                }
                const branchTag : number = StringUtils.IndexOf($value, "#");
                if (branchTag !== -1) {
                    $value = StringUtils.Substring($value, 0, branchTag);
                }
                return $value;
            };
            let path : string[] = [];
            for (dependencyName in this.project.dependencies) {
                if (this.project.dependencies.hasOwnProperty(dependencyName)) {
                    const dependency : IProjectDependency = this.project.dependencies[dependencyName];
                    if (ObjectValidator.IsString(dependency)) {
                        path.push(resolvePath(dependency));
                    } else if (ObjectValidator.IsString(dependency.location)) {
                        path.push(resolvePath(dependency.location));
                    } else if (ObjectValidator.IsObject(dependency.location) && dependency.location.hasOwnProperty("path")) {
                        if (!dependency.location.hasOwnProperty("type") ||
                            (<IProjectDependencyLocation>dependency.location).type !== "mvn") {
                            path = path.concat(new DependenciesDownload().ResolveDependencyPath(
                                (<IProjectDependencyLocation>dependency.location).path));
                        }
                    }
                }
            }

            path.forEach(($value : string) : void => {
                if (StringUtils.StartsWith($value, "$")) {
                    const index : number = StringUtils.IndexOf($value, "$", true, 1);
                    const platform : string = StringUtils.Substring($value, 1, index);
                    $value = StringUtils.Substring($value, index + 1) + "/" + platform;
                }
                if (!StringUtils.StartsWith($value, "mvn+") && !this.fileSystem.Exists($value)) {
                    initProject = true;
                }
            });
        }

        if (!initProject && !this.fileSystem.Exists(this.properties.projectBase + "/.git/hooks/pre-commit")) {
            initProject = true;
        }

        if (initProject || $option === "force") {
            tasks.push("git-manager:hooks", "project-cleanup:bin");
        }

        if (initProject || Loader.getInstance().getEnvironmentArgs().getConfigChanged() && !this.builderConfig.noAutoinstall) {
            if (!this.programArgs.IsAgentTask()) {
                tasks.push("selfinstall");
            }
            tasks.push("dependencies-download", "string-replace:dependencies", "dependencies-sort",
                "copy-local-scripts", "dependencies-install");
            if (this.properties.projectHas.Python.Source() || this.properties.projectHas.Python.Tests()) {
                tasks.push("python-install");
            }
        } else {
            tasks.push("dependencies-sort", "copy-local-scripts");
            const cacheExist : boolean = this.fileSystem.Exists(this.properties.projectBase + "/build_cache/metadata.json");
            if (!cacheExist && !this.build.isTest) {
                tasks.push("dependencies-install");
            } else {
                tasks.push("dependencies-install:configure");
            }
            if (!cacheExist && (this.properties.projectHas.Python.Source() || this.properties.projectHas.Python.Tests())) {
                tasks.push("python-install");
            }
        }
        if (this.properties.projectHas.Cpp.Source()) {
            if (XCppCacheRegister.IsChanged() || initProject) {
                if (XCppCacheRegister.IsCleanRequired()) {
                    tasks.push("project-cleanup:cache");
                }
                tasks.push("copy-scripts:cmake");
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Copy of CMake scripts skipped: " +
                    "project structure changes has not been detected");
            }
        }
        if (initProject) {
            tasks.push("project-cleanup:after-install");
        }

        await this.runTaskAsync(tasks);
    }
}
