/*! ******************************************************************************************************** *
 *
 * Copyright 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import {ColorType} from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { ProductType } from "../../Enums/ProductType.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { IProjectSplashScreen } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { StringReplace } from "../Utils/StringReplace.js";

export class CodeSign extends BaseTask {
    private config : ICodeSignConfig;

    constructor() {
        super();
        this.config = {
            extensions: ["exe", "dll", "node"],
            sign      : {
                args: [
                    "sign",
                    "/v",
                    "/f",
                    "\"<? @var custom.codeSign.certPath ?>\"",
                    "/tr", "<? @var custom.codeSign.timestampServer ?>",
                    "/td", "SHA256",
                    "\"<? @var custom.codeSign.executable ?>\""
                ],
                cmd : "signtool.exe",
                cwd : Loader.getInstance().getProgramArgs().ProjectBase() + "/resource/libs/signtool/"
            },
            verify    : null
        };

        if (EnvironmentHelper.IsMac()) {
            // https://www.unix.com/man-page/osx/1/codesign/
            this.config = {
                extensions: ["dylib", "node"],
                sign      : {
                    args: [
                        "--force",
                        "--sign", "\"<? @var custom.codeSign.identity ?>\"",
                        "--prefix", "\"<? @var custom.codeSign.prefix ?>\"",
                        "--timestamp",
                        "--options runtime,library",
                        "--entitlements", "\"" + this.properties.binBase + "/resource/configs/macos.entitlements\"",
                        "\"<? @var custom.codeSign.executable ?>\""
                    ],
                    cmd : "codesign",
                    cwd : ""
                },
                verify    : {
                    args: [
                        "--verify",
                        "\"<? @var custom.codeSign.executable ?>\""
                    ],
                    cmd : "codesign",
                    cwd : ""
                }
            };
        }
    }

    protected getName() : string {
        return "code-sign";
    }

    protected async processAsync($options? : string) : Promise<void> {
        if (this.project.target.codeSigning.enabled) {
            let forOfflineSelfextractor : boolean = false;
            if (!ObjectValidator.IsEmptyOrNull(this.build.product) && this.build.product.Type() === ProductType.INSTALLER) {
                const url : string = (<IProjectSplashScreen>this.project.target.splashScreen).url;
                if (ObjectValidator.IsEmptyOrNull(url) || ObjectValidator.IsEmptyOrNull(require("url").parse(url).protocol)) {
                    forOfflineSelfextractor = true;
                }
            }

            const files : string[] = [];
            if (!ObjectValidator.IsEmptyOrNull(this.project.target.codeSigning.files)) {
                // TODO(mkelnar) this should be refactored and list should be additive because there are no easy way to add
                //  some binary/assembly in i.e. modules without full list of each files in codeSigning.files
                //  and also resolver should match build/target, build/releases directly
                this.project.target.codeSigning.files.forEach(($file : string) : void => {
                    $file = this.fileSystem.NormalizePath($file);
                    if (StringUtils.StartsWith($file, "./")) {
                        $file = StringUtils.Substring($file, 2);
                    }
                    if (!StringUtils.Contains($file, ":/") && !StringUtils.StartsWith($file, "/")) {
                        $file = this.properties.projectBase + "/" + $file;
                    }
                    if (!forOfflineSelfextractor || forOfflineSelfextractor && !StringUtils.Contains($file, "/build/target/**")) {
                        files.push($file);
                    }
                });
            }

            let executables : string[] = this.fileSystem.Expand(files);
            let extensions : string = "{" + this.config.extensions.join(",") + "}";
            if (!ObjectValidator.IsEmptyOrNull(this.project.target.codeSigning.extensions)) {
                extensions = this.project.target.codeSigning.extensions.join(",");
                if (this.project.target.codeSigning.extensions.length > 1) {
                    extensions = "{" + extensions + "}";
                }
            }
            if (ObjectValidator.IsEmptyOrNull(executables)) {
                executables = this.fileSystem.Expand(this.properties.projectBase + "/build/*." + extensions);
            }
            if (ObjectValidator.IsEmptyOrNull(executables) && !forOfflineSelfextractor) {
                executables = this.fileSystem.Expand(this.properties.projectBase + "/build/target/**/*." + extensions);
            }
            if (ObjectValidator.IsEmptyOrNull(executables)) {
                executables = this.fileSystem.Expand(this.properties.projectBase + "/build/releases/**/*." + extensions);
            }
            const main : string[] = this.fileSystem.Expand(this.properties.projectBase + "/build/**/" + this.project.target.name);
            if (!EnvironmentHelper.IsWindows()) {
                executables = executables.concat(main);
            }

            executables = executables.filter(($item : string, $index : number, $self : string[]) => $self.indexOf($item) === $index);
            LogIt.Debug(executables);

            const cert : string = this.fileSystem.NormalizePath(this.project.target.codeSigning.certPath, true);
            let prefix : string = this.project.target.codeSigning.prefix;
            if (ObjectValidator.IsEmptyOrNull(prefix)) {
                let home : string[] = StringUtils.Split(this.project.homepage, ".", "/", ":", "?")
                    .filter(($value : string) => $value.length > 0);
                if (home.length > 2) {
                    home = home.slice(home.length - 2);
                }
                home = home.reverse();
                prefix = home.join(".").toLowerCase().trim();
                if (home.length > 0) {
                    prefix += ".";
                }
                if (!StringUtils.EndsWith(prefix, ".") && !EnvironmentHelper.IsWindows()) {
                    throw new Error("Prefix for assembly signing has to ends by dot and currently is resolved from homepage in " +
                        "project configuration. Check your homepage: \"" + this.project.homepage + "\" inside project configuration.");
                }
            } else {
                if (!StringUtils.EndsWith(prefix, ".")) {
                    prefix += ".";
                }
            }
            this.project.target.codeSigning.prefix = prefix.toLowerCase();
            StringReplace.getCustomProperties().codeSign = {
                certPath       : cert,
                identity       : cert,
                prefix,
                timestampServer: this.project.target.codeSigning.timestampServer
            };
            for await (const executable of executables) {
                StringReplace.getCustomProperties().codeSign.executable = executable;
                await this.signExecutable(executable);
                await this.verify(executable);
            }
            if (!ObjectValidator.IsEmptyOrNull(this.project.target.codeSigning.notarize) && this.project.target.codeSigning.notarize) {
                if (main.length === 1) {
                    await this.notarize(require("path").dirname(main[0]));
                } else {
                    throw new Error("Multiple mains has been identified by globbing project.target.name in project build project");
                }
            }
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " Code signing skipped: target configuration is missing enabled codeSigning");
        }
    }

    protected async signExecutable($path : string) : Promise<void> {
        if (this.project.target.codeSigning.serverConfig !== "") {
            return this.remoteSign($path);
        }
        return this.localSign($path);
    }

    protected async localSign($executablePath : string, $retry : number = 0) : Promise<void> {
        if (EnvironmentHelper.IsWindows() || EnvironmentHelper.IsMac()) {
            if (this.fileSystem.Exists($executablePath)) {
                if (!this.fileSystem.IsFileLocked($executablePath)) {
                    const cmd : string = StringReplace.Content(this.config.sign.cmd, StringReplaceType.VARIABLES);
                    const args : string[] = this.config.sign.args.map(
                        ($item : string) => StringReplace.Content($item, StringReplaceType.VARIABLES));

                    const result : IExecuteResult = await this.terminal.SpawnAsync(cmd, args, this.config.sign.cwd);

                    if (result.exitCode !== 0) {
                        LogIt.Error("Code signing has failed");
                    } else {
                        LogIt.Info(">>"[ColorType.YELLOW] + " Target has been signed successfully");
                    }
                } else {
                    await this.delay(500);
                    if ($retry < 10) {
                        await this.localSign($executablePath, $retry + 1);
                    } else {
                        LogIt.Error("Code signing has failed: target file is being used by another process");
                    }
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Code signing skipped: executable does not exit");
            }
        } else {
            LogIt.Warning(">>"[ColorType.YELLOW] + " Code signing skipped: signing is NOT supported at Linux environment");
        }
    }

    protected async remoteSign($executablePath : string) : Promise<void> {
        return new Promise<void>(($resolve : () => void, $reject : ($error : Error) => void) : void => {
            const path : any = require("path");
            const fs : any = require("fs");

            const sign : any = ($config : any, $callback : () => void) : void => {
                let body : string = "";
                for (const variable in $config.request.settings) {
                    if ($config.request.settings.hasOwnProperty(variable)) {
                        if (body !== "") {
                            body += "&";
                        }
                        body += variable + "=" + encodeURIComponent($config.request.settings[variable]);
                    }
                }
                this.fileSystem.Download(<any>{
                    body,
                    headers     : {
                        "Accept"                   : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                        "Accept-Language"          : "en-US,en;q=0.5",
                        "Connection"               : "keep-alive",
                        "Content-Length"           : body.length,
                        "Content-Type"             : "application/x-www-form-urlencoded",
                        "Upgrade-Insecure-Requests": "1"
                    },
                    method      : $config.request.method,
                    streamOutput: true,
                    url         : $config.request.url
                }, ($headers : string, $body : string) : void => {
                    LogIt.Debug($body.toString());
                    $callback();
                });
            };

            let counter : number = 0;
            const download : any = ($source : string, $dest : string) : void => {
                if (counter < 25) {
                    Echo.Print(".");
                } else {
                    Echo.Println(".");
                    counter = 0;
                }
                if (counter === 0) {
                    LogIt.Info("Waiting for signed file: " + $source);
                }
                if (fs.existsSync($source)) {
                    Echo.Println("");
                    try {
                        LogIt.Info("Downloading from " + $source + " to " + $dest);
                        fs.copyFileSync($source, $dest);
                        fs.rmSync($source, {force: true, maxRetries: 10, retryDelay: 1000, recursive: true});
                        $resolve();
                    } catch (ex) {
                        $reject(ex);
                    }
                } else {
                    setTimeout(() : void => {
                        counter++;
                        download($source, $dest);
                    }, 10000);
                }
            };

            const getConfig : any = ($callback : ($config : any) => void) : void => {
                let serverConfig : string = this.project.target.codeSigning.serverConfig;
                if (!StringUtils.Contains(serverConfig, "http://", "https://", "file://") &&
                    !StringUtils.StartsWith(serverConfig, "/")) {
                    serverConfig = "file:///" + this.properties.projectBase + "/" + serverConfig;
                }
                this.fileSystem.Download(<any>{
                    streamOutput: true,
                    url         : serverConfig
                }, ($headers : string, $body : string) : void => {
                    $body = StringUtils.Replace($body, "<? @var this.fileName ?>",
                        this.getUID() + path.extname($executablePath));
                    $body = StringReplace.Content($body, StringReplaceType.VARIABLES);
                    try {
                        $callback(eval("(" + $body + ")"));
                    } catch (ex) {
                        $reject(ex);
                    }
                });
            };

            if (this.fileSystem.Exists($executablePath)) {
                getConfig(($config : any) : void => {
                    if (!fs.existsSync($config.uploadUrl)) {
                        LogIt.Info("Uploading from " + $executablePath + " to " + $config.uploadUrl);
                        fs.copyFileSync($executablePath, $config.uploadUrl);
                        try {
                            sign($config, () : void => {
                                Echo.Println("Waiting for sign process ...");
                                download($config.downloadUrl, $executablePath);
                            });
                        } catch (ex) {
                            LogIt.Error("Unable to upload file.", ex);
                        }
                    } else {
                        sign(() : void => {
                            Echo.Println("Waiting for sign process ...");
                            download($config.downloadUrl, $executablePath);
                        });
                    }
                });
            } else {
                $reject(new Error("File does not exist"));
            }
        });
    }

    protected async verify($binaryPath : string) : Promise<void> {
        if (EnvironmentHelper.IsMac()) {
            if (this.fileSystem.Exists($binaryPath)) {
                const cmd : string = StringReplace.Content(this.config.verify.cmd, StringReplaceType.VARIABLES);
                const args : string[] = this.config.verify.args.map(
                    ($item : string) => StringReplace.Content($item, StringReplaceType.VARIABLES));

                const result : IExecuteResult = await this.terminal.SpawnAsync(cmd, args, this.config.verify.cwd);

                if (result.exitCode !== 0) {
                    LogIt.Error("Signed assembly verification has failed.");
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " Assembly signature verification succeed.");
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Code signature verification skipped: executable does not exit");
            }
        }
    }

    protected async notarize($pkg : string) : Promise<void> {
        if (EnvironmentHelper.IsMac()) {
            if (ObjectValidator.IsEmptyOrNull(this.project.target.codeSigning.username)) {
                throw new Error("Notarization requires username (project.target.codeSigning.username)");
            }
            if (ObjectValidator.IsEmptyOrNull(this.project.target.codeSigning.password)) {
                throw new Error("Notarization requires password (project.target.codeSigning.password)");
            }
            if (ObjectValidator.IsEmptyOrNull(this.project.target.codeSigning.bundleId)) {
                this.project.target.codeSigning.bundleId = this.project.target.codeSigning.prefix + this.project.target.name;
                LogIt.Info("Notarization requires bundleId (project.target.codeSigning.bundleId) which is " +
                    "missing in project config so project.target.codeSigning.prefix + \".\" + project.target.name " +
                    "will be used instead. However, undefined prefix will be resolved form project.homepage. " +
                    "So resolved bundleId is \"" + this.project.target.codeSigning.bundleId + "\" and if this is not what you want " +
                    "then provide proper value in configuration.");
            }
            this.project.target.codeSigning.bundleId = this.project.target.codeSigning.bundleId.toLowerCase();

            const tmpPkg : string = this.fileSystem.getTempPath() + "/" + Loader.getInstance().getEnvironmentArgs().getProjectName() +
                "/notarize/" + StringUtils.getSha1(this.project.name) + ".zip";  // zip/pkg/dmg are only allowed for notarization
            let execResult : IExecuteResult = await this.terminal.SpawnAsync(
                "ditto",
                ["-c", "-k", "--keepParent", $pkg, tmpPkg],
                $pkg);
            if (execResult.exitCode !== 0) {
                throw new Error("Failed to prepare package for notarization.");
            }
            execResult = await this.terminal.SpawnAsync("xcrun",
                [
                    "altool",
                    "--notarize-app",
                    "--primary-bundle-id", this.project.target.codeSigning.bundleId,
                    "--username", this.project.target.codeSigning.username,
                    "--password", this.project.target.codeSigning.password,
                    "--file", tmpPkg
                ], {cwd: $pkg, advanced: {noTerminalLog: true}});
            if (execResult.exitCode !== 0) {
                throw new Error("Failed to send package for notarization.");
            }

            if (!ObjectValidator.IsEmptyOrNull(this.project.target.codeSigning.noWait) && this.project.target.codeSigning.noWait) {
                LogIt.Warning("Notarization status checker is disabled by project.target.codeSigning.noWait option set to true." +
                    "Please wait for automatic email notification by Apple server and check result manually " +
                    "before package deployment. (There is no back propagation result to local package from server " +
                    "because it is just check and passed package is registered on Apple server for future check by Gatekeeper.)");
            } else {
                const match : RegExpMatchArray = /RequestUUID\s*=\s*((?:[a-zA-Z0-9]+-?)+)/g.exec(execResult.std[0]);
                if (ObjectValidator.IsEmptyOrNull(match) || match.length !== 2) {
                    throw new Error("Notarization request ID could not be found in response.");
                }
                const requestId : string = match[1];
                LogIt.Info("Starting notarization status check (" + requestId + "), this can take several minutes...\n\t" +
                    "(use project.target.codeSigning.noWait to suppress this check)");
                let checkRunning : boolean = true;
                let index : number = 12;
                const waitFor : number = 10000;
                do {
                    await this.delay(waitFor);
                    if (--index > 0) {
                        LogIt.Info("...next check in " + (waitFor * index) / 1000 + " seconds...");
                    } else {
                        LogIt.Info("...checking notarization progress...");
                        execResult = await this.terminal.SpawnAsync("xcrun", [
                                "altool",
                                "--notarization-info", requestId,
                                "--username", this.project.target.codeSigning.username,
                                "--password", this.project.target.codeSigning.password
                            ],
                            {cwd: $pkg, advanced: {noTerminalLog: true}});
                        if (execResult.exitCode === 0) {
                            if (execResult.std[0].match(/status:\s*in\sprogress/gi)) {
                                index = 12;
                            } else {
                                checkRunning = false;
                                if (execResult.std[0].match(/status:\s*success/gi)) {
                                    LogIt.Info("Package notarization succeed. You can deploy package (oidis target-deploy).");
                                } else {
                                    throw new Error("Package notarization failed. See link above or arrived mail for more info.");
                                }
                            }
                        } else {
                            throw new Error("Notarization progress check failed: " + execResult.std.join());
                        }
                    }
                } while (checkRunning);
            }
        } else {
            LogIt.Warning("Package bundle notarization is not supported on other than MacOS platform. Skipping...");
        }
    }

    private async delay($ms : number) : Promise<void> {
        return new Promise<void>(($resolve : () => void) : void => {
            setTimeout($resolve, $ms);
        });
    }
}

export interface ICodeSignConfig {
    sign : ISignToolConfig;
    verify : ISignToolConfig;
    extensions : string[];
}

export interface ISignToolConfig {
    cmd : string;
    args : string[];
    cwd : string;
}

// generated-code-start
export const ICodeSignConfig = globalThis.RegisterInterface(["sign", "verify", "extensions"]);
export const ISignToolConfig = globalThis.RegisterInterface(["cmd", "args", "cwd"]);
// generated-code-end
