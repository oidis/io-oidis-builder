/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { EventType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/Events/EventType.js";
import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { ArrayList } from "@io-oidis-commons/Io/Oidis/Commons/Primitives/ArrayList.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { StdinManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/StdinManager.js";
import { BuilderHubConnector, IBuilderAgent } from "../../Connectors/BuilderHubConnector.js";
import { BuilderServiceConnector } from "../../Connectors/BuilderServiceConnector.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuildProductArgs } from "../../Structures/BuildProductArgs.js";
import { ProgramArgs } from "../../Structures/ProgramArgs.js";
import { BuildExecutor, IReleaseProperties } from "../BuildProcessor/BuildExecutor.js";
import { IBuilderTaskProtocol } from "../Servers/BuilderServer.js";

export class BuilderClient extends BaseTask {
    public static Register : ArrayList<ITaskRegisterRecord>;

    protected getName() : string {
        return "builder-client";
    }

    protected process($done : any, $option : string) : void {
        const stdinManager : StdinManager = new StdinManager();

        const isAgent : boolean = $option === "agent";
        let currentArgs : string[] = this.programArgs.getOptions().forwardArgs;
        if (ObjectValidator.IsEmptyOrNull(currentArgs)) {
            currentArgs = this.programArgs.getTasks().concat("--chain-id=\"" + this.programArgs.ChainId() + "\"");
        }
        const forwardArgs : ProgramArgs = new ProgramArgs();
        forwardArgs.Parse(currentArgs);
        const isBaseTask : boolean = forwardArgs.IsBaseTask();

        let base : string = this.properties.projectBase;
        let workspace : string;
        for (workspace in this.builderConfig.workspaces) {
            if (this.builderConfig.workspaces.hasOwnProperty(workspace)) {
                const path : string = this.builderConfig.workspaces[workspace];
                if (path !== "" && base.indexOf(path) !== -1) {
                    base = base.replace(path, "[WORKSPACE_" + workspace + "]/");
                    break;
                }
            }
        }

        BuilderClient.Register = new ArrayList<ITaskRegisterRecord>();
        const handleMessage : any = ($message : IBuilderTaskProtocol, $taskId : string) : void => {
            try {
                if (ObjectValidator.IsObject($message)) {
                    const task : ITaskRegisterRecord = BuilderClient.Register.getItem($taskId);
                    switch ($message.type) {
                    case EventType.ON_START:
                        const onStartArgs : ProgramArgs = new ProgramArgs(); // eslint-disable-line no-case-declarations
                        onStartArgs.Parse($message.data.split(" "));
                        if (!onStartArgs.IsBaseTask()) {
                            LogIt.Info("Oidis Builder task has been accepted" + (isAgent ? " with id: " + $taskId : "."));
                        }
                        break;
                    case EventType.ON_CHANGE:
                        if (ObjectValidator.IsString($message.data)) {
                            if (isAgent && !isBaseTask && !ObjectValidator.IsEmptyOrNull(task.platform)) {
                                const prefix : string = "[" + task.platform + "] ";
                                $message.data = StringUtils.Replace($message.data, "\n", "\n" + prefix);
                            }
                            Echo.Print($message.data);
                        } else {
                            stdinManager.setQuestions([
                                {
                                    callback: () : void => {
                                        // default callback
                                    },
                                    hidden  : $message.data.hidden,
                                    prefix  : Buffer.from($message.data.prefix, "base64").toString("utf8")
                                }
                            ]);
                            stdinManager.NextQuestion();
                        }
                        break;
                    case EventType.ON_COMPLETE:
                        const onCompleteArgs : ProgramArgs = new ProgramArgs(); // eslint-disable-line no-case-declarations
                        onCompleteArgs.Parse($message.data.cmd.split(" "));
                        if (!onCompleteArgs.IsBaseTask()) {
                            let message : string = "Oidis Builder task has been completed with " +
                                "exit code: " + $message.data.exitCode;
                            if (ObjectValidator.IsDigit($message.data.taskMinutes)) {
                                message += " in " + $message.data.taskMinutes + "ms";
                                if (ObjectValidator.IsDigit($message.data.buildMinutes)) {
                                    message += " from " + Convert.TimeToLongString($message.data.buildMinutes);
                                }
                            }
                            LogIt.Info(message);
                        }
                        if (!ObjectValidator.IsEmptyOrNull($message.data.exitCode)) {
                            if (isAgent && !isBaseTask) {
                                task.completed = true;
                                task.exitCode = $message.data.exitCode;
                                let completed : boolean = true;
                                BuilderClient.Register.foreach(($task : ITaskRegisterRecord) : void => {
                                    if (!$task.completed) {
                                        completed = false;
                                    }
                                });
                                if (completed) {
                                    $done();
                                }
                            } else {
                                Loader.getInstance().Exit($message.data.exitCode);
                            }
                        } else {
                            $done();
                        }
                        break;
                    default:
                        LogIt.Debug($message);
                        break;
                    }
                } else if (!ObjectValidator.IsEmptyOrNull($message)) {
                    LogIt.Debug($message);
                }
            } catch (ex) {
                LogIt.Debug("Unable to handle client message. {0}", ex.message);
            }
        };

        if (isAgent) {
            if (isBaseTask) {
                LogIt.setLevel(LogLevel.ERROR);
            }
            const connector : BuilderHubConnector = new BuilderHubConnector();
            if (base.indexOf("[WORKSPACE_") === -1) {
                base = "[CLOUDBASE_" + this.builderConfig.hub.user + "]/" + require("path").basename(base);
            }

            const platforms : string[] = [];
            const executor : BuildExecutor = new BuildExecutor();
            executor.getReleases().forEach(($release : IReleaseProperties) : void => {
                executor.getTargetProducts($release.target).forEach(($platform : BuildProductArgs) : void => {
                    platforms.push($platform.Value());
                });
            });
            let responseIndex : number = 0;
            let agent : IBuilderAgent = this.builderConfig.agent;
            if (ObjectValidator.IsEmptyOrNull(agent)) {
                agent = <any>{};
            }
            if (!ObjectValidator.IsEmptyOrNull(this.project.builder.location) &&
                this.project.builder.location !== "agent") {
                agent.name = this.project.builder.location;
            }
            if (!ObjectValidator.IsEmptyOrNull(this.project.builder.version)) {
                agent.version = this.project.builder.version;
            }
            if (ObjectValidator.IsEmptyOrNull(agent.version)) {
                agent.version = "latest";
            }
            connector
                .RunTask({
                    agent,
                    args: currentArgs.concat([
                        "--project", this.project.name, "--base", base, "--agent-task"
                    ]),
                    platforms
                })
                .OnError(($message : string) : void => {
                    LogIt.Error("Task has been rejected by Oidis Hub: " + $message);
                })
                .OnMessage(handleMessage)
                .Then(($status : boolean, $builderId? : string, $taskId? : string) : void => {
                    if ($status) {
                        if (!isBaseTask) {
                            LogIt.Info("Task has been accepted by Oidis Hub. Forwarding to builder with suitable platform.");
                            BuilderClient.Register.Add({
                                completed: false,
                                exitCode : 0,
                                platform : platforms[responseIndex]
                            }, $taskId);
                        }
                        responseIndex++;
                        stdinManager.setOnKeyPress(($value : string) : void => {
                            connector.ForwardMessage($builderId, <IBuilderTaskProtocol>{
                                data  : $value,
                                taskId: $taskId
                            });
                        });
                    } else {
                        LogIt.Error("Task has been rejected by Oidis Hub: builder with suitable platform has not been found");
                    }
                });
        } else {
            const connector : BuilderServiceConnector =
                new BuilderServiceConnector(this.project.builder.port, this.project.builder.location);
            connector
                .RunTask(currentArgs.concat(["--base", base, "--service-task"]))
                .Then(handleMessage);
            stdinManager.setOnKeyPress(($value : string) : void => {
                connector.RunTask([$value]).Then(handleMessage);
            });
        }
    }
}

export interface ITaskRegisterRecord {
    completed : boolean;
    exitCode : number;
    platform : string;
}

// generated-code-start
export const ITaskRegisterRecord = globalThis.RegisterInterface(["completed", "exitCode", "platform"]);
// generated-code-end
