/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import {ColorType} from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { BuilderClient, ITaskRegisterRecord } from "./BuilderClient.js";

export class ClientReport extends BaseTask {

    protected getName() : string {
        return "client-report";
    }

    protected async processAsync() : Promise<void> {
        LogIt.Info("-------------------------------"[ColorType.GREEN]);
        LogIt.Info("Build report for all platforms:"[ColorType.GREEN]);
        let success : boolean = true;
        BuilderClient.Register.foreach(($task : ITaskRegisterRecord) : void => {
            LogIt.Info("[" + $task.platform + "] exit code: " + $task.exitCode);
            if ($task.exitCode !== 0) {
                success = false;
            }
        });
        Loader.getInstance().Exit(success ? 0 : -1);
    }
}
