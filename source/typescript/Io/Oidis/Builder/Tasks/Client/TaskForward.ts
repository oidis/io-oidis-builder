/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { ProgramArgs } from "../../Structures/ProgramArgs.js";

export class TaskForward extends BaseTask {

    protected getName() : string {
        return "forward-task";
    }

    protected async processAsync() : Promise<void> {
        let chain : string[] = [];
        if (this.programArgs.IsHubClient()) {
            if (ObjectValidator.IsEmptyOrNull(this.builderConfig.hub.url)) {
                LogIt.Error("Unable to forward task to Oidis Hub due to missing hub url at builder configuration.");
            }
            let currentArgs : string[] = this.programArgs.getOptions().forwardArgs;
            if (ObjectValidator.IsEmptyOrNull(currentArgs)) {
                currentArgs = this.programArgs.getTasks();
            }
            const agentArgs : ProgramArgs = new ProgramArgs();
            agentArgs.Parse(currentArgs);
            if (!agentArgs.IsBaseTask() && !agentArgs.getOptions().noTarget) {
                if (agentArgs.IsAgentTask()) {
                    chain = agentArgs.getTasks();
                } else if (agentArgs.IsRun()) {
                    chain = ["run", "builder-client:agent"];
                } else {
                    chain = ["cloud-sync-manager:push", "builder-client:agent", "cloud-sync-manager:pull", "client-report"];
                }
            } else {
                chain = ["builder-client:agent"];
            }
        } else {
            chain = ["builder-client:service"];
        }
        await this.runTaskAsync(chain);
    }
}
