/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { GitManager } from "../Utils/GitManager.js";

export class ConfigCleanup extends BaseTask {

    protected getName() : string {
        return "config-cleanup";
    }

    protected async processAsync($option : string) : Promise<void> {
        if ($option === "version") {
            if (GitManager.IsGitRepo()) {
                const tagSha : string = await GitManager.getGitValue([
                    "rev-list", "--tags", "--max-count=1"
                ], "latest tag sha");
                const tag : string = await GitManager.getGitValue([
                    "describe", "--tags", tagSha
                ], "latest tag name");
                if (ObjectValidator.IsEmptyOrNull(tagSha) || ObjectValidator.IsEmptyOrNull(tag)) {
                    LogIt.Info(">>"[ColorType.YELLOW] + " version check skipped: can not identify any tag for validation");
                    return;
                }
                const branch : string = await GitManager.getGitValue([
                    "branch", "--show-current"
                ], "current branch name");
                const revSha : string = await GitManager.getGitValue([
                    "rev-parse", "HEAD"
                ], "current revision sha");

                LogIt.Info("> validating version for " +
                    "branch " + branch + " on sha " + revSha + " against latest tag " + tag + " on sha " + tagSha);

                if (tagSha !== revSha) {
                    /// TODO: any conditional validation for master/main/release?
                    if (StringUtils.VersionIsLower(this.project.version, tag) || this.project.version === tag) {
                        const message : string = "Current repository must have higher version than latest tag " + tag + ".";
                        if (!this.project.lintErrorsAsWarnings) {
                            LogIt.Error(message);
                        } else {
                            LogIt.Warning("Suppressed lint error: " + message);
                            return;
                        }
                    }
                }
                LogIt.Info(">>"[ColorType.YELLOW] + " version check passed");
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " Not a git repository: skipping version check");
            }
            return;
        }
        this.fileSystem.Expand([
            this.properties.projectBase + "/package.conf.json*",
            this.properties.projectBase + "/private.conf.json*",
            this.properties.projectBase + "/bin/project/**/*.jsonp"
        ]).forEach(($configPath : string) : void => {
            if (this.fileSystem.Exists($configPath)) {
                let content : string = this.fileSystem.Read($configPath).toString();
                const contentCrc : number = StringUtils.getCrc(content);
                [
                    {old: "install-script", new: "installScript"},
                    {old: "configure-script", new: "configureScript"},
                    {old: "skip-replace", new: "skipReplace"}
                ].forEach(($keyword : any) : void => {
                    let oldKeyword : string;
                    let newKeyword : string;
                    if (StringUtils.Contains(content, "\"" + $keyword.old + "\":")) {
                        oldKeyword = "\"" + $keyword.old + "\":";
                        if (StringUtils.EndsWith($configPath, ".jsonp")) {
                            newKeyword = $keyword.new + ":";
                        } else {
                            newKeyword = "\"" + $keyword.new + "\":";
                        }
                    } else if (StringUtils.Contains(content, $keyword.old + ":")) {
                        oldKeyword = $keyword.old + ":";
                        newKeyword = $keyword.new + ":";
                    }
                    if (!ObjectValidator.IsEmptyOrNull(oldKeyword)) {
                        content = StringUtils.Replace(content, oldKeyword, newKeyword);
                    }
                });
                if (contentCrc !== StringUtils.getCrc(content)) {
                    this.fileSystem.Write($configPath, content);
                    LogIt.Info(">>"[ColorType.RED] + " " + $configPath + " has been cleaned.");
                }
            }
        });
    }
}
