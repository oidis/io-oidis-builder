/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class ProjectCleanup extends BaseTask {
    /// TODO: create task for creation of .oidis folder,
    /// TODO: where will be moved package-*.json files and content of bin folder moved to .oidis/shared
    private insecureWarning : boolean;

    constructor() {
        super();
        this.insecureWarning = false;
    }

    protected getName() : string {
        return "project-cleanup";
    }

    protected async processAsync($option : string) : Promise<void> {
        const config : any = {
            bin      : ["bin/**/*.*", "!bin/project/**/*.*"],
            init     : ["build/releases"],
            build    : [
                "build/compiled", "build/apptarget", "build/webtarget", "build/target", "build/reports",
                "build/*.*"
            ],
            source   : [
                "source/typescript/**/*.js", "source/typescript/**/*.js.map", "source/typescript/**/*.d.ts",
                "!source/typescript/**/reference.d.ts",
                "!source/typescript/**/namespacesMap.d.ts",
                "resource/sass/**/*.compiled.d.scss"
            ],
            test     : [
                "test/unit/**/*.js", "test/unit/**/*.js.map", "test/unit/**/*.d.ts",
                "!test/unit/**/reference.d.ts"
            ],
            prod     : [
                "build/**/target/test", "build/**/target/**/*.*.map", "build/**/connector.config.jsonp",
                "build/**/target/UnitTestRunner*"
            ],
            cache    : ["build_cache/**/*", "!build_cache/package.lock.json"],
            cache_ts : ["build_cache/**/*.ts", "build_cache/**/*.js"],
            cache_css: ["build_cache/**/*.css"]
        };

        if (ObjectValidator.IsEmptyOrNull($option) || $option === "after-install") {
            const tasks : string[] = [
                "dependencies-sort", "structure-cleanup", "config-cleanup", "scr-cleanup",
                "typescript-autocleanup", "sass-cleanup", "git-cleanup"
            ];
            if (ObjectValidator.IsEmptyOrNull($option)) {
                tasks.push("copyrights-cleanup", "config-cleanup:version");
            } else {
                tasks.push("copyrights-cleanup:init");
            }
            await this.runTaskAsync(tasks);
        } else if (config.hasOwnProperty($option) || $option === "all") {
            const clean : any = async ($pattern : string[]) : Promise<void> => {
                const files : string[] = this.fileSystem.Expand($pattern);
                for await (const file of files) {
                    try {
                        await this.fileSystem.DeleteAsync({
                            ignoreLocked : true,
                            path         : file,
                            statusHandler: ($message : string, $type : LogLevel | string, $severity : LogSeverity) : void => {
                                if ($type === LogLevel.WARNING) {
                                    const containsInsecure : boolean = StringUtils.Contains($message, "running insecurely");
                                    if (!containsInsecure || containsInsecure && !this.insecureWarning) {
                                        if (containsInsecure) {
                                            this.insecureWarning = true;
                                        }
                                        LogIt.Warning($message);
                                    }
                                } else if ($type === LogLevel.INFO) {
                                    LogIt.Info($message, $severity);
                                } else if ($type === LogLevel.ERROR) {
                                    LogIt.Error($message);
                                } else if ($type === "print") {
                                    Echo.Print($message);
                                } else if ($type === "println") {
                                    Echo.Println($message);
                                } else {
                                    LogIt.Debug($message);
                                }
                            }
                        });
                    } catch (ex) {
                        LogIt.Warning(ex.message);
                    }
                }
            };
            if ($option === "all") {
                const patterns : string[] = [];
                let configName : string;
                for (configName in config) {
                    if (config.hasOwnProperty(configName)) {
                        patterns.push(config[configName]);
                    }
                }
                for await (const pattern of patterns) {
                    await clean(pattern);
                }
            } else {
                await clean(config[$option]);
            }
        } else {
            LogIt.Error("Unsupported cleanup option \"" + $option + "\".");
        }
    }
}
