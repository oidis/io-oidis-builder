/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseTask } from "../../Primitives/BaseTask.js";

export class DevBlocksCleanup extends BaseTask {

    protected getName() : string {
        return "dev-blocks-cleanup";
    }

    protected async processAsync($option : string) : Promise<void> {
        let source : string = this.properties.projectBase + "/build/compiled/**/typescript/**/source.js";
        if (this.properties.projectHas.ESModules()) {
            source = this.properties.projectBase + "/build/target/resource/esmodules/**/*.js";
        }
        const files : string[] = this.fileSystem.Expand(source);
        files.forEach(($file : string) : void => {
            const startTag : string = "/* dev_start */".replace("_", ":");
            const endTag : string = "/* dev_end */".replace("_", ":");

            const blocks : string[] = this.fileSystem.Read($file).toString()
                .replace(/^\/\* dev:start \*\/[\s\S]*?\/\* dev:end \*\/$/igm, "")
                .split(endTag);
            let blockIndex : number;
            const blocksLength : number = blocks.length;
            for (blockIndex = 0; blockIndex < blocksLength; blockIndex++) {
                const block : string = blocks[blockIndex];
                const parts : any[] = block.split(startTag);
                let partIndex : number;
                const partsLength : number = parts.length;
                for (partIndex = 1; partIndex < partsLength; partIndex++) {
                    let firstLine : string = parts[partIndex].substring(2, 140).replace(/".*"/g, "\"\"");
                    if (firstLine.indexOf("\n") !== -1) {
                        firstLine = firstLine.substring(0, firstLine.indexOf("\n"));
                    }
                    const startsWithFunction : boolean = (firstLine.match(/.* function .*\(.*\) {$/m) || []).length > 0;
                    if (!startsWithFunction || startsWithFunction && firstLine.indexOf("var ") !== -1) {
                        if (parts[partIndex].indexOf("return _this;") !== -1) {
                            parts[partIndex] = "return _this;";
                        } else {
                            parts[partIndex] = "";
                        }
                    } else {
                        const part : string = parts[partIndex];
                        const partSize : number = part.length;
                        let charIndex : number = 0;
                        let codeBlockIndex : number = -1;
                        let functionFound : boolean = false;
                        let isString : boolean = false;
                        while (!functionFound && charIndex < partSize) {
                            if (!isString) {
                                if (part[charIndex] === "\"") {
                                    isString = true;
                                } else {
                                    if (part[charIndex] === "{") {
                                        if (codeBlockIndex === -1) {
                                            codeBlockIndex = 1;
                                        } else {
                                            codeBlockIndex++;
                                        }
                                    }
                                    if (part[charIndex] === "}") {
                                        codeBlockIndex--;
                                    }
                                    if (codeBlockIndex === 0 && part[charIndex + 1] === ";") {
                                        functionFound = true;
                                        parts[partIndex] = part.substring(charIndex + 2);
                                    }
                                }
                            } else if (part[charIndex] === "\"" && (part[charIndex - 1] !== "\\" || part[charIndex - 2] === "\\")) {
                                isString = false;
                            }
                            charIndex++;
                        }
                    }
                }
                blocks[blockIndex] = parts.join("");
            }
            this.fileSystem.Write($file, blocks.join(""));
        });
    }
}
