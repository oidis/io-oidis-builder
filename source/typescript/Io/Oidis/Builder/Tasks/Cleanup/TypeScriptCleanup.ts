/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseTask } from "../../Primitives/BaseTask.js";
import { TypeScriptLint } from "../TypeScript/TypeScriptLint.js";

export class TypeScriptCleanup extends BaseTask {
    private autofixState : boolean;

    constructor() {
        super();
        this.autofixState = false;
    }

    protected getName() : string {
        return "typescript-autocleanup";
    }

    protected async processAsync($option : string) : Promise<void> {
        if ($option === "restore") {
            TypeScriptLint.getConfig().options.fix = this.autofixState;
        } else {
            this.autofixState = TypeScriptLint.getConfig().options.fix;
            TypeScriptLint.getConfig().options.fix = true;
            await this.runTaskAsync("tslint", "typescript-autocleanup:restore");
        }
    }
}
