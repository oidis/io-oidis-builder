/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class StructureCleanup extends BaseTask {
    private config : IProjectCleanupConfig;
    private cleanUpLog : string;

    protected getName() : string {
        return "structure-cleanup";
    }

    protected async processAsync($option : string) : Promise<void> {
        const os : any = require("os");
        this.config = JSON.parse(this.fileSystem.Read(
            this.properties.binBase + "/resource/configs/project-cleanup.conf.json").toString());
        const configDestination : string = this.properties.projectBase + "/project-cleanup.conf.json";
        if (this.fileSystem.Exists(configDestination)) {
            const projectConfig : IProjectCleanupConfig = JSON.parse(this.fileSystem.Read(configDestination).toString());
            let configBlock : string;
            for (configBlock in projectConfig) {
                if (projectConfig.hasOwnProperty(configBlock)) {
                    this.config[configBlock] = projectConfig[configBlock];
                }
            }
        }

        this.cleanUpLog = "";
        const target : string = "__cleanup";
        this.fileSystem.Delete(this.properties.projectBase + "/" + target);
        if (this.config.hasOwnProperty("targetFolder") && this.config.targetFolder !== "") {
            if (this.fileSystem.Exists(this.properties.projectBase + "/" + this.config.targetFolder)) {
                this.logIt("clean up target: " + this.config.targetFolder);
                this.fileSystem.Delete(this.properties.projectBase + "/" + this.config.targetFolder);
            }
        }

        const filePaths : string[] = this.config.filePaths;
        const replacements : string[] = this.config.contentReplacements;
        try {
            let files : string[] = this.fileSystem.Expand(this.getExpandPattern());

            let index : number;
            let filePath : string;
            let destinationPath : string;
            let origPath : string;
            let newPath : string;
            let replacement : string;
            const cleanedFiles : string[] = [];
            for (origPath in filePaths) {
                if (!replacements.hasOwnProperty(origPath)) {
                    replacements[origPath] = filePaths[origPath];
                }
            }

            for (index = 0; index < files.length; index++) {
                filePath = files[index];
                if (this.isIgnored(filePath)) {
                    continue;
                }
                if (this.fileSystem.IsFile(filePath)) {
                    destinationPath = this.properties.projectBase + "/" +
                        target + "/" + StringUtils.Remove(filePath, this.properties.projectBase + "/");
                    for (origPath in filePaths) {
                        if (filePaths.hasOwnProperty(origPath)) {
                            newPath = filePaths[origPath];
                            if (filePath.indexOf(origPath) !== -1) {
                                destinationPath = destinationPath.replace(origPath, newPath);
                            }
                            break;
                        }
                    }
                    this.logIt("copy file " + filePath + " to " + destinationPath);
                    this.fileSystem.Write(destinationPath, this.fileSystem.Read(filePath));

                    let readFile : boolean = false;
                    if (this.config.hasOwnProperty("readExtensions")) {
                        for (const extension of this.config.readExtensions) {
                            if ((<any>destinationPath).endsWith(extension)) {
                                readFile = true;
                                break;
                            }
                        }
                    }
                    if (readFile) {
                        this.logIt("processing file: " + filePath);
                        let data : string = this.fileSystem.Read(destinationPath).toString();
                        const origData : string = data;
                        for (replacement in replacements) {
                            if (replacements.hasOwnProperty(replacement)) {
                                data = data.replace(new RegExp(this.escapeRegExp(replacement), "gm"), replacements[replacement]);
                            }
                        }

                        if ((<any>destinationPath).endsWith(".ts") || (<any>destinationPath).endsWith(".tsx")) {
                            let startIndex : number = data.indexOf(os.EOL + "namespace");
                            if (startIndex !== -1) {
                                startIndex = startIndex + os.EOL.length;
                            }
                            if (startIndex > data.indexOf("{")) {
                                startIndex = data.indexOf("{");
                            }
                            if (startIndex > data.indexOf(";")) {
                                startIndex = data.indexOf(";");
                            }
                            const header : string = data.substring(0, startIndex);
                            if (header !== "") {
                                let cleanHeader : string = header.replace(/^[\r\n]+/gm, "");
                                if (header.indexOf("\r\n") !== -1) {
                                    cleanHeader = cleanHeader.replace(/\r/gm, "\r\n");
                                }
                                if (header !== cleanHeader) {
                                    this.logIt(">> removing header blank lines");
                                    data = data.replace(header, cleanHeader);
                                }
                            }

                            const imports : RegExpMatchArray = data.match(/import (.*?) =/gm);
                            let importIndex : number;
                            if (imports !== null) {
                                for (importIndex = 0; importIndex < imports.length; importIndex++) {
                                    const importName : string =
                                        imports[importIndex].replace("import ", "").replace(" =", "");
                                    if (importName !== "Echo" &&
                                        importName !== "LogIt" &&
                                        importName.indexOf(" ") === -1 &&
                                        data.indexOf(importName + ".") === -1 &&
                                        data.indexOf(" " + importName + ";") === -1 &&
                                        data.indexOf("new " + importName) === -1 &&
                                        data.indexOf(" = " + importName) === -1 &&
                                        data.indexOf(" : " + importName) === -1 &&
                                        data.indexOf(" ? " + importName) === -1 &&
                                        data.indexOf("|" + importName) === -1 &&
                                        data.indexOf(importName + "|") === -1 &&
                                        data.indexOf(" | " + importName) === -1 &&
                                        data.indexOf(importName + " | ") === -1 &&
                                        data.indexOf("(" + importName) === -1 &&
                                        data.indexOf(importName + ")") === -1 &&
                                        data.indexOf("extends " + importName) === -1 &&
                                        data.indexOf("implements " + importName) === -1 &&
                                        data.indexOf(", " + importName) === -1 &&
                                        data.indexOf(importName + ", ") === -1 &&
                                        data.indexOf("<" + importName + ">") === -1 &&
                                        data.indexOf(importName + "[") === -1 &&
                                        data.indexOf("@" + importName) === -1) {
                                        this.logIt(">> removing unused import \"" + importName + "\"");
                                        data = data.replace(new RegExp("[\n\r]*.*import " + importName + " =[\n\r]*.*;", "gm"), "");
                                    }
                                }
                            }
                        }
                        if (origData !== data) {
                            cleanedFiles.push(destinationPath);
                            this.fileSystem.Write(destinationPath, data);
                        }
                    }
                }
            }

            let destinationFolder : string = "";
            if (this.config.hasOwnProperty("targetFolder") && this.config.targetFolder !== "") {
                destinationFolder = this.config.targetFolder;
            }

            files = this.fileSystem.Expand(this.properties.projectBase + "/" + target + "/**/*");
            if (cleanedFiles.length > 0) {
                LogIt.Info(">>"[ColorType.RED] + " " + cleanedFiles.length + " files cleaned at:");
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " 0 files cleaned. All " + files.length + " monitored files are clean.");
            }
            for (index = 0; index < files.length; index++) {
                filePath = files[index];
                if (this.isIgnored(filePath)) {
                    continue;
                }
                destinationPath = this.properties.projectBase + "/" + destinationFolder +
                    filePath.replace(this.properties.projectBase + "/" + target + "/", "");
                if (this.fileSystem.IsFile(filePath)) {
                    if (cleanedFiles.indexOf(filePath) !== -1) {
                        LogIt.Info("   > " + destinationPath);
                        this.fileSystem.Write(destinationPath, this.fileSystem.Read(filePath));
                    }
                } else if (destinationFolder !== "" && this.fileSystem.IsDirectory(filePath)) {
                    this.fileSystem.CreateDirectory(destinationPath);
                }
            }
            this.fileSystem.Delete(this.properties.projectBase + "/" + target);

            if (destinationFolder === "") {
                files = this.fileSystem.Expand(this.getExpandPattern());
                for (index = 0; index < files.length; index++) {
                    filePath = files[index];
                    if (this.isIgnored(filePath)) {
                        continue;
                    }
                    if (this.fileSystem.Exists(filePath) && this.fileSystem.IsDirectory(filePath)) {
                        const dirs : string[] = [];
                        for (origPath in filePaths) {
                            if (filePaths.hasOwnProperty(origPath)) {
                                if (filePath.indexOf(origPath) !== -1) {
                                    dirs.push(filePath);

                                }
                            }
                        }
                        for await (const dir of dirs) {
                            this.logIt("delete " + dir);
                            await this.fileSystem.DeleteAsync(dir);
                        }
                    }
                }
            }

            if (this.config.cleanuplog) {
                this.fileSystem.Write(this.properties.projectBase + "/log/cleanup.log", this.cleanUpLog);
            }
        } catch (ex) {
            LogIt.Error(ex);
        }
    }

    private escapeRegExp($input : string) : string {
        const specials : string[] = ["-", "[", "]", "/", "{", "}", "(", ")", "*", "+", "?", ".", "\\", "^", "$", "|"];
        return $input.replace(new RegExp("[" + specials.join("\\") + "]", "g"), "\\$&");
    }

    private getExpandPattern() : string[] {
        let pattern : string[] = [this.properties.projectBase + "/*"];
        if (this.config.hasOwnProperty("ignore")) {
            pattern = [];
            this.fileSystem.Expand(this.properties.projectBase + "/*").forEach(($file : string) : void => {
                let contains : boolean = false;
                this.config.ignore.forEach(($pattern : string) : void => {
                    if (StringUtils.PatternMatched(this.properties.projectBase + "/" + $pattern, $file)) {
                        contains = true;
                    }
                });
                if (!contains && this.fileSystem.IsDirectory($file)) {
                    pattern.push($file + "/**/*");
                }
            });
            this.config.ignore.forEach(($pattern : string) : void => {
                if (StringUtils.EndsWith($pattern, "/*") && !StringUtils.Contains($pattern, "/**")) {
                    $pattern += "*/*";
                }
                pattern.push("!" + $pattern);
            });
        }
        return pattern;
    }

    private isIgnored($filePath : string) : boolean {
        let status : boolean = false;
        for (const ignore of this.config.ignore) {
            if (StringUtils.PatternMatched(this.properties.projectBase + "/" + ignore, $filePath)) {
                status = true;
                break;
            }
        }
        return status;
    }

    private logIt($message : string, $force : boolean = false) : void {
        if (this.config.cleanuplog) {
            this.cleanUpLog += $message + require("os").EOL;
        }
        if (this.config.verbose || $force === true) {
            LogIt.Debug($message);
        }
    }
}

export interface IProjectCleanupConfig {
    cleanuplog : boolean;
    verbose : boolean;
    targetFolder : string;
    ignore : string[];
    readExtensions : string[];
    filePaths : any;
    contentReplacements : any;
}

// generated-code-start
/* eslint-disable */
export const IProjectCleanupConfig = globalThis.RegisterInterface(["cleanuplog", "verbose", "targetFolder", "ignore", "readExtensions", "filePaths", "contentReplacements"]);
/* eslint-enable */
// generated-code-end
