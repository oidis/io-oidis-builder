/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { BaseTask } from "../../Primitives/BaseTask.js";

export class SassCleanup extends BaseTask {

    protected getName() : string {
        return "sass-cleanup";
    }

    protected async processAsync($option : string) : Promise<void> {
        return this.runTaskAsync("sass-lint:autocleanup");
    }
}
