/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class SCRCleanup extends BaseTask {
    private allDependencies : string[] = [];
    private missingDependencies : string[] = [];
    private missingRecords : IScrTextRecord[] = [];
    private releaseNameTag : string = "Release Name:";
    private scrData : string;

    protected getName() : string {
        return "scr-cleanup";
    }

    protected async processAsync($option : string) : Promise<void> {
        const os : any = require("os");

        const scrFile : string = this.properties.projectBase + "/SW-Content-Register.txt";
        const dependenciesFiles : string[] =
            this.fileSystem.Expand(this.properties.projectBase + "/dependencies/*/SW-Content-Register.txt");

        if (this.fileSystem.Exists(scrFile)) {
            this.scrData = this.fileSystem.Read(scrFile).toString();
            const scrRecords : string[] = this.scrData.split(os.EOL + os.EOL);
            let newScrData : string = "";

            dependenciesFiles.forEach(($file : string) : void => {
                this.parseSCR(this.fileSystem.Read($file).toString());
            });

            scrRecords.forEach(($record : string) : void => {
                if (StringUtils.Remove($record, os.EOL).trim() !== "") {
                    if ($record.indexOf(this.releaseNameTag) === -1) {
                        const record : IScrTextRecord = this.parseRecord($record);
                        this.missingRecords.forEach(($dependency : IScrTextRecord) : void => {
                            if (record.name === $dependency.name && !$dependency.isPrinted) {
                                record.optional = $dependency.optional;
                                record.version = $dependency.version;
                                record.description = $dependency.description;
                                record.author = $dependency.author;
                                record.license = $dependency.license;
                                record.format = $dependency.format;
                                record.location = $dependency.location;
                                $dependency.isPrinted = true;
                            }
                        });
                        if (!record.isDependency ||
                            record.isDependency && this.allDependencies.indexOf(record.getHash()) !== -1) {
                            newScrData += record.ToString() + os.EOL;
                        }
                    } else {
                        newScrData += $record + os.EOL + os.EOL;
                    }
                }
            });
            this.missingRecords.forEach(($dependency : IScrTextRecord) : void => {
                if (!$dependency.isPrinted) {
                    $dependency.isPrinted = true;
                    newScrData += $dependency.ToString() + os.EOL;
                }
            });
            if ((<any>newScrData).endsWith(os.EOL + os.EOL)) {
                newScrData = newScrData.substring(0, newScrData.lastIndexOf(os.EOL));
            }

            if (this.scrData !== newScrData) {
                this.fileSystem.Write(scrFile, newScrData);
                LogIt.Info(">>"[ColorType.RED] + " SW-Content-Register.txt has been cleaned.");
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " SW-Content-Register.txt is up to date.");
            }
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " Clean up of SW-Content-Register.txt skipped: file does not exist");
        }
    }

    private parseSCR($data : string) : void {
        const os : any = require("os");
        const release : string =
            $data.substring($data.indexOf(this.releaseNameTag) + this.releaseNameTag.length, $data.indexOf(" v")).trim();
        const versionTag : string = this.releaseNameTag + " " + release + " v";
        const version : string =
            $data.substring($data.indexOf(versionTag) + versionTag.length, $data.indexOf(os.EOL)).trim();

        const records : string[] = $data.split(os.EOL + os.EOL);
        records.forEach(($record : string) : void => {
            if (StringUtils.Remove($record, os.EOL).trim() !== "") {
                if ($record.indexOf(this.releaseNameTag) === -1 && $record.indexOf(this.project.name) === -1) {
                    const record : IScrTextRecord = this.parseRecord($record);
                    record.isDependency = true;
                    if (record.name === release) {
                        record.version = version;
                    }
                    if (this.allDependencies.indexOf(record.getHash()) === -1) {
                        this.allDependencies.push(record.getHash());
                    }
                    if (this.scrData.indexOf(record.ToString()) === -1 &&
                        this.missingDependencies.indexOf(record.getHash()) === -1) {
                        this.missingDependencies.push(record.getHash());
                        this.missingRecords.push(record);
                    }
                }
            }
        });
    }

    private parseRecord($data : string) : IScrTextRecord {
        const os : any = require("os");
        const record : IScrTextRecord = <IScrTextRecord>{
            ToString() : string {
                return (this.optional ? "[OPTIONAL]" + os.EOL : "") +
                    this.name + (this.version !== "" ? " v" + this.version : "") + os.EOL +
                    "Description: " + this.description + os.EOL +
                    "Author: " + this.author + os.EOL +
                    "License: " + this.license + os.EOL +
                    "Format: " + this.format + os.EOL +
                    "Location: " + this.location + os.EOL;
            },
            author     : "",
            description: "",
            format     : "",
            getHash() : string {
                return this.name + this.version;
            },
            isDependency: false,
            isPrinted   : false,
            license     : "",
            location    : "",
            name        : "",
            optional    : false,
            version     : ""
        };
        if ($data.indexOf("[OPTIONAL]") !== -1) {
            $data = $data.replace("[OPTIONAL]" + os.EOL, "");
            record.optional = true;
        }
        const lines : string[] = $data.split(os.EOL);
        if (lines[0].indexOf(" v") !== -1) {
            const firstLine : string[] = lines[0].split(" v");
            record.name = firstLine[0];
            record.version = firstLine[1];
        } else {
            record.name = lines[0];
        }
        let index : number;
        let isLocation : boolean = false;
        for (index = 1; index < lines.length; index++) {
            const line : string[] = lines[index].split(": ");
            switch (line[0]) {
            case "Description":
                record.description = line[1];
                break;
            case "Author":
                record.author = line[1];
                break;
            case "License":
                record.license = line[1];
                break;
            case "Format":
                record.format = line[1];
                break;
            case "Location":
                record.location = line[1];
                isLocation = true;
                break;
            default:
                if (isLocation && !StringUtils.StartsWith(lines[index], os.EOL)) {
                    if (StringUtils.Remove(lines[index], os.EOL).trim() !== "") {
                        record.location += os.EOL + lines[index];
                    }
                } else {
                    isLocation = false;
                }
                break;
            }
        }
        return record;
    }
}

export interface IScrTextRecord {
    optional : boolean;
    name : string;
    version : string;
    description : string;
    author : string;
    license : string;
    format : string;
    location : string;
    isPrinted : boolean;
    isDependency : boolean;
    getHash : () => string;
    "ToString" : () => string; // eslint-disable-line @stylistic/quote-props
}

// generated-code-start
/* eslint-disable */
export const IScrTextRecord = globalThis.RegisterInterface(["optional", "name", "version", "description", "author", "license", "format", "location", "isPrinted", "isDependency", "getHash", "\"ToString\""]);
/* eslint-enable */
// generated-code-end
