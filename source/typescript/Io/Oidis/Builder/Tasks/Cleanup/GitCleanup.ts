/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { GitManager } from "../Utils/GitManager.js";
import { StringReplace } from "../Utils/StringReplace.js";

export class GitCleanup extends BaseTask {

    protected getName() : string {
        return "git-cleanup";
    }

    protected async processAsync($option : string) : Promise<void> {
        let defaultIgnoreFile : string = this.properties.binBase + "/resource/configs/gitignore.conf";
        const targetFile : string = this.properties.projectBase + "/.gitignore";

        if (this.properties.projectHas.Cpp.Source()) {
            defaultIgnoreFile = this.properties.binBase + "/resource/configs/gitignore-cpp.conf";
        }

        const generateDefault : any = async () : Promise<void> => {
            await this.fileSystem.CopyAsync(defaultIgnoreFile, targetFile);
            StringReplace.File(targetFile, StringReplaceType.VARIABLES);
            if (!this.programArgs.IsAgentTask()) {
                await GitManager.UpdateTracked();
            }
        };
        if (!this.fileSystem.Exists(this.properties.projectBase + "/.git") || !GitManager.IsGitRepo()) {
            LogIt.Warning(">>"[ColorType.YELLOW] + " project has not been detect as GIT repository, cleanup skipped.");
        } else if (this.fileSystem.Exists(targetFile)) {
            const splittingPattern = "# Project individual ignore patterns";
            const targetData : string[] = this.fileSystem.Read(targetFile).toString().split(splittingPattern);
            const defaultData : string[] = StringReplace.Content(this.fileSystem.Read(defaultIgnoreFile).toString(),
                StringReplaceType.VARIABLES).split(splittingPattern);

            if (targetData.length !== 2) {
                LogIt.Warning(">>"[ColorType.YELLOW] + " .gitignore file in project root is corrupted. " +
                    "Replacing by default values. Old ones are listed below\n" + targetData[0] + "\n");
                await generateDefault();
            } else if (targetData[0] !== defaultData[0]) {
                const newData = defaultData[0] + splittingPattern + targetData[1];
                if (this.fileSystem.Write(targetFile, newData)) {
                    LogIt.Info(">>"[ColorType.RED] + " .gitignore has been cleaned.");
                    await GitManager.UpdateTracked();
                } else {
                    LogIt.Error("Failed to write data into .gitignore file.");
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " .gitignore is up to date.");
                if (Loader.getInstance().getProgramArgs().IsForce()) {
                    await GitManager.UpdateTracked();
                }
            }
        } else {
            LogIt.Info("No .gitignore file found in project, thus the default one has been generated.");
            await generateDefault();
        }
    }
}
