/*! ******************************************************************************************************** *
 *
 * Copyright 2024-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { Resources } from "../../DAO/Resources.js";
import { IProjectConfigLoadResponse } from "../../EnvironmentArgs.js";
import { IProjectAuthor } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { GitManager, IGitExecuteResult } from "../Utils/GitManager.js";

export class CopyrightsCleanup extends BaseTask {

    protected getName() : string {
        return "copyrights-cleanup";
    }

    protected async processAsync($option : string) : Promise<void> {
        if (!GitManager.IsGitRepo()) {
            LogIt.Info(">>"[ColorType.YELLOW] + " Not a git repository: skipping copyrights check");
        } else if (!this.fileSystem.Exists(this.properties.projectBase + "/.git")) {
            LogIt.Warning(">>"[ColorType.YELLOW] + " project has not been detect as GIT repository, copyrights cleanup skipped.");
        } else {
            const currentYear : number = (new Date()).getFullYear();
            const changes : IGitExecuteResult = await GitManager.ExecuteAsync([
                "log", "--reverse", "--after=" + currentYear + "-01-01", "--format=format:%H"
            ], {repoPath: this.properties.projectBase, verbose: false, throwError: false});
            if (!changes.status) {
                LogIt.Info(">>"[ColorType.YELLOW] +
                    " Failed to get git log required for changes detection: skipping copyrights check");
                return;
            }
            const sha : string = StringUtils.Split(StringUtils.Remove(changes.stdout, "\r"), "\n")[0];
            const files : IGitExecuteResult = await GitManager.ExecuteAsync([
                "diff", "--name-only", "--diff-filter=AM", "--format=format:%H", sha
            ], {repoPath: this.properties.projectBase, verbose: false, throwError: false});
            if (!files.status) {
                LogIt.Info(">>"[ColorType.YELLOW] +
                    " Failed to get git diff required for collection of modified files: skipping copyrights check");
                return;
            }
            const modified : string[] = StringUtils.Split(StringUtils.Remove(files.stdout, "\r"), "\n");

            const options : any = JSON.parse(
                this.fileSystem.Read(this.properties.binBase + "/resource/configs/oidislint.conf.json").toString());
            let overridePath : string = this.properties.projectBase + "/bin/project/configs/oidislint.conf.jsonp";
            if (!this.fileSystem.Exists(overridePath)) {
                overridePath = overridePath.slice(0, -1);
            }
            if (!this.fileSystem.Exists(overridePath)) {
                overridePath = this.properties.projectBase + "/resource/configs/oidislint.conf.json";
            }
            const res : IProjectConfigLoadResponse = await Loader.getInstance().getEnvironmentArgs().LoadPackageConf(overridePath);
            if (res.status) {
                Resources.Extend(options, res.data);
            }
            const contributors : string[] = [];
            contributors.push(this.project.author.name);
            if (!ObjectValidator.IsEmptyOrNull(this.project.author.organization) &&
                !contributors.includes(this.project.author.organization)) {
                contributors.push(this.project.author.organization);
            } else {
                options.copyrights.contributors.forEach(($contributor : string) : void => {
                    if (!contributors.includes($contributor)) {
                        contributors.push($contributor);
                    }
                });
            }
            if (!ObjectValidator.IsEmptyOrNull(this.project.contributors)) {
                this.project.contributors.forEach(($contributor : IProjectAuthor) : void => {
                    if (!contributors.includes($contributor.name)) {
                        contributors.push($contributor.name);
                    }
                    if (!ObjectValidator.IsEmptyOrNull($contributor.organization) &&
                        !contributors.includes($contributor.organization)) {
                        contributors.push($contributor.organization);
                    }
                });
            }
            let passed : boolean = true;
            modified.forEach(($path : string) : void => {
                $path = this.fileSystem.NormalizePath($path);
                if (!ObjectValidator.IsEmptyOrNull($path) && this.fileSystem.IsFile($path)) {
                    let excluded : boolean = false;
                    options.copyrights.exclude.forEach(($pattern : string) : void => {
                        if (!excluded && StringUtils.PatternMatched($pattern, $path)) {
                            excluded = true;
                        }
                    });
                    if (!excluded) {
                        const data : string = this.fileSystem.Read(this.properties.projectBase + "/" + $path).toString("utf8");
                        const lintIgnore : string = "copyrights";
                        let filePassed : boolean = false;
                        if (StringUtils.Contains(data, "// oidislint ignore:" + lintIgnore)) {
                            filePassed = true;
                        } else {
                            const replacement : string = "build.year";
                            contributors.forEach(($contributor : string) => {
                                if (!filePassed && !ObjectValidator.IsEmptyOrNull($contributor) && (
                                    StringUtils.Contains(data,
                                        currentYear + " " + $contributor + " ",
                                        currentYear + " " + $contributor + "<",
                                        currentYear + " " + $contributor + "\r\n",
                                        currentYear + " " + $contributor + "\n",
                                        currentYear + " [" + $contributor + "]",
                                        "<? @var " + replacement + " ?> " + $contributor + " ",
                                        "<? @var " + replacement + " ?> " + $contributor + "<",
                                        "<? @var " + replacement + " ?> " + $contributor + "\r\n",
                                        "<? @var " + replacement + " ?> " + $contributor + "\n"))) {
                                    filePassed = true;
                                }
                            });
                        }
                        if (!filePassed) {
                            LogIt.Warning("Missing year at file: " + $path);
                            passed = false;
                        }
                    }
                }
            });

            if (!passed) {
                const message : string = "Not all of files have up to date copyrights.";
                if (!this.project.lintErrorsAsWarnings && $option !== "init") {
                    LogIt.Error(message);
                } else {
                    LogIt.Warning("Suppressed lint error: " + message);
                    return;
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " All files have up to date copyrights.");
            }
        }
    }
}
