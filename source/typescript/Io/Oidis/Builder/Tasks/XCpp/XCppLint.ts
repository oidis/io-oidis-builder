/*! ******************************************************************************************************** *
 *
 * Copyright 2015-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { Resources } from "../../DAO/Resources.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class XCppLint extends BaseTask {
    private taskOption : string;

    protected getName() : string {
        return "xcpp-lint";
    }

    protected async processAsync($option : string) : Promise<void> {
        this.taskOption = $option;

        const configs : IXCppLintConfigs = <any>{
            options     : {
                root: {
                    source      : "source/cpp",
                    dependencies: "dependencies/*/source/cpp",
                    unit        : "test/unit/cpp"
                }
            },
            source      : [
                this.properties.sources + "/*.cpp",
                this.properties.sources + "/*.h",
                this.properties.sources + "/*.hpp"
            ],
            dependencies: [
                this.properties.dependencies + "/*.cpp",
                this.properties.dependencies + "/*.h",
                this.properties.dependencies + "/*.hpp"
            ],
            unit        : [
                "test/unit/**/*.cpp",
                "test/unit/**/*.h",
                "test/unit/**/*.hpp"
            ]
        };
        const filters : any = JSON.parse(
            this.fileSystem.Read(this.properties.binBase + "/resource/configs/cpplint.conf.json").toString());
        const overridePath : string = this.properties.projectBase + "/resource/configs/cpplint.conf.json";
        if (this.fileSystem.Exists(overridePath)) {
            Resources.Extend(filters, JSON.parse(this.fileSystem.Read(overridePath).toString()));
        }
        const options : IXCppLintConfigOptions = {
            counting  : "detailed",
            extensions: ["h", "hpp", "cpp"],
            filters,
            headers   : ["h", "hpp"],
            linelength: 140,
            root      : {
                dependencies: "",
                source      : ""
            },
            verbose   : 0
        };

        if (!ObjectValidator.IsEmptyOrNull(configs)) {
            if (configs.hasOwnProperty($option)) {
                if (configs.hasOwnProperty("options")) {
                    this.mergeOptions(options, configs.options);
                }
                configs.options = options;

                let tmp : string[] = [];
                if (configs.options.root.source !== "") {
                    tmp = this.fileSystem.Expand(configs.options.root.source);
                    if (tmp.length === 1) {
                        configs.options.root.source = tmp[0];
                    }
                }
                if (configs.options.root.dependencies !== "" && this.programArgs.getOptions().withDependencies) {
                    tmp = this.fileSystem.Expand(configs.options.root.dependencies);
                    if (tmp.length === 1) {
                        configs.options.root.dependencies = tmp[0];
                    }
                }

                // todo xcpplint will not work properly for more than one wui-dependencies

                if (this.fileSystem.Expand(configs[$option]).length > 0) {
                    await this.compiler(configs.options, configs[$option]);
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " XCpp lint task \"" + $option + "\" skipped: 0 source files have been found");
                }
            } else {
                LogIt.Error("Configuration for XCpp lint task \"" + $option + "\" does not exist.");
            }
        } else {
            LogIt.Error("Configuration for XCpp lint task does not exist.");
        }
    }

    private async cppcheck($options : IXCppLintConfigOptions, $files : string[]) : Promise<void> {
        const os : any = require("os");
        const path : any = require("path");

        let filesToCheck : number = $files.length;
        $files.forEach(($value : string) => {
            if (StringUtils.EndsWith($value, "reference.hpp") ||
                StringUtils.EndsWith($value, "interfacesMap.hpp") ||
                StringUtils.EndsWith($value, "sourceFilesMap.hpp")) {
                filesToCheck--;
            }
        });
        if (filesToCheck > 0) {
            if (this.properties.projectHas.ContentFor($options.root[this.taskOption].replace(/\//gi, path.sep) + "/**/*.cpp")) {
                const res : IExecuteResult = await this.terminal.SpawnAsync("cppcheck", [
                    "-j", os.cpus().length + "",
                    "--enable=warning,style,performance,portability",
                    "--inconclusive",
                    "-I", "\"" + (this.properties.projectBase + "/" + this.properties.compiled)
                        .replace(/\//gi, path.sep) + "\"",
                    "--inline-suppr",
                    "--suppress=syntaxError",
                    "--language=c++",
                    "--std=c++11",
                    "--platform=native",
                    "--template", "\"[{file}:{line}]: ({severity}:{id}) {message}\"",
                    "\"" + $options.root[this.taskOption].replace(/\//gi, path.sep) + "\""
                ], this.properties.projectBase);
                if (res.exitCode === 0) {
                    LogIt.Info(">>"[ColorType.YELLOW] + " " + $files.length + " files lint free.");
                } else {
                    if (res.std[1].indexOf(":0]: (error:syntaxError) syntax error") > -1) {
                        LogIt.Warning("XCpp lint (cppcheck task) warns about syntax error " +
                            "(cppcheck do not understand file syntax which could be valid).");
                    } else {
                        LogIt.Error("XCpp lint[static check] task \"" + this.taskOption + "\" has failed");
                    }
                }
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " " + $files.length + " files lint free. " +
                    "XCpp lint[static check] skipped 0 source files.");
            }
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " CppCheck skipped: test root contains only project reference files.");
        }
    }

    private async compiler($options : IXCppLintConfigOptions, source : string[]) : Promise<void> {
        const path : any = require("path");
        const fileBlockSize : number = 20;
        const cpplintPath : string = (Loader.getInstance().getProgramArgs().ProjectBase() +
            "/resource/libs/cpplint").replace(/\//gi, path.sep);
        const files : string[] = this.fileSystem.Expand(source);

        if ($options.verbose < 0) {
            $options.verbose = 0;
        }
        if ($options.verbose > 5) {
            $options.verbose = 5;
        }

        let index : number;
        for (index = 0; index < files.length; index++) {
            files[index] = (this.properties.projectBase + "/" + files[index].replace("./", "")).replace(/\//gi, path.sep);
        }

        let filesChunk : string[] = [];
        let chunkIndex : number = 0;
        do {
            filesChunk = files.slice(chunkIndex * fileBlockSize, fileBlockSize * (chunkIndex + 1));
            if (filesChunk.length > 0) {
                if ((await this.terminal.SpawnAsync("python3",
                    ["cpplint.py"].concat(this.generateArgs($options)).concat(filesChunk), cpplintPath)).exitCode === 0) {
                    chunkIndex++;
                } else {
                    LogIt.Error("XCpp lint task \"" + this.taskOption + "\" has failed");
                }
            }
        } while (filesChunk.length > 0);
        LogIt.Info(">>"[ColorType.YELLOW] + " XCpp lint task succeed. Starting cppcheck task...");
        await this.cppcheck($options, files);
    }

    private mergeOptions($parent : IXCppLintConfigOptions, $child : IXCppLintConfigOptions) : void {
        let property : string;
        for (property in $child) {
            if ($child.hasOwnProperty(property)) {
                if ($parent.hasOwnProperty(property) && typeof $child[property] === "object" && $child[property] !== null) {
                    this.mergeOptions($parent[property], $child[property]);
                } else {
                    $parent[property] = $child[property];
                }
            }
        }
    }

    private generateArgs($options : IXCppLintConfigOptions) : string[] {
        const output : string[] = [];
        let property : string;
        let value : string;
        let filterSettings : string[];
        const resolveCategories : any = ($categoryName : string) : void => {
            if (!ObjectValidator.IsEmptyOrNull(value)) {
                const category : any = value[$categoryName];
                const subcategories : string[] = Object.keys(category);
                for (const subcategory of subcategories) {
                    let filterSetting : string = "";
                    if (category[subcategory]) {
                        filterSetting += "+";
                    } else {
                        filterSetting += "-";
                    }
                    filterSetting += $categoryName + "/" + subcategory;
                    filterSettings.push(filterSetting);
                }
            }
        };

        for (property in $options) {
            if ($options.hasOwnProperty(property)) {
                let key : string = property;

                if (key === "root") {
                    value = $options[key][this.taskOption].replace("./", "").replace(/\\/gi, "/");
                    if ((<any>value).endsWith("/")) {
                        value = value.substr(0, value.length - 1);
                    }
                } else {
                    value = $options[property];
                    if (!ObjectValidator.IsEmptyOrNull(value)) {
                        switch (key) {
                        case "filters":
                            const categories : string[] = Object.keys(value); // eslint-disable-line no-case-declarations
                            filterSettings = [];
                            categories.forEach(resolveCategories);
                            if (filterSettings.length > 0) {
                                key = "filter";
                                value = filterSettings.join(",");
                            } else {
                                value = "";
                            }
                            break;
                        case "headers":
                        case "extensions":
                            value = value.length > 0 ? (<any>value).join(",") : "";
                            break;
                        default:
                            break;
                        }
                    }
                }

                if (value !== "") {
                    output.push("--" + key + "=" + value);
                }
            }
        }
        return output;
    }
}

export interface IXCppLintRootOptions {
    source : string;
    dependencies : string;
}

export interface IXCppLintConfigOptions {
    verbose : number;
    filters : any;
    counting : string;
    root : IXCppLintRootOptions;
    linelength : number;
    extensions : string[];
    headers : string[];
}

export interface IXCppLintConfigs {
    options : IXCppLintConfigOptions;
    source : string[];
    dependencies : string[];
    unit : string[];
}

// generated-code-start
/* eslint-disable */
export const IXCppLintRootOptions = globalThis.RegisterInterface(["source", "dependencies"]);
export const IXCppLintConfigOptions = globalThis.RegisterInterface(["verbose", "filters", "counting", "root", "linelength", "extensions", "headers"]);
export const IXCppLintConfigs = globalThis.RegisterInterface(["options", "source", "dependencies", "unit"]);
/* eslint-enable */
// generated-code-end
