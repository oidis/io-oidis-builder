/*! ******************************************************************************************************** *
 *
 * Copyright 2015-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IXCppConfig, IXCppConfigs } from "../../Interfaces/IXCppConfig.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { XCppCompile } from "./XCppCompile.js";

export class XCppIntefaces extends BaseTask {
    private classInfoList : ICppClassDefinition;
    private classInfoListName : string = "__$classInfoList";
    private interfaceBaseName : string = "__$interfaceBaseName";
    private interfacesBody : string;
    private ifDefine : string;
    private addIfDefine : boolean;
    private excludeProperty : any;
    private reflectionBody : string;

    protected getName() : string {
        return "xcpp-interfaces";
    }

    protected async processAsync($option : string) : Promise<void> {
        const os : any = require("os");

        const confs : IXCppConfigs = XCppCompile.getConfig();
        const generated : string[] = [];

        this.classInfoList = <any>{};
        this.interfacesBody = "";
        this.ifDefine = "";
        this.addIfDefine = true;
        this.excludeProperty = {};
        this.reflectionBody = "";

        let confBlock : string;
        for (confBlock in confs) {
            if (confs.hasOwnProperty(confBlock) && confBlock === $option) {
                if (confs[confBlock].hasOwnProperty("interface")) {
                    const conf : IXCppConfig = confs[confBlock];
                    const interfaceFile : string = this.properties.projectBase + "/" + conf.interface;
                    const reflectionFile : string = this.properties.projectBase + "/" + conf.reflection;
                    const src : string[] = conf.src;
                    src.push("!" + this.properties.dependencies);
                    const hash : string = (interfaceFile + src.join(""))
                        .replace(/\*/g, "")
                        .replace(/\./g, "")
                        .replace(/\//g, "")
                        .replace(/<%= /g, "")
                        .replace(/ %>/g, "")
                        .replace(/,/g, "");
                    if (generated.indexOf(hash) === -1) {
                        this.classInfoList = <any>{};
                        let isRootGenerated : boolean = false;
                        let subSrc : string;
                        const writeOnlyChanged : any = ($path : string, $data : string) => {
                            let oldFileContent : string = "";
                            if (this.fileSystem.Exists($path)) {
                                oldFileContent = this.fileSystem.Read($path).toString();
                            }
                            if (oldFileContent !== $data) {
                                this.fileSystem.Write($path, $data);
                            } else {
                                LogIt.Info("File \"" + $path + "\" regeneration skipped: no change were found.");
                            }
                        };

                        for (subSrc in conf.srcBase) {
                            if (conf.srcBase.hasOwnProperty(subSrc)) {
                                const foundRoot : string[] = this.fileSystem.Expand(conf.srcBase[subSrc]);
                                if (foundRoot.length === 1) {
                                    if (isRootGenerated === false) {
                                        this.fileSystem.Expand(foundRoot[0] + "/**/*.*").forEach(($file : string) : void => {
                                            this.generateClassInfoMap($file, foundRoot[0]);
                                        });

                                        this.interfacesBody = "";
                                        this.ifDefine = "";
                                        this.addIfDefine = true;

                                        this.generateInterface(this.classInfoList, "");

                                        if (this.interfacesBody !== "") {
                                            LogIt.Info("Automatically added interfacesMap file to: \"" + interfaceFile + "\"");
                                            this.ifDefine += "INTERFACESMAP_HPP_";
                                            const interfacesMap : string =
                                                "/** WARNING: this file has been automatically generated from C++ interfaces " +
                                                "and classes, which exist in this package. */" + os.EOL +
                                                "// NOLINT (legal/copyright)" + os.EOL + os.EOL +
                                                "#ifndef " + this.ifDefine + "  // NOLINT" + os.EOL +
                                                "#define " + this.ifDefine + os.EOL + os.EOL +
                                                this.interfacesBody + os.EOL +
                                                "#endif  // " + this.ifDefine + "  // NOLINT" + os.EOL;
                                            writeOnlyChanged(interfaceFile, interfacesMap);
                                            generated.push(hash);
                                        }
                                        isRootGenerated = true;
                                    } else {
                                        this.fileSystem.Expand(foundRoot[0] + "/**/*.*").forEach(($file : string) : void => {
                                            this.generateClassInfoMap($file, foundRoot[0]);
                                        });
                                    }
                                }
                            }
                        }
                        this.reflectionBody = "";
                        this.generateReflectionData(this.classInfoList, "");

                        if (this.reflectionBody !== "") {
                            LogIt.Info("Automatically generated reflection data to: \"" + reflectionFile + "\"");

                            const reflectionMap : string = "" +
                                "/** WARNING: This file contains data generated from all sources in project and dependencies. */" +
                                os.EOL + os.EOL +
                                "#ifndef REFLECTIONDATA_HPP_" + os.EOL +
                                "#define REFLECTIONDATA_HPP_" + os.EOL + os.EOL +
                                "#include <boost/bind.hpp>" + os.EOL +
                                "#include \"../../source/cpp/reference.hpp\"" + os.EOL +
                                os.EOL +
                                "using std::string;" + os.EOL +
                                os.EOL +
                                "static std::map<string, std::function<int(const int, const char **)>> map = {" + os.EOL +
                                this.reflectionBody +
                                "};" + os.EOL +
                                os.EOL +
                                "class ReflectionData {" + os.EOL +
                                " public:" + os.EOL +
                                "    static std::map<string, std::function<int(const int, const char **)>> getReflectionData() {" +
                                os.EOL +
                                "        return map;" + os.EOL +
                                "    }" + os.EOL +
                                "};" + os.EOL +
                                os.EOL +
                                "#endif  // REFLECTION_DATA_HPP_" + os.EOL;
                            writeOnlyChanged(reflectionFile, reflectionMap);
                        }
                    }
                }
            }
        }
    }

    private getClassInfo($classInfoName : string) : any {
        if (!ObjectValidator.IsEmptyOrNull($classInfoName)) {
            const $parts : string[] = $classInfoName.split("/");
            let classInfo : ICppClassDefinition = this.classInfoList;
            for (const part of $parts) {
                if (!classInfo.hasOwnProperty(part)) {
                    classInfo[part] = {};
                }
                classInfo = classInfo[part];
            }
            if (!classInfo.hasOwnProperty(this.classInfoListName)) {
                classInfo.__$classInfoList = [];
            }
            return classInfo;
        }
        return null;
    }

    private generateClassInfoMap($filename : string, $rootdir : string) : void {
        const path : any = require("path");

        if (StringUtils.EndsWith($filename, ".hpp") &&
            !StringUtils.EndsWith($filename, "interfacesMap.hpp") &&
            !StringUtils.EndsWith($filename, "reference.hpp")) {
            const subdir : string = path.dirname(StringUtils.Remove($filename, $rootdir + "/"));
            if (subdir !== ".") {
                const namespaceInfo : ICppClassDefinition = this.getClassInfo(subdir);
                if (StringUtils.EndsWith($filename, "sourceFilesMap.hpp")) {
                    namespaceInfo.__$interfaceBaseName = subdir;
                }
                if (namespaceInfo !== null) {
                    const data : string = this.fileSystem.Read($filename).toString();
                    let regex : RegExp = new RegExp(
                        "^(?:\\s*(template<.*?>)\\s*)?\\s*(class|enum|struct)\\s(([A-Za-z0-9]+::)*([A-Za-z]+[A-Za-z0-9]*))" +
                        "(?=\\s*(?:$)?(?:|(?:final)|:\\s*(?:$)?(?:(?:public|private|protected|public\\s+virtual|public\\s+final)" +
                        "\\s*)?(?:(?:$)?(?:(?:[A-Za-z0-9_]+::)*([a-zA-Z_]+[a-zA-Z0-9_]*)))\\s*)\\s*(?:$)?[,<{])", "im");

                    const def : RegExpMatchArray = data.match(regex);
                    if (def !== null && def.length >= 5) { // matched result array length corresponds to regex structure
                        const classNameDef : string = def[5];
                        const classAttribute : string = def[1];
                        const classEntityType : string = def[2];
                        const classBaseNameDef : string = def[6];
                        const classFullName : string = def[3];
                        let namespace : string = "";
                        regex = new RegExp("namespace\\s(\\w(?:\\w|::)+)\\s*{");
                        const ns : RegExpMatchArray = data.match(regex);
                        if (ns !== null && ns.length === 2) {
                            namespace = ns[1];
                        }

                        const classInfo : ICppClassDefinition = <any>{};
                        if (!ObjectValidator.IsEmptyOrNull(classNameDef)) {
                            classInfo.__$namespace = namespace;
                            classInfo.__$classEntityType = classEntityType;
                            classInfo.__$classType = classNameDef;
                            classInfo.__$classFullTypename = classFullName;
                            if (!ObjectValidator.IsEmptyOrNull(classAttribute)) {
                                classInfo.__$classAttribute = classAttribute
                                    .replace("/*", "")
                                    .replace("*/", "");
                            }
                            if (!ObjectValidator.IsEmptyOrNull(classBaseNameDef)) {
                                classInfo.__$classBaseTypeDefName = classBaseNameDef;
                            }
                        }
                        namespaceInfo.__$classInfoList.push(classInfo);
                    }
                }
            }
        }
    }

    private generateInterface($infoList : any, $padding : string) : void {
        const os : any = require("os");
        let item : string;
        for (item in $infoList) {
            if (!this.excludeProperty.hasOwnProperty(item) && $infoList.hasOwnProperty(item)) {
                if (item !== this.classInfoListName && item !== this.interfaceBaseName) {
                    this.interfacesBody += $padding + "namespace " + item + " {" + os.EOL;

                    if (this.addIfDefine) {
                        this.ifDefine += item.toUpperCase() + "_";
                    }
                }
                if ($infoList[item].hasOwnProperty(this.interfaceBaseName)) {
                    this.addIfDefine = false;
                }
                if ($infoList[item].hasOwnProperty(this.classInfoListName)) {
                    const classInfos : ICppClassDefinition[] = $infoList[item].__$classInfoList;
                    for (const classInfo of classInfos) {
                        let typedef : string = classInfo.__$classEntityType;
                        let baseTypeDef : string = "";
                        if (classInfo.hasOwnProperty("__$classBaseTypeDefName") && typedef === "enum") {
                            baseTypeDef = " : " + classInfo.__$classBaseTypeDefName;
                        }
                        if (classInfo.hasOwnProperty("__$classAttribute")) {
                            typedef = classInfo.__$classAttribute + " " + typedef;
                        }
                        this.interfacesBody += $padding + "    " + typedef + " " + classInfo.__$classType + baseTypeDef + ";" +
                            os.EOL;
                        this.addIfDefine = false;
                    }
                }
                if (item !== this.classInfoListName && typeof $infoList[item] === "object") {
                    this.generateInterface($infoList[item], $padding + "    ");
                }
                if (item !== this.classInfoListName && item !== this.interfaceBaseName) {
                    this.interfacesBody += $padding + "}" + os.EOL;
                }
            }
        }
    }

    private generateReflectionData($infoList : any, $padding : string) : void {
        const os : any = require("os");
        let item : string;
        for (item in $infoList) {
            if (!this.excludeProperty.hasOwnProperty(item) && $infoList.hasOwnProperty(item)) {
                if ($infoList[item].hasOwnProperty(this.classInfoListName)) {
                    const classInfos : ICppClassDefinition[] = $infoList[item].__$classInfoList;

                    for (const classInfo of classInfos) {
                        const methodName : string = classInfo.__$namespace + "::" + classInfo.__$classFullTypename +
                            "::Load";
                        if (methodName.match(/(?:::)?Loader::/)) {
                            this.reflectionBody += "    {\"" + methodName.replace(/::/ig, ".") + "\"," +
                                " boost::bind(&" + methodName + ", boost::placeholders::_1, boost::placeholders::_2)}," + os.EOL;
                        }
                    }
                }
                if (item !== this.classInfoListName && typeof $infoList[item] === "object") {
                    this.generateReflectionData($infoList[item], $padding);
                }
            }
        }
    }
}

export interface ICppClassDefinition {
    __$classInfoList : ICppClassDefinition[];
    __$classType : string;
    __$classAttribute : string;
    __$classEntityType : string;
    __$interfaceBaseName : string;
    __$classBaseTypeDefName : string;
    __$classFullTypename : string;
    __$namespace : string;
}

// generated-code-start
/* eslint-disable */
export const ICppClassDefinition = globalThis.RegisterInterface(["__$classInfoList", "__$classType", "__$classAttribute", "__$classEntityType", "__$interfaceBaseName", "__$classBaseTypeDefName", "__$classFullTypename", "__$namespace"]);
/* eslint-enable */
// generated-code-end
