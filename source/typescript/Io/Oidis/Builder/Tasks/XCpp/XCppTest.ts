/*! ******************************************************************************************************** *
 *
 * Copyright 2015-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { IXCppConfigs } from "../../Interfaces/IXCppConfig.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { XCppCompile } from "./XCppCompile.js";

export class XCppTest extends BaseTask {
    private static config : any;

    public static getConfig() : IXCppUnitConfig {
        if (ObjectValidator.IsEmptyOrNull(XCppTest.config)) {
            XCppTest.config = {
                filter: "*",
                target: "./build/target/UnitTestRunner"
            };
        }
        return XCppTest.config;
    }

    protected getName() : string {
        return "xcpp-test";
    }

    protected async processAsync($option : string) : Promise<void> {
        const unitConfig : IXCppUnitConfig = XCppTest.getConfig();

        if (EnvironmentHelper.IsWindows()) {
            let cmd = unitConfig.target;
            if (!StringUtils.EndsWith(cmd, ".exe")) {
                cmd += ".exe";
            }
            unitConfig.target = cmd;
        }

        switch ($option) {
        case "unit":
            if (!ObjectValidator.IsEmptyOrNull(unitConfig)) {
                if (this.fileSystem.Exists(unitConfig.target)) {
                    if (!await this.runTest(
                        unitConfig.target,
                        unitConfig.filter,
                        false)) {
                        LogIt.Error("XCpp unit task has failed");
                    }
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " XCpp unit task skipped: " +
                        "target \"" + unitConfig.target + "\" has not been found");
                }
            } else {
                LogIt.Error("Configuration for XCpp unit task does not exist.");
            }
            break;
        case "coverage":
            if (!ObjectValidator.IsEmptyOrNull(unitConfig)) {
                if (this.fileSystem.Exists(unitConfig.target)) {
                    if (!await this.runTest(
                        unitConfig.target,
                        unitConfig.filter,
                        true)) {
                        LogIt.Error("XCpp unit task has failed");
                    }
                    await this.runCoverage();
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " XCpp unit task skipped: " +
                        "target \"" + unitConfig.target + "\" has not been found");
                }
            } else {
                LogIt.Error("Configuration for XCpp unit task does not exist.");
            }
            break;
        default:
            LogIt.Error("Skipping task xcpp-test: task \"" + $option + "\" not found.");
            break;
        }
    }

    protected async runTest($target : string, $filter : string, $quiet : boolean) : Promise<boolean> {
        const path : any = require("path");
        const unitConfig : IXCppUnitConfig = XCppTest.getConfig();
        const buildPath : string =
            (this.properties.projectBase + "/" + $target.substring(0, $target.lastIndexOf("/")).replace("./", ""))
                .replace(/\//gi, path.sep);
        let cmd = unitConfig.target.substring($target.lastIndexOf("/") + 1);
        if (!EnvironmentHelper.IsWindows()) {
            cmd = "./" + cmd;
        }

        const res : IExecuteResult = await this.terminal.SpawnAsync(cmd, this.getTestArgs($filter), buildPath);
        if (ObjectValidator.IsEmptyOrNull($quiet) || $quiet === false) {
            if (res.exitCode === 0) {
                return true;
            }
        }
        return false;
    }

    protected async runCoverage() : Promise<void> {
        const path : any = require("path");
        const xcppConfig : IXCppConfigs = XCppCompile.getConfig();

        const gcovReporterPath : string = (Loader.getInstance().getProgramArgs().ProjectBase() +
            "/resource/libs/gcov_reporter").replace(/\//gi, path.sep);

        LogIt.Info("Run coverage collect.");

        this.fileSystem.Delete(xcppConfig.coverage.dest);
        this.fileSystem.Delete(xcppConfig.coverage.report);
        this.fileSystem.CreateDirectory(xcppConfig.coverage.dest);
        this.fileSystem.CreateDirectory(xcppConfig.coverage.report);

        const dest : string = path.join(this.properties.projectBase, xcppConfig.coverage.dest);
        const report : string = path.join(this.properties.projectBase, xcppConfig.coverage.report);
        let buildPath : string = this.properties.projectBase + "/build_cache";
        if (EnvironmentHelper.IsWindows()) {
            buildPath += "/win";
        } else if (EnvironmentHelper.IsMac()) {
            buildPath += "/mac";
        } else {
            buildPath += "/linux";
        }

        if ((await this.terminal.SpawnAsync("python", [
            "gcov_reporter.py",
            "-s", path.join(this.properties.projectBase, "source/cpp"),
            "-d", path.join(buildPath),
            "-o", dest,
            "-r", report
        ], gcovReporterPath)).exitCode !== 0) {
            LogIt.Error("XCpp coverage task has failed");
        }
    }

    protected getTestArgs($filter : string) : string[] {
        return ["--gtest_filter=" + $filter];
    }
}

export interface IXCppUnitConfig {
    target : string;
    filter : string;
}

// generated-code-start
export const IXCppUnitConfig = globalThis.RegisterInterface(["target", "filter"]);
// generated-code-end
