/*! ******************************************************************************************************** *
 *
 * Copyright 2015-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { CliTaskType } from "../../Enums/CliTaskType.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { IToolchainSettings } from "../../Interfaces/IProject.js";
import { IProperties } from "../../Interfaces/IProperties.js";
import { IXCppConfig, IXCppConfigOptions, IXCppConfigs } from "../../Interfaces/IXCppConfig.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { ResourceHandler } from "../../Utils/ResourceHandler.js";
import { Toolchains } from "../../Utils/Toolchains.js";
import { StringReplace } from "../Utils/StringReplace.js";
import { XCppCacheRegister } from "./XCppCacheRegister.js";

export class XCppCompile extends BaseTask {
    private static config : any;
    private taskOption : string;

    public static getConfig() : IXCppConfigs {
        if (ObjectValidator.IsEmptyOrNull(XCppCompile.config)) {
            const properties : IProperties = Loader.getInstance().getAppProperties();
            XCppCompile.config = {
                source  : {
                    src       : [properties.sources + "/*.cpp", properties.dependencies + "/*.cpp"],
                    srcBase   : [properties.sources + "/cpp", properties.dependencies + "/cpp"],
                    dest      : "build/target",
                    reference : "source/cpp/reference.hpp",
                    interface : "source/cpp/interfacesMap.hpp",
                    reflection: "build/compiled/reflectionData.hpp",
                    options   : {
                        cmakeDest: "build_cache"
                    }
                },
                unit    : {
                    src    : [properties.sources + "/*.cpp", properties.dependencies + "/*.cpp"],
                    dest   : "build/target",
                    options: {
                        targetName: "UnitTestRunner",
                        cmakeDest : "build_cache"
                    }
                },
                coverage: {
                    src   : [
                        properties.sources + "/*.cpp", properties.dependencies + "/*.cpp",
                        "test/unit/**/*.cpp", "dependencies/**/test/unit/**/*.cpp"
                    ],
                    dest  : "build/compiled/test/coverage/source/cpp",
                    report: "build/reports/coverage/source/cpp"
                }
            };
        }
        return XCppCompile.config;
    }

    protected getName() : string {
        return "xcpp-compile";
    }

    protected async processAsync($option : string) : Promise<void> {
        this.taskOption = $option;
        const configs : IXCppConfigs = XCppCompile.getConfig();

        if (!ObjectValidator.IsEmptyOrNull(configs)) {
            if (configs.hasOwnProperty($option)) {
                let withCoverage : boolean = false;
                if ($option === "coverage") {
                    $option = "unit";
                    withCoverage = true;
                }
                const config : IXCppConfig = configs[$option];
                if (this.properties.projectHas.Cpp.Source()) {
                    await this.compile(config.options, config.dest, withCoverage);
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " XCpp compile task \"" + $option + "\" skipped: " +
                        "0 source files have been found");
                }
            } else {
                LogIt.Error("Configuration for XCpp compile task \"" + $option + "\" does not exist.");
            }
        } else {
            LogIt.Error("Configuration for XCpp compile task does not exist.");
        }
    }

    private binaryResource($options : IXCppConfigOptions, $isSharedLib? : boolean) : void {
        const os : any = require("os");
        const EOL : string = os.EOL;
        const versions : string[] = this.project.version.split("-")[0].split(".");
        let content : string = "";

        let isSharedLib : boolean = false;
        if (!ObjectValidator.IsEmptyOrNull($isSharedLib)) {
            isSharedLib = $isSharedLib;
        }

        if (!isSharedLib) {
            const iconPath : string = this.properties.projectBase + "/" + this.project.target.icon;
            if (this.fileSystem.Exists(iconPath)) {
                content += "0 ICON \"" + iconPath.replace(/\\/gi, "/") + "\"" + EOL + EOL;
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " XCpp compile task \"" + this.taskOption + "\": used default icon for executable, " +
                    "because custom icon has not been found at \"" + iconPath + "\"");
            }
        }
        content += "1 VERSIONINFO" + EOL +
            "FILEVERSION     " + versions[0] + "," + versions[1] + "," + versions[2] + ",0" + EOL +
            "PRODUCTVERSION  " + versions[0] + "," + versions[1] + "," + versions[2] + ",0" + EOL +
            "BEGIN" + EOL +
            "   BLOCK \"StringFileInfo\"" + EOL +
            "   BEGIN" + EOL +
            "       BLOCK \"040904E4\"" + EOL +
            "       BEGIN" + EOL +
            "           VALUE \"ProductName\", \"" + $options.targetName + "\"" + EOL +
            "           VALUE \"ProductVersion\", \"" + this.project.version + "\"" + EOL +
            "           VALUE \"CompanyName\", \"" + this.project.target.companyName + "\"" + EOL +
            "           VALUE \"FileDescription\", \"" + this.project.target.description + "\"" + EOL +
            "           VALUE \"LegalCopyright\", \"" + this.project.target.copyright + "\"" + EOL +
            "           VALUE \"InternalName\", \"" + this.project.name + "\"" + EOL +
            "           VALUE \"FileVersion\", \"" + this.project.version + "\"" + EOL +
            "           VALUE \"OriginalFilename\", \"" + this.project.name + (isSharedLib ? ".dll\"" : ".exe\"") + EOL +
            "           VALUE \"BaseProjectName\", \"" + this.project.name + "\"" + EOL +
            "       END" + EOL +
            "   END" + EOL + EOL +
            "   BLOCK \"VarFileInfo\"" + EOL +
            "   BEGIN" + EOL +
            "       VALUE \"Translation\", 0x409, 1252" + EOL +
            "   END" + EOL +
            "END";
        this.fileSystem.Write(this.properties.projectBase + "/build/compiled/version.rc", content);
    }

    private prepareInitScript($script : string) : string {
        if (!ObjectValidator.IsEmptyOrNull($script)) {
            if (!EnvironmentHelper.IsWindows()) {
                if (this.fileSystem.Exists($script)) {
                    return ". \"" + $script + "\" && ";
                }
            } else {
                if (StringUtils.StartsWith($script, "vcvars")) {
                    $script = "C:/Program Files/Microsoft Visual Studio/2019/Community/VC/Auxiliary/Build/";
                    if (EnvironmentHelper.Is64bit()) {
                        $script += "vcvars64.bat";
                    } else {
                        $script += "vcvarsx86_amd64.bat";
                    }
                }
                return "\"" + $script + "\" && ";
            }
        }
        return "";
    }

    private async compile($options : IXCppConfigOptions, $dest : string, $withCoverage : boolean) : Promise<void> {
        if (!$options.hasOwnProperty("targetName") || $options.targetName === "") {
            $options.targetName = this.project.target.name.replace(" ", "_");
        }
        if (this.build.platform === "app") {
            if (EnvironmentHelper.IsWindows()) {
                this.binaryResource($options);
            } else if (EnvironmentHelper.IsLinux()) {
                LogIt.Info(this.properties.projectBase + "\n" + this.project.target.name + "\n" +
                    this.project.target.icon + "\n" + this.project.target.description);
                ResourceHandler.CreateDesktopEntryTemplate(this.project.target.name, this.project.target.icon);
            }
        } else if (this.build.platform === "shared") {
            if (EnvironmentHelper.IsWindows()) {
                this.binaryResource($options, true);

                const defs : string[] = this.fileSystem.Expand([
                    this.properties.projectBase + "/resource/configs/*.def"
                ]);

                let item : string;
                for (item in defs) {
                    if (defs.hasOwnProperty(item)) {
                        this.fileSystem.Write("build/compiled/" + defs[item],
                            StringReplace.Content(this.fileSystem.Read(defs[item]).toString(), StringReplaceType.VARIABLES,
                                ($value : string) : string => {
                                    if (StringUtils.Contains($value, "project.version")) {
                                        return this.project.version.match(/\d+.\d+/)[0];
                                    }
                                    return $value;
                                }));
                    }
                }
            }
        }

        const toolchain : IToolchainSettings = Toolchains.getToolchain();
        const cmd : string = "cmake";
        let buildPath : string = this.properties.projectBase + "/build_cache";

        if ($options.hasOwnProperty("cmakeDest")) {
            buildPath = $options.cmakeDest.replace("./", this.properties.projectBase + "/");
        }

        if (EnvironmentHelper.IsWindows()) {
            buildPath += "/win";
        } else if (EnvironmentHelper.IsMac()) {
            buildPath += "/mac";
        } else {
            buildPath += "/linux";
        }

        if (!this.fileSystem.Exists($dest)) {
            this.fileSystem.CreateDirectory($dest);
        }
        if (!this.fileSystem.Exists(this.properties.projectBase + "/" + this.properties.compiled + "/lib")) {
            this.fileSystem.CreateDirectory(this.properties.projectBase + "/" + this.properties.compiled + "/lib");
        }
        if (!this.fileSystem.Exists(this.properties.projectBase + "/" + this.properties.compiled + "/bin")) {
            this.fileSystem.CreateDirectory(this.properties.projectBase + "/" + this.properties.compiled + "/bin");
        }

        if (!this.fileSystem.Exists(buildPath) || XCppCacheRegister.IsChanged()) {
            this.fileSystem.CreateDirectory(buildPath);
            let args : string[] = ["-DCMAKE_TOOLCHAIN_FILE=\"" + toolchain.cmakeToolchain + "\"", "-Wno-dev"]
                .concat(toolchain.cmakeOptions);
            if (ObjectValidator.IsEmptyOrNull(toolchain.cmakeGenerator)) {
                if (EnvironmentHelper.IsWindows()) {
                    if (this.project.target.toolchain === ToolchainType.MSVC) {
                        toolchain.cmakeGenerator = "Visual Studio 16 2019";
                    } else if (this.project.target.toolchain === ToolchainType.GCC) {
                        toolchain.cmakeGenerator = "MinGW Makefiles";
                    } else {
                        LogIt.Error("Windows platform supports only \"gcc\" or \"msvc\" toolchain.");
                    }
                } else {
                    toolchain.cmakeGenerator = "Unix Makefiles";
                }
            }
            args = args.concat(["-G", "\"" + toolchain.cmakeGenerator + "\""]);

            if ((await this.terminal.SpawnAsync(this.prepareInitScript(toolchain.initScript) + cmd, args.concat("../.."),
                {cwd: buildPath, env: process.env})).exitCode !== 0) {
                LogIt.Error("XCpp compile task \"" + this.taskOption + "\" has failed");
            }
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " XCpp compile task \"" + this.taskOption + "\": " +
                "cmake skipped, because makefile already exists");
        }
        const args : string[] = ["--build", ".", "--target", $options.targetName];
        if (StringUtils.StartsWith(this.project.target.toolchain, ToolchainType.MSVC)) {
            args.push("--config", this.build.type === CliTaskType.DEV ? "Debug" : "Release");
        }
        args.push("--");
        if (this.project.target.toolchain === ToolchainType.GCC ||
            this.project.target.toolchain === ToolchainType.ARM ||
            this.project.target.toolchain === ToolchainType.AARCH32 ||
            this.project.target.toolchain === ToolchainType.AARCH64 ||
            this.project.target.toolchain === ToolchainType.CLANG) {
            args.push("--jobs=" + EnvironmentHelper.getCores());
        }
        const res : IExecuteResult = await this.terminal.SpawnAsync(this.prepareInitScript(toolchain.initScript) + cmd, args, buildPath);
        XCppCacheRegister.SaveRegister();
        if (this.fileSystem.Expand(
            this.properties.projectBase + "/" + this.properties.compiled + "/lib/*").length === 0) {
            this.fileSystem.Delete(this.properties.projectBase + "/" + this.properties.compiled + "/lib");
        }
        if (this.fileSystem.Expand(
            this.properties.projectBase + "/" + this.properties.compiled + "/bin/*").length === 0) {
            this.fileSystem.Delete(this.properties.projectBase + "/" + this.properties.compiled + "/bin");
        }
        if (res.exitCode !== 0) {
            LogIt.Error("XCpp compile task \"" + this.taskOption + "\" has failed");
        }
    }
}
