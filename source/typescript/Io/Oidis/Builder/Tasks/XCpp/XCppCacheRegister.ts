/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IBuildArgs } from "../../Interfaces/IAppConfiguration.js";
import { IProject } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { CopyScripts } from "../Composition/CopyScripts.js";

export class XCppCacheRegister extends BaseTask {
    private static registerPath : string = "";

    public static SaveRegister() : void {
        Loader.getInstance().getFileSystemHandler()
            .Write(XCppCacheRegister.registerPath, JSON.stringify(XCppCacheRegister.createRegisterData()));
    }

    public static IsChanged() : boolean {
        const data : any = this.loadCacheInfo();
        return JSON.stringify(data.current) !== JSON.stringify(data.last);
    }

    public static IsCleanRequired() : boolean {
        const data : any = this.loadCacheInfo();
        return JSON.stringify(data.current.toolchainSettings) !== JSON.stringify(data.last.toolchainSettings);
    }

    protected static loadCacheInfo() : any {
        const cacheInfo : any = {
            current: XCppCacheRegister.createRegisterData(),
            last   : {}
        };
        if (Loader.getInstance().getFileSystemHandler().Exists(XCppCacheRegister.registerPath)) {
            try {
                cacheInfo.last = JSON.parse(Loader.getInstance().getFileSystemHandler()
                    .Read(XCppCacheRegister.registerPath).toString());
            } catch {
                // dummy
            }
        }
        return cacheInfo;
    }

    private static createRegisterData() : any {
        const build : IBuildArgs = Loader.getInstance().getEnvironmentArgs().getBuildArgs();
        const project : IProject = Loader.getInstance().getProjectConfig();

        let cmakeHash : string = "";
        let item : string;
        const config : any = CopyScripts.getConfig();
        for (item in config.cmake) {
            if (config.cmake.hasOwnProperty(item)) {
                cmakeHash += Loader.getInstance().getFileSystemHandler().Read("bin/" + config.cmake[item]).toString();
            }
        }
        cmakeHash = StringUtils.getSha1(cmakeHash);

        return {
            buildType         : build.type,
            cmakeHash,
            dependenciesConfig: project.dependencies,
            platformType      : build.platform,
            project           : project.name,
            releasesConfig    : project.releases,
            sourceHash        : StringUtils.getSha1(Loader.getInstance().getFileSystemHandler()
                .Expand(Loader.getInstance().getAppProperties().projectBase + "/source/cpp/**/*").join("")),
            targetName        : project.target.name,
            toolchainSettings : project.target.toolchainSettings,
            toolchainType     : project.target.toolchain,
            version           : project.version
        };
    }

    constructor() {
        super();
        XCppCacheRegister.registerPath = this.properties.projectBase + "/build_cache/CacheRegister.json";
    }

    protected getName() : string {
        return "xcpp-cache-register";
    }
}
