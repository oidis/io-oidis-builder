/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { GeneralTaskType } from "../../Enums/GeneralTaskType.js";
import { EnvironmentArgs } from "../../EnvironmentArgs.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class SystemInfo extends BaseTask {

    protected getName() : string {
        return GeneralTaskType.GET_SYSTEM_INFO;
    }

    protected async processAsync() : Promise<void> {
        const os : any = require("os");
        const env : EnvironmentArgs = Loader.getInstance().getEnvironmentArgs();
        LogIt.Info(
            "Oidis Builder " + env.getProjectVersion() + " (" + Convert.TimeToGMTformat(env.getBuildTime()) + "), " +
            os.type() + "(" + os.release() + ") " + os.arch() +
            ", " + os.cpus().length + "x " + os.cpus()[0].model +
            ", RAM: " + Math.round(os.totalmem() / (1024 * 1024 * 1024)) + " GB");
    }
}
