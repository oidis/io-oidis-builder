/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IEnvironmentLoadResponse } from "../../EnvironmentArgs.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class SolutionLoader extends BaseTask {

    protected getName() : string {
        return "solution-loader";
    }

    protected async processAsync() : Promise<void> {
        const onLoad : any = ($res : IEnvironmentLoadResponse) : void => {
            if (!$res.status) {
                LogIt.Error("Failed to load target environment. Missing configuration: " + $res.path);
            }
        };
        const res : IEnvironmentLoadResponse = await Loader.getInstance().getEnvironmentArgs().LoadTargetEnvironment();
        if (res.status || this.programArgs.getOptions().noTarget) {
            onLoad(res);
        } else if (!res.status) {
            let initDownload : boolean = this.fileSystem.IsEmpty(this.properties.projectBase + "/dependencies");
            if (!initDownload && this.programArgs.getTasks().includes("install")) {
                initDownload = true;
            }
            if (initDownload && !StringUtils.Contains(res.path, "http://", "https://", "ftp://")) {
                await this.runTaskAsync("project-cleanup:cache", "dependencies-download:preload", "string-replace:dependencies");
                onLoad(await Loader.getInstance().getEnvironmentArgs().LoadTargetEnvironment());
            } else {
                onLoad(res);
            }
        }
    }
}
