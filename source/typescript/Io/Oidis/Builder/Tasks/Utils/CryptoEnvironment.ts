/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { ProtectionType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ProtectionType.js";

export class CryptoEnvironment {
    private protectionMap : any = null;
    private privateKeyData : any = null;
    private publicKeyData : any = null;
    private protectionType : ProtectionType = ProtectionType.NONE;

    public getProtectionType() : ProtectionType {
        return this.protectionType;
    }

    public getProtectionMap() : any {
        if (!ObjectValidator.IsEmptyOrNull(this.protectionMap)) {
            return this.protectionMap;
        }

        if (this.protectionType === ProtectionType.ENCRYPTION) {
            this.protectionMap = [];
        } else if (this.protectionType === ProtectionType.SIGNATURE) {
            this.protectionMap = {};
        } else if (this.protectionType === ProtectionType.HASH) {
            this.protectionMap = {};
        } else {
            this.protectionMap = {};
        }
        return this.protectionMap;
    }

    public Method($value? : string | boolean) : ProtectionType {
        if (!ObjectValidator.IsEmptyOrNull($value)) {
            let method : string = "";
            if (ObjectValidator.IsString($value)) {
                if ($value !== "sign" && $value !== "encrypt" && $value !== "hash") {
                    throw new Error("Invalid protect value: " + $value);
                }
            } else if (ObjectValidator.IsBoolean($value)) {
                $value = $value ? "sign" : "none";
            }
            method = <string>$value;

            if (ObjectValidator.IsEmptyOrNull(method)) {
                throw new Error("Method invalid.");
            }

            if (method === "sign") {
                this.protectionType = ProtectionType.SIGNATURE;
            } else if (method === "encrypt") {
                this.protectionType = ProtectionType.ENCRYPTION;
            } else if (method === "hash") {
                this.protectionType = ProtectionType.HASH;
            } else {
                this.protectionType = ProtectionType.NONE;
            }
        }

        return this.protectionType;
    }

    public PrivateKeyData() : any {
        if (ObjectValidator.IsEmptyOrNull(this.privateKeyData)) {
            this.generateKeyPair();
        }
        return this.privateKeyData;
    }

    public PublicKeyData() : any {
        if (ObjectValidator.IsEmptyOrNull(this.publicKeyData)) {
            this.generateKeyPair();
        }
        return this.publicKeyData;
    }

    private generateKeyPair() : void {
        if (!ObjectValidator.IsEmptyOrNull(this.publicKeyData) &&
            !ObjectValidator.IsEmptyOrNull(this.privateKeyData)) {
            LogIt.Info("Public and private keys loaded in memory.");
            return;
        }

        const {privateKey, publicKey} = require("crypto").generateKeyPairSync("ec", {
            namedCurve        : "sect239k1",
            privateKeyEncoding: {
                format: "pem",
                type  : "pkcs8"
            },
            publicKeyEncoding : {
                format: "pem",
                type  : "spki"
            }
        });

        this.privateKeyData = privateKey;
        this.publicKeyData = publicKey;
    }
}
