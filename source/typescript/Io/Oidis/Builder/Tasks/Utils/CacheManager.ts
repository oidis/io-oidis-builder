/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { FileSystemHandler } from "../../Connectors/FileSystemHandler.js";
import { ArchType } from "../../Enums/ArchType.js";
import { ProductType } from "../../Enums/ProductType.js";
import { ToolchainType } from "../../Enums/ToolchainType.js";
import { IProjectDependency } from "../../Interfaces/IProject.js";
import { IProperties } from "../../Interfaces/IProperties.js";
import { ITypeScriptConfigs } from "../../Interfaces/ITypeScriptConfig.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { TypeScriptCompile } from "../TypeScript/TypeScriptCompile.js";

export class CacheManager extends BaseTask {
    private rootNamespaces : string[];
    private moduleBase : string;

    public static ReferenceRollback() : void {
        const properties : IProperties = Loader.getInstance().getAppProperties();
        const conf : ITypeScriptConfigs = TypeScriptCompile.getConfig();
        if (conf.hasOwnProperty("source") && conf.source.hasOwnProperty("reference")) {
            const referenceFile : string = properties.projectBase + "/" + conf.source.reference;
            const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
            if (fileSystem.Exists(referenceFile + ".orig")) {
                LogIt.Info("Rollback of " + referenceFile + " file");
                fileSystem.Write(referenceFile, fileSystem.Read(referenceFile + ".orig"));
                fileSystem.Delete(referenceFile + ".orig");
            }
        }
    }

    constructor() {
        super();
        this.rootNamespaces = null;
    }

    protected getName() : string {
        return "cache-manager";
    }

    protected async processAsync($option : string) : Promise<void> {
        const os : any = require("os");
        const concatMap : any = require("concat-with-sourcemaps");
        let concat : any;
        const conf : ITypeScriptConfigs = TypeScriptCompile.getConfig();
        let referenceFile : string;
        let data : string;

        if (this.properties.projectHas.ESModules() && ObjectValidator.IsEmptyOrNull(this.rootNamespaces)) {
            this.rootNamespaces = [];
            this.project.namespaces.forEach(($namespace : string) : void => {
                const root : string = StringUtils.Split($namespace, ".")[0];
                if (!this.rootNamespaces.includes(root) &&
                    !ObjectValidator.IsEmptyOrNull(this.fileSystem.Expand(this.getFilter(root)))) {
                    this.rootNamespaces.push(root);
                }
            });
        }

        if (conf.hasOwnProperty("cache")) {
            if (conf.cache.hasOwnProperty("dest")) {
                switch ($option) {
                case "generate-ts":
                case "generate-prod-ts":
                    CacheManager.ReferenceRollback();
                    if (!this.fileSystem.Exists(this.properties.projectBase + "/" + conf.cache.dest) ||
                        $option === "generate-prod-ts") {
                        if (this.properties.projectHas.ContentFor("dependencies/*/source/typescript/**/*.{ts,tsx}")) {
                            await this.runTaskAsync("typescript:cache");
                        } else {
                            LogIt.Info("Generation of TypeScript cache has been skipped: no dependencies have been found");
                        }
                    } else {
                        LogIt.Info("Generation of TypeScript cache has been skipped: cache already exists");
                    }
                    break;
                case "prepare-ts":
                    if (this.fileSystem.Exists(this.properties.projectBase + "/" + conf.cache.dest)) {
                        const dependenciesReferenceFile : string = conf.cache.dest.replace(".js", ".d.ts");
                        LogIt.Info("Preparation of TypeScript cache: clean up of \"" + dependenciesReferenceFile + "\"");
                        this.fileSystem.Write(dependenciesReferenceFile, this.fileSystem.Read(dependenciesReferenceFile).toString()
                            .replace(/\/\/\/ <reference path="(.*)" \/>/ig, ""));
                        if (conf.hasOwnProperty("source") && conf.source.hasOwnProperty("reference")) {
                            referenceFile = conf.source.reference;
                            if (this.fileSystem.Exists(referenceFile)) {
                                data = this.fileSystem.Read(referenceFile).toString();
                                LogIt.Info("Preparation of TypeScript cache: creation of backup for \"" + referenceFile + "\"");
                                this.fileSystem.Write(referenceFile + ".orig", data);
                                let referenceContent : string = "";
                                if (this.properties.projectHas.ESModules()) {
                                    /// TODO: replacements must be resolved by source code update and removed from here
                                    if (this.fileSystem.Exists(this.properties.projectBase +
                                        "/dependencies/io-oidis-localhost/source/typescript/Types.ts")) {
                                        referenceContent +=
                                            "/// <reference path=\"../../" +
                                            "dependencies/io-oidis-localhost/source/typescript/Types.ts\" />" +
                                            os.EOL;
                                    }
                                    referenceContent +=
                                        this.fileSystem.Read(dependenciesReferenceFile).toString()
                                            .replace(new RegExp("^declare module[\\S\\s]*declare namespace " +
                                                    "Io.Oidis.Localhost.Interfaces \\{[\\S\\s]*abstract class IProject extends " +
                                                    "Io.Oidis.Commons.Interfaces.IProject", "gm"),
                                                "declare namespace Io.Oidis.Localhost.Interfaces {" + os.EOL +
                                                "    abstract class IProject extends Io.Oidis.Commons.Interfaces.IProject")
                                            .replace(/:.Array<(.*)>/gm, ": ($1)[]")
                                            .replace("...$value: (string | FileSystemFilter | FileSystemItemType)[]",
                                                "$value?: (string | FileSystemFilter | FileSystemItemType)[]")
                                            .replace(
                                                "CreateReport($data: IReportProtocol, $sendEmail?: boolean, " +
                                                "$callback?: IResponse): void;",
                                                "CreateReport($data: IReportProtocol, $sendEmail: boolean, $callback: " +
                                                "(($status: boolean) => void) | IResponse): void;") +
                                        os.EOL +
                                        "export {" + this.rootNamespaces.join(",") + "};";
                                } else {
                                    referenceContent =
                                        "/// <reference path=\"../../" + dependenciesReferenceFile + "\" />" + os.EOL + os.EOL;
                                    const lines : string[] = data.split(os.EOL);
                                    let lineIndex : number;
                                    for (lineIndex = 0; lineIndex < lines.length; lineIndex++) {
                                        if (lines[lineIndex].indexOf("/dependencies/") === -1 &&
                                            lines[lineIndex] !== os.EOL) {
                                            referenceContent += lines[lineIndex] + os.EOL;
                                        }
                                    }
                                }
                                LogIt.Info("Preparation of TypeScript cache: creation of \"" + referenceFile + "\" suitable for cache");
                                this.fileSystem.Write(referenceFile, referenceContent);
                            }
                        }
                        TypeScriptCompile.getConfig().source.src = [this.properties.sources + "/*.{ts,tsx}"];
                    } else {
                        LogIt.Info("Preparation of TypeScript cache has been skipped: cache does not exist");
                    }
                    break;
                case "finalize-ts":
                case "finalize-prod-ts":
                case "finalize-ts-nobundle":
                    CacheManager.ReferenceRollback();
                    data = "";
                    if (this.properties.projectHas.ESModules()) {
                        let isServerless : boolean = false;
                        try {
                            if (this.project.target.toolchainSettings.serverless) {
                                isServerless = true;
                            }
                        } catch (ex) {
                            /// TODO: just sanity check should be possible to remove after full refactoring
                            LogIt.Warning("Failed to read toolchainSettings: " + ex.message);
                        }
                        try {
                            await this.cleanBuilds();
                            const tsCacheDest : string = this.properties.projectBase + "/" + conf.cache.dest;
                            if (this.fileSystem.Exists(tsCacheDest)) {
                                const jsFile : string = this.properties.projectBase + "/" + this.properties.dest +
                                    "/resource/esmodules-" + this.build.timestamp + "/reference.js";
                                concat = new concatMap(true, jsFile, "\n");
                                concat.add(null, this.properties.licenseText);
                                concat.add(tsCacheDest,
                                    this.fileSystem.Read(tsCacheDest).toString(),
                                    this.fileSystem.Read(tsCacheDest + ".map").toString());
                                concat.add(null, "export {" + this.rootNamespaces.join(",") + "};");

                                if ($option !== "finalize-prod-ts") {
                                    this.fileSystem.Write(jsFile, concat.content.toString()
                                        .replace(
                                            /^\/\/# sourceMappingURL=dependencies\.js\.map$/gm,
                                            "//# sourceMappingURL=reference.js.map"));
                                    this.fileSystem.Write(jsFile + ".map", concat.sourceMap.toString()
                                        .replace(/\.\.\/dependencies\//gim, "../../../../dependencies/"));
                                } else {
                                    this.fileSystem.Write(jsFile, concat.content.toString()
                                        .replace(/^\/\/# sourceMappingURL=dependencies\.js\.map$/gm, ""));
                                }
                            }
                            const rootPath : string = this.properties.projectBase + "/" + this.properties.dest +
                                "/resource/esmodules-" + this.build.timestamp;
                            let withMapFile : boolean = false;
                            this.fileSystem.Expand([
                                rootPath + "/**/*.js",
                                "!**/namespacesMap.js",
                                "!**/reference.js"
                            ]).forEach(($path : string) : void => {
                                if (this.fileSystem.IsFile($path)) {
                                    let sourceFile : string = this.fileSystem.Read($path).toString();
                                    const nesting : number = StringUtils.OccurrenceCount(
                                        StringUtils.Remove($path, rootPath + "/"), "/");
                                    let modulePath : string = "./";
                                    for (let index : number = 0; index < nesting; index++) {
                                        modulePath += "../";
                                    }
                                    this.properties.esModuleDependencies.forEach(($value : IProjectDependency) : void => {
                                        sourceFile = StringUtils.Replace(sourceFile,
                                            "\"@" + $value.name + "/", "\"" + modulePath);
                                    });
                                    if (this.build.product.Toolchain() === ToolchainType.NODEJS) {
                                        const nodeAlias : string = "nodejs";
                                        if (this.build.product.Type() === ProductType.SHARED ||
                                            this.build.product.Type() === ProductType.PACKAGE) {
                                            sourceFile = StringUtils.Replace(sourceFile, "\"@" + nodeAlias + "/",
                                                "\"" + modulePath + "../nodejs/node_modules/");
                                        } else if (this.build.product.Type() === ProductType.APP) {
                                            sourceFile = StringUtils.Replace(sourceFile, "\"@" + nodeAlias + "/",
                                                "\"/snapshot/node_modules/");
                                        } else {
                                            sourceFile = StringUtils.Replace(sourceFile, "\"@" + nodeAlias + "/",
                                                "\"" + modulePath + "../../../../dependencies/nodejs/build/node_modules/");
                                        }
                                    }
                                    const sourcePathPatter : string = "source/typescript/";
                                    const depsPathPatter : string = "dependencies/";
                                    sourceFile = StringUtils.Replace(sourceFile, "/../" + sourcePathPatter, "/");
                                    this.fileSystem.Write($path, sourceFile
                                        .replace(/\.\.\/\.\.\/dependencies\/.*\/source\/typescript\//gmi, ""));
                                    if (this.fileSystem.Exists($path + ".map")) {
                                        // TODO(mkelnar) this could be partially replaced by compilation options (sourceRoot, mapRoot)
                                        //  but not sure how to resolve dependencies properly, so these replaces are skipped for
                                        //  coverage map (using proper mapping root paths with sourceRoot
                                        //  and without relative backsteps ../)
                                        let mapFile : string = this.fileSystem.Read($path + ".map").toString();
                                        if (!isServerless) {
                                            mapFile = StringUtils.Replace(mapFile, "../" + sourcePathPatter, sourcePathPatter);
                                            mapFile = StringUtils.Replace(mapFile, "../../../" + depsPathPatter, depsPathPatter);
                                        } else {
                                            mapFile = StringUtils.Replace(mapFile,
                                                "../" + sourcePathPatter, "../../../../../" + sourcePathPatter);
                                            mapFile = StringUtils.Replace(mapFile,
                                                "../../../" + depsPathPatter, "../../../../../../../" + depsPathPatter);
                                        }
                                        this.fileSystem.Write($path + ".map", mapFile);
                                        withMapFile = true;
                                    }
                                }
                            });
                            if ($option !== "finalize-ts-nobundle") {
                                await this.buildBundle(withMapFile);
                            }
                        } catch (ex) {
                            LogIt.Error(ex);
                        }
                    } else {
                        const minJsFile : string = this.properties.projectBase + "/" + this.properties.dest +
                            "/resource/javascript/" + this.properties.packageName + ".min.js";
                        concat = new concatMap(true, minJsFile, "\n");
                        concat.add(null, this.properties.licenseText);
                        const tsCacheDest : string = this.properties.projectBase + "/" + conf.cache.dest;
                        if (this.fileSystem.Exists(tsCacheDest)) {
                            concat.add(tsCacheDest,
                                this.fileSystem.Read(tsCacheDest).toString(), this.fileSystem.Read(tsCacheDest + ".map").toString());
                        }
                        const tsSourceDest : string = this.properties.projectBase + "/" + conf.source.dest;
                        if (this.fileSystem.Exists(tsSourceDest)) {
                            LogIt.Info("Generating package JS file: " + minJsFile);
                            concat.add(tsSourceDest,
                                this.fileSystem.Read(tsSourceDest).toString(), this.fileSystem.Read(tsSourceDest + ".map").toString());
                        }
                        this.fileSystem.Write(minJsFile, concat.content.toString()
                            .replace(/^\/\/# sourceMappingURL=dependencies\.js\.map$/gm, "")
                            .replace(
                                /^\/\/# sourceMappingURL=source\.js\.map$/gm,
                                "//# sourceMappingURL=" + this.properties.packageName + ".min.js.map"));
                        this.fileSystem.Write(minJsFile + ".map", concat.sourceMap.toString()
                            .replace(/\.\.\/dependencies\//gim, "../../../../dependencies/"));
                    }
                    break;

                case "generate-sass":
                    if (!this.fileSystem.Exists(this.properties.projectBase + "/build_cache/dependencies.css") ||
                        this.properties.projectHas.ESModules()) {
                        /// TODO: sass cache rebuild is forced for ESModules because TS sources are not cache
                        //        > re-enable sass cache when TS cache will be supported also for ESModules
                        if (this.properties.projectHas.ContentFor("dependencies/*/resource/sass/**/*.scss")) {
                            await this.runTaskAsync("sass-compile:cache");
                        } else {
                            LogIt.Info("Generation of SASS cache has been skipped: no dependencies have been found");
                        }
                    } else {
                        LogIt.Info("Generation of SASS cache has been skipped: cache already exists");
                    }
                    break;
                case "finalize-sass":
                    // eslint-disable-next-line no-case-declarations
                    const sassCacheDest : string = this.properties.projectBase + "/build_cache/sass/cache.css";
                    // eslint-disable-next-line no-case-declarations
                    const sassDependencies : string = this.properties.projectBase + "/build_cache/dependencies.css";
                    if (this.fileSystem.Exists(sassCacheDest)) {
                        this.fileSystem.Write(sassDependencies, this.fileSystem.Read(sassCacheDest));
                        this.fileSystem.Write(sassDependencies + ".map", this.fileSystem.Read(sassCacheDest + ".map"));
                    }
                    if (this.fileSystem.Exists(this.properties.projectBase + "/build_cache/sass")) {
                        this.fileSystem.Delete(this.properties.projectBase + "/build_cache/sass");
                    }
                    // eslint-disable-next-line no-case-declarations
                    const minCssFile : string = this.properties.projectBase + "/" + this.properties.dest +
                        "/resource/css/" + this.properties.packageName + ".min.css";
                    LogIt.Info("Generating package CSS file: " + minCssFile);
                    concat = new concatMap(true, minCssFile, "\n");
                    concat.add("", this.properties.licenseText, null);
                    if (this.fileSystem.Exists(sassDependencies)) {
                        if (this.fileSystem.Exists(sassDependencies + ".map")) {
                            concat.add("dependencies.css",
                                this.fileSystem.Read(sassDependencies).toString(),
                                this.fileSystem.Read(sassDependencies + ".map").toString());
                        } else {
                            concat.add("dependencies.css", this.fileSystem.Read(sassDependencies).toString(), null);
                            this.fileSystem.Delete(sassDependencies);
                            LogIt.Warning("Invalidating dependencies SASS cache due to missing map file. " +
                                "Current build may have malfunction in CSS mapping.");
                        }
                    }
                    this.fileSystem.Expand(this.properties.projectBase + "/build/compiled/source/sass/**/*.css")
                        .forEach(($file : string) : void => {
                            concat.add($file, this.fileSystem.Read($file).toString(), this.fileSystem.Read($file + ".map").toString());
                        });
                    this.fileSystem.Write(minCssFile, concat.content.toString());
                    this.fileSystem.Write(minCssFile + ".map", concat.sourceMap.toString());

                    break;

                case "finalize-chromiumre":
                    this.prepareConnector();
                    this.moduleBase = this.getModuleBase("com-wui-framework-chromiumre");
                    if (this.fileSystem.Exists(this.moduleBase)) {
                        this.copyRelease(this.moduleBase, "WuiChromiumRE");
                    }
                    break;

                case "finalize-eclipse":
                    this.prepareConnector();
                    this.moduleBase = this.getModuleBase("com-wui-framework-idejre-eclipse");
                    // eslint-disable-next-line no-case-declarations
                    const jarFile : string[] = this.fileSystem.Expand(this.moduleBase + "/com-wui-framework-idejre-*.jar");
                    if (this.fileSystem.Exists(this.moduleBase) && jarFile.length === 1) {
                        await this.fileSystem
                            .UnpackAsync(jarFile[0], <any>{output: this.properties.projectBase + "/build_cache/WuiEclipseRe"});
                    } else {
                        LogIt.Error("Unable to locate Eclipse source module.");
                    }
                    break;
                case "finalize-idea":
                    this.prepareConnector();
                    this.moduleBase = this.getModuleBase("com-wui-framework-idejre-idea");
                    // eslint-disable-next-line no-case-declarations
                    const zipFile : string[] = this.fileSystem.Expand(this.moduleBase + "/com-wui-framework-idejre-*.zip");
                    if (this.fileSystem.Exists(this.moduleBase) && zipFile.length === 1) {
                        await this.fileSystem
                            .UnpackAsync(zipFile[0], <any>{output: this.properties.projectBase + "/build_cache/WuiIdeaRe"});
                        const appPath : string = this.properties.projectBase + "/build_cache/WuiIdeaRe/app";
                        const libPath : string = this.properties.projectBase + "/build_cache/WuiIdeaRe/lib";
                        await this.fileSystem.UnpackAsync(libPath + "/com-wui-framework-idejre.jar", <any>{output: appPath});
                        this.fileSystem.Delete(libPath + "/com-wui-framework-idejre.jar");
                    } else {
                        LogIt.Error("Unable to locate Idea source module.");
                    }
                    break;

                default:
                    // Do nothing
                    break;
                }
            }
        }
    }

    private getFilter($root : string) : string[] {
        const output : string[] = [];
        this.properties.oidisDependencies.forEach(($value : IProjectDependency) : void => {
            if (this.properties.esModuleDependencies.indexOf($value) === -1) {
                output.push(this.properties.projectBase + "/dependencies/" + $value.name + "/source/typescript/" + $root + "/");
            }
        });
        return output;
    }

    private getModuleBase($name : string) : string {
        let moduleBase : string = this.wuiModules + "/" + $name + "/" +
            this.build.product.OS() + "/" + (this.build.product.Arch() === ArchType.X32 ? "32bit" : "64bit");
        if (!this.fileSystem.Exists(moduleBase)) {
            moduleBase = this.wuiModules + "/" + $name + "/" + this.build.product.OS() + "/32bit";
        }
        return moduleBase;
    }

    private copyRelease($source : string, $destFolder : string) : void {
        const sourcePathLength : number = $source.length;
        const files : string[] = this.fileSystem.Expand($source + "/**/*");
        if (files.length > 0) {
            for (const file of files) {
                if (!this.fileSystem.IsDirectory(file) &&
                    !StringUtils.Contains(file, ".pdb", "README.md") &&
                    !StringUtils.EndsWith(file, ".config")) {
                    this.fileSystem.Write(this.properties.projectBase + "/build_cache/" +
                        $destFolder + file.substr(sourcePathLength, file.length), this.fileSystem.Read(file));
                }
            }
        }
    }

    private prepareConnector() : void {
        this.moduleBase = this.getModuleBase("com-wui-framework-connector");
        if (this.fileSystem.Exists(this.moduleBase)) {
            this.copyRelease(this.moduleBase, "OidisConnector");
        }
        this.moduleBase = this.getModuleBase("com-wui-framework-launcher");
        if (this.fileSystem.Exists(this.moduleBase)) {
            this.copyRelease(this.moduleBase, "WuiLauncher");
        }
    }

    private async buildBundle($withMap : boolean = false) : Promise<void> {
        const rollup : any = require("rollup");
        let loaderClass : string = this.project.loaderClass;
        if (!ObjectValidator.IsEmptyOrNull(this.project.guiLoaderClass)) {
            loaderClass = this.project.guiLoaderClass;
        } else {
            // todo(mkelnar) looks like rollup tries to create bundle also from npm modules but resolved path
            //  is in ./target/resource/nodejs/node_modules folder while this location not exists at this time
            //  (will be created after bundling and copying rest of resources)
            //  => this feature needs to be called after the node resources will be copied or use path from dependencies folder
            //     but this can have consequences somewhere
            //  ! skipping for now, lets bundle only FE project for now
            LogIt.Warning("Bundling is not supported for BE only projects right now. To be implemented.");
            return;
        }
        const loaderPath : string = this.properties.projectBase + "/" + this.properties.dest +
            "/resource/esmodules-" + this.build.timestamp + "/" +
            StringUtils.Replace(loaderClass, ".", "/") + ".js";
        LogIt.Debug("> bundle input: " + loaderPath);
        let bundle : any;
        let buildFailed : Error = null;
        try {
            const sourcemaps : any = require("rollup-plugin-sourcemaps");
            bundle = await rollup.rollup({
                input  : loaderPath,
                plugins: [sourcemaps()],
                onwarn : ($data : any, $warn : any) : void => {
                    try {
                        if ($data.code === "CIRCULAR_DEPENDENCY" ||
                            $data.code === "THIS_IS_UNDEFINED") {
                            return;
                        }
                        if (!ObjectValidator.IsEmptyOrNull($data.loc)) {
                            LogIt.Warning(`${$data.loc.file} (${$data.loc.line}:${$data.loc.column}) ${$data.message}`);
                            if ($data.frame) {
                                LogIt.Warning($data.frame);
                            }
                        } else {
                            LogIt.Warning($data.message);
                        }
                    } catch (ex) {
                        LogIt.Debug("> Rollup warn translation error:\n" + ex.stack);
                        $warn($data);
                    }
                }
            });
            await bundle.write({
                format              : "iife",
                banner              : this.properties.licenseText,
                name                : StringUtils.Remove(this.project.target.name, " ", ".", "/", "-"),
                inlineDynamicImports: true,
                file                : this.properties.projectBase + "/" + this.properties.dest + "/resource/javascript/" +
                    this.properties.packageName + ".js",
                sourcemap           : $withMap
            });
        } catch (ex) {
            buildFailed = ex;
        }
        if (!ObjectValidator.IsEmptyOrNull(bundle)) {
            await bundle.close();
        }
        if (!ObjectValidator.IsEmptyOrNull(buildFailed)) {
            throw buildFailed;
        }
    }

    private async cleanBuilds() : Promise<void> {
        const modulesPath : string = this.properties.projectBase + "/" + this.properties.dest + "/resource/esmodules-";
        const previousBuilds : string[] = this.fileSystem.Expand([
            modulesPath + "*",
            "!" + modulesPath + this.build.timestamp
        ]);
        for await (const path of previousBuilds) {
            await this.fileSystem.DeleteAsync(path);
        }
    }
}
