/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import {ColorType} from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { UpdatesManagerConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/UpdatesManagerConnector.js";
import { EnvironmentArgs } from "../../EnvironmentArgs.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class Selfupdate extends BaseTask {

    protected getName() : string {
        return "selfupdate";
    }

    protected process($done : any, $option : string) : void {
        const register : IPersistenceHandler = PersistenceFactory.getPersistence(this.getClassName());
        let updaterPath : string = this.fileSystem.getTempPath() + "/OidisBuilderUpdate";
        if (EnvironmentHelper.IsWindows()) {
            updaterPath += ".exe";
        }

        const validate : any = ($onUpdate? : () => void) : void => {
            const connector : UpdatesManagerConnector = new UpdatesManagerConnector(3,
                this.builderConfig.hub.url + "/connector.config.jsonp");
            connector.ErrorPropagation(false);
            connector.getEvents().OnError(() : void => {
                LogIt.Warning("Failed to check Oidis Framework Builder updates. Continuing ...");
                $done();
            });
            const environment : EnvironmentArgs = Loader.getInstance().getEnvironmentArgs();
            connector
                .UpdateExists(
                    environment.getAppName(),
                    environment.getReleaseName(),
                    environment.getPlatform(),
                    environment.getProjectVersion(),
                    new Date(environment.getBuildTime()).getTime())
                .Then(($success : boolean) : void => {
                    if ($success) {
                        if (ObjectValidator.IsSet($onUpdate)) {
                            $onUpdate();
                        } else {
                            LogIt.Error("Oidis Framework Builder should be self installed. " +
                                "Please run 'oidis update' command.");
                            $done();
                        }
                    } else {
                        register.Variable("LastSelfUpdate", new Date().getTime());
                        if (this.programArgs.IsForce() && ObjectValidator.IsSet($onUpdate)) {
                            $onUpdate();
                        } else {
                            LogIt.Info(">>"[ColorType.YELLOW] + " Oidis Framework Builder is up to date.");
                            $done();
                        }
                    }
                });
        };
        if ($option === "check") {
            if (this.builderConfig.noSelfupdate) {
                LogIt.Info("Selfupdate check skipped due to builder configuration.");
                $done();
            } else if (register.Exists("LastSelfUpdate")) {
                if ((new Date().getTime() - register.Variable("LastSelfUpdate")) > (24 * 3600000) ||
                    this.programArgs.IsForce()) {
                    this.fileSystem.Delete(updaterPath);
                    validate();
                } else {
                    LogIt.Info("Selfupdate check skipped. Recommended period is once per day, " +
                        "to force run selfupdate:check please run task with --force option.");
                    $done();
                }
            } else {
                validate();
            }
        } else {
            validate(() : void => {
                this.fileSystem.Delete(updaterPath);
                const updaterSource : string = this.builderConfig.target.updater;
                if (ObjectValidator.IsEmptyOrNull(updaterSource)) {
                    LogIt.Warning("Update skipped: Updater configuration is missing. " +
                        "Please validate target.updater property in package.conf.json or private.conf.json.");
                    $done();
                } else {
                    this.fileSystem.Download(updaterSource, ($headers : string, $bodyOrPath : string) : void => {
                        this.fileSystem.Copy($bodyOrPath, updaterPath, () : void => {
                            this.fileSystem.Delete($bodyOrPath);
                            const detached : any = () : void => {
                                register.Variable("LastSelfUpdate", new Date().getTime());
                                LogIt.Info(">>"[ColorType.YELLOW] +
                                    " Update has been started, please wait for notification...");
                                $done();
                            };
                            const detachDetect : any = setTimeout(() : void => {
                                detached();
                            }, 2500);
                            this.terminal.Spawn(require("path").basename(updaterPath), [], {
                                    cwd: this.fileSystem.getTempPath()
                                },
                                ($exitCode : number) : void => {
                                    clearTimeout(detachDetect);
                                    if ($exitCode !== 0) {
                                        LogIt.Error("Unable to start update process.");
                                    } else {
                                        detached();
                                    }
                                });
                        });
                    });
                }
            });
        }
    }
}
