/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class WuiAppKill extends BaseTask {

    protected getName() : string {
        return "wui-app-kill";
    }

    protected async processAsync($option : string) : Promise<void> {
        let extension : string = "";
        if (EnvironmentHelper.IsWindows()) {
            extension = ".exe";
        }
        const appName : string = this.project.target.name;
        const cwd : string = this.properties.projectBase + "/build/target";
        if (this.fileSystem.Exists(cwd + "/build/target/" + appName + extension) &&
            this.fileSystem.Exists(cwd + "/wuichromiumre/" + appName + extension)) {
            try {
                await this.terminal.KillAsync(cwd + "/" + appName + extension);
            } catch (ex) {
                LogIt.Error("Oidis app kill: unable to stop ChromiumRE process");
            }
        }
        const files : string[] = this.fileSystem.Expand([
            this.properties.projectBase + "/build/**/WuiConnector/*Connector" + extension,
            this.properties.projectBase + "/build/**/OidisConnector/*Connector" + extension
        ]);
        if (files.length > 0) {
            let stopSuccess : boolean = true;
            for await (const file of files) {
                if (!this.fileSystem.IsDirectory(file)) {
                    try {
                        await this.terminal.KillAsync(file);
                    } catch (ex) {
                        stopSuccess = false;
                    }
                }
            }
            if (!stopSuccess) {
                LogIt.Error("Oidis app kill: unable to stop all process");
            } else {
                LogIt.Info("Oidis app kill: all process stopped successfully");
            }
        } else {
            LogIt.Info("Oidis app kill: skipped, because application or OidisConnector has not been found");
        }
    }
}
