/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { StdinManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/StdinManager.js";
import { Resources } from "../../DAO/Resources.js";
import { IDeployServers } from "../../Interfaces/IDeployServers.js";
import { INexusConfig } from "../../Interfaces/IProject.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class Nexus extends BaseTask {
    private config : INexusConfig;

    public async Upload($profile? : string) : Promise<void> {
        const profile : string = ObjectValidator.IsEmptyOrNull($profile) ?
            Loader.getInstance().getAppConfiguration().build.type : $profile;
        await this.ResolveConfig();
        const url = this.config.location[profile.toLowerCase()];
        let files : string[] = [];
        if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getProgramArgs().getOptions().file)) {
            files.push(Loader.getInstance().getProgramArgs().getOptions().file);
        } else {
            files = Loader.getInstance().getFileSystemHandler().Expand(
                Loader.getInstance().getAppProperties().projectBase + this.config.filter);
        }
        if (files.length === 1) {
            LogIt.Info("Uploading {0} to NEXUS: {1}", files[0], url);
            const result : IExecuteResult = await Loader.getInstance().getTerminal().ExecuteAsync(
                "curl",
                [
                    "--fail",
                    "-u", this.config.user + ":" + this.config.pass,
                    "--upload-file",
                    files[0],
                    url + "/" + require("path").basename(files[0])
                ],
                {cwd: Loader.getInstance().getAppProperties().projectBase, verbose: Loader.getInstance().getProgramArgs().IsDebug()});
            if (result.exitCode === 0) {
                LogIt.Info("Upload done.");
            } else {
                LogIt.Error("Upload failed: \n" + result.std.join("\n"));
            }
        } else {
            LogIt.Error("Multiple or none packages found please check configuration and project condition: \n{0}",
                JSON.stringify(files));
        }
    }

    public async ResolveConfig() : Promise<INexusConfig> {
        if (ObjectValidator.IsEmptyOrNull(this.config)) {
            const builderConfig : INexusConfig = Loader.getInstance().getAppConfiguration().nexusConfig;
            LogIt.Debug("nexus: builder config: {0}", builderConfig);
            if (!Loader.getInstance().getProgramArgs().getOptions().noTarget) {
                const projectConfig : INexusConfig = Loader.getInstance().getProjectConfig().nexusConfig;
                LogIt.Debug("nexus: project config: {0}", projectConfig);
                this.config = Resources.Extend(builderConfig, projectConfig);
            } else {
                this.config = builderConfig;
            }

            if (ObjectValidator.IsEmptyOrNull(this.config) || ObjectValidator.IsEmptyOrNull(this.config.location)) {
                LogIt.Error("Nexus location is undefined. Please fix nexusConfig definition in builder " +
                    "(mandatory for --no-target mode) or project config.");
            } else if (ObjectValidator.IsString(this.config.location)) {
                const loc : string = <any>(this.config.location);
                this.config.location = <IDeployServers>{};
                this.config.location.dev = this.config.location.eap = this.config.location.prod = loc;
            }

            if (Loader.getInstance().getProgramArgs().IsForce()) {
                LogIt.Info("Forcing credentials from command line.");
                this.config.user = "";
                this.config.pass = "";
            }

            if (ObjectValidator.IsEmptyOrNull(this.config.user)) {
                await StdinManager.ProcessAsync([
                    {
                        callback($value, $callback) {
                            this.config.user = $value;
                            $callback(true);
                        },
                        prefix: "userOrToken"
                    }
                ]);
            }

            const tmp = StringUtils.Split(Buffer.from(this.config.user, "base64").toString("ascii"), ":")
                .filter(($value) => !ObjectValidator.IsEmptyOrNull($value));
            if (!ObjectValidator.IsEmptyOrNull(tmp) && tmp.length === 2) {
                LogIt.Info("base64 encoded string found in user property, replacing user and pass by parsed data");
                this.config.user = tmp[0];
                this.config.pass = tmp[1];
            }
            if (ObjectValidator.IsEmptyOrNull(this.config.pass)) {
                await StdinManager.ProcessAsync([
                    {
                        callback($value, $callback) {
                            this.config.pass = $value;
                            $callback(true);
                        },
                        hidden: true,
                        prefix: "pass"
                    }
                ]);
            }

            LogIt.Debug("nexus: config: {0}", this.config);
        }
        return this.config;
    }

    protected getName() : string {
        return "nexus";
    }

    protected async processAsync($option? : string) : Promise<void> {
        return this.Upload($option);
    }
}
