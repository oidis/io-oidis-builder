/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import * as path from "path";
import { Resources } from "../../DAO/Resources.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { IDockerRegistryConfig } from "../../Interfaces/IAppConfiguration.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { StringReplace } from "./StringReplace.js";

export class Docker extends BaseTask {
    private context : DockerContext;
    private dockerContextPath : string;
    private docker : any;

    /**
     * @returns Returns internal dockerode instance. (see https://www.npmjs.com/package/dockerode)
     */
    public getDocker() : any {
        if (ObjectValidator.IsEmptyOrNull(this.docker)) {
            this.docker = new (require("dockerode"))();
            this.getDocker = () : any => {
                return this.docker;
            };
        }
        return this.docker;
    }

    /**
     * Invoke docker/dockerode endpoint API function as wrapper for
     *   const docker = new (require("dockerode"))()
     *   docker.<function>(<args>).then(...).catch(...)
     * This also automatically handle Docker API endpoint JSON stream and forward it to data sink (console log by default)
     * and errors are propagated into promise rejection.
     * @param $fcn API function reference.
     * @param $context Context for API function. Use default (docker root object) by setting this argument to NULL.
     * @param $args Argument list.
     */
    public async DockerApiInvoker($fcn : any, $context : any, ...$args : any[]) : Promise<void> {
        if (ObjectValidator.IsEmptyOrNull($context)) {
            $context = this.getDocker();
        }
        return new Promise<void>(($resolve, $reject) : void => {
            $fcn.apply($context, $args)
                .then(($response : any) : void => {
                    if (!ObjectValidator.IsEmptyOrNull($response.pipe)) {  // tag response is empty buffer ??
                        const parser : any = require("JSONStream").parse();
                        $response.pipe(parser);
                        parser.on("data", ($data : any) : void => {
                            if (!ObjectValidator.IsEmptyOrNull($data.error)) {
                                $reject(new Error($data.error));
                            }
                            if (!ObjectValidator.IsEmptyOrNull($data.stream)) {
                                this.dataSink($data.stream);
                            } else if (!ObjectValidator.IsEmptyOrNull($data.status)) {
                                if (!ObjectValidator.IsEmptyOrNull($data.progress)) {
                                    this.dataSink($data.status + " [" + $data.id + "]... " + $data.progress);
                                } else if (!ObjectValidator.IsEmptyOrNull($data.id)) {
                                    this.dataSink($data.status + " [" + $data.id + "]");
                                } else {
                                    this.dataSink($data.status);
                                }
                            }
                        });
                        parser.on("end", () : void => {
                            $resolve();
                        });
                    } else if (!ObjectValidator.IsEmptyOrNull($response.message)) {
                        $reject(new Error($response.message));
                    } else {
                        $resolve();
                    }
                })
                .catch($reject);
        });
    }

    public async BuildImage($options? : BuildOptions) : Promise<string> {
        let options : BuildOptions = {
            advanced: {},
            name    : "",
            useEnv  : true
        };
        if (ObjectValidator.IsEmptyOrNull($options)) {
            if (!ObjectValidator.IsEmptyOrNull(this.context.config) && !ObjectValidator.IsEmptyOrNull(this.context.config.build)) {
                options = Resources.Extend(options, this.context.config.build);
                if (!ObjectValidator.IsEmptyOrNull(options.name) && (!StringUtils.Contains(options.name, ":") ||
                        StringUtils.IndexOf(options.name, ":", false) < StringUtils.IndexOf(options.name, "/")) &&
                    ObjectValidator.IsArray(this.context.config.version) && this.context.config.version.length > 0) {
                    options.name = options.name + ":" + this.context.config.version[0];
                }
            }
        } else {
            options = Resources.Extend(options, $options);
        }

        let buildArgs : any = {};
        if (ObjectValidator.IsObject(options.useEnv)) {
            buildArgs = <any>(options.useEnv);
        } else if (options.useEnv && this.fileSystem.Exists(this.dockerContextPath + "/.env")) {
            const envString : string = StringUtils.Remove(<string>this.fileSystem.Read(this.dockerContextPath + "/.env"), "\r");
            if (!ObjectValidator.IsEmptyOrNull(envString)) {
                StringUtils.Split(envString, "\n").forEach(($line : string) : void => {
                    if (!StringUtils.StartsWith($line, "#")) {
                        const result : any = /([a-zA-Z0-9_]+)=(.*)/.exec($line);
                        if (!ObjectValidator.IsEmptyOrNull(result)) {
                            if (!buildArgs.hasOwnProperty(result[1])) {
                                buildArgs[result[1]] = result[2];
                            }
                        }
                    } else {
                        LogIt.Warning("Commented environment variable in \"" + this.dockerContextPath + "/.env file is skipped.");
                    }
                });
            }
        }

        if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getAppConfiguration().httpProxy)) {
            buildArgs.http_proxy = buildArgs.HTTP_PROXY = Loader.getInstance().getAppConfiguration().httpProxy;
        }
        if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getAppConfiguration().httpsProxy)) {
            buildArgs.https_proxy = buildArgs.HTTPS_PROXY = Loader.getInstance().getAppConfiguration().httpsProxy;
        }
        if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getAppConfiguration().noProxy)) {
            buildArgs.no_proxy = buildArgs.NO_PROXY = Loader.getInstance().getAppConfiguration().noProxy;
        }

        const buildOptions : any = options.advanced;
        if (!ObjectValidator.IsEmptyOrNull(options.name)) {
            buildOptions.t = options.name;
        }
        if (!ObjectValidator.IsEmptyOrNull(buildArgs)) {
            buildOptions.buildargs = buildArgs;
            LogIt.Debug("Docker buildargs: {0}", buildArgs);
        }
        if (this.programArgs.IsForce()) {
            buildOptions.nocache = true;
        }
        if (!ObjectValidator.IsEmptyOrNull(this.builderConfig.dockerRegistry)) {
            // TODO(mkelnar) when base image in private repo will have dependency from another private repo
            //  then this api needs to be modified to be able to pass multiple credentials
            buildOptions.registryconfig = {};
            buildOptions.registryconfig[this.builderConfig.dockerRegistry.url] = {
                password: this.builderConfig.dockerRegistry.pass,
                username: this.builderConfig.dockerRegistry.user
            };
        }
        await this.DockerApiInvoker(this.getDocker().buildImage, null, {
            context: this.dockerContextPath,
            src    : this.fileSystem.Expand(this.dockerContextPath + "/**/*")
                .map(($value) => StringUtils.Remove($value, this.dockerContextPath + "/"))
        }, buildOptions);
        return buildOptions.t;
    }

    public async PullImage($image : string) : Promise<void> {
        return this.DockerApiInvoker(this.getDocker().pull, null, $image, {authconfig: this.createAuth()});
    }

    public async PushImage($image : string, $tag? : string) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull($tag) && $image !== $tag) {
            await this.TagImage($image, $tag);
        } else {
            $tag = $image;
        }
        LogIt.Info("Pushing: " + $tag);
        const image : any = this.getDocker().getImage($tag);
        return this.DockerApiInvoker(image.push, image, {authconfig: this.createAuth()});
    }

    public async TagImage($image : string, $tag : string) : Promise<void> {
        const repo : string = StringUtils.Substring($tag, 0, StringUtils.IndexOf($tag, ":"));
        const tag : string = StringUtils.Remove($tag, repo, ":");
        const image = this.getDocker().getImage($image);
        return this.DockerApiInvoker(image.tag, image, {repo, tag});
    }

    public async CheckAuth($authOptions : IDockerRegistryConfig) : Promise<void> {
        const auth : any = this.createAuth($authOptions);
        if (ObjectValidator.IsEmptyOrNull(auth)) {
            throw new Error("Empty docker registry credentials could not be validated. " +
                "Please update builder config or pass it to arguments.");
        }
        return this.DockerApiInvoker(this.getDocker().checkAuth, null, auth);
    }

    public async ComposeUp($yaml : string) : Promise<void> {
        const args : string[] = ["-f", $yaml, "up", "-d"];
        if (Loader.getInstance().getProgramArgs().IsForce()) {
            args.push("--force-recreate");
        }
        const result : IExecuteResult = await this.terminal.SpawnAsync("docker-compose", args,
            {
                advanced: {
                    noTerminalLog: true
                },
                cwd     : this.dockerContextPath
            });
        if (result.exitCode !== 0) {
            throw new Error(result.std[1]);
        }
    }

    public async ComposeDown($yaml : string) : Promise<void> {
        const result : IExecuteResult = await this.terminal.SpawnAsync("docker-compose", ["-f", $yaml, "down", "--remove-orphans"],
            {
                advanced: {
                    noTerminalLog: true
                },
                cwd     : this.dockerContextPath
            });
        if (result.exitCode !== 0) {
            throw new Error(result.std[1]);
        }
    }

    public async ComposePull($yaml : string) : Promise<void> {
        const result : IExecuteResult = await this.terminal.SpawnAsync("docker-compose", ["-f", $yaml, "pull"],
            {
                advanced: {
                    noTerminalLog: true
                },
                cwd     : this.dockerContextPath
            });
        if (result.exitCode !== 0) {
            throw new Error(result.std[1]);
        }
    }

    public ResolveContext($filePath? : string) : any {
        this.context = {
            buildAllowed  : false,
            composeAllowed: false,
            config        : null,
            file          : ""
        };
        let configFile : string;
        if (ObjectValidator.IsEmptyOrNull($filePath)) {
            $filePath = this.fileSystem.NormalizePath(this.programArgs.getOptions().file);
        }

        let fileOrDir : string = this.getCwd();
        if (!ObjectValidator.IsEmptyOrNull($filePath)) {
            if (!path.isAbsolute($filePath)) {
                fileOrDir = this.fileSystem.NormalizePath(fileOrDir + "/" + $filePath);
            } else {
                fileOrDir = $filePath;
            }
            if (path.basename(fileOrDir).match(/\w+\.config\.json/g)) {
                configFile = path.basename(fileOrDir);
                fileOrDir = path.dirname(fileOrDir);
            }
        }

        if (this.fileSystem.IsFile(fileOrDir)) {
            // only dockerfile and .yml are supported right now
            this.dockerContextPath = path.dirname(fileOrDir);
            this.context.file = path.basename(fileOrDir);

            if (path.extname(this.context.file) === ".yml") {
                this.context.composeAllowed = true;
                this.context.file = path.basename($filePath);

                if (!this.fileSystem.Exists(this.dockerContextPath + "/" + this.context.file)) {
                    this.context.file = "";
                }
            } else if (this.context.file.toLowerCase() === "dockerfile") {
                this.context.buildAllowed = true;
            } else {
                return null;
            }
        } else if (this.fileSystem.IsDirectory(fileOrDir)) {
            this.dockerContextPath = fileOrDir;
            if (!this.fileSystem.Expand(this.dockerContextPath + "/*").some(($path) => this.fileSystem.IsFile($path))) {
                return null;
            }
            // directory could be used to build (needs Dockerfile) or default compose (needs docker-compose.yml)
            if (this.fileSystem.IsFile(this.dockerContextPath + "/Dockerfile")) {
                this.context.buildAllowed = true;
            }
            if (this.fileSystem.IsFile(this.dockerContextPath + "/docker-compose.yml")) {
                this.context.file = "docker-compose.yml";
                this.context.composeAllowed = true;
            }
        } else {
            return null;
        }

        if (!ObjectValidator.IsEmptyOrNull(configFile)) {
            configFile = this.fileSystem.NormalizePath(this.dockerContextPath + "/" + configFile);
        } else {
            configFile = this.dockerContextPath + "/docker.config.json";
        }
        if (this.fileSystem.Exists(configFile)) {
            try {
                this.context.config = JSON.parse(
                    StringReplace.Content(this.fileSystem.Read(configFile).toString(), StringReplaceType.VARIABLES));
                if (ObjectValidator.IsString(this.context.config.version)) {
                    this.context.config.version = [<any>(this.context.config.version)];
                }
            } catch ($e) {
                LogIt.Error("Failed to load and parse JSON file '" + path.basename(configFile) + "' from '" + this.dockerContextPath + "'");
            }
        }
        return this.context;
    }

    protected dataSink($data : string) : void {
        Echo.Print($data);
    }

    protected getName() : string {
        return "docker";
    }

    protected getCwd() : string {
        return process.cwd();
    }

    protected async processAsync($option? : string) : Promise<void> {
        const ctx : DockerContext = this.ResolveContext();
        LogIt.Debug("Processing docker: {0}", JSON.stringify(ctx));
        if ($option === "build" && ctx.buildAllowed) {
            await this.BuildImage();
        } else if ($option === "push") {
            if (!ObjectValidator.IsEmptyOrNull(ctx.config) && !ObjectValidator.IsEmptyOrNull(ctx.config.build) &&
                !ObjectValidator.IsEmptyOrNull(ctx.config.build.name)) {
                let imageName : string = ctx.config.build.name;
                let tags : string[] = JsonUtils.Clone(ctx.config.version);
                // be sure that tag is not resolved from location port
                if (StringUtils.Contains(imageName, ":") &&
                    (StringUtils.IndexOf(imageName, ":", false) > StringUtils.IndexOf(imageName, "/"))) {
                    const parts : string[] = StringUtils.Split(imageName, ":");
                    if (parts.length > 2) {
                        imageName = parts[0] + ":" + parts[1];
                        tags = [parts[2]].concat(tags);
                    } else {
                        imageName = parts[0];
                        tags = [parts[1]].concat(tags);
                    }

                }
                if (ObjectValidator.IsEmptyOrNull(tags)) {
                    tags = ["latest"];
                } else {
                    tags = tags.filter(($value, $index) => tags.indexOf($value) === $index);
                }
                const firstTag : string = imageName + ":" + tags[0];
                for await(const tag of tags) {
                    await this.PushImage(firstTag, imageName + ":" + tag);
                }
            } else {
                LogIt.Error("Missing information for image push. Please specify docker.config.json file properly.");
            }
        } else if ($option === "auth") {
            return this.CheckAuth(this.builderConfig.dockerRegistry);
        } else if ($option === "up" && ctx.composeAllowed) {
            return this.ComposeUp(ctx.file);
        } else if ($option === "down" && ctx.composeAllowed) {
            return this.ComposeDown(ctx.file);
        } else if ($option === "pull" && ctx.composeAllowed) {
            return this.ComposePull(ctx.file);
        } else {
            LogIt.Error("Unsupported task option: '" + $option + "' in path: " + this.dockerContextPath);
        }
    }

    protected createAuth($registry? : IDockerRegistryConfig) : any {
        let authconfig : any = null;
        if (ObjectValidator.IsEmptyOrNull($registry) && !ObjectValidator.IsEmptyOrNull(this.builderConfig)) {
            $registry = this.builderConfig.dockerRegistry;
        }
        if (!ObjectValidator.IsEmptyOrNull($registry)) {
            authconfig = {
                email        : $registry.email,
                password     : $registry.pass,
                serveraddress: $registry.url,
                username     : $registry.user
            };
        }
        return authconfig;
    }
}

export class BuildOptions {
    public name : string;
    public useEnv? : boolean | any;
    public advanced? : any;
}

export class BindPortOptions {
    public containerPort : number;
    public hostPort : number;
    public protocol? : string;
    public hostIp? : string;
}

export class ContainerOptions {
    public name : string;
    public bindPorts? : BindPortOptions[];
    public cmd? : string[];
    public advanced? : any;
}

export class DockerOptions {
    public version : string[];
    public build : BuildOptions;
    public container : ContainerOptions;
}

export class DockerContext {
    public buildAllowed : boolean;
    public composeAllowed : boolean;
    public file : string;
    public config : DockerOptions;
}
