/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class InstallIdeaTools extends BaseTask {

    protected getName() : string {
        return "install-idea-tools";
    }

    protected async processAsync($option : string) : Promise<void> {
        const configBase : string = this.programArgs.BinBase() + "/resource/configs/ideaTools";
        let ideaBase : string = this.programArgs.getOptions().file;
        if (ObjectValidator.IsEmptyOrNull(ideaBase)) {
            LogIt.Error("Install of IDEA tools has failed: config base has not been specified by --file option");
        }
        if (!this.fileSystem.Exists(ideaBase)) {
            LogIt.Error("Install of IDEA tools has failed: path to config base has not been found");
        }
        ideaBase += (EnvironmentHelper.IsMac() ? "" : "/config") + "/tools";
        ideaBase = this.fileSystem.NormalizePath(ideaBase);
        const files : string[] = this.fileSystem.Expand(configBase + "/*.xml");
        for await (const file of files) {
            await this.fileSystem.CopyAsync(file, ideaBase + StringUtils.Remove(file, configBase));
        }
    }
}
