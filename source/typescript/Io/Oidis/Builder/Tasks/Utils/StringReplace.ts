/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { StringReplaceType } from "../../Enums/StringReplaceType.js";
import { EnvironmentArgs } from "../../EnvironmentArgs.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class StringReplace extends BaseTask {

    private static singleton : StringReplace;
    private static customProperties : any = {};
    protected varFormatter : ($value : string) => string = null;

    public static getInstance() : StringReplace {
        if (ObjectValidator.IsEmptyOrNull(StringReplace.singleton)) {
            StringReplace.singleton = new StringReplace();
        }

        return StringReplace.singleton;
    }

    public static getCustomProperties() : any {
        return this.customProperties;
    }

    public static File($path : string, $type : StringReplaceType, $dest? : string) : void {
        StringReplace.getInstance().file($path, $type, $dest);
    }

    public static Content($input : string, $type : StringReplaceType, $varFormatter? : ($value : string) => string) : string {
        return StringReplace.getInstance().content($input, $type, $varFormatter);
    }

    protected file($path : string, $type : StringReplaceType, $dest? : string) : void {
        if (ObjectValidator.IsEmptyOrNull($dest)) {
            $dest = $path;
        }
        if (this.fileSystem.IsFile($path)) {
            let content : string = this.fileSystem.Read($path).toString();
            content = StringReplace.Content(content, $type);
            this.fileSystem.Write($dest, content);
        }
    }

    protected content($input : string, $type : StringReplaceType, $varFormatter? : ($value : string) => string) : string {
        this.varFormatter = $varFormatter;
        let replacements : any[] = [];
        switch ($type) {
        case StringReplaceType.IMPORTS_PREPARE:
            replacements = [
                {
                    pattern    : /<\? @import (.*?) \?>/ig,
                    replacement: this.importsPrepareReplacement
                },
                {
                    pattern    : /<!-- @import (.*?) -->/ig,
                    replacement: this.importsPrepareReplacement
                },
                {
                    pattern    : /#?\/\* @import (.*?) \*\//ig,
                    replacement: this.importsPrepareReplacement
                },
                {
                    pattern    : /\/\*\*\s*<!-- @import (.*?) -->\s*\*\//ig,
                    replacement: this.importsPrepareReplacement
                }
            ];
            break;
        case StringReplaceType.IMPORTS_FINISH:
            replacements = [
                {
                    pattern    : /<\? @import (.*?) \?>/ig,
                    replacement: this.importsFinishReplacement
                },
                {
                    pattern    : /<!-- @import (.*?) -->/ig,
                    replacement: this.importsFinishReplacement
                },
                {
                    pattern    : /\/\* @import (.*?) \*\//ig,
                    replacement: this.importsFinishReplacement
                },
                {
                    pattern    : /\/\*\*\s*<!-- @import (.*?) -->\s*\*\//ig,
                    replacement: this.importsPrepareReplacement
                }
            ];
            break;
        case StringReplaceType.FOR_EACH:
            replacements = [
                {
                    pattern    : /<\? @forEach (.*?) in (.*?) \?>/ig,
                    replacement: this.forEachReplacement
                },
                {
                    pattern    : /<!-- @forEach (.*?) in (.*?) -->/ig,
                    replacement: this.forEachReplacement
                }
            ];
            break;
        case StringReplaceType.VARIABLES:
            replacements = [
                {
                    pattern    : /<\? @var (.*?) \?>/ig,
                    replacement: this.varReplacement
                },
                {
                    pattern    : /<!-- @var (.*?) -->/ig,
                    replacement: this.varReplacement
                }
            ];
            break;
        case StringReplaceType.VARIABLES_PLUGIN:
            replacements = [
                {
                    pattern    : /<\? @var (.*?) \?>/ig,
                    replacement: this.varReplacement
                },
                {
                    pattern    : /<!-- @var (.*?) -->/ig,
                    replacement: this.varReplacement
                }
            ];
            break;
        case StringReplaceType.DEPENDENCIES:
            replacements = [
                {
                    pattern    : /..\/dependencies\//ig,
                    replacement: "../../"
                },
                {
                    pattern    : /..\/bin\//ig,
                    replacement: "../../../bin/"
                }
            ];
            break;
        case StringReplaceType.XCPP:
            replacements = [
                {
                    pattern    : /\/\*\*.*\r?\n( \* WARNING:.*)(.*\r?\n)+(.*\*\/?\r?\n\r?\n)/,
                    replacement: ""
                },
                {
                    pattern    : /<\? @var (.*?) \?>/ig,
                    replacement: this.varReplacement
                },
                {
                    pattern    : /"reference\.hpp"/ig,
                    replacement: "\"../../source/cpp/reference.hpp\""
                },
                {
                    pattern    : /\r?\n\/\/\s*NOLINT\s*\(legal\/copyright\)\s*\r?\n/ig,
                    replacement: ""
                }
            ];
            break;

        default:
            LogIt.Warning("Unsupported StringReplace type: " + $type);
            break;
        }
        if (ObjectValidator.IsEmptyOrNull($input)) {
            $input = "";
        }
        replacements.forEach(($replacement : any) : void => {
            $input = $input.replace($replacement.pattern, ($match : string, $what : string, $where : string) : string => {
                if (ObjectValidator.IsFunction($replacement.replacement)) {
                    return $replacement.replacement.apply(this, [$match, $what, $where]);
                }
                return $replacement.replacement;
            });
        }, this);
        this.varFormatter = null;
        return $input;
    }

    protected getName() : string {
        return "string-replace";
    }

    protected getTemplates() : any {
        const os : any = require("os");
        return {
            cmake   : {
                commonDependency: "" +
                    "set(<? @var this.name ?>_CONFIG_SCRIPT_PATH <? @var this.configureScript.path ?>/" +
                    "<? @var this.configureScript.name ?>)" + os.EOL +
                    "if (NOT IS_ABSOLUTE ${<? @var this.name ?>_CONFIG_SCRIPT_PATH})" + os.EOL +
                    "    set(<? @var this.name ?>_CONFIG_SCRIPT_PATH ${CMAKE_SOURCE_DIR}/${<? @var this.name ?>_CONFIG_SCRIPT_PATH})" +
                    os.EOL +
                    "endif ()" + os.EOL +
                    "include(${<? @var this.name ?>_CONFIG_SCRIPT_PATH})" + os.EOL +
                    "<? @var this.configureScript.macro-name ?>(<? @var project.target.name ?> internal-unit-test-runner " +
                    "${TEST_TARGET_NAME}" +
                    os.EOL +
                    "        \"<? @var this.location.path ?>\"" + os.EOL +
                    "        \"<? @var this.version ?>\"" + os.EOL +
                    "        \"<? @var this.configureScript.attributes ?>\")" + os.EOL,

                wuiDependencyEntry: "" +
                    "set(DEPENDENCIES_LOOK_FOLDERS)",

                wuiDependency: "" +
                    "list(APPEND DEPENDENCIES_LOOK_FOLDERS <? @var this.name ?>)" + os.EOL,

                wuiDependencyCore: "" +
                    "set(DEPENDENCIES_FILES \"\")" + os.EOL +
                    "set(DEPENDENCIES_HEADERS \"\")" + os.EOL +
                    "foreach (item ${DEPENDENCIES_LOOK_FOLDERS})" + os.EOL +
                    "    FILE(GLOB_RECURSE _FILES ${CMAKE_SOURCE_DIR}/dependencies/${item}/source/cpp/*.cpp)" + os.EOL +
                    "    FILE(GLOB_RECURSE _HEADERS ${CMAKE_SOURCE_DIR}/dependencies/${item}/source/cpp/reference.hpp)" + os.EOL +
                    "    set(DEPENDENCIES_FILES ${DEPENDENCIES_FILES} ${_FILES})" + os.EOL +
                    "    set(DEPENDENCIES_HEADERS ${DEPENDENCIES_HEADERS} ${_HEADERS})" + os.EOL +
                    "endforeach ()" + os.EOL +
                    "list(LENGTH DEPENDENCIES_FILES DEPENDENCIES_COUNT)" + os.EOL +
                    "if (NOT DEPENDENCIES_COUNT EQUAL 0)" + os.EOL +
                    "    add_library(${PROJECT_NAME}_wui_dependencies.lib ${DEPENDENCIES_HEADERS} ${DEPENDENCIES_FILES})" + os.EOL +
                    "    target_include_directories(${PROJECT_NAME}_wui_dependencies.lib PUBLIC ${COMMON_DEP_CPP_INCLUDES})" + os.EOL +
                    "    target_link_libraries(${PROJECT_NAME}_wui_dependencies.lib ${COMMON_DEP_CPP_LIBS})" + os.EOL +
                    "    target_link_libraries(${PROJECT_TARGET_NAME} ${PROJECT_NAME}_wui_dependencies.lib)" + os.EOL +
                    "    target_link_libraries(internal-unit-test-runner ${PROJECT_NAME}_wui_dependencies.lib)" + os.EOL +
                    "endif ()" + os.EOL
            },
            maven   : {
                artifact  : os.EOL +
                    "        <dependency>" + os.EOL +
                    "            <groupId><? @var this.groupId ?></groupId>" + os.EOL +
                    "            <artifactId><? @var this.artifactId ?></artifactId>" + os.EOL +
                    "            <version><? @var this.version ?></version>" + os.EOL +
                    "        </dependency>",
                repository: os.EOL +
                    "        <repository>" + os.EOL +
                    "            <id><? @var this.id ?></id>" + os.EOL +
                    "            <url><? @var this.url ?></url>" + os.EOL +
                    "        </repository>",
                resource  : os.EOL +
                    "            <resource>" + os.EOL +
                    "                <directory><? @var this.value ?></directory>" + os.EOL +
                    "                <targetPath>${project.build.outputDirectory}/resource</targetPath>" + os.EOL +
                    "                <filtering>true</filtering>" + os.EOL +
                    "                <excludes>" + os.EOL +
                    "                    <exclude>*.jar</exclude>" + os.EOL +
                    "                    <exclude>**/*.jar</exclude>" + os.EOL +
                    "                </excludes>" + os.EOL +
                    "            </resource>",
                source    : os.EOL +
                    "                            <source>${project.basedir}/dependencies/<? @var this.name ?>/source/java/</source>"
            },
            phonegap: {
                platform: "    <platform name=\"<? @var this.value ?>\" />"
            }
        };
    }

    protected getProperty($name : string, $from? : any) : any {
        if (ObjectValidator.IsEmptyOrNull($from)) {
            const env : EnvironmentArgs = Loader.getInstance().getEnvironmentArgs();
            $from = {
                build       : env.getBuildArgs(),
                clientConfig: env.getAppProperties().clientConfig,
                custom      : StringReplace.customProperties,
                project     : env.getTargetConfig(),
                properties  : env.getAppProperties(),
                serverConfig: env.getAppProperties().serverConfig,
                templates   : this.getTemplates(),
                testConfig  : env.getTestConfig()
            };
        }
        const split : string[] = $name.split(".");
        let fromCopy : any = JsonUtils.Clone($from);
        let index : number;
        let found : boolean = false;
        for (index = 0; index < split.length; index++) {
            if (fromCopy.hasOwnProperty(split[index])) {
                fromCopy = fromCopy[split[index]];
                found = true;
            } else {
                found = false;
            }
        }
        if (found) {
            return fromCopy;
        }
        return undefined;
    }

    protected forEachReplacement($match : string, $what : string, $where : string) : string {
        const os : any = require("os");
        try {
            const whatValue : any = this.getProperty($what);
            const whereValue : any = this.getProperty($where);
            let output : string = "";
            const replaceSingle : any = ($target : any) : void => {
                if (output !== "") {
                    output += os.EOL;
                }
                if (typeof $target === "string") {
                    output += whatValue.replace(/<\? @var this\.value \?>/gi, $target);
                } else {
                    output += whatValue.replace(/<\? @var this\.(.*?) \?>/gi, ($match : string, $value : string) : string => {
                        try {
                            const replacement : string = this.getProperty($value, $target);
                            if (ObjectValidator.IsSet(replacement)) {
                                return replacement;
                            }
                        } catch (ex) {
                            LogIt.Debug(ex);
                        }
                        LogIt.Warning(
                            "string-replace: property \"" + $value + "\" was not found, so replace was skipped.");
                        return "<? @var this." + $value + " ?>";
                    });
                }
            };
            if (ObjectValidator.IsArray(whereValue)) {
                for (const item of whereValue) {
                    replaceSingle(item);
                }
            } else {
                for (const item in whereValue) {
                    if (whereValue.hasOwnProperty(item)) {
                        replaceSingle(whereValue[item]);
                    }
                }
            }

            return output;
        } catch (ex) {
            LogIt.Warning(ex);
        }
        LogIt.Warning("string-replace: forEach replacement for \"" + $where + "\" has failed, so replace was skipped.");
        return $match;
    }

    protected importsFinishReplacement($match : string, $path : string) : string {
        let content : string = this.fileSystem.Read($path).toString();
        content = StringReplace.Content(content, StringReplaceType.FOR_EACH);
        content = StringReplace.Content(content, StringReplaceType.VARIABLES);
        return content;
    }

    protected importsPrepareReplacement($match : string, $value : string) : string {
        let path : string = $value;
        if ($value.toString().indexOf(".html") !== -1) {
            path = "source/html/" + $value;
        } else if ($value.toString().indexOf(".js") !== -1) {
            path = "resource/javascript/" + $value;
        }
        const index : number = $value.toString().indexOf("dependencies");
        const isDependency : boolean = index !== -1;
        if (isDependency) {
            path = $value.substr(index, $value.length);
        }
        if (this.fileSystem.Exists(path)) {
            const dest : string = this.properties.projectBase + "/build/compiled/" + path;
            if (isDependency) {
                this.fileSystem.Write(dest, this.fileSystem.Read(path));
            }
            return "<!-- @import " + dest + " -->";
        } else {
            return "<!-- @import " + $value + " -->";
        }
    }

    protected varReplacement($match : string, $value : string) : string {
        try {
            let replacement : string = this.getProperty($value);
            if (ObjectValidator.IsSet(replacement)) {
                if (!ObjectValidator.IsEmptyOrNull(this.varFormatter)) {
                    replacement = this.varFormatter(replacement);
                }
                if (ObjectValidator.IsObject(replacement)) {
                    replacement = JSON.stringify(JSON.stringify(replacement));
                    replacement = replacement.substring(1, replacement.length - 1);
                }
                return replacement;
            }
        } catch (ex) {
            // ignore unresolved replacement
        }
        LogIt.Warning("string-replace: property \"" + $value + "\" was not found, so replace was skipped.");
        return $match;
    }

    protected async processAsync($option : string) : Promise<void> {
        switch ($option) {
        case "imports-prepare":
            this.processAll([
                this.properties.sources + "/html/**/*.html",
                this.properties.dependenciesResources + "/javascript/**/*.js",
                this.properties.resources + "/javascript/**/*.js",
                this.properties.resources + "/configs/wuirunner.config.jsonp",
                this.properties.sources + "/cpp/ApplicationMain.cpp",
                this.properties.sources + "/python/**/*.py",
                this.properties.dependencies + "/python/**/*.py"
            ], this.properties.compiled, StringReplaceType.IMPORTS_PREPARE);
            break;
        case "imports-finish":
            this.processAll([
                this.properties.compiled + "/**/html/**/*.html",
                this.properties.compiled + "/**/javascript/**/*.js",
                this.properties.compiled + "/**/wuirunner.config.jsonp",
                this.properties.tmp + "/cpp/ApplicationMain.cpp",
                this.properties.compiled + "/**/python/**/*.py"
            ], this.properties.compiled, StringReplaceType.IMPORTS_FINISH);
            break;
        case "variables":
            this.processAll([
                this.properties.compiled + "/**/html/**/*.html",
                this.properties.compiled + "/**/javascript/**/*.js",
                this.properties.compiled + "/**/wuirunner.config.jsonp",
                this.properties.compiled + "/**/python/**/*.py"
            ], this.properties.compiled, StringReplaceType.VARIABLES);
            break;
        case "variables-plugin":
            this.processAll([
                this.properties.dest + "/**/plugin.xml",
                this.properties.dest + "/**/**/plugin.xml"
            ], this.properties.dest, StringReplaceType.VARIABLES_PLUGIN);
            break;
        case "dependencies":
            this.processAll([
                "dependencies/*/source/typescript/**/*.d.ts",
                "dependencies/*/test/**/*.d.ts",
                "dependencies/*/resource/sass/**/*.scss"
            ], "dependencies", StringReplaceType.DEPENDENCIES);
            this.fileSystem.Expand("dependencies/*/bin/project/**/*.jsonp").forEach(($file : string) : void => {
                let root : string = StringUtils.Substring($file, 0, StringUtils.IndexOf($file, "/bin/"));
                root = StringUtils.Remove(root, this.properties.projectBase + "/");
                const content : string = this.fileSystem.Read($file).toString();
                if (StringUtils.Contains(content, "\"bin/project/")) {
                    this.fileSystem.Write($file,
                        StringUtils.Replace(content, "\"bin/project/", "\"" + root + "/bin/project/"));
                }
            });
            break;
        case "xcpp":
            this.processAll([
                this.properties.tmp + "/cpp/ApplicationMain.cpp"
            ], this.properties.tmp + "/cpp", "xcpp");
            this.processAll([
                "bin/resource/scripts/UnitTestLoader.cpp",
                "bin/resource/scripts/UnitTestRunner.cpp"
            ], "bin/resource/scripts/", StringReplaceType.XCPP);
            break;

        default:
            LogIt.Warning("Unsupported string-replace option: " + $option);
            break;
        }
    }

    private processAll($sources : string[], $dest : string, $type : string) : void {
        for (let index : number = 0; index < $sources.length; index++) {
            $sources[index] = this.properties.projectBase + "/" + $sources[index];
        }
        $dest = this.properties.projectBase + "/" + $dest;
        this.fileSystem.Expand($sources).forEach(($file : string) : void => {
            let destination : string = $file;
            destination = StringUtils.Remove(destination, $dest);
            destination = StringUtils.Remove(destination, this.properties.projectBase + "/");
            StringReplace.File($file, $type, $dest + "/" + destination);
        });
    }
}
