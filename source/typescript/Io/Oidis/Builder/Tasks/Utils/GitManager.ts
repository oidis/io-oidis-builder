/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class GitManager extends BaseTask {

    public static Execute($args : string[], $callback : ($stdout : string) => void, $onError? : () => void,
                          $repoPath? : string, $verbose : boolean = true) : void {
        const path : any = require("path");
        if (ObjectValidator.IsEmptyOrNull($repoPath)) {
            $repoPath = Loader.getInstance().getAppProperties().projectBase;
        }
        const gitBin : string = EnvironmentHelper.IsWindows() ? "/cmd" : "/bin";
        const PATH : string = StringUtils.Replace(Loader.getInstance().getProgramArgs().AppDataPath() + "/external_modules" +
            "/git" + gitBin, "/", path.sep);
        let env : any = JsonUtils.Clone(process.env);
        if (Loader.getInstance().getFileSystemHandler().Exists(PATH)) {
            // TODO(mkelnar) if there will be another one problem with git/git-lfs on Win then it needs to be solved properly in
            //  git setup or do proper cleanup of path (remove git/builder stuff) but system defaults (like Windows, system32)
            //  directories must persist in search path!!
            //  It is highly recommended to do not use customized git any more and use standard installation -> delete git from
            //  external modules will bypass this risky setup below.
            if (EnvironmentHelper.IsWindows()) {
                env = {};  // this causes also situation the git didn't see TEMP, WINroot; HTTP(S) proxy setup is removed too!!
                env.PATH = PATH;
            } else {
                env.PATH = PATH + ":" + env.PATH;
            }
        }
        Loader.getInstance().getTerminal().Spawn("git", $args, {
                cwd    : $repoPath,
                env,
                verbose: $verbose
            },
            ($exitCode : number, $std : string[]) : void => {
                if ($exitCode === 0) {
                    $callback($std[0]);
                } else if (ObjectValidator.IsSet($onError)) {
                    $onError();
                } else {
                    LogIt.Error("GIT execution has failed");
                }
            });
    }

    public static async ExecuteAsync($args : string[], $options? : IGitExecuteOptions) : Promise<IGitExecuteResult> {
        $options = JsonUtils.Extend({
            repoPath  : null,
            throwError: true,
            verbose   : true
        }, $options);
        return new Promise<IGitExecuteResult>(($resolve, $reject) : void => {
            GitManager.Execute($args, ($stdout : string) : void => {
                $resolve({status: true, stdout: $stdout});
            }, () : void => {
                if (!$options.throwError) {
                    $resolve({status: false, stdout: ""});
                } else {
                    $reject(new Error("GIT execution has failed"));
                }
            }, $options.repoPath, $options.verbose);
        });
    }

    public static async getGitValue($args : string[], $name : string) : Promise<string> {
        const res : IGitExecuteResult = await GitManager.ExecuteAsync($args, {
            repoPath  : Loader.getInstance().getAppProperties().projectBase,
            throwError: false,
            verbose   : false
        });
        if (!res.status) {
            LogIt.Warning("Failed to get value for: " + $name);
            return null;
        }
        return StringUtils.Remove(res.stdout, "\r\n", "\n");
    }

    public static StashRestore() : void {
        if (!this.IsGitRepo()) {
            LogIt.Info(">>"[ColorType.YELLOW] + " Not a git repository: skipping git stash");
            return;
        }
        const path : any = require("path");
        const backupFilePath : any =
            Loader.getInstance().getAppProperties().projectBase.replace(/\\/g, "/") + "/build_cache/unstaged.diff";
        const spawnSync : any = require("child_process").spawnSync;
        if (!Loader.getInstance().getFileSystemHandler().IsEmpty(backupFilePath)) {
            const retVal : any =
                spawnSync("git apply --whitespace=fix \"" + backupFilePath.replace(/[/\\]/g, path.sep) + "\"", {
                    cwd        : Loader.getInstance().getAppProperties().projectBase,
                    env        : process.env,
                    shell      : true,
                    windowsHide: true
                });

            if (retVal.status === 0) {
                LogIt.Info("Git stash rollback done.");
                if (Loader.getInstance().getFileSystemHandler().Delete(backupFilePath)) {
                    LogIt.Info(">>"[ColorType.YELLOW] + " Git backup file cleaned.");
                } else {
                    LogIt.Warning("Git backup file cleanup failed.");
                }
            } else {
                LogIt.Debug(retVal.stdout.toString() + retVal.stderr.toString());
                LogIt.Warning("Git stash rollback failed. " +
                    "Use 'git apply \"" + backupFilePath + "\"' to rollback manually.");
            }
        } else {
            LogIt.Info("Git stash rollback not necessary.");
        }
    }

    public static async UpdateTracked() : Promise<void> {
        const res : any = await GitManager.ExecuteAsync(["ls-files", "--ignored", "--cached", "--exclude-standard"]);
        const removeList : string[] = [];
        StringUtils.Split(StringUtils.Replace(res.stdout, "\r", ""), "\n")
            .forEach(($record : string) : void => {
                if (!ObjectValidator.IsEmptyOrNull($record)) {
                    removeList.push($record);
                }
            });
        if (removeList.length > 0) {
            await GitManager.ExecuteAsync(["rm", "--cached"].concat(removeList));
        }
    }

    public static IsGitRepo() : boolean {
        const spawnSync : any = require("child_process").spawnSync;
        const retVal : any =
            spawnSync("git rev-parse --git-dir", {
                cwd        : Loader.getInstance().getAppProperties().projectBase,
                env        : process.env,
                shell      : true,
                windowsHide: true
            });

        const output : boolean = retVal.status === 0 &&
            !(retVal.stdout.toString() + retVal.stderr.toString()).includes("not a git repository");
        GitManager.IsGitRepo = () : boolean => {
            return output;
        };
        return output;
    }

    protected getName() : string {
        return "git-manager";
    }

    protected async processAsync($option : string) : Promise<void> {
        if (!GitManager.IsGitRepo()) {
            LogIt.Info(">>"[ColorType.YELLOW] + " Not a git repository: skipping git-manager task");
            return;
        }
        const path : any = require("path");
        const backupFilePath : any = this.properties.projectBase.replace(/\\/g, "/") + "/build_cache/unstaged.diff";

        switch ($option) {
        case "backup":
            this.fileSystem.CreateDirectory(path.dirname(backupFilePath));
            const res : IExecuteResult = await this.terminal // eslint-disable-line no-case-declarations
                .ExecuteAsync("git diff > \"" + backupFilePath + "\"", [], this.properties.projectBase);
            if (res.exitCode === 0) {
                if (this.fileSystem.Read(backupFilePath).length > 0) {
                    const res : any = await GitManager.ExecuteAsync(["apply", "-R", "--whitespace=fix", backupFilePath],
                        {throwError: false});
                    if (res.status) {
                        LogIt.Info(">>"[ColorType.YELLOW] + " Unstaged files have been moved to backup.");
                    } else {
                        LogIt.Warning("Can not remove unstaged files.");
                    }
                } else {
                    LogIt.Info(">>"[ColorType.YELLOW] + " GIT unstage skipped: nothing to unstage");
                }
            } else {
                LogIt.Warning("Can not process git diff: " + res.std[1]);
            }
            break;
        case "restore":
            GitManager.StashRestore();
            break;
        case "push":
            let name : string = "origin"; // eslint-disable-line no-case-declarations
            if (!ObjectValidator.IsEmptyOrNull(this.programArgs.getOptions().to)) {
                const remote : string[] = StringUtils.Split(this.programArgs.getOptions().to, ";");
                if (remote.length === 2) {
                    name = remote[0];
                    const source : string = remote[1];
                    if (!StringUtils.Contains((await GitManager.ExecuteAsync(["remote", "-v"])).stdout, name + source)) {
                        await GitManager.ExecuteAsync(["remote", "add", name, source]);
                    }
                } else {
                    LogIt.Error("Argument in incorrect format: Expected is --to=\"<remote_name>;<remote_url>\"");
                }
            }
            await GitManager.ExecuteAsync(["push", name]);
            await GitManager.ExecuteAsync(["push", name, "--tags"]);
            break;
        case "porcelaincheck":
            await GitManager.ExecuteAsync(["update-index", "-q", "--ignore-submodules", "--refresh"]);
            try {
                await GitManager.ExecuteAsync(["diff-files", "--quiet", "--ignore-submodules"]);
                await GitManager.ExecuteAsync(["diff-index", "--cached", "--quiet", "HEAD", "--ignore-submodules"]);
            } catch (ex) {
                LogIt.Error("Uncommitted changes has been found. Commit or stash them.");
            }
            break;
        case "hooks":
            const hookPath : string = this.properties.projectBase + "/.git/hooks/pre-commit"; // eslint-disable-line no-case-declarations
            if (this.programArgs.IsForce()) {
                this.fileSystem.Delete(hookPath);
            }
            if (!this.fileSystem.Exists(hookPath)) {
                await this.fileSystem.CopyAsync(this.properties.binBase + "/resource/scripts/pre-commit", hookPath);
            } else {
                LogIt.Info(">>"[ColorType.YELLOW] + " GIT hooks install skipped: hooks already installed. " +
                    "Use option --force for reinstall.");
            }
            break;
        default:
            LogIt.Error("Unknown option \"" + $option + "\" for git-manager.");
            break;
        }
    }
}

export interface IGitExecuteOptions {
    repoPath? : string;
    throwError? : boolean;
    verbose? : boolean;
}

export interface IGitExecuteResult {
    status : boolean;
    stdout : string;
}

// generated-code-start
export const IGitExecuteOptions = globalThis.RegisterInterface(["repoPath", "throwError", "verbose"]);
export const IGitExecuteResult = globalThis.RegisterInterface(["status", "stdout"]);
// generated-code-end
