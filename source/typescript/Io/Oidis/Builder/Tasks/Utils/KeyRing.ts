/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IStdinQuestion, StdinManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/StdinManager.js";
import { CredentialsManager } from "../../Connectors/CredentialsManager.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class KeyRing extends BaseTask {

    protected getName() : string {
        return "key-ring";
    }

    protected async processAsync($option : string) : Promise<void> {
        await StdinManager.ProcessAsync([
            <IStdinQuestion>{
                callback($password : string, $callback : ($status : boolean) => void) : void {
                    CredentialsManager.setLinuxRoot($password).then($callback);
                },
                hidden: true,
                prefix: "Linux root password"
            },
            <IStdinQuestion>{
                callback($password : string, $callback : ($status : boolean) => void) : void {
                    CredentialsManager.setPhonegapToken($password).then($callback);
                },
                hidden: true,
                prefix: "Phonegap token"
            }
        ]);
    }
}
