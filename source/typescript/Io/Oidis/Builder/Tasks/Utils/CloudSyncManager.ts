/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { Echo } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Echo.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { WebServiceClientType } from "@io-oidis-services/Io/Oidis/Services/Enums/WebServiceClientType.js";
import { CloudSyncHandler } from "../../Connectors/CloudSyncHandler.js";
import { CloudSyncHandlerConnector } from "../../Connectors/CloudSyncHandlerConnector.js";
import { IAppConfiguration } from "../../Interfaces/IAppConfiguration.js";
import { ICloudChanges, ICloudFilterType, ICloudSyncMap } from "../../Interfaces/ICloudItem.js";
import { Loader } from "../../Loader.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class CloudSyncManager extends BaseTask {
    private static isCppProfile : any;

    protected getName() : string {
        return "cloud-sync-manager";
    }

    protected process($done : any, $option : string) : void {
        const appConfig : IAppConfiguration = Loader.getInstance().getAppConfiguration();
        const hubUrl : string = appConfig.hub.url;

        // todo: do it for non forwarded also?
        const remoteCloud : CloudSyncHandlerConnector = new CloudSyncHandlerConnector(true,
            hubUrl + "/connector.config.jsonp", WebServiceClientType.FORWARD_CLIENT);
        remoteCloud.setAgent(appConfig.agent.name, hubUrl);
        remoteCloud.ErrorPropagation(false);
        remoteCloud.getEvents().OnError(() : void => {
            $done();
        });

        const projectBase : string = Loader.getInstance().getAppProperties().projectBase;
        const excludes : string[] = ["/**/" + Loader.getInstance().getAppProperties().packageName + ".+(zip|tar.gz)"];
        let projectName : string = require("path").basename(projectBase);
        if (!ObjectValidator.IsEmptyOrNull(this.programArgs.getOptions().project)) {
            projectName = this.programArgs.getOptions().project;
        }
        const localCloud : CloudSyncHandler = new CloudSyncHandler(projectBase);
        CloudSyncManager.isCppProfile = this.properties.projectHas.Cpp.Source();

        const ok : any = ($beforeStamp : number, $lineBreak : boolean = true) : void => {
            const stamp : number = Math.ceil(new Date().getTime() - $beforeStamp);
            if ($lineBreak) {
                LogIt.Debug("> OK (" + stamp + " ms)");
            } else {
                LogIt.Debug(" OK (" + stamp + " ms)");
            }
        };

        const complete : any = () : void => {
            LogIt.Info("> Synchronization complete. (" + Math.ceil(new Date().getTime() - beforeTask) + "ms)");
            $done();
        };

        const error : any = ($message : string = "") : void => {
            LogIt.Error("> Synchronization incomplete. " + $message);
            $done();
        };

        const pushFiles : any = ($items : string[], $localSyncMap : ICloudSyncMap, $remoteSyncMap : ICloudSyncMap,
                                 $done : any) : void => {
            if ($items.length > 0) {
                LogIt.Info("> Pushing files:");
                pushFile(0, $items, $localSyncMap, $remoteSyncMap, $done);
            } else {
                $done();
            }
        };

        const pushFile : any = ($index : number, $items : string[], $localSyncMap : ICloudSyncMap, $remoteSyncMap : ICloudSyncMap,
                                $done : any) : void => {
            if ($index < $items.length) {
                const path : string = $items[$index];
                Echo.Print("  " + path + " (" + Convert.IntegerToSize($localSyncMap.items[path].size) + ")");

                let printDots : boolean = false;
                const before : number = new Date().getTime();
                remoteCloud.UploadFile({
                    metadata: {
                        changed   : $localSyncMap.items[path].changed,
                        isCreate  : ObjectValidator.IsEmptyOrNull($remoteSyncMap.items[path]),
                        syncFolder: projectName
                    },
                    path
                }, this.fileSystem.NormalizePath(localCloud.getSyncPath(projectName) + "/" + path))
                    .OnMessage(() : void => {
                        Echo.Print(printDots ? "." : " ");
                        printDots = true;
                    })
                    .OnError(($data : string) : void => {
                        LogIt.Error($data);
                    })
                    .Then(() : void => {
                        ok(before, false);
                        pushFile($index + 1, $items, $localSyncMap, $remoteSyncMap, $done);
                    });
            } else {
                $done();
            }
        };

        const pushFolders : any = ($items : string[], $changeTimeMap : number[], $done : any) : void => {
            if ($items.length > 0) {
                LogIt.Info("> Pushing folders:\n  " + $items.join("\n  "));
                const before : number = new Date().getTime();
                remoteCloud.CreateFolders(projectName, localCloud.getChangeArgs($items, $changeTimeMap))
                    .Then(($status : boolean) : void => {
                        if ($status) {
                            ok(before);
                            $done();
                        } else {
                            error("Push of folders failed.");
                        }
                    });
            } else {
                $done();
            }
        };

        const deleteRemote : any = ($items : string[], $changeTimeMap : number[], $done : any) : void => {
            if ($items.length > 0) {
                LogIt.Info("> Deleting remote items:\n  " + $items.join("\n  "));
                const before : number = new Date().getTime();
                remoteCloud.DeleteItems(projectName,
                    localCloud.getChangeArgs($items, $changeTimeMap))
                    .Then(($status : boolean) : void => {
                        if ($status) {
                            ok(before);
                            $done();
                        } else {
                            error("Remote item deletion failed.");
                        }
                    });
            } else {
                $done();
            }
        };

        const pullFile : any = ($index : number, $items : string[], $remoteSyncMap : ICloudSyncMap, $localSyncMap : ICloudSyncMap,
                                $done : any) : void => {
            if ($index < $items.length) {
                const path : string = $items[$index];
                let printDots : boolean = false;
                Echo.Print("  " + path + " (" + Convert.IntegerToSize($remoteSyncMap.items[path].size) + ")");
                const before : number = new Date().getTime();
                remoteCloud.DownloadFile({
                    metadata: {
                        syncFolder: projectName
                    },
                    path
                }, localCloud.getSyncPath(projectName) + path)
                    .OnMessage(() : void => {
                        Echo.Print(printDots ? "." : " ");
                        printDots = true;
                    })
                    .OnError(($data : string) : void => {
                        LogIt.Error($data);
                    })
                    .Then(() : void => {
                        localCloud.UpdateFileSyncMetadata(
                            localCloud.getSyncPath(projectName),
                            path,
                            ObjectValidator.IsEmptyOrNull($localSyncMap.items[path]),
                            $remoteSyncMap.items[path].changed,
                            ($success : boolean) : void => {
                                if ($success) {
                                    ok(before, false);
                                    pullFile($index + 1, $items, $remoteSyncMap, $localSyncMap, $done);
                                } else {
                                    error("Update of synchronization map failed.");
                                }
                            });
                    });
            } else {
                $done();
            }
        };

        const pullFiles : any = ($items : string[], $remoteSyncMap : ICloudSyncMap, $localSyncMap : ICloudSyncMap,
                                 $done : any) : void => {
            if ($items.length > 0) {
                LogIt.Info("> Pulling files:");
                pullFile(0, $items, $remoteSyncMap, $localSyncMap, $done);
            } else {
                $done();
            }
        };

        const pullFolders : any = ($items : string[], $changeTimeMap : number[], $done : any) : void => {
            if ($items.length > 0) {
                LogIt.Info("> Pulling folders:\n  " + $items.join("\n  "));
                const before : number = new Date().getTime();
                if (localCloud.CreateFolders(projectName,
                    localCloud.getChangeArgs($items, $changeTimeMap))) {
                    ok(before);
                    $done();
                } else {
                    error("Pull of folders failed.");
                }
            } else {
                $done();
            }
        };

        const deleteLocal : any = ($items : string[], $changeTimeMap : number[], $done : any) : void => {
            if ($items.length > 0) {
                LogIt.Info("> Deleting local items:\n  " + $items.join("\n  "));
                const before : number = new Date().getTime();
                if (localCloud.DeleteItems(projectName,
                    localCloud.getChangeArgs($items, $changeTimeMap))) {
                    ok(before);
                    $done();
                } else {
                    error("Local item deletion failed.");
                }
            } else {
                $done();
            }
        };

        const beforeTask : number = new Date().getTime();
        if ($option === "push") {
            LogIt.Info("> Initiating push synchronization.");
            remoteCloud.getSyncMap({
                filters   : [ICloudFilterType.PUSH, {excludes}],
                syncFolder: projectName
            }).Then(($remoteSyncMap : ICloudSyncMap) : void => {
                localCloud.getSyncMap({
                    filters   : [ICloudFilterType.PUSH, {excludes}],
                    syncFolder: projectName
                }, ($localSyncMap : ICloudSyncMap) : void => {
                    const changes : ICloudChanges = localCloud.ReduceChanges(localCloud.getChanges($localSyncMap, $remoteSyncMap));
                    if (changes.pushFiles.length !== 0 ||
                        changes.deleteItems.length !== 0 ||
                        changes.pushFolders.length !== 0) {
                        LogIt.Info("> Changes detected. Synchronizing remote repository.");
                        const touchedItems : string[] = [].concat(
                            changes.pushFiles,
                            changes.pushFolders,
                            changes.deleteItems);
                        const changeTimes : number[] = localCloud.getGlobalChangeTimeMap($localSyncMap, touchedItems);
                        deleteRemote(changes.deleteItems, changeTimes, () : void => {
                            pushFolders(changes.pushFolders, changeTimes, () : void => {
                                pushFiles(changes.pushFiles, $localSyncMap, $remoteSyncMap, () : void => {
                                    complete();
                                });
                            });
                        });
                    } else {
                        LogIt.Info("> No changes detected.");
                        complete();
                    }
                });
            });
        } else if ($option === "pull") {
            LogIt.Info("> Initiating pull synchronization.");
            remoteCloud.getSyncMap({
                filters   : [ICloudFilterType.PULL, {excludes}],
                syncFolder: projectName
            }).Then(($remoteSyncMap : ICloudSyncMap) : void => {
                localCloud.getSyncMap({
                    filters   : [ICloudFilterType.PULL, {excludes}],
                    syncFolder: projectName
                }, ($localSyncMap : ICloudSyncMap) : void => {
                    const changes : ICloudChanges = localCloud.ReduceChanges(localCloud.getChanges($remoteSyncMap, $localSyncMap));
                    if (changes.pushFiles.length !== 0 ||
                        changes.deleteItems.length !== 0 ||
                        changes.pushFolders.length !== 0) {
                        LogIt.Info("> Changes detected. Synchronizing local repository.");
                        const touchedItems : string[] = [].concat(
                            changes.pushFiles,
                            changes.pushFolders,
                            changes.deleteItems);
                        const changeTimes : number[] = localCloud.getGlobalChangeTimeMap($remoteSyncMap, touchedItems);
                        deleteLocal(changes.deleteItems, changeTimes, () : void => {
                            pullFolders(changes.pushFolders, changeTimes, () : void => {
                                pullFiles(changes.pushFiles, $remoteSyncMap, $localSyncMap, () : void => {
                                    complete();
                                });
                            });
                        });
                    } else {
                        LogIt.Info("> No changes detected.");
                        complete();
                    }
                });
            });
        } else if ($option === "check") {
            LogIt.Info("> Checking remote project status.");
            remoteCloud.getSyncMap({
                filters   : [ICloudFilterType.PUSH, {excludes}],
                syncFolder: projectName
            }).Then(($remoteSyncMap : ICloudSyncMap) : void => {
                localCloud.getSyncMap({
                    filters   : [ICloudFilterType.PUSH, {excludes}],
                    syncFolder: projectName
                }, ($localSyncMap : ICloudSyncMap) : void => {
                    const changes : ICloudChanges = localCloud.ReduceChanges(localCloud.getChanges($localSyncMap, $remoteSyncMap));
                    if (changes.pushFiles.length !== 0 ||
                        changes.deleteItems.length !== 0 ||
                        changes.pushFolders.length !== 0) {
                        // todo: make it an error when all files that change during build are excluded
                        LogIt.Warning("Remote project source files are not in synchronization with local source files.\n" +
                            "Please validate that build or synchronization process was completed successfully.\n" +
                            "required actions:\n" +
                            JSON.stringify(changes, null, "    "));
                        $done();
                    }
                });
            });
        } else {
            LogIt.Info("> Initiating project synchronization.");
            remoteCloud.getSyncMap({
                filters   : [ICloudFilterType.PROJECT],
                syncFolder: projectName
            }).Then(($remoteSyncMap : ICloudSyncMap) : void => {
                localCloud.getSyncMap({
                    filters   : [ICloudFilterType.PROJECT],
                    syncFolder: projectName
                }, ($localSyncMap : ICloudSyncMap) : void => {
                    const modificationsRemote : ICloudChanges = localCloud.getChanges($localSyncMap, $remoteSyncMap);
                    const touchedItems : string[] = [].concat(
                        modificationsRemote.pushFiles,
                        modificationsRemote.pushFolders,
                        modificationsRemote.deleteItems);
                    const localChangeTimes : number[] = localCloud.getGlobalChangeTimeMap($localSyncMap, touchedItems);
                    const remoteChangeTimes : number[] = localCloud.getGlobalChangeTimeMap($remoteSyncMap, touchedItems);
                    let localChanges : ICloudChanges = {
                        deleteItems: [],
                        pushFiles  : [],
                        pushFolders: []
                    };
                    let remoteChanges : ICloudChanges = {
                        deleteItems: [],
                        pushFiles  : [],
                        pushFolders: []
                    };

                    modificationsRemote.pushFiles.forEach(($item : string) : void => {
                        if (localChangeTimes[$item] >= remoteChangeTimes[$item]) {
                            remoteChanges.pushFiles.push($item);
                        } else if (ObjectValidator.IsEmptyOrNull($remoteSyncMap.items[$item])) {
                            localChanges.deleteItems.push($item);
                        } else {
                            localChanges.pushFiles.push($item);
                        }
                    });

                    modificationsRemote.pushFolders.forEach(($item : string) : void => {
                        if (localChangeTimes[$item] >= remoteChangeTimes[$item]) {
                            remoteChanges.pushFolders.push($item);
                        } else if (ObjectValidator.IsEmptyOrNull($remoteSyncMap.items[$item])) {
                            localChanges.deleteItems.push($item);
                        } else {
                            localChanges.pushFolders.push($item);
                        }
                    });

                    modificationsRemote.deleteItems.forEach(($item : string) : void => {
                        if (localChangeTimes[$item] >= remoteChangeTimes[$item]) {
                            remoteChanges.deleteItems.push($item);
                        } else if (ObjectValidator.IsEmptyOrNull($remoteSyncMap.items[$item])) {
                            remoteChanges.deleteItems.push($item);
                        } else {
                            if (!ObjectValidator.IsSet($remoteSyncMap.items[$item].size)) {
                                localChanges.pushFolders.push($item);
                            } else {
                                localChanges.pushFiles.push($item);
                            }
                        }
                    });

                    localChanges = localCloud.ReduceChanges(localChanges);
                    remoteChanges = localCloud.ReduceChanges(remoteChanges);

                    deleteRemote(remoteChanges.deleteItems, localChangeTimes, () : void => {
                        pushFolders(remoteChanges.pushFolders, localChangeTimes, () : void => {
                            pushFiles(remoteChanges.pushFiles, $localSyncMap, $remoteSyncMap, () : void => {
                                deleteLocal(localChanges.deleteItems, remoteChangeTimes, () : void => {
                                    pullFolders(localChanges.pushFolders, remoteChangeTimes, () : void => {
                                        pullFiles(localChanges.pushFiles, $remoteSyncMap, $localSyncMap, () : void => {
                                            complete();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }
    }
}
