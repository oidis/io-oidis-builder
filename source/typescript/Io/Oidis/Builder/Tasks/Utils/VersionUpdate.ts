/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IStdinQuestion, StdinManager } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/StdinManager.js";
import { IProject } from "../../Interfaces/IProject.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class VersionUpdate extends BaseTask {

    protected getName() : string {
        return "version-update";
    }

    protected async processAsync($option : string) : Promise<void> {
        const os : any = require("os");
        let version : string = this.programArgs.getOptions().newVersion;
        let description : string = this.programArgs.getOptions().description;

        if (ObjectValidator.IsEmptyOrNull(version) || ObjectValidator.IsEmptyOrNull(description)) {
            await StdinManager.ProcessAsync([
                <IStdinQuestion>{
                    callback($value : string, $callback : ($status : boolean) => void) : void {
                        version = $value;
                        $callback(true);
                    },
                    prefix: "new version"
                },
                <IStdinQuestion>{
                    callback($value : string, $callback : ($status : boolean) => void) : void {
                        description = $value;
                        $callback(true);
                    },
                    prefix: "description"
                }
            ]);

        }

        if (!ObjectValidator.IsEmptyOrNull(version)) {
            if (!ObjectValidator.IsEmptyOrNull(description)) {
                let packageConfPath : string = this.properties.projectBase + "/package.conf.jsonp";
                const scrPath : string = this.properties.projectBase + "/SW-Content-Register.txt";
                const readmePath : string = this.properties.projectBase + "/README.md";

                if (!this.fileSystem.Exists(packageConfPath)) {
                    packageConfPath = packageConfPath.slice(0, -1);
                }

                let packageConfOrig : string;
                let scrOrig : string;
                let readmeOrig : string;
                try {
                    if (this.fileSystem.Exists(packageConfPath)) {
                        packageConfOrig = this.fileSystem.Read(packageConfPath).toString();
                        const conf : IProject = this.project;
                        const packageConf : string =
                            packageConfOrig.replace(
                                "version: \"" + conf.version + "\"", "version: \"" + version + "\"");
                        packageConfOrig.replace(
                            "\"version\": \"" + conf.version + "\"", "\"version\": \"" + version + "\"");
                        this.fileSystem.Write(packageConfPath, packageConf);

                        if (this.fileSystem.Exists(scrPath)) {
                            scrOrig = this.fileSystem.Read(scrPath).toString();
                            const scr : string = scrOrig.replace(
                                "Release Name: " + conf.name + " v" + conf.version, "Release Name: " + conf.name + " v" + version);
                            this.fileSystem.Write(scrPath, scr.replace(
                                "Release Name: " + conf.name + " v" + conf.version, "Release Name: " + conf.name + " v" + version));
                        }

                        if (this.fileSystem.Exists(readmePath)) {
                            readmeOrig = this.fileSystem.Read(readmePath).toString();
                            let readme : string = readmeOrig.replace(
                                "# " + conf.name + " v" + conf.version, "# " + conf.name + " v" + version);
                            readme = readme.replace("## Licence", "## License");
                            // eslint-disable-next-line no-control-regex
                            const regex : RegExp = new RegExp("## History((?:.*[\r\n])+)[\r\n][\r\n]## License", "gmi");
                            const history : RegExpMatchArray = regex.exec(readme);
                            if (history !== null && history.length === 2) {
                                if (StringUtils.StartsWith(history[1], os.EOL)) {
                                    history[1] = history[1].substring(os.EOL.length);
                                }
                                readme = readme.replace(history[1], os.EOL + "### v" + version + os.EOL + description + history[1]);
                                this.fileSystem.Write(readmePath, readme);
                            }
                        } else {
                            LogIt.Error("README.md file has not been found.");
                        }
                    } else {
                        LogIt.Error("package.conf.json file has not been found.");
                    }
                } catch (ex) {
                    if (!ObjectValidator.IsEmptyOrNull(packageConfOrig)) {
                        this.fileSystem.Write(packageConfPath, packageConfOrig);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(scrOrig)) {
                        this.fileSystem.Write(scrPath, scrOrig);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(readmeOrig)) {
                        this.fileSystem.Write(readmePath, readmeOrig);
                    }
                    LogIt.Error(ex);
                }
            } else {
                LogIt.Error("Argument \"description\" must be specified.");
            }
        } else {
            LogIt.Error("Argument \"new-version\" must be specified.");
        }
    }
}
