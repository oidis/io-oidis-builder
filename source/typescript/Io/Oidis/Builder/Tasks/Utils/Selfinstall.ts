/*! ******************************************************************************************************** *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { IPersistenceHandler } from "@io-oidis-commons/Io/Oidis/Commons/Interfaces/IPersistenceHandler.js";
import { PersistenceFactory } from "@io-oidis-commons/Io/Oidis/Commons/PersistenceApi/PersistenceFactory.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { InstallationProgressCode } from "@io-oidis-services/Io/Oidis/Services/Enums/InstallationProgressCode.js";
import { InstallationProtocolType } from "@io-oidis-services/Io/Oidis/Services/Enums/InstallationProtocolType.js";
import { InstallationProgressEventArgs } from "@io-oidis-services/Io/Oidis/Services/Events/Args/InstallationProgressEventArgs.js";
import { SelfinstallDAO } from "../../DAO/SelfinstallDAO.js";
import { ISelfinstallRecipe } from "../../Interfaces/ISelfinstallRecipe.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class Selfinstall extends BaseTask {

    protected getName() : string {
        return "selfinstall";
    }

    protected process($done : any, $option : string) : void {
        let installChain : string[] = this.getDefaultChain($option);

        const register : IPersistenceHandler = PersistenceFactory.getPersistence(this.getClassName());
        const loadScripts : any = () : void => {
            const dao : SelfinstallDAO = new SelfinstallDAO();
            let scriptPath : string = this.project.target.selfinstall.script;
            if (!ObjectValidator.IsEmptyOrNull(scriptPath)) {
                if (!StringUtils.Contains(scriptPath, "https://", "http://", "file://", ":/", this.properties.projectBase) &&
                    !StringUtils.StartsWith(scriptPath, "/")) {
                    scriptPath = this.properties.projectBase + "/" + scriptPath;
                }
            } else {
                const scriptBase = this.properties.binBase + "/resource/data/Io/Oidis/Builder/Configuration";
                if (EnvironmentHelper.IsWindows()) {
                    scriptPath = scriptBase + "/SelfinstallWin.jsonp";
                } else if (EnvironmentHelper.IsLinux()) {
                    scriptPath = scriptBase + "/SelfinstallLinux.jsonp";
                } else if (EnvironmentHelper.IsMac()) {
                    scriptPath = scriptBase + "/SelfinstallMac.jsonp";
                }
            }
            if (!ObjectValidator.IsEmptyOrNull(scriptPath)) {
                dao.setConfigurationPath(scriptPath);
            }
            let success : boolean = true;
            dao.Load(LanguageType.EN, () : void => {
                dao.getEvents().setOnComplete(() : void => {
                    register.Variable("LastSelfInstall", new Date().getTime());
                    register.Variable("LastStatus", success);
                    if (!success) {
                        LogIt.Error("Not all of packages have been installed correctly.");
                    }
                    $done();
                });
                dao.getEvents().setOnChange(($args : InstallationProgressEventArgs) : void => {
                    if ($args.ProgressCode() === InstallationProgressCode.CHAIN_CHANGE && !$args.Status()) {
                        success = false;
                        LogIt.Warning($args.Message());
                    }
                });
                dao.getEvents().setOnError(($args : ErrorEventArgs) : void => {
                    LogIt.Error($args.Exception().ToString("", false));
                });
                if (!ObjectValidator.IsEmptyOrNull(this.project.target.selfinstall.chain)) {
                    installChain = this.project.target.selfinstall.chain;
                    LogIt.Debug("Using install chain configured in project configuration.");
                } else if (!ObjectValidator.IsEmptyOrNull(dao.getChain())) {
                    installChain = dao.getChain();
                    LogIt.Debug("Using install chain configured in Selfinstall DAO.");
                } else {
                    LogIt.Debug("Using default install chain.");
                }

                if (this.checkSingleInstall($option)) {
                    let singleTool : string = "";
                    installChain.forEach(($tool : string) : void => {
                        if (StringUtils.ToLowerCase($tool) === StringUtils.ToLowerCase($option)) {
                            singleTool = $tool;
                        }
                    });
                    if (ObjectValidator.IsEmptyOrNull(singleTool)) {
                        LogIt.Error("Unable to install required external module \"" + $option + "\": module does not exist");
                    } else {
                        installChain = [singleTool];
                    }
                }
                dao.getChain = () : string[] => {
                    return installChain;
                };
                if (dao.getConfigurationInterface() !== "IBackendInstallationRecipe") {
                    LogIt.Warning("Running installation recipe with unsupported interface: " +
                        dao.getConfigurationInterface());
                }
                const library : ISelfinstallRecipe = dao.ConfigurationLibrary();
                library.externalModules = this.externalModules;
                library.programArgs = this.programArgs;
                LogIt.Info("Running selfinstall for: " + JSON.stringify(dao.getChain()));
                dao.RunInstallationChain(InstallationProtocolType.INSTALL);
            });
        };
        if (this.builderConfig.noSelfupdate && !this.programArgs.IsForce()) {
            LogIt.Info("Selfinstall skipped due to builder configuration.");
            $done();
        } else if ($option !== "selfextract" && !this.programArgs.IsForce()) {
            if (register.Exists("LastSelfInstall")) {
                if ((new Date().getTime() - register.Variable("LastSelfInstall")) > (24 * 3600000)) {
                    loadScripts();
                } else {
                    if (register.Exists("LastStatus") && !register.Variable("LastStatus")) {
                        LogIt.Warning("Selfinstall skipped but last run ended with failures try to fix it by " +
                            "run of selfinstall with --force option.");
                        /// TODO: report also list of failed scripts and provide help for manual rollback
                        //        mechanism or create task for rollback
                    } else {
                        LogIt.Info("Selfinstall skipped. Recommended period is once per day, " +
                            "to force run selfinstall please run task with --force option.");
                    }
                    $done();
                }
            } else {
                loadScripts();
            }
        } else {
            loadScripts();
        }
    }

    protected getDefaultChain($option : string) : string[] {
        const isSingleInstall : boolean = this.checkSingleInstall($option);
        const installChain : string[] = [];
        if ($option !== "selfextract") {
            if (EnvironmentHelper.IsMac() || EnvironmentHelper.IsLinux()) {
                installChain.push(
                    "Autoconf",
                    "GCC"
                );
            }

            if (EnvironmentHelper.IsLinux()) {
                installChain.push("General");
            } else if (EnvironmentHelper.IsWindows()) {
                installChain.push("UPX");
            }

            installChain.push(
                "Git",
                "JDK",
                "Python2",
                "CMake"
            );
            if (EnvironmentHelper.IsWindows()) {
                installChain.push(
                    "ResourceHacker",
                    "NASM"
                );
            }

            if (this.properties.projectHas.Cpp.Source() || $option === "cpp" || $option === "all" || isSingleInstall) {
                if (EnvironmentHelper.IsWindows()) {
                    installChain.push("Msys2");
                } else {
                    if (!EnvironmentHelper.IsMac()) {
                        installChain.push("GCC");
                    }
                    installChain.push("AArch64");
                    if (EnvironmentHelper.IsMac()) {
                        installChain.push("Clang");
                    }
                }
                installChain.push(
                    "Doxygen",
                    "Python3",
                    "Cppcheck",
                    "Gtest"
                );
            }
            if (this.properties.projectHas.Python.Source() || $option === "python") {
                if (installChain.indexOf("Doxygen") === -1) {
                    installChain.push("Doxygen");
                }
                if (installChain.indexOf("Python3") === -1) {
                    installChain.push("Python3");
                }
            }
            if (this.properties.projectHas.Java.Source() || $option === "java" || $option === "all" || isSingleInstall) {
                installChain.push(
                    "Eclipse",
                    "Maven",
                    "Idea"
                );
            }
        } else {
            installChain.push(
                "RegisterCLI"
            );
        }

        return installChain;
    }

    protected checkSingleInstall($option : string) : boolean {
        return !ObjectValidator.IsEmptyOrNull($option) &&
            $option !== "all" && $option !== "cpp" && $option !== "java" && $option !== "python" && $option !== "selfextract";
    }
}
