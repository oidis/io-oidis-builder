/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { GeneralTaskType } from "../../Enums/GeneralTaskType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class VariableReplace extends BaseTask {

    protected getName() : string {
        return GeneralTaskType.VAR_REPLACE;
    }

    protected async processAsync($option : string) : Promise<void> {
        await this.runTaskAsync("string-replace:imports-prepare", "string-replace:variables", "string-replace:imports-finish");
    }
}
