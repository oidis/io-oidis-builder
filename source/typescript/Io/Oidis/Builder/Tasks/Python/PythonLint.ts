/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ColorType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ColorType.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { PythonUtils } from "../../Utils/PythonUtils.js";

export class PythonLint extends BaseTask {

    protected getName() : string {
        return "python-lint";
    }

    protected async processAsync($option : string) : Promise<void> {
        if (this.properties.projectHas.Python.Source()) {
            if ((await this.terminal.SpawnAsync(PythonUtils.getInterpreterPath(), [
                this.properties.binBase + "/resource/scripts/LintTestLoader.py",
                "--rcFile=" + this.properties.binBase + "/resource/configs/.pylintrc"
            ], {
                cwd: this.properties.projectBase,
                env: {
                    WITH_DEPENDENCIES: this.programArgs.getOptions().withDependencies ? 1 : 0
                }
            })).exitCode !== 0) {
                LogIt.Error("Python lint failed.");
            }
        } else {
            LogIt.Info(">>"[ColorType.YELLOW] + " Python lint task skipped: 0 source files have been found");
        }
    }
}
