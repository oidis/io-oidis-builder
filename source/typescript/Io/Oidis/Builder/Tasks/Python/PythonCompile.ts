/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseTask } from "../../Primitives/BaseTask.js";

export class PythonCompile extends BaseTask {

    protected getName() : string {
        return "python-compile";
    }

    protected async processAsync($option : string) : Promise<void> {
        const compiled : string = this.properties.projectBase + "/build/compiled/**/python";

        this.fileSystem
            .Expand([compiled + "/*/**", "!" + compiled + "/**/[__]*"])
            .filter(($path : string) : boolean => {
                return this.fileSystem.IsDirectory($path);
            })
            .forEach(($path : string) : void => {
                if (!this.fileSystem.Exists($path + "/__init__.py")) {
                    this.fileSystem.Write($path + "/__init__.py", "");
                }
            });

        const sources : string[] = this.fileSystem.Expand(compiled + "/*");
        for await (const source of sources) {
            await this.fileSystem.CopyAsync(StringUtils.Substring(source, 0, StringUtils.IndexOf(source, "/", false)),
                this.properties.projectBase + "/build/target");
        }
    }
}
