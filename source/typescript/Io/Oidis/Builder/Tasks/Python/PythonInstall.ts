/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { IExecuteResult } from "@io-oidis-localhost/Io/Oidis/Localhost/Connectors/Terminal.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { IPythonPackage } from "../../Interfaces/IPythonPackage.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { PythonUtils } from "../../Utils/PythonUtils.js";

export class PythonInstall extends BaseTask {
    protected getName() : string {
        return "python-install";
    }

    protected async processAsync($option : string) : Promise<void> {
        if (!ObjectValidator.IsEmptyOrNull($option) && !($option === "2" || $option === "3")) {
            LogIt.Error("Invalid option '" + $option + "' - expected '2' or '3' or none.");
        }
        if (ObjectValidator.IsSet($option)) {
            let cmd : string = "python";
            let cwd : string = this.properties.projectBase + "/build_cache/pyenv";
            if ($option === "3") {
                cmd += "3";
                cwd += "3";
            }
            if (!this.fileSystem.Exists(cwd)) {
                if ((await this.terminal.SpawnAsync(cmd, ["-m", $option === "3" ? "venv" : "virtualenv", cwd], null))
                    .exitCode === 0) {
                    if (!EnvironmentHelper.IsWindows()) {
                        cwd += "/bin";
                    }
                    let cmd : string = "pip";
                    if (!EnvironmentHelper.IsWindows()) {
                        cmd = "./" + cmd;
                    }
                    const res : IExecuteResult = await this.terminal.SpawnAsync(cmd, ["list"], cwd);
                    const requiresByProject : any = {};
                    requiresByProject[this.properties.projectBase] = PythonUtils.getRequiresMap(this.properties.projectBase, $option);
                    this.fileSystem.Expand(this.properties.projectBase + "/dependencies/*").forEach(($dependency : string) : void => {
                        requiresByProject[$dependency] = PythonUtils.getRequiresMap($dependency, $option);
                    });
                    requiresByProject[this.properties.binBase] = PythonUtils.getRequiresMap(this.properties.binBase, $option);

                    let packages : IPythonPackage[] = [];
                    Object.keys(requiresByProject).forEach(($path : string) : void => {
                        packages = packages.concat(PythonUtils.getPackages($path, requiresByProject[$path]));
                    });

                    StringUtils.Split(res.std[0], "\n").forEach(($line : string) : void => {
                        packages.forEach(($package : IPythonPackage) : void => {
                            if (!$package.installed &&
                                StringUtils.PatternMatched($package.name + " *" + $package.version + "*", $line)) {
                                $package.installed = true;
                            }
                        });
                    });

                    const notFound : string[] = [];
                    packages.forEach(($package : IPythonPackage) : void => {
                        if (!$package.installed) {
                            notFound.push($package.name + " " + $package.version);
                        }
                    });
                    if (ObjectValidator.IsEmptyOrNull(notFound)) {
                        LogIt.Info("Up-to-date");
                    } else {
                        LogIt.Warning("\nMissing packages: " + notFound.join(","));
                        for await (const pipPackage of packages) {
                            if (!pipPackage.installed) {
                                const args : string[] = [
                                    "install"
                                ];
                                if (ObjectValidator.IsEmptyOrNull(pipPackage.location)) {
                                    args.push(PythonUtils.PackageToString(pipPackage));
                                } else {
                                    args.push(pipPackage.location);
                                }
                                if ((await this.terminal.SpawnAsync(cmd, args, cwd)).exitCode !== 0) {
                                    LogIt.Error("Failed to install required package.");
                                }
                            }
                        }
                    }
                } else {
                    LogIt.Error("Virtual environment initialization failed.");
                }
            } else {
                LogIt.Info("Nothing to install.");
            }
        } else {
            await this.runTaskAsync(["python-install:2", "python-install:3"]);
        }
    }
}
