/*! ******************************************************************************************************** *
 *
 * Copyright 2020-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { PythonUtils } from "../../Utils/PythonUtils.js";

export class PythonPackaging extends BaseTask {

    protected getName() : string {
        return "python-packaging";
    }

    protected async processAsync($option : string) : Promise<void> {
        const target : string = this.properties.projectBase + "/build/target";
        const compiled : string = this.properties.projectBase + "/build/compiled";

        const dataFolder : string = target + "/" + this.project.name + "_data";
        this.fileSystem.CreateDirectory(dataFolder);
        this.fileSystem.Write(dataFolder + "/__init__.py", "");
        this.fileSystem.Expand([
            target + "/*",
            "!" + target + "/MANIFEST.in",
            "!" + target + "/setup.py",
            "!" + target + "/LICENSE.txt",
            "!" + target + "/SW-Content-Register.txt"
        ]).forEach(($path : string) : void => {
            this.fileSystem.Rename($path, StringUtils.Replace($path, target, dataFolder));
        });

        if ((await this.terminal.SpawnAsync(PythonUtils.getInterpreterPath(), [
            "setup.py", "sdist", "--formats=gztar"
        ], this.properties.projectBase + "/build/target")).exitCode === 0) {
            this.fileSystem.Delete(compiled);
            this.fileSystem.Rename(target, compiled);
            await this.fileSystem.CopyAsync(this.fileSystem.Expand(compiled + "/dist/*.tar.gz")[0],
                target + "/" + this.properties.packageName + ".tar.gz");
            await this.fileSystem.CopyAsync(compiled + "/LICENSE.txt",
                target + "/LICENSE.txt");
            await this.fileSystem.CopyAsync(compiled + "/SW-Content-Register.txt",
                target + "/SW-Content-Register.txt");
        } else {
            LogIt.Error("Python packaging failed.");
        }
    }
}
