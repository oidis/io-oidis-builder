/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { BaseTask } from "../../Primitives/BaseTask.js";
import { PythonUtils } from "../../Utils/PythonUtils.js";

export class PythonTest extends BaseTask {
    private static config : any;

    public static getConfig() : any {
        if (ObjectValidator.IsEmptyOrNull(PythonTest.config)) {
            PythonTest.config = {
                unit: {
                    filter: "*"
                }
            };
        }
        return PythonTest.config;
    }

    protected getName() : string {
        return "python-test";
    }

    protected async processAsync($option : string) : Promise<void> {
        if ((await this.terminal.SpawnAsync(PythonUtils.getInterpreterPath(), [
            this.properties.projectBase + "/bin/resource/scripts/UnitTestLoader.py",
            "--filter=" + PythonTest.getConfig().unit.filter
        ], this.properties.projectBase)).exitCode !== 0) {
            LogIt.Error("Python unit test failed.");
        }
    }
}
