/*! ******************************************************************************************************** *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import "@io-oidis-commons/Io/Oidis/Commons/Utils/ReflectionEmitter.js";
import "@io-oidis-commons/Io/Oidis/Commons/Interfaces/Interface.js";

import { LogLevel } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogLevel.js";
import { LogSeverity } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LogSeverity.js";
import { EnvironmentArgs as CommonsEnvironmentArgs } from "@io-oidis-commons/Io/Oidis/Commons/EnvironmentArgs.js";
import { ErrorEventArgs } from "@io-oidis-commons/Io/Oidis/Commons/Events/Args/ErrorEventArgs.js";
import { Convert } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Convert.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { Reflection } from "@io-oidis-commons/Io/Oidis/Commons/Utils/Reflection.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { LogAdapterType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/LogAdapterType.js";
import { Loader as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/Loader.js";
import { FileAdapter } from "@io-oidis-localhost/Io/Oidis/Localhost/LogProcessor/Adapters/FileAdapter.js";
import { JsonAdapter } from "@io-oidis-localhost/Io/Oidis/Localhost/LogProcessor/Adapters/JsonAdapter.js";
import { IntegrityCheck } from "@io-oidis-localhost/Io/Oidis/Localhost/Primitives/Decorators.js";
import { EnvironmentHelper } from "@io-oidis-localhost/Io/Oidis/Localhost/Utils/EnvironmentHelper.js";
import { ReportServiceConnector } from "@io-oidis-services/Io/Oidis/Services/Connectors/ReportServiceConnector.js";
import stripJsonComments from "@nodejs/strip-json-comments/index.js";
import { BuilderServiceManager } from "./Connectors/BuilderServiceManager.js";
import { FileSystemHandler } from "./Connectors/FileSystemHandler.js";
import { Terminal } from "./Connectors/Terminal.js";
import { Resources } from "./DAO/Resources.js";
import { EnvironmentArgs } from "./EnvironmentArgs.js";
import "./ForceCompile.js";
import { HttpResolver } from "./HttpProcessor/HttpResolver.js";
import { HttpServer } from "./HttpProcessor/HttpServer.js";
import { IAppConfiguration } from "./Interfaces/IAppConfiguration.js";
import { IProject, IProjectBuilder } from "./Interfaces/IProject.js";
import { IProperties } from "./Interfaces/IProperties.js";
import { ProcessAdapter } from "./LogProcessor/Adapters/ProcessAdapter.js";
import { ProgramArgs } from "./Structures/ProgramArgs.js";
import { DependenciesDownload } from "./Tasks/Composition/DependenciesDownload.js";
import { CacheManager } from "./Tasks/Utils/CacheManager.js";
import { GitManager } from "./Tasks/Utils/GitManager.js";
import { TasksResolver } from "./TasksResolver.js";

export class Loader extends Parent {
    private reportMessage : string;
    private errorDetected : boolean;
    private taskResolver : TasksResolver;
    private jsdom : any;
    private updateEnv : boolean;

    public static getInstance() : Loader {
        return <Loader>super.getInstance();
    }

    @IntegrityCheck({name: "Io.Oidis.Localhost.Loader.npmModules"})
    private static async npmModulesExt() : Promise<void> {
        return (<any>Parent).npmModules();
    }

    constructor() {
        super();
        this.errorDetected = false;
        this.updateEnv = false;
    }

    public getAppConfiguration() : IAppConfiguration {
        return <IAppConfiguration>super.getAppConfiguration();
    }

    public getAppProperties() : IProperties {
        return this.getEnvironmentArgs().getAppProperties();
    }

    public getEnvironmentArgs() : EnvironmentArgs {
        return <EnvironmentArgs>super.getEnvironmentArgs();
    }

    public getProjectConfig() : IProject {
        return this.getEnvironmentArgs().getTargetConfig();
    }

    public getHttpResolver() : HttpResolver {
        return <HttpResolver>super.getHttpResolver();
    }

    public getProgramArgs() : ProgramArgs {
        return <ProgramArgs>super.getProgramArgs();
    }

    public getFileSystemHandler() : FileSystemHandler {
        return <FileSystemHandler>super.getFileSystemHandler();
    }

    public getTerminal() : Terminal {
        return <Terminal>super.getTerminal();
    }

    public setLogPath($path : string) : void {
        if (!ObjectValidator.IsEmptyOrNull($path)) {
            (<FileAdapter>this.logger.getAdapter(LogAdapterType.FILE)).LogPath($path);
            (<JsonAdapter>this.logger.getAdapter(LogAdapterType.JSON)).LogPath($path);
        }
    }

    protected initProgramArgs() : ProgramArgs {
        return new ProgramArgs();
    }

    protected initEnvironment() : EnvironmentArgs {
        return new EnvironmentArgs();
    }

    protected initHttpServer() : HttpServer {
        return new HttpServer();
    }

    protected initResolver() : HttpResolver {
        return new HttpResolver("/");
    }

    protected initFileSystem() : FileSystemHandler {
        return new FileSystemHandler();
    }

    protected initTerminal() : Terminal {
        return new Terminal();
    }

    protected processProgramArgs() : boolean {
        const args : ProgramArgs = this.getProgramArgs();
        if (args.IsHubClient() || args.IsServiceClient()) {
            args.StartServer(false);
        }
        let isChainParent : boolean = false;
        if (ObjectValidator.IsEmptyOrNull(args.ChainId())) {
            isChainParent = true;
            args.ChainId(StringUtils.getSha1(new Date().getTime() + "" + Math.random()));
        }

        let processed : boolean = super.processProgramArgs();
        if (!ObjectValidator.IsEmptyOrNull(this.getAppConfiguration().target.appDataPath)) {
            args.AppDataPath(this.getAppConfiguration().target.appDataPath);
            this.setAnonymizer({
                appData: args.AppDataPath()
            });
        }

        const processLogger : ProcessAdapter = new ProcessAdapter();
        this.logger.AddAdapter(processLogger);
        processLogger.setOwner(this);

        if (args.AppDataPath() !== args.ProjectBase()) {
            const filepath : string = this.getFileSystemHandler().NormalizePath(
                args.AppDataPath() + "/" + this.getEnvironmentArgs().getAppName() + ".config.json");
            const fs : any = require("fs");
            if (fs.existsSync(filepath)) {
                try {
                    const configData : any = JSON.parse(stripJsonComments(fs.readFileSync(filepath).toString()));
                    Resources.Extend(this.getAppConfiguration(), configData);
                } catch (ex) {
                    this.stderrWrite("Unable to parse \"" + filepath + "\". " + ex.stack);
                }
            }
        }

        const onExitHandler : any = ($exitCode : number = 0) : void => {
            this.Exit($exitCode);
        };

        process.env.PATH = this.getTerminal().NormalizeEnvironmentPath(this.getAppEnvironmentPath());

        if (!processed) {
            if (args.IsExternalModuleTask() && !args.IsForwardedTask()) {
                processed = true;
                const argv : string[] = args.getOptions().forwardArgs;

                for (const key in argv) {
                    if (argv.hasOwnProperty(key) && StringUtils.Contains(argv[key], " ") && !StringUtils.StartsWith(argv[key], "\"")) {
                        argv[key] = "\"" + argv[key] + "\"";
                    }
                }

                const cmd : string = argv[0];
                argv.shift();
                process.env.PATH = this.getTerminal().NormalizeEnvironmentPath(EnvironmentHelper.getNodejsRoot());
                this.getTerminal()
                    .Spawn(cmd, argv, {
                        advanced: {noTerminalLog: true, noExitOnStderr: true, useStdIn: true},
                        cwd     : process.cwd()
                    }, onExitHandler);
            } else {
                processed = true;
                BuilderServiceManager.CreatePIDRecord();
                if (args.IsTargetInfo()) {
                    LogIt.MuteToggle();
                }
                const environmentArgs : EnvironmentArgs = this.getEnvironmentArgs();
                environmentArgs.LoadProjectEnvironment(async () : Promise<void> => {
                    if (!args.getOptions().noTarget) {
                        const targetBase : string = environmentArgs.getTargetBase();
                        this.jsdom.reconfigure({url: "file:///" + targetBase + "/"});
                        process.cwd = () : string => {
                            return targetBase;
                        };
                    }
                    const buildTime : number = new Date(this.getEnvironmentArgs().getBuildTime()).getTime();
                    const rootPath : string = this.getAppProperties().binBase + "/resource/esmodules-" + buildTime + "/Io/Oidis/Builder";
                    const tasks : string[] = this.getFileSystemHandler().Expand(rootPath + "/Tasks/**/*.{js,mjs}");
                    try {
                        for await (let task of tasks) {
                            task = StringUtils.Replace(task, rootPath, ".");
                            LogIt.Debug("> " + task);
                            await import(task);
                        }
                    } catch (ex) {
                        LogIt.Error("Failed to load tasks.", ex);
                    }
                    Reflection.Refresh();
                    this.taskResolver = new TasksResolver();
                    await this.taskResolver.Resolve("solution-loader");

                    if (args.IsTargetInfo()) {
                        LogIt.MuteToggle();
                        const propertyKey : string = args.getOptions().targetInfoProperty;
                        if (ObjectValidator.IsSet(this.getProjectConfig()[propertyKey])) {
                            try {
                                let value : any = this.getProjectConfig()[propertyKey];
                                if (!ObjectValidator.IsString(value)) {
                                    value = JSON.stringify(value);
                                }
                                LogIt.Info(value);
                            } catch (ex) {
                                LogIt.Error("Failed to read property \"" + propertyKey + "\" " +
                                    "from target configuration.");
                                LogIt.Debug(ex.stack);
                            }
                        } else {
                            LogIt.Error("Failed to find property \"" + propertyKey + "\" " +
                                "in target configuration.");
                        }

                        LogIt.MuteToggle();
                        onExitHandler();
                    }
                    this.taskResolver = new TasksResolver();
                    if (isChainParent) {
                        if (!args.IsForwardedTask()) {
                            const requiredBuilder : IProjectBuilder = this.getProjectConfig().builder;
                            if (!ObjectValidator.IsEmptyOrNull(requiredBuilder.location)) {
                                requiredBuilder.location = StringUtils.Replace(requiredBuilder.location, "\\", "/");
                            }
                            let isHostForward : boolean = false;
                            if (!args.IsServiceClient() && !args.IsHubClient()) {
                                if (requiredBuilder.port > 0 || requiredBuilder.location === "service") {
                                    args.IsServiceClient(true);
                                } else if (!ObjectValidator.IsEmptyOrNull(requiredBuilder.location) &&
                                    requiredBuilder.location !== "host") {
                                    if (requiredBuilder.location === "agent" ||
                                        !ObjectValidator.IsEmptyOrNull(requiredBuilder.version) ||
                                        requiredBuilder.version === "latest") {
                                        args.IsHubClient(true);
                                    } else if ((StringUtils.StartsWith(requiredBuilder.location, "/") ||
                                            StringUtils.Contains(requiredBuilder.location, ":/")) &&
                                        this.getFileSystemHandler().Exists(requiredBuilder.location)) {
                                        isHostForward = true;
                                    } else {
                                        args.IsHubClient(true);
                                    }
                                }
                            }
                            if (args.IsServiceClient() || args.IsHubClient()) {
                                await this.taskResolver.Resolve("forward-task");
                                onExitHandler();
                            } else if (args.StartAgent()) {
                                await this.taskResolver.Resolve("builder-agent:start");
                                onExitHandler();
                            } else if (isHostForward) {
                                const cliArgs : string[] = [].concat(process.argv);
                                if (EnvironmentHelper.IsEmbedded() || StringUtils.Contains(cliArgs[0], "nodejs")) {
                                    cliArgs.shift();
                                }
                                if (StringUtils.ContainsIgnoreCase(cliArgs[0], "loader.min.js")) {
                                    cliArgs.shift();
                                }
                                const env : any = process.env;
                                env.IS_OIDIS_PROXY_TASK = 1;
                                env.PATH = this.getTerminal().NormalizeEnvironmentPath(requiredBuilder + "/cmd");
                                this.getTerminal().Spawn("oidis", cliArgs.concat(["--forward-task"]), {
                                    advanced: {
                                        colored: true
                                    },
                                    cwd     : args.TargetBase(),
                                    env
                                }, onExitHandler);
                            } else if (args.getOptions().selfextract) {
                                await this.taskResolver.Resolve("selfinstall:selfextract");
                                BuilderServiceManager.RemovePIDRecord();
                                if (EnvironmentHelper.IsWindows()) {
                                    this.getTerminal().Spawn("notifu.exe", [
                                            "/p", "\"Oidis Builder - install\"",
                                            "/m", "\"The installation has been finished.\\nHave fun! :)\"",
                                            "/q",
                                            "/t", "info",
                                            "/d", "10000",
                                            "/i", "\"" + this.getFileSystemHandler().NormalizePath(args.ProjectBase(), true) +
                                            "\\resource\\graphics\\icon.ico\""
                                        ],
                                        {
                                            cwd    : args.ProjectBase() + "/resource/libs/node-notifier",
                                            verbose: false
                                        },
                                        ($exitCode : number, $std : string[]) : void => {
                                            if ($exitCode !== 0) {
                                                LogIt.Error($std[0] + $std[1]);
                                            }
                                            onExitHandler();
                                        });
                                } else {
                                    onExitHandler();
                                }
                            } else if (args.getOptions().selfupdate) {
                                await this.taskResolver.Resolve("selfupdate");
                                onExitHandler();
                            } else {
                                await this.taskResolver.Resolve(args.getTasks());
                                onExitHandler();
                            }
                        } else if (args.getOptions().selfextract || args.getOptions().selfupdate || args.StartAgent()) {
                            LogIt.Error("Current task is not supported by remote execution.");
                        } else {
                            await this.taskResolver.Resolve(args.getTasks());
                            onExitHandler();
                        }
                    } else {
                        await this.taskResolver.Resolve(args.getTasks());
                        onExitHandler();
                    }
                });
            }
        }

        return processed;
    }

    protected onCloseHandler($done : ($exitCode? : number) => void) : void {
        const handleExit : any = ($message? : string) : void => {
            this.stderrWrite($message);
            super.onCloseHandler(($exitCode? : number) : void => {
                $done($exitCode);
            });
        };
        const createReport : any = () : void => {
            if (ObjectValidator.IsEmptyOrNull(this.reportMessage)) {
                handleExit();
            } else if (!this.getAppConfiguration().noReport) {
                try {
                    const connector : ReportServiceConnector =
                        new ReportServiceConnector(false, this.getAppConfiguration().reportHub + "/connector.config.jsonp");
                    connector.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                        handleExit($eventArgs.ToString("", false));
                    });
                    const env : CommonsEnvironmentArgs = this.getEnvironmentArgs();
                    let logPath : string;
                    if (this.getProgramArgs().getOptions().noTarget) {
                        logPath = (<any>this).logPath;
                    } else {
                        logPath = this.getAppProperties().projectBase + "/log/build.log";
                    }
                    connector
                        .CreateReport({
                            appId      : StringUtils.getSha1(env.getAppName() + env.getProjectVersion()),
                            appName    : env.getProjectName(),
                            appVersion : env.getProjectVersion(),
                            log        : this.getFileSystemHandler().Read(logPath).toString(),
                            printScreen: "",
                            timeStamp  : new Date().getTime(),
                            trace      : this.reportMessage
                        })
                        .Then(() : void => {
                            handleExit();
                        });
                } catch (ex) {
                    handleExit("Unable to send error report.");
                }
            } else {
                handleExit("Crash report skipped: reports are disabled by noReport option");
            }
        };
        try {
            if (!this.getProgramArgs().IsBaseTask() && !this.getProgramArgs().IsExternalModuleTask()) {
                const colors : any = require("colors");
                LogIt.Info(colors.underline("\nRunning on close task"));
                BuilderServiceManager.RemovePIDRecord();
                CacheManager.ReferenceRollback();
                DependenciesDownload.Rollback();
                GitManager.StashRestore();
                this.getEnvironmentArgs().UpdateMetadata();
                if (!ObjectValidator.IsEmptyOrNull(this.taskResolver) && this.getProgramArgs().IsDebug()) {
                    this.taskResolver.getReport();
                }
                LogIt.Info(colors.underline("\nExecution Time") + " " +
                    colors.gray("(" + Convert.TimeToGMTformat((<any>this).startTime.getTime()) + ")"));
                LogIt.Info("Total " + Convert.TimeToLongString(this.getUptime()));
                if (this.errorDetected && !ObjectValidator.IsEmptyOrNull(this.taskResolver)) {
                    LogIt.Info(colors.red("\nFAILED task: " + this.taskResolver.getCurrentTaskName()));
                }
            }
            createReport();
        } catch (ex) {
            createReport();
        }
    }

    protected getAppEnvironmentPath() : string {
        if (!this.updateEnv) {
            this.updateEnv = true;
            return super.getAppEnvironmentPath();
        } else {
            const path : any = require("path");
            const args : ProgramArgs = Loader.getInstance().getProgramArgs();
            const externalModules : string = args.AppDataPath() + "/external_modules";
            if (EnvironmentHelper.IsWindows()) {
                const envPath : string =
                    args.ProjectBase() + ";" +
                    externalModules + "/jdk/bin;" +
                    externalModules + "/doxygen;" +
                    externalModules + "/cppcheck;" +
                    externalModules + "/python3;" + externalModules + "/python/3Scripts;" +
                    externalModules + "/python36;" + externalModules + "/python36/Scripts;" +
                    externalModules + "/python;" + externalModules + "/python/Scripts;" +
                    externalModules + "/msys2;" + externalModules + "/msys2/mingw64/bin;" +
                    externalModules + "/cmake/bin;" +
                    externalModules + "/git/cmd;" +
                    externalModules + "/maven/bin;" +
                    externalModules + "/nasm;" +
                    externalModules + "/upx;" +
                    externalModules + "/resourcehacker;";
                return StringUtils.Replace(envPath, "/", path.sep);
            } else {
                return "" +
                    args.ProjectBase() + ":" +
                    externalModules + "/jdk/bin:" +
                    externalModules + "/gcc:" +
                    externalModules + "/aarch32/bin:" +
                    externalModules + "/aarch64/bin:" +
                    externalModules + "/clang:" +
                    externalModules + "/cmake/bin:" +
                    externalModules + "/git/bin:" +
                    externalModules + "/doxygen" + (EnvironmentHelper.IsMac() ? "" : "/bin") + ":" +
                    externalModules + "/cppcheck:" +
                    externalModules + "/python:" +
                    externalModules + "/python36:" +
                    externalModules + "/maven/bin:" +
                    externalModules + "/upx";
            }
        }
    }

    protected initDOM() : any {
        return this.jsdom = super.initDOM();
    }

    private stderrWrite($message : string) : void {
        this.logger.getAdapter(LogAdapterType.STD).Process({
            entryPoint: null,
            level     : LogLevel.ERROR,
            message   : $message,
            severity  : LogSeverity.HIGH,
            time      : new Date().getTime(),
            trace     : null
        });
    }
}
