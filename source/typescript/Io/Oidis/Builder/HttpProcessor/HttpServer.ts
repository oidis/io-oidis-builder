/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { HttpServer as Parent} from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/HttpServer.js";
import { Network } from "../Connectors/Network.js";
import { IAppConfiguration } from "../Interfaces/IAppConfiguration.js";
import { Loader } from "../Loader.js";
import { ProgramArgs } from "../Structures/ProgramArgs.js";

export class HttpServer extends Parent {

    public Start() : void {
        let port : number = Loader.getInstance().getAppConfiguration().servicePort;
        const args : ProgramArgs = Loader.getInstance().getProgramArgs();
        if (args.Port() > 0) {
            port = args.Port();
        }
        if (ObjectValidator.IsEmptyOrNull(port) || port < 0) {
            port = 0;
        }
        const networking : Network = new Network();
        networking.IsPortFree(port, ($status : boolean) : void => {
            if ($status) {
                this.createServer(port, false, true);
            } else {
                LogIt.Error("Service port \"" + port + "\" is already in use.\n" +
                    "    Please use --port option or kill process using required port.");
            }
        });
    }

    public StartConnector($isService : boolean = true) : void {
        const args : ProgramArgs = Loader.getInstance().getProgramArgs();
        const networking : Network = new Network();
        networking.getFreePort(($port : number) : void => {
            if ($isService) {
                const logPath : string = args.TargetBase() + "/log/server/connector.log";
                Loader.getInstance().getFileSystemHandler().Delete(logPath);
                Loader.getInstance().setLogPath(logPath);
            }
            Loader.getInstance().getFileSystemHandler()
                .CreateDirectory(args.TargetBase() + "/resource/data/Io/Oidis/Builder");
            Loader.getInstance().getFileSystemHandler()
                .CreateDirectory(args.TargetBase() + "/build/target/resource/data/Io/Oidis/Builder");
            this.createServer($port, false, true, [
                args.TargetBase() + "/resource/data/Io/Oidis/Builder",
                args.TargetBase() + "/build/target/resource/data/Io/Oidis/Builder"
            ]);
        });
    }

    protected getAppConfiguration() : IAppConfiguration {
        return <IAppConfiguration>super.getAppConfiguration();
    }
}
