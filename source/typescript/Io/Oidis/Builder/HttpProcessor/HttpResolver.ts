/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { IHttpResolverContext } from "@io-oidis-commons/Io/Oidis/Commons/HttpProcessor/HttpResolver.js";
import { HttpResolver as Parent } from "@io-oidis-localhost/Io/Oidis/Localhost/HttpProcessor/HttpResolver.js";

export class HttpResolver extends Parent {

    protected async getStartupResolvers($context : IHttpResolverContext) : Promise<any> {
        if ($context.isBrowser) {
            await this.registerResolver(async () => (await import("../Index.js")).Index,
                "/", "/index", "/index.html", "/web/");
        } else {
            await this.registerResolver(async () => (await import(
                    "@io-oidis-connector/Io/Oidis/Connector/HttpProcessor/Resolvers/LiveContentResolver.js")).LiveContentResolver,
                "/liveContent/");
        }
        return super.getStartupResolvers($context);
    }
}
