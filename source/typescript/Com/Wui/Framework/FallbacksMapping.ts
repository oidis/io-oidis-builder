/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2025 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
// eslint-disable-next-line @typescript-eslint/no-namespace
namespace Com.Wui.Framework {
    "use strict";

    export const Commons = globalThis.Io.Oidis.Commons;
    export const Gui = globalThis.Io.Oidis.Gui;
    export const Services = globalThis.Io.Oidis.Services;
    export const Localhost = (<any>globalThis.Io).Oidis.Localhost;
    export const Builder = (<any>globalThis.Io).Oidis.Builder;
}
