/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { IFileSystemDownloadOptions } from "@io-oidis-services/Io/Oidis/Services/Connectors/FileSystemHandlerConnector.js";
import stripJsonComments from "@nodejs/strip-json-comments/index.js";
import { FileSystemHandler } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Connectors/FileSystemHandler.js";
import {
    IProjectDependency,
    IProjectDependencyLocation, IProjectDependencyLocationPath
} from "../../../../../../../../source/typescript/Io/Oidis/Builder/Interfaces/IProject.js";
import { Loader } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Loader.js";
import { ProgramArgs } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Structures/ProgramArgs.js";
import { TaskEnvironment } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Structures/TaskEnvironment.js";
import { DependenciesDownload } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Tasks/Composition/DependenciesDownload.js";

export class MockDependenciesDownload extends DependenciesDownload {
    public async processSingleSource($dependencyName : string, $source : IProjectDependencyLocation,
                                     $dependency : IProjectDependency, $targetPath : string) : Promise<void> {
        return super.processSingleSource($dependencyName, $source, $dependency, $targetPath);
    }

    public ResolveDependencyPath($path : string | IProjectDependencyLocationPath) : string[] {
        return super.ResolveDependencyPath($path);
    }

    public isPlatformMatch($platform : string) : boolean {
        return super.isPlatformMatch($platform);
    }

    public async download($location : string | IFileSystemDownloadOptions, $targetPath : string, $unpack : boolean = true) : Promise<void> {
        return super.download($location, $targetPath, $unpack);
    }

    public async processDependency($dependencyName : string, $dependency : IProjectDependency | string) : Promise<void> {
        return super.processDependency($dependencyName, $dependency);
    }

    public collectDependencies($dependencies : IProjectDependency[]) : string[] {
        return super.collectDependencies($dependencies);
    }

    public async processDependencies($dependencies : IProjectDependency[]) : Promise<void> {
        return super.processDependencies($dependencies);
    }
}

export class DependenciesDownloadTest extends UnitTestRunner {
    private dependenciesDownload : MockDependenciesDownload;
    private fileSystem : FileSystemHandler;
    private readonly env : TaskEnvironment;
    private dependencies : any;
    private readonly dependencyNames : string[];

    constructor() {
        super();
        // this.setMethodFilter("testCollectDependencies");
        this.env = new TaskEnvironment();
        this.dependencyNames = [];
    }

    public testParseLocation() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            for (const item in this.dependencies) {
                if (this.dependencies.hasOwnProperty(item) && !ObjectValidator.IsEmptyOrNull(this.dependencyNames) &&
                    (this.dependencyNames.indexOf(item) !== -1 || this.dependencyNames.length === 0)) {
                    if (this.dependencies[item].hasOwnProperty("releases")) {
                        this.env.Project().releases = this.dependencies[item].releases;
                    } else {
                        this.env.Project().releases = [];
                    }
                    (<any>this.dependenciesDownload).buildExecutor.setEnvironment(this.env);
                    assert.equal(
                        JSON.stringify(this.dependenciesDownload.ParseLocation(this.dependencies[item].input)),
                        JSON.stringify(this.dependencies[item].output), "Failed case: " + item);
                }
            }
            $done();
        };
    }

    // todo to much hacks, not sure how to test it
    public testgetSource() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const testItems : string[] = [];
            for (const item in this.dependencies) {
                if (this.dependencies.hasOwnProperty(item) && !ObjectValidator.IsEmptyOrNull(this.dependencyNames) &&
                    (this.dependencyNames.indexOf(item) !== -1 || this.dependencyNames.length === 0)) {
                    testItems.push(item);
                }
            }

            const hackClone : any = ($repoSource : string, $branch : string, $targetPath : string,
                                     $callback : ($repoSource : string, $branch : string, $targetPath : string) => void) : void => {
                $callback($repoSource, $branch, $targetPath);
            };
            const hackDownload : any = ($location : string | any, $targetPath : string, $callback : () => void) : void => {
                $callback();
            };
            (<any>this.dependenciesDownload).clone = hackClone;
            (<any>this.dependenciesDownload).download = hackDownload;

            let index : number = 0;
            const processNext : any = () : void => {
                if (index < testItems.length) {
                    const itemName : string = testItems[index];
                    const testItem : any = this.dependencies[itemName];
                    if (testItem.hasOwnProperty("releases")) {
                        this.env.Project().releases = testItem.releases;
                    } else {
                        this.env.Project().releases = [];
                    }
                    (<any>this.dependenciesDownload).buildExecutor.setEnvironment(this.env);

                    this.dependenciesDownload.getSource(itemName, testItem.input, testItem, "<path>", () : void => {
                        // assert.equal(
                        //     JSON.stringify(this.dependenciesDownload.ParseLocation(this.dependencies[item].input)),
                        //     JSON.stringify(this.dependencies[item].output), "Failed case: " + item);
                        index++;
                        processNext();
                    });
                } else {
                    $done();
                }
            };

            processNext();
        };
    }

    public testCollectDependencies() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const dependenciesDownload : MockDependenciesDownload = new MockDependenciesDownload();
            const deps : any = {
                dep1: "git+https://some.org/project#branch",
                dep2: "file://some.org/project",
                dep3: "git+https://some.org/project#branch"
            };

            const backup : any = LogIt.Error;
            let errorCalled : boolean = false;
            LogIt.Error = () : void => {
                errorCalled = true;
            };
            assert.deepEqual(dependenciesDownload.collectDependencies(deps), ["dep1", "dep2", "dep3"]);
            deps.dep3 = "git+https://some.org/project#branch1";
            assert.deepEqual(dependenciesDownload.collectDependencies(deps), []);
            assert.ok(errorCalled);
            deps.dep3 = "!git+https://some.org/project#branch1";
            assert.deepEqual(dependenciesDownload.collectDependencies(deps), []);
            assert.equal(deps.dep3, "git+https://some.org/project#branch1");
            LogIt.Error = backup;
            $done();
        };
    }

    protected setUp() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.env.Project(<any>{
                releases: {},
                target  : {
                    name     : "DependenciesDownloadTest",
                    platforms: []
                }
            });
            this.env.Properties(<any>{
                projectHas: {
                    Cpp : {
                        Source() : boolean {
                            return false;
                        }
                    },
                    Java: {
                        Source() : boolean {
                            return false;
                        }
                    },
                    PHP : {
                        Source() : boolean {
                            return false;
                        }
                    }
                }
            });
            (<any>Loader.getInstance()).getEnvironmentArgs().project = this.env.Project();
            this.dependenciesDownload = new MockDependenciesDownload();
            $done();
        };
    }

    protected before() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.fileSystem = Loader.getInstance().getFileSystemHandler();
            this.env.ProgramArgs(new ProgramArgs());

            this.dependencies = JSON.parse(stripJsonComments(this.fileSystem.Read(this.getAbsoluteRoot() +
                "/test/resource/data/Io/Oidis/Builder/Tasks/Composition/dependenciesDownloadData.json").toString()));
            $done();
        };
    }
}
