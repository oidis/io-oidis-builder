/*! ******************************************************************************************************** *
 *
 * Copyright 2018-2019 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { Loader } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Loader.js";
import { ProgramArgs } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Structures/ProgramArgs.js";
import { TaskEnvironment } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Structures/TaskEnvironment.js";
import { MavenEnv } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Tasks/Java/MavenEnv.js";
import { RunUnitTest } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Tasks/Testing/RunUnitTest.js";
import { TypeScriptCompile } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Tasks/TypeScript/TypeScriptCompile.js";
import { XCppTest } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Tasks/XCpp/XCppTest.js";

export class RunUnitTestTest extends UnitTestRunner {
    private executor : RunUnitTest;
    private env : TaskEnvironment;
    private type : string;

    constructor() {
        super();
        // this.setMethodFilter("testDefault");
    }

    public testDefault() : void {
        assert.deepEqual(this.executor.getDependenciesTree(), [
            "product-settings:0",
            "install:dev",
            "mock-server",
            "builder-server:start",
            "var-replace",
            "copy-staticfiles:targetresource",
            "copy-staticfiles:resource",
            "copy-local-scripts:schema",
            "cache-manager:generate-prod-ts",
            "cache-manager:prepare-ts",
            "typescript-interfaces:test",
            "typescript-mappings",
            "typescript-reference:test",
            "typescript:unit",
            "cache-manager:finalize-ts-nobundle",
            "copy-local-scripts:declaration",
            "typescript-test:" + this.type
        ]);
        assert.deepEqual(TypeScriptCompile.getConfig().unitModule.src, [
            "test/unit/**/*.{ts,tsx}",
            "!**/*.d.ts"
        ]);

        assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
            "install:dev",
            "mock-server",
            "builder-server:start",
            "typescript-test:" + this.type
        ]);
        assert.deepEqual(TypeScriptCompile.getConfig().unitModule.src, [
            "test/unit/**/*.{ts,tsx}",
            "!**/*.d.ts"
        ]);
    }

    public testTypeScriptWithDependencies() : void {
        this.env.ProgramArgs().getOptions().withDependencies = true;
        assert.deepEqual(this.executor.getDependenciesTree(), [
            "product-settings:0",
            "install:dev",
            "mock-server",
            "builder-server:start",
            "var-replace",
            "copy-staticfiles:targetresource",
            "copy-staticfiles:resource",
            "copy-local-scripts:schema",
            "cache-manager:generate-prod-ts",
            "cache-manager:prepare-ts",
            "typescript-interfaces:test",
            "typescript-mappings",
            "typescript-reference:test",
            "typescript:unit",
            "cache-manager:finalize-ts-nobundle",
            "copy-local-scripts:declaration",
            "typescript-test:" + this.type
        ]);
        assert.deepEqual(TypeScriptCompile.getConfig().unitModule.src, [
            "test/unit/**/*.{ts,tsx}",
            "!**/*.d.ts"
        ]);
    }

    public testCpp() : void {
        this.env.Properties().projectHas.TypeScript.Tests = () : boolean => {
            return false;
        };
        this.env.Properties().projectHas.Cpp.Source = () : boolean => {
            return true;
        };
        this.env.Properties().projectHas.Cpp.Tests = () : boolean => {
            return true;
        };
        assert.deepEqual(this.executor.getDependenciesTree(), [
            "product-settings:0",
            "install:dev",
            "mock-server",
            "builder-server:start",
            "var-replace",
            "string-replace:xcpp",
            "copy-staticfiles:targetresource",
            "copy-staticfiles:resource",
            "copy-local-scripts:schema",
            "xcpp-interfaces:source",
            "xcpp-reference:source",
            "xcpp-compile:" + this.type,
            "copy-staticfiles:targetresource-postbuild",
            "test:xcpp" + this.type
        ]);
        assert.equal(XCppTest.getConfig().filter, "*");

        assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
            "install:dev",
            "mock-server",
            "builder-server:start",
            "test:xcpp" + this.type
        ]);
        assert.equal(XCppTest.getConfig().filter, "*");
    }

    public testJava() : void {
        this.env.Properties().projectHas.TypeScript.Tests = () : boolean => {
            return false;
        };
        this.env.Properties().projectHas.Java.Source = () : boolean => {
            return true;
        };
        this.env.Properties().projectHas.Java.Tests = () : boolean => {
            return true;
        };
        assert.deepEqual(this.executor.getDependenciesTree(), [
            "product-settings:0",
            "install:dev",
            "mock-server",
            "builder-server:start",
            "var-replace",
            "copy-staticfiles:targetresource",
            "copy-staticfiles:resource",
            "copy-local-scripts:schema",
            "maven-env:init",
            "maven-build:" + this.type,
            "maven-env:clean"
        ]);
        assert.deepEqual(MavenEnv.getConfig().unit.src, []);

        assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
            "install:dev",
            "mock-server",
            "builder-server:start",
            "maven-env:init",
            "maven-build:" + this.type,
            "maven-env:clean"
        ]);
        assert.deepEqual(MavenEnv.getConfig().unit.src, []);
    }

    public testSingleTypeScript() : void {
        /// TODO: override of program args is not populated correctly?
        this.env.ProgramArgs().getOptions().file = "/unit/someTest.ts";
        assert.deepEqual(this.executor.getDependenciesTree(), [
            "product-settings:0",
            "install:dev",
            "mock-server",
            "builder-server:start",
            "var-replace",
            "copy-staticfiles:targetresource",
            "copy-staticfiles:resource",
            "copy-local-scripts:schema",
            "cache-manager:generate-prod-ts",
            "cache-manager:prepare-ts",
            "typescript-interfaces:test",
            "typescript-mappings",
            "typescript-reference:test",
            "typescript:unit",
            "cache-manager:finalize-ts-nobundle",
            "copy-local-scripts:declaration",
            "typescript-test:" + this.type
        ]);
        // assert.deepEqual(TypeScriptCompile.getConfig().unitModule.src, [
        //     "test/unit/someTest.ts",
        //     "!**/*.d.ts"
        // ]);

        assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
            "install:dev",
            "mock-server",
            "builder-server:start",
            "typescript-test:" + this.type
        ]);
        // assert.deepEqual(TypeScriptCompile.getConfig().unitModule.src, [
        //     "test/unit/someTest.ts",
        //     "!**/*.d.ts"
        // ]);
    }

    public testSingleCpp() : void {
        this.env.ProgramArgs().getOptions().file = "/unit/someTest.cpp";
        this.env.Properties().projectHas.TypeScript.Tests = () : boolean => {
            return false;
        };
        this.env.Properties().projectHas.Cpp.Source = () : boolean => {
            return true;
        };
        this.env.Properties().projectHas.Cpp.Tests = () : boolean => {
            return true;
        };
        assert.deepEqual(this.executor.getDependenciesTree(), [
            "product-settings:0",
            "install:dev",
            "mock-server",
            "builder-server:start",
            "var-replace",
            "string-replace:xcpp",
            "copy-staticfiles:targetresource",
            "copy-staticfiles:resource",
            "copy-local-scripts:schema",
            "xcpp-interfaces:source",
            "xcpp-reference:source",
            "xcpp-compile:" + this.type,
            "copy-staticfiles:targetresource-postbuild",
            "test:xcpp" + this.type
        ]);
        // assert.equal(XCppTest.getConfig().filter, "unit/someTest.*");

        assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
            "install:dev",
            "mock-server",
            "builder-server:start",
            "test:xcpp" + this.type
        ]);
        // assert.equal(XCppTest.getConfig().filter, "unit/someTest.*");
    }

    public testSingleJava() : void {
        this.env.ProgramArgs().getOptions().file = "/unit/someTest.java";
        this.env.Properties().projectHas.TypeScript.Tests = () : boolean => {
            return false;
        };
        this.env.Properties().projectHas.Java.Source = () : boolean => {
            return true;
        };
        this.env.Properties().projectHas.Java.Tests = () : boolean => {
            return true;
        };
        assert.deepEqual(this.executor.getDependenciesTree(), [
            "product-settings:0",
            "install:dev",
            "mock-server",
            "builder-server:start",
            "var-replace",
            "copy-staticfiles:targetresource",
            "copy-staticfiles:resource",
            "copy-local-scripts:schema",
            "maven-env:init",
            "maven-build:" + this.type,
            "maven-env:clean"
        ]);
        // assert.deepEqual(MavenEnv.getConfig().unit.src, ["unit.someTest"]);

        assert.deepEqual(this.executor.getDependenciesTree("skipbuild"), [
            "install:dev",
            "mock-server",
            "builder-server:start",
            "maven-env:init",
            "maven-build:" + this.type,
            "maven-env:clean"
        ]);
        // assert.deepEqual(MavenEnv.getConfig().unit.src, ["unit.someTest"]);
    }

    protected setUp() : void {
        this.type = "unit";
        this.env = new TaskEnvironment();
        this.env.Project(<any>{
            releases: {},
            target  : {
                name     : "ExecutorTest",
                platforms: ["web"]
            }
        });
        this.env.Properties(<any>{
            dependencies: "dependencies/*/source/**",
            projectHas  : {
                Cpp       : {
                    Source() : boolean {
                        return false;
                    },
                    Tests() : boolean {
                        return false;
                    }
                },
                ESModules : () : boolean => {
                    return true;
                },
                Java      : {
                    Source() : boolean {
                        return false;
                    },
                    Tests() : boolean {
                        return false;
                    }
                },
                Python    : {
                    Tests() : boolean {
                        return false;
                    }
                },
                TypeScript: {
                    Tests() : boolean {
                        return true;
                    }
                }
            },
            sources     : "source/**"
        });
        this.env.ProgramArgs(new ProgramArgs());
        (<any>Loader.getInstance()).getEnvironmentArgs().properties = this.env.Properties();
        (<any>Loader.getInstance()).getEnvironmentArgs().project = this.env.Project();
        (<any>TypeScriptCompile).config = null;
        (<any>XCppTest).config = null;
        (<any>MavenEnv).config = null;

        this.executor = new RunUnitTest();
        this.executor.setEnvironment(this.env);
    }
}
