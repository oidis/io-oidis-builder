/*! ******************************************************************************************************** *
 *
 * Copyright 2022-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../../UnitTestRunner.js";

import { LanguageType } from "@io-oidis-commons/Io/Oidis/Commons/Enums/LanguageType.js";
import { JsonUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/JsonUtils.js";
import { LogIt } from "@io-oidis-commons/Io/Oidis/Commons/Utils/LogIt.js";
import { ObjectValidator } from "@io-oidis-commons/Io/Oidis/Commons/Utils/ObjectValidator.js";
import { StringUtils } from "@io-oidis-commons/Io/Oidis/Commons/Utils/StringUtils.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { FileSystemHandler } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Connectors/FileSystemHandler.js";
import { BaseDAO } from "../../../../../../../../source/typescript/Io/Oidis/Builder/DAO/BaseDAO.js";
import {
    IAppConfiguration,
    IDockerRegistryConfig
} from "../../../../../../../../source/typescript/Io/Oidis/Builder/Interfaces/IAppConfiguration.js";
import { Loader } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Loader.js";
import { ProgramArgs } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Structures/ProgramArgs.js";
import { TaskEnvironment } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Structures/TaskEnvironment.js";
import { Docker } from "../../../../../../../../source/typescript/Io/Oidis/Builder/Tasks/Utils/Docker.js";

export class MockDocker extends Docker {
    public cwd : string;
    public sinkData : string = "";

    public getProgramArgs() : ProgramArgs {
        return this.programArgs;
    }

    public dataSink($data : string) {
        this.sinkData += $data;
    }

    public getBuilderCfg() : IAppConfiguration {
        return this.builderConfig;
    }

    protected getCwd() : string {
        return this.cwd;
    }
}

export class DependenciesDownloadTest extends UnitTestRunner {
    private docker : MockDocker;
    private fileSystem : FileSystemHandler;
    private readonly env : TaskEnvironment;
    private testRoot : string;
    private credentials : IDockerRegistryConfig;

    constructor() {
        super();
        this.env = new TaskEnvironment();

        this.setMethodFilter(
            // "testResolveContext"
            // "testResolveContextRelative"
            // "testCheckAuth"
            // "testPullRestricted"
            // "testTag"
            // "testBuild"
            // "testBuildFailed"
            // "testBuildFromPrivate"
            // "testPush"
        );
    }

    public async testResolveContext() : Promise<void> {
        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/Compose/normal.yml";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : false,
            composeAllowed: true,
            config        : null,
            file          : "normal.yml"
        });

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/Compose";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : false,
            composeAllowed: false,
            config        : null,
            file          : ""
        });

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/UNKNOWN/normal.yml";
        assert.deepEqual(this.docker.ResolveContext(), null);

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/Compose/UNKNOWN.yml";
        assert.deepEqual(this.docker.ResolveContext(), null);

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/Compose/normal.pdf";
        assert.deepEqual(this.docker.ResolveContext(), null);

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/Compose/normal.txp";
        assert.deepEqual(this.docker.ResolveContext(), null);

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/ComposeDefault";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : true,
            composeAllowed: true,
            config        : null,
            file          : "docker-compose.yml"
        });

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/ComposeDefault/docker-compose.yml";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : false,
            composeAllowed: true,
            config        : null,
            file          : "docker-compose.yml"
        });

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/ComposeDefault/Dockerfile";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : true,
            composeAllowed: false,
            config        : null,
            file          : "Dockerfile"
        });

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/FailBuild/Dockerfile";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : true,
            composeAllowed: false,
            config        : {
                build  : {
                    advanced: {
                        nocache: true
                    },
                    name    : "oidis-test/fail-build"
                },
                version: "1.0.0"
            },
            file          : "Dockerfile"
        });

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/FailBuild/docker.config.json";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : true,
            composeAllowed: false,
            config        : {
                build  : {
                    advanced: {
                        nocache: true
                    },
                    name    : "oidis-test/fail-build"
                },
                version: "1.0.0"
            },
            file          : ""
        });

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/NormalBuild/docker.config.json";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : true,
            composeAllowed: false,
            config        : {
                build  : {
                    advanced: {
                        nocache: true
                    },
                    name    : "oidis-test/normal-build"
                },
                version: "1.0.0"
            },
            file          : ""
        });

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/NormalBuild/docker-dev.config.json";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : true,
            composeAllowed: false,
            config        : {
                build  : {
                    advanced: {
                        nocache: true
                    },
                    name    : "oidis-test-DEV/normal-build"
                },
                version: "<? @var project.version ?>"  // stringreplace vars are not initialized in unit-tests
            },
            file          : ""
        });

        this.docker.getProgramArgs().getOptions().file = "UNKNOWN";
        assert.deepEqual(this.docker.ResolveContext(this.testRoot + "/NormalBuild"), {
            buildAllowed  : true,
            composeAllowed: false,
            config        : {
                build  : {
                    advanced: {
                        nocache: true
                    },
                    name    : "oidis-test/normal-build"
                },
                version: "1.0.0"
            },
            file          : ""
        });
    }

    public async testResolveContextRelative() : Promise<void> {
        this.docker.cwd = this.testRoot + "/Compose";
        this.docker.getProgramArgs().getOptions().file = "normal.yml";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : false,
            composeAllowed: true,
            config        : null,
            file          : "normal.yml"
        });

        this.docker.cwd = this.testRoot + "/Compose";
        this.docker.getProgramArgs().getOptions().file = "";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : false,
            composeAllowed: false,
            config        : null,
            file          : ""
        });

        this.docker.cwd = this.testRoot;
        this.docker.getProgramArgs().getOptions().file = "/UNKNOWN/normal.yml";
        assert.deepEqual(this.docker.ResolveContext(), null);

        this.docker.cwd = this.testRoot + "/Compose";
        this.docker.getProgramArgs().getOptions().file = "UNKNOWN.yml";
        assert.deepEqual(this.docker.ResolveContext(), null);

        this.docker.cwd = this.testRoot + "/Compose";
        this.docker.getProgramArgs().getOptions().file = "normal.pdf";
        assert.deepEqual(this.docker.ResolveContext(), null);

        this.docker.cwd = this.testRoot + "/Compose";
        this.docker.getProgramArgs().getOptions().file = "normal.txp";
        assert.deepEqual(this.docker.ResolveContext(), null);

        this.docker.cwd = this.testRoot + "/ComposeDefault";
        this.docker.getProgramArgs().getOptions().file = "";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : true,
            composeAllowed: true,
            config        : null,
            file          : "docker-compose.yml"
        });

        this.docker.cwd = this.testRoot + "/ComposeDefault";
        this.docker.getProgramArgs().getOptions().file = "docker-compose.yml";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : false,
            composeAllowed: true,
            config        : null,
            file          : "docker-compose.yml"
        });

        this.docker.cwd = this.testRoot + "/ComposeDefault";
        this.docker.getProgramArgs().getOptions().file = "UNKNOWN";
        assert.deepEqual(this.docker.ResolveContext("Dockerfile"), {
            buildAllowed  : true,
            composeAllowed: false,
            config        : null,
            file          : "Dockerfile"
        });
    }

    public async testCheckAuth() : Promise<void> {
        await this.docker.CheckAuth(this.credentials);
        const cred : IDockerRegistryConfig = JsonUtils.Clone(this.credentials);
        cred.pass = "unknownpass";
        try {
            await this.docker.CheckAuth(cred);
        } catch ($e) {
            assert.patternEqual($e.message, "(HTTP code 401) unexpected - Get *: unauthorized: incorrect username or password*");
        }
    }

    public async testPull() : Promise<void> {
        this.timeoutLimit(30000);

        this.docker.sinkData = "";
        await this.docker.PullImage("hello-world");
        assert.patternEqual(this.docker.sinkData.replace(/\s+/g, ""), "" +
            "Pullingfromlibrary/hello-world[latest]" +
            "*" +
            "Digest:sha256:*:" +
            "*" +
            "Downloadednewerimageforhello-world:latest");

        this.docker.sinkData = "";
        await this.docker.PullImage("hello-world");
        assert.patternEqual(this.docker.sinkData, "" +
            "Pulling from library/hello-world [latest]Digest: sha256:*: " +
            "Image is up to date for hello-world:latest");
    }

    public async testPullRestricted() : Promise<void> {
        this.timeoutLimit(30000);

        this.docker.getBuilderCfg().dockerRegistry = null;
        this.docker.sinkData = "";
        try {
            await this.docker.PullImage("nxpwebix/webix-hello-world:latest");
        } catch (e) {
            this.docker.sinkData = e.message;
        }

        assert.patternEqual(this.docker.sinkData, "" +
            "(HTTP code 404) unexpected - pull access denied for nxpwebix/webix-hello-world, repository does not exist or may " +
            "require 'docker login': denied: requested access to the resource is denied ");

        this.docker.sinkData = "";
        this.docker.getBuilderCfg().dockerRegistry = JsonUtils.Clone(this.credentials);
        await this.docker.PullImage("nxpwebix/webix-hello-world:latest");
        assert.patternEqual(this.docker.sinkData.replace(/\s+/g, ""), "" +
            "Pullingfromnxpwebix/webix-hello-world[latest]" +
            "*" +
            "Digest:sha256:*:" +
            "*" +
            "Downloadednewerimagefornxpwebix/webix-hello-world:latest");

        this.docker.sinkData = "";
        this.docker.getBuilderCfg().dockerRegistry = JsonUtils.Clone(this.credentials);
        await this.docker.PullImage("nxpwebix/webix-hello-world:latest");
        assert.patternEqual(this.docker.sinkData, "" +
            "Pulling from nxpwebix/webix-hello-world [latest]Digest: sha256:*: " +
            "Image is up to date for nxpwebix/webix-hello-world:latest");
    }

    public async testTag() : Promise<void> {
        await this.docker.PullImage("hello-world:latest");

        const ids : string[] = await this.listImages("hello-world:latest");
        assert.equal(ids.length, 1);

        let tags : any[] = (await this.docker.getDocker().listImages())
            .map(($image : any) : any => {
                if (!ObjectValidator.IsEmptyOrNull($image) && ObjectValidator.IsArray($image.RepoTags)) {
                    if ($image.Id === ids[0]) {
                        return $image.RepoTags;
                    }
                }
                return [];
            })
            .filter(($image) => !ObjectValidator.IsEmptyOrNull($image));
        assert.equal(tags.length, 1);

        assert.deepEqual(tags[0], ["hello-world:latest"]);

        await this.docker.TagImage("hello-world:latest", "custom-tag:version");

        tags = (await this.docker.getDocker().listImages())
            .map(($image : any) : any => {
                if (!ObjectValidator.IsEmptyOrNull($image) && ObjectValidator.IsArray($image.RepoTags)) {
                    if ($image.Id === ids[0]) {
                        return $image.RepoTags;
                    }
                }
                return [];
            })
            .filter(($image) => !ObjectValidator.IsEmptyOrNull($image));
        assert.equal(tags.length, 1);

        assert.deepEqual(tags[0], ["custom-tag:version", "hello-world:latest"]);
    }

    public async testBuild() : Promise<void> {
        this.timeoutLimit(30000);

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/NormalBuild";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : true,
            composeAllowed: false,
            config        : {
                build  : {
                    advanced: {
                        nocache: true
                    },
                    name    : "oidis-test/normal-build"
                },
                version: "1.0.0"
            },
            file          : ""
        });

        this.docker.sinkData = "";
        await this.docker.BuildImage();
        assert.patternEqual(this.docker.sinkData.replace(/\s+/g, ""), "" +
            "Step1/5:FROMhello-world" +
            "Pullingfromlibrary/hello-world[latest]" +
            "*" +
            "Step2/5:MAINTAINERMichalKelnar\"michal@oidis.io\"" +
            "*" +
            "Step3/5:ADD\"https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h\"skipcache" +
            "*" +
            "Step4/5:COPYdocker.config.json/tmp/docker.config.json" +
            "*" +
            "Step5/5:CMD[\"/sbin/my_init\"]" +
            "*" +
            "Successfullybuilt*" +
            "Successfullytaggedoidis-test/normal-build:1.0.0");
    }

    public async testBuildFailed() : Promise<void> {
        this.timeoutLimit(30000);

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/FailBuild";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : true,
            composeAllowed: false,
            config        : {
                build  : {
                    advanced: {
                        nocache: true
                    },
                    name    : "oidis-test/fail-build"
                },
                version: "1.0.0"
            },
            file          : ""
        });

        this.docker.sinkData = "";
        try {
            await this.docker.BuildImage();
        } catch (e) {
            this.docker.sinkData = e.message;
        }
        assert.patternEqual(this.docker.sinkData, "" +
            "invalid reference format: repository name must be lowercase");
    }

    public async testBuildFromPrivate() : Promise<void> {
        this.timeoutLimit(30000);

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/PrivateBuild";
        assert.deepEqual(this.docker.ResolveContext(), {
            buildAllowed  : true,
            composeAllowed: false,
            config        : {
                build  : {
                    advanced: {
                        nocache: true
                    },
                    name    : "nxpwebix-test/private-build"
                },
                version: "2021.0.0"
            },
            file          : ""
        });

        this.docker.sinkData = "";
        this.docker.getBuilderCfg().dockerRegistry = JsonUtils.Clone(this.credentials);
        await this.docker.BuildImage();
        assert.patternEqual(this.docker.sinkData.replace(/\s+/g, ""), "" +
            "Step1/5:FROMnxpwebix/webix-hello-world:latest" +
            "*" +
            "Step2/5:MAINTAINERMichalKelnar\"michal@oidis.io\"" +
            "*" +
            "Step3/5:ADD\"https://www.random.org/cgi-bin/randbyte?nbytes=10&format=h\"skipcache" +
            "*" +
            "Step4/5:COPYdocker.config.json/tmp/docker.config.json" +
            "*" +
            "Step5/5:CMD[\"/sbin/my_init\"]" +
            "*" +
            "Successfullybuilt*" +
            "Successfullytaggednxpwebix-test/private-build:2021.0.0");
    }

    public async testPush() : Promise<void> {
        this.timeoutLimit(60000);

        this.docker.getProgramArgs().getOptions().file = this.testRoot + "/NormalBuild";
        this.docker.ResolveContext();
        await this.docker.BuildImage();

        this.docker.sinkData = "";
        try {
            await this.docker.PushImage("oidis-test/normal-build:1.0.0", "oidis/hello-world:latest");
        } catch ($e) {
            this.docker.sinkData = $e.message;
        }
        assert.equal(this.docker.sinkData, "(HTTP code 400) unexpected - Bad parameters and missing X-Registry-Auth: EOF ");

        this.docker.sinkData = "";
        this.docker.getBuilderCfg().dockerRegistry = JsonUtils.Clone(this.credentials);
        await this.docker.PushImage("oidis-test/normal-build:1.0.0", "oidis/hello-world:latest");
        assert.patternEqual(this.docker.sinkData, "" +
            "The push refers to repository [docker.io/oidis/hello-world]" +
            "Preparing *" +
            "*" +
            "Pushing *" +
            "*" +
            "Pushed *" +
            "*" +
            "latest: digest: sha256:* size: *");
    }

    protected async setUp() : Promise<void> {
        this.env.Project(<any>{
            releases: {},
            target  : {
                name     : "DockerTest",
                platforms: []
            }
        });
        this.env.Properties(<any>{
            projectHas: {
                Cpp : {
                    Source() : boolean {
                        return false;
                    }
                },
                Java: {
                    Source() : boolean {
                        return false;
                    }
                },
                PHP : {
                    Source() : boolean {
                        return false;
                    }
                }
            }
        });
        (<any>Loader.getInstance()).getEnvironmentArgs().project = this.env.Project();
        this.docker = new MockDocker();

        // private conf content is not available in test runner so bypass
        await new Promise<void>(($resolve, $reject) : void => {
            const loader : BaseDAO = new BaseDAO();
            loader.setConfigurationPath(this.getAbsoluteRoot() + "/../../private.conf.jsonp");
            loader.OnErrorHandler(($error : ErrorEvent, $path? : string) : void => {
                LogIt.Warning("Failed to load private config from: " + $path + "\n" + $error.message);
                $reject($error);
            });
            loader.Load(LanguageType.EN, () : void => {
                this.credentials = (<any>loader.getStaticConfiguration()).dockerRegistry;
                $resolve();
            }, true);
        });

        if (ObjectValidator.IsEmptyOrNull(this.credentials) || ObjectValidator.IsEmptyOrNull(this.credentials.pass)) {
            throw new Error("Docker registry credentials has to be specified in private.conf.jsonp " +
                "for this project to pass this tests.");
        }
        if (ObjectValidator.IsEmptyOrNull(this.credentials.url)) {
            this.credentials.url = "https://index.docker.io/v1/";  // resolved from default config in normal load
        }
        let conflicts : string[] = await this.listImages("nxpwebix-test/private-build:2021.0.0");
        try {
            if (conflicts.length > 0) {
                for await(const imageName of conflicts) {
                    await this.docker.getDocker().getImage(imageName).remove({force: true});
                }
                conflicts = await this.listImages("nxpwebix-test/private-build:2021.0.0");
            }
        } catch (e) {
            // dummy
        }
        assert.deepEqual(conflicts, []);

        conflicts = await this.listImages("nxpwebix/webix-hello-world:latest");
        try {
            if (conflicts.length > 0) {
                for await(const imageName of conflicts) {
                    await this.docker.getDocker().getImage(imageName).remove({force: true});
                }
                conflicts = await this.listImages("nxpwebix/webix-hello-world:latest");
            }
        } catch (e) {
            // dummy
        }
        assert.deepEqual(conflicts, []);

        conflicts = await this.listImages("oidis-test/normal-build:1.0.0");
        try {
            if (conflicts.length > 0) {
                for await(const imageName of conflicts) {
                    await this.docker.getDocker().getImage(imageName).remove({force: true});
                }
                conflicts = await this.listImages("oidis-test/normal-build:1.0.0");
            }
        } catch (e) {
            // dummy
        }
        assert.deepEqual(conflicts, []);

        conflicts = await this.listImages("oidis/hello-world:latest");
        try {
            if (conflicts.length > 0) {
                for await(const imageName of conflicts) {
                    await this.docker.getDocker().getImage(imageName).remove({force: true});
                }
                conflicts = await this.listImages("oidis/hello-world:latest");
            }
        } catch (e) {
            // dummy
        }
        assert.deepEqual(conflicts, []);

        conflicts = await this.listImages("hello-world:latest");
        try {
            if (conflicts.length > 0) {
                for await(const imageName of conflicts) {
                    await this.docker.getDocker().getImage(imageName).remove({force: true});
                }
                conflicts = await this.listImages("hello-world:latest");
            }
        } catch (e) {
            // dummy
        }
        assert.deepEqual(conflicts, []);

        this.docker.getBuilderCfg().dockerRegistry = null;
    }

    protected async before() : Promise<void> {
        this.fileSystem = Loader.getInstance().getFileSystemHandler();
        this.env.ProgramArgs(new ProgramArgs());
        this.testRoot = this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Builder/Tasks/Docker";
    }

    private async listImages($imageName : string) : Promise<string[]> {
        const images = await this.docker.getDocker().listImages();
        return images
            .map(($image : any) : string => {
                if (!ObjectValidator.IsEmptyOrNull($image) && ObjectValidator.IsArray($image.RepoTags)) {
                    if ($image.RepoTags.some(($tag) => $tag === $imageName)) {
                        return $image.Id;
                    }
                }
                return "";
            })
            .filter(($image) => !StringUtils.IsEmpty($image));
    }
}
