/*! ******************************************************************************************************** *
 *
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { ProtectionType } from "@io-oidis-localhost/Io/Oidis/Localhost/Enums/ProtectionType.js";
import { CryptoEnvironment } from "../../../../../../../source/typescript/Io/Oidis/Builder/Tasks/Utils/CryptoEnvironment.js";

export class CryptoEnvironmentTest extends UnitTestRunner {

    constructor() {
        super();
        // this.setMethodFilter("testMethod");
    }

    public testSignatureMap() : void {
        const cryptoEnvironment : CryptoEnvironment = new CryptoEnvironment();
        assert.equal(JSON.stringify(cryptoEnvironment.getProtectionType()), JSON.stringify(ProtectionType.NONE));
        cryptoEnvironment.getProtectionMap().foo = "bar";
        assert.equal(JSON.stringify(cryptoEnvironment.getProtectionMap()), JSON.stringify({foo: "bar"}));
    }

    public testMethod() : void {
        const cryptoEnvironment : CryptoEnvironment = new CryptoEnvironment();
        assert.doesNotThrow(() => {
            cryptoEnvironment.Method();
        });
        assert.doesNotThrow(() => {
            cryptoEnvironment.Method("");
        });
        assert.throws(() => {
            cryptoEnvironment.Method("foo");
        });
        assert.doesNotThrow(() => {
            cryptoEnvironment.Method("sign");
        });
        assert.doesNotThrow(() => {
            cryptoEnvironment.Method("encrypt");
        });
        assert.doesNotThrow(() => {
            cryptoEnvironment.Method(true);
            assert.equal(cryptoEnvironment.Method(), ProtectionType.SIGNATURE);
        });
        assert.doesNotThrow(() => {
            cryptoEnvironment.Method(false);
            assert.equal(cryptoEnvironment.Method(), ProtectionType.NONE);
        });
        assert.doesNotThrow(() => {
            cryptoEnvironment.Method("sign");
            assert.equal(cryptoEnvironment.Method(), ProtectionType.SIGNATURE);
        });
        assert.doesNotThrow(() => {
            cryptoEnvironment.Method("encrypt");
            assert.equal(cryptoEnvironment.Method(), ProtectionType.ENCRYPTION);
        });
    }

    public testKeys() : void {
        const cryptoEnvironment : CryptoEnvironment = new CryptoEnvironment();
        const publicKey : any = cryptoEnvironment.PublicKeyData();
        const privateKey : any = cryptoEnvironment.PrivateKeyData();
        assert.notEqual(publicKey, null);
        assert.notEqual(privateKey, null);
        assert.equal(JSON.stringify(cryptoEnvironment.PublicKeyData()), JSON.stringify(publicKey));
        assert.equal(JSON.stringify(cryptoEnvironment.PrivateKeyData()), JSON.stringify(privateKey));
    }
}
