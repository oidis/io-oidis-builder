/*! ******************************************************************************************************** *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2023 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import { UnitTestRunner } from "../UnitTestRunner.js";

import { IUnitTestRunnerPromise } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/BaseUnitTestRunner.js";
import { assert } from "@io-oidis-commons/test/unit/Io/Oidis/Commons/UnitTestEnvironment.js";
import { FileSystemHandler } from "../../../../../../../source/typescript/Io/Oidis/Builder/Connectors/FileSystemHandler.js";
import { Loader } from "../../../../../../../source/typescript/Io/Oidis/Builder/Loader.js";
import { Patcher } from "../../../../../../../source/typescript/Io/Oidis/Builder/Utils/Patcher.js";

export class PatcherTest extends UnitTestRunner {
    private testDir : string;
    private fileSystem : FileSystemHandler;

    constructor() {
        super();
        // this.setMethodFilter("testPatchMultipleKeys");
    }

    public testPatch() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const origData : string = this.fileSystem.Read(this.testDir + "/patchMe.txt").toString();
            Patcher.getInstance().Patch(this.testDir + "/patchMe.txt", "to-be-patched", "has-been-patched", () : void => {
                assert.equal(
                    this.fileSystem.Read(this.testDir + "/patchMe.txt").toString(),
                    this.fileSystem.Read(this.testDir + "/patched.txt").toString());
                Patcher.getInstance().Rollback(() : void => {
                    assert.equal(this.fileSystem.Read(this.testDir + "/patchMe.txt").toString(), origData);
                    $done();
                });
            });
        };
    }

    public testPatchMultiFiles() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const origData : string[] = [
                this.fileSystem.Read(this.testDir + "/patchMe_a.txt").toString(),
                this.fileSystem.Read(this.testDir + "/patchMe_b.txt").toString()
            ];
            Patcher.getInstance().PatchMultiFiles([
                this.testDir + "/patchMe_a.txt",
                this.testDir + "/patchMe_b.txt"
            ], "Utils.ObjectValidator", "Utils.<patched>", () : void => {
                assert.equal(
                    this.fileSystem.Read(this.testDir + "/patchMe_a.txt").toString(),
                    this.fileSystem.Read(this.testDir + "/patched_a.txt").toString());
                assert.equal(
                    this.fileSystem.Read(this.testDir + "/patchMe_b.txt").toString(),
                    this.fileSystem.Read(this.testDir + "/patched_b.txt").toString());
                Patcher.getInstance().Rollback(() : void => {
                    assert.equal(this.fileSystem.Read(this.testDir + "/patchMe_a.txt").toString(), origData[0]);
                    assert.equal(this.fileSystem.Read(this.testDir + "/patchMe_b.txt").toString(), origData[1]);
                    $done();
                });
            });
        };
    }

    public testPatchMultiKeys() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            const origData : string = this.fileSystem.Read(this.testDir + "/patchMe_1.txt").toString();
            Patcher.getInstance().PatchMultiKeys(this.testDir + "/patchMe_1.txt",
                {"Utils.ObjectValidator": "Utils.<patched1>", "ObjectValidator": "<patched2>"}, () : void => {
                    assert.equal(
                        this.fileSystem.Read(this.testDir + "/patchMe_1.txt").toString(),
                        this.fileSystem.Read(this.testDir + "/patched_1.txt").toString());
                    Patcher.getInstance().Rollback(() : void => {
                        assert.equal(this.fileSystem.Read(this.testDir + "/patchMe_1.txt").toString(), origData);
                        $done();
                    });
                });
        };
    }

    protected setUp() : IUnitTestRunnerPromise {
        return ($done : () => void) : void => {
            this.testDir = this.getAbsoluteRoot() + "/test/resource/data/Io/Oidis/Builder/Utils";
            this.fileSystem = Loader.getInstance().getFileSystemHandler();
            $done();
        };
    }
}
