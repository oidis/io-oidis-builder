# /* ********************************************************************************************************* *
#  *
#  * Copyright 2019 Oidis
#  *
#  * SPDX-License-Identifier: BSD-3-Clause
#  * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
#  * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
#  *
#  * ********************************************************************************************************* */
import argparse


def Process(cwd, args):
    print('>' + cwd + ': ', args)
    return args


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some args')
    parser.add_argument('arg1')
    parser.add_argument('arg2')

    args = parser.parse_args()
    Process('cli call',[args.arg1, args.arg2])